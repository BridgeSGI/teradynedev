package com.teradyne.file.util.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.ui.backend.servlets.BaseServlet;

public class FileDownloadServlet extends BaseServlet {
  
  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  private static final long serialVersionUID = -4493606271267841767L;

  @Override
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
        
    log.info("Download pdf file servlet hits");
    try {
      HttpSession session = request.getSession();
      
      String directory = (String) session.getAttribute("FilePath");
      String pdfFileName = request.getParameter("file");
      
      String filePath = directory+pdfFileName;
      
      log.debug("Directory:"+directory);
      log.debug("PdfFileName:"+pdfFileName);
      log.debug("filePath:"+filePath);
      
      if(!XmlUtils.isVoid(pdfFileName)){
        
        File pdfFile = new File(filePath);
        if(pdfFile.exists()){
          
          response.setContentType("application/pdf");
          response.addHeader("Content-Disposition", "attachment; filename=" + pdfFileName);
          response.setContentLength((int) pdfFile.length());
          
          FileInputStream fin = new FileInputStream(pdfFile);
          ServletOutputStream outStream = response.getOutputStream();
          
          int bytes ;
          while ((bytes = fin.read())!= -1) {
            outStream.write(bytes);
          }
          
          outStream.close();
          outStream.flush();
          fin.close();
        }
      }else{
        log.debug("File is not exist");
      }
    } catch (Exception e) {
      log.debug(e.getMessage()+" "+e.getCause());
      if(log.isVerboseEnabled()){
        log.verbose(e);
      }
    }

  }

}

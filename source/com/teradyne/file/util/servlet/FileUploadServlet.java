package com.teradyne.file.util.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.ui.backend.servlets.BaseServlet;

public class FileUploadServlet extends BaseServlet {
  private static final long serialVersionUID = 1L;

  // location to store file uploaded
  //private static final String UPLOAD_DIRECTORY = "cert";
  // upload settings
  private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3; // 3MB
  private static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
  private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");

  /**
   * Upon receiving file upload submission, parses the request to read upload data and saves the
   * file on disk.
   */
  @Override
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
//    System.out.println("I came into the FileUploadServlet");
	  // checks if the request actually contains upload file
    HttpSession session = request.getSession();
    
    log.info("Upload servlet hits");
    
    try {
      
      String fileName = null;
      String quote = null;
      String fdls = null;
      String orderLineKey = null;
      
      if (!ServletFileUpload.isMultipartContent(request)) {
        log.debug("Form is not multipart form data");
        PrintWriter writer = response.getWriter();
        writer.println("Error: Form must has enctype=multipart/form-data.");
        writer.flush();
        return;
      }
      
      // configures upload settings
      DiskFileItemFactory factory = new DiskFileItemFactory();
      // sets memory threshold - beyond which files are stored in disk
      factory.setSizeThreshold(MEMORY_THRESHOLD);
      // sets temporary location to store files
      factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

      ServletFileUpload upload = new ServletFileUpload(factory);

      // sets maximum size of upload file
      upload.setFileSizeMax(MAX_FILE_SIZE);

      // sets maximum size of request (include file + form data)
      upload.setSizeMax(MAX_REQUEST_SIZE);

      // constructs the directory path to store upload file
      // this path is relative to application's directory
      String uploadPath = (String) session.getAttribute("FilePath");
      if(XmlUtils.isVoid(uploadPath)){
        throw new Exception("File Path is not configured");
      }
      
      log.info("File Directory:"+uploadPath);
      // creates the directory if it does not exist
      File uploadDir = new File(uploadPath);
      if (!uploadDir.exists()) {
        uploadDir.mkdir();
      }
      
      // parses the request's content to extract file data
      @SuppressWarnings("unchecked")
      List<FileItem> formItems = upload.parseRequest(request);
      if (formItems != null && formItems.size() > 0) {
        for (FileItem item : formItems) {
                    
          if ((item.getFieldName()).equals("xml:/Order/@RequiredKey")) {
            
            if(!XmlUtils.isVoid(item.getString())){
              orderLineKey = item.getString();
            }
            continue;
          }
          
          if ((item.getFieldName()).equals("xml:/Order/@TempQuote")) {

            if(!XmlUtils.isVoid(item.getString())){
              quote = item.getString();
            }              
            continue;
          }
          
          if ((item.getFieldName()).equals("xml:/Order/@TempFDLS")) {

              if(!XmlUtils.isVoid(item.getString())){
                fdls = item.getString();
              }              
              continue;
          }
          // processes only fields that are not form fields
          if (!item.isFormField() && !XmlUtils.isVoid(item.getName())) {
            fileName = new File(item.getName()).getName();
            if(!XmlUtils.isVoid(fileName)){
              fileName = fileName.substring(fileName.lastIndexOf("\\")+1, fileName.length());
            }
//            System.out.println(fileName);
            String filePath = uploadPath + File.separator + fileName;
            File storeFile = new File(filePath);

            // saves the file on disk
            item.write(storeFile);
            request.setAttribute("message", "Upload has been done successfully!");
            log.info("Upload has been done succesffully" );
          }
        }
      }
      
      this.setFileName(request,quote,fdls,fileName,orderLineKey);
    } catch (Exception ex) {
      log.debug("Upload failed:"+ex.getMessage()+" "+ex.getCause());
      if(log.isVerboseEnabled()){
        log.verbose(ex);
      }
      request.setAttribute("message", "There was an error: " + ex.getMessage());
    }
//    System.out.println(session.getAttribute("EKey"));
    String docType = (String) session.getAttribute("docType");
    String url = "";
    if (docType.equals(Constants.DOCUMENT_TYPE_SO)) {
    	url = "/smcfs/console/order.detail?EntityKey=" + session.getAttribute("EKey");
    } else if (docType.equals(Constants.DOCUMENT_TYPE_RO)) {
    	url = "/smcfs/console/return.detail?EntityKey=" + session.getAttribute("EKey");
    }
    getServletContext().getRequestDispatcher(url).forward(request, response);
  }
  
  private void setFileName(HttpServletRequest request, String quote,String fdls, String fileName,String orderLineKey){
    
    log.debug("fileName:"+fileName);
    request.setAttribute("orderLineKey", orderLineKey);
    if(!XmlUtils.isVoid(quote)){
      log.debug("Is Quote? True");
      request.setAttribute("quote", fileName);
      request.setAttribute("quoteOrderLineId",orderLineKey+"_quote");
    } else if (!XmlUtils.isVoid(fdls)) {
    	log.debug("Is FDLS? True");
    	request.setAttribute("fdls", fileName);
        request.setAttribute("fdlsOrderLineId",orderLineKey+"_fdls");
    } else {
      log.debug("Is Quote? False");
      request.setAttribute("confirm", fileName);
      request.setAttribute("confirmOrderLineId",orderLineKey+"_confirm");
    }
  }
}

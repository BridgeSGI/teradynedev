package com.teradyne.customer.audit;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class GetCustomerAudits {

	public Document getAuditHeader(YFSEnvironment env, Document inXML) throws Exception {
		YFCDocument inDocXML = YFCDocument.getDocumentFor(inXML);
		YFCDocument getAuditListTemplateDoc = YFCDocument.parse("<AuditList>" +
																	"<Audit AuditKey=\"\" TableName=\"\"" +
																		" TableKey=\"\" Operation=\"\"" +
																		" Reference1=\"\" Reference2=\"\"" +
																		" Reference3=\"\" Reference4=\"\"" +
																		" Createts=\"\" />" +
																"</AuditList>");
		YFCDocument getAuditListForOrgOutput = SterlingUtil.callAPI(env, Constants.GET_AUDIT_LIST, inDocXML, getAuditListTemplateDoc);
		String orgKey = inDocXML.getDocumentElement().getAttribute(XMLConstants.TABLE_KEY);
		YFCDocument getCustomerListInputDoc = YFCDocument.parse("<Customer BuyerOrganizationCode=\"" + orgKey + "\"/>");
		YFCDocument getCustomerListTemplateDoc = YFCDocument.parse("<CustomerList>" +
																		"<Customer CustomerKey=\"\"/>" +
																   "</CustomerList>");
		YFCDocument getCustomerListOutputDoc = SterlingUtil.callAPI(env, Constants.GET_CUSTOMER_LIST, getCustomerListInputDoc, getCustomerListTemplateDoc);
		YFCElement customerList = getCustomerListOutputDoc.getDocumentElement();
		YFCElement customer = customerList.getChildElement(Constants.CUSTOMER);
		YFCDocument getAuditListForCustomerOutput = null;
		if (!XmlUtils.isVoid(customer)) {
			String custKey = customer.getAttribute(XMLConstants.CUSTOMER_KEY);
			YFCDocument inputDoc = YFCDocument.getDocumentFor(inXML);
			inputDoc.getDocumentElement().setAttribute(XMLConstants.TABLE_KEY, custKey);
			inputDoc.getDocumentElement().setAttribute(XMLConstants.TABLE_NAME, "YFS_CUSTOMER");
			getAuditListForCustomerOutput = SterlingUtil.callAPI(env, Constants.GET_AUDIT_LIST, inputDoc, getAuditListTemplateDoc);
		}
		Map<String, YFCElement> map = new TreeMap<String, YFCElement>();
		YFCElement auditList = getAuditListForOrgOutput.getDocumentElement();
		sortAudits(map, auditList.getChildren(XMLConstants.AUDIT));
		if (!XmlUtils.isVoid(getAuditListForCustomerOutput)) {
			auditList = getAuditListForCustomerOutput.getDocumentElement();
			sortAudits(map, auditList.getChildren(XMLConstants.AUDIT));
		}
		YFCDocument outDoc = YFCDocument.createDocument(XMLConstants.AUDIT_LIST);
		for(Iterator<String> iter = map.keySet().iterator(); iter.hasNext();){
			String key = iter.next();
			YFCElement audit  = map.get(key);
			outDoc.getDocumentElement().importNode(audit);
		}
		return outDoc.getDocument();
	}
	
	private void sortAudits(Map<String, YFCElement> map, YFCIterable<YFCElement> audits) {
		if (!XmlUtils.isVoid(audits)) {
			for (YFCElement audit : audits) {
				String key = audit.getAttribute(XMLConstants.AUDIT_KEY);
				map.put(key, audit);
			}
		}
	}

	public Document getAuditDetail(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		YFCDocument getAuditListOut = SterlingUtil.callAPI(env, Constants.GET_AUDIT_LIST, YFCDocument.getDocumentFor(inputDoc), "");
		String xmlStr = getAuditListOut.getDocumentElement().getChildElement(XMLConstants.AUDIT).getAttribute("AuditXml");
		String shortStr = xmlStr.substring(38);
		YFCDocument outDoc = YFCDocument.parse(shortStr);
		return outDoc.getDocument();
	}
}

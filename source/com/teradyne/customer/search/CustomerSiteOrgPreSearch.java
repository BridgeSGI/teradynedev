package com.teradyne.customer.search;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.SterlingUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;

public class CustomerSiteOrgPreSearch implements YIFCustomApi {

	private static final String CUSTOMER = "C";
	private static final String CUSTOMER_SITE = "CS";

	@Override
	public void setProperties(Properties p) throws Exception {}
	
	public Document initiateSearch(YFSEnvironment env, Document inXML) {
		YFCDocument inDocXML = YFCDocument.getDocumentFor(inXML);
		Document searchOutput = null;
		String searchEntityKey = determineSearchEntity(inDocXML);
		if (searchEntityKey.equals(CUSTOMER)) {
			searchOutput = searchCustomer(env, inDocXML);
		} else  if (searchEntityKey.equals(CUSTOMER_SITE)) {
			searchOutput = searchCustomerSite(env, inDocXML);
		}
		return searchOutput;
	}

	private Document searchCustomerSite(YFSEnvironment env, YFCDocument inDocXML) {
		return search(env, inDocXML);
	}

	private Document searchCustomer(YFSEnvironment env, YFCDocument inDocXML) {
		Document output = search(env, inDocXML);
		filterOutNodesFromOutput(output);
		return output;
	}

	private Document search(YFSEnvironment env, YFCDocument inDocXML) {
		YFCDocument inputXML = formInputXML(inDocXML);
		Document output = this.getOrganizationList(env, inputXML);
		if (XmlUtils.isVoid(output)) {
			return YFCDocument.createDocument("OrganizationList").getDocument();
		}
		setRoles(output);
		setExtraParams(inputXML, output);
		return output;
	}

	private void filterOutNodesFromOutput(Document output) {
		YFCDocument outDocXML = YFCDocument.getDocumentFor(output);
		YFCElement organizationList = outDocXML.getDocumentElement();
		YFCIterable<YFCElement> allOrgs = organizationList.getChildren(Constants.ORGANIZATION);
		if (!XmlUtils.isVoid(allOrgs)) {
			for (YFCElement organization : allOrgs) {
				String roles = organization.getAttribute(Constants.ROLE_LIST);
				if (roles.contains(Constants.N)) {
					organizationList.removeChild(organization);
				}
			}
		}
	}

	private YFCDocument formInputXML(YFCDocument inDocXML) {
		Map<String, YFCElement> addressDetails = fetchAddressDetails(inDocXML);
		if (addressDetails != null) {
			if (addressDetails.containsKey("S")) {
				formXMLWithCorporatePersonInfo(inDocXML, addressDetails.get("S"));
			} else if (addressDetails.containsKey("B")) {
				formXMLWithBillToPersonInfo(inDocXML, addressDetails.get("B"));
			}
		}
		return inDocXML;
	}

	private String determineSearchEntity(YFCDocument inDocXML) {
		YFCElement organization = inDocXML.getDocumentElement();
		String searchCustomer = organization.getAttribute(Constants.SEARCH_CUSTOMER);
		if (!XmlUtils.isVoid(searchCustomer)) {
			if (searchCustomer.equals(Constants.Y)) {
				return CUSTOMER;
			} else {
				return CUSTOMER_SITE;
			}
		}
		return CUSTOMER_SITE;
	}

	private void setExtraParams(YFCDocument inputXML, Document output) {
		YFCDocument outDocXML = YFCDocument.getDocumentFor(output);
		YFCElement organization = inputXML.getDocumentElement();
		YFCElement extraParams = organization.getChildElement("ExtraParams");
		Map<String, String> allExtraParams = null;
		if (extraParams != null) {
			allExtraParams = extraParams.getAttributes();
		} else {
			allExtraParams = new HashMap<String, String>();
			allExtraParams.put("ViewDetails","Y");
			allExtraParams.put("CreateCustomerOrg","Y");
			allExtraParams.put("CreateCustomerSiteOrg","Y");
			allExtraParams.put("ViewAudits","Y");
			allExtraParams.put("AddParticipantEnt","N");
			allExtraParams.put("AddChildOrg","N");
		}
		outDocXML.getDocumentElement().setAttributes(allExtraParams);
	}

	private void formXMLWithCorporatePersonInfo(YFCDocument inputXML, YFCElement address) {
		YFCElement organization = inputXML.getDocumentElement();
		if (address != null) {
			YFCElement corporatePersonInfo = organization.createChild(Constants.CORPORATE_PERSON_INFO);
			corporatePersonInfo.setAttributes(address.getAttributes());
		}
		organization.removeChild(organization.getChildElement(Constants.TEMP));
	}

	private Map<String, YFCElement> fetchAddressDetails(YFCDocument inputXML) {
		YFCElement organization = inputXML.getDocumentElement();
		YFCElement temp = organization.getChildElement(Constants.TEMP);
		if (temp == null) {
			return null;
		}
		String addressType = temp.getAttribute(Constants.ADDRESS_TYPE);
		YFCElement address = temp.getChildElement(Constants.ADDRESS);
		Map<String, YFCElement> m = new HashMap<String, YFCElement>();
		m.put(addressType, address);
		return m;
	}

	private void formXMLWithBillToPersonInfo(YFCDocument inputXML, YFCElement address) {
		YFCElement organization = inputXML.getDocumentElement();
		String isNode = organization.getAttribute(Constants.IS_NODE);
		if (isNode == null) {
			if (address != null) {
				Map<String, String> attr = address.getAttributes();
				YFCElement billingPersonInfo = organization.createChild(Constants.BILLING_PERSON_INFO);
				billingPersonInfo.setAttributes(attr);
			}
		} else {
			if (address != null) {
				Map<String, String> attr = address.getAttributes();
				YFCElement extn = organization.createChild(Constants.EXTN);
				YFCElement TerCustBillingOrgList = extn.createChild("TerCustBillingOrgList");
				YFCElement TerCustBillingOrg = TerCustBillingOrgList.createChild("TerCustBillingOrg");
				for (String key : attr.keySet()) {
					TerCustBillingOrg.setAttribute(Constants.PREFIX + key, address.getAttribute(key));
				}
			}
		}
		organization.removeChild(organization.getChildElement(Constants.TEMP));
	}

	private Document getOrganizationList(YFSEnvironment env, YFCDocument inputDoc) {
		Document output = null;
		YIFApi api;
		String template = 
				"<OrganizationList>" +
						"<Organization OrganizationCode=\"\" OrganizationName=\"\"" +
							" ParentOrganizationCode=\"\" CustomerSiteStatus=\"\">" +
							" <CorporatePersonInfo AddressID=\"\" AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\"" +
								" AddressLine4=\"\" AddressLine5=\"\" AddressLine6=\"\" AlternateEmailID=\"\" Beeper=\"\"" +
								" City=\"\" Company=\"\" Country=\"\" DayFaxNo=\"\" DayPhone=\"\" Department=\"\" EMailID=\"\"" +
								" ErrorTxt=\"\" EveningFaxNo=\"\" EveningPhone=\"\" FirstName=\"\" HttpUrl=\"\" IsCommercialAddress=\"\"" +
								" JobTitle=\"\" LastName=\"\" Latitude=\"\" Longitude=\"\" MiddleName=\"\" MobilePhone=\"\" OtherPhone=\"\"" +
								" PersonID=\"\" PersonInfoKey=\"\" PreferredShipAddress=\"\" State=\"\" Suffix=\"\" TaxGeoCode=\"\" Title=\"\"" +
								" UseCount=\"\" VerificationStatus=\"\" ZipCode=\"\"/>" +
							"<OrgRoleList>" +
								"<OrgRole/>" +
							"</OrgRoleList>" +
							"<Extn CustomerSiteStatus=\"\" />" +
						"</Organization>" +
				"</OrganizationList>";
		YFCDocument templateDoc = YFCDocument.getDocumentFor(template);
		try {
			env.setApiTemplate(Constants.API_GET_ORGANIZATION_LIST, templateDoc.getDocument());
			api = YIFClientFactory.getInstance().getApi();
			output = api.invoke(env, Constants.API_GET_ORGANIZATION_LIST, inputDoc.getDocument());
			env.clearApiTemplate(Constants.API_GET_ORGANIZATION_LIST);
			String creditHold = inputDoc.getDocumentElement().getAttribute(XMLConstants.IS_CREDIT_HOLD);
			String serviceHold = inputDoc.getDocumentElement().getAttribute(XMLConstants.IS_SERVICE_HOLD);
			if (!XmlUtils.isVoid(creditHold) || !XmlUtils.isVoid(serviceHold)) {
				filterOutputForCustomerHoldStatus(env, creditHold, serviceHold, output);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}

	private void filterOutputForCustomerHoldStatus(YFSEnvironment env, String creditHold, String serviceHold, Document output) throws Exception {
		YFCDocument outputYFCDoc = YFCDocument.getDocumentFor(output);
		YFCElement organizationList = outputYFCDoc.getDocumentElement();
		YFCIterable<YFCElement> allOrgs = organizationList.getChildren(Constants.ORGANIZATION);
		YFCDocument outputDocTemplate = YFCDocument.parse("<CustomerList>" +
															"<Customer CustomerID=\"\">" +
																"<Extn IsCreditHold=\"\" IsServiceHold=\"\" />" +
															"</Customer>" +
														  "</CustomerList>");
		for (;allOrgs.hasNext();) {
			YFCElement organization = allOrgs.next();
			String orgCode = organization.getAttribute(Constants.ORGANIZATION_CODE);
			YFCDocument inputXML = YFCDocument.parse("<Customer CustomerID=\"" + orgCode + "\" />");
			YFCDocument getCustomerListOutput = SterlingUtil.callAPI(env, Constants.API_GET_CUSTOMER_LIST, inputXML, outputDocTemplate);
			YFCElement customerList = getCustomerListOutput.getDocumentElement();
			YFCElement customer = customerList.getChildElement(Constants.CUSTOMER);
			if (XmlUtils.isVoid(customer)) {
				organizationList.removeChild(organization);
			} else {
				YFCElement extn = customer.getChildElement(Constants.EXTN);
				if (!XmlUtils.isVoid(creditHold)) {
					String isCreditHold = extn.getAttribute(XMLConstants.IS_CREDIT_HOLD);
					if (!isCreditHold.equals(creditHold)) {
						organizationList.removeChild(organization);
						continue;
					}
				}
				if (!XmlUtils.isVoid(serviceHold)) {
					String isServiceHold = extn.getAttribute(XMLConstants.IS_SERVICE_HOLD);
					if (!isServiceHold.equals(serviceHold)) {
						organizationList.removeChild(organization);
					}
				}
			}
		}
	}

	private void setRoles(Document output) {
		YFCDocument yfcOutputDoc = YFCDocument.getDocumentFor(output);
		YFCElement yfcOutputEle = yfcOutputDoc.getDocumentElement();
		YFCNodeList<YFCElement> organizationList = yfcOutputEle.getElementsByTagName(Constants.ORGANIZATION);
		for (int org = 0; org < organizationList.getLength(); org++) {
			YFCElement orgEle = organizationList.item(org);
			YFCNodeList<YFCElement> yfcOrgRolesList = orgEle.getElementsByTagName(Constants.ORG_ROLE);
			StringBuilder role = new StringBuilder();
			for (int i = 0; i < yfcOrgRolesList.getLength(); i++) {
				YFCElement element = yfcOrgRolesList.item(i);
				String roleKey = element.getAttribute(Constants.ROLE_KEY);
				role.append(roleKey.charAt(0)).append(",");
			}
			if (role.length() > 0) {
				role.deleteCharAt(role.length() - 1);
			}
			orgEle.setAttribute(Constants.ROLE_LIST, role.toString());
		}
	}

}

package com.teradyne.customer.api;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.Properties;
import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCErrorDetails;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class ManageCustomerSiteOrganization implements YIFCustomApi {

	@Override
	public void setProperties(Properties arg0) throws Exception {}
	
	public Document createCustomerSiteOrganization(YFSEnvironment env, Document inXML) throws Exception {
		YFCDocument inDocXML = YFCDocument.getDocumentFor(inXML);
		inDocXML = insertMandatoryDataToInputXML(inDocXML);
		callManageOrgAPI(env, inDocXML);
		Document outXMLDoc = getNewOrganizationDetails(env, inDocXML);
		return outXMLDoc;
	}
	
	public Document updateCustomerSiteOrganization(YFSEnvironment env, Document inXML) throws Exception {
		YFCDocument inDocXML = YFCDocument.getDocumentFor(inXML);
		validate(inDocXML);
		YFCDocument inputForUpdateOrg = seperateinXMLforUpdateOrg(inDocXML);
		if (inputForUpdateOrg != null) {
			setAuditDataForOrganization(inDocXML, inputForUpdateOrg);
			callManageOrgAPI(env, inputForUpdateOrg);
			createCustomer(env, inDocXML);
		}
		return inXML;
	}
	
	private void validate(YFCDocument inDocXML) throws Exception {
		YFCElement organization = inDocXML.getDocumentElement();
		YFCElement forCustomer = organization.getChildElement("ForCustomer");
		if (!XmlUtils.isVoid(forCustomer)) {
			String paymentTerms = forCustomer.getAttribute(XMLConstants.PAYMENT_TERMS);
			if (XmlUtils.isVoid(paymentTerms)) {
				throw new YFCException(new YFCErrorDetails("EXTN_ERR_000020", "Payment Terms is mandatory"));
			}
		}
	}

	private void setAuditDataForOrganization(YFCDocument inDocXML,
			YFCDocument inputForUpdateOrg) {
		YFCElement organization = inDocXML.getDocumentElement();
		String reasonCode = organization.getAttribute(XMLConstants.TER_REASON_CODE);
		if (!XmlUtils.isVoid(reasonCode)) {
			String reasonText = organization.getAttribute(XMLConstants.TER_REASON_TEXT);
			YFCElement extn = inputForUpdateOrg.getDocumentElement().getChildElement(XMLConstants.EXTN);
			if (XmlUtils.isVoid(extn)) {
				extn = organization.createChild(XMLConstants.EXTN);
			}
			extn.setAttribute(XMLConstants.TER_ORG_REASON_CODE, reasonCode);
			if (!XmlUtils.isVoid(reasonText)) {
				extn.setAttribute(XMLConstants.TER_ORG_REASON_TEXT, reasonText);
			}
		}
	}

	private YFCDocument insertMandatoryDataToInputXML(YFCDocument inDocXML) {
		YFCElement organization = inDocXML.getDocumentElement();
		organization.createChild(Constants.CORPORATE_PERSON_INFO);
		organization.setAttribute(Constants.CATALOG_ORGANIZATION_CODE, "CSO");
		organization.setAttribute(Constants.CREATOR_ORGANIZATION_KEY, "CSO");
		organization.setAttribute(Constants.INVENTORY_PUBLISHED, Constants.N);
		YFCElement node = organization.createChild(Constants.NODE);
		node.setAttribute(Constants.NODE_TYPE, "Customer-Site");
		node.setAttribute(Constants.INTERFACE_TYPE, "YFX");
		return inDocXML;
	}
	private void callManageOrgAPI(YFSEnvironment env, YFCDocument inDocXML)
			throws YFSException, RemoteException, YIFClientCreationException {
		YIFApi callmanageOrganizationHierarchy = YIFClientFactory.getInstance().getApi();
		callmanageOrganizationHierarchy.manageOrganizationHierarchy(env, inDocXML.getDocument());
	}
	private Document getNewOrganizationDetails(YFSEnvironment env, YFCDocument inDocXML) throws Exception {
		Document inputXML = formInputXMLForGetOrgHierarchyAPI(inDocXML);
		Document outputTemplateDoc = formOutputTemplateForGetOrgHierarchyAPI();
		env.setApiTemplate(Constants.API_GET_ORGANIZATION_HIERARCHY, outputTemplateDoc);
		YIFApi callgetOrganizationHierarchy = YIFClientFactory.getInstance().getApi();
		Document outDoc = callgetOrganizationHierarchy.getOrganizationHierarchy(env, inputXML);
		env.clearApiTemplate(Constants.API_GET_ORGANIZATION_HIERARCHY);
		return outDoc;
	}
	private Document formInputXMLForGetOrgHierarchyAPI(YFCDocument inDocXML) {
		YFCDocument doc = YFCDocument.createDocument(Constants.ORGANIZATION);
		YFCElement organization = doc.getDocumentElement();
		organization.setAttribute(Constants.ORGANIZATION_CODE, inDocXML.getDocumentElement().getAttribute(Constants.ORGANIZATION_CODE));
		return doc.getDocument();
	}
	private Document formOutputTemplateForGetOrgHierarchyAPI() {
		YFCDocument doc = YFCDocument.createDocument(Constants.ORGANIZATION);
		YFCElement organization = doc.getDocumentElement();
		organization.setAttribute(Constants.ORGANIZATION_KEY, "");
		return doc.getDocument();
	}
	private YFCDocument seperateinXMLforUpdateOrg(YFCDocument inDocXML) {
		YFCElement organization = inDocXML.getDocumentElement();
		Map<String, String> allAttrUnderOrg = organization.getAttributes();
		YFCElement extnElement = organization.getChildElement(Constants.EXTN);
		YFCDocument docForUpdateOrg = YFCDocument.createDocument(Constants.ORGANIZATION);
		Map<String, String> allExtnAttributes = null;
		if (extnElement != null) {
			allExtnAttributes = extnElement.getAttributes();
			if (allExtnAttributes.containsKey(XMLConstants.CUSTOMER_SITE_STATUS)) {
				String status = allExtnAttributes.get(XMLConstants.CUSTOMER_SITE_STATUS);
				if (XmlUtils.isVoid(status)) {
					allExtnAttributes.put(XMLConstants.CUSTOMER_SITE_STATUS, "A");
				}
			}
		} else {
			extnElement = organization.createChild(XMLConstants.EXTN);
			extnElement.setAttribute(XMLConstants.CUSTOMER_SITE_STATUS, "A");
			allExtnAttributes = extnElement.getAttributes();
		}
		YFCElement organization1 = docForUpdateOrg.getDocumentElement();
		organization1.setAttributes(allAttrUnderOrg);
		YFCElement extn = organization1.createChild(Constants.EXTN);
		extn.setAttributes(allExtnAttributes);
		return docForUpdateOrg;
	}

	private void createCustomer(YFSEnvironment env, YFCDocument inDocXML) throws Exception {
		YFCDocument inputDoc = createInputXMLForCreatingCustomer(env, inDocXML);
		callManageCustomerAPI(env, inputDoc);
	}
	private YFCDocument createInputXMLForCreatingCustomer(YFSEnvironment env, YFCDocument inDocXML) throws Exception {
		YFCElement organization = inDocXML.getDocumentElement();
		String organizationCode = organization.getAttribute(Constants.ORGANIZATION_CODE);
		YFCElement forCustomer = organization.getChildElement("ForCustomer");
		Map<String, String> allforcustomer = forCustomer.getAttributes();
		YFCDocument forAddress = getAddress(env, inDocXML);
		YFCElement organization1 = forAddress.getDocumentElement();
		YFCDocument doc = YFCDocument.createDocument(Constants.CUSTOMER);
		YFCElement customer = doc.getDocumentElement();
		customer.setAttribute(Constants.CUSTOMER_ID, organizationCode);
		customer.setAttribute(Constants.CUSTOMER_TYPE, "01");
		customer.setAttribute(Constants.ORGANIZATION_CODE, organizationCode);
		customer.setAttribute(Constants.BUYER_ORGANIZATION_CODE, organizationCode);
		YFCElement extndedAttr = customer.createChild("Extn");
		extndedAttr.setAttributes(allforcustomer);
		YFCElement customerAdditionalAddressList = customer.createChild(Constants.CUSTOMER_ADDITIONAL_ADDRESS_LIST);
		YFCElement corporatePersonInfo = organization1.getChildElement(Constants.CORPORATE_PERSON_INFO);
		if(corporatePersonInfo != null) {
			Map<String, String> corporatePersonInfoDetails = corporatePersonInfo.getAttributes();
			YFCElement shipToCustomerAdditionalAddress = customerAdditionalAddressList.createChild(Constants.CUSTOMER_ADDITIONAL_ADDRESS);
			shipToCustomerAdditionalAddress.setAttribute(Constants.IS_SHIP_TO, Constants.Y);
			//shipToCustomerAdditionalAddress.setAttribute(Constants.IS_DEFAULT_SHIP_TO, Constants.Y);
			YFCElement shipToPersonInfo = shipToCustomerAdditionalAddress.createChild(Constants.PERSON_INFO);
			shipToPersonInfo.setAttributes(corporatePersonInfoDetails);
		}
		setAuditDataForCustomer(inDocXML, customer);
		return doc;
	}
	private void setAuditDataForCustomer(YFCDocument inDocXML,
			YFCElement customer) {
		YFCElement organization = inDocXML.getDocumentElement();
		String reasonCode = organization.getAttribute(XMLConstants.TER_REASON_CODE);
		if (!XmlUtils.isVoid(reasonCode)) {
			String reasonText = organization.getAttribute(XMLConstants.TER_REASON_TEXT);
			YFCElement extn = customer.getChildElement(XMLConstants.EXTN);
			if (XmlUtils.isVoid(extn)) {
				extn = customer.createChild(XMLConstants.EXTN);
			}
			extn.setAttribute(XMLConstants.TER_CUST_REASON_CODE, reasonCode);
			if (!XmlUtils.isVoid(reasonText)) {
				extn.setAttribute(XMLConstants.TER_CUST_REASON_TEXT, reasonText);
			}
		}
	}

	private YFCDocument getAddress(YFSEnvironment env, YFCDocument inDocXML) throws Exception {
		YFCElement organization = inDocXML.getDocumentElement();
		String organizationCode = organization.getAttribute(Constants.ORGANIZATION_CODE);
		YFCDocument docForGetOrgHier = YFCDocument.createDocument(Constants.ORGANIZATION);
		YFCElement organization1 = docForGetOrgHier.getDocumentElement();
		organization1.setAttribute(Constants.ORGANIZATION_CODE,	organizationCode);
		YFCDocument templateforgetOrgHier = YFCDocument.createDocument(Constants.ORGANIZATION);
		YFCElement organization2 = templateforgetOrgHier.getDocumentElement();
		organization2.createChild(Constants.CORPORATE_PERSON_INFO);
		env.setApiTemplate(Constants.API_GET_ORGANIZATION_HIERARCHY, templateforgetOrgHier.getDocument());
		YIFApi callgetOrganizationHierarchy = YIFClientFactory.getInstance().getApi();
		Document outDoc = callgetOrganizationHierarchy.getOrganizationHierarchy(env, docForGetOrgHier.getDocument());
		env.clearApiTemplate(Constants.API_GET_ORGANIZATION_HIERARCHY);
		return YFCDocument.getDocumentFor(outDoc);
	}

	private void callManageCustomerAPI(YFSEnvironment env, YFCDocument inputDoc) throws Exception {
		inputDoc.getDocumentElement().setAttribute(Constants.ORGANIZATION_CODE, "CSO");
		YIFApi callmanageCustomer = YIFClientFactory.getInstance().getApi();
		callmanageCustomer.manageCustomer(env, inputDoc.getDocument());
	}

}

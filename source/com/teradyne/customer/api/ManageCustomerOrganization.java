package com.teradyne.customer.api;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCErrorDetails;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class ManageCustomerOrganization implements YIFCustomApi {
	
	private static YIFApi api;
	
	static {
		try {
			api = YIFClientFactory.getInstance().getApi();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setProperties(Properties p) throws Exception {}
	
	public Document createCustomerOrganization(YFSEnvironment env, Document inXML) throws Exception {
		YFCDocument inDocXML = YFCDocument.getDocumentFor(inXML);
		inDocXML = insertMandatoryDataToInputXML(inDocXML);
		callManageOrgAPI(env, inDocXML);
		Document outXMLDoc = getNewOrganizationDetails(env, inDocXML);
		return outXMLDoc;
	}
	
	public Document updateCustomerOrganization(YFSEnvironment env, Document inXML) throws Exception {
		YFCDocument inDocXML = YFCDocument.getDocumentFor(inXML);
		validateAddresses(inDocXML);
		setAuditDataForOrganization(inDocXML);
		callManageOrgAPI(env, inDocXML);
		createCustomer(env, inDocXML);
		return inXML;
	}
	
	private void setAuditDataForOrganization(YFCDocument inDocXML) {
		YFCElement organization = inDocXML.getDocumentElement();
		String reasonCode = organization.getAttribute(XMLConstants.TER_REASON_CODE);
		if (!XmlUtils.isVoid(reasonCode)) {
			String reasonText = organization.getAttribute(XMLConstants.TER_REASON_TEXT);
			YFCElement extn = organization.getChildElement(XMLConstants.EXTN);
			if (XmlUtils.isVoid(extn)) {
				extn = organization.createChild(XMLConstants.EXTN);
			}
			extn.setAttribute(XMLConstants.TER_ORG_REASON_CODE, reasonCode);
			if (!XmlUtils.isVoid(reasonText)) {
				extn.setAttribute(XMLConstants.TER_ORG_REASON_TEXT, reasonText);
			}
		}
	}

	private void createCustomer(YFSEnvironment env, YFCDocument inDocXML) throws Exception {
		YFCDocument inputDoc = createInputXMLForCreatingCustomer(inDocXML);
		callManageCustomerAPI(env, inputDoc);
	}

	private void callManageCustomerAPI(YFSEnvironment env, YFCDocument inputDoc) throws Exception {
		api.invoke(env, Constants.API_MANAGE_CUSTOMER, inputDoc.getDocument());
	}

	private YFCDocument createInputXMLForCreatingCustomer(YFCDocument inDocXML) {
		YFCElement organization = inDocXML.getDocumentElement();
		String organizationCode = organization.getAttribute(Constants.ORGANIZATION_CODE);
		YFCElement corporatePersonInfo = organization.getChildElement(Constants.CORPORATE_PERSON_INFO);
		Map<String, String> corporatePersonInfoDetails = corporatePersonInfo.getAttributes();
		YFCElement billingPersonInfo = organization.getChildElement(Constants.BILLING_PERSON_INFO);
		Map<String, String> billingPersonInfoDetails = billingPersonInfo.getAttributes();
		YFCDocument doc = YFCDocument.createDocument(Constants.CUSTOMER);
		YFCElement customer = doc.getDocumentElement();
		customer.setAttribute(Constants.CUSTOMER_ID, organizationCode);
		customer.setAttribute(Constants.CUSTOMER_TYPE, "01");
		customer.setAttribute(Constants.OPERATION, "Manage");
		customer.setAttribute(Constants.ORGANIZATION_CODE, "CSO");
		customer.setAttribute(Constants.BUYER_ORGANIZATION_CODE, organizationCode);
		YFCElement customerAdditionalAddressList = customer.createChild(Constants.CUSTOMER_ADDITIONAL_ADDRESS_LIST);
		YFCElement billToCustomerAdditionalAddress = customerAdditionalAddressList.createChild(Constants.CUSTOMER_ADDITIONAL_ADDRESS);
		billToCustomerAdditionalAddress.setAttribute(Constants.IS_BILL_TO, Constants.Y);
		//billToCustomerAdditionalAddress.setAttribute(Constants.IS_DEFAULT_BILL_TO, Constants.Y);
		YFCElement billToPersonInfo = billToCustomerAdditionalAddress.createChild(Constants.PERSON_INFO);
		billToPersonInfo.setAttributes(billingPersonInfoDetails);
		YFCElement shipToCustomerAdditionalAddress = customerAdditionalAddressList.createChild(Constants.CUSTOMER_ADDITIONAL_ADDRESS);
		shipToCustomerAdditionalAddress.setAttribute(Constants.IS_SHIP_TO, Constants.Y);
		//shipToCustomerAdditionalAddress.setAttribute(Constants.IS_DEFAULT_SHIP_TO, Constants.Y);
		YFCElement shipToPersonInfo = shipToCustomerAdditionalAddress.createChild(Constants.PERSON_INFO);
		shipToPersonInfo.setAttributes(corporatePersonInfoDetails);
		setAuditDataForCustomer(inDocXML, customer);
		return doc;
	}

	private void setAuditDataForCustomer(YFCDocument inDocXML, YFCElement customer) {
		YFCElement organization = inDocXML.getDocumentElement();
		String reasonCode = organization.getAttribute(XMLConstants.TER_REASON_CODE);
		if (!XmlUtils.isVoid(reasonCode)) {
			String reasonText = organization.getAttribute(XMLConstants.TER_REASON_TEXT);
			YFCElement extn = customer.getChildElement(XMLConstants.EXTN);
			if (XmlUtils.isVoid(extn)) {
				extn = customer.createChild(XMLConstants.EXTN);
			}
			extn.setAttribute(XMLConstants.TER_CUST_REASON_CODE, reasonCode);
			if (!XmlUtils.isVoid(reasonText)) {
				extn.setAttribute(XMLConstants.TER_CUST_REASON_TEXT, reasonText);
			}
		}
	}

	private void validateAddresses(YFCDocument inDocXML) {
		YFCElement organization = inDocXML.getDocumentElement();
		YFCElement billingPersonInfo = organization.getChildElement(Constants.BILLING_PERSON_INFO);
		if (billingPersonInfo == null) {
			throw new YFCException(new YFCErrorDetails("EXTN_ERR_000010", "Bill To address is mandatory"));
		} else {
			YFCElement corporatePersonInfo = organization.getChildElement(Constants.CORPORATE_PERSON_INFO);
			if (corporatePersonInfo == null) {
				corporatePersonInfo = organization.createChild(Constants.CORPORATE_PERSON_INFO);
				Map<String, String> billToAttributes = billingPersonInfo.getAttributes();
				corporatePersonInfo.setAttributes(billToAttributes);
			}
		}
	}

	private Document getNewOrganizationDetails(YFSEnvironment env, YFCDocument inDocXML) throws YFSException, RemoteException {
		Document inputXML = formInputXMLForGetOrgHierarchyAPI(inDocXML);
		Document outputTemplateDoc = formOutputTemplateForGetOrgHierarchyAPI();
		env.setApiTemplate(Constants.API_GET_ORGANIZATION_HIERARCHY, outputTemplateDoc);
		Document outDoc = api.invoke(env, Constants.API_GET_ORGANIZATION_HIERARCHY, inputXML);
		env.clearApiTemplate(Constants.API_GET_ORGANIZATION_HIERARCHY);
		return outDoc;
	}

	private Document formInputXMLForGetOrgHierarchyAPI(YFCDocument inDocXML) {
		YFCDocument doc = YFCDocument.createDocument(Constants.ORGANIZATION);
		YFCElement organization = doc.getDocumentElement();
		organization.setAttribute(Constants.ORGANIZATION_CODE,
				inDocXML.getDocumentElement().getAttribute(Constants.ORGANIZATION_CODE));
		return doc.getDocument();
	}

	private Document formOutputTemplateForGetOrgHierarchyAPI() {
		YFCDocument doc = YFCDocument.createDocument(Constants.ORGANIZATION);
		YFCElement organization = doc.getDocumentElement();
		organization.setAttribute(Constants.ORGANIZATION_KEY, "");
		return doc.getDocument();
	}

	private void callManageOrgAPI(YFSEnvironment env, YFCDocument inDocXML) throws YFSException, RemoteException {
		api.invoke(env, Constants.API_MANAGE_ORGANIZATION_HIERARCHY, inDocXML.getDocument());
	}

	private YFCDocument insertMandatoryDataToInputXML(YFCDocument inDocXML) {
		YFCElement organization = inDocXML.getDocumentElement();
		organization.setAttribute(Constants.OPERATION, Constants.OPERATION_CREATE);
		organization.setAttribute(Constants.CATALOG_ORGANIZATION_CODE, "CSO");
		organization.setAttribute(Constants.CREATOR_ORGANIZATION_KEY, "CSO");
		organization.setAttribute(Constants.INVENTORY_PUBLISHED, Constants.N);
		organization.setAttribute(Constants.PRIMARY_ENTERPRISE_KEY, "CSO");
		organization.createChild(Constants.CORPORATE_PERSON_INFO);
		YFCElement orgRoleList = organization.createChild(Constants.ORG_ROLE_LIST);
		YFCElement orgRole = orgRoleList.createChild(Constants.ORG_ROLE);
		orgRole.setAttribute(Constants.ROLE_KEY, Constants.BUYER);
		return inDocXML;
	}

}

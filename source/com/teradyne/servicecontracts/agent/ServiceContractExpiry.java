package com.teradyne.servicecontracts.agent;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.XPathUtil;

import com.teradyne.servicecontracts.api.ServiceContractUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.ycp.japi.util.YCPBaseTaskAgent;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;


public class ServiceContractExpiry extends YCPBaseTaskAgent {
	
	Properties props = null;
	int iNextTaskQInterval;
	Date expirationDate;

	public void setProperties(Properties props) throws Exception {
		this.props = props;
	}
	
	@Override
	public Document executeTask(YFSEnvironment env, Document doc) throws Exception {
		
		YFCDocument inDoc = YFCDocument.getDocumentFor(doc);
		System.out.println("InDoc::"+inDoc.getString());
		YFCElement taskEle = inDoc.getDocumentElement();

		String dataKeyStr = taskEle.getAttribute(XMLConstants.DATA_KEY);
		YFCElement eleFilters = taskEle.getChildElement(XMLConstants.TRANSACTION_FILTERS);
		iNextTaskQInterval = eleFilters.getIntAttribute(XMLConstants.NEXT_TASK_Q_INTERVAL);
		
		Boolean isTaskFailed = true;
		String orderTemplateStr = "<Order OrderHeaderKey=\"\" ><Extn ExpirationDate=\"\" /></Order>";
		YFCDocument orderInDoc =  YFCDocument.createDocument(XMLConstants.ORDER);
		orderInDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, dataKeyStr);
		YFCDocument orderOutDoc = SterlingUtil.callAPI(env,Constants.GET_ORDER_LIST, orderInDoc, YFCDocument.parse(orderTemplateStr));
		YFCElement orderOutEle = orderOutDoc.getElementsByTagName(XMLConstants.ORDER).item(0);
		String expirationDateStr = XPathUtil.getXpathAttribute(orderOutEle, "/Order/Extn/@ExpirationDate");
		GregorianCalendar c = new GregorianCalendar();
		Date sysDate = c.getTime();
		expirationDate = DateTimeUtil.convertDate(expirationDateStr);
		if(sysDate.after(expirationDate)){
			ServiceContractUtil scu = new ServiceContractUtil();
			YFCDocument out = scu.changeOrderStatus(env, dataKeyStr, "1100.500", Constants.SC_EXPIRED);
			System.out.println("ChangeOrderStatus Doc::"+out.getString());
			isTaskFailed = false;
		}
		
		registerProcessCompletion(env, inDoc.getDocument(), isTaskFailed);
		return doc;
	}
	
	

	private void registerProcessCompletion(final YFSEnvironment env, final Document inXML, boolean processFailed) throws RemoteException, YIFClientCreationException {
		
		YFCDocument inRegisterXML = YFCDocument.createDocument(XMLConstants.REGISTER_PROCESS_COMPLETION_INPUT);
		YFCElement registerElem = inRegisterXML.getDocumentElement();
		if (processFailed){
			registerElem.setAttribute(XMLConstants.KEEP_TASK_OPEN, Constants.YES);
		}else{
			registerElem.setAttribute(XMLConstants.KEEP_TASK_OPEN, Constants.NO);
		}
			
		YFCElement currentElem = inRegisterXML.createElement(XMLConstants.CURRENT_TASK);
		Date nextAvailDate = new Date();
		nextAvailDate.setHours(expirationDate.getHours() + iNextTaskQInterval);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		currentElem.setAttribute(XMLConstants.AVAILABLE_DATE, dateFormat.format(nextAvailDate));
		currentElem.setAttribute(XMLConstants.DATA_KEY, inXML.getDocumentElement().getAttribute(XMLConstants.DATA_KEY));
		currentElem.setAttribute(XMLConstants.TASK_Q_KEY, inXML.getDocumentElement().getAttribute(XMLConstants.TASK_Q_KEY));
		currentElem.setAttribute(XMLConstants.TRANSACTION_KEY,inXML.getDocumentElement().getAttribute(XMLConstants.TRANSACTION_KEY));
		registerElem.appendChild(currentElem);
		System.out.println("RPC Doc::"+inRegisterXML.getString());
		SterlingUtil.callAPI(env, Constants.REGISTER_PROCESS_COMPLETION, inRegisterXML, "");

		
	}

	
}

package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.rmi.RemoteException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;

public class ActivateExpireSC {

	public Document expireSC(YFSEnvironment env, Document inputDoc)
			throws RemoteException, YIFClientCreationException, SAXException,
			IOException {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		YFCElement orderEle = inDoc.getDocumentElement().getChildElement(
				XMLConstants.ORDER);
		String orderHeaderKeyStr = orderEle
				.getAttribute(XMLConstants.ORDER_HEADER_KEY);

		String monitorEventStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.MONITOR_CONSOLIDATION_ID);
		ServiceContractUtil scUtil = new ServiceContractUtil();
		YFCDocument outDoc = null;
		if (monitorEventStr.equals(Constants.EXPIRE)) {
			outDoc = scUtil.changeOrderStatus(env, orderHeaderKeyStr,
					Constants.EXPIRED, Constants.SC_EXPIRED);
		}
		return outDoc.getDocument();
	}

	public Document activateLine(YFSEnvironment env, Document inputDoc)
			throws RemoteException, YIFClientCreationException, SAXException,
			IOException {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);

		YFCElement orderEle = inDoc.getDocumentElement().getChildElement(
				XMLConstants.ORDER);
		YFCElement orderStatusEle = orderEle.getChildElement(
				XMLConstants.ORDER_STATUSES).getChildElement(
				XMLConstants.ORDER_STATUS);
		String olKey = orderStatusEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
		String orderHeaderKeyStr = orderStatusEle
				.getAttribute(XMLConstants.ORDER_HEADER_KEY);

		String monitorEventStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.MONITOR_CONSOLIDATION_ID);
		if (monitorEventStr.equals(Constants.ACTIVATE)) {
			YFCDocument changeStatusDoc = YFCDocument
					.createDocument(XMLConstants.ORDER_STATUS_CHANGE);
			changeStatusDoc.getDocumentElement().setAttribute(
					XMLConstants.ORDER_HEADER_KEY, orderHeaderKeyStr);
			changeStatusDoc.getDocumentElement().setAttribute(
					XMLConstants.TRANSACTION_ID, Constants.POST_CONFIRM);
			YFCElement inOrderLinesEle = changeStatusDoc.getDocumentElement()
					.createChild(XMLConstants.ORDER_LINES);
			YFCElement inOrderLineEle = inOrderLinesEle
					.createChild(XMLConstants.ORDER_LINE);
			inOrderLineEle.setAttribute(XMLConstants.ORDER_LINE_KEY, olKey);
			inOrderLineEle.setAttribute(
					XMLConstants.CHANGE_FOR_ALL_AVAILABLE_QTY, Constants.YES);
			inOrderLineEle.setAttribute(XMLConstants.BASE_DROP_STATUS,
					Constants.ACTIVE);
			String templateStr = "<OrderStatusChange OrderHeaderKey=\"\" />";

			YFCDocument outDoc = SterlingUtil.callAPI(env,
					Constants.CHANGE_ORDER_STATUS, changeStatusDoc,
					YFCDocument.parse(templateStr));
			return outDoc.getDocument();
		}
		return inputDoc;
	}

}

package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class ServiceContractUtil {

	public YFCDocument changeOrderStatus(YFSEnvironment env,
			String orderHeaderKey, String baseDropStatus, String transactionID)
			throws RemoteException, YIFClientCreationException, SAXException,
			IOException {
		YFCDocument inDoc = YFCDocument
				.createDocument(XMLConstants.ORDER_STATUS_CHANGE);
		inDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY,
				orderHeaderKey);
		inDoc.getDocumentElement().setAttribute(XMLConstants.BASE_DROP_STATUS,
				baseDropStatus);
		inDoc.getDocumentElement().setAttribute(
				XMLConstants.CHANGE_FOR_ALL_AVAILABLE_QTY, Constants.YES);
		inDoc.getDocumentElement().setAttribute(XMLConstants.TRANSACTION_ID,
				transactionID);
		String templateStr = "<OrderStatusChange OrderHeaderKey=\"\" />";
		YFCDocument outDoc = SterlingUtil.callAPI(env,
				Constants.CHANGE_ORDER_STATUS, inDoc,
				YFCDocument.parse(templateStr));
		return outDoc;
	}

	private YFCDocument getIBMapping(YFSEnvironment env, YFCDocument doc,
			String level) throws YIFClientCreationException, YFSException,
			RemoteException {

		YFCDocument inDoc = YFCDocument
				.createDocument(XMLConstants.TER_SCIB_MAPPING);
		if (level.equals(XMLConstants.ORDER)) {
			String dataKey = doc.getDocumentElement().getAttribute(
					XMLConstants.ORDER_HEADER_KEY);
			inDoc.getDocumentElement().setAttribute(XMLConstants.TER_SCOH_KEY,
					dataKey);
		} else {
			String dataKey = doc.getDocumentElement().getAttribute(
					XMLConstants.ORDER_LINE_KEY);
			inDoc.getDocumentElement().setAttribute(XMLConstants.TER_SCOL_KEY,
					dataKey);
		}

		YIFApi api = YIFClientFactory.getInstance().getApi();
		Document outDoc = api.executeFlow(env, Constants.GET_IB_FOR_LINE,
				inDoc.getDocument());
		return YFCDocument.getDocumentFor(outDoc);

	}

	public void multiApiForSCIBMapping(YFSEnvironment env, YFCDocument inDoc,
			String operation, String level) throws YFSException,
			RemoteException, YIFClientCreationException {

		YFCDocument mappingDoc = getIBMapping(env, inDoc, level);

		YFCDocument multiInDoc = YFCDocument
				.createDocument(XMLConstants.MULTI_API_ELE);
		YFCNodeList<YFCElement> mappingListEle = mappingDoc
				.getDocumentElement().getElementsByTagName(
						XMLConstants.TER_SCIB_MAPPING);

		for (Iterator<YFCElement> iter = mappingListEle.iterator(); iter
				.hasNext();) {
			YFCElement mappingEle = iter.next();

			YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
			apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
			if (operation.equals("Delete")) {
				apiEle.setAttribute(XMLConstants.NAME,
						Constants.DELETE_TER_SCIB_MAPPING);
			}

			YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);

			YFCElement newMappingEle = multiInDoc
					.createElement(XMLConstants.TER_SCIB_MAPPING);
			newMappingEle = multiInDoc.importNode(mappingEle, false);
			inputEle.appendChild(newMappingEle);

			multiInDoc.getDocumentElement().appendChild(apiEle);

		}

		// System.out.println("Input to MultiApi"+multiInDoc.getString());
		SterlingUtil.callAPI(env, Constants.MULTI_API, multiInDoc, "");
	}

	public void setPriceAttributes(YFSEnvironment env, YFCDocument outDoc)
			throws RemoteException, YIFClientCreationException, SAXException,
			IOException {
		String templateStr = "<Order OrderHeaderKey=\"\" />";
		String ohKey = outDoc.getDocumentElement().getAttribute(
				XMLConstants.ORDER_HEADER_KEY);
		String grandTotalStr = outDoc.getDocumentElement()
				.getChildElement(XMLConstants.OVERALL_TOTALS)
				.getAttribute(XMLConstants.GRAND_TOTAL);
		String lineSubTotalStr = outDoc.getDocumentElement()
				.getChildElement(XMLConstants.OVERALL_TOTALS)
				.getAttribute(XMLConstants.LINE_SUB_TOTAL);

		YFCDocument orderDoc = YFCDocument.createDocument(XMLConstants.ORDER);
		orderDoc.getDocumentElement().setAttribute(
				XMLConstants.ORDER_HEADER_KEY, ohKey);
		YFCElement extnEle = orderDoc.getDocumentElement().createChild(
				XMLConstants.EXTN);
		extnEle.setAttribute(XMLConstants.TOTAL_SELLING_PRICE, grandTotalStr);
		if (Double.parseDouble(lineSubTotalStr) == 0) {
			extnEle.setAttribute(XMLConstants.AGREEMENT_DISCOUNT, "0");
		} else {

			double discount = ((Double.parseDouble(lineSubTotalStr) - Double
					.parseDouble(grandTotalStr)) * 100)
					/ Double.parseDouble(lineSubTotalStr);
			String disStr = new BigDecimal(discount).setScale(2,
					BigDecimal.ROUND_HALF_UP).toString();
			extnEle.setAttribute(XMLConstants.AGREEMENT_DISCOUNT, disStr);
		}
		outDoc = SterlingUtil.callAPI(env, Constants.CHANGE_ORDER_API,
				orderDoc, YFCDocument.parse(templateStr));
	}

	public YFCDocument callGetOrderList(YFSEnvironment env, String ohKey,
			String templateStr) throws RemoteException,
			YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		YFCDocument inDoc = YFCDocument.createDocument(XMLConstants.ORDER);
		inDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY,
				ohKey);
		YFCDocument outDoc = SterlingUtil
				.callAPI(env, Constants.GET_ORDER_LIST, inDoc,
						YFCDocument.parse(templateStr));
		return outDoc;
	}

	public void changeStatusOfLines(YFSEnvironment env, YFCDocument outDoc)
			throws RemoteException, YIFClientCreationException, SAXException,
			IOException {
		String ohKey = outDoc.getDocumentElement().getAttribute(
				XMLConstants.ORDER_HEADER_KEY);
		YFCDocument inDoc = YFCDocument
				.createDocument(XMLConstants.ORDER_STATUS_CHANGE);
		inDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY,
				ohKey);
		inDoc.getDocumentElement().setAttribute(XMLConstants.TRANSACTION_ID,
				Constants.POST_CONFIRM);
		YFCElement inOrderLinesEle = inDoc.getDocumentElement().createChild(
				XMLConstants.ORDER_LINES);
		YFCNodeList<YFCElement> orderLinesEle = outDoc.getDocumentElement()
				.getElementsByTagName(XMLConstants.ORDER_LINE);
		for (Iterator<YFCElement> iter = orderLinesEle.iterator(); iter
				.hasNext();) {
			YFCElement orderLineEle = iter.next();
			String olKey = orderLineEle
					.getAttribute(XMLConstants.ORDER_LINE_KEY);
			String status = orderLineEle.getAttribute(XMLConstants.STATUS);
			GregorianCalendar c = new GregorianCalendar();
			Date sysDate = c.getTime();

			YFCElement extnEle = orderLineEle
					.getChildElement(XMLConstants.EXTN);
			String serviceStartDateStr = extnEle
					.getAttribute(XMLConstants.SERVICE_START_DATE_TIME);
			Date serviceStartDate = DateTimeUtil
					.convertDate(serviceStartDateStr);

			if (status.equals(Constants.STATUS_DESC_CREATED)
					|| status.equals(Constants.STATUS_DESC_DRAFT_CREATED)) {
				YFCElement inOrderLineEle = inOrderLinesEle
						.createChild(XMLConstants.ORDER_LINE);
				inOrderLineEle.setAttribute(XMLConstants.ORDER_LINE_KEY, olKey);
				inOrderLineEle.setAttribute(
						XMLConstants.CHANGE_FOR_ALL_AVAILABLE_QTY,
						Constants.YES);

				if (serviceStartDate.getTime() > sysDate.getTime()) {
					inOrderLineEle.setAttribute(XMLConstants.BASE_DROP_STATUS,
							Constants.CREATED_AWAITING_ACTIVATION);
				} else {
					inOrderLineEle.setAttribute(XMLConstants.BASE_DROP_STATUS,
							Constants.ACTIVE);
				}
			}
			if (status.equals("Created Awaiting Activation")) {
				if (serviceStartDate.getTime() <= sysDate.getTime()) {
					YFCElement inOrderLineEle = inOrderLinesEle
							.createChild(XMLConstants.ORDER_LINE);
					inOrderLineEle.setAttribute(XMLConstants.ORDER_LINE_KEY,
							olKey);
					inOrderLineEle.setAttribute(
							XMLConstants.CHANGE_FOR_ALL_AVAILABLE_QTY,
							Constants.YES);
					inOrderLineEle.setAttribute(XMLConstants.BASE_DROP_STATUS,
							Constants.ACTIVE);
				}
			}
		}// end-for-loop

		String templateStr = "<OrderStatusChange OrderHeaderKey=\"\" />";
		// System.out.println("Input to changeOrderStatus ::"+inDoc.getString());
		if (inDoc.getDocumentElement()
				.getElementsByTagName(XMLConstants.ORDER_LINE).getLength() > 0) {
			YFCDocument doc = SterlingUtil.callAPI(env,
					Constants.CHANGE_ORDER_STATUS, inDoc,
					YFCDocument.parse(templateStr));
		}
		return;

	}
}

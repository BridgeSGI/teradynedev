package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;

public class RemoveServiceItems {

	public Document removeServiceItems(YFSEnvironment env, Document inDoc)
			throws YIFClientCreationException, SAXException, IOException {
		YFCElement rootEle = YFCDocument.getDocumentFor(inDoc)
				.getDocumentElement();
		String kitCodeStr = rootEle.getAttribute(XMLConstants.KIT_CODE);
		if (!StringUtil.isEmpty(kitCodeStr) && kitCodeStr.equals("BUNDLE")) {
			removeBundleComponents(env, rootEle);
		} else {
			YIFApi api = YIFClientFactory.getInstance().getApi();
			api.executeFlow(env, Constants.DEL_SER_ITEM_SERVICE, inDoc);
		}
		return inDoc;
	}

	public void removeBundleComponents(YFSEnvironment env, YFCElement rootEle)
			throws RemoteException, YIFClientCreationException {
		String siKeyStr = rootEle.getAttribute(XMLConstants.TER_SI_KEY);
		String siItemKeyStr = rootEle
				.getAttribute(XMLConstants.TER_SI_ITEM_KEY);
		String sctKeyStr = rootEle.getAttribute(XMLConstants.TER_SCT_KEY);

		YFCDocument getServiceItemsDoc = YFCDocument
				.createDocument(XMLConstants.TER_SERVICE_ITEM);
		getServiceItemsDoc.getDocumentElement().setAttribute(
				XMLConstants.TER_SCT_KEY, sctKeyStr);
		getServiceItemsDoc.getDocumentElement().setAttribute(
				XMLConstants.TER_SI_PARENT_KEY, siItemKeyStr);
		YFCDocument getSerItemsOut = SterlingUtil.callService(env,
				Constants.GET_SERVICE_ITEM_LIST, getServiceItemsDoc, "");

		YFCNodeList<YFCElement> serItemsNL = getSerItemsOut
				.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);

		YFCDocument multiInDoc = YFCDocument
				.createDocument(XMLConstants.MULTI_API_ELE);
		YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
		apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
		apiEle.setAttribute(XMLConstants.NAME, Constants.DEL_SER_ITEM_API);

		YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
		YFCElement serItemInEle = inputEle
				.createChild(XMLConstants.TER_SERVICE_ITEM);
		serItemInEle.setAttribute(XMLConstants.TER_SI_KEY, siKeyStr);
		multiInDoc.getDocumentElement().appendChild(apiEle);

		for (Iterator<YFCElement> iter = serItemsNL.iterator(); iter.hasNext();) {
			YFCElement serItemEle = iter.next();

			apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
			apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
			apiEle.setAttribute(XMLConstants.NAME, Constants.DEL_SER_ITEM_API);

			inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
			serItemInEle = inputEle.createChild(XMLConstants.TER_SERVICE_ITEM);
			serItemInEle.setAttribute(XMLConstants.TER_SI_KEY,
					serItemEle.getAttribute(XMLConstants.TER_SI_KEY));

			multiInDoc.getDocumentElement().appendChild(apiEle);

		}
		// System.out.println("MultiApi Input::"+multiInDoc.getString());
		SterlingUtil.callAPI(env, Constants.MULTI_API, multiInDoc, "");

	}
}

package com.teradyne.servicecontracts.api;

import java.io.IOException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class CancelServiceContract {

	public Document cancelLine(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);

		YFCElement orderLineEle = inDoc.getDocumentElement()
				.getElementsByTagName(XMLConstants.ORDER_LINE).item(0);
		String orderLineKey = orderLineEle
				.getAttribute(XMLConstants.ORDER_LINE_KEY);
		System.out.println("ChangeOrder In::" + inDoc.getString());
		String changeOrderStr = "<Order OrderHeaderKey=\"\" ><OverallTotals LineSubTotal=\"\" GrandTotal=\"\" /></Order>";
		YFCDocument changeOrderOut = SterlingUtil.callAPI(env, "changeOrder",
				inDoc, YFCDocument.parse(changeOrderStr));
		System.out.println("ChangeOrder Out::" + changeOrderOut.getString());
		inDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_LINE_KEY,
				orderLineKey);
		ServiceContractUtil scUtil = new ServiceContractUtil();
		scUtil.multiApiForSCIBMapping(env, inDoc, "Delete", "OrderLine");

		scUtil.setPriceAttributes(env, changeOrderOut);

		return changeOrderOut.getDocument();

	}

	public Document cancelOrder(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String orderHeaderKey = inDoc.getDocumentElement().getAttribute(
				XMLConstants.ORDER_HEADER_KEY);
		// need to make extended price attributes 0.
		YFCElement extnEle = inDoc.getDocumentElement().createChild(
				XMLConstants.EXTN);
		extnEle.setAttribute("TotalSellingPrice", Constants.ZERO);
		extnEle.setAttribute("AgreementDiscount", Constants.ZERO);
		System.out.println("ChangeOrder Out::" + inDoc.getString());
		String changeOrderStr = "<Order OrderHeaderKey=\"\" />";
		YFCDocument changeOrderOut = SterlingUtil.callAPI(env, "changeOrder",
				inDoc, YFCDocument.parse(changeOrderStr));

		ServiceContractUtil scUtil = new ServiceContractUtil();
		scUtil.multiApiForSCIBMapping(env, inDoc, "Delete", "Order");

		return changeOrderOut.getDocument();

	}

}

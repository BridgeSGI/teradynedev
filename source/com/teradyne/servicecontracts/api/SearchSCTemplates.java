package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.teradyne.utils.*;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class SearchSCTemplates {

	private static final String	CALL_FOR_LOOKUP	= "CallForLookup";

	public Document searchSCTemplates(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String callForLookup = inDoc.getDocumentElement().getAttribute(
				SearchSCTemplates.CALL_FOR_LOOKUP);
		YFCDocument getSCTemplateVwListDoc = SterlingUtil.callService(env,
				Constants.GET_TER_SCT_VW_LIST,
				YFCDocument.getDocumentFor(inputDoc), "");

		YFCDocument getSCTListOut = itemIDIsEmpty(env, inDoc);

		if (getSCTListOut != null) {
			for (Iterator<YFCElement> iter = getSCTListOut
					.getElementsByTagName(XMLConstants.TER_SER_CONT_TEMP_VW)
					.iterator(); iter.hasNext();) {
				YFCElement getSCTOutEle = getSCTemplateVwListDoc
						.createElement(XMLConstants.TER_SER_CONT_TEMP_VW);
				getSCTOutEle = getSCTemplateVwListDoc.importNode(iter.next(),
						true);
				getSCTemplateVwListDoc.getDocumentElement().appendChild(
						getSCTOutEle);
			}
		}

		YFCDocument outDoc = removeDuplicates(getSCTemplateVwListDoc);
		if (!StringUtil.isEmpty(callForLookup)) {
			outDoc.getDocumentElement().setAttribute(
					SearchSCTemplates.CALL_FOR_LOOKUP, callForLookup);
		}
		return outDoc.getDocument();

	}

	private YFCDocument removeDuplicates(YFCDocument getSCTemplateVwListDoc) {

		YFCNodeList<YFCElement> scTemplateNL = getSCTemplateVwListDoc
				.getElementsByTagName(XMLConstants.TER_SER_CONT_TEMP_VW);

		Set<String> terSCTKeySet = new HashSet<String>();
		YFCDocument outDoc = YFCDocument
				.createDocument(XMLConstants.TER_SER_CONT_TEMP_VW_LIST);

		for (Iterator<YFCElement> i = scTemplateNL.iterator(); i.hasNext();) {
			YFCElement scTemplateEle = i.next();
			String terSCTKeyStr = scTemplateEle
					.getAttribute(XMLConstants.TER_SCT_KEY);
			if (!terSCTKeySet.contains(terSCTKeyStr)) {
				terSCTKeySet.add(terSCTKeyStr);
				YFCElement outSCTemplateEle = outDoc
						.createElement(XMLConstants.TER_SER_CONT_TEMP_VW);
				outSCTemplateEle = outDoc.importNode(scTemplateEle, true);
				outDoc.getDocumentElement().appendChild(outSCTemplateEle);
			}
		}
		return outDoc;
	}

	private YFCDocument itemIDIsEmpty(YFSEnvironment env, YFCDocument inDoc)
			throws RemoteException, YIFClientCreationException {
		String itemID = inDoc.getDocumentElement().getAttribute(
				XMLConstants.ITEM_ID);
		if (!StringUtil.isEmpty(itemID)) {
			return null;
		}

		YFCDocument getSCTListIn = YFCDocument
				.createDocument(XMLConstants.TER_SC_TEMPLATE);
		getSCTListIn.getDocumentElement().setAttributes(
				inDoc.getDocumentElement().getAttributes());
		YFCDocument getSCTListOut = SterlingUtil.callService(env,
				Constants.GET_SC_TEMPLATE_LIST, getSCTListIn, "");
		YFCDocument outDoc = YFCDocument
				.createDocument(XMLConstants.TER_SER_CONT_TEMP_VW_LIST);
		for (Iterator<YFCElement> iter = getSCTListOut.getElementsByTagName(
				XMLConstants.TER_SC_TEMPLATE).iterator(); iter.hasNext();) {
			YFCElement sctEle = iter.next();
			YFCElement outEle = outDoc
					.createElement(XMLConstants.TER_SER_CONT_TEMP_VW);
			outEle.setAttributes(sctEle.getAttributes());
			outDoc.getDocumentElement().appendChild(outEle);
		}

		return outDoc;
	}

}

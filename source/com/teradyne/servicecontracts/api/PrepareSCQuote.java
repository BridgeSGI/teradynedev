package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.teradyne.om.api.PrepareEmailTemplate;
import com.teradyne.om.util.CreatePDF;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;


public class PrepareSCQuote implements YIFCustomApi{ 

	
	private Properties properties = null;
	
	public void setProperties(Properties prop) throws Exception {
	    properties = prop;
	  }  

	
	public Document prepareQuote(YFSEnvironment env, Document inDoc) throws Exception {
		
	    ArrayList<String> fileList = new ArrayList<String>();
	    Document output = getOrderDetailsForReport(env, inDoc);
	    String currentStatus = output.getDocumentElement().getAttribute(XMLConstants.STATUS);
	    String orderHeaderKey = output.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY);
	    if(!StringUtil.isEmpty(currentStatus) && currentStatus.equals("Draft Order Created")){
	    	ServiceContractUtil scutil = new ServiceContractUtil();
	    	scutil.changeOrderStatus(env, orderHeaderKey, "1000.100", "CS_AWT_CUST_ACCEPTANCE.0018.ex.ex");
	    }
	    
	    String firstPage = "global/template/jasper/SC.jrxml";
	    String secondPage = "global/template/jasper/SC1.jrxml";
	    
	    CreatePDF pdfUtil = new CreatePDF();
	    pdfUtil.setProperties(properties);
	    pdfUtil.exportPdf(output, firstPage, false, 0, fileList, null);
	    pdfUtil.exportPdf(output, secondPage, false, 1, fileList, null);


	    String filePath = pdfUtil.doMerge(output, fileList);
	    YFCDocument changeOrderDoc =  changeOrderToSaveFilePath(env, orderHeaderKey, filePath);
	    
	    return changeOrderDoc.getDocument();
	  }

	private YFCDocument changeOrderToSaveFilePath(YFSEnvironment env,
			String orderHeaderKey, String filePath) throws RemoteException, YIFClientCreationException, SAXException, IOException {
		
		YFCDocument inDoc = YFCDocument.createDocument(XMLConstants.ORDER);
		inDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, orderHeaderKey);
		YFCElement extnEle = inDoc.getDocumentElement().createChild(XMLConstants.EXTN);
		extnEle.setAttribute("LinktoQuote", filePath);
		
		String templateStr = "<Order OrderHeaderKey=\"\" ><Extn LinktoQuote=\"\" /></Order>";
		YFCDocument changeOrderDoc = SterlingUtil.callAPI(env, Constants.CHANGE_ORDER_API, inDoc, YFCDocument.parse(templateStr));
		return changeOrderDoc;
	}


	private Document getOrderDetailsForReport(YFSEnvironment env, Document inDoc) throws RemoteException, YIFClientCreationException, SAXException, IOException, TransformerException, ParserConfigurationException {
		// TODO Auto-generated method stub
		String templateStr = "<OrderList><Order DocumentType=\"\" Status=\"\" OrderHeaderKey=\"\" EnterpriseCode=\"\" BuyerOrganizationCode=\"\" OrderNo=\"\" TermsCode=\"\">"
				+ "<Extn /><PersonInfoBillTo /><PersonInfoShipTo /><PriceInfo Currency=\"\" /><OrderLines><OrderLine PrimeLineNo=\"\" OrderedQty=\"\" >"
				+ "<Extn /><Item ItemID=\"\" ItemShortDesc=\"\" /><LinePriceInfo UnitPrice=\"\" "
				+ "DiscountPercentage=\"\" /><LineOverallTotals LineTotal=\"\" /><BundleParentLine OrderLineKey=\"\" />"
				+ "<OrderLineTranQuantity TransactionalUOM=\"\" /></OrderLine></OrderLines></Order></OrderList>";
		
		YFCDocument outDoc = YFCDocument.createDocument();
		String terSCOHKey = inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY);
		YFCDocument getOrderListIn = YFCDocument.createDocument(XMLConstants.ORDER);
		getOrderListIn.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, terSCOHKey);
		YFCDocument getOrderListDoc = SterlingUtil.callAPI(env, Constants.GET_ORDER_LIST, getOrderListIn, YFCDocument.parse(templateStr));
		YFCElement oldRootEle = getOrderListDoc.getElementsByTagName(XMLConstants.ORDER).item(0);
		YFCElement rootEle = outDoc.importNode(oldRootEle, true);
		outDoc.appendChild(rootEle);
		YFCElement orderLinesElement = rootEle.getChildElement(XMLConstants.ORDER_LINES);
		YFCNodeList<YFCElement> oldOLNL = oldRootEle.getElementsByTagName(XMLConstants.ORDER_LINE);
		
		for(int i=0; i  < oldOLNL.getLength(); i++){
			YFCElement olEle = oldOLNL.item(i);
			String primeLineNo = olEle.getAttribute("PrimeLineNo");
			YFCNodeList<YFCElement> bundleParentNL = olEle.getElementsByTagName("BundleParentLine");
			String bundleParentKey = null;
			if (bundleParentNL.getLength() > 0) {
				bundleParentKey = bundleParentNL.item(0).getAttribute(XMLConstants.ORDER_LINE_KEY);
				if(!StringUtil.isEmpty(bundleParentKey)){
					YFCNode olNode = XPathUtil.getXpathNode(outDoc, "/Order/OrderLines/OrderLine[@PrimeLineNo='"+primeLineNo+"']");
					if(olNode != null){
						orderLinesElement.removeChild(olNode); 
					}
				}
			}
		}
		
		
		YFCDocument scibMappingIn = YFCDocument.createDocument(XMLConstants.TER_SCIB_MAPPING);
		scibMappingIn.getDocumentElement().setAttribute(XMLConstants.TER_SCOH_KEY, terSCOHKey);
		YFCDocument scibMappingOut = SterlingUtil.callService(env, Constants.GET_IB_FOR_LINE, scibMappingIn, "");
		YFCElement scibMappingEle = outDoc.createElement(XMLConstants.TER_SCIB_MAPPING_LIST);
		scibMappingEle = outDoc.importNode(scibMappingOut.getDocumentElement(), true);
		outDoc.getDocumentElement().appendChild(scibMappingEle);
		
		//System.out.println("outDoc"+outDoc.getString());
		
		return outDoc.getDocument();
	}


}

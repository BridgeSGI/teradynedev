package com.teradyne.servicecontracts.api;


import java.io.IOException;
import java.rmi.RemoteException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCErrorDetails;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

public class BeforeCreateOrderForSC implements YFSBeforeCreateOrderUE {
	
	public Document beforeCreateOrder(YFSEnvironment env, Document inXML) throws YFSUserExitException {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
		String buyerStr = inDoc.getDocumentElement().getAttribute(XMLConstants.BUYER_ORGANIZATION_CODE);
		
		try {
			
			
			YFCDocument orgInDoc = YFCDocument.createDocument(XMLConstants.ORGANIZATION);
			orgInDoc.getDocumentElement().setAttribute(XMLConstants.ORGANIZATION_CODE, buyerStr);
			String orgTemplateStr = "<OrganizationList><Organization OrganizationCode=\"\" IsNode=\"\" ><Extn TeradyneServiceOffice=\"\" />"
					+ "</Organization></OrganizationList>";
			YFCDocument orgOutDoc = SterlingUtil.callAPI(env, "getOrganizationList", orgInDoc, YFCDocument.parse(orgTemplateStr));
			String isNodeStr = XPathUtil.getXpathAttribute(orgOutDoc, "/OrganizationList/Organization/@IsNode");
			
			if(!"Y".equals(isNodeStr)){
				return inXML;
			}
			
			String offCodeStr = XPathUtil.getXpathAttribute(orgOutDoc, "/OrganizationList/Organization/Extn/@TeradyneServiceOffice");
			
			YFCDocument offInDoc = YFCDocument.createDocument("TerOfficeRecords");
			offInDoc.getDocumentElement().setAttribute("TerOfficeCode", offCodeStr);
			YFCDocument offOutDoc = SterlingUtil.callService(env, "getTerOfficeRecordsList", offInDoc, "");
			YFCElement offEle = offOutDoc.getDocumentElement().getElementsByTagName("TerOfficeRecords").item(0);
			
			if(offEle != null){
				YFCElement extnEle = inDoc.getDocumentElement().getChildElement(XMLConstants.EXTN);
				extnEle.setAttribute("ContractCoordinator", offEle.getAttribute("TerOpsCoordinator"));
				extnEle.setAttribute("ContractManager", offEle.getAttribute("TerRegionalManager"));
			}
			
			
		} catch (RemoteException e) {
			throw new YFCException(new YFCErrorDetails("EXTN_SC001","An internal error occured while creating a service contract"));
		} catch (YIFClientCreationException e) {
			throw new YFCException(new YFCErrorDetails("EXTN_SC001","An internal error occured while creating a service contract"));
		} catch (SAXException e) {
			throw new YFCException(new YFCErrorDetails("EXTN_SC001","An internal error occured while creating a service contract"));
		} catch (IOException e) {
			throw new YFCException(new YFCErrorDetails("EXTN_SC001","An internal error occured while creating a service contract"));
		} catch (TransformerException e) {
			throw new YFCException(new YFCErrorDetails("EXTN_SC001","An internal error occured while creating a service contract"));
		} catch (ParserConfigurationException e) {
			throw new YFCException(new YFCErrorDetails("EXTN_SC001","An internal error occured while creating a service contract"));
		}
		return inDoc.getDocument();
		
	}

	
	@Override
	public String beforeCreateOrder(YFSEnvironment env, String arg1)
			throws YFSUserExitException {
		// TODO Auto-generated method stub
		return null;
	}
}
		

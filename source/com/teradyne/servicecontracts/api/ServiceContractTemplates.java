package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCErrorDetails;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class ServiceContractTemplates {

	public Document modifyTemplate(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		// System.out.println("Input::"+inDoc.getString());
		String scOutTemplate = "<TerSerContTemplate TerSCTKey=\"\" />";
		YFCDocument scTemplateOut = inDoc;
		if (checkModifyTemplateNeeded(inDoc)) {
			scTemplateOut = SterlingUtil.callService(env,
					Constants.MANAGE_SC_TEMPLATE, inDoc,
					YFCDocument.parse(scOutTemplate));
		}

		addServiceItemsToTemplate(env, inDoc);

		return scTemplateOut.getDocument();
	}

	private boolean checkModifyTemplateNeeded(YFCDocument inDoc) {
		YFCElement inEle = inDoc.getDocumentElement();
		if (!StringUtil.isEmpty(inEle.getAttribute(XMLConstants.TER_SCT_NAME))
				|| !StringUtil.isEmpty(inEle
						.getAttribute(XMLConstants.TER_SCT_PRIORITY))
				|| !StringUtil.isEmpty(inEle
						.getAttribute(XMLConstants.TER_IB_ASSO_REQD))
				|| !StringUtil.isEmpty(inEle
						.getAttribute(XMLConstants.TER_SCT_STATUS))
				|| !StringUtil.isEmpty(inEle
						.getAttribute(XMLConstants.TER_SCT_EFFECTIVE_DATE))
				|| !StringUtil.isEmpty(inEle
						.getAttribute(XMLConstants.TER_SCT_DURATION_DAYS))
				|| !StringUtil.isEmpty(XMLConstants.TER_SCT_DRAFT)) {
			return true;
		}
		return false;
	}

	private boolean checkServiceItemsAPICallNeeded(YFCElement inEle) {
		String kitCodeStr = inEle.getAttribute(XMLConstants.KIT_CODE);
		String siKeyStr = inEle.getAttribute(XMLConstants.TER_SI_KEY);
		if (!StringUtil.isEmpty(inEle
				.getAttribute(XMLConstants.TER_SI_EXPEDITE_DISCOUNT))
				|| !StringUtil
						.isEmpty(inEle
								.getAttribute(XMLConstants.TER_SI_REPAIR_LIST_DISCOUNT))
				|| !StringUtil.isEmpty(inEle
						.getAttribute(XMLConstants.TER_SI_ADDITIONAL_DISCOUNT))
				|| !StringUtil.isEmpty(inEle
						.getAttribute(XMLConstants.TER_REP_PARTS_COVERED))
				|| !StringUtil.isEmpty(inEle
						.getAttribute(XMLConstants.TER_NON_REP_PARTS_COVERED))) {
			return true;
		} else {
			if (StringUtil.isEmpty(siKeyStr) && StringUtil.isEmpty(kitCodeStr)) {
				throw new YFCException(new YFCErrorDetails("EXTN_SCT_MAN",
						"All fields except description are mandatory"));
			}
		}
		return false;
	}

	public Document getTemplate(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);

		String sctKeyStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.TER_SCT_KEY);
		YFCDocument scTemplateOut = SterlingUtil.callService(env,
				Constants.GET_SC_TEMPLATE, inDoc, "");
		YFCDocument serviceItemsInDoc = YFCDocument
				.createDocument(XMLConstants.TER_SERVICE_ITEM);
		serviceItemsInDoc.getDocumentElement().setAttribute(
				XMLConstants.TER_SCT_KEY, sctKeyStr);
		YFCDocument serviceItemsList = SterlingUtil.callService(env,
				Constants.GET_SERVICE_ITEM_LIST, serviceItemsInDoc, "");
		YFCNodeList<YFCElement> serItemNL = serviceItemsList
				.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);
		if (serItemNL.getLength() != 0) {
			scTemplateOut.getDocumentElement().setAttribute("HasChildren",
					Constants.YES);
		}

		YFCElement serItemListOutEle = scTemplateOut
				.createElement(XMLConstants.TER_SERVICE_ITEM_LIST);
		for (Iterator<YFCElement> iter = serItemNL.iterator(); iter.hasNext();) {
			YFCElement serItem = iter.next();
			String bundleKeyStr = serItem
					.getAttribute(XMLConstants.TER_SI_PARENT_KEY);
			if (StringUtil.isEmpty(bundleKeyStr)) {
				YFCElement serItemOutEle = scTemplateOut
						.createElement(XMLConstants.TER_SERVICE_ITEM);
				serItemOutEle = scTemplateOut.importNode(serItem, true);
				serItemListOutEle.appendChild(serItemOutEle);
			}
		}
		scTemplateOut.getDocumentElement().appendChild(serItemListOutEle);

		return scTemplateOut.getDocument();
	}

	public void addServiceItemsToTemplate(YFSEnvironment env, YFCDocument inDoc)
			throws YIFClientCreationException, SAXException, IOException {
		YFCElement rootEle = inDoc.getDocumentElement();
		// System.out.println("Root Ele for addServiceItemsToTemplate"+rootEle.getString());
		String reasonCodeStr = rootEle.getAttribute("TerSCTReasonCode");
		String reasonTextStr = rootEle.getAttribute("TerSCTReasonText");
		YFCNodeList<YFCElement> serItemsNL = rootEle
				.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);
		String sctKeyStr = rootEle.getAttribute(XMLConstants.TER_SCT_KEY);
		ArrayList<String> existingSerItems = fetchExistingServiceItems(env, sctKeyStr);

		YFCDocument multiInDoc = YFCDocument
				.createDocument(XMLConstants.MULTI_API_ELE);

		for (Iterator<YFCElement> iter = serItemsNL.iterator(); iter.hasNext();) {
			YFCElement serItemEle = iter.next();
			String kitCodeStr = serItemEle.getAttribute(XMLConstants.KIT_CODE);
			String siKeyStr = serItemEle.getAttribute(XMLConstants.TER_SI_KEY);
			String siItemKeyStr = serItemEle
					.getAttribute(XMLConstants.TER_SI_ITEM_KEY);
			if (StringUtil.isEmpty(siItemKeyStr)
					&& StringUtil.isEmpty(siKeyStr)) {
				fetchItemKey(env, serItemEle);
				kitCodeStr = serItemEle.getAttribute(XMLConstants.KIT_CODE);
				siItemKeyStr = serItemEle
						.getAttribute(XMLConstants.TER_SI_ITEM_KEY);
			}
			if(!existingSerItems.isEmpty() && existingSerItems.contains(siItemKeyStr)){
				throw new YFCException(new YFCErrorDetails("EXTN_SCT_UNIQUE",
						"Unique item constraint violated."));
			}
			if (checkServiceItemsAPICallNeeded(serItemEle)
					|| (StringUtil.isEmpty(siKeyStr) && kitCodeStr
							.equals("BUNDLE"))) {
				serItemEle.setAttribute(XMLConstants.TER_SCT_KEY, sctKeyStr);
				serItemEle.setAttribute("TerSIReasonCode", reasonCodeStr);
				serItemEle.setAttribute("TerSIReasonText", reasonTextStr);

				if (!StringUtil.isEmpty(kitCodeStr)
						&& kitCodeStr.equals("BUNDLE")) {
					processBundleComponents(env, multiInDoc, serItemEle);
				}

				YFCElement apiEle = multiInDoc
						.createElement(XMLConstants.API_ELE);
				YFCElement inputEle = apiEle
						.createChild(XMLConstants.INPUT_ELE);
				YFCElement serItemInEle = inputEle
						.createChild(XMLConstants.TER_SERVICE_ITEM);

				YFCElement templateEle = apiEle
						.createChild(XMLConstants.TEMPLATE_ELE);
				YFCElement siTempEle = templateEle
						.createChild(XMLConstants.TER_SERVICE_ITEM);
				siTempEle.setAttribute(XMLConstants.TER_SI_KEY, "");

				apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API,
						Constants.YES);
				if (StringUtil.isEmpty(siKeyStr)) {
					apiEle.setAttribute(XMLConstants.NAME,
							Constants.CREATE_SERVICE_ITEM);
				} else {
					apiEle.setAttribute(XMLConstants.NAME,
							Constants.MODIFY_SERVICE_ITEM);
				}
				serItemInEle.setAttributes(serItemEle.getAttributes());
				multiInDoc.getDocumentElement().appendChild(apiEle);

			}
		}
		SterlingUtil.callAPI(env, Constants.MULTI_API, multiInDoc, "");
		// System.out.println("MultiAPI output::"+outDoc.getString());

	}

	private ArrayList<String> fetchExistingServiceItems(YFSEnvironment env,
			String sctKeyStr) throws RemoteException, YIFClientCreationException {
		ArrayList<String> itemKeyList = new ArrayList<String>();
		YFCDocument serviceItemsInDoc = YFCDocument
				.createDocument(XMLConstants.TER_SERVICE_ITEM);
		serviceItemsInDoc.getDocumentElement().setAttribute(
				XMLConstants.TER_SCT_KEY, sctKeyStr);
		YFCDocument serviceItemsList = SterlingUtil.callService(env,
				Constants.GET_SERVICE_ITEM_LIST, serviceItemsInDoc, "");
		YFCNodeList<YFCElement> serItemNL = serviceItemsList
				.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM); 
		for(Iterator<YFCElement> iter = serItemNL.iterator(); iter.hasNext();){
			YFCElement serItemEle = iter.next();
			String itemKeyStr = serItemEle.getAttribute(XMLConstants.TER_SI_ITEM_KEY);
			itemKeyList.add(itemKeyStr);
		}
		return itemKeyList;
	}

	private YFCDocument fetchItemKey(YFSEnvironment env, YFCElement serItemEle)
			throws YIFClientCreationException, SAXException, IOException {
		String itemKeyStr = serItemEle
				.getAttribute(XMLConstants.TER_SI_ITEM_KEY);
		if (StringUtil.isEmpty(itemKeyStr)) {
			String itemIDStr = serItemEle.getChildElement("YFSItem")
					.getAttribute(XMLConstants.ITEM_ID);
			String uomStr = serItemEle.getChildElement("YFSItem").getAttribute(
					XMLConstants.UNIT_OF_MEASURE);
			if (StringUtil.isEmpty(itemIDStr) || StringUtil.isEmpty(uomStr)) {
				throw new YFCException(new YFCErrorDetails("EXTN_SCT_ITEM",
						"Item ID and UOM are mandatory"));
			}
			YFCDocument getItemDetails = YFCDocument
					.createDocument(XMLConstants.ITEM);
			getItemDetails.getDocumentElement().setAttribute(
					XMLConstants.ITEM_ID, itemIDStr);
			getItemDetails.getDocumentElement().setAttribute(
					XMLConstants.UNIT_OF_MEASURE, uomStr);
			getItemDetails.getDocumentElement().setAttribute(
					XMLConstants.ORGANIZATION_CODE, "CSO");
			String getItemDetTempStr = "<Item ItemKey=\"\" ><PrimaryInformation KitCode=\"\" /></Item>";

			YFCDocument getItemDetOut = SterlingUtil.callAPI(env,
					Constants.GET_ITEM_DETAILS_API, getItemDetails,
					YFCDocument.parse(getItemDetTempStr));
			serItemEle.setAttribute(XMLConstants.TER_SI_ITEM_KEY, getItemDetOut
					.getDocumentElement().getAttribute(XMLConstants.ITEM_KEY));
			serItemEle.setAttribute(
					XMLConstants.KIT_CODE,
					getItemDetOut.getDocumentElement()
							.getChildElement(XMLConstants.PRIMARY_INFORMATION)
							.getAttribute(XMLConstants.KIT_CODE));
			return getItemDetOut;
		}
		return null;
	}

	private void processBundleComponents(YFSEnvironment env,
			YFCDocument multiInDoc, YFCElement serItemEle)
			throws RemoteException, YIFClientCreationException, SAXException,
			IOException {
		String itemKeyStr = serItemEle
				.getAttribute(XMLConstants.TER_SI_ITEM_KEY);
		String sctKeyStr = serItemEle.getAttribute(XMLConstants.TER_SCT_KEY);
		String reasonCodeStr = serItemEle.getAttribute("TerSCTReasonCode");
		String reasonTextStr = serItemEle.getAttribute("TerSCTReasonText");
		YFCDocument getItemDetOut = null;

		YFCDocument getItemDetails = YFCDocument
				.createDocument(XMLConstants.ITEM);
		getItemDetails.getDocumentElement().setAttribute(XMLConstants.ITEM_KEY,
				itemKeyStr);
		String getItemDetTempStr = "<Item ItemKey=\"\" ><Components><Component ComponentItemKey=\"\" /></Components></Item>";
		getItemDetOut = SterlingUtil.callAPI(env,
				Constants.GET_ITEM_DETAILS_API, getItemDetails,
				YFCDocument.parse(getItemDetTempStr));

		YFCNodeList<YFCElement> compNL = getItemDetOut
				.getElementsByTagName(XMLConstants.COMPONENT);
		for (Iterator<YFCElement> iter = compNL.iterator(); iter.hasNext();) {
			String siItemKeyStr = iter.next().getAttribute("ComponentItemKey");

			YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
			apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
			apiEle.setAttribute(XMLConstants.NAME,
					Constants.CREATE_SERVICE_ITEM);
			YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
			YFCElement serItemInEle = inputEle
					.createChild(XMLConstants.TER_SERVICE_ITEM);
			serItemInEle.setAttribute(XMLConstants.TER_SI_ITEM_KEY,
					siItemKeyStr);
			serItemInEle.setAttribute(XMLConstants.TER_SI_PARENT_KEY,
					itemKeyStr);
			serItemInEle.setAttribute(XMLConstants.TER_SCT_KEY, sctKeyStr);
			serItemInEle.setAttribute("TerSIReasonCode", reasonCodeStr);
			serItemInEle.setAttribute("TerSIReasonText", reasonTextStr);

			YFCElement templateEle = apiEle
					.createChild(XMLConstants.TEMPLATE_ELE);
			YFCElement siTempEle = templateEle
					.createChild(XMLConstants.TER_SERVICE_ITEM);
			siTempEle.setAttribute(XMLConstants.TER_SI_KEY, "");

			multiInDoc.getDocumentElement().appendChild(apiEle);
		}

	}

	public Document deleteTemplate(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String sctKeyStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.TER_SCT_KEY);

		YFCDocument getServiceItemsDoc = YFCDocument
				.createDocument(XMLConstants.TER_SERVICE_ITEM);
		getServiceItemsDoc.getDocumentElement().setAttribute(
				XMLConstants.TER_SCT_KEY, sctKeyStr);
		YFCDocument getSerItemsOut = SterlingUtil.callService(env,
				Constants.GET_SERVICE_ITEM_LIST, getServiceItemsDoc, "");

		YFCNodeList<YFCElement> serItemsNL = getSerItemsOut
				.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);

		YFCDocument multiInDoc = YFCDocument
				.createDocument(XMLConstants.MULTI_API_ELE);
		YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
		apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
		apiEle.setAttribute(XMLConstants.NAME, Constants.DEL_SC_TEMPLATE_API);

		YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
		YFCElement serItemInEle = inputEle
				.createChild(XMLConstants.TER_SC_TEMPLATE);
		serItemInEle.setAttribute(XMLConstants.TER_SCT_KEY, sctKeyStr);
		multiInDoc.getDocumentElement().appendChild(apiEle);

		for (Iterator<YFCElement> iter = serItemsNL.iterator(); iter.hasNext();) {
			YFCElement serItemEle = iter.next();

			String kitCodeStr = serItemEle.getAttribute(XMLConstants.KIT_CODE);
			if (!StringUtil.isEmpty(kitCodeStr) && kitCodeStr.equals("BUNDLE")) {
				RemoveServiceItems rsi = new RemoveServiceItems();
				rsi.removeBundleComponents(env, serItemEle);
			} else {
				apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
				apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API,
						Constants.YES);
				apiEle.setAttribute(XMLConstants.NAME,
						Constants.DEL_SER_ITEM_API);

				inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
				serItemInEle = inputEle
						.createChild(XMLConstants.TER_SERVICE_ITEM);
				serItemInEle.setAttribute(XMLConstants.TER_SI_KEY,
						serItemEle.getAttribute(XMLConstants.TER_SI_KEY));

				multiInDoc.getDocumentElement().appendChild(apiEle);
			}

		}
		SterlingUtil.callAPI(env, Constants.MULTI_API, multiInDoc, "");

		return inputDoc;
	}
}

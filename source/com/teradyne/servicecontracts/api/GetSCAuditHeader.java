package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class GetSCAuditHeader {

	/*
	 * Input Doc in the form of <Audit TableName="" TableKey="" />
	 */
	public Document getAuditHeader(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		HashMap<String, String> parentChildName = new HashMap<String, String>();
		parentChildName.put(Constants.TER_SERVICE_CONTRACT_TEMPLATE,
				Constants.TER_SERVICE_ITEM);
		parentChildName.put(Constants.TER_SCT_SELECTION_RULE,
				Constants.TER_SCT_SELECTION_RULE_LINE);

		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String tableNameStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.TABLE_NAME);
		String tableKeyStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.TABLE_KEY);
		String headerTemplateStr = "<AuditList><Audit AuditKey=\"\" TableName=\"\" TableKey=\"\" Operation=\"\" Reference1=\"\" "
				+ "Reference2=\"\" Reference3=\"\" Reference4=\"\" Createts=\"\" /></AuditList>";
		YFCDocument outParentDoc = SterlingUtil.callAPI(env,
				Constants.GET_AUDIT_LIST, inDoc,
				YFCDocument.parse(headerTemplateStr));
		// System.out.println("outParentDoc"+outParentDoc.getString());
		YFCDocument childInDoc = YFCDocument.getDocumentFor(inputDoc);
		childInDoc.getDocumentElement()
				.setAttribute(XMLConstants.TABLE_KEY, "");
		childInDoc.getDocumentElement().setAttribute(XMLConstants.TABLE_NAME,
				parentChildName.get(tableNameStr));

		childInDoc.getDocumentElement().setAttribute(XMLConstants.REFERENCE4,
				tableKeyStr);
		YFCDocument outChildDoc = SterlingUtil.callAPI(env,
				Constants.GET_AUDIT_LIST, inDoc,
				YFCDocument.parse(headerTemplateStr));
		// System.out.println("outChildDoc"+outChildDoc.getString());
		YFCNodeList<YFCElement> outParentNL = outParentDoc
				.getElementsByTagName(XMLConstants.AUDIT);
		HashMap<String, YFCElement> outHM = new HashMap<String, YFCElement>();
		for (Iterator<YFCElement> iter1 = outParentNL.iterator(); iter1
				.hasNext();) {
			YFCElement outParentEle = iter1.next();
			String keyStr = outParentEle.getAttribute(XMLConstants.AUDIT_KEY);
			outHM.put(keyStr, outParentEle);
		}
		YFCNodeList<YFCElement> outChildNL = outChildDoc
				.getElementsByTagName(XMLConstants.AUDIT);
		for (Iterator<YFCElement> iter2 = outChildNL.iterator(); iter2
				.hasNext();) {
			YFCElement outChildEle = iter2.next();
			String keyStr = outChildEle.getAttribute(XMLConstants.AUDIT_KEY);
			outHM.put(keyStr, outChildEle);
		}

		TreeSet<String> sortKeys = new TreeSet<String>(outHM.keySet());
		// System.out.println("SortKeys"+sortKeys.size()+":"+sortKeys.toString());
		YFCDocument outDoc = YFCDocument
				.createDocument(XMLConstants.AUDIT_LIST);
		for (Iterator<String> iter3 = sortKeys.iterator(); iter3.hasNext();) {
			String keyStr = iter3.next();
			YFCElement outEle = outDoc.createElement(XMLConstants.AUDIT);
			outEle = outDoc.importNode(outHM.get(keyStr), false);
			outDoc.getDocumentElement().appendChild(outEle);
		}
		// System.out.println("outDoc"+outDoc.getString());
		return outDoc.getDocument();
	}

	public Document getAuditDetail(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument getAuditListOut = SterlingUtil.callAPI(env,
				Constants.GET_AUDIT_LIST, YFCDocument.getDocumentFor(inputDoc),
				"");
		String xmlStr = getAuditListOut.getDocumentElement()
				.getChildElement(XMLConstants.AUDIT).getAttribute("AuditXml");
		String shortStr = xmlStr.substring(38);
		// System.out.println("ShortStr"+shortStr);
		YFCDocument outDoc = YFCDocument.parse(shortStr);
		// System.out.println("OutDoc"+outDoc.getString());
		return outDoc.getDocument();
	}
}

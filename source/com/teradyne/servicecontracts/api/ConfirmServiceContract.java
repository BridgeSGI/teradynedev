package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.GregorianCalendar;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class ConfirmServiceContract {

	public Document confirm(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {

		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		// System.out.println("Input Doc to confirm::"+inDoc.getString());
		String ohKeyStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.ORDER_HEADER_KEY);
		String effectiveDateStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.EFFECTIVE_DATE);
		String expirationDateStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.EXPIRATION_DATE);
		String templateStr = "<Order OrderHeaderKey=\"\" />";
		YFCDocument outDoc = SterlingUtil.callAPI(env,
				Constants.CONFIRM_DRAFT_ORDER, inDoc,
				YFCDocument.parse(templateStr));
		GregorianCalendar c = new GregorianCalendar();
		Date sysDate = c.getTime();
		Date effectiveDate = DateTimeUtil.convertDate(effectiveDateStr);
		Date expirationDate = DateTimeUtil.convertDate(expirationDateStr);
		if (sysDate.getTime() < expirationDate.getTime()) {
			String orderListStr = "<OrderList><Order OrderHeaderKey=\"\" DraftOrderFlag=\"\" ><OrderLines>"
					+ "<OrderLine OrderLineKey=\"\" Status=\"\">"
					+ "<Extn ServiceStartDateTime=\"\" /></OrderLine></OrderLines></Order></OrderList>";
			ServiceContractUtil scUtil = new ServiceContractUtil();
			YFCDocument getOrderListOut = scUtil.callGetOrderList(env,
					ohKeyStr, orderListStr);
			YFCDocument changeOrderStatus = YFCDocument.createDocument();
			YFCElement rootEle = changeOrderStatus.importNode(getOrderListOut
					.getDocumentElement().getChildElement(XMLConstants.ORDER),
					true);
			changeOrderStatus.appendChild(rootEle);
			// System.out.println("Input to changeOrderStatus"+changeOrderStatus.getString());
			scUtil.changeStatusOfLines(env, changeOrderStatus);
		}

		return outDoc.getDocument();
	}

}

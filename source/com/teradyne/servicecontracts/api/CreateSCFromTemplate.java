package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCDate;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class CreateSCFromTemplate {

	Date	tdShipDate	= null;
	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	public Document createFromTemplate(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		// fetch template details
		YFCDocument sctOutDoc = fetchSCTemplateDetails(env, inDoc);
		formOrderLinesFromTemplate(inDoc, sctOutDoc);
		String templateStr = "<Order OrderHeaderKey=\"\" ><OrderLines><OrderLine OrderLineKey=\"\" ><BundleParentLine OrderLineKey=\"\" /></OrderLine></OrderLines></Order>";
		YFCDocument outDoc = SterlingUtil.callAPI(env, Constants.CREATE_ORDER,
				inDoc, YFCDocument.parse(templateStr));
		YFCDocument multiApiDoc = createSCIBMapping(inDoc, outDoc);
		if (multiApiDoc != null && multiApiDoc.hasChildNodes()) {
			SterlingUtil.callAPI(env, Constants.MULTI_API, multiApiDoc, "");
		}
		return outDoc.getDocument();
	}

	private YFCDocument createSCIBMapping(YFCDocument inDoc, YFCDocument outDoc) {
		// TODO Auto-generated method stub
		String scOHKeyStr = outDoc.getDocumentElement().getAttribute(
				XMLConstants.ORDER_HEADER_KEY);
		YFCElement extnEle = inDoc.getDocumentElement().getChildElement(
				XMLConstants.EXTN);
		String ibOHKeyStr = extnEle.getAttribute(XMLConstants.ORDER_HEADER_KEY);
		String ibOLKeyStr = extnEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
		if (!StringUtil.isEmpty(ibOLKeyStr) && !StringUtil.isEmpty(ibOHKeyStr)) {
			YFCNodeList<YFCElement> orderLineNL = outDoc.getDocumentElement()
					.getElementsByTagName(XMLConstants.ORDER_LINE);
			YFCDocument multiApiDoc = YFCDocument
					.createDocument(XMLConstants.MULTI_API_ELE);
			for (Iterator<YFCElement> iter = orderLineNL.iterator(); iter
					.hasNext();) {
				YFCElement orderLineEle = iter.next();
				YFCNodeList<YFCElement> bundleParentNL = orderLineEle
						.getElementsByTagName("BundleParentLine");
				String bundleParentKey = null;
				if (bundleParentNL.getLength() > 0) {
					bundleParentKey = bundleParentNL.item(0).getAttribute(
							XMLConstants.ORDER_LINE_KEY);
				}
				if (StringUtil.isEmpty(bundleParentKey)) {
					String scOLKeyStr = orderLineEle
							.getAttribute(XMLConstants.ORDER_LINE_KEY);

					YFCElement apiEle = multiApiDoc
							.createElement(XMLConstants.API_ELE);
					apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API,
							Constants.YES);
					apiEle.setAttribute(XMLConstants.NAME,
							Constants.CREATE_SC_IB_MAPPING_API);

					YFCElement inputEle = apiEle
							.createChild(XMLConstants.INPUT_ELE);
					YFCElement scibEle = inputEle
							.createChild(XMLConstants.TER_SCIB_MAPPING);

					scibEle.setAttribute(XMLConstants.TER_SCOH_KEY, scOHKeyStr);
					scibEle.setAttribute(XMLConstants.TER_SCOL_KEY, scOLKeyStr);
					scibEle.setAttribute(XMLConstants.TER_IBOH_KEY, ibOHKeyStr);
					scibEle.setAttribute(XMLConstants.TER_IBOL_KEY, ibOLKeyStr);

					YFCElement templateEle = apiEle
							.createChild(XMLConstants.TEMPLATE_ELE);
					YFCElement outTempEle = templateEle
							.createChild(XMLConstants.TER_SCIB_MAPPING);
					outTempEle.setAttribute(XMLConstants.TER_SCIB_KEY, "");

					multiApiDoc.getDocumentElement().appendChild(apiEle);
				}
			}

			return multiApiDoc;
		}
		return null;
	}

	private YFCDocument fetchSCTemplateDetails(YFSEnvironment env,
			YFCDocument inDoc) throws YFCException, SAXException, IOException,
			Exception {
		YFCElement extnEle = inDoc.getDocumentElement().getChildElement(
				XMLConstants.EXTN);
		Map<String, String> extnAttrHM = extnEle.getAttributes();
		String scTemplateIDStr = extnAttrHM.get(XMLConstants.SC_TEMPLATE_ID);
		String scTemplateKeyStr = extnAttrHM.get(XMLConstants.TER_SCT_KEY);
		String tdShipDateStr = extnAttrHM.get(XMLConstants.TD_SHIP_DATE);
		if(!StringUtil.isEmpty(tdShipDateStr)){
			tdShipDate = extnEle.getDateAttribute(XMLConstants.TD_SHIP_DATE);
		}
		
		YFCDocument sctInDoc = YFCDocument
				.createDocument(XMLConstants.TER_SC_TEMPLATE);
		sctInDoc.getDocumentElement().setAttribute(XMLConstants.TER_SCT_ID,
				scTemplateIDStr);
		sctInDoc.getDocumentElement().setAttribute(XMLConstants.TER_SCT_KEY,
				scTemplateKeyStr);
		YFCDocument outDoc = getTemplate(env, sctInDoc);
		return outDoc;
	}

	private void formOrderLinesFromTemplate(YFCDocument inDoc,
			YFCDocument sctOutDoc) throws ParserConfigurationException,
			TransformerException {
		YFCElement sctOutEle = sctOutDoc.getDocumentElement();
		String scTypeStr = sctOutEle
				.getAttribute(XMLConstants.TER_SER_CONT_TYPE);
		String scPriorityStr = sctOutEle
				.getAttribute(XMLConstants.TER_SCT_PRIORITY);
		inDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_TYPE,
				scTypeStr);
		inDoc.getDocumentElement().setAttribute(XMLConstants.DRAFT_ORDER_FLAG,
				Constants.YES);
		inDoc.getDocumentElement().setAttribute(XMLConstants.PRIORITY_NUMBER,
				scPriorityStr);
		YFCElement extnEle = inDoc.getDocumentElement().getChildElement(
				XMLConstants.EXTN);
		extnEle.setAttribute(XMLConstants.SC_TEMPLATE_ID, 
				sctOutEle.getAttribute(XMLConstants.TER_SCT_ID));
		calculateDates(inDoc, sctOutEle);

		YFCNodeList<YFCElement> serItemsNL = sctOutDoc.getDocumentElement()
				.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);
		YFCElement orderLinesEle = inDoc
				.createElement(XMLConstants.ORDER_LINES);
		inDoc.getDocumentElement().appendChild(orderLinesEle);
		ArrayList<String> tranAL = new ArrayList<String>();
		for (Iterator<YFCElement> iter = serItemsNL.iterator(); iter.hasNext();) {
			YFCElement serItemEle = iter.next();
			YFCElement yfsItemEle = serItemEle
					.getChildElement(XMLConstants.YFS_ITEM);
			String kitCodeStr = yfsItemEle.getChildElement(
					XMLConstants.PRIMARY_INFORMATION).getAttribute(
					XMLConstants.KIT_CODE);
			String parentSIKeyStr = serItemEle
					.getAttribute(XMLConstants.TER_SI_PARENT_KEY);

			YFCElement orderLineEle = inDoc
					.createElement(XMLConstants.ORDER_LINE);
			orderLineEle.setAttribute(XMLConstants.ORDERED_QTY, "1");
			orderLineEle.setAttribute(XMLConstants.ACTION, XMLConstants.CREATE);

			YFCElement itemEle = inDoc.createElement(XMLConstants.ITEM);
			orderLineEle.appendChild(itemEle);
			itemEle.setAttribute(XMLConstants.ITEM_ID,
					yfsItemEle.getAttribute(XMLConstants.ITEM_ID));

			YFCElement olTranQtyEle = inDoc
					.createElement(XMLConstants.ORDER_LINE_TRAN_QUANTITY);
			orderLineEle.appendChild(olTranQtyEle);
			olTranQtyEle.setAttribute(XMLConstants.TRANSACTIONAL_UOM,
					yfsItemEle.getAttribute(XMLConstants.UNIT_OF_MEASURE));

			YFCElement extnOLEle = inDoc.createElement(XMLConstants.EXTN);
			orderLineEle.appendChild(extnOLEle);

			extnOLEle.setAttribute(
					XMLConstants.NONREPAIR_PARTS_COVERED,
					serItemEle
							.getAttribute(XMLConstants.TER_NON_REP_PARTS_COVERED));
			extnOLEle.setAttribute(XMLConstants.REPAIRABLE_PARTS_COVERED,
					serItemEle.getAttribute(XMLConstants.TER_REP_PARTS_COVERED));
			extnOLEle.setAttribute(
					XMLConstants.STO_REPAIR_LIST_DISCOUNT,
					serItemEle
							.getAttribute(XMLConstants.TER_SI_REPAIR_LIST_DISCOUNT));
			extnOLEle.setAttribute(
					XMLConstants.STO_EXPEDITE_LIST_DISCOUNT,
					serItemEle
							.getAttribute(XMLConstants.TER_SI_EXPEDITE_DISCOUNT));
			extnOLEle.setAttribute(
					XMLConstants.STO_ADDITIONAL_DISCOUNT,
					serItemEle
							.getAttribute(XMLConstants.TER_SI_ADDITIONAL_DISCOUNT));
			extnOLEle.setAttribute(XMLConstants.SERVICE_START_DATE_TIME, extnEle
					.getAttribute(XMLConstants.EFFECTIVE_DATE));

			if (kitCodeStr.equals(Constants.BUNDLE)) {
				String siKey = serItemEle
						.getAttribute(XMLConstants.TER_SI_ITEM_KEY);
				tranAL.add(siKey);
				orderLineEle.setAttribute(XMLConstants.TRANSACTIONAL_LINE_ID,
						Integer.toString(tranAL.indexOf(siKey)));
			} else if (!StringUtil.isEmpty(parentSIKeyStr)) {
				orderLineEle.setAttribute(XMLConstants.TER_SI_PARENT_KEY,
						parentSIKeyStr);
			}

			orderLinesEle.appendChild(orderLineEle);
		}
		YFCNodeList<YFCElement> orderlinesNL = orderLinesEle
				.getElementsByTagName(XMLConstants.ORDER_LINE);
		for (Iterator<YFCElement> iter = orderlinesNL.iterator(); iter
				.hasNext();) {
			YFCElement orderLineEle = iter.next();
			String parentSIKeyStr = orderLineEle
					.getAttribute(XMLConstants.TER_SI_PARENT_KEY);
			if (!StringUtil.isEmpty(parentSIKeyStr)) {
				int tranLineId = tranAL.indexOf(parentSIKeyStr);
				YFCElement olBundleParentEle = inDoc
						.createElement(XMLConstants.BUNDLE_PARENT_LINE);
				orderLineEle.appendChild(olBundleParentEle);
				olBundleParentEle.setAttribute(
						XMLConstants.TRANSACTIONAL_LINE_ID,
						Integer.toString(tranLineId));
			}
		}
		return;
	}

	private void calculateDates(YFCDocument inDoc, YFCElement sctOutEle) {
		// TODO Auto-generated method stub
		YFCElement extnEle = inDoc.getDocumentElement().getChildElement(
				XMLConstants.EXTN);
		String effectiveDateStr = extnEle
				.getAttribute(XMLConstants.EFFECTIVE_DATE);
		
		int effectiveDays = sctOutEle
				.getIntAttribute(XMLConstants.TER_SCT_EFFECTIVE_DATE);
		
		int durationDays = sctOutEle
				.getIntAttribute(XMLConstants.TER_SCT_DURATION_DAYS);
		//System.out.println("EffDate, EffDays, ExpDays"+effectiveDateStr+":"+effectiveDays+":"+durationDays);
		Date effectiveDate = null;
		if (tdShipDate != null) {
			
			effectiveDate = DateTimeUtil.addToDate(tdShipDate,
					Calendar.DAY_OF_MONTH, effectiveDays);
			//System.out.println("TdShipDate, EffectiveDate"+tdShipDate+":"+effectiveDate);
			extnEle.setAttribute(XMLConstants.EFFECTIVE_DATE, DateTimeUtil
					.formatDate(effectiveDate, DATE_FORMAT));

		} else if (!StringUtil.isEmpty(effectiveDateStr)) {
			effectiveDate = extnEle.getDateAttribute(XMLConstants.EFFECTIVE_DATE);
		}
		if (effectiveDate != null) {
			Date expirationDate = DateTimeUtil.addToDate(effectiveDate,
					Calendar.DAY_OF_MONTH, durationDays);
			//System.out.println("EffDate, ExpDate"+effectiveDate+":"+expirationDate);
			extnEle.setAttribute(XMLConstants.EXPIRATION_DATE, DateTimeUtil
					.formatDate(expirationDate, DATE_FORMAT));

		}
	}

	private YFCDocument getTemplate(YFSEnvironment env, YFCDocument inDoc)
			throws YFCException, SAXException, IOException, Exception {

		String sctKeyStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.TER_SCT_KEY);
		YFCDocument scTemplateOut = SterlingUtil.callService(env,
				Constants.GET_SC_TEMPLATE, inDoc, "");
		YFCDocument serviceItemsInDoc = YFCDocument
				.createDocument(XMLConstants.TER_SERVICE_ITEM);
		serviceItemsInDoc.getDocumentElement().setAttribute(
				XMLConstants.TER_SCT_KEY, sctKeyStr);
		YFCDocument serviceItemsList = SterlingUtil.callService(env,
				Constants.GET_SERVICE_ITEM_LIST, serviceItemsInDoc, "");

		YFCElement serItemsListEle = scTemplateOut
				.createElement(XMLConstants.TER_SERVICE_ITEM_LIST);
		serItemsListEle = scTemplateOut.importNode(
				serviceItemsList.getDocumentElement(), true);
		scTemplateOut.getDocumentElement().appendChild(serItemsListEle);
		//System.out.println("SCTemplate Out::"+scTemplateOut.getString());
		return scTemplateOut;
	}
}

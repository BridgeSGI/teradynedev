package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.teradyne.servicecontracts.api.ServiceContractTemplates;

public class CopySCTemplate {

	public Document copyTemplate(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		YFCElement inEle = inDoc.getDocumentElement();
		String oldSCTKey = inEle.getAttribute("OldTerSCTKey");
		String scOutTemplate = "<TerSerContTemplate TerSCTKey=\"\" />";

		YFCDocument getOldTemplateIn = YFCDocument
				.createDocument(XMLConstants.TER_SC_TEMPLATE);
		getOldTemplateIn.getDocumentElement().setAttribute(
				XMLConstants.TER_SCT_KEY, oldSCTKey);
		YFCDocument getOldTemplate = YFCDocument
				.getDocumentFor(getTemplateForCopy(env,
						getOldTemplateIn.getDocument()));
		YFCElement getOldTemplateEle = getOldTemplate.getDocumentElement();

		inEle.removeAttribute(XMLConstants.TER_SCT_KEY);
		inEle.setAttribute(XMLConstants.TER_IB_ASSO_REQD,
				getOldTemplateEle.getAttribute(XMLConstants.TER_IB_ASSO_REQD));
		inEle.setAttribute(XMLConstants.TER_SCT_PRIORITY,
				getOldTemplateEle.getAttribute(XMLConstants.TER_SCT_PRIORITY));
		inEle.setAttribute(XMLConstants.TER_SCT_STATUS,
				getOldTemplateEle.getAttribute(XMLConstants.TER_SCT_STATUS));
		inEle.setAttribute(XMLConstants.TER_SCT_EFFECTIVE_DATE,
				getOldTemplateEle
						.getAttribute(XMLConstants.TER_SCT_EFFECTIVE_DATE));
		inEle.setAttribute(XMLConstants.TER_SCT_DURATION_DAYS,
				getOldTemplateEle
						.getAttribute(XMLConstants.TER_SCT_DURATION_DAYS));

		YFCDocument newTemplateOut = SterlingUtil.callService(env,
				Constants.CREATE_SC_TEMPLATE, inDoc,
				YFCDocument.parse(scOutTemplate));
		YFCElement newSerItemsList = newTemplateOut
				.createElement(XMLConstants.TER_SERVICE_ITEM_LIST);
		newSerItemsList = newTemplateOut.importNode(getOldTemplateEle
				.getChildElement(XMLConstants.TER_SERVICE_ITEM_LIST), true);
		newTemplateOut.getDocumentElement().appendChild(newSerItemsList);
		// System.out.println("new TemplateOut::"+newTemplateOut.getString());

		ServiceContractTemplates sctObj = new ServiceContractTemplates();
		sctObj.addServiceItemsToTemplate(env, newTemplateOut);

		return newTemplateOut.getDocument();
	}

	public Document getTemplateForCopy(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);

		String sctKeyStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.TER_SCT_KEY);
		YFCDocument scTemplateOut = SterlingUtil.callService(env,
				Constants.GET_SC_TEMPLATE, inDoc, "");

		YFCDocument serviceItemsInDoc = YFCDocument
				.createDocument(XMLConstants.TER_SERVICE_ITEM);
		serviceItemsInDoc.getDocumentElement().setAttribute(
				XMLConstants.TER_SCT_KEY, sctKeyStr);

		YFCDocument serviceItemsList = SterlingUtil.callService(env,
				Constants.GET_SERVICE_ITEM_LIST, serviceItemsInDoc, "");
		YFCNodeList<YFCElement> serItemsNL = serviceItemsList
				.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);
		// Remove service items primary key
		for (Iterator<YFCElement> iter = serItemsNL.iterator(); iter.hasNext();) {
			YFCElement serItemEle = iter.next();
			serItemEle.removeAttribute(XMLConstants.TER_SCT_KEY);
			serItemEle.removeAttribute(XMLConstants.TER_SI_KEY);
		}

		YFCElement serItemListOutEle = scTemplateOut
				.createElement(XMLConstants.TER_SERVICE_ITEM_LIST);
		serItemListOutEle = scTemplateOut.importNode(
				serviceItemsList.getDocumentElement(), true);
		scTemplateOut.getDocumentElement().appendChild(serItemListOutEle);
		return scTemplateOut.getDocument();
	}

}

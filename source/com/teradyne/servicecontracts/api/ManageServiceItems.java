package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;

import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;

public class ManageServiceItems {

	public Document manageServiceItem(YFSEnvironment env, Document inDoc)
			throws YIFClientCreationException, SAXException, IOException {
		YFCElement rootEle = YFCDocument.getDocumentFor(inDoc)
				.getDocumentElement();
		YFCNodeList<YFCElement> serItemsNL = rootEle
				.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);

		YFCDocument multiInDoc = YFCDocument
				.createDocument(XMLConstants.MULTI_API_ELE);

		for (Iterator<YFCElement> iter = serItemsNL.iterator(); iter.hasNext();) {
			YFCElement serItemEle = iter.next();

			YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
			apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
			apiEle.setAttribute(XMLConstants.NAME,
					Constants.MODIFY_SERVICE_ITEM);

			YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
			YFCElement serItemInEle = inputEle
					.createChild(XMLConstants.TER_SERVICE_ITEM);
			serItemInEle.setAttributes(serItemEle.getAttributes());

			YFCElement templateEle = apiEle
					.createChild(XMLConstants.TEMPLATE_ELE);
			YFCElement siTempEle = templateEle
					.createChild(XMLConstants.TER_SERVICE_ITEM);
			siTempEle.setAttribute(XMLConstants.TER_SI_KEY, "");

			multiInDoc.getDocumentElement().appendChild(apiEle);

		}

		YFCDocument outDoc = SterlingUtil.callAPI(env, Constants.MULTI_API,
				multiInDoc, "");
		return outDoc.getDocument();
	}

}

package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.bridge.sterling.utils.StringUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class ManageServiceContract {

	public Document manage(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		ServiceContractUtil scUtil = new ServiceContractUtil();
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String ohKey = inDoc.getDocumentElement().getAttribute(
				XMLConstants.ORDER_HEADER_KEY);

		String orderListStr = "<OrderList><Order OrderHeaderKey=\"\" DraftOrderFlag=\"\" ><OrderLines><OrderLine OrderLineKey=\"\" "
				+ "OrderedQty=\"\" ><LinePriceInfo UnitPrice=\"\" DiscountPercentage=\"\" /></OrderLine></OrderLines></Order></OrderList>";

		YFCDocument getOrderListOut = scUtil.callGetOrderList(env, ohKey,
				orderListStr);
		addOrderDatesElement(inDoc);
		setServiceStartDateForLines(inDoc, getOrderListOut);
		String templateStr = "<Order OrderHeaderKey=\"\" DraftOrderFlag=\"\" ><OrderLines><OrderLine OrderLineKey=\"\" "
				+ "Status=\"\" ><Extn ServiceStartDateTime=\"\" /></OrderLine></OrderLines><OverallTotals LineSubTotal=\"\" "
				+ "GrandTotal=\"\" /></Order>";

		// System.out.println("Input to ChangeOrder:: "+inDoc.getString());
		YFCDocument outDoc = SterlingUtil.callAPI(env,
				Constants.CHANGE_ORDER_API, inDoc,
				YFCDocument.parse(templateStr));

		scUtil.setPriceAttributes(env, outDoc);
		// changeStatus for lines
		String draftOrderFlag = outDoc.getDocumentElement().getAttribute(
				Constants.DRAFT_ORDER_FLAG);
		if (draftOrderFlag.equals(Constants.NO)) {
			scUtil.changeStatusOfLines(env, outDoc);// no changes required for
													// bundle parent
		}
		return outDoc.getDocument();
	}

	private void addOrderDatesElement(YFCDocument inDoc) {
		YFCElement extnEle = inDoc.getDocumentElement().getChildElement(
				XMLConstants.EXTN);
		if (extnEle != null) {
			Map<String, String> extnAttrHM = extnEle.getAttributes();
			if (extnAttrHM.containsKey(XMLConstants.EXPIRATION_DATE)
					&& !StringUtil.isEmpty(extnAttrHM
							.get(XMLConstants.EXPIRATION_DATE))) {
				YFCElement orderDatesEle = inDoc.getDocumentElement()
						.createChild(XMLConstants.ORDER_DATES);
				YFCElement orderDateEle = orderDatesEle
						.createChild(XMLConstants.ORDER_DATE);
				orderDateEle.setAttribute(XMLConstants.DATE_TYPE_ID,
						Constants.SC_EXPIRATION_DATE);
				orderDateEle.setAttribute(XMLConstants.REQUESTED_DATE,
						extnAttrHM.get(XMLConstants.EXPIRATION_DATE));
			}
		}
	}

	private void setServiceStartDateForLines(YFCDocument inDoc,
			YFCDocument orderDoc) throws TransformerException,
			ParserConfigurationException {
		String effectiveDateStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.EFFECTIVE_DATE);
		YFCNodeList<YFCElement> orderLinesEle = inDoc.getDocumentElement()
				.getElementsByTagName(XMLConstants.ORDER_LINE);

		for (Iterator<YFCElement> iter = orderLinesEle.iterator(); iter
				.hasNext();) {
			double oldUnitPrice = 0;
			double oldDiscountPercent = 0;
			double oldOrderedQty = 1;
			YFCElement orderLineEle = iter.next();
			Map<String, String> olAttrHM = orderLineEle.getAttributes();

			YFCNodeList<YFCElement> linePriceNL = orderLineEle
					.getElementsByTagName(XMLConstants.LINE_PRICE_INFO);
			if (linePriceNL.getLength() > 0) {
				YFCElement linePriceInfo = linePriceNL.item(0);

				if (olAttrHM.containsKey(XMLConstants.ACTION)
						&& olAttrHM.get(XMLConstants.ACTION).equals(
								Constants.CREATE)) {
					YFCElement extnEle = orderLineEle
							.getChildElement(XMLConstants.EXTN);
					Map<String, String> extnAttrHM = extnEle.getAttributes();

					if (!extnAttrHM
							.containsKey(XMLConstants.SERVICE_START_DATE_TIME)) {
						extnEle.setAttribute(
								XMLConstants.SERVICE_START_DATE_TIME,
								effectiveDateStr);
					}
					YFCElement itemEle = orderLineEle
							.getChildElement(XMLConstants.ITEM);
					if (itemEle.getAttribute(XMLConstants.KIT_CODE).equals(
							Constants.BUNDLE)) {
						linePriceInfo
								.setAttribute(
										XMLConstants.IS_LINE_PRICE_FOR_INFORMATION_ONLY,
										Constants.YES);
					}

				} else {
					String olKey = orderLineEle
							.getAttribute(XMLConstants.ORDER_LINE_KEY);
					String oldUPStr = XPathUtil
							.getXpathAttributeWithDefaultValue(orderDoc,
									"//OrderLine[@OrderLineKey=\"" + olKey
											+ "\"]/LinePriceInfo/@UnitPrice",
									"0");
					oldUnitPrice = Double.parseDouble(oldUPStr);
					String oldDPStr = XPathUtil
							.getXpathAttributeWithDefaultValue(
									orderDoc,
									"//Order/OrderLines/OrderLine[@OrderLineKey=\""
											+ olKey
											+ "\"]/LinePriceInfo/@DiscountPercentage",
									"0");
					oldDiscountPercent = Double.parseDouble(oldDPStr);
					String oldQtyStr = XPathUtil
							.getXpathAttributeWithDefaultValue(orderDoc,
									"//OrderLine[@OrderLineKey=\"" + olKey
											+ "\"]/@OrderedQty", "1");
					oldOrderedQty = Double.parseDouble(oldQtyStr);
				}

				Map<String, String> linePIHM = linePriceInfo.getAttributes();

				double unitPrice = linePIHM
						.containsKey(XMLConstants.UNIT_PRICE) ? linePriceInfo
						.getDoubleAttribute(XMLConstants.UNIT_PRICE, 0)
						: oldUnitPrice;
				double discount = linePIHM
						.containsKey(XMLConstants.DISCOUNT_PERCENTAGE) ? linePriceInfo
						.getDoubleAttribute(XMLConstants.DISCOUNT_PERCENTAGE, 0)
						: oldDiscountPercent;
				double qty = olAttrHM.containsKey(XMLConstants.ORDERED_QTY) ? orderLineEle
						.getDoubleAttribute(XMLConstants.ORDERED_QTY)
						: oldOrderedQty;
				if (discount != 0) {
					double amount = (unitPrice * qty * discount) / 100;
					YFCElement lineChargesEle = orderLineEle
							.createChild(XMLConstants.LINE_CHARGES);
					YFCElement lineChargeEle = lineChargesEle
							.createChild(XMLConstants.LINE_CHARGE);
					lineChargeEle.setAttribute(XMLConstants.CHARGE_CATEGORY,
							Constants.SC_DISCOUNT);
					lineChargeEle.setAttribute(XMLConstants.CHARGE_NAME,
							Constants.SC_DISCOUNT);
					String chargePerLine = new BigDecimal(amount).setScale(2,
							BigDecimal.ROUND_HALF_UP).toString();
					lineChargeEle.setAttribute(XMLConstants.CHARGE_PER_LINE,
							chargePerLine);
				}

			}
			addDatesAtLineLevel(orderLineEle);

		}

	}

	private void addDatesAtLineLevel(YFCElement orderLineEle) {
		YFCNodeList<YFCElement> extnNL = orderLineEle
				.getElementsByTagName(XMLConstants.EXTN);
		if (extnNL.getLength() > 0) {
			YFCElement extnEle = extnNL.item(0);
			Map<String, String> extnHM = extnEle.getAttributes();
			if (extnHM.containsKey(XMLConstants.SERVICE_START_DATE_TIME)
					&& !StringUtil.isEmpty(extnHM
							.get(XMLConstants.SERVICE_START_DATE_TIME))) {
				YFCElement orderDatesEle = orderLineEle
						.createChild(XMLConstants.ORDER_DATES);
				YFCElement orderDateEle = orderDatesEle
						.createChild(XMLConstants.ORDER_DATE);
				orderDateEle.setAttribute(XMLConstants.DATE_TYPE_ID,
						"EFFECTIVE_DATE");
				orderDateEle.setAttribute(XMLConstants.REQUESTED_DATE,
						extnHM.get(XMLConstants.SERVICE_START_DATE_TIME));
			}
		}

	}
}

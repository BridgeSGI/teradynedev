package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class SCTSelectionRule {

	public Document modifySelectionRule(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String srOutTemplate = "<TerSCTSelectionRule TerSRKey=\"\" />";

		// System.out.println("Input to modifySelectionRule::"+inDoc.getString());
		YFCDocument selRuleOut;
		if (!checkModifyTemplateNeeded(inDoc)) {
			addTemplatesToRules(env, inDoc);
			selRuleOut = YFCDocument.createDocument("TerSCTSelectionRule");
			selRuleOut.getDocumentElement().setAttribute("TerSRKey",
					inDoc.getDocumentElement().getAttribute("TerSRKey"));
		} else {
			inDoc = appendReasonCodeToLines(inDoc);
			selRuleOut = SterlingUtil.callService(env,
					Constants.MANAGE_SEL_RULE, inDoc,
					YFCDocument.parse(srOutTemplate));
		}
		// System.out.println("Output to modifySelectionRule::"+selRuleOut.getString());
		return selRuleOut.getDocument();
	}

	private YFCDocument appendReasonCodeToLines(YFCDocument inDoc) {
		String reasonCodeStr = inDoc.getDocumentElement().getAttribute(
				"TerSRReasonCode");
		String reasonTextStr = inDoc.getDocumentElement().getAttribute(
				"TerSRReasonText");
		YFCNodeList<YFCElement> srLinesNL = inDoc.getDocumentElement()
				.getElementsByTagName("TerSelectionRuleLine");
		if (srLinesNL.getLength() == 0) {
			return inDoc;
		}
		for (Iterator<YFCElement> iter = srLinesNL.iterator(); iter.hasNext();) {
			YFCElement selRuleLineEle = iter.next();
			selRuleLineEle.setAttribute("TerSRLReasonCode", reasonCodeStr);
			selRuleLineEle.setAttribute("TerSRLReasonText", reasonTextStr);
		}
		return inDoc;
	}

	private boolean checkModifyTemplateNeeded(YFCDocument inDoc) {
		YFCElement inEle = inDoc.getDocumentElement();
		if (!StringUtil.isEmpty(inEle.getAttribute("TerCountryCode"))
				|| !StringUtil.isEmpty(inEle.getAttribute("TerBuildStatus"))
				|| !StringUtil.isEmpty(inEle
						.getAttribute("TerExpToInstallDate"))
				|| !StringUtil.isEmpty(inEle
						.getAttribute("TerExpFromInstallDate"))
				|| !StringUtil.isEmpty(inEle.getAttribute("TerFromBookYw"))
				|| !StringUtil.isEmpty(inEle.getAttribute("TerToBookYw"))
				|| !StringUtil.isEmpty(inEle.getAttribute("TerFromShipDate"))
				|| !StringUtil.isEmpty(inEle.getAttribute("TerToShipDate"))
				|| !StringUtil.isEmpty(inEle.getAttribute("TerInstallNode"))
				|| !StringUtil.isEmpty(inEle.getAttribute("TerOwnerOrgID"))
				|| !StringUtil.isEmpty(inEle.getAttribute("TerItemID"))
				|| !StringUtil.isEmpty(inEle.getAttribute("TerSRName"))) {
			return true;
		}
		return false;
	}

	public void addTemplatesToRules(YFSEnvironment env, YFCDocument inDoc)
			throws YIFClientCreationException, SAXException, IOException {
		YFCElement rootEle = inDoc.getDocumentElement();
		String reasonCodeStr = rootEle.getAttribute("TerSRReasonCode");
		String reasonTextStr = rootEle.getAttribute("TerSRReasonText");
		YFCNodeList<YFCElement> srLinesNL = rootEle
				.getElementsByTagName(XMLConstants.TER_SELECTION_RULE_LINE);
		String srKeyStr = rootEle.getAttribute(XMLConstants.TER_SR_KEY);

		YFCDocument multiInDoc = YFCDocument
				.createDocument(XMLConstants.MULTI_API_ELE);

		for (Iterator<YFCElement> iter = srLinesNL.iterator(); iter.hasNext();) {
			YFCElement srLineEle = iter.next();
			srLineEle.setAttribute(XMLConstants.TER_SR_KEY, srKeyStr);
			srLineEle.setAttribute("TerSRLReasonCode", reasonCodeStr);
			srLineEle.setAttribute("TerSRLReasonText", reasonTextStr);

			YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
			YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
			YFCElement srLineInEle = inputEle
					.createChild(XMLConstants.TER_SELECTION_RULE_LINE);

			YFCElement templateEle = apiEle
					.createChild(XMLConstants.TEMPLATE_ELE);
			YFCElement siTempEle = templateEle
					.createChild(XMLConstants.TER_SELECTION_RULE_LINE);
			siTempEle.setAttribute(XMLConstants.TER_SRL_KEY, "");

			apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
			apiEle.setAttribute(XMLConstants.NAME,
					Constants.CREATE_SEL_RULE_LINE);
			srLineInEle.setAttributes(srLineEle.getAttributes());
			multiInDoc.getDocumentElement().appendChild(apiEle);

		}
		YFCDocument outDoc = SterlingUtil.callAPI(env, Constants.MULTI_API,
				multiInDoc, "");
		// System.out.println("MultiAPI output::"+outDoc.getString());

	}

}

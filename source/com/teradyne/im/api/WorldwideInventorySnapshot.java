package com.teradyne.im.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.bridge.sterling.utils.XPathUtil;

public class WorldwideInventorySnapshot {

	public static final String TAIWAN_NODE = "H2T";
	public static final String BAD = "DEFECTIVE";
	public static final String GOOD = "GOOD";
	private static final String XPATH_SHPNODE_INV_FIRST = "//Item/ShipNodes/ShipNode[@ShipNode=\"";
	private static final String XPATH_SHPNODE_INV_SUPPLY = "\"]/Supplies/InventorySupplyType[@SupplyType=\"";
	private static final String XPATH_SHPNODE_INV_DEMAND = "\"]/Demands/InventoryDemandType[@DemandType=\"";
	private static final String XPATH_SHPNODE_INV_QTY = "\"]/@Quantity";
	private static final String orderListTemplate = "<OrderList><Order OrderNo=\"\" ><OrderLines><OrderLine ShipNode=\"\"><Item ItemID=\"\" ProductClass=\"\" />"
			+ "</OrderLine></OrderLines></Order></OrderList>";



	public Document getInventoryDetails(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String templateName = null;
		YFCDocument getShipNodeInvGoodOut = SterlingUtil.callAPI(env,
				Constants.GET_SHIP_NODE_INVENTORY,
				createInDocForShipNodeInv(inDoc, WorldwideInventorySnapshot.GOOD), templateName);
		String itemShortDesc = XPathUtil.getXpathAttribute(getShipNodeInvGoodOut, "/ShipNodeInventory/Item/PrimaryInformation/@ShortDescription");
		YFCDocument getShipNodeInvBadOut = SterlingUtil.callAPI(env,
				Constants.GET_SHIP_NODE_INVENTORY,
				createInDocForShipNodeInv(inDoc, WorldwideInventorySnapshot.BAD), templateName);

		Document getPlanNoGoodDoc = new ShipNodeInventory().getShipNodeInventory(env,getShipNodeInvGoodOut,inDoc);
		YFCDocument getPlanNoGoodOut = YFCDocument.getDocumentFor(getPlanNoGoodDoc);

		Document getPlanNoBadDoc = new ShipNodeInventory().getShipNodeInventory(env,getShipNodeInvBadOut,inDoc);

		YFCDocument getPlanNoBadOut = YFCDocument.getDocumentFor(getPlanNoBadDoc);

		YFCDocument outDoc = YFCDocument.createDocument(XMLConstants.ITEM);

		YFCElement eleShipNodes = outDoc.createElement(XMLConstants.SHIP_NODES);
		outDoc.getDocumentElement().appendChild(eleShipNodes);

		String itemid = inDoc.getDocumentElement().getAttribute(XMLConstants.ITEM_ID);
		outDoc.getDocumentElement().setAttribute(XMLConstants.ITEM_ID,
				itemid);
		String uom = inDoc.getDocumentElement().getAttribute(
				XMLConstants.UNIT_OF_MEASURE);
		outDoc.getDocumentElement().setAttribute(
				XMLConstants.UNIT_OF_MEASURE,
				uom);

		outDoc.getDocumentElement().setAttribute("ShortDescription", itemShortDesc);
		outDoc.getDocumentElement().setAttribute(XMLConstants.DISTRIBUTION_RULE_ID, 
				inDoc.getDocumentElement().getAttribute(XMLConstants.DISTRIBUTION_RULE_ID));

		outDoc.getDocumentElement().setAttribute("PUP", computePUP(env, inDoc));
		outDoc.getDocumentElement().setAttribute(
				XMLConstants.ORGANIZATION_CODE,
				inDoc.getDocumentElement().getAttribute(
						XMLConstants.ORGANIZATION_CODE));

		YFCNodeList<YFCElement> nlGoodShipNodes = getShipNodeInvGoodOut
				.getElementsByTagName(XMLConstants.SHIP_NODE);
		YFCNodeList<YFCElement> nlBadShipNodes = getShipNodeInvBadOut
				.getElementsByTagName(XMLConstants.SHIP_NODE);
		HashSet<String> hsShipNodes = new HashSet<String>();

		for (int i = 0; i < nlGoodShipNodes.getLength(); i++) {
			YFCElement eleShipNode = (YFCElement) nlGoodShipNodes.item(i);
			hsShipNodes.add(eleShipNode.getAttribute(XMLConstants.SHIP_NODE));
		}

		for (int j = 0; j < nlBadShipNodes.getLength(); j++) {
			YFCElement eleShipNode = (YFCElement) nlBadShipNodes.item(j);
			hsShipNodes.add(eleShipNode.getAttribute(XMLConstants.SHIP_NODE));
		}

		Map<String, Integer> ordersMap = callGetOrderList(env, inDoc, false);
		Map<String, Integer> advancedExchangeMap = callGetOrderList(env, inDoc,
				true);

		for (Iterator<String> it = hsShipNodes.iterator(); it.hasNext();) {
			String strShipNode = it.next();
			YFCElement eleOutShipNode = outDoc
					.createElement(XMLConstants.SHIP_NODE);
			eleOutShipNode.setAttribute(XMLConstants.SHIP_NODE, strShipNode);
			boolean cylceCountReqExists = isCycleCountRequestExists(env,strShipNode,itemid,uom);

			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("GoodOnhandQty", "*");
			} else {
				eleOutShipNode.setAttribute(
						"GoodOnhandQty",
						getSupplyForShipNode(getShipNodeInvGoodOut, strShipNode,
								Constants.ONHAND));
			}

			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("BadOnhandQty", "*");
			} else {
				eleOutShipNode.setAttribute(
						"BadOnhandQty",
						getSupplyForShipNode(getShipNodeInvBadOut, strShipNode,
								Constants.ONHAND));
			}


			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("GoodInTransit", "*");
			} else {
				eleOutShipNode.setAttribute(
						"GoodInTransit",
						getSupplyForShipNode(getShipNodeInvGoodOut, strShipNode,
								Constants.INTRANSIT));
			}


			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("BadInTransit", "*");
			} else {
				eleOutShipNode.setAttribute(
						"BadInTransit",
						getSupplyForShipNode(getShipNodeInvBadOut, strShipNode,
								Constants.INTRANSIT));
			}


			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("GoodOutTransit", "*");
			} else {
				eleOutShipNode.setAttribute(
						"GoodOutTransit",
						getSupplyForShipNode(getShipNodeInvGoodOut, strShipNode,
								Constants.OUTBOUND));
			}


			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("BadOutTransit", "*");
			} else {
				eleOutShipNode.setAttribute(
						"BadOutTransit",
						getSupplyForShipNode(getShipNodeInvGoodOut, strShipNode,
								Constants.OUTBOUND));
			}


			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("POQty", "*");
			} else {
				eleOutShipNode.setAttribute(
						"POQty",
						getPOQtyForShipNode(getShipNodeInvGoodOut,
								getShipNodeInvBadOut, strShipNode));
			}


			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("OpenWO", "*");
			} else {
				eleOutShipNode.setAttribute(
						"OpenWO",
						getSumQtyForShipNode(getShipNodeInvGoodOut,
								getShipNodeInvBadOut, strShipNode,
								Constants.WO_PLACED));
			}


			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("PlanNo", "*");
			} else {
				eleOutShipNode.setAttribute(
						"PlanNo",
						getPlanNoForShipNode(getPlanNoGoodOut, getPlanNoBadOut,
								strShipNode));
			}


			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("Allocated", "*");
			} else {
				eleOutShipNode.setAttribute(
						"Allocated",
						getSumQtyForShipNode(getShipNodeInvGoodOut,
								getShipNodeInvBadOut, strShipNode,
								Constants.ALLOCATED));
			}


			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("Backordered", "*");
			} else {
				eleOutShipNode.setAttribute(
						"Backordered",
						getSumQtyForShipNode(getShipNodeInvGoodOut,
								getShipNodeInvBadOut, strShipNode,
								Constants.BACKORDERED));
			}



			Integer usageQty = ordersMap.containsKey(strShipNode) ? ordersMap.get(strShipNode) : new Integer(0);
			Integer advExchangeQty = advancedExchangeMap.containsKey(strShipNode) ? advancedExchangeMap.get(strShipNode) : new Integer(0);

			if(cylceCountReqExists) {
				eleOutShipNode.setAttribute("Usage", "*");
			} else {
				eleOutShipNode.setAttribute("Usage", usageQty.toString());
			}

			if (usageQty.intValue() == 0) {
				if(cylceCountReqExists) {
					eleOutShipNode.setAttribute("EMSPercentage", "*");
				} else {
					eleOutShipNode.setAttribute("EMSPercentage", "0");
				}

			} else {
				if(cylceCountReqExists) {
					eleOutShipNode.setAttribute("EMSPercentage", "*");
				} else {
					Double emsPercentDbl = new Double(
							(advExchangeQty.intValue() / usageQty.intValue()) * 100);
					eleOutShipNode.setAttribute("EMSPercentage", new BigDecimal(
							emsPercentDbl).setScale(2, BigDecimal.ROUND_HALF_UP)
							.toString());
				}

			}

			eleShipNodes.appendChild(eleOutShipNode);
		}

		YFCDocument returnOut = callOrderListForWR(env, inDoc,"ReturnOut");
		YFCDocument workOrderOut = callOrderListForWR(env,inDoc,"WorkOrderOut");
		outDoc.getDocumentElement().setAttribute("CPSOwned", returnOut.getDocumentElement().getAttribute("Total"));
		outDoc.getDocumentElement().setAttribute("CPSOwnedAtTaiwan", returnOut.getDocumentElement().getAttribute("Taiwan"));
		outDoc.getDocumentElement().setAttribute("CustomerOwned", workOrderOut.getDocumentElement().getAttribute("Total"));
		outDoc.getDocumentElement().setAttribute("CustomerOwnedAtTaiwan", workOrderOut.getDocumentElement().getAttribute("Taiwan"));

		return outDoc.getDocument();
	}



	private boolean isCycleCountRequestExists(YFSEnvironment env,
			String strShipNode, String itemid, String uom) {
		try {
			String strCountReqListInput = "<CountRequest Node='" + strShipNode + "' ItemID='" + itemid + "' UnitOfMeasure='" + uom + "' RequestType='DEFAULT' StatusQryType='LT' Status='2000'/>";
			String strCountReqListTemplate = "<CountRequestList TotalNumberOfRecords='' />";
			YFCDocument docGetCountRequestListOutput = SterlingUtil.callAPI(env, "getCountRequestList", YFCDocument.getDocumentFor(strCountReqListInput), 
					YFCDocument.getDocumentFor(strCountReqListTemplate));
			int totCountRequests = docGetCountRequestListOutput.getDocumentElement().getIntAttribute("TotalNumberOfRecords",0);
			if(totCountRequests != 0) {
				return true;
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (YFCException e) {
			e.printStackTrace();
		} catch (YIFClientCreationException e) {
			e.printStackTrace();
		}
		return false;
	}



	private String getPlanNoForShipNode(YFCDocument getPlanNoGoodOut,
			YFCDocument getPlanNoBadOut, String strShipNode) throws ParserConfigurationException, TransformerException {
		String goodPlanNoStr = XPathUtil.getXpathAttribute(getPlanNoGoodOut,
				"/ShipNodeInventory/Item/ShipNodes/ShipNode[@ShipNode=\""
						+ strShipNode + "\"]/@ExtnPlanNo");
		String badPlanNoStr = XPathUtil.getXpathAttribute(getPlanNoBadOut,
				"/ShipNodeInventory/Item/ShipNodes/ShipNode[@ShipNode=\""
						+ strShipNode + "\"]/@ExtnPlanNo");
		if(StringUtil.isEmpty(goodPlanNoStr)){
			goodPlanNoStr = "0";
		}
		if(StringUtil.isEmpty(badPlanNoStr)){
			badPlanNoStr = "0";
		}
		int planNo = Integer.parseInt(goodPlanNoStr)
				+ Integer.parseInt(badPlanNoStr);
		return Integer.toString(planNo);
	}

	private YFCDocument createInDocForShipNodeInv(YFCDocument inDoc,
			String productClassStr) {
		inDoc.getDocumentElement().setAttribute(XMLConstants.SHIP_DATE,
				DateTimeUtil.getFutureDate(Constants.YYYY_MM_DD, 180));
		inDoc.getDocumentElement().setAttribute(XMLConstants.PRODUCT_CLASS,
				productClassStr);
		return inDoc;
	}

	private String getSumQtyForShipNode(YFCDocument inGoodDoc, YFCDocument inBadDoc,
			String strShipNode, String strDemandType)
					throws ParserConfigurationException, TransformerException {
		// TODO Auto-generated method stub
		double qty = Double.parseDouble(getDemandForShipNode(inGoodDoc,
				strShipNode, strDemandType))
				+ Double.parseDouble(getDemandForShipNode(inBadDoc,
						strShipNode, strDemandType));

		String strQty = Integer.toString(new BigDecimal(qty).intValue());
		return strQty;
	}

	private String getPOQtyForShipNode(YFCDocument inGoodDoc, YFCDocument inBadDoc,
			String strShipNode) throws ParserConfigurationException, TransformerException {
		// TODO Auto-generated method stub
		Double poQty = getDoubleValueForPOSupply(inGoodDoc,	strShipNode, Constants.PO_PLACED)
				+ getDoubleValueForPOSupply(inBadDoc, strShipNode, Constants.PO_PLACED)
				+ getDoubleValueForPOSupply(inGoodDoc, strShipNode, Constants.PO_BACKORDER)
				+ getDoubleValueForPOSupply(inBadDoc, strShipNode, Constants.PO_BACKORDER)
				+ getDoubleValueForPOSupply(inGoodDoc, strShipNode, Constants.PO_SCHEDULED)
				+ getDoubleValueForPOSupply(inBadDoc, strShipNode, Constants.PO_SCHEDULED)
				+ getDoubleValueForPOSupply(inGoodDoc, strShipNode, Constants.PO_RELEASED)
				+ getDoubleValueForPOSupply(inBadDoc, strShipNode, Constants.PO_RELEASED);
		String strPOQty = Integer.toString(new BigDecimal(poQty).intValue());
		return strPOQty;

	}

	private String getDemandForShipNode(YFCDocument inDoc, String strShipNode,
			String strDemandType) throws ParserConfigurationException, TransformerException {

		String xpathDemandQty = XPATH_SHPNODE_INV_FIRST + strShipNode
				+ XPATH_SHPNODE_INV_DEMAND + strDemandType
				+ XPATH_SHPNODE_INV_QTY;
		return XPathUtil.getXpathAttributeWithDefaultValue(inDoc,
				xpathDemandQty, Constants.ZERO);

	}

	private String getSupplyForShipNode(YFCDocument inDoc, String strShipNode,
			String strSupplyType) throws ParserConfigurationException, TransformerException {
		String xpathSupplyQty = XPATH_SHPNODE_INV_FIRST + strShipNode
				+ XPATH_SHPNODE_INV_SUPPLY + strSupplyType
				+ XPATH_SHPNODE_INV_QTY;
		double supply =  Double.parseDouble(XPathUtil.getXpathAttributeWithDefaultValue(inDoc,
				xpathSupplyQty, Constants.ZERO));
		return Integer.toString(new BigDecimal(supply).intValue());

	}

	private Double getDoubleValueForPOSupply(YFCDocument inDoc, String strShipNode,
			String strSupplyType) throws ParserConfigurationException, TransformerException {
		String xpathSupplyQty = XPATH_SHPNODE_INV_FIRST + strShipNode
				+ XPATH_SHPNODE_INV_SUPPLY + strSupplyType
				+ XPATH_SHPNODE_INV_QTY;
		return Double.parseDouble(XPathUtil.getXpathAttributeWithDefaultValue(inDoc,
				xpathSupplyQty, Constants.ZERO));

	}

	private YFCDocument formInputForGetOrderList(String documentType, String fromStatus, String toStatus, String itemID, String uomStr, String enterpriseCode, String orderType){
		YFCDocument getOrderListIn = YFCDocument.createDocument(XMLConstants.ORDER);

		YFCElement orderEle = getOrderListIn.getDocumentElement();
		orderEle.setAttribute(XMLConstants.ENTERPRISE_CODE, enterpriseCode);
		orderEle.setAttribute(XMLConstants.DOCUMENT_TYPE, documentType);
		orderEle.setAttribute(XMLConstants.MAXIMUM_RECORDS, "5000");
		orderEle.setAttribute(XMLConstants.DRAFT_ORDER_FLAG, Constants.NO);
		orderEle.setAttribute(XMLConstants.ORDER_DATE_QRY_TYPE, Constants.GE);
		orderEle.setAttribute(XMLConstants.ORDER_DATE,
				DateTimeUtil.getPreviousDate(Constants.YYYY_MM_DD, 270));
		orderEle.setAttribute("FromStatus", fromStatus);
		orderEle.setAttribute("ToStatus", toStatus);
		if(!StringUtil.isEmpty(orderType)){
			orderEle.setAttribute(XMLConstants.ORDER_TYPE, orderType);
		}

		YFCElement orderLineEle = getOrderListIn
				.createElement(XMLConstants.ORDER_LINE);
		orderEle.appendChild(orderLineEle);
		orderLineEle.setAttribute(XMLConstants.ORDERING_UOM, uomStr);

		YFCElement itemEle = getOrderListIn.createElement(XMLConstants.ITEM);
		orderLineEle.appendChild(itemEle);
		itemEle.setAttribute(XMLConstants.ITEM_ID, itemID);


		return getOrderListIn;
	}

	private String computePUP(YFSEnvironment env, YFCDocument inDoc) throws RemoteException, YIFClientCreationException, SAXException, IOException {
		String orderListStr = "<OrderList TotalNumberOfRecords=\"\" TotalOrderList=\"\" />";
		String enterpriseCode = inDoc.getDocumentElement().getAttribute(XMLConstants.ORGANIZATION_CODE);
		String itemIDStr = inDoc.getDocumentElement().getAttribute(XMLConstants.ITEM_ID);
		String uomStr = inDoc.getDocumentElement().getAttribute(XMLConstants.UNIT_OF_MEASURE);
		YFCDocument salesOrderListIn = formInputForGetOrderList("0001", "1100", "3700", itemIDStr, uomStr, enterpriseCode, "PUP");
		YFCDocument installBaseListIn = formInputForGetOrderList("0017.ex", "1100", "3700", itemIDStr, uomStr, enterpriseCode, "PUP");
		YFCDocument salesOrderListOut = SterlingUtil.callAPI(env,
				Constants.GET_ORDER_LIST, salesOrderListIn, YFCDocument.parse(orderListStr));
		YFCDocument installBaseListOut = SterlingUtil.callAPI(env,
				Constants.GET_ORDER_LIST, installBaseListIn, YFCDocument.parse(orderListStr));
		int pupQty = salesOrderListOut.getDocumentElement().getIntAttribute("TotalOrderList") +
				installBaseListOut.getDocumentElement().getIntAttribute("TotalOrderList");
		return Integer.toString(pupQty);		

	}


	private YFCDocument callOrderListForWR(YFSEnvironment env,YFCDocument inDoc, String docName) throws Exception{
		YFCDocument outDoc = YFCDocument.createDocument(docName);
		String docType=null;
		String fromStatus=null;
		String toStatus=null;
		if(docName.equals("WorkOrderOut")){
			docType = "7001";
			fromStatus="1100";
			toStatus="1400.20";
		}else{
			docType = "0003";
			fromStatus="1100";
			toStatus= "3780";
		}
		String enterpriseCode = inDoc.getDocumentElement().getAttribute(XMLConstants.ORGANIZATION_CODE);
		String itemIDStr = inDoc.getDocumentElement().getAttribute(XMLConstants.ITEM_ID);
		String uomStr = inDoc.getDocumentElement().getAttribute(XMLConstants.UNIT_OF_MEASURE);
		YFCDocument getOrderListIn = formInputForGetOrderList(docType, fromStatus, toStatus, itemIDStr, uomStr, enterpriseCode,"");
		YFCDocument getOrderListOut = SterlingUtil.callAPI(env,
				Constants.GET_ORDER_LIST, getOrderListIn, YFCDocument.parse(orderListTemplate));
		int taiwanOrders = countOrdersForTaiwan(getOrderListOut, itemIDStr, WorldwideInventorySnapshot.GOOD);

		int remainingOrders = getOrderListOut.getDocumentElement().getChildNodes().getLength() - taiwanOrders;

		outDoc.getDocumentElement().setAttribute("Total", Integer.toString(remainingOrders));
		outDoc.getDocumentElement().setAttribute("Taiwan", Integer.toString(taiwanOrders));

		return outDoc;
	}

	private Map<String, Integer> callGetOrderList(YFSEnvironment env,
			YFCDocument inDoc, boolean isExchange) throws Exception {

		String orderListTemplate = "<OrderList><Order OrderNo=\"\" ><OrderLines><OrderLine ShipNode=\"\"><Item ItemID=\"\" ProductClass=\"\" />"
				+ "</OrderLine></OrderLines></Order></OrderList>";

		YFCDocument getOrderListIn = YFCDocument.createDocument(XMLConstants.ORDER);

		YFCElement orderEle = getOrderListIn.getDocumentElement();
		orderEle.setAttribute(
				XMLConstants.ENTERPRISE_CODE,
				inDoc.getDocumentElement().getAttribute(
						XMLConstants.ORGANIZATION_CODE));
		orderEle.setAttribute(XMLConstants.DOCUMENT_TYPE, "0001");
		orderEle.setAttribute(XMLConstants.MAXIMUM_RECORDS, "1000");
		orderEle.setAttribute(XMLConstants.DRAFT_ORDER_FLAG, Constants.NO);
		orderEle.setAttribute(XMLConstants.ORDER_DATE_QRY_TYPE, Constants.GE);
		orderEle.setAttribute(XMLConstants.ORDER_DATE,
				DateTimeUtil.getPreviousDate(Constants.YYYY_MM_DD, 270));
		if (isExchange) {
			orderEle.setAttribute(XMLConstants.EXCHANGE_TYPE, Constants.ADVANCED);
		}

		YFCElement orderLineEle = getOrderListIn
				.createElement(XMLConstants.ORDER_LINE);
		orderEle.appendChild(orderLineEle);
		orderLineEle.setAttribute(XMLConstants.ORDERING_UOM, Constants.EACH);

		YFCElement itemEle = getOrderListIn.createElement(XMLConstants.ITEM);
		orderLineEle.appendChild(itemEle);
		String itemIDStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.ITEM_ID);
		itemEle.setAttribute(XMLConstants.ITEM_ID, itemIDStr);
		itemEle.setAttribute(XMLConstants.PRODUCT_CLASS, WorldwideInventorySnapshot.GOOD);

		YFCDocument getOrderListGoodOut = SterlingUtil.callAPI(env,
				Constants.GET_ORDER_LIST, getOrderListIn, YFCDocument.parse(orderListTemplate));
		Map<String, Integer> ordersNodeMap = new HashMap<String, Integer>();
		countOrdersPerNode(getOrderListGoodOut, ordersNodeMap, itemIDStr,
				WorldwideInventorySnapshot.GOOD);

		itemEle.setAttribute(XMLConstants.PRODUCT_CLASS, WorldwideInventorySnapshot.BAD);
		YFCDocument getOrderListBadOut = SterlingUtil.callAPI(env, Constants.GET_ORDER_LIST,
				getOrderListIn, YFCDocument.parse(orderListTemplate));
		countOrdersPerNode(getOrderListBadOut, ordersNodeMap, itemIDStr, WorldwideInventorySnapshot.BAD);

		return ordersNodeMap;
	}

	private void countOrdersPerNode(YFCDocument inDoc,
			Map<String, Integer> ordersNodeMap, String itemIDStr,
			String productClassStr) throws Exception {
		YFCNodeList<YFCNode> ordersNL = inDoc.getDocumentElement().getChildNodes();

		for (int i = 0; i < ordersNL.getLength(); i++) {
			YFCNode orderNode =  ordersNL.item(i);

			YFCNodeList<YFCNode> itemNL = XPathUtil.getXpathNodeList(orderNode,
					"/Order/OrderLines/OrderLine/Item[@ItemID = \"" + itemIDStr
					+ "\" " + "and @ProductClass = \""
					+ productClassStr + "\"]");
			for(int j=0; j < itemNL.getLength(); j++){
				YFCNode itemEle = itemNL.item(j);
				String shipNodeStr = ((YFCElement) itemEle.getParentNode()).getAttribute(XMLConstants.SHIP_NODE);
				if (!StringUtil.isEmpty(shipNodeStr)) {
					if (ordersNodeMap.containsKey(shipNodeStr)) {
						ordersNodeMap.put(shipNodeStr,
								Integer.valueOf(ordersNodeMap.get(shipNodeStr)
										.intValue() + 1));
					} else {
						ordersNodeMap.put(shipNodeStr, Integer.parseInt("1"));
					}
				}
			}

		}

	}

	private int countOrdersForTaiwan(YFCDocument inDoc, String itemIDStr, String productClassStr) throws Exception {
		YFCNodeList<YFCNode> ordersNL = inDoc.getDocumentElement().getChildNodes();
		int count = 0;
		for (int i = 0; i < ordersNL.getLength(); i++) {
			YFCNode orderNode = ordersNL.item(i);
			YFCNodeList<YFCNode> itemNL = XPathUtil.getXpathNodeList(orderNode,
					"/Order/OrderLines/OrderLine/Item[@ItemID = \"" + itemIDStr
					+ "\" " + "and @ProductClass = \""
					+ productClassStr + "\"]");

			for(int j=0; j < itemNL.getLength(); j++){
				YFCNode itemEle =  itemNL.item(j);
				String shipNodeStr = ((YFCElement) itemEle.getParentNode())
						.getAttribute(XMLConstants.SHIP_NODE);
				if(shipNodeStr.equals(WorldwideInventorySnapshot.TAIWAN_NODE)){
					count++;
					break;
				}
			}
		}
		return count;
	}
}

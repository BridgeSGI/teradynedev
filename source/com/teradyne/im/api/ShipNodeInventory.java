package com.teradyne.im.api;

import java.io.IOException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.XPathUtil;


public class ShipNodeInventory {
	
	public Document getShipNodeInventory(YFSEnvironment env, YFCDocument getShipNodeInvOut, YFCDocument inDoc) throws YFCException,SAXException, IOException,Exception {
		YFCDocument itemNodeDefnOut = callGetItemNodeDefnList(env,inDoc);
		YFCNodeList<YFCNode> nlShipNode = XPathUtil.getXpathNodeList(getShipNodeInvOut, "/ShipNodeInventory/Item/ShipNodes/ShipNode");
		for(int i=0 ; i < nlShipNode.getLength(); i++){
			YFCElement eleShipNode = (YFCElement) nlShipNode.item(i);
			String shipNodeStr = eleShipNode.getAttribute(XMLConstants.SHIP_NODE);
			String planStr = XPathUtil.getXpathAttributeWithDefaultValue(itemNodeDefnOut, "//ItemNodeDefnList/ItemNodeDefn[@Node=\""+shipNodeStr+"\"]/Extn/@ExtnPlan",Constants.ZERO);
			String platinumPlanStr = XPathUtil.getXpathAttributeWithDefaultValue(itemNodeDefnOut, "//ItemNodeDefnList/ItemNodeDefn[@Node=\""+shipNodeStr+"\"]/Extn/@ExtnPlatinumPlan",Constants.ZERO);
			eleShipNode.setAttribute(XMLConstants.EXTN_PLAN_NO, planStr);
			eleShipNode.setAttribute(XMLConstants.EXTN_PLATINUM_PLAN, platinumPlanStr);
		}
		return getShipNodeInvOut.getDocument();
	}
	
	  private YFCDocument callGetItemNodeDefnList(YFSEnvironment env, YFCDocument inDoc) throws Exception {
		// TODO Auto-generated method stub
		  YFCDocument itemNodeDefnInDoc = YFCDocument.createDocument(XMLConstants.ITEM_NODE_DEFN);
		  YFCElement inDocEle = inDoc.getDocumentElement();
		  itemNodeDefnInDoc.getDocumentElement().setAttribute(XMLConstants.ORGANIZATION_CODE, inDocEle.getAttribute(XMLConstants.ORGANIZATION_CODE));
		  itemNodeDefnInDoc.getDocumentElement().setAttribute(XMLConstants.ITEM_ID,inDocEle.getAttribute(XMLConstants.ITEM_ID));
		  itemNodeDefnInDoc.getDocumentElement().setAttribute(XMLConstants.UNIT_OF_MEASURE, inDocEle.getAttribute(XMLConstants.UNIT_OF_MEASURE));
		  String templateName = null;
		  return SterlingUtil.callAPI(env, Constants.GET_ITEM_NODE_DEFN_LIST, itemNodeDefnInDoc, templateName);
	}
	  
	    
}


package com.teradyne.IBSC.api;

import org.w3c.dom.Document;

import com.teradyne.om.api.CustomCodeHelper;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;

public class TeradyneGetAllSelectionRuleLineList extends CustomCodeHelper

{

	private static YFCLogCategory _cat = YFCLogCategory
			.instance("com.yantra.CustomCode");

	public YFCDocument getSelectionRuleLineList(YFSEnvironment arg0)
			throws YFSUserExitException {
		setEnv(arg0);

		Document SelectionRuleLineList = null;
		Document inXML = getSelectionRuleLineListIP();
		SelectionRuleLineList = executeFlow("getSRLineList", inXML);
		
		return YFCDocument.getDocumentFor(SelectionRuleLineList);

	}

	private Document getSelectionRuleLineListIP() throws YFSException {
		final YFCDocument getSelectionRuleLineListIP = YFCDocument
				.getDocumentFor("<TerSCTSelectionRuleLine TerSRDraft=\"N\" >"
						+ "<TERSerContTemp TerIBAssoReqd=\"Y\" TerSCTDraft=\"N\" "
						+ "TerSCTStatus=\"A\" TerSerContType=\"FW\"/>"
						+ "</TerSCTSelectionRuleLine>");
		return getSelectionRuleLineListIP.getDocument();
	}

}
package com.teradyne.IBSC.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Document;
import com.bridge.sterling.utils.DateTimeUtil;
import com.teradyne.om.api.CustomCodeHelper;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class TeradyneAutoIBSCcreationONConfirm extends CustomCodeHelper

{

	private static YFCLogCategory _cat = YFCLogCategory
			.instance("com.yantra.CustomCode");
	ArrayList<HashMap<?, ?>> listofMaps = new ArrayList<HashMap<?, ?>>();
	HashMap<String, ArrayList<String>> SRKeyFilters = new HashMap<String, ArrayList<String>>();
	HashMap<String, ArrayList<String>> SRKeyFiltersFirstPass = new HashMap<String, ArrayList<String>>();
	HashMap<String, String> SRLineKeySRKey = new HashMap<String, String>();
	HashMap<String, String> SRLineKeySCTKey = new HashMap<String, String>();
	HashMap<String, Integer> SRSCTKeyDays = new HashMap<String, Integer>();
	String sTdShipDate = null;
	String sBookYW = null;
	String sCountry = null;
	String sSuperMatchedSRKey = null;
	String sSuperMatchedSCTKey = null;
	String sBuyerOrgCode=null;
	String sSellerOrgCode=null;
	String sEntPCode=null;
	String sOdrHdrKey=null;
	String sOdrLineKey=null;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public void manageAutoIBSC(YFSEnvironment arg0, Document inXML)
			throws Exception {
		setEnv(arg0);
		if (_cat.isVerboseEnabled()) {
			_cat.verbose("inside TeradyneAutoIBSCcreationONConfirm.manageAutoIBSC:"
					);
		}

		final TeradyneMapSelectionRuleFilters TeradyneMapSelectionRuleFilters = new TeradyneMapSelectionRuleFilters();

		listofMaps = TeradyneMapSelectionRuleFilters
				.bestMatchSCContractTemplate(arg0);
		if (_cat.isVerboseEnabled()) {
			_cat.verbose("List of maps containg the selection rules:"+listofMaps
					);
		}
		SRKeyFilters = (HashMap<String, ArrayList<String>>) listofMaps.get(0);
		SRLineKeySRKey = (HashMap<String, String>) listofMaps.get(1);
		SRLineKeySCTKey = (HashMap<String, String>) listofMaps.get(2);
		SRSCTKeyDays = (HashMap<String, Integer>) listofMaps.get(3);
		if (_cat.isVerboseEnabled()) {
			_cat.verbose("SRKeyFilters:"+SRKeyFilters+"SRLineKeySRKey:"+SRLineKeySRKey+"SRLineKeySCTKey:"+SRLineKeySCTKey+SRSCTKeyDays
					);
		}
		
		YFCDocument inYFCXML = null;
		inYFCXML = YFCDocument.getDocumentFor(inXML);
		if (_cat.isVerboseEnabled()) {
			_cat.verbose("input to the service component:"+inYFCXML.toString()
					);
		}
		sTdShipDate = inYFCXML.getDocumentElement().getChildElement("Extn")
				.getAttribute("TdShipDate");
		sBookYW = inYFCXML.getDocumentElement().getChildElement("Extn")
				.getAttribute("BookYW");
		sCountry = inYFCXML.getDocumentElement()
				.getChildElement("PersonInfoShipTo").getAttribute("Country");
		sBuyerOrgCode=inYFCXML.getDocumentElement().getAttribute("BuyerOrganizationCode");
		sSellerOrgCode=inYFCXML.getDocumentElement().getAttribute("SellerOrganizationCode");
		sEntPCode=inYFCXML.getDocumentElement().getAttribute("EnterpriseCode");
		sOdrHdrKey=inYFCXML.getDocumentElement().getAttribute("OrderHeaderKey");
		final TeradyneSelectionRuleFirstPass TeradyneSelectionRuleFirstPass = new TeradyneSelectionRuleFirstPass();
		SRKeyFiltersFirstPass = TeradyneSelectionRuleFirstPass
				.selectionRuleFirstPass(SRKeyFilters, sCountry, sBookYW,
						sTdShipDate);
		if (_cat.isVerboseEnabled()) {
			_cat.verbose("first pass filtered selection rule"+SRKeyFiltersFirstPass
					);
		}
		/* for each line find the set of filters and do a supermatch */
		final YFCNodeList<YFCElement> OrderLineNodeList = inYFCXML
				.getDocumentElement().getElementsByTagName("OrderLine");

	

		if (OrderLineNodeList.getLength() > 0)

		{
			
			for (int i = 0; i < OrderLineNodeList.getLength(); i++)

			{
				// OrderLineFilters=null;
				/* prepare arraylist of filers from order line */
				ArrayList<Integer> sNumOfDays=new ArrayList<Integer>();
				ArrayList<String> OrderLineFilters = new ArrayList<String>();
				final YFCNode cOLNode = OrderLineNodeList.item(i);
				final YFCElement OdrlineElem = (YFCElement) cOLNode;
				sOdrLineKey=OdrlineElem.getAttribute("OrderLineKey");
				String sTesterOwnerOrgID = OdrlineElem.getChildElement("Extn")
						.getAttribute("TesterOwnerOrgID");
				String ActualInstallDate=OdrlineElem.getChildElement("Extn")
						.getAttribute("ActualInsDate");
				String sInstallNode = OdrlineElem.getAttribute("ReceivingNode");
				String sItemID = OdrlineElem.getChildElement("Item")
						.getAttribute("ItemID");
				String sIBBuildStatus = OdrlineElem.getChildElement("Extn")
						.getAttribute("IBBuildStatus");
				String sRespProductMfgID = OdrlineElem
						.getChildElement("ItemDetails").getChildElement("Extn")
						.getAttribute("RespProductMfgID");
				String sRespProdMfgDivCode = OdrlineElem
						.getChildElement("ItemDetails").getChildElement("Extn")
						.getAttribute("RespProdMfgDivCode");
				OrderLineFilters.add(0, sTesterOwnerOrgID);
				OrderLineFilters.add(1, sInstallNode);
				OrderLineFilters.add(2, sItemID);
				OrderLineFilters.add(3, sIBBuildStatus);
				OrderLineFilters.add(4, sRespProductMfgID);
				OrderLineFilters.add(5, sRespProdMfgDivCode);
				if (_cat.isVerboseEnabled()) {
					_cat.verbose("orderline filters"+OrderLineFilters+"for key"+sOdrLineKey
							);
				}

				/*
				 * call another method to do the supermatch with above array and
				 * SRKeyFiltersFirstPass
				 */
				final TeradyneSelectionRuleSuperMatch TeradyneSelectionRuleSuperMatch = new TeradyneSelectionRuleSuperMatch();
				sSuperMatchedSRKey = TeradyneSelectionRuleSuperMatch
						.selectionRuleSuperMatch(SRKeyFiltersFirstPass,
								OrderLineFilters);
				if (_cat.isVerboseEnabled()) {
					_cat.verbose("sSuperMatchedSRKey"+sSuperMatchedSRKey
							);
				}
				if (sSuperMatchedSRKey != null) {
					for (Object result : getKeysFromValue(SRLineKeySRKey,
							sSuperMatchedSRKey)) {
						/* get sctkey from srlinekey */
						sSuperMatchedSCTKey = SRLineKeySCTKey.get(result
								.toString());
						if (_cat.isVerboseEnabled()) {
							_cat.verbose("sSuperMatchedSCTKey"+sSuperMatchedSCTKey
									);
						}
					

					if (sSuperMatchedSCTKey != null) {
						
						
						
					Document IPtoServiceCall=IPtoCreateSContractServiceCall(sSuperMatchedSCTKey,sBuyerOrgCode,sSellerOrgCode,
							sTdShipDate,sOdrHdrKey,sOdrLineKey,sEntPCode);
					if (_cat.isVerboseEnabled()) {
						_cat.verbose("IPtoServiceCall document "+YFCDocument.getDocumentFor(IPtoServiceCall).toString()
								);
					}
					Document dOutPutfromSCcreation=executeFlow("CreateSCFromTemplate", IPtoServiceCall);
					/*do the date calculations and based on result change status as well*/
						YFCDocument dYFCOutPutfromSCcreation=YFCDocument.getDocumentFor(dOutPutfromSCcreation);
					if(dYFCOutPutfromSCcreation.getDocumentElement().hasAttribute("OrderHeaderKey")&&	dYFCOutPutfromSCcreation.getDocumentElement().getAttribute("OrderHeaderKey")!=null)
					{
						/*add days for all valid SCs created*/
						sNumOfDays.add(SRSCTKeyDays.get(sSuperMatchedSCTKey));
						
						
					}
					}
						
					}

if(!sNumOfDays.isEmpty())
					{
						Integer minNoOfDays=Collections.min(sNumOfDays);
						
						Date dExpecetdInstallDate=DateTimeUtil.addToDate(dateFormat
										.parse(sTdShipDate),Calendar.DAY_OF_MONTH, minNoOfDays);
										String sExpectedDate=dateFormat.format(dExpecetdInstallDate);
								
							if(ActualInstallDate==null||dateFormat.parse(ActualInstallDate).after(dExpecetdInstallDate))
							{
								/*call final  orderline change here with only expected install date*/
							System.out.println("ActualInstallDate==null");	
							
							invokeApi("changeOrder", IPtoChangeIBLineExpDate(sOdrHdrKey,sOdrLineKey,sExpectedDate));
						
						
						
							}	
							
							else if(dateFormat.parse(ActualInstallDate).before(dExpecetdInstallDate))
							{
								System.out.println("dActaulInstallDate.before(dExpecetdInstallDate==null");	
								
								/*call final  orderline change here with only expected install date*/
								
								/*call change order staus to put the line into active status*/
								
								invokeApi("multiApi",IPtoChangeIBLineExpDatetoActive(sOdrHdrKey,sOdrLineKey,sExpectedDate));
							
						


				}
				}
				}

			}
		}

	}

	public static ArrayList<Object> getKeysFromValue(Map<?, ?> hm, Object value) {
		ArrayList<Object> list = new ArrayList<Object>();
		for (Object o : hm.keySet()) {
			if (hm.get(o).equals(value)) {
				list.add(o);
			}
		}

		return list;

	}
	
	
	
	
	private Document IPtoCreateSContractServiceCall(String strTerSCTKey,String strBuyerOrg,
			String sstrSellerOrg,String strtdShipDate,String strOrderHeaderKey,String strOrderLineKey,String strEntpCode) throws Exception 
			{
String sDocType="0018.ex";
String sOdrTyp="FW";
		YFCDocument inputdoc = YFCDocument
				.getDocumentFor("<Order BuyerOrganizationCode=\""+strBuyerOrg+ "\" SellerOrganizationCode=\""+sstrSellerOrg+ 
						"\" EnterpriseCode=\""+strEntpCode+ "\" DocumentType=\""+sDocType+"\" OrderType=\""+sOdrTyp+ "\" >"
								+ "<Extn TerSCTKey=\""+strTerSCTKey+ "\" TdShipDate=\""+strtdShipDate+"\" OrderHeaderKey=\""+strOrderHeaderKey+"\" OrderLineKey=\""+strOrderLineKey+"\" /></Order>");

		return inputdoc.getDocument();
	}
	private Document IPtoChangeIBLineExpDate(String strOrderHeaderKey,String strOrderLineKey,String strExpectedDate) throws Exception 
			{

		YFCDocument inputdocchangeIBLine = YFCDocument
				.getDocumentFor("<Order Override=\"Y\" OrderHeaderKey=\""+strOrderHeaderKey+"\"><OrderLines><OrderLine   OrderLineKey=\""+strOrderLineKey+"\" ><Extn ExpectedInsDate =\""+strExpectedDate+"\"/></OrderLine></OrderLines></Order>");
		
		

		return inputdocchangeIBLine.getDocument();
	}

	

	private Document IPtoChangeIBLineExpDatetoActive(String strOrderHeaderKey,String strOrderLineKey,String strExpectedDate) throws Exception 
			{

		YFCDocument inputdocchangeIBLine = YFCDocument
				.getDocumentFor("<MultiApi><API Name=\"changeOrder\"><Input><Order  "
						+ "OrderHeaderKey=\""+strOrderHeaderKey+"\" Override=\"Y\"><OrderLines><OrderLine   "
								+ "OrderLineKey=\""+strOrderLineKey+"\"><Extn ExpectedInsDate =\""+strExpectedDate+"\" />"
										+ "</OrderLine></OrderLines></Order></Input></API>"
										+ "<API Name=\"changeOrderStatus\"><Input><OrderStatusChange IgnoreTransactionDependencies=\"Y\"  "
										+ "OrderHeaderKey=\""+strOrderHeaderKey+"\"  "
												+ "TransactionId=\"InstallBaseCycle.0017.ex.ex\"><OrderLines>"
												+ "<OrderLine BaseDropStatus=\"1100.30\" ChangeForAllAvailableQty=\"Y\" "
														+ "OrderLineKey=\""+strOrderLineKey+"\" />"
																+ "</OrderLines></OrderStatusChange>"
																+ "</Input>"
																+ "</API></MultiApi>");
		

		return inputdocchangeIBLine.getDocument();
	}

}

package com.teradyne.IBSC.api;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;

public class TeradyneMapSelectionRuleFilters

{
	String cSCTConID = null;
	YFCDocument doc = null;
	ArrayList<HashMap<?, ?>> listx = new ArrayList<HashMap<?, ?>>();

	public ArrayList<HashMap<?, ?>> bestMatchSCContractTemplate(
			YFSEnvironment arg0) throws YFSException {
		final TeradyneGetAllSelectionRuleLineList TeradyneGetAllSelectionRuleLineList = new TeradyneGetAllSelectionRuleLineList();

		try {
			doc = TeradyneGetAllSelectionRuleLineList
					.getSelectionRuleLineList(arg0);
		} catch (YFSUserExitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
		Map<String, String> map2 = new HashMap<String, String>();
		Map<String, String> map3 = new HashMap<String, String>();
		Map<String,Integer>map4=new HashMap<String,Integer>();
		final YFCNodeList<YFCElement> TerSelectionRuleLineList = doc
				.getDocumentElement().getElementsByTagName(
						"TerSelectionRuleLine");
		int TerSelectionRuleLineLength = TerSelectionRuleLineList.getLength();
		System.out.println(TerSelectionRuleLineLength);
		for (int o = 0; o < TerSelectionRuleLineLength; o++)

		{

			final YFCNode cNodeSelCRuleLineNode = TerSelectionRuleLineList
					.item(o);

			final YFCElement cNodeSelCRuleLineElem = (YFCElement) cNodeSelCRuleLineNode;
			

			YFCElement TERSCTSelectionRule = cNodeSelCRuleLineElem
					.getChildElement("TERSCTSelectionRule");

			ArrayList<String> valSetOne = new ArrayList<String>();

			valSetOne.add(TERSCTSelectionRule.getAttribute("TerOwnerOrgID"));
			valSetOne.add(TERSCTSelectionRule.getAttribute("TerInstallNode"));
			valSetOne.add(TERSCTSelectionRule.getAttribute("TerItemID"));
			valSetOne.add(TERSCTSelectionRule.getAttribute("TerBuildStatus"));
			valSetOne.add(TERSCTSelectionRule.getAttribute("TerCountryCode"));
			valSetOne.add(TERSCTSelectionRule
					.getAttribute("TerRespProductMfgID"));
			valSetOne.add(TERSCTSelectionRule
					.getAttribute("TerRespProdMfgDivCode"));
			valSetOne.add(TERSCTSelectionRule.getAttribute("TerFromBookYw"));
			valSetOne.add(TERSCTSelectionRule.getAttribute("TerToBookYw"));
			valSetOne.add(TERSCTSelectionRule.getAttribute("TerFromShipDate"));
			valSetOne.add(TERSCTSelectionRule.getAttribute("TerToShipDate"));
			map.put(TERSCTSelectionRule.getAttribute("TerSRKey"), valSetOne);
			map2.put(cNodeSelCRuleLineElem.getAttribute("TerSRLineKey"),
					cNodeSelCRuleLineElem.getAttribute("TerSRKey"));
			map3.put(cNodeSelCRuleLineElem.getAttribute("TerSRLineKey"),
					cNodeSelCRuleLineElem.getAttribute("TerSCTKey"));
			
			
			
			
			map4.put(cNodeSelCRuleLineElem.getAttribute("TerSCTKey"),
						(cNodeSelCRuleLineElem.getChildElement
								("TERSerContTemp").getIntAttribute("TerSCTEffectiveDate")));
			
			
			
			
			

			listx.add((HashMap<?, ?>) map);
			listx.add((HashMap<?, ?>) map2);
			listx.add((HashMap<?, ?>) map3);
			listx.add((HashMap<?, ?>) map4);
		}

		return listx;

	}

}

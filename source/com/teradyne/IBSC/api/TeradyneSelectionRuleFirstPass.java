package com.teradyne.IBSC.api;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfs.japi.YFSException;

public class TeradyneSelectionRuleFirstPass {

	ArrayList<String> validSRKey = new ArrayList<String>();
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public HashMap<String, ArrayList<String>> selectionRuleFirstPass(
			HashMap<String, ArrayList<String>> SRKeyFilters, String sCountry,
			String sBookYW, String sTdShipDate) throws YFSException,
			ParseException

	{
		
		for (Map.Entry<String, ArrayList<String>> entry : SRKeyFilters
				.entrySet()) {
			

			ArrayList<String> value = entry.getValue();

			if(!XmlUtils.isVoid(value.get(6))){
			if ((value.get(6) != null)
					&& !(value.get(6).equalsIgnoreCase(sCountry))) {

				validSRKey.add(entry.getKey());
			}
			}
			if ((!XmlUtils.isVoid(sBookYW))&&(!XmlUtils.isVoid(value.get(7))
					&& ((Integer.parseInt(value.get(7))) > Integer
							.parseInt(sBookYW)))
					||

					((!XmlUtils.isVoid(sBookYW))&&(!XmlUtils.isVoid(value.get(8))
					&& ((Integer.parseInt(value.get(8))) < Integer
							.parseInt(sBookYW))))) {

				validSRKey.add(entry.getKey());

			}
			if (!XmlUtils.isVoid(value.get(9))
					&& !((dateFormat.parse(value.get(9)).before(dateFormat
							.parse(sTdShipDate))))
					||

					(!XmlUtils.isVoid(value.get(10)))
					&& !((dateFormat.parse(value.get(10)).after(dateFormat
							.parse(sTdShipDate))))) {

				validSRKey.add(entry.getKey());

			}

		}

		HashSet<String> uniqueValuesvalidSRKey = new HashSet<String>(validSRKey);
		for (String aString : uniqueValuesvalidSRKey) {
			SRKeyFilters.remove(aString);
		}

		return SRKeyFilters;

	}

}

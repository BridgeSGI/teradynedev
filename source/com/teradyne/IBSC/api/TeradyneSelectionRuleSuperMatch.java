package com.teradyne.IBSC.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;

public class TeradyneSelectionRuleSuperMatch 


{

String sselectionRuleKey=null;
int SelectionRulesScore=0;
public String selectionRuleSuperMatch(HashMap<String, ArrayList<String>> SRKeyFiltersFirstPass, ArrayList<String>OrderLineFilters)
	{
		
		
	
		String sLineOwnerOrg=OrderLineFilters.get(0);
		String sLineInstallNode=OrderLineFilters.get(1);
		String sLineItemID=OrderLineFilters.get(2);
		String sLineIBBuildStatus=OrderLineFilters.get(3);
		String sLinesRespProductMfgID=OrderLineFilters.get(4);
		String sRespProdMfgDivCode=OrderLineFilters.get(5);
		
		ArrayList<String> rejectedSRKey=new ArrayList<String>();
		HashMap<String, Integer> SRKeyScore= new HashMap<String, Integer>();
		int score=0;
		 
		for (String key : SRKeyFiltersFirstPass.keySet())
		{
			Boolean bRejcet=false;
			  score=0;
			  
		
			   if(!bRejcet){
			   if(XmlUtils.isVoid(SRKeyFiltersFirstPass.get(key).get(0)))
			   {
				   
				  
			   }
			 
			 else if(sLineOwnerOrg.startsWith(SRKeyFiltersFirstPass.get(key).get(0)))
			   {
				   score=score+SRKeyFiltersFirstPass.get(key).get(0).length();
				   
				   
			   }
		
			 else {
				 
				 bRejcet=true;
				 rejectedSRKey.add(key);
				 
			 }
			   }
			
			   if(!bRejcet)
			   {
				   
				   if(XmlUtils.isVoid(SRKeyFiltersFirstPass.get(key).get(1)))
				   {
					   
					  
				   }
				 
				 else if(sLineInstallNode.startsWith(SRKeyFiltersFirstPass.get(key).get(1)))
				   {
					   score=score+SRKeyFiltersFirstPass.get(key).get(1).length();
					   
					   
				   }
			
				 else {
					 
					 bRejcet=true;
					 rejectedSRKey.add(key);
					 
				 }
				   
				   
				   
			   }
			   
			   if(!bRejcet)
			   {
				   
				   if(XmlUtils.isVoid(SRKeyFiltersFirstPass.get(key).get(2)))
				   {
					   
					 
				   }
				 
				 else if(sLineItemID.startsWith(SRKeyFiltersFirstPass.get(key).get(2)))
				   {
					   score=score+SRKeyFiltersFirstPass.get(key).get(2).length();
					   
					   
				   }
			
				 else {
					 
					 bRejcet=true;
					 rejectedSRKey.add(key);
					 
				 }
				   
				   
				   
			   }
			   if(!bRejcet)
			   {
				   
				   if(XmlUtils.isVoid(SRKeyFiltersFirstPass.get(key).get(3)))
				   {
					   
					  
				   }
				 
				 else if(sLineIBBuildStatus.equalsIgnoreCase(SRKeyFiltersFirstPass.get(key).get(3)))
				   {
					   score=score+1;
					   
					   
				   }
			
				 else {
					 
					 bRejcet=true;
					 rejectedSRKey.add(key);
					 
				 }
				   
				   
				   
			   }
			   if(!bRejcet)
			   {
				   
				   if(XmlUtils.isVoid(SRKeyFiltersFirstPass.get(key).get(4)))
				   {
					   
					  
				   }
				 
				 else if(sLinesRespProductMfgID.equalsIgnoreCase(SRKeyFiltersFirstPass.get(key).get(4)))
				   {
					   score=score+1;
					   
					   
				   }
			
				 else {
					 
					 bRejcet=true;
					 rejectedSRKey.add(key);
					 
				 }
				   
				   
				   
			   }
			   if(!bRejcet)
			   {
				   
				   if(XmlUtils.isVoid(SRKeyFiltersFirstPass.get(key).get(5)))
				   {
					   
					   
				   }
				 
				 else if(sRespProdMfgDivCode.equalsIgnoreCase(SRKeyFiltersFirstPass.get(key).get(5)))
				   {
					   score=score+1;
					   
					   
				   }
			
				 else {
					 
					 bRejcet=true;
					 rejectedSRKey.add(key);
					 
				 }
				   
				   
				   
			   }
			   
			 
			
			   SRKeyScore.put(key, score);
			}
		
		for (String rejceted : rejectedSRKey) {
			SRKeyScore.remove(rejceted);
		}
   
		
		int maxValueInMap=(Collections.max(SRKeyScore.values())); 
		for (Entry<String, Integer> entry : SRKeyScore.entrySet()) {  // Itrate through hashmap
            if (entry.getValue()==maxValueInMap) {
            	sselectionRuleKey=entry.getKey();
              
                break;
            }
        }

return sselectionRuleKey;
	}




}

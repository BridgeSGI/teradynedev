package com.teradyne.om.event;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.CustomLogCategory;
import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.teradyne.om.util.ExchangeOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;


public class OnSuccessOfReturnCreation {
	public static ArrayList<String> NCLinesList = new ArrayList<String>();
	private static ArrayList<String> nonExpressServices = new ArrayList<String>() {{
		add("TPS"); add("MPS"); add("B20"); add("BPS"); add("SDS"); add("ESWP"); add("SCAL"); }};
	private static String enterpriseCode = " ";
	private static CustomLogCategory log = CustomLogCategory.instance(OnSuccessOfReturnCreation.class);
	public Document onSuccess(YFSEnvironment env, Document inXML) throws ParserConfigurationException, TransformerException, YIFClientCreationException, SAXException, IOException {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
		//System.out.println("InXML in OnSuccessOfReturnCreation"+inDoc.toString());
		log.verbose("InXML in OnSuccessOfReturnCreation", inXML);
		if(inDoc.getDocumentElement().getElementsByTagName(XMLConstants.ORDER_LINE).getLength() == 0){
			return inXML;
		}
		
		ExchangeOrderUtils eoUtils = new ExchangeOrderUtils();
		boolean isDOA = inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_TYPE).equals("DOA")?true:false;
		boolean isPIP = eoUtils.checkIfDerivedParentisPIP(env, inDoc);
		boolean isItemNC = eoUtils.anyNonRepairableConsumableParts(inDoc);
		//String hasDerivedParent = inDoc.getDocumentElement().getAttribute("HasDerivedParent");
		//create an advanced EO if RO type is DOA or SO type is PIP
		
		if(isDOA || isPIP ){
			createAdvancedExchange(env, inDoc, "ORD");
		}else{
			createAdvancedExchange(env, inDoc, "LINE");
		}
		
		//change in status of return order/blanking ship node for non repairable scenario
		if(isItemNC){
			String orderHeaderKey = inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY);
			if(inDoc.getDocumentElement().getAttribute(XMLConstants.STATUS).equals("Created")){
				eoUtils.changeOrderForReturn(env, orderHeaderKey, NCLinesList);
				eoUtils.changeOrderStatusForReturn(env, orderHeaderKey, NCLinesList);
			}
		}
		return inXML;
	}
	

	public void createAdvancedExchange(YFSEnvironment env, YFCDocument inDoc, String conditionStr) throws ParserConfigurationException, TransformerException, YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		YFCDocument eoInDoc = YFCDocument.createDocument(XMLConstants.ORDER);
		YFCElement eoRootEle = eoInDoc.getDocumentElement();
		YFCElement inRootEle = inDoc.getDocumentElement();
		enterpriseCode = inRootEle.getAttribute(XMLConstants.ENTERPRISE_CODE);
		eoRootEle.setAttribute(XMLConstants.DOCUMENT_TYPE, "0001");
		eoRootEle.setAttribute(XMLConstants.ENTERPRISE_CODE, enterpriseCode);
		eoRootEle.setAttribute(XMLConstants.BUYER_ORGANIZATION_CODE, inRootEle.getAttribute(XMLConstants.BUYER_ORGANIZATION_CODE));
		eoRootEle.setAttribute(XMLConstants.SELLER_ORGANIZATION_CODE, inRootEle.getAttribute(XMLConstants.SELLER_ORGANIZATION_CODE));
		eoRootEle.setAttribute("OrderPurpose", "EXCHANGE");
		eoRootEle.setAttribute("ReturnOrderHeaderKeyForExchange", inRootEle.getAttribute(XMLConstants.ORDER_HEADER_KEY));
		if(inRootEle.getChildElement(XMLConstants.EXTN) != null){
			YFCElement eoExtnEle = eoInDoc.importNode(inRootEle.getChildElement(XMLConstants.EXTN), true);
			eoRootEle.appendChild(eoExtnEle);
		}
		eoRootEle.setAttribute("ExchangeType", "ADVANCED");
		copyLinesWithExtnElements(env, inDoc, eoInDoc, conditionStr);
		
		YFCDocument eoOutDoc = SterlingUtil.callAPI(env, Constants.CREATE_ORDER, eoInDoc, "");
		//System.out.println("EoOutDoc"+eoOutDoc.toString());
		log.verbose("eoOutDoc", eoOutDoc.getDocument());
		
	}

	private void copyLinesWithExtnElements(YFSEnvironment env, YFCDocument inDoc, YFCDocument eoInDoc, String conditionStr) throws ParserConfigurationException, TransformerException, RemoteException, YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		YFCNodeList<YFCElement> orderlinesNL = inDoc.getDocumentElement().getElementsByTagName(XMLConstants.ORDER_LINE);
		YFCElement eoOrderLinesEle = eoInDoc.createElement(XMLConstants.ORDER_LINES);
		
		for(Iterator<YFCElement> iter = orderlinesNL.iterator(); iter.hasNext(); ){
			YFCElement orderLineEle = iter.next();
			
			String repairCodeStr = XPathUtil.getXpathAttribute(orderLineEle, "/OrderLine/ItemDetails/Extn/@RepairCode");
			if("N".equals(repairCodeStr) || "C".equals(repairCodeStr)){
				NCLinesList.add(orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY));
			}
			
			YFCElement eoOrderLineEle = importKeyLineAttr(env, orderLineEle, eoInDoc, conditionStr);
			eoOrderLinesEle.appendChild(eoOrderLineEle);
			//System.out.println("EoInDoc"+eoOrderLineEle.toString());
			log.verbose(eoOrderLineEle);
		}	
		eoInDoc.getDocumentElement().appendChild(eoOrderLinesEle);
		
	}

	

	private YFCElement importKeyLineAttr(YFSEnvironment env, YFCElement orderLineEle,
			YFCDocument eoInDoc, String conditionStr) throws ParserConfigurationException, TransformerException, RemoteException, YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		YFCElement eoOrderLineEle = eoInDoc.createElement(XMLConstants.ORDER_LINE);
		eoOrderLineEle.setAttribute(XMLConstants.ORDERED_QTY, orderLineEle.getAttribute(XMLConstants.ORDERED_QTY));
		String serviceTypeStr =  XPathUtil.getXpathAttribute(orderLineEle, "/OrderLine/Extn/@ServiceType");
		String orderLineKeyStr = orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
		YFCElement eoItemEle = eoOrderLineEle.createChild(XMLConstants.ITEM);
		YFCElement itemEle = orderLineEle.getChildElement(XMLConstants.ITEM);
		
		eoItemEle.setAttribute(XMLConstants.ITEM_ID, itemEle.getAttribute(XMLConstants.ITEM_ID));
		//check with Sambit if EO item product class is same as RO item. I think RO will be defective and EO will 
		eoItemEle.setAttribute(XMLConstants.PRODUCT_CLASS, "GOOD");
		eoItemEle.setAttribute(XMLConstants.UNIT_OF_MEASURE, itemEle.getAttribute(XMLConstants.UNIT_OF_MEASURE));
		
		
		if("LINE".equals(conditionStr) && !NCLinesList.contains(orderLineKeyStr) && nonExpressServices.contains(serviceTypeStr)){
			String dateStr = calculateServiceDays(env, serviceTypeStr, enterpriseCode);
			eoOrderLineEle.setAttribute("ReqShipDate", dateStr);
			eoOrderLineEle.setAttribute("ReqDeliveryDate", dateStr);
			eoOrderLineEle.setAttribute("EarliestScheduleDate", dateStr);
		}
		
		if(orderLineEle.getChildElement(XMLConstants.LINE_PRICE_INFO) != null){
			YFCElement eoLinePriceEle = eoInDoc.importNode(orderLineEle.getChildElement(XMLConstants.LINE_PRICE_INFO), true);
			eoOrderLineEle.appendChild(eoLinePriceEle);
		}
		if(orderLineEle.getChildElement(XMLConstants.EXTN) != null){
			YFCElement eoExtnEle = eoInDoc.importNode(orderLineEle.getChildElement(XMLConstants.EXTN), true);
			if(!("LINE".equals(conditionStr) && ("ESWP".equals(serviceTypeStr) || "SCAL".equals(serviceTypeStr)))){
				eoExtnEle.removeAttribute(XMLConstants.SYSTEM_SERIAL_NO);
			}
			eoOrderLineEle.appendChild(eoExtnEle);
		}
		return eoOrderLineEle;
	}




	private String calculateServiceDays(YFSEnvironment env,String itemID, String enterpriseCode) throws RemoteException, YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		YFCDocument itemListIn = YFCDocument.createDocument(XMLConstants.ITEM);
		YFCElement itemRootEle = itemListIn.getDocumentElement();
		itemRootEle.setAttribute(XMLConstants.ITEM_TYPE, "SERVICE");
		itemRootEle.setAttribute(XMLConstants.ITEM_ID, itemID);
		itemRootEle.setAttribute(XMLConstants.CALLING_ORGANIZATION_CODE, enterpriseCode);
		itemRootEle.setAttribute("ItemGroupCode", "PROD");
		
		String itemListTemplate = "<ItemList><Item ItemID=\"\" ><Extn ServiceDays=\"\" /></Item></ItemList>";
		YFCDocument itemListOut = SterlingUtil.callAPI(env, Constants.GET_ITEM_LIST, itemListIn, YFCDocument.parse(itemListTemplate));
		//System.out.println("itemListOut:"+itemListOut.toString());
		log.verbose(itemListOut);
		int serviceDays = itemListOut.getElementsByTagName(XMLConstants.EXTN).item(0).getIntAttribute("ServiceDays", 0);
		GregorianCalendar gc = new GregorianCalendar();
		Date serviceDate = DateTimeUtil.addToDate(gc.getTime(),
				Calendar.DAY_OF_MONTH, serviceDays);
		
		return DateTimeUtil.formatDate(serviceDate, "yyyy-MM-dd'T'HH:mm:ss");
	}

	
	

}

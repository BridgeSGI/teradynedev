package com.teradyne.om.event;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;




import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.CustomLogCategory;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.api.CreateWorkOrder;
import com.teradyne.om.util.ExchangeOrderUtils;
import com.teradyne.om.util.ReturnOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;


public class OnSuccessOnReturnReceipt {

	private String receivingNodeStr = " ";
	private String roOHK = " ";
	private boolean isPIP;
	private boolean isDOA;
	private static String orderListTemplate = "<OrderList><Order OrderHeaderKey=\"\" OrderType=\"\" ><Extn />"
			+ "<OrderLines><OrderLine OrderLineKey=\"\" DerivedFromOrderHeaderKey=\"\" >"
			+ "<ItemDetails ItemID=\"\" ><Extn RespProdRepairDivCode=\"\" RepairModel=\"\" RepairCode=\"\" />"
			+ "</ItemDetails><Item ItemID=\"\" ProductClass=\"\" UnitOfMeasure=\"\" />"
			+ "<Extn /></OrderLine></OrderLines></Order></OrderList>";
	private static CustomLogCategory log = CustomLogCategory.instance(OnSuccessOnReturnReceipt.class);
	public Document onSuccess(YFSEnvironment env, Document inXML) throws Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
		//System.out.println("InDoc :"+inDoc.toString());
		log.verbose("InDoc", inXML);
		updateProcessingStatusForInstallBaseLine(env, inDoc);
		receivingNodeStr = inDoc.getDocumentElement().getAttribute(XMLConstants.RECEIVING_NODE);
		roOHK = XPathUtil.getXpathAttribute(inDoc, "/Receipt/ReceiptLines/ReceiptLine/@OrderHeaderKey");
		if(!isInboundDirectShipAllowed(env, inDoc)){
			String toOHK = createTOForEligibleLines(env, inDoc);
			changeOrderOnRO(env, roOHK, toOHK);
		} else {
			CreateWorkOrder wo = new CreateWorkOrder();
			wo.createWorkOrder(env, inDoc);
		}
		ExchangeOrderUtils eoUtils = new ExchangeOrderUtils();
		eoUtils.changeDatesOnExchange(env, roOHK, inDoc, "RO");
		return null;
	}

	private void updateProcessingStatusForInstallBaseLine(YFSEnvironment env, YFCDocument inDoc) throws Exception {
		System.out.println("Updating Install Base Processing Status");		
		YFCElement receipt = inDoc.getDocumentElement();
		YFCElement receiptLines = receipt.getChildElement(XMLConstants.RECEIPT_LINES);
		YFCIterable<YFCElement> allReceiptLines = receiptLines.getChildren(XMLConstants.RECEIPT_LINE);
		if (XmlUtils.isVoid(allReceiptLines)) {
			for (YFCElement receiptLine : allReceiptLines) {
				System.out.println("Processing receipt line: " + receiptLine);
				String orderLineKey = receiptLine.getAttribute(XMLConstants.ORDER_LINE_KEY);
				System.out.println("OrderLineKey: " + orderLineKey);
				String ibLineKey = getInstallBaseLineKey(env, orderLineKey);
				System.out.println("Install Base Line Key: " + ibLineKey);
				if (!XmlUtils.isVoid(ibLineKey)) {
					String ibHeaderKey = getInstallBaseHeaderKey(env, ibLineKey);
					System.out.println("Install Base Header Key: " + ibHeaderKey);
					if (!XmlUtils.isVoid(ibHeaderKey)) {
						updateProcessingStatusForInstallBase(env, ibHeaderKey, ibLineKey);
					}
				}
			}
		}
	}

	private void updateProcessingStatusForInstallBase(YFSEnvironment env, String ibHeaderKey, String ibLineKey) throws Exception {
		YFCDocument inputDocForChangeOrder = YFCDocument.parse(
				"<Order Override=\"Y\" OrderHeaderKey=\"" + ibHeaderKey + "\">" +
					"<OrderLines>" +
						"<OrderLine OrderLineKey=\"" + ibLineKey + "\">" +
							"<Extn ProcessingStatus=\"R\"/>" +
						"</OrderLine>" +
					"</OrderLines>" +
				"</Order>");
		System.out.println("Input XML for change Order : " + inputDocForChangeOrder);
		SterlingUtil.callAPI(env, Constants.CHANGE_ORDER_API, inputDocForChangeOrder, "");
	}

	private String getInstallBaseHeaderKey(YFSEnvironment env, String ibLineKey) throws Exception {
		YFCDocument template = YFCDocument.parse(
				"<OrderLineList>" +
					"<OrderLine OrderLineKey=\"\" OrderHeaderKey=\"\" />" +
				"</OrderLineList>");
		YFCElement orderLine = callGetOrderLineListAPIUsingOrderLineKey(env, ibLineKey, template);
		if (!XmlUtils.isVoid(orderLine)) {
			return orderLine.getAttribute(XMLConstants.ORDER_HEADER_KEY);
		}
		return null;
	}

	private String getInstallBaseLineKey(YFSEnvironment env, String orderLineKey) throws Exception {
		YFCDocument template = YFCDocument.parse(
				"<OrderLineList>" +
					"<OrderLine OrderLineKey=\"\">" +
						"<Extn IBLineKey=\"\"/>" +
					"</OrderLine>" +
				"</OrderLineList>");
		YFCElement orderLine = callGetOrderLineListAPIUsingOrderLineKey(env, orderLineKey, template);
		if (!XmlUtils.isVoid(orderLine)) {
			YFCElement extn = orderLine.getChildElement(XMLConstants.EXTN);
			if (XmlUtils.isVoid(extn)) {
				return extn.getAttribute(XMLConstants.INSTALL_BASE_LINE_KEY);
			}
		}
		return null;
	}

	private YFCElement callGetOrderLineListAPIUsingOrderLineKey(YFSEnvironment env, String key, YFCDocument template) throws Exception {
		YFCDocument inputDocForGetOrderLineList = YFCDocument.parse("<OrderLine OrderLineKey=\"" + key + "\" />");
		System.out.println("Input XML: " + inputDocForGetOrderLineList);
		System.out.println("Template: " + template);
		YFCDocument outputDocForGetOrderLineList = SterlingUtil.callAPI(env, Constants.GET_ORDER_LINE_LIST, inputDocForGetOrderLineList, template);
		System.out.println("Output XML: " + outputDocForGetOrderLineList);
		YFCElement orderLineList = outputDocForGetOrderLineList.getDocumentElement();
		YFCElement orderLine = orderLineList.getChildElement(XMLConstants.ORDER_LINE);
		return orderLine;
	}

	private void checkPIPOrDOAScenario(YFSEnvironment env,
			YFCDocument orderListOut) throws TransformerException, ParserConfigurationException, RemoteException, YIFClientCreationException {
		// TODO Auto-generated method stub
		ExchangeOrderUtils eoUtils = new ExchangeOrderUtils();
		isPIP = eoUtils.checkIfDerivedParentisPIP(env, orderListOut);
		isDOA = "DOA".equals(XPathUtil.getXpathAttribute(orderListOut, "//Order/@OrderType"))?true:false;
	}

	private String createTOForEligibleLines(YFSEnvironment env, YFCDocument inDoc) throws Exception {
		// TODO Auto-generated method stub
		YFCDocument createOrderIn = YFCDocument.createDocument(XMLConstants.ORDER);
		YFCElement coRootEle = createOrderIn.getDocumentElement();
		coRootEle.setAttribute(XMLConstants.DOCUMENT_TYPE, "0006");
		coRootEle.setAttribute(XMLConstants.ENTERPRISE_CODE, inDoc.getDocumentElement().getAttribute(XMLConstants.ENTERPRISE_CODE));
		coRootEle.setAttribute(XMLConstants.BUYER_ORGANIZATION_CODE, XPathUtil.getXpathAttribute(inDoc, "/Receipt/Shipment/@SellerOrganizationCode"));
		coRootEle.setAttribute(XMLConstants.SELLER_ORGANIZATION_CODE, XPathUtil.getXpathAttribute(inDoc, "/Receipt/Shipment/@SellerOrganizationCode"));
		
		YFCElement referencesEle = coRootEle.createChild("References");
		YFCElement referenceEle = referencesEle.createChild("Reference");
		referenceEle.setAttribute("Name", "RO-OHK");
		referenceEle.setAttribute("Value", roOHK);
		
		YFCNodeList<YFCElement> receiptLinesEle = inDoc.getDocumentElement().getElementsByTagName("ReceiptLine");
		YFCElement orderLinesEle = createOrderIn.createElement(XMLConstants.ORDER_LINES);
		coRootEle.appendChild(orderLinesEle);
		
		HashMap<String, String> orderLinesHM = new HashMap<String, String>();
		for(int i = 0; i < receiptLinesEle.getLength(); i++){
			YFCElement orderLineEle = createOrderIn.createElement(XMLConstants.ORDER_LINE);
			YFCElement receiptLineEle = receiptLinesEle.item(i);
			String orderLineKeyStr = receiptLinesEle.item(i).getAttribute(XMLConstants.ORDER_LINE_KEY);
			orderLineEle.setAttribute(XMLConstants.ORDER_LINE_KEY, orderLineKeyStr);
			orderLineEle.setAttribute(XMLConstants.SHIP_NODE, receivingNodeStr);
			orderLineEle.setAttribute(XMLConstants.ORDERED_QTY, receiptLineEle.getAttribute(XMLConstants.QUANTITY));
			
			orderLinesEle.appendChild(orderLineEle);
			orderLinesHM.put(orderLineKeyStr, " ");
		}
		
		YFCDocument orderListOut = getWorkCenterDetails(env, orderLinesHM, roOHK);
		
		for(YFCElement orderLineEle : orderLinesEle.getChildren()){
			String orderLineKeyStr = orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
			String serviceTypeStr = XPathUtil.getXpathAttribute(orderLineEle, "/OrderLine/Extn/@ServiceType");
			
			orderLineEle.setAttribute(XMLConstants.RECEIVING_NODE, orderLinesHM.get(orderLineKeyStr));
			orderLineEle.removeAttribute(XMLConstants.ORDER_LINE_KEY);
			
			YFCElement extnEle = (YFCElement) XPathUtil.getXpathNode(orderListOut, "//OrderLines/OrderLine[@OrderLineKey='"+orderLineKeyStr+"']/Extn");
			YFCElement coExtnEle = orderLineEle.createChild(XMLConstants.EXTN);
			coExtnEle.setAttribute("OutOfBoxFailure", extnEle.getAttribute("OutOfBoxFailure"));
			coExtnEle.setAttribute("OEMSerialNo", extnEle.getAttribute("OEMSerialNo"));
			coExtnEle.setAttribute("SystemSerialNo", extnEle.getAttribute("SystemSerialNo"));
			orderLineEle.appendChild(coExtnEle);
			
			YFCNode itemNode = XPathUtil.getXpathNode(orderListOut, "//OrderLines/OrderLine[@OrderLineKey='"+orderLineKeyStr+"']/Item");
			YFCNode coItemNode = createOrderIn.importNode(itemNode, false);
			orderLineEle.appendChild(coItemNode);
			
		}
		
		YFCNode orderExtnNode = XPathUtil.getXpathNode(orderListOut, "//Order/Extn");
		YFCNode createOrderExtnNode = createOrderIn.importNode(orderExtnNode, false);
		coRootEle.appendChild(createOrderExtnNode);
		
		//System.out.println("CreateOrderIn :"+createOrderIn.toString());
		log.verbose("createOrderIn", createOrderIn.getDocument());
		YFCDocument createOrderOut = SterlingUtil.callAPI(env, Constants.CREATE_ORDER, createOrderIn, YFCDocument.parse("<Order OrderHeaderKey=\"\" />"));
		
		return createOrderOut.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY);
		
	}
	
	private void changeOrderOnRO(YFSEnvironment env, String roOHK, String toOHK) throws RemoteException, YIFClientCreationException, SAXException, IOException{
		//use References Ele to store the orderheaderkey of TO in RO
		YFCDocument changeOrderIn = YFCDocument.createDocument(XMLConstants.ORDER);
		changeOrderIn.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, roOHK);
		
		YFCElement referencesEle = changeOrderIn.getDocumentElement().createChild("References");
		YFCElement referenceEle = referencesEle.createChild("Reference");
		referenceEle.setAttribute("Name", "TO-OHK");
		referenceEle.setAttribute("Value", toOHK);
		
		//System.out.println("changeOrderIn :"+changeOrderIn.toString());
		log.verbose("changeOrderIn", changeOrderIn.getDocument());
		YFCDocument changeOrderOut = SterlingUtil.callAPI(env, Constants.CHANGE_ORDER_API, changeOrderIn, YFCDocument.parse("<Order OrderHeaderKey=\"\" />"));
		
		
	}

	private YFCDocument getWorkCenterDetails(YFSEnvironment env,
			HashMap<String, String> orderLinesHM, String orderHeaderKey) throws Exception {
		ExchangeOrderUtils eoUtils = new ExchangeOrderUtils();
		YFCDocument orderListOut = eoUtils.getOrderList(env, orderHeaderKey, orderListTemplate);
		checkPIPOrDOAScenario(env, orderListOut);
		YFCNodeList<YFCElement> orderLinesNL = orderListOut.getElementsByTagName(XMLConstants.ORDER_LINE);
		HashMap<String, String> repairModelHM = new HashMap<String, String>();
		HashMap<String, String> pipHM = new HashMap<String, String>();
		HashMap<String, String> sprHM = new HashMap<String, String>();
		ReturnOrderUtils roUtil = new ReturnOrderUtils();
		for(int i = 0; i < orderLinesNL.getLength(); i++){
			YFCElement orderLineEle = orderLinesNL.item(i);
			String olKeyStr = orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
			String sprNameStr = XPathUtil.getXpathAttribute(orderLineEle, "/OrderLine/Extn/@SPRName");
			if(orderLinesHM.containsKey(olKeyStr)){
				if(isPIP){
					String pipDivCodeStr = XPathUtil.getXpathAttribute(orderLineEle, "//ItemDetails/Extn/@RespProdRepairDivCode");
					if(StringUtil.isEmpty(pipDivCodeStr)){
						String systemTypeStr = XPathUtil.getXpathAttribute(orderLineEle, "/OrderLine/Extn/@SystemType");
						pipDivCodeStr = roUtil.fetchDivisionCodeFromSystemType(env, systemTypeStr);
					}
					pipHM.put(pipDivCodeStr, "");
					orderLinesHM.put(olKeyStr, pipDivCodeStr);
					
					
				} else if(!StringUtil.isEmpty(sprNameStr)){
					sprHM.put(sprNameStr, "");
					orderLinesHM.put(olKeyStr, sprNameStr);
				}else {
					String repairModelStr = XPathUtil.getXpathAttribute(orderLineEle, "/OrderLine/ItemDetails/Extn/@RepairModel");
					repairModelHM.put(repairModelStr, "");
					orderLinesHM.put(olKeyStr, repairModelStr);
				}
			}
		}
		if(!repairModelHM.isEmpty()){
			createRepairCenterHashMap(env, repairModelHM);
		}
		if(!pipHM.isEmpty()){
			createPIPCenterHashMap(env, pipHM);
		}
		if(!sprHM.isEmpty()){
			createSPRCenterHashMap(env, sprHM);
		}
		
		for(Iterator<String> olIter = orderLinesHM.keySet().iterator(); olIter.hasNext();){
			String orderLineKey = olIter.next();
			String codeForNodeStr = orderLinesHM.get(orderLineKey);
			if(sprHM.containsKey(codeForNodeStr)){
				orderLinesHM.put(orderLineKey, sprHM.get(codeForNodeStr));
			}else if (pipHM.containsKey(codeForNodeStr)){
				orderLinesHM.put(orderLineKey, pipHM.get(codeForNodeStr));
			}else {
				orderLinesHM.put(orderLineKey, repairModelHM.get(codeForNodeStr));
			}
		}
		
		return orderListOut;
	}
	
	private void createSPRCenterHashMap(YFSEnvironment env,
			HashMap<String, String> sprHM) throws SAXException, IOException, YIFClientCreationException, TransformerException, ParserConfigurationException {
		// TODO Auto-generated method stub
		YFCDocument userListTemplate = YFCDocument.parse("<UserList><User Username=\"\" ><Extn PartReviewNode=\"\" />"
				+ "	</User></UserList>");
		for(Iterator<String> iter = sprHM.keySet().iterator(); iter.hasNext(); ){
			String sprName = iter.next();
			YFCDocument userListIn = YFCDocument.parse("<User Username=\"" +sprName + "\" />");
			log.verbose("userListIn", userListIn.getDocument());
			YFCDocument userListOut = SterlingUtil.callAPI(env, "getUserList", userListIn, userListTemplate);
			log.verbose("userListOut", userListOut.getDocument());
			sprHM.put(sprName, XPathUtil.getXpathAttribute(userListOut, "//Extn/@PartReviewNode"));
		}
		
		
	}

	private void createPIPCenterHashMap(YFSEnvironment env,
			HashMap<String, String> pipHM) throws SAXException, IOException, YIFClientCreationException {
		ReturnOrderUtils roUtil = new ReturnOrderUtils();
		for(Iterator<String> iter = pipHM.keySet().iterator(); iter.hasNext(); ){
			String divCodeStr = iter.next();
			pipHM.put(divCodeStr, roUtil.fetchPIPDestinationCenter(env, divCodeStr));
		}
		
	}

	private void createRepairCenterHashMap(YFSEnvironment env,
			HashMap<String, String> repairModelHM) throws SAXException, IOException, YIFClientCreationException {
		// TODO Auto-generated method stub
		YFCDocument repairCenterListIn = YFCDocument.parse("<TerRepairModel CenterCode=\"" + receivingNodeStr + "\" />");
		log.verbose("repairCenterListIn", repairCenterListIn.getDocument());
		YFCDocument repairCenterListOut = SterlingUtil.callService(env, "GetRepairCenterList", repairCenterListIn, "");
		log.verbose("repairCenterListOut", repairCenterListOut.getDocument());
		for(YFCElement repairModelEle : repairCenterListOut.getDocumentElement().getChildren()){
			if (repairModelHM.containsKey(repairModelEle.getAttribute("RepairModelCode"))) {
				repairModelHM.put(repairModelEle.getAttribute("RepairModelCode"), repairModelEle.getAttribute("RepairCenter"));
			}
		}
		
	}

	private boolean isInboundDirectShipAllowed(YFSEnvironment env, YFCDocument inDoc) throws RemoteException, YIFClientCreationException, SAXException, IOException, TransformerException, ParserConfigurationException {
		// TODO Auto-generated method stub
		YFCDocument customerIn = YFCDocument.createDocument(Constants.CUSTOMER);
		String buyerOrgCodeStr = XPathUtil.getXpathAttribute(inDoc, "//Shipment/@BuyerOrganizationCode");
		if(StringUtil.isEmpty(buyerOrgCodeStr)){
			return false;
		}
		customerIn.getDocumentElement().setAttribute(Constants.CUSTOMER_ID, buyerOrgCodeStr);
		String customerTemplate = "<CustomerList><Customer CustomerID=\"\" ><Extn IsInboundDirectShipAllowed=\"\" /></Customer></CustomerList>";
		//System.out.println("customerIn :"+customerIn.toString());
		YFCDocument customerOut = SterlingUtil.callAPI(env, Constants.GET_CUSTOMER_LIST, customerIn, YFCDocument.parse(customerTemplate));
		//System.out.println("customerOut :"+customerIn.toString());
		return "Y".equals(XPathUtil.getXpathAttribute(customerOut, "/CustomerList/Customer/Extn/@IsInboundDirectShipAllowed"));
	}
}

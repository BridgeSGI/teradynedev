package com.teradyne.om.event;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.CustomLogCategory;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.teradyne.om.api.CreateWorkOrder;
import com.teradyne.om.util.ExchangeOrderUtils;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfs.japi.YFSEnvironment;


public class OnSuccessOfTransferReceipt {
	private static CustomLogCategory log = CustomLogCategory.instance(OnSuccessOfTransferReceipt.class);
	public Document onSuccess(YFSEnvironment env, Document inXML) throws TransformerException, ParserConfigurationException, RemoteException, YIFClientCreationException, SAXException, IOException{
		YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
		//System.out.println("inDoc :"+inDoc.toString());
		log.verbose("input XML", inXML);
		String orderHeaderKey = XPathUtil.getXpathAttribute(inDoc, "//ReceiptLine/@OrderHeaderKey");
		String orderListTemplate = "<OrderList><Order OrderHeaderKey=\"\"><References>"
				+ "<Reference Name=\"\" /></References></Order></OrderList>";
		ExchangeOrderUtils eoUtils = new ExchangeOrderUtils();
		YFCDocument orderListOut= eoUtils.getOrderList(env, orderHeaderKey, orderListTemplate);
		if(XPathUtil.getXpathNode(orderListOut, "//Reference[@Name='RO-OHK']") != null){
			if(XPathUtil.getXpathNode(orderListOut, "//Reference[@Name='Reverse-TO']") != null){
				eoUtils.changeDatesOnExchange(env, XPathUtil.getXpathAttribute(orderListOut, "//Reference[@Name='RO-OHK']/@Value"), inDoc, "TO");
			}else {
				CreateWorkOrder wo = new CreateWorkOrder();
				wo.createWorkOrder(env, inDoc);
			}
		}
		return inXML;
	}
}

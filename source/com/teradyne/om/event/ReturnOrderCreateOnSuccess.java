package com.teradyne.om.event;

import java.util.Map;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.SterlingUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCLocale;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.bridge.sterling.utils.SterlingUtil;
//import com.teradyne.utils.Constants;
//import com.teradyne.utils.XMLConstants;

public class ReturnOrderCreateOnSuccess {

	public void onSuccess(YFSEnvironment env, Document inXML) throws Exception {
		String isIBDirectShipAllowed = "N";
		YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inXML);
		System.out.println("Incoming: " + inYFCDoc);
		YFCElement order = inYFCDoc.getDocumentElement();
		
		YFCElement orderLines = order.getChildElement("OrderLines");
		Map<String, String> allAttr = order.getAttributes();
		YFCElement extnLines = order.getChildElement("Extn");
		//Map<String, String> extnAttr = extnLines.getAttributes();
		
		String sReturnOrderHeaderKey= order.getAttribute("OrderHeaderKey");
		String customer = order.getAttribute("ReceivingNode");
		String orderType= order.getAttribute("OrderType");
		System.out.println("Order Type is " + orderType);
		
		if("DOA".equalsIgnoreCase(orderType)){
			createEOForDOA(env, inYFCDoc);
		}
		else{
			if (!XmlUtils.isVoid(orderLines)) {
				YFCIterable<YFCElement> allOrderLines = orderLines.getChildren("OrderLine");
				for (YFCElement orderLine : allOrderLines) {
					
					YFCElement orderLineClone = cloneElement(orderLine);
					//removeOrderLineKeys(orderLineClone);
					orderLineClone.removeAttribute("OrderLineKey");
	
					YFCElement orderLineExtn = orderLineClone.getChildElement("Extn");
					String serviceType= orderLineExtn.getAttribute("ServiceType");
					
					if(serviceType !=null){
						if ("B20".equalsIgnoreCase(serviceType) || "MPS".equalsIgnoreCase(serviceType) || "BPS".equalsIgnoreCase(serviceType)
						|| "RPD".equalsIgnoreCase(serviceType) ||"EPS".equalsIgnoreCase(serviceType) || "SDS".equalsIgnoreCase(serviceType) || 
						"ECAL".equalsIgnoreCase(serviceType) || "EAR".equalsIgnoreCase(serviceType)){

							String sShipToKey = "";
							String sBillToKey = "";
							
							YFCElement eleShipTo = order.getChildElement("PersonInfoShipTo");
							if(!YFCElement.isVoid(eleShipTo)){
								sShipToKey=eleShipTo.getAttribute("PersonInfoKey");
							}
							
							YFCElement eleBillTo = order.getChildElement("PersonInfoBillTo");
							if(!YFCElement.isVoid(eleBillTo)){
								sBillToKey=eleBillTo.getAttribute("PersonInfoKey");
							}
							
							YFCDocument docEO= YFCDocument.createDocument("Order");
							YFCElement orderEO = docEO.getDocumentElement();
							orderEO.setAttributes(allAttr);
							removeOrderHeaderKey(orderEO);
							orderEO.removeAttribute("OrderNo");
							orderEO.setAttribute("DocumentType", "0001");
							orderEO.setAttribute("IgnoreOrdering", "Y");
							orderEO.setAttribute("ReturnOrderHeaderKeyForExchange", sReturnOrderHeaderKey);
							orderEO.setAttribute("ExchangeType", "ADVANCED");
							orderEO.setAttribute("OrderPurpose", "EXCHANGE");
							orderEO.setAttribute("BillToKey", sBillToKey);
							orderEO.setAttribute("ShipToKey", sShipToKey);
							
							YFCElement orderExtnEO = orderEO.createChild("Extn");
							if (XmlUtils.isVoid(extnLines)) {
								orderExtnEO.setAttributes(extnLines.getAttributes());
							}
							
							YFCElement orderLinesEO = orderEO.createChild("OrderLines");
							orderLinesEO.importNode(orderLineClone);
							
							System.out.println("1. Input XML for creating exchange order: " + docEO);
							
							YIFClientFactory.getInstance().getLocalApi().invoke(env, "createOrder",docEO.getDocument());

						}
					}
				}
			}
		}// end of if DOA or not
		
		
		//creating WO for orderLines if the PDT class is DEFECTIVE
		YFCElement orderLines1 = order.getChildElement("OrderLines");

		YFCIterable<YFCElement> allOrderLines = orderLines.getChildren("OrderLine");
		for (YFCElement orderLine : allOrderLines) {
			YFCElement item = orderLine.getChildElement("Item");
			String productClass = item.getAttribute("ProductClass");
			if (productClass.equals("DEFECTIVE")) {
				

				YFCDocument docWO= YFCDocument.createDocument("Order");
				YFCElement orderWO = docWO.getDocumentElement();
				orderWO.setAttributes(allAttr);
				YFCElement orderExtnEO = orderWO.createChild("Extn");
				if (XmlUtils.isVoid(extnLines)) {
					orderExtnEO.setAttributes(extnLines.getAttributes());
				}
				orderWO.importNode(orderLines1);
				
				YIFClientFactory.getInstance().getLocalApi().executeFlow(env, "Repair_WO", docWO.getDocument());
			}
		
		}
		
		
		YFCDocument getCustomerListInputDoc = YFCDocument.parse("<Customer CustomerID=\"" + customer + "\"/>");
		YFCDocument getCustomerListOutputTemplateDoc = YFCDocument.parse(
				"<CustomerList>" +
					"<Customer CustomerID=\"\">" +
						"<Extn IsInboundDirectShipAllowed=\"\"/>" +
					"</Customer>" +
				"</CustomerList>");
		System.out.println("Calling getCustomerList with input :\n" + getCustomerListInputDoc + "\nand template:\n" + getCustomerListOutputTemplateDoc);
		env.setApiTemplate("getCustomerList",getCustomerListOutputTemplateDoc.getDocument());
		Document getCustomerListOutputDoc = YIFClientFactory.getInstance().getLocalApi().invoke(env, "getCustomerList", getCustomerListInputDoc.getDocument());
		System.out.println("getCustomerList output: " + getCustomerListOutputDoc);
		YFCElement customerList =  YFCDocument.getDocumentFor(getCustomerListOutputDoc).getDocumentElement();
		
		YFCElement customerFromOutput = customerList.getChildElement("Customer");
		if (!XmlUtils.isVoid(customerFromOutput)) {
			YFCElement extn = customerFromOutput.getChildElement("Extn");
			isIBDirectShipAllowed = extn.getAttribute("IsInboundDirectShipAllowed");
		}
		
		if (isIBDirectShipAllowed.equals("N")) {
			createTransferOrder(env, inYFCDoc);
		}

	}

	private void createEOForDOA(YFSEnvironment env, YFCDocument inYFCDoc) throws Exception {
	
		//YFCDocument inDoc = YFCDocument.getDocumentFor((Document) inYFCDoc);
		YFCElement order = inYFCDoc.getDocumentElement();
		
		Map<String, String> allAttr = order.getAttributes();
		
		String sReturnOrderHeaderKey=order.getAttribute("OrderHeaderKey");
		
		String sShipToKey = "";
		String sBillToKey = "";
		
		YFCElement eleShipTo = order.getChildElement("PersonInfoShipTo");
		if(!YFCElement.isVoid(eleShipTo)){
			sShipToKey=eleShipTo.getAttribute("PersonInfoKey");
		}
		
		YFCElement eleBillTo = order.getChildElement("PersonInfoBillTo");
		if(!YFCElement.isVoid(eleBillTo)){
			sBillToKey=eleBillTo.getAttribute("PersonInfoKey");
		}
		
		YFCElement orderLines = order.getChildElement("OrderLines");
		YFCElement orderLinesClone = cloneElement(orderLines);
		removeOrderLineKeys(orderLinesClone);
		
		YFCElement extnLines = order.getChildElement("Extn");
		
		
		YFCDocument docEO= YFCDocument.createDocument("Order");
		YFCElement orderEO = docEO.getDocumentElement();
		//orderEO.importNode(order);
		orderEO.setAttributes(allAttr);
		
		removeOrderHeaderKey(orderEO);
		
		orderEO.setAttribute("DocumentType", "0001");
		orderEO.setAttribute("IgnoreOrdering", "Y");
		orderEO.setAttribute("ReturnOrderHeaderKeyForExchange", sReturnOrderHeaderKey);
		orderEO.setAttribute("ExchangeType", "ADVANCED");
		orderEO.setAttribute("OrderPurpose", "EXCHANGE");
		orderEO.setAttribute("BillToKey", sBillToKey);
		orderEO.setAttribute("ShipToKey", sShipToKey);
		
		if (!XmlUtils.isVoid(extnLines)) {
			orderEO.importNode(extnLines);
		}

		orderEO.importNode(orderLinesClone);
		
		
		System.out.println("2. Input XML for createing exchange order: " + docEO);
		YIFClientFactory.getInstance().getLocalApi().invoke(env, "createOrder",docEO.getDocument());
		
	}
	
	private void removeOrderHeaderKey(YFCElement order) {
		order.removeAttribute("OrderHeaderKey");
	}

	private void removeOrderLineKeys(YFCElement orderLines) {
		YFCIterable<YFCElement> allOrderLines = orderLines.getChildren("OrderLine");
		for (YFCElement orderLine : allOrderLines) {
			orderLine.removeAttribute("OrderLineKey");
		}
	}

	private YFCElement cloneElement(YFCElement e) {
		YFCDocument d = YFCDocument.getDocumentFor(e.toString(), YFCLocale.getDefaultLocale());
		return d.getDocumentElement();
	}

	private void createTransferOrder(YFSEnvironment env, YFCDocument inYFCDoc) throws Exception {
		/*
		<Order ApplyDefaultTemplate="Y" BuyerOrganizationCode="2345-00"
		    DocumentType="0006" DraftOrderFlag="N" EnterpriseCode="CSO"
		    OrderNo="TestTO110" OrderType="BO" SellerOrganizationCode="CSO">
			<OrderLines>
				<OrderLine OrderedQty="1" ReceivingNode="1234-51" ShipNode="FS"
				 DerivedFromOrderHeaderKey="20140910135354278352"
				  DerivedFromOrderLineKey="20140910135451278353">
					<OrderLineTranQuantity TransactionalUOM="EA"/>
					<Item ItemID="I1" ProductClass="DEFECTIVE"/>
				</OrderLine>
			</OrderLines>
		</Order>
		*/
		
		YFCDocument createTOInputDoc = YFCDocument.createDocument(XMLConstants.ORDER);
		YFCElement _order = createTOInputDoc.getDocumentElement();
		_order.setAttribute("ApplyDefaultTemplate", Constants.Y);
		_order.setAttribute(XMLConstants.DOCUMENT_TYPE, Constants.DOCUMENT_TYPE_TO);
		_order.setAttribute(XMLConstants.DRAFT_ORDER_FLAG, Constants.N);
		
		YFCElement order = inYFCDoc.getDocumentElement();
		String orderHeaderKey = order.getAttribute(XMLConstants.ORDER_HEADER_KEY);
		String buyerOrg = order.getAttribute(XMLConstants.RECEIVING_NODE);
		String enterpriseCode = order.getAttribute(XMLConstants.ENTERPRISE_CODE);
		String sellerOrg = order.getAttribute(XMLConstants.SELLER_ORGANIZATION_CODE);
		
//		_order.setAttribute(XMLConstants.BUYER_ORGANIZATION_CODE, buyerOrg);
		_order.setAttribute(XMLConstants.BUYER_ORGANIZATION_CODE, Constants.CSO);
		_order.setAttribute(XMLConstants.ENTERPRISE_CODE, enterpriseCode);
		_order.setAttribute(XMLConstants.SELLER_ORGANIZATION_CODE, sellerOrg);
		
		YFCElement orderLines = order.getChildElement(XMLConstants.ORDER_LINES);
		if (!XmlUtils.isVoid(orderLines)) {
			YFCElement _orderLines = _order.createChild(XMLConstants.ORDER_LINES);
			YFCIterable<YFCElement> allOrderLines = orderLines.getChildren(XMLConstants.ORDER_LINE);
			for (YFCElement orderLine : allOrderLines) {
				YFCElement _orderLine = _orderLines.createChild(XMLConstants.ORDER_LINE);
				String qty = orderLine.getAttribute(XMLConstants.ORDERED_QTY);
				_orderLine.setAttribute(XMLConstants.ORDERED_QTY, qty);
				String shipNode = orderLine.getAttribute(XMLConstants.SHIP_NODE);
				_orderLine.setAttribute(XMLConstants.SHIP_NODE, shipNode);
				String receivingNode = getReceivingNode(env, orderLine, enterpriseCode);
				_orderLine.setAttribute(XMLConstants.RECEIVING_NODE, receivingNode);
				String orderLineKey = orderLine.getAttribute(XMLConstants.ORDER_LINE_KEY);
				_orderLine.setAttribute(XMLConstants.DERIVED_FROM_ORDER_LINE_KEY, orderLineKey);
				_orderLine.setAttribute(XMLConstants.DERIVED_FROM_ORDER_HEADER_KEY, orderHeaderKey);
				YFCElement item = orderLine.getChildElement(XMLConstants.ITEM);
				String itemID = item.getAttribute(XMLConstants.ITEM_ID);
				String pc = item.getAttribute(XMLConstants.PRODUCT_CLASS);
				String uom = item.getAttribute(XMLConstants.UNIT_OF_MEASURE);
				YFCElement _item = _orderLine.createChild(XMLConstants.ITEM);
				_item.setAttribute(XMLConstants.ITEM_ID, itemID);
				_item.setAttribute(XMLConstants.PRODUCT_CLASS, pc);
				YFCElement _orderLineTranQty = _orderLine.createChild(XMLConstants.ORDER_LINE_TRAN_QUANTITY);
				_orderLineTranQty.setAttribute(XMLConstants.TRANSACTIONAL_UOM, uom);
			}
			SterlingUtil.callAPI(env, Constants.CREATE_ORDER, createTOInputDoc, "");
		}
		
	}

	private String getReceivingNode(YFSEnvironment env, YFCElement orderLine, String enterpriseCode) throws Exception {
		YFCElement extn = orderLine.getChildElement(XMLConstants.EXTN);
		if (!XmlUtils.isVoid(extn)) {
			String repairCenterCode = extn.getAttribute(XMLConstants.REPAIR_CENTER_CODE);
			if (!XmlUtils.isVoid(repairCenterCode)) {
				YFCElement item = orderLine.getChildElement(XMLConstants.ITEM);
				String itemId = item.getAttribute(XMLConstants.ITEM_ID);
				String uom = item.getAttribute(XMLConstants.UNIT_OF_MEASURE);
				YFCDocument getItemListInputDoc = YFCDocument.parse("<Item ItemID=\"" + itemId + "\" OrganizationCode=\"" + enterpriseCode + "\" UnitOfMeasure=\"" + uom + "\" />");
				YFCDocument getItemListOutputTemplate = YFCDocument.parse("<ItemList><Item ItemID=\"\"><Extn RepairModel=\"\"/></Item></ItemList>");
				YFCDocument getItemListOuputDoc = SterlingUtil.callAPI(env, Constants.GET_ITEM_LIST, getItemListInputDoc, getItemListOutputTemplate);
				YFCElement itemList = getItemListOuputDoc.getDocumentElement();
				YFCElement itemInOutput = itemList.getChildElement(XMLConstants.ITEM);
				YFCElement itemExtn = itemInOutput.getChildElement(XMLConstants.EXTN);
				if (!XmlUtils.isVoid(itemExtn)) {
					String repairModel = extn.getAttribute(XMLConstants.REPAIR_MODEL);
					if (!XmlUtils.isVoid(repairModel)) {
						YFCDocument getRepairCenterListInputDoc = YFCDocument.parse("<TerRepairModel CenterCode=\"" + repairCenterCode + "\" RepairModelCode=\"" + repairModel +"\"/>");
						YFCDocument getRepairCenterListOutputDoc = SterlingUtil.callService(env, "GetRepairCenterList", getRepairCenterListInputDoc, "");
						YFCElement terRepairModelList = getRepairCenterListOutputDoc.getDocumentElement();
						YFCElement terRepairModel = terRepairModelList.getChildElement(XMLConstants.TER_REPAIR_MODEL);
						if (!XmlUtils.isVoid(terRepairModel)) {
							String repairCenter = terRepairModel.getAttribute(XMLConstants.REPAIR_CENTER);
							return repairCenter;
						}
					}
				}
			}
		}
		return null;
	}

}

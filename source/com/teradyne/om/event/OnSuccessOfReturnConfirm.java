package com.teradyne.om.event;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.CustomLogCategory;
import com.bridge.sterling.utils.StringUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.teradyne.om.util.ExchangeOrderUtils;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;


public class OnSuccessOfReturnConfirm {
	private static CustomLogCategory log = CustomLogCategory.instance(OnSuccessOfReturnConfirm.class);
	public Document onSuccess(YFSEnvironment env, Document inXML) throws ParserConfigurationException, TransformerException, YIFClientCreationException, SAXException, IOException {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
		//System.out.println("InXML in OnSuccessOfReturnConfirm"+inDoc.toString());
		log.verbose("InXML", inXML);
		ExchangeOrderUtils eoUtils = new ExchangeOrderUtils();
		OnSuccessOfReturnCreation rc = new OnSuccessOfReturnCreation();
		
		boolean isDOA = inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_TYPE).equals("DOA")?true:false;
		//boolean isPIP = eoUtils.checkIfDerivedParentisPIP(env, inDoc);
		boolean isItemNC =  eoUtils.anyNonRepairableConsumableParts(inDoc);
		String hasDerivedParent = inDoc.getDocumentElement().getAttribute("HasDerivedParent");
		boolean doesEOExists = inDoc.getDocumentElement().getElementsByTagName("ExchangeOrder").getLength() > 0?true:false;
		//create an advanced EO is RO type is DOA or SO type is PIP.
		if(!"Y".equals(hasDerivedParent) && !doesEOExists){
			if(isDOA){
				rc.createAdvancedExchange(env, inDoc, "ORD");
			}else{
				rc.createAdvancedExchange(env, inDoc, "LINE");
			}
		}
		
		if(isItemNC){
			ArrayList<String> NCLinesList = new ArrayList<String>();
			YFCNodeList<YFCElement> orderlinesNL = inDoc.getDocumentElement().getElementsByTagName(XMLConstants.ORDER_LINE);
			for(Iterator<YFCElement> iter = orderlinesNL.iterator(); iter.hasNext(); ){
				YFCElement orderLineEle = iter.next() ;
				String repairCodeStr = XPathUtil.getXpathAttribute(orderLineEle, "/OrderLine/ItemDetails/Extn/@RepairCode");
				if("N".equals(repairCodeStr) || "C".equals(repairCodeStr)){
					NCLinesList.add(orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY));
				}
			}	
			eoUtils.changeOrderForReturn(env, inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY), NCLinesList);
			eoUtils.changeOrderStatusForReturn(env, inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY), NCLinesList);
		}
		return inXML;
	}


}

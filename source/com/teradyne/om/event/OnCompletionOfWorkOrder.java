package com.teradyne.om.event;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.CustomLogCategory;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.teradyne.om.util.ExchangeOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfs.japi.YFSEnvironment;


public class OnCompletionOfWorkOrder {
	private static String orderListTemplate = "<OrderList><Order OrderHeaderKey=\"\" SellerOrganizationCode=\"\" >"
			+ "<OrderLines><OrderLine OrderLineKey=\"\" ShipNode=\"\" ReceivingNode=\"\" >"
			+ "<Item ItemID=\"\" UnitOfMeasure=\"\" /><Extn /></OrderLine>"
			+ "</OrderLines></Order></OrderList>";
	private static CustomLogCategory log = CustomLogCategory.instance(OnCompletionOfWorkOrder.class);
	public Document onSuccess(YFSEnvironment env, Document inXML) throws TransformerException, ParserConfigurationException, RemoteException, YIFClientCreationException, SAXException, IOException{
		YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
		YFCElement inRootEle = inDoc.getDocumentElement();
		//System.out.println("inDoc :"+inDoc.toString());
		log.verbose("inDoc", inXML);
		ExchangeOrderUtils eoUtils = new ExchangeOrderUtils();
		
		String toOHK = inRootEle.getAttribute("Segment");
		String roOHK = XPathUtil.getXpathAttribute(inDoc, "//Order/@OrderHeaderKey");
		if(toOHK.equals(roOHK)){
			//inboundDirectShipAllowed flag is Y.
			eoUtils.changeDatesOnExchange(env, roOHK, inDoc, "WO");
			return inXML;
		}
		
		String enterpriseCodeStr = inRootEle.getAttribute("EnterpriseCode");
		String itemIDStr = inRootEle.getAttribute(XMLConstants.ITEM_ID);
		String pcStr = inRootEle.getAttribute(XMLConstants.PRODUCT_CLASS);
		String uomStr = inRootEle.getAttribute("Uom");
		
		YFCDocument orderListOut = eoUtils.getOrderList(env, toOHK, orderListTemplate);
		//System.out.println("orderListOut :"+orderListOut.toString());
		log.verbose("orderListOut", orderListOut.getDocument());
		
		YFCDocument transferOrderIn = YFCDocument.createDocument(XMLConstants.ORDER);
		YFCElement toRootEle = transferOrderIn.getDocumentElement();
		toRootEle.setAttribute(XMLConstants.DOCUMENT_TYPE, "0006");
		toRootEle.setAttribute(XMLConstants.ENTERPRISE_CODE, enterpriseCodeStr);
		toRootEle.setAttribute(XMLConstants.SELLER_ORGANIZATION_CODE, XPathUtil.getXpathAttribute(orderListOut, "//Order/@SellerOrganizationCode"));
		toRootEle.setAttribute(XMLConstants.BUYER_ORGANIZATION_CODE, XPathUtil.getXpathAttribute(orderListOut, "//Order/@SellerOrganizationCode"));
		
		YFCElement referencesEle = toRootEle.createChild("References"); 
		YFCElement referenceEle = transferOrderIn.createElement("Reference");
		referenceEle.setAttribute("Name", "RO-OHK");
		referenceEle.setAttribute("Value", roOHK);
		referencesEle.appendChild(referenceEle);
		
		referenceEle = transferOrderIn.createElement("Reference");
		referenceEle.setAttribute("Name", "Reverse-TO");
		referenceEle.setAttribute("Value", "Y");
		referencesEle.appendChild(referenceEle);
		
		YFCElement orderLinesEle = toRootEle.createChild(XMLConstants.ORDER_LINES);
		YFCElement orderLineEle = orderLinesEle.createChild(XMLConstants.ORDER_LINE);
		
		YFCElement toOLEle = (YFCElement) XPathUtil.getXpathNode(orderListOut, "//OrderLine[Item/@ItemID='"+itemIDStr+"' and Item/@UnitOfMeasure='"+uomStr+"']");
		orderLineEle.setAttribute(XMLConstants.ORDERED_QTY, inRootEle.getAttribute("QuantityCompleted"));
		orderLineEle.setAttribute(XMLConstants.RECEIVING_NODE, toOLEle.getAttribute(XMLConstants.SHIP_NODE));
		orderLineEle.setAttribute(XMLConstants.SHIP_NODE, toOLEle.getAttribute(XMLConstants.RECEIVING_NODE));
		
		YFCElement itemEle = orderLineEle.createChild(XMLConstants.ITEM);
		itemEle.setAttribute(XMLConstants.ITEM_ID, itemIDStr);
		itemEle.setAttribute(XMLConstants.UNIT_OF_MEASURE, uomStr);
		itemEle.setAttribute(XMLConstants.PRODUCT_CLASS, pcStr);
		
		YFCNode extnNode = transferOrderIn.importNode(XPathUtil.getXpathNode(toOLEle, "//Extn"), false);
		orderLineEle.appendChild(extnNode);
		
		//System.out.println("transferOrderIn :"+transferOrderIn.toString());
		log.verbose("transferOrderIn", transferOrderIn.getDocument());
		YFCDocument transferOrderOut = SterlingUtil.callAPI(env, Constants.CREATE_ORDER, transferOrderIn, YFCDocument.parse("<Order OrderHeaderKey=\"\" />"));
		//System.out.println("transferOrderOut :"+transferOrderOut.toString());
		log.verbose("transferOrderOut", transferOrderOut.getDocument());
		
		return inXML;
	}
	
	}

package com.teradyne.om.api;

import java.rmi.RemoteException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class OfflineShipment {

  private static YFCLogCategory _cat = YFCLogCategory.instance("com.yantra.CustomCode");
  private ConfirmSalesOrder confirmSalesOrder = new ConfirmSalesOrder();
  private SalesOrderUtils orderUtils = new SalesOrderUtils();

  public Document offlineShipment(YFSEnvironment env, Document inDoc)
      throws YIFClientCreationException, Exception {

    _cat.debug("Offline Shipment - begin");
    if (_cat.isVerboseEnabled()) {
      _cat.verbose("InputDoc:" + XmlUtils.getString(inDoc));
    }

    YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
    Document orderDoc = orderModificationForOfflineOrder(env, inDoc);
    YFCElement yfcOrderEle = YFCDocument.getDocumentFor(orderDoc).getDocumentElement();

    if (_cat.isVerboseEnabled()) {
      _cat.verbose("Before stamping Ship Node in Order:" + yfcOrderEle);
    }

    String shipNode = getDummyShipNode(env, yfcOrderEle);
    validateOfflineOrder(shipNode, yfcOrderEle);
    confirmSalesOrder.confirmOrder(env, inDoc);
    scheduleOrder(env, yfcOrderEle);
    releaseOrder(env, yfcOrderEle);
    Document outDoc = createShipment(env, inputDoc);
    Document confirmShip = confirmShipment(env, outDoc);
    return confirmShip;

  }

  private String getDummyShipNode(YFSEnvironment env, YFCElement element)
      throws YIFClientCreationException, Exception {

    String shipNode = null;
    String input =
        "<Organization EnterpriseCode=\"" + element.getAttribute(XMLConstants.ENTERPRISE_CODE)
            + "\" IsNode=\"Y\"><Node InventoryTracked=\"N\" /></Organization >";
    String template =
        "<OrganizationList><Organization OrganizationCode=\"\"><Node InventoryTracked=\"\" Inventorytype=\"\" /></Organization ></OrganizationList>";

    YFCDocument inputDoc = YFCDocument.getDocumentFor(input);
    YFCDocument templateDoc = YFCDocument.getDocumentFor(template);

    Document outputDoc =
        callApi(env, inputDoc.getDocument(), templateDoc.getDocument(),
            Constants.API_GET_ORGANIZATION_LIST);

    if (!XmlUtils.isVoid(outputDoc)) {

      YFCDocument document = YFCDocument.getDocumentFor(outputDoc);
      YFCElement outputEle = document.getDocumentElement();

      YFCNodeList<YFCElement> list = outputEle.getElementsByTagName(XMLConstants.ORGANIZATION);

      if (list.getLength() != 0) {
        YFCElement organizationEle = list.item(0);
        shipNode = organizationEle.getAttribute(XMLConstants.ORGANIZATION_CODE);
      }

    }

    if (XmlUtils.isVoid(shipNode)) {
      YFSException ex = new YFSException();
      ex.setErrorDescription(Constants.OFFLINE_SHIP_NODE_ERROR
          + element.getAttribute(XMLConstants.ENTERPRISE_CODE));
      throw ex;
    } else {
      _cat.debug("Ship Node:" + shipNode);
      return shipNode;
    }

  }

  private Document orderModificationForOfflineOrder(YFSEnvironment env, Document inDoc)
      throws YIFClientCreationException, Exception {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInEle = yfcInDoc.getDocumentElement();

    String input = "<Order OrderHeaderKey=\"" + yfcInEle.getAttribute("OrderHeaderKey") + "\" />";
    String strTemplate =
        "<OrderList><Order DocumentType=\"\" OrderType=\"\" OrderHeaderKey=\"\" EnterpriseCode=\"\" ReceivingNode=\"\" HoldFlag=\"\" >"
            + "<OrderHoldTypes TotalNumberOfRecords=\"\">"
            + "<OrderHoldType HoldType=\"\"/></OrderHoldTypes></Order></OrderList>";

    YFCDocument inputDocForGetOrderList = YFCDocument.getDocumentFor(input);
    YFCDocument templateDocForGetOrderList = YFCDocument.getDocumentFor(strTemplate);
    
    Document output = getOrder(env, inputDocForGetOrderList,templateDocForGetOrderList);

    YFCDocument template = YFCDocument.getDocumentFor("<Order OrderHeaderKey=\"\" />");
    if (!XmlUtils.isVoid(output)) {
      String strOrderType = yfcInEle.getAttribute(XMLConstants.ORDER_TYPE);
     
      YFCDocument yfcOutput = YFCDocument.getDocumentFor(output);
      YFCElement yfcOutputEle = yfcOutput.getDocumentElement();
      if (!(XMLConstants.OFFLINE_ORDER_TYPE).equalsIgnoreCase(strOrderType)) {
        yfcOutputEle.setAttribute(XMLConstants.ORDER_TYPE, XMLConstants.OFFLINE_ORDER_TYPE);
      }


      if ((Constants.Y).equalsIgnoreCase(yfcOutputEle.getAttribute(XMLConstants.HOLD_FLAG))) {
        yfcOutputEle.removeAttribute(XMLConstants.HOLD_FLAG);
        
        YFCElement holdTypesDoc = yfcOutputEle.getChildElement(XMLConstants.ORDER_HOLD_TYPES);
        if (!XmlUtils.isVoid(holdTypesDoc)) {

          YFCIterable<YFCElement> list = holdTypesDoc.getChildren(XMLConstants.ORDER_HOLD_TYPE);
          for (YFCElement orderHoldType : list) {
            orderHoldType.setAttribute(XMLConstants.STATUS, "1300");
          }
        }

      }
    }
    orderUtils.callApi(env, output, template.getDocument(), Constants.CHANGE_ORDER_API,
        Constants.TRUE);
    return inDoc;
  }

  private Document getOrder(YFSEnvironment env, YFCDocument inDoc,YFCDocument templateDoc) throws YFSException,
      RemoteException, YIFClientCreationException {

    /*String strTemplate =
        "<OrderList><Order DocumentType=\"\" OrderType=\"\" OrderHeaderKey=\"\" EnterpriseCode=\"\" ReceivingNode=\"\" >"
            + " <OrderLines><OrderLine ShipNode=\"\" ><ItemDetails ItemKey=\"\" />"
            + "<Extn ServiceType=\"\" /></OrderLine></OrderLines><OrderStatuses>"
            + "<OrderStatus OrderReleaseKey=\"\" Status=\"\" /></OrderStatuses><OrderHoldTypes TotalNumberOfRecords=\"\">"
            + "<OrderHoldType HoldType=\"\"/></OrderHoldTypes></Order></OrderList>";
*/
    
    Document outputDoc =
        callApi(env, inDoc.getDocument(), templateDoc.getDocument(), Constants.GET_ORDER_LIST);

    YFCDocument yfcOrderDoc = YFCDocument.getDocumentFor(outputDoc);
    YFCElement yfcOrderEle = yfcOrderDoc.getDocumentElement();

    if (!XmlUtils.isVoid(yfcOrderEle)) {

      if (yfcOrderEle.hasChildNodes()) {
        YFCElement orderEle = yfcOrderEle.getFirstChildElement();
        YFCDocument orderDoc = YFCDocument.getDocumentFor(orderEle.getString());
        return orderDoc.getDocument();
      }
    }
    return null;
  }

  // I am not take care of Order header ship node... test it later
  private void validateOfflineOrder(String shipNode, YFCElement element) {

    _cat.debug("Validating Offline Order");
    YFCElement orderlinesEle = element.getChildElement("OrderLines");
    if (!XmlUtils.isVoid(orderlinesEle)) {
      YFCIterable<YFCElement> orderLineList = orderlinesEle.getChildren("OrderLine");

      for (YFCElement orderLine : orderLineList) {

        YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);

        if (XmlUtils.isVoid(orderLine.getAttribute(XMLConstants.SHIP_NODE))) {
          YFSException ex = new YFSException();
          ex.setErrorCode("TDYOM0002");
          ex.setErrorDescription("For offline orders, Order line should have Ship Node. It can't be empty");
          throw ex;
        } else if (!XmlUtils.isVoid(extnEle)
            && !Constants.USL_SERVICE_TYPE.equalsIgnoreCase(extnEle
                .getAttribute(XMLConstants.SERVICE_TYPE))
            && !shipNode.equalsIgnoreCase(orderLine.getAttribute(XMLConstants.SHIP_NODE))) {
          YFSException ex = new YFSException();
          ex.setErrorCode("TDYOM0001");
          ex.setErrorDescription("Except USL Service Type, All offline order line should have dummy Ship Node "
              + shipNode + " for enterprise " + element.getAttribute(XMLConstants.ENTERPRISE_CODE));
          _cat.debug(ex.getErrorCode() + " " + ex.getErrorDescription());
          throw ex;
        }
      }
    }

  }


  private Document scheduleOrder(YFSEnvironment env, YFCElement element)
      throws YIFClientCreationException, Exception {

    _cat.debug("Scheduling Order");
    String strInput =
        "<ScheduleOrder CheckInventory=\"N\" OrderHeaderKey=\""
            + element.getAttribute("OrderHeaderKey") + "\" />";
    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);

    Document outputDoc = callApi(env, inputDoc.getDocument(), null, Constants.SCHEDULEORDER_API);

    return outputDoc;

  }

  private Document releaseOrder(YFSEnvironment env, YFCElement element)
      throws YIFClientCreationException, Exception {

    _cat.debug("Releasing Order");
    String strInput =
        "<ReleaseOrder CheckInventory=\"N\" OrderHeaderKey=\""
            + element.getAttribute("OrderHeaderKey") + "\" />";
    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);

    Document outputDoc = callApi(env, inputDoc.getDocument(), null, Constants.RELEASEORDER_API);

    return outputDoc;
  }



  private Document createShipment(YFSEnvironment env, YFCDocument inDoc)
      throws YIFClientCreationException, Exception {

    _cat.debug("Creating Shipment");

    YFCElement element = inDoc.getDocumentElement();
    String input =
        "<Order OrderHeaderKey=\"" + element.getAttribute("OrderHeaderKey")
            + "\" DraftOrderFlag=\"N\" />";
    String strTemplate =
        "<OrderList><Order DocumentType=\"\" OrderType=\"\" OrderHeaderKey=\"\" EnterpriseCode=\"\" ReceivingNode=\"\" >"
            + " <OrderLines><OrderLine ShipNode=\"\" ><ItemDetails ItemKey=\"\" />"
            + "<Extn ServiceType=\"\" /></OrderLine></OrderLines><OrderStatuses>"
            + "<OrderStatus OrderReleaseKey=\"\" Status=\"\" /></OrderStatuses><OrderHoldTypes TotalNumberOfRecords=\"\">"
            + "<OrderHoldType HoldType=\"\"/></OrderHoldTypes></Order></OrderList>";

    YFCDocument inputDocForGetOrder = YFCDocument.getDocumentFor(input);
    YFCDocument templateDocForGetOrder = YFCDocument.getDocumentFor(strTemplate);
    Document orderDoc = getOrder(env, inputDocForGetOrder,templateDocForGetOrder);

    YFCDocument inputDoc = prepareInputDocForCreateShipment(orderDoc);
    Document outputDoc = callApi(env, inputDoc.getDocument(), null, Constants.CREATESHIPMENT_API);

    return outputDoc;
  }

  private Document confirmShipment(YFSEnvironment env, Document inDoc)
      throws YIFClientCreationException, Exception {

    _cat.debug("Confirm Shipment");
    Element element = inDoc.getDocumentElement();
    String strInput = "<Shipment ShipmentKey=\"" + element.getAttribute("ShipmentKey") + "\" />";
    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);

    Document outputDoc = callApi(env, inputDoc.getDocument(), null, Constants.CONFIRMSHIPMENT_API);
    return outputDoc;
  }

  private YFCDocument prepareInputDocForCreateShipment(Document inputDoc) {

    YFCDocument yfcInputDoc = YFCDocument.getDocumentFor(inputDoc);
    YFCElement yfcElement = yfcInputDoc.getDocumentElement();
   
    YFCDocument makeInput =
        YFCDocument.getDocumentFor("<Shipment DocumentType=\""
            + yfcElement.getAttribute("DocumentType") + "\" EnterpriseCode=\""
            + yfcElement.getAttribute("EnterpriseCode") + "\" OrderHeaderKey=\""
            + yfcElement.getAttribute("OrderHeaderKey")
            + "\" IsSingleOrder=\"Y\" OrderAvailableOnSystem=\"Y\" ReceivingNode=\""
            + yfcElement.getAttribute("ReceivingNode") + "\" ></Shipment>");
    YFCElement makeIPele = makeInput.getDocumentElement();
    YFCElement orderReleases = makeIPele.createChild(XMLConstants.ORDER_RELEASES);
    YFCNodeList<YFCElement> orderStatusEleList =
        yfcElement.getElementsByTagName(XMLConstants.ORDER_STATUS);

    for (int i = 0; i < orderStatusEleList.getLength(); i++) {
      YFCElement orderStatus = orderStatusEleList.item(i);

      if (orderStatus.getAttribute(XMLConstants.STATUS).startsWith("3200")) {

        YFCElement childEle = orderReleases.createChild(XMLConstants.ORDER_RELEASE);
        childEle.setAttribute(XMLConstants.ORDER_HEADER_KEY,
            yfcElement.getAttribute(XMLConstants.ORDER_HEADER_KEY));
        childEle.setAttribute(XMLConstants.ORDER_RELEASE_KEY,
            orderStatus.getAttribute(XMLConstants.ORDER_RELEASE_KEY));

        YFCElement orderChild = childEle.createChild(XMLConstants.ORDER);
        orderChild.setAttribute(XMLConstants.DOCUMENT_TYPE,
            yfcElement.getAttribute(XMLConstants.DOCUMENT_TYPE));
        orderChild.setAttribute(XMLConstants.ENTERPRISE_CODE,
            yfcElement.getAttribute(XMLConstants.ENTERPRISE_CODE));
        orderChild.setAttribute(XMLConstants.ORDER_HEADER_KEY,
            yfcElement.getAttribute(XMLConstants.ORDER_HEADER_KEY));
      }
    }
    _cat.debug("Prepared input document for create Shipmet");
    return makeInput;
  }

  private Document callApi(YFSEnvironment env, Document inDoc, Document template, String apiName)
      throws YIFClientCreationException, YFSException, RemoteException {

    YIFApi api = YIFClientFactory.getInstance().getApi();

    if (!XmlUtils.isVoid(template)) {
      env.setApiTemplate(apiName, template);
    }

    Document outputDoc = api.invoke(env, apiName, inDoc);

    return outputDoc;
  }
}

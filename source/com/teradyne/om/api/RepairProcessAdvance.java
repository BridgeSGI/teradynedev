package com.teradyne.om.api;

import java.rmi.RemoteException;

import org.w3c.dom.Document;

import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;


public class RepairProcessAdvance {

	Document outDoc=null;
	
	public void repairProcessAdvance(YFSEnvironment env,Document inDoc) {
		
		String sReturnOrderHeaderKey="";
		String sOrderLineKey="";
		String sServiceType="";
		String sEnterpriseCode="";
		String sOrderedQty="";
		String sShipNode="";
		String sItemID="";
		String sPdtClass="";
		String sUOM="";
		String sBuyerOrg="";
		String sSerialNo="";
		String sShipToKey="";
		String sBillToKey="";
		Boolean invokeEO=false;
		String sCreateOrderApi="createOrder";
		YFCDocument inputOrder=null;
		//YFCDocument tempOrder=null;
		//Document outputOrder=null;
		try{
			
			YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleInput = inYFCDoc.getDocumentElement();
			
			sReturnOrderHeaderKey= eleInput.getAttribute("OrderHeaderKey");
			sEnterpriseCode=eleInput.getAttribute("EnterpriseCode");
			sBuyerOrg=eleInput.getAttribute("BuyerOrganizationCode");
			System.out.println("In Doc --------"+ inYFCDoc);
			
			YFCElement eleShipTo = eleInput.getChildElement("PersonInfoShipTo");
			if(!YFCElement.isVoid(eleShipTo)){
				sShipToKey=eleShipTo.getAttribute("PersonInfoKey");
			}
			
			YFCElement eleBillTo = eleInput.getChildElement("PersonInfoBillTo");
			if(!YFCElement.isVoid(eleBillTo)){
				sBillToKey=eleBillTo.getAttribute("PersonInfoKey");
			}
			
			
			
			/*inputOrder = YFCDocument.getDocumentFor("<Order BillToKey=\"\" ShipToKey=\"\" DraftOrderFlag=\"\" BuyerOrganizationCode=\"\" EnterpriseCode=\"\" DocumentType=\"\" IgnoreOrdering=\"\" ReturnOrderHeaderKeyForExchange=\"\" ExchangeType=\"\" OrderPurpose=\"\" >" +
					"<OrderLines><OrderLine ShipNode=\"\" OrderedQty=\"\" SerialNo=\"\"> <Item ItemID=\"\" ProductClass=\"\" UnitOfMeasure=\"\" /></OrderLine></OrderLines></Order>");
			*/
			inputOrder = YFCDocument.createDocument("Order");
			YFCElement orderElement = inputOrder.getDocumentElement();
			
			
			orderElement.setAttribute("EnterpriseCode", sEnterpriseCode);
			orderElement.setAttribute("BuyerOrganizationCode", sBuyerOrg);
			orderElement.setAttribute("BillToKey", sBillToKey);
			orderElement.setAttribute("ShipToKey", sShipToKey);
			orderElement.setAttribute("DocumentType", "0001");
			orderElement.setAttribute("IgnoreOrdering", "Y");
			orderElement.setAttribute("ReturnOrderHeaderKeyForExchange", sReturnOrderHeaderKey);
			orderElement.setAttribute("ExchangeType", "ADVANCED");
			orderElement.setAttribute("OrderPurpose", "EXCHANGE");
			//orderElement.setAttribute("DraftOrderFlag", "Y");
			
			YFCElement orderLinesEle = orderElement.createChild("OrderLines");
		
			

			YFCElement eleOrderLines = eleInput.getChildElement("OrderLines");
			if(!YFCElement.isVoid(eleOrderLines)){
				
				YFCIterable<YFCElement> allInputline = eleOrderLines.getChildren();
				for (YFCElement eleOrderLine : allInputline){
				
				YFCElement orderLineEle = orderLinesEle.createChild("OrderLine");
				
				if (!YFCElement.isVoid(eleOrderLine)){
					sOrderLineKey= eleOrderLine.getAttribute("OrderLineKey");
					sShipNode=eleOrderLine.getAttribute("ShipNode");
					sOrderedQty=eleOrderLine.getAttribute("OrderedQty");
					sSerialNo=eleOrderLine.getAttribute("SerialNo");
					
					YFCElement eleExtnOrderLines = eleOrderLine.getChildElement("Extn");
					if(!YFCElement.isVoid(eleExtnOrderLines)){
						sServiceType= eleExtnOrderLines.getAttribute("ServiceType");
					}
					
					if(sServiceType.isEmpty()){
					//Calling the function to get the servicetype
						if(!YFCElement.isVoid(sOrderLineKey)){
							sServiceType = getServiceType(sOrderLineKey,env);
						}
					}
					
				}
				
				YFCElement eleItemLine = eleOrderLine.getChildElement("Item");
				
				if(!YFCElement.isVoid(eleItemLine)){
					sItemID= eleItemLine.getAttribute("ItemID");
					sPdtClass= eleItemLine.getAttribute("ProductClass");
					sUOM= eleItemLine.getAttribute("UnitOfMeasure");
					
				}
				
				
				if(!sServiceType.isEmpty() && sServiceType !=null){
				if("B20".equalsIgnoreCase(sServiceType) || "MPS".equalsIgnoreCase(sServiceType) || "BPS".equalsIgnoreCase(sServiceType)
						|| "RPD".equalsIgnoreCase(sServiceType) ||"EPS".equalsIgnoreCase(sServiceType) || "SDS".equalsIgnoreCase(sServiceType) || 
						"ECAL".equalsIgnoreCase(sServiceType) || "EAR".equalsIgnoreCase(sServiceType) ){
				
				//orderLineEle.setAttribute("ShipNode", sShipNode); Ship From Blank Decide by Sourcing Rule
				orderLineEle.setAttribute("OrderedQty", sOrderedQty);
				if ("ECAL".equalsIgnoreCase(sServiceType)){
				orderLineEle.setAttribute("SerialNo", sSerialNo);
				}
				YFCElement itemEle = orderLineEle.createChild("Item");
				itemEle.setAttribute("ItemID", sItemID);
				itemEle.setAttribute("ProductClass", sPdtClass);
				itemEle.setAttribute("UnitOfMeasure", sUOM);
				orderLinesEle.appendChild(orderLineEle);
				System.out.println("adding the orderline");
				
				orderLineEle.setAttribute(XMLConstants.DERIVED_FROM_ORDER_HEADER_KEY, sReturnOrderHeaderKey);
				orderLineEle.setAttribute(XMLConstants.DERIVED_FROM_ORDER_LINE_KEY, sOrderLineKey);
				
				}
				invokeEO= true;
				}
				
			}//end of for orderlines
			}
			
				System.out.println("Input Order for exchange ======" +inputOrder  );
				
				if(invokeEO){
				
				YIFClientFactory.getInstance().getLocalApi().invoke(env, sCreateOrderApi,inputOrder.getDocument());
				env.clearApiTemplate(sCreateOrderApi);
				}
			
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("######### Exception Caught : " + e.toString());
		}
		
		
		
		
	}
	public String getServiceType(String ordLineKey,YFSEnvironment e1) throws YFSException, RemoteException, YIFClientCreationException{
		String sevType="";
		String sSalesOrdLineKey="";
		String sGetOrderLineDetailApi = "getOrderLineDetails";
		YFCDocument ordDet=null;
		YFCDocument salesOrdLine=null;
		YFCDocument tempOrdDet=null;
		YFCDocument tempSalesOrdLine=null;
		Document outputOrdDet=null;
		Document outSalDet=null;
		
		
		/*From return orderlinekey getting the salesorderlinekey. 
		 
		 */
		ordDet = YFCDocument.getDocumentFor("<OrderLineDetail OrderLineKey=\"\"/>");
		ordDet.getDocumentElement().setAttribute("OrderLineKey", ordLineKey);
		System.out.println("#####ordDet"	+ ordDet);
		tempOrdDet = YFCDocument.getDocumentFor("<OrderLine DerivedFromOrderLineKey=\"\" />");
		e1.setApiTemplate(sGetOrderLineDetailApi,tempOrdDet.getDocument());
		outputOrdDet = YIFClientFactory.getInstance().getLocalApi().invoke(e1, sGetOrderLineDetailApi,ordDet.getDocument());
		
		YFCElement outOrderLineDet= YFCDocument.getDocumentFor(outputOrdDet).getDocumentElement();
		if (!YFCElement.isVoid(outOrderLineDet)) {
			System.out.println("###########"+ outputOrdDet);
			sSalesOrdLineKey = outOrderLineDet.getAttribute("DerivedFromOrderLineKey");
			System.out.println("Sales order line key----" +sSalesOrdLineKey );

		}
		/* From the Sales OrderLinekey getting Order_Line.Extn_Service_Type field value*/
		if(sSalesOrdLineKey!=null){
			salesOrdLine = YFCDocument.getDocumentFor("<OrderLineDetail OrderLineKey=\"\"/>");
			salesOrdLine.getDocumentElement().setAttribute("OrderLineKey", sSalesOrdLineKey);
			tempSalesOrdLine = YFCDocument.getDocumentFor("<OrderLine OrderLineKey=\"\"><Extn ServiceType=\"\"/> </OrderLine>");
			e1.setApiTemplate(sGetOrderLineDetailApi,tempSalesOrdLine.getDocument());
			outSalDet = YIFClientFactory.getInstance().getLocalApi().invoke(e1, sGetOrderLineDetailApi,salesOrdLine.getDocument());
			
			YFCElement outSalOrdLineDet= YFCDocument.getDocumentFor(outSalDet).getDocumentElement();
			if (!YFCElement.isVoid(outSalOrdLineDet)){
				
				YFCElement eleExtn = outSalOrdLineDet.getChildElement("Extn");
				if(!YFCElement.isVoid(eleExtn)){
					sevType= eleExtn.getAttribute("ServiceType");
					System.out.println("Sales order ServiceType----" +sevType );
				}
				
			}
		}
		
		return sevType;
	}
	
}
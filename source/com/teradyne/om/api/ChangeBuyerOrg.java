package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
//import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;


public class ChangeBuyerOrg {

public Document changeBuyerOrg(YFSEnvironment env, Document inDoc )
{
	try{
	String sOrderNo="";
	String sBuyerOrg="";
	String sItemId="";
	String sDocumentType="";
	String sPrimeLineNo="";
	String sSubLineNo="";
	String sShipnode="";
	
	
	 YFCDocument document = YFCDocument.getDocumentFor(inDoc);
     YFCElement element = document.getDocumentElement();
     YFCElement eleShipLinesInput = element.getChildElement("ShipmentLines");
		
		YFCIterable<YFCElement> allShipmentline = eleShipLinesInput.getChildren();
		for (YFCElement eleShpLine : allShipmentline){
    	    	
			sOrderNo=eleShpLine.getAttribute("OrderNo");
			sItemId = eleShpLine.getAttribute("ItemID");
			sDocumentType = eleShpLine.getAttribute("DocumentType");
			
			YFCDocument inOrderLineList = YFCDocument.getDocumentFor("<OrderLine DocumentType=\"\" > " +
					"<Order OrderNo=\"\"/>" +
					"<Item ItemID=\"\" /> " +
					"</OrderLine>");

			//LOGIC to find the right OrderLine PrimeLineNo
			inOrderLineList.getDocumentElement().setAttribute("DocumentType", sDocumentType);
			inOrderLineList.getDocumentElement().getChildElement("Order").setAttribute("OrderNo", sOrderNo);				
			inOrderLineList.getDocumentElement().getChildElement("Item").setAttribute("ItemID", sItemId);
			
			YFCDocument tempOrderLineList = YFCDocument.getDocumentFor("<OrderLineList><OrderLine SubLineNo=\"\" PrimeLineNo=\"\" OrderLineKey=\"\" >"
							+ "<Order BuyerOrganizationCode=\"\"/><Item ItemID=\"\" /></OrderLine></OrderLineList>");
			env.setApiTemplate("getOrderLineList", tempOrderLineList.getDocument());
			Document outOrderLineList = YIFClientFactory.getInstance().getLocalApi().invoke(env, "getOrderLineList", inOrderLineList.getDocument());
			env.clearApiTemplate("getOrderLineList");
			
			YFCDocument oOrderLineList = YFCDocument.getDocumentFor(outOrderLineList);
			if(oOrderLineList != null){
				YFCElement oOrderLineEle = oOrderLineList.getDocumentElement().getChildElement("OrderLine");
				if(oOrderLineEle != null){
					sPrimeLineNo = oOrderLineEle.getAttribute("PrimeLineNo");
					sSubLineNo=oOrderLineEle.getAttribute("SubLineNo");
					sShipnode=oOrderLineEle.getAttribute("ShipNode");
					YFCElement eleOrderLine = oOrderLineEle.getChildElement("Order");
					if(!YFCElement.isVoid(eleOrderLine)){
						sBuyerOrg= eleOrderLine.getAttribute("BuyerOrganizationCode");
					}
				}
			}
			
			
				eleShpLine.setAttribute("PrimeLineNo", sPrimeLineNo);
				eleShpLine.setAttribute("SubLineNo", sSubLineNo);
				
			
			}
				
		element.setAttribute("BuyerOrganizationCode", sBuyerOrg);
		element.setAttribute("ShipNode", sShipnode);
	}
	 catch (Exception e) {
			e.printStackTrace();
			System.out.println("######### Exception Caught : " + e.toString());
			throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}
	return inDoc;
}

}

package com.teradyne.om.api;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfs.japi.YFSEnvironment;
import com.sterlingcommerce.baseutil.SCXmlUtil;
import com.bridge.sterling.utils.*;
public class ImportInfo {

	/*
	 * Fields to be imported:
	 * From Merlin:
	 * REPAIR_ORDER_NO
	 * PROD_NO
	 * PROD_SERIAL_NO
	 * DATE_TO_REPAIR
	 * 
	 * To Sterling:
	 * Sterling work order no, item, item serial number, date to repair(not needed)
	 * <Merlin WorkOrderNo="" item="" itemSerialNo="" repairDate="" />         
	 */
	public void ImportWorkOrderInfo(YFSEnvironment env, Document merlinDoc) throws Exception {

		Element eleMerlinXML = merlinDoc.getDocumentElement();
		String strWorkOrder = eleMerlinXML.getAttribute("WorkOrderNo");

		//using the values obtained from the file, build the input xml and template for the api getWorkOrderList
		Document inputWorkOrderDoc = SCXmlUtil.createDocument("WorkOrder");
		Element inputXML = inputWorkOrderDoc.getDocumentElement();
		inputXML.setAttribute("WorkOrderNo", strWorkOrder);

		Document inputTemplateDoc = SCXmlUtil.createDocument("WorkOrder");
		Element inputTemplateXML = inputTemplateDoc.getDocumentElement();
		inputTemplateXML.setAttribute("WorkOrderNo", "' '");
		inputTemplateXML.setAttribute("NodeKey", "' '");
		inputTemplateXML.setAttribute("EnterpriseCode", "' '");

		YFCDocument WorkOrderYFCDoc = SterlingUtil.callAPI(env,"getWorkOrderList",YFCDocument.getDocumentFor(inputWorkOrderDoc), YFCDocument.getDocumentFor(inputTemplateDoc));
		Document WorkOrderDoc=WorkOrderYFCDoc.getDocument();

		Element WorkOrdersEle = WorkOrderDoc.getDocumentElement();
		Element WorkOrderEle = (Element) WorkOrdersEle.getElementsByTagName("WorkOrder").item(0);
		//build input xml for confirmWorkOrder api
		Document inputConfirmDoc = SCXmlUtil.createDocument("WorkOrder");
		Element inputConfirmXML = inputConfirmDoc.getDocumentElement();
		inputConfirmXML.setAttribute("WorkOrderNo", WorkOrderEle.getAttribute("WorkOrderNo"));
		inputConfirmXML.setAttribute("NodeKey", WorkOrderEle.getAttribute("NodeKey"));
		inputConfirmXML.setAttribute("EnterpriseCode", WorkOrderEle.getAttribute("EnterpriseCode"));

		(SterlingUtil.callAPI(env, "confirmWorkOrder",YFCDocument.getDocumentFor(inputConfirmDoc), YFCDocument.getDocumentFor(inputTemplateDoc))).getDocument();
		//return docPrintServiceOutput;
	}
}

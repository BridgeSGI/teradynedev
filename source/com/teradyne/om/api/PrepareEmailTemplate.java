package com.teradyne.om.api;


import java.util.ArrayList;
import java.util.Properties;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.CreatePDF;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class PrepareEmailTemplate implements YIFCustomApi{
  
  private Properties properties = null;
  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  
  public void setProperties(Properties prop) throws Exception {
    properties = prop;
  }  

  private SalesOrderUtils salesOrderUtils = new SalesOrderUtils();

  public Document prepareEmailTemplate(YFSEnvironment env, Document inDoc) throws Exception {

    ArrayList<String> fileList = new ArrayList<String>();
    Document output = getOrderListForJasperReport(env, inDoc);

    String path = Constants.PATH;
    String quoteLogoImgPath = Constants.LOGO_IMAGE_PATH;
    String termsFirstPage = Constants.TERMS_FIRST_PAGE;
    String termsSecPage = Constants.TERMS_SECOND_PAGE;
    
    log.debug("Quote file:"+path);
    log.debug("Terms file:"+termsFirstPage);
    log.debug("Terms1 file:"+termsSecPage);
    
    CreatePDF pdfUtil = new CreatePDF();
    pdfUtil.setProperties(properties);
    
    pdfUtil.exportPdf(output, path, false, 0, fileList, quoteLogoImgPath);
    pdfUtil.exportPdf(output, termsFirstPage, true, 1, fileList,null);
    pdfUtil.exportPdf(output, termsSecPage, true, 2, fileList,null);

    String filePath = pdfUtil.doMerge(output,fileList);
    
    YFCDocument document = YFCDocument.getDocumentFor(output);
    YFCElement element = document.getDocumentElement();
    
    element.setAttribute("FilePath", filePath);
    return document.getDocument();
  }

  
  

  
  
  private Document getOrderListForJasperReport(YFSEnvironment env, Document inDoc) throws Exception {

    YFCDocument template =
        YFCDocument
            .getDocumentFor("<OrderList><Order DocumentType=\"\" ShipToID=\"\" OrderDate=\"\" OrderHeaderKey=\"\" OrderNo=\"\" ><PersonInfoShipTo AddressLine1=\"\" City=\"\" State=\"\" ZipCode=\"\" Country=\"\" EMailID=\"\"/>"
                + "<PersonInfoBillTo Company=\"\" AddressLine1=\"\" City=\"\" State=\"\" ZipCode=\"\" Country=\"\"/><Extn ContactAttnTo=\"\" ContactEMailID=\"\" ContactName=\"\" ContactPhone=\"\" />"
                + "<OrderLines><OrderLine PrimeLineNo=\"\" OrderedQty=\"\" Status=\"\" ><LinePriceInfo UnitPrice=\"\" LineTotal=\"\" /><Item ItemID=\"\" ItemShortDesc=\"\" UnitCost=\"\" />"
                + "<Extn ControlNo=\"\" PartExpiditingPrice=\"\" ServiceType=\"\"/></OrderLine></OrderLines></Order></OrderList>");

    Document output =
        salesOrderUtils.callApi(env, inDoc, template.getDocument(), "getOrderList", true);
    YFCDocument yfcOutputDoc = YFCDocument.getDocumentFor(output);

    String strOutput = "";
    YFCElement orderEle = yfcOutputDoc.getDocumentElement().getFirstChildElement();
    
    trimDate(orderEle);
    strOutput = prepareOrderForJasperTemplate(strOutput, orderEle);

    YFCDocument yfcOutput = YFCDocument.getDocumentFor(strOutput);
    return yfcOutput.getDocument();
  }

  private String prepareOrderForJasperTemplate(String strOutput, YFCElement orderEle) {
    if (!XmlUtils.isVoid(orderEle)) {

      YFCElement orderLinesEle = orderEle.getChildElement("OrderLines");
      for (YFCElement orderLine : orderLinesEle.getChildren("OrderLine")) {
        if ("Cancelled".equalsIgnoreCase(orderLine.getAttribute("Status"))) {
          orderLine.getParentElement().removeChild(orderLine);
        }
      }
      strOutput = orderEle.getString();
      strOutput = strOutput.replaceAll("&", "&amp;");
    }
    return strOutput;
  }
  
  private void trimDate(YFCElement orderEle){
    
    if(!XmlUtils.isVoid(orderEle)){
      
      String strOrderDate = orderEle.getAttribute(XMLConstants.ORDER_DATE);
      if(!XmlUtils.isVoid(strOrderDate)){
        
        strOrderDate = strOrderDate.substring(0, strOrderDate.indexOf('T'));
        orderEle.setAttribute(XMLConstants.ORDER_DATE, strOrderDate);
        
      }
    }
  }


}

package com.teradyne.om.api;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class SendEmail implements YIFCustomApi {

  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  private Properties properties = null;
  private SalesOrderUtils orderUtils = new SalesOrderUtils();

  public void setProperties(Properties prop) throws Exception {
    properties = prop;
  }

  // For now, I am hard coding email address
  public Document sendQuoteMail(YFSEnvironment env, Document inDoc) throws AddressException {

    if (log.isVerboseEnabled()) {
      log.verbose("Input to sendQuoteMail:" + XmlUtils.getString(inDoc));
    }
    String server = "";
    String port = "";
    String subject = "";
    if (properties != null) {

      server = properties.getProperty("host");
      port = properties.getProperty("port");
      subject = properties.getProperty("Subject");

    } else {
      String errorCode = "TD001";
      String errorDescription = "SDF arguments are not passed correctly";
      YFSException exeception = new YFSException(errorDescription, errorCode, errorDescription);
      log.verbose(exeception);
      throw exeception;
    }

    YFCDocument document = YFCDocument.getDocumentFor(inDoc);
    YFCElement element = document.getDocumentElement();
    
    String filePath = element.getAttribute("FilePath");
    String sEmailBodyText =
        "Dear Customer,\nPlease find the attached report.\n\nWe appreciate your business with Teradyne.\n\nThank you!";

    final String sFromEmailID = getUserName(env);
    final String strPassword = getPassword(env);

    ArrayList<String> toEmailIds = getToEmailIds(element);

    if (toEmailIds.size() == 0) {
      return document.getDocument();
    }
    InternetAddress[] toAddress = new InternetAddress[toEmailIds.size()];
    for (int i = 0; i < toEmailIds.size(); i++) {
      toAddress[i] = new InternetAddress(toEmailIds.get(i));
    }
    Session session = null;
    Properties props = System.getProperties();
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.smtp.port", port);
    props.put("mail.smtp.host", server);
    props.put("mail.smtp.auth", "false");

    if (!XmlUtils.isVoid(strPassword)) {
      props.put("mail.smtp.auth", "true");
      session = Session.getInstance(props, new Authenticator() {
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
          return new PasswordAuthentication(sFromEmailID, strPassword);
        }
      });
    } else {
      session = Session.getDefaultInstance(props);
    }

    try {
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(sFromEmailID, true));
      message.setSentDate(new Date());
      message.setRecipients(Message.RecipientType.TO, toAddress);
      message.setSubject(subject);
      
      Multipart multipart = addBodyPart(filePath, sEmailBodyText, message);
      message.setContent(multipart);
      Transport.send(message);
    } catch (MessagingException e) {
      log.debug(e.getMessage() + " " + e.getCause());
      if (log.isVerboseEnabled()) {
        log.verbose(e);
      }
      e.printStackTrace();
    }
    return inDoc;
  }

  private Multipart addBodyPart(String filePath, String sEmailBodyText, Message message)
      throws MessagingException {
    Multipart multipart = new MimeMultipart();

    BodyPart msgTextPart = new MimeBodyPart();
    msgTextPart.setText(sEmailBodyText);
    multipart.addBodyPart(msgTextPart);

    BodyPart messageBodyPart = new MimeBodyPart();
    if (!XmlUtils.isVoid(filePath)) {
      File file = new File(filePath);

      DataSource dataSource = new FileDataSource(file) {
        public String getContentType() {
          return "application/pdf";
        }
      };
      messageBodyPart.setDataHandler(new DataHandler(dataSource));
      messageBodyPart.setFileName(file.getName());
      messageBodyPart.setDisposition(Part.ATTACHMENT);
      multipart.addBodyPart(messageBodyPart);
    }
    return multipart;
  }

  private ArrayList<String> getToEmailIds(YFCElement inDocEle) {

    YFCElement personInfo = inDocEle.getChildElement(XMLConstants.PERSON_INFO_SHIP_TO);
    YFCElement extnEle = inDocEle.getChildElement(XMLConstants.EXTN);

    String perInfoEmail = personInfo.getAttribute(XMLConstants.EMAIL_ID);
    String contactEmail = extnEle.getAttribute(XMLConstants.CONTACT_EMAIL_ID);

    ArrayList<String> emailIds = new ArrayList<String>();

    if (!XmlUtils.isVoid(perInfoEmail))
      emailIds.add(perInfoEmail);
    if (!XmlUtils.isVoid(contactEmail))
      emailIds.add(contactEmail);

    return emailIds;
  }

  private String getUserName(YFSEnvironment env) {

    String username = orderUtils.getProperty(env, "email.username");

    if (XmlUtils.isVoid(username)) {
      log.debug("Username is not configured in Properties");
    }

    return username;
  }

  private String getPassword(YFSEnvironment env) {

    String password = orderUtils.getProperty(env, "email.password");

    if (XmlUtils.isVoid(password)) {
      log.debug("Password is not configured in Properties");
    }

    return password;
  }

}

package com.teradyne.om.api;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.CustomLogCategory;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.teradyne.om.util.ExchangeOrderUtils;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;


public class CreateWorkOrder {
	
	private String orderListTemplate = "<OrderList><Order OrderNo=\"\" OrderHeaderKey=\"\" ><References>"
			+ "<Reference Name=\"\" Value=\"\" /></References><OrderLines><OrderLine OrderLineKey=\"\">"
			+ "<Extn /></OrderLine></OrderLines></Order></OrderList>";
	
	private static CustomLogCategory log = CustomLogCategory.instance(CreateWorkOrder.class);
	public void createWorkOrder(YFSEnvironment env, YFCDocument inDoc) throws TransformerException, ParserConfigurationException, RemoteException, YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		log.verbose("inXML", inDoc.getDocument());
		String documentTypeStr = XPathUtil.getXpathAttribute(inDoc, "//Shipment/@DocumentType");
		String enterpriseCodeStr = inDoc.getDocumentElement().getAttribute(XMLConstants.ENTERPRISE_CODE);
		String receivingNodeStr = inDoc.getDocumentElement().getAttribute(XMLConstants.RECEIVING_NODE);
		String roOHK;
		String toOHK;
		if("0006".equals(documentTypeStr)){
			toOHK = XPathUtil.getXpathAttribute(inDoc, "//ReceiptLine/@OrderHeaderKey");
			ExchangeOrderUtils eoUtils = new ExchangeOrderUtils();
			YFCDocument orderListOut = eoUtils.getOrderList(env, toOHK, orderListTemplate);
			roOHK = XPathUtil.getXpathAttribute(orderListOut, "//Reference[@Name='RO-OHK']/@Value");
			
		}else {
			toOHK = XPathUtil.getXpathAttribute(inDoc, "//ReceiptLine/@OrderHeaderKey");
			roOHK = toOHK;
		}
		
		for(YFCElement receiptLineEle : inDoc.getDocumentElement().getChildElement("ReceiptLines").getChildren()){
			//String orderLineKeyStr = receiptLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
			YFCDocument receiptLineIn = YFCDocument.createDocument();
			receiptLineIn.appendChild(receiptLineIn.importNode(receiptLineEle, true));
			
			receiptLineIn.getDocumentElement().setAttribute("roOHK", roOHK);
			receiptLineIn.getDocumentElement().setAttribute("EnterpriseCode", enterpriseCodeStr);
			receiptLineIn.getDocumentElement().setAttribute("ReceivingNode", receivingNodeStr);
			
			formWOXMLForMerlin(env, receiptLineIn);
			
			
		}
		
		
	}
	
	public void formWOXMLForMerlin(YFSEnvironment env, YFCDocument inDoc) throws YIFClientCreationException, SAXException, IOException, TransformerException, ParserConfigurationException{
		System.out.println("receiptLine Doc"+inDoc.toString());
		String roOHK = inDoc.getDocumentElement().getAttribute("roOHK");
		String orderLineKey = inDoc.getDocumentElement().getAttribute("OrderLineKey");
		HashMap<String, String> woHM = callApiForWO(env, inDoc.getDocumentElement());
		YFCDocument woDoc = YFCDocument.createDocument("WorkOrder");
		YFCElement woRootEle = woDoc.getDocumentElement();
		woRootEle.setAttribute("RepairOrderNo", woHM.get("WorkOrderNo"));
		woRootEle.setAttribute("DateToRepair", woHM.get("StatusDate"));
		woRootEle.setAttribute("ItemID", inDoc.getDocumentElement().getAttribute("ItemID"));
		woRootEle.setAttribute("RepairCenter", inDoc.getDocumentElement().getAttribute("ReceivingNode"));
		
		String orderLineTemplate = "<OrderLineList><OrderLine OrderLineKey=\"\" ><Extn SystemType=\"\" SystemSerialNo=\"\" ServiceType=\"\" "
				+ "OEMSerialNo=\"\" ProdSerialNumber=\"\" SpecificReviewFlag=\"\" /><ItemDetails ItemID=\"\">"
				+ "<Extn RespProdRepairDivCode=\"\"/></ItemDetails><Order OrderType=\"\" CustomerPONo=\"\" ShipToID=\"\" /></OrderLine></OrderLineList>";
		YFCDocument getOrderLineListIn = YFCDocument.parse("<OrderLine OrderLineKey=\""+orderLineKey +"\" />");
		YFCDocument getOrderLineListOut = SterlingUtil.callAPI(env, "getOrderLineList", getOrderLineListIn, "");
		System.out.println("getOrderLineListOut"+getOrderLineListOut.toString());
		String isPartDOA = "DOA".equals(XPathUtil.getXpathAttribute(getOrderLineListOut, "//Order/@OrderType"))? "Y" : "N";
		woRootEle.setAttribute("IsPartDOA", isPartDOA);
		woRootEle.setAttribute("ReferenceNo", XPathUtil.getXpathAttribute(getOrderLineListOut, "//Order/@CustomerPONo"));
		woRootEle.setAttribute("CustNo", XPathUtil.getXpathAttribute(getOrderLineListOut, "//Order/@ShipToID"));
		woRootEle.setAttribute("CustAddNo", XPathUtil.getXpathAttribute(getOrderLineListOut, "//Order/@ShipToID"));
		woRootEle.setAttribute("RespProdRepairDivCode", XPathUtil.getXpathAttribute(getOrderLineListOut, "//ItemDetails/Extn/@RespProdRepairDivCode"));
		woRootEle.setAttribute("SystemType", XPathUtil.getXpathAttribute(getOrderLineListOut, "//OrderLine/Extn/@SystemType"));
		woRootEle.setAttribute("SystemSerialNo", XPathUtil.getXpathAttribute(getOrderLineListOut, "//OrderLine/Extn/@SystemSerialNo"));
		woRootEle.setAttribute("ServiceType", XPathUtil.getXpathAttribute(getOrderLineListOut, "//OrderLine/Extn/@ServiceType"));
		woRootEle.setAttribute("OEMSerialNo", XPathUtil.getXpathAttribute(getOrderLineListOut, "//OrderLine/Extn/@OEMSerialNo"));
		woRootEle.setAttribute("ProdSerialNumber", XPathUtil.getXpathAttribute(getOrderLineListOut, "//OrderLine/Extn/@ProdSerialNumber"));
		woRootEle.setAttribute("SpecificReviewFlag", XPathUtil.getXpathAttribute(getOrderLineListOut, "//OrderLine/Extn/@SpecificReviewFlag"));
		System.out.println("Sending to Queue"+woDoc.toString());
		
		YIFApi api = YIFClientFactory.getInstance().getApi();
		api.executeFlow(env, "WODetailForMerlin", woDoc.getDocument());
		
		
	}

	private HashMap<String,String> callApiForWO(YFSEnvironment env, YFCElement receiptLineEle) throws RemoteException, YIFClientCreationException {
		// TODO Auto-generated method stub
				
		YFCDocument workOrderIn = YFCDocument.createDocument("WorkOrder");
		YFCElement woRootEle = workOrderIn.getDocumentElement();
		woRootEle.setAttribute(XMLConstants.DOCUMENT_TYPE, "7001");
		woRootEle.setAttribute(XMLConstants.ENTERPRISE_CODE, receiptLineEle.getAttribute("EnterpriseCode"));
		woRootEle.setAttribute("NodeKey", receiptLineEle.getAttribute("ReceivingNode"));
		woRootEle.setAttribute(XMLConstants.ITEM_ID, receiptLineEle.getAttribute(XMLConstants.ITEM_ID));
		woRootEle.setAttribute("Uom", receiptLineEle.getAttribute(XMLConstants.UNIT_OF_MEASURE));
		woRootEle.setAttribute(XMLConstants.PRODUCT_CLASS, "GOOD");
		woRootEle.setAttribute("ServiceItemID", "TESTSI");
		woRootEle.setAttribute("ServiceItemGroupCode", "COMPL");
		woRootEle.setAttribute("QuantityRequested", receiptLineEle.getAttribute(XMLConstants.QUANTITY));
		woRootEle.setAttribute("SegmentType", "MTO");
		woRootEle.setAttribute("Segment", receiptLineEle.getAttribute("OrderHeaderKey"));
		
		
		YFCElement woOrderEle = woRootEle.createChild(XMLConstants.ORDER);
		woOrderEle.setAttribute(XMLConstants.DOCUMENT_TYPE, "0003");
		woOrderEle.setAttribute(XMLConstants.ENTERPRISE_CODE, receiptLineEle.getAttribute("EnterpriseCode"));
		woOrderEle.setAttribute(XMLConstants.ORDER_HEADER_KEY, receiptLineEle.getAttribute("roOHK"));
		
		YFCElement woComponentsEle = woRootEle.createChild("WorkOrderComponents");
		YFCElement woComponentEle = woComponentsEle.createChild("WorkOrderComponent");
		woComponentEle.setAttribute(XMLConstants.ITEM_ID, receiptLineEle.getAttribute(XMLConstants.ITEM_ID));
		woComponentEle.setAttribute("Uom", receiptLineEle.getAttribute(XMLConstants.UNIT_OF_MEASURE));
		woComponentEle.setAttribute(XMLConstants.PRODUCT_CLASS, receiptLineEle.getAttribute(XMLConstants.PRODUCT_CLASS));
		woComponentEle.setAttribute("ComponentQuantity", receiptLineEle.getAttribute(XMLConstants.QUANTITY));
		
		System.out.println("workOrderIn :"+workOrderIn.toString());
		//log.verbose("workOrderIn", workOrderIn.getDocument());
		YFCDocument workOrderOut = SterlingUtil.callAPI(env, "createWorkOrder", workOrderIn, "");
		System.out.println("workOrderOut :"+workOrderOut.toString());
		//log.verbose("workOrderOut", workOrderOut.getDocument());
		HashMap<String, String> woHM = new HashMap<String, String>();
		woHM.put("WorkOrderNo", workOrderOut.getDocumentElement().getAttribute("WorkOrderNo"));
		woHM.put("StatusDate", workOrderOut.getDocumentElement().getAttribute("StatusDate"));
		
		return woHM;
		
	}
	
	public Document completeWorkOrder(YFSEnvironment env, Document inXML) throws RemoteException, YIFClientCreationException, SAXException, IOException{
		YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
		YFCElement inRootEle = inDoc.getDocumentElement();
		YFCDocument woListIn = YFCDocument.createDocument("WorkOrder");
		System.out.println("WorkOrderIn"+inDoc.toString());
		woListIn.getDocumentElement().setAttribute("WorkOrderNo", inRootEle.getAttribute("RepairOrderNo"));
		woListIn.getDocumentElement().setAttribute("ItemID", inRootEle.getAttribute("ItemID"));
		woListIn.getDocumentElement().setAttribute("StatusDate", inRootEle.getAttribute("DateToRepair"));
		woListIn.getDocumentElement().setAttribute("StatusDateQryType", "GT");
		
		String woTemplateStr = "<WorkOrders><WorkOrder WorkOrderKey=\"\" /></WorkOrders>";
		YFCDocument woListOut = SterlingUtil.callAPI(env, "getWorkOrderList", woListIn, YFCDocument.parse(woTemplateStr));
		System.out.println("WorkOrderListOutn"+woListOut.toString());
		if(!woListOut.hasChildNodes()){
			return inXML;
		}
		String workOrderKeyStr = woListOut.getDocumentElement().getChildElement("WorkOrder").getAttribute("WorkOrderKey");
		YFCDocument confirmWOIn = YFCDocument.parse("<WorkOrder AttemptSuccessful=\"Y\" WorkOrderKey=\""+ workOrderKeyStr + "\" />");
		YFCDocument confirmWOOut = SterlingUtil.callAPI(env, "confirmWorkOrder", confirmWOIn, YFCDocument.parse("<WorkOrder WorkOrderKey=\"\" />"));
		
		return confirmWOOut.getDocument();
		
	}

}

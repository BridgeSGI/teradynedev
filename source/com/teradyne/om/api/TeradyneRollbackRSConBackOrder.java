package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.api.CustomCodeHelper;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;

public class TeradyneRollbackRSConBackOrder extends CustomCodeHelper

{

	String sOdrHdrKey = null;
	String sOdrLineKey = null;
	String sDisRuleId = null;
	YFCDocument inYFCXML = null;
	String sBuyerOrg = null;
	String sEXTNDIC = null;
	String sEXTDOC = null;
	String sEXTNRPM = null;
	String sEXTNRSC = null;

	String sItemID = null;
	int OrderLineListLength = 0;
	YFCDocument changeOrderIPXML = null;

	private static YFCLogCategory _cat = YFCLogCategory
			.instance("com.yantra.CustomCode");

	public void rollbackRSC(YFSEnvironment arg0, Document inXML)
			throws YFSUserExitException {
		setEnv(arg0);

		inYFCXML = YFCDocument.getDocumentFor(inXML);


		if (_cat.isVerboseEnabled()) {
			_cat.verbose("input to TeradyneRollbackRSConBackOrder call is:"
					+ inYFCXML
							.toString());
		}

		final YFCNodeList<YFCElement> OrderLineNodeList = inYFCXML
				.getDocumentElement().getElementsByTagName("OrderLine");

		OrderLineListLength = OrderLineNodeList.getLength();
		if (OrderLineListLength > 0)

		{
			for (int i = 0; i < OrderLineListLength; i++)

			{

				final YFCNode cNode = OrderLineNodeList.item(i);

				if (cNode.getNodeName().equalsIgnoreCase("OrderLine")) {
					if (cNode instanceof YFCElement) {
						final YFCElement OdrlineElem = (YFCElement) cNode;
						try {

							if (!XmlUtils.isVoid(OdrlineElem.getChildElement("Extn")))
							
							{
								if ((!XmlUtils.isVoid(OdrlineElem.getChildElement("Extn").getAttribute("RapidSourceCenter")))
										&&(!XmlUtils.isVoid(OdrlineElem.getAttribute("ShipNode"))
												&&(OdrlineElem.getChildElement("Extn").getAttribute("RapidSourceCenter").equalsIgnoreCase(OdrlineElem.getAttribute("ShipNode")))
												)
										)
												
												{
									/*
									 * call change order here to remove shipNode
									 * and assign distribution group
									 */

									if (_cat.isVerboseEnabled()) {
										_cat.verbose("ready to rollback");
									}
									
									if (inYFCXML.getDocumentElement().hasAttribute("OrderHeaderKey")) {
										sOdrHdrKey = inYFCXML.getDocumentElement().getAttribute(
												"OrderHeaderKey");

										sBuyerOrg = getCustomerId(sOdrHdrKey);

										if (sBuyerOrg.length() != 0) {

											final YFCDocument getCustListInput = YFCDocument
													.getDocumentFor(getCustomerIP());
											getCustListInput.getDocumentElement().setAttribute(
													"CustomerID", sBuyerOrg);
											final YFCDocument CustomerListOutput = getCustomerListCall(getCustListInput);
											if (CustomerListOutput.getDocumentElement().hasChildNodes()) {
												sEXTDOC = CustomerListOutput.getDocumentElement()
														.getChildElement("Customer")
														.getChildElement("Extn")
														.getAttribute("DefaultOrderCenter");

											}
										}
									}

									sOdrLineKey = OdrlineElem
											.getAttribute("OrderLineKey");

									if (sEXTDOC != null) {

										sItemID = OdrlineElem.getChildElement(
												"Item").getAttribute("ItemID");
										String sUOM = OdrlineElem
												.getChildElement("Item")
												.getAttribute("UnitOfMeasure");
										final YFCDocument getItemListInput = YFCDocument
												.getDocumentFor(getItemListIP());

										getItemListInput
												.getDocumentElement()
												.setAttribute("ItemID", sItemID);
										getItemListInput.getDocumentElement()
												.setAttribute("UnitOfMeasure",
														sUOM);

										final YFCDocument ItemListOutput = getItemListCall(getItemListInput);

										if (!ItemListOutput
												.getDocumentElement()
												.getAttribute("TotalItemList")
												.equalsIgnoreCase("0")) {
											sEXTNRPM = ItemListOutput
													.getDocumentElement()
													.getChildElement("Item")
													.getChildElement("Extn")
													.getAttribute("RepairModel");

											sEXTNDIC = ItemListOutput
													.getDocumentElement()
													.getChildElement("Item")
													.getChildElement("Extn")
													.getAttribute(
															"DefaultInventoryCenter");

										}

										if ((sEXTNRPM.length() != 0 && !sEXTNRPM
												.equalsIgnoreCase(" "))
												&& (sEXTNDIC.length() != 0 && !sEXTNDIC
														.equalsIgnoreCase(" ")))

										{

											sDisRuleId = sEXTDOC + sEXTNDIC
													+ sEXTNRPM;
											changeOrderIPXML = changeOrderIP(sOdrHdrKey,sOdrLineKey,sDisRuleId);

											if (_cat.isVerboseEnabled()) {
												_cat.verbose("input to changeOrder +if call is:"
														+ changeOrderIPXML
																.toString());
											}

											
											if (isDistRule(sDisRuleId)) {
												changeOrderCall(changeOrderIPXML);
											}
										}

										/*
										 * else { changeOrderIPXML =
										 * changeOrderIP(); changeOrderIPXML
										 * .getDocumentElement()
										 * .getChildElement( "OrderLine")
										 * .removeAttribute(
										 * "DistributionRuleId"); if
										 * (_cat.isVerboseEnabled()) {
										 * _cat.verbose
										 * ("input to changeOrder +else1 call is:"
										 * + changeOrderIPXML .toString()); }
										 * //changeOrderCall(changeOrderIPXML);
										 * 
										 * }
										 */
									}

									/*
									 * else { changeOrderIPXML =
									 * changeOrderIP(); changeOrderIPXML
									 * .getDocumentElement()
									 * .getChildElement("OrderLine")
									 * .removeAttribute( "DistributionRuleId");
									 * if (_cat.isVerboseEnabled()) {
									 * _cat.verbose
									 * ("input to changeOrder +else2 call is:" +
									 * changeOrderIPXML .toString()); }
									 * //changeOrderCall(changeOrderIPXML);
									 * 
									 * }
									 */
								}
							}

						}

						catch (final Exception e) {
							e.printStackTrace();
							try {
								throw e;
							} catch (final Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();

							}
						}

					}
				}
			}
		}
	}

	protected void changeOrderCall(YFCDocument inXML) throws YFSException {

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("input to changeOrder call is:" + inXML.toString());
		}

		invokeApi("changeOrder", inXML.getDocument());

	}

	private YFCDocument changeOrderIP(String sOdrHdrKey,String sOdrLineKey,String sDisRuleId) throws YFSException {
		final YFCDocument changeOrderIP = YFCDocument
				.getDocumentFor("<Order Action=\"MODIFY\"  OrderHeaderKey='"
						+ sOdrHdrKey + "' Override=\"Y\" >"
						+ "<OrderLines><OrderLine OrderLineKey='" + sOdrLineKey
						+ "' ShipNode=\"\" DistributionRuleId='" + sDisRuleId
						+ "' ><Extn  RapidSourceCenter=\"\"/>"
						+ "</OrderLine></OrderLines></Order>");
		return changeOrderIP;
	}

	private Document getCustomerIP() throws YFSException {
		final YFCDocument getCustomerIP = YFCDocument
				.getDocumentFor("<Customer  />");
		return getCustomerIP.getDocument();
	}

	private Document getCustomerOP() throws YFSException {
		final YFCDocument getCustomerOP = YFCDocument
				.getDocumentFor("<CustomerList>"
						+ "<Customer  CustomerKey=\"\">"
						+ "<Extn DefaultOrderCenter=\"\"/>"
						+ "<CustomerAdditionalAddressList TotalNumberOfRecords=\"\">"
						+ "<CustomerAdditionalAddress CustomerAdditionalAddressID=\"\" IsShipTo=\"\" IsDefaultShipTo=\"\">"
						+ "<PersonInfo  Country=\"\" />"
						+ "</CustomerAdditionalAddress>"
						+ "</CustomerAdditionalAddressList>"
						+ "</Customer></CustomerList>");
		return getCustomerOP.getDocument();
	}

	private Document getItemListIP() throws YFSException {
		final YFCDocument getItemListIP = YFCDocument
				.getDocumentFor("<Item ItemID=\"\" UnitOfMeasure=\"\"/>");
		return getItemListIP.getDocument();
	}

	private Document getItemListOP() throws YFSException {
		final YFCDocument getItemListOP = YFCDocument
				.getDocumentFor("<ItemList TotalItemList=\"\"><Item ItemID=\"\" "
						+ "ItemKey=\"\" OrganizationCode=\"\" UnitOfMeasure=\"\"><Extn RepairModel=\"\" DefaultInventoryCenter=\"\"/>"
						+ "</Item></ItemList>");
		return getItemListOP.getDocument();
	}

	protected YFCDocument getCustomerListCall(YFCDocument inXML)
			throws YFSException {
		YFCDocument CustListOP = null;

		final Document CALDayDtlscallOPTemp = getCustomerOP();
		Document CALDayDtlscallOP = null;
		CALDayDtlscallOP = invokeApi("getCustomerList", inXML.getDocument(),
				CALDayDtlscallOPTemp);
		CustListOP = YFCDocument.getDocumentFor(CALDayDtlscallOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getcalendardaydtls call is:"
					+ CustListOP.toString());
		}

		return CustListOP;

	}

	protected YFCDocument getItemListCall(YFCDocument inXML)
			throws YFSException {
		YFCDocument ItemListOP = null;

		final Document getItemListOPtemp = getItemListOP();
		Document getItemListOP = null;
		getItemListOP = invokeApi("getItemList", inXML.getDocument(),
				getItemListOPtemp);
		ItemListOP = YFCDocument.getDocumentFor(getItemListOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getitemListcall is:"
					+ ItemListOP.toString());
		}

		return ItemListOP;

	}

	protected String getCustomerId(String oderheaderkey) {
		String sCustomerId = null;
		YFCDocument inputXML = YFCDocument
				.getDocumentFor("<Order OrderHeaderKey='" + oderheaderkey
						+ "' />");
		Document getOdrlist = null;
		YFCDocument getItemListOPtemp = YFCDocument
				.getDocumentFor("<OrderList  TotalNumberOfRecords=\"\" ><Order ReceivingNode=\"\"/></OrderList>");
		getOdrlist = invokeApi("getOrderList", inputXML.getDocument(),
				getItemListOPtemp.getDocument());

		if (YFCDocument.getDocumentFor(getOdrlist).getDocumentElement()
				.getElementsByTagName("Order").getLength() > 0)

		{
			if (YFCDocument.getDocumentFor(getOdrlist).getDocumentElement()
					.getElementsByTagName("Order").item(0)
					.hasAttribute("ReceivingNode")
					&&

					YFCDocument.getDocumentFor(getOdrlist).getDocumentElement()
							.getElementsByTagName("Order").item(0)
							.getAttribute("ReceivingNode").length() > 0)

			{
				sCustomerId = YFCDocument.getDocumentFor(getOdrlist)
						.getDocumentElement().getElementsByTagName("Order")
						.item(0).getAttribute("ReceivingNode");

			}

		}

		return sCustomerId;
	}

	protected Boolean isDistRule(String sDistId)

	{
		Boolean isDistExits = false;
		Document getDistRuleList = null;
		YFCDocument getDistListOPtemp = YFCDocument
				.getDocumentFor("<ItemShipNodeList>"
						+ "<ItemShipNode DistributionRuleId=\"\" ActiveFlag=\"\"/>"
						+ "</ItemShipNodeList>");
		YFCDocument inputXMLDisList = YFCDocument
				.getDocumentFor("<ItemShipNode  ActiveFlag=\"Y\"  DistributionRuleId='"
						+ sDistId + "' />");
		getDistRuleList = invokeApi("getDistributionList",
				inputXMLDisList.getDocument(), getDistListOPtemp.getDocument());

		if (YFCDocument.getDocumentFor(getDistRuleList).getDocumentElement()
				.hasChildNodes()) {

			isDistExits = true;

		}
		return isDistExits;
	}

}

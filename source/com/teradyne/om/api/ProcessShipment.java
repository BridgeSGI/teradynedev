package com.teradyne.om.api;



import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
//import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class ProcessShipment implements YIFCustomApi {

	private YFCLogCategory logger;
	YFCDocument outDoc = null;
	Double zeroQty = 0.0;
	String sQty="";
	String sOrderNo="";
	String sPrimeLineNo = "";
	String sItemId = "";
	String sDocumentType = "";
	String sCustomerPoNo = "";
	String sRequestedSerialNo = "";
	String sSubLineNo="";
	String sOrderedQty="";

	public ProcessShipment() {
		logger = YFCLogCategory.instance(this.getClass());
	}

	 /* Sample INPUT format expected to the service
	  * <Shipment ShipNode="CHRK" SellerOrganizationcode="ICD" ReceivingNode="FS" EnterpriseCode="CSO" DocumentType="0005" DoNotVerifyPalletContent="Y" DoNotVerifyCaseContent="Y" ConfirmShip="Y" BuyerOrganizationCode="CSO" ActualShipmentDate="2011-04-08">
          <ShipmentLines>
              <ShipmentLine UnitOfMeasure="EA" ShipmentSubLineNo="0" ShipmentLineNo="1" Quantity="1.0000" ProductClass="GOOD" OrderNo="1815" ItemID="405-648-00" DocumentType="0005" CustomerPoNo="1815">
                 <Extn RecvdQty="1.0000" RecDate="2011-04-08"/>
               </ShipmentLine>
          </ShipmentLines>
        </Shipment>
	  */
	public Document processTerShipment(YFSEnvironment env, Document inDoc)
			throws Exception {
		try {
			YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleInput = inYFCDoc.getDocumentElement();
			
			YFCElement eleShipLinesInput = eleInput.getChildElement("ShipmentLines");
			
			YFCIterable<YFCElement> allShipmentline = eleShipLinesInput.getChildren();
			for (YFCElement eleShpLine : allShipmentline){
			
				sQty = eleShpLine.getAttribute("Quantity");
				Double lineQty = Double.parseDouble(sQty);
				
				if(lineQty.equals(zeroQty))
				{
					//Call the order line function
					
					YFCDocument inOrderLineList = YFCDocument.getDocumentFor("<OrderLine DocumentType=\"\" CustomerPONo=\"\"  > " +
							"<Order OrderNo=\"\"/>" +
							"<Item ItemID=\"\" /> " +
							"</OrderLine>");
					
					inOrderLineList.getDocumentElement().setAttribute("DocumentType", sDocumentType);
					inOrderLineList.getDocumentElement().getChildElement("Order").setAttribute("OrderNo", sOrderNo);		
					//inOrderLineList.getDocumentElement().setAttribute("CustomerPONo", sCustomerPoNo);						
					inOrderLineList.getDocumentElement().getChildElement("Item").setAttribute("ItemID", sItemId);
					
					YFCDocument tempOrderLineList = YFCDocument.getDocumentFor("<OrderLineList><OrderLine SubLineNo=\"\" PrimeLineNo=\"\" OrderedQty=\"\" ShipNode=\"\" ReceivingNode=\"\" OrderLineKey=\"\" >"
							+ "<Item ItemID=\"\" /><Extn RecvdQty=\"\"/></OrderLine></OrderLineList>");
					env.setApiTemplate("getOrderLineList", tempOrderLineList.getDocument());
					Document outOrderLineList = YIFClientFactory.getInstance().getLocalApi().invoke(env, "getOrderLineList", inOrderLineList.getDocument());
					env.clearApiTemplate("getOrderLineList");
					
					YFCDocument oOrderLineList = YFCDocument.getDocumentFor(outOrderLineList);
					if(oOrderLineList != null){
						YFCElement oOrderLineEle = oOrderLineList.getDocumentElement().getChildElement("OrderLine");
						if(oOrderLineEle != null){
							sOrderedQty= oOrderLineEle.getAttribute("OrderedQty");
							sPrimeLineNo = oOrderLineEle.getAttribute("PrimeLineNo");
							sSubLineNo=oOrderLineEle.getAttribute("SubLineNo");
						}
												
					}
						
					
					eleShpLine.setAttribute("Quantity", sOrderedQty);
					if(sPrimeLineNo != null || !sPrimeLineNo.isEmpty()){
						eleShpLine.setAttribute("PrimeLineNo", sPrimeLineNo);
					}
					
					if(sSubLineNo != null || !sSubLineNo.isEmpty()){
						eleShpLine.setAttribute("SubLineNo", sSubLineNo);
					}
					
					
					YFCElement eleExtnShpLine = eleShpLine.getChildElement("Extn");
					if(eleExtnShpLine !=null){
						eleExtnShpLine.setAttribute("RecvdQty", "0");
					}
					
					
					
				}
				else
				{
					sOrderNo= eleShpLine.getAttribute("OrderNo");
					sItemId = eleShpLine.getAttribute("ItemID");
					sDocumentType = eleShpLine.getAttribute("DocumentType");
					if(eleShpLine.hasAttribute("CustomerPoNo")){
						sCustomerPoNo = eleShpLine.getAttribute("CustomerPoNo");
					}
									
					
					//prepare getOrderLineList API call
					YFCDocument inOrderLineList = YFCDocument.getDocumentFor("<OrderLine DocumentType=\"\" CustomerPONo=\"\"  > " +
																				"<Order OrderNo=\"\"/>" +
																				"<Item ItemID=\"\" /> " +
																				"</OrderLine>");
					
					//LOGIC to find the right OrderLine PrimeLineNo
					inOrderLineList.getDocumentElement().setAttribute("DocumentType", sDocumentType);
					inOrderLineList.getDocumentElement().getChildElement("Order").setAttribute("OrderNo", sOrderNo);		
					//inOrderLineList.getDocumentElement().setAttribute("CustomerPONo", sCustomerPoNo);						
					inOrderLineList.getDocumentElement().getChildElement("Item").setAttribute("ItemID", sItemId);
					
					YFCDocument tempOrderLineList = YFCDocument.getDocumentFor("<OrderLineList><OrderLine SubLineNo=\"\" PrimeLineNo=\"\" OrderLineKey=\"\" >"
																			+ "<Item ItemID=\"\" /></OrderLine></OrderLineList>");
					env.setApiTemplate("getOrderLineList", tempOrderLineList.getDocument());
					Document outOrderLineList = YIFClientFactory.getInstance().getLocalApi().invoke(env, "getOrderLineList", inOrderLineList.getDocument());
					env.clearApiTemplate("getOrderLineList");
					
					YFCDocument oOrderLineList = YFCDocument.getDocumentFor(outOrderLineList);
					if(oOrderLineList != null){
						YFCElement oOrderLineEle = oOrderLineList.getDocumentElement().getChildElement("OrderLine");
						if(oOrderLineEle != null){
							sPrimeLineNo = oOrderLineEle.getAttribute("PrimeLineNo");
							sSubLineNo=oOrderLineEle.getAttribute("SubLineNo");
						}
					}
					
					if(sPrimeLineNo != null || !sPrimeLineNo.isEmpty()){
						eleShpLine.setAttribute("PrimeLineNo", sPrimeLineNo);
					}
					
					if(sSubLineNo != null || !sSubLineNo.isEmpty()){
						eleShpLine.setAttribute("SubLineNo", sSubLineNo);
					}
					
					
				}
			}// end of FOR each Shipment Lines
			logger.verbose("Modified Shipment API  -- "+ inDoc);
			
				
		} catch (Exception e) {
			e.printStackTrace();
			throw new YFSException("Error found in the code","ERROR-01", "Exception Thrown");
		}
		
		return inDoc;
		
	}

	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	

	
	
}
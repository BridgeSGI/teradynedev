package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;

public class LoadSCOrder extends CustomCodeHelper{

    private YIFApi api = null;
    String sBuyer=null;
    YFCElement element=null;
    public Document loadOrders(YFSEnvironment env, Document inDoc) {
    	setEnv(env);
	YFCDocument document = YFCDocument.getDocumentFor(inDoc);
	 element = document.getDocumentElement();

	if (!XmlUtils.isVoid(element)) {
	    String billToId = element.getAttribute("BillToID");
	    String shipToId = element.getAttribute("ShipToID");
	    sBuyer=element.getAttribute("BuyerOrganizationCode");
	    modifyBillToAddress(env, element, billToId, shipToId);
	    element.removeAttribute("BillToID");
	    
	}

	if(element.getAttribute("DocumentType").endsWith("0018.ex"))
	{
		try{
		
			
	YFCElement CorporatePersonInfo =YFCDocument.getDocumentFor(getShipToAddressForSC(env,element.
			getAttribute("ShipToID"))).getDocumentElement().getElementsByTagName("CorporatePersonInfo").item(0);
	
	String Address=CorporatePersonInfo.toString().replaceAll("CorporatePersonInfo", "PersonInfoShipTo");
	
	 final YFCNodeList<YFCElement> OrderLineNodeList = document.getElementsByTagName("OrderLine");
	 
	 int OrderLineListLength = OrderLineNodeList.getLength();
		
		if (OrderLineListLength > 0)
		{
			for (int i = 0; i < OrderLineListLength; i++)
			{
				final YFCNode cNode = OrderLineNodeList.item(i);
				final YFCElement OdrlineElem = (YFCElement) cNode;
				OdrlineElem.addXMLToNode(Address);
				
			}
			
			
		}
		
		}
	 catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		
	}
	
	
	
	
	return document.getDocument();
    }

    private void modifyBillToAddress(YFSEnvironment env, YFCElement element,
	    String strBillToId, String strShipToId) {

	if (!XmlUtils.isVoid(strShipToId) || !XmlUtils.isVoid(strBillToId)) {
	    Document billList;
	    try {
		billList = getCustomerSiteBillList(env, strBillToId,
			strShipToId);
		setBillToValues(element, billList);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    private Document getCustomerSiteBillList(YFSEnvironment env,
	    String strBillToId, String strShipToId) throws Exception {

	api = YIFClientFactory.getInstance().getApi();
	YFCDocument inputdoc = YFCDocument
		.getDocumentFor("<TerCustBillingOrg TerBillingID=\""
			+ strBillToId + "\" TerShipToOrg=\""+strShipToId+"\" />");
	return api.executeFlow(env, "getTerCustBillingOrgList",
		inputdoc.getDocument());
    }

    private void setBillToValues(YFCElement inDocEle, Document billListDoc) {

	YFCDocument yfcBillList = YFCDocument.getDocumentFor(billListDoc);
	YFCElement element = yfcBillList.getDocumentElement();

	YFCElement terBillToEle = element.getFirstChildElement();
	if (!XmlUtils.isVoid(terBillToEle)) {
	    YFCElement personBillTo = inDocEle.createChild("PersonInfoBillTo");

	    personBillTo.setAttribute("AddressID",
		    terBillToEle.getAttribute("TerBillingID"));
	    personBillTo.setAttribute("AddressLine1",
		    terBillToEle.getAttribute("TerAddressLine1"));
	    personBillTo.setAttribute("AddressLine2",
		    terBillToEle.getAttribute("TerAddressLine2"));
	    personBillTo.setAttribute("AddressLine3",
		    terBillToEle.getAttribute("TerAddressLine3"));
	    personBillTo.setAttribute("AddressLine4",
		    terBillToEle.getAttribute("TerAddressLine4"));
	    personBillTo.setAttribute("AddressLine5",
		    terBillToEle.getAttribute("TerAddressLine5"));
	    personBillTo.setAttribute("AddressLine6",
		    terBillToEle.getAttribute("TerAddressLine6"));
	    personBillTo.setAttribute("AlternateEmailID",
		    terBillToEle.getAttribute("TerAlternateEmailID"));
	    personBillTo.setAttribute("Beeper",
		    terBillToEle.getAttribute("TerBeeper"));
	    personBillTo.setAttribute("City",
		    terBillToEle.getAttribute("TerCity"));
	    personBillTo.setAttribute("State",
		    terBillToEle.getAttribute("TerState"));
	    personBillTo.setAttribute("Country",
		    terBillToEle.getAttribute("TerCountry"));
	    personBillTo.setAttribute("DayFaxNo",
		    terBillToEle.getAttribute("TerDayFaxNo"));
	    personBillTo.setAttribute("DayPhone",
		    terBillToEle.getAttribute("TerDayPhone"));
	    personBillTo.setAttribute("EMailID",
		    terBillToEle.getAttribute("TerEMailID"));
	    personBillTo.setAttribute("EveningFaxNo",
		    terBillToEle.getAttribute("TerEveningFaxNo"));
	    personBillTo.setAttribute("EveningPhone",
		    terBillToEle.getAttribute("TerEveningPhone"));
	    personBillTo.setAttribute("MobilePhone",
		    terBillToEle.getAttribute("TerMobilePhone"));
	    personBillTo.setAttribute("OtherPhone",
		    terBillToEle.getAttribute("TerOtherPhone"));
	    personBillTo.setAttribute("ZipCode",
		    terBillToEle.getAttribute("TerZipCode"));
	} 
	
	
	else {
		try{
		  
		   YFCElement BillingPersonInfo =YFCDocument.getDocumentFor( getBillToAddressForSC(sBuyer)).
				   getDocumentElement().getElementsByTagName("BillingPersonInfo").item(0);
			
			String Address2=BillingPersonInfo.toString().replaceAll("BillingPersonInfo", "PersonInfoBillTo");
			
			inDocEle.addXMLToNode(Address2);
		
		}
		
		 catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
	}
    }

    private Document getShipToAddressForSC(YFSEnvironment env,
    	     String strShipToId) throws Exception {

    	
    	YFCDocument inputdoc = YFCDocument
    		.getDocumentFor("<Organization   OrganizationCode=\""+strShipToId+"\" />");
    	final YFCDocument outputdoc = YFCDocument
				.getDocumentFor("<OrganizationList><Organization><CorporatePersonInfo AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\" AddressLine6=\"\"  Beeper=\"\" City=\"\" Company=\"\" Country=\"\"  DayFaxNo=\"\" DayPhone=\"\" Department=\"\" EMailID=\"\" EveningFaxNo=\"\" EveningPhone=\"\" FirstName=\"\"  JobTitle=\"\" LastName=\"\"  MiddleName=\"\" MobilePhone=\"\"  OtherPhone=\"\" State=\"\" Suffix=\"\"  ZipCode=\"\" /></Organization></OrganizationList>");
    	return invokeApi("getOrganizationList",
    		inputdoc.getDocument(),outputdoc.getDocument());
        }
    
    private Document getBillToAddressForSC(
   	     String strBillToId) throws Exception {

   	
   	YFCDocument inputdoc = YFCDocument
   		.getDocumentFor("<Organization   OrganizationCode=\""+strBillToId+"\" />");
   	final YFCDocument outputdoc = YFCDocument
				.getDocumentFor("<OrganizationList><Organization><BillingPersonInfo AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\" AddressLine6=\"\"  Beeper=\"\" City=\"\" Company=\"\" Country=\"\"  DayFaxNo=\"\" DayPhone=\"\" Department=\"\" EMailID=\"\" EveningFaxNo=\"\" EveningPhone=\"\" FirstName=\"\"  JobTitle=\"\" LastName=\"\"  MiddleName=\"\" MobilePhone=\"\"  OtherPhone=\"\" State=\"\" Suffix=\"\"  ZipCode=\"\" /></Organization></OrganizationList>");
   	return invokeApi("getOrganizationList",
   		inputdoc.getDocument(),outputdoc.getDocument());
       }
    
    
}
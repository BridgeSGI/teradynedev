package com.teradyne.om.api;

import java.util.ArrayList;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class TeradyneSourcingHelper extends CustomCodeHelper
{
	private static YFCLogCategory _cat = YFCLogCategory
			.instance("com.yantra.CustomCode");
	ArrayList<String> param =new ArrayList<String>();
	ArrayList<String> paramresult = new ArrayList<String>();
	private Document getItemListIP() throws YFSException {
		final YFCDocument getItemListIP = YFCDocument
				.getDocumentFor("<Item ItemID=\"\" UnitOfMeasure=\"\"/>");
		return getItemListIP.getDocument();
	}
	
	
	private Document getItemListOP() throws YFSException {
		final YFCDocument getItemListOP = YFCDocument
				.getDocumentFor("<ItemList TotalItemList=\"\"><Item ItemID=\"\" "
						+ "ItemKey=\"\" OrganizationCode=\"\" UnitOfMeasure=\"\"><Extn RepairCode=\"\" RepairModel=\"\" DefaultInventoryCenter=\"\"/>"
						+ "</Item></ItemList>");
		return getItemListOP.getDocument();
	}
	protected Boolean isUSLR(YFCElement orderlineElem, String repCode) {
		Boolean isUSLR = false;
System.out.println(orderlineElem.toString());
System.out.println(repCode);
		if ((orderlineElem.getChildElement("Extn")!=null&&!XmlUtils.isVoid(orderlineElem.getChildElement("Extn")
				.getAttribute("ServiceType")))
				&& orderlineElem.getChildElement("Extn")
						.getAttribute("ServiceType").equalsIgnoreCase("USL")
				&& repCode.equalsIgnoreCase("R")) {
			isUSLR = true;

		}

		return isUSLR;
	}

	protected YFCDocument getItemListCall(YFCDocument inXML)
			throws YFSException {
		YFCDocument ItemListOP = null;

		final Document getItemListOPtemp = getItemListOP();
	
		Document getItemListOP = null;
		getItemListOP = invokeApi("getItemList", inXML.getDocument(),getItemListOPtemp);
		ItemListOP = YFCDocument.getDocumentFor(getItemListOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getItemListCall call is:"
					+ ItemListOP.toString());
		}

		return ItemListOP;

	}
	protected ArrayList<String> getSourcingParams(YFCElement orderlineElem,YFSEnvironment arg0) {
		setEnv(arg0);
		String sUOM=null;
		String sItemID =null;
		System.out.println("getSourcingParams"+orderlineElem.toString());
		 sItemID = orderlineElem.getChildElement("Item").getAttribute("ItemID");
		if(!XmlUtils.isVoid(orderlineElem.getChildElement("Item").getAttribute(
				"UnitOfMeasure"))){
		
		
		
		 sUOM = orderlineElem.getChildElement("Item").getAttribute(
				"UnitOfMeasure");
		
		}
		
		else if(!XmlUtils.isVoid(orderlineElem.getChildElement("OrderLineTranQuantity").getAttribute(
				"TransactionalUOM"))){
		
		
		
		 sUOM = orderlineElem.getChildElement("OrderLineTranQuantity").getAttribute(
				"TransactionalUOM");
		
		}
		
		
	
		final YFCDocument getItemListInput = YFCDocument
				.getDocumentFor(getItemListIP());

		getItemListInput.getDocumentElement().setAttribute("ItemID", sItemID);
		getItemListInput.getDocumentElement().setAttribute("UnitOfMeasure",
				sUOM);
		
		
		
		final YFCDocument ItemListOutput = getItemListCall(getItemListInput);

		if (!ItemListOutput.getDocumentElement().getAttribute("TotalItemList")
				.equalsIgnoreCase("0")) {
			
			String sEXTNRPM = ItemListOutput.getDocumentElement()
					.getChildElement("Item").getChildElement("Extn")
					.getAttribute("RepairModel");
			param.add(sEXTNRPM);

			String	sEXTNDIC = ItemListOutput.getDocumentElement()
					.getChildElement("Item").getChildElement("Extn")
					.getAttribute("DefaultInventoryCenter");
			param.add(sEXTNDIC);
			String	sRepairCode = ItemListOutput.getDocumentElement()
					.getChildElement("Item").getChildElement("Extn")
					.getAttribute("RepairCode");
			param.add(sRepairCode);
		}

		return param;

	}
	protected Boolean isDistRule(String sDistId,YFSEnvironment arg0)

	{
		setEnv(arg0);
		Boolean isDistExits = false;
		Document getDistRuleList = null;
		YFCDocument getDistListOPtemp = YFCDocument
				.getDocumentFor("<ItemShipNodeList>"
						+ "<ItemShipNode DistributionRuleId=\"\" ActiveFlag=\"\"/>"
						+ "</ItemShipNodeList>");
		YFCDocument inputXMLDisList = YFCDocument
				.getDocumentFor("<ItemShipNode  ActiveFlag=\"Y\"  DistributionRuleId='"
						+ sDistId + "' />");
		getDistRuleList = invokeApi("getDistributionList",
				inputXMLDisList.getDocument(), getDistListOPtemp.getDocument());

		if (YFCDocument.getDocumentFor(getDistRuleList).getDocumentElement()
				.hasChildNodes()) {

			isDistExits = true;

		}
		return isDistExits;
	}


	protected String getRSCfromIBLinee(YFCDocument inXML,YFSEnvironment arg0) throws YFSException {
		setEnv(arg0);
		String RSC = null;
		YFCDocument getorderLineListOP = null;

		final Document getorderLineListOPTemp = getIBLineRSCOPTemp();
		Document getorderLineListOp = null;
		getorderLineListOp = invokeApi("getOrderLineList", inXML.getDocument(),
				getorderLineListOPTemp);
		getorderLineListOP = YFCDocument.getDocumentFor(getorderLineListOp);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("order line list for RSC is:"
					+ getorderLineListOP.toString());
		}
		if (getorderLineListOP.getDocumentElement().hasAttribute(
				"TotalLineList")) {
			int TotalLineList = Integer.parseInt(getorderLineListOP
					.getDocumentElement().getAttribute("TotalLineList"));

			if (TotalLineList > 0) {
				if (getorderLineListOP.getDocumentElement()
						.getElementsByTagName("Extn").item(0)
						.hasAttribute("RapidSourceCenter")) {

					RSC = getorderLineListOP.getDocumentElement()
							.getElementsByTagName("Extn").item(0)
							.getAttribute("RapidSourceCenter");

				}

			}
		}

		return RSC;

	}


	private Document getIBLineRSCOPTemp() throws YFSException {
		final YFCDocument getorderLineListOPTemp = YFCDocument
				.getDocumentFor("<OrderLineList TotalLineList=\"\">"
						+ "<OrderLine OrderLineKey=\"\">"
						+ "<Extn SystemSerialNo=\"\" RapidSourceCenter=\"\"  /></OrderLine>"
						+ "</OrderLineList>");
		return getorderLineListOPTemp.getDocument();

	}
	
	
	protected Document getIBLineSerialIP() throws YFSException {
		final YFCDocument getduplicateSerialIP = YFCDocument
				.getDocumentFor("<OrderLine StatusQryType=\"BETWEEN\" ToStatus=\"1100.30\" FromStatus=\"1000.10\"><Order DocumentType=\"0017.ex\" EnterpriseCode=\"CSO\"/><Item /><Extn "
						+ " /></OrderLine>");
		return getduplicateSerialIP.getDocument();

	}
	protected YFCDocument getCustomerListCall(YFCDocument inXML,YFSEnvironment arg0)
			throws YFSException {
	setEnv(arg0);
		YFCDocument CustListOP = null;

		final Document CALDayDtlscallOPTemp = getCustomerOP();
		Document CALDayDtlscallOP = null;
		CALDayDtlscallOP = invokeApi("getCustomerList", inXML.getDocument(),
				CALDayDtlscallOPTemp);
		CustListOP = YFCDocument.getDocumentFor(CALDayDtlscallOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getCustomerListCall call is:"
					+ CustListOP.toString());
		}

		return CustListOP;

	}

	protected Document getCustomerIP() throws YFSException {
		final YFCDocument getCustomerIP = YFCDocument
				.getDocumentFor("<Customer  />");
		return getCustomerIP.getDocument();
	}

	private Document getCustomerOP() throws YFSException {
		final YFCDocument getCustomerOP = YFCDocument
				.getDocumentFor("<CustomerList>"
						+ "<Customer  CustomerKey=\"\">"
						+ "<Extn DefaultOrderCenter=\"\"/>"
						+ "<CustomerAdditionalAddressList TotalNumberOfRecords=\"\">"
						+ "<CustomerAdditionalAddress CustomerAdditionalAddressID=\"\" IsShipTo=\"\" IsDefaultShipTo=\"\">"
						+ "<PersonInfo  Country=\"\" />"
						+ "</CustomerAdditionalAddress>"
						+ "</CustomerAdditionalAddressList>"
						+ "</Customer></CustomerList>");
		return getCustomerOP.getDocument();
	}

	protected Boolean isNonExpress(String sServiceType) throws YFSException{
		Boolean isNonExpress=false;
		final YFCDocument getNonExpSrvcIP = YFCDocument
				.getDocumentFor("<CommonCode CodeType=\"NONEXPSRVC\" CodeValue= '" + sServiceType+ "'/>");
		final YFCDocument getNonExpSrvcOP = YFCDocument
				.getDocumentFor("<CommonCodeList><CommonCode  CodeValue=\"\" CodeType=\"\" /></CommonCodeList>");
		Document NonExpSrvdOP = null;
		NonExpSrvdOP = invokeApi("getCommonCodeList", getNonExpSrvcIP.getDocument(),
				getNonExpSrvcOP.getDocument());
if(YFCDocument.getDocumentFor(NonExpSrvdOP).getDocumentElement().hasChildNodes())
{
	
	
	isNonExpress=true;
	
}

		
		
		return isNonExpress;
	}

}

package com.teradyne.om.api;

import java.rmi.RemoteException;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.SterlingUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;


public class RepairProcessRegular {

	Document outDoc=null;
	
	public void repairProcessRegular(YFSEnvironment env,Document inDoc) {
		
		String sReturnOrderHeaderKey="";
		String sOrderLineKey="";
		String sServiceType="";
		String sEnterpriseCode="";
		String sOrderedQty="";
		String sShipNode="";
		String sItemID="";
		String sPdtClass="";
		String sUOM="";
		String sBuyerOrg="";
		String sSerialNo="";
		String sShipToKey="";
		String sBillToKey="";
		String sCreateOrderApi="createOrder";
		YFCDocument inputOrder=null;
		YFCDocument TempOrdDet=null;
		Boolean invokeEO=false;
		//Document outputOrder=null;
		try{
			
			YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleInput = inYFCDoc.getDocumentElement();
			
			if (isDirectShipAllowed(env, eleInput.getAttribute(XMLConstants.RECEIVING_NODE))) {
				YFCElement eleRecpLines = eleInput.getChildElement("ReceiptLines");
				
				
				/*inputOrder = YFCDocument.getDocumentFor("<Order BillToKey=\"\" ShipToKey=\"\" DraftOrderFlag=\"\" BuyerOrganizationCode=\"\" EnterpriseCode=\"\" DocumentType=\"\" IgnoreOrdering=\"\" ReturnOrderHeaderKeyForExchange=\"\" ExchangeType=\"\" OrderPurpose=\"\" >" +
					"<OrderLines><OrderLine SerialNo=\"\" ShipNode=\"\" OrderedQty=\"\"> <Item ItemID=\"\" ProductClass=\"\" UnitOfMeasure=\"\" /></OrderLine></OrderLines></Order>");
				 */
				inputOrder = YFCDocument.createDocument("Order");
				YFCElement orderElement = inputOrder.getDocumentElement();
				
				YFCElement orderLinesEle = orderElement.createChild("OrderLines");
				
				YFCIterable<YFCElement> allInputline = eleRecpLines.getChildren();
				for (YFCElement eleRecpLine : allInputline){
					
					//YFCElement eleRecpLine = eleRecpLines.getChildElement("ReceiptLine");
					
					
					sReturnOrderHeaderKey= eleRecpLine.getAttribute("OrderHeaderKey");
					sEnterpriseCode=eleRecpLine.getAttribute("EnterpriseCode");
					sOrderedQty=eleRecpLine.getAttribute("Quantity");
					sOrderLineKey= eleRecpLine.getAttribute("OrderLineKey");
					sItemID= eleRecpLine.getAttribute("ItemID");
					sPdtClass= eleRecpLine.getAttribute("ProductClass");
					sUOM= eleRecpLine.getAttribute("UnitOfMeasure");
					
					
					
					YFCElement eleOrdLine = eleRecpLine.getChildElement("OrderLine");
					
					sSerialNo=eleOrdLine.getAttribute("SerialNo");
					sShipNode=eleOrdLine.getAttribute("ShipNode");
					
					
					YFCElement eleExtnOrderLines = eleOrdLine.getChildElement("Extn");
					if(!YFCElement.isVoid(eleExtnOrderLines)){
						sServiceType= eleExtnOrderLines.getAttribute("ServiceType");
					}
					
					if(sServiceType.isEmpty()){
						//Calling the function to get the servicetype
						if(!YFCElement.isVoid(sOrderLineKey)){
							sServiceType = getServiceType(sOrderLineKey,env);
						}
					}
					
					if(!sServiceType.isEmpty() && sServiceType !=null){
						
						if("TPS".equalsIgnoreCase(sServiceType) || "ESWP".equalsIgnoreCase(sServiceType) || "SCAL".equalsIgnoreCase(sServiceType)
								|| "QEWP".equalsIgnoreCase(sServiceType) ){
							
							YFCElement orderLineEle = orderLinesEle.createChild("OrderLine");
							orderLineEle.setAttribute("ShipNode", sShipNode);
							orderLineEle.setAttribute("OrderedQty", sOrderedQty);
							
							if ("ESWP".equalsIgnoreCase(sServiceType)){
								orderLineEle.setAttribute("SerialNo", sSerialNo);
							}
							
							YFCElement itemEle = orderLineEle.createChild("Item");
							itemEle.setAttribute("ItemID", sItemID);
							itemEle.setAttribute("ProductClass", sPdtClass);
							itemEle.setAttribute("UnitOfMeasure", sUOM);
							//orderLinesEle.appendChild(orderLineEle);
						}
					}
					
					
					YFCElement eleShipment = eleInput.getChildElement("Shipment");
					sBuyerOrg=eleShipment.getAttribute("BuyerOrganizationCode");
					
					String apiName1 = "getOrderDetails";
					YFCDocument inpDoc = YFCDocument.getDocumentFor("<Order OrderHeaderKey=\"\" />");
					inpDoc.getDocumentElement().setAttribute("OrderHeaderKey", sReturnOrderHeaderKey);
					
					TempOrdDet = YFCDocument.getDocumentFor("<Order BillToKey=\"\" ShipToKey=\"\" />");
					env.setApiTemplate(apiName1,TempOrdDet.getDocument());
					
					outDoc = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inpDoc.getDocument());
					env.clearApiTemplate(apiName1);
					
					if (outDoc !=null){
						YFCElement OrdDets = YFCDocument.getDocumentFor(outDoc).getDocumentElement();
						if (OrdDets != null){
							sBillToKey=OrdDets.getAttribute("BillToKey");
							sShipToKey=OrdDets.getAttribute("ShipToKey");
						}
					}
					
					orderElement.setAttribute("EnterpriseCode", sEnterpriseCode);
					orderElement.setAttribute("BuyerOrganizationCode", sBuyerOrg);
					orderElement.setAttribute("DocumentType", "0001");
					orderElement.setAttribute("IgnoreOrdering", "Y");
					orderElement.setAttribute("ReturnOrderHeaderKeyForExchange", sReturnOrderHeaderKey);
					orderElement.setAttribute("ExchangeType", "REGULAR");
					orderElement.setAttribute("OrderPurpose", "EXCHANGE");
					orderElement.setAttribute("BillToKey", sBillToKey);
					orderElement.setAttribute("ShipToKey", sShipToKey);
					
					
					invokeEO=true;
				}
				
				System.out.println("Input Order for exchange ======" +inputOrder.toString() );
				
				if(invokeEO){
					YIFClientFactory.getInstance().getLocalApi().invoke(env, sCreateOrderApi,inputOrder.getDocument());
					env.clearApiTemplate(sCreateOrderApi);
				} 
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("######### Exception Caught : " + e.toString());
		}
		
		
		
		
	}
	
	private boolean isDirectShipAllowed(YFSEnvironment env, String customer) throws Exception {
		String isIBDirectShipAllowed = "N";
		YFCDocument getCustomerListInputDoc = YFCDocument.parse("<Customer CustomerID=\"" + customer + "\"/>");
		YFCDocument getCustomerListOutputTemplateDoc = YFCDocument.parse(
				"<CustomerList>" +
					"<Customer CustomerID=\"\">" +
						"<Extn IsInboundDirectShipAllowed=\"\"/>" +
					"</Customer>" +
				"</CustomerList>");
		//System.out.println("Calling getCustomerList with input :\n" + getCustomerListInputDoc + "\nand template:\n" + getCustomerListOutputTemplateDoc);
		YFCDocument getCustomerListOutputDoc = SterlingUtil.callAPI(env, Constants.GET_CUSTOMER_LIST, getCustomerListInputDoc, getCustomerListOutputTemplateDoc);
		//System.out.println("getCustomerList output: " + getCustomerListOutputDoc);
		YFCElement customerList = getCustomerListOutputDoc.getDocumentElement();
		YFCElement customerFromOutput = customerList.getChildElement(Constants.CUSTOMER);
		if (!XmlUtils.isVoid(customerFromOutput)) {
			YFCElement extn = customerFromOutput.getChildElement(XMLConstants.EXTN);
			isIBDirectShipAllowed = extn.getAttribute(XMLConstants.IS_INBOUND_DIRECT_SHIP_ALLOWED);
		}
		return isIBDirectShipAllowed.equals(Constants.Y);
	}
	
	public String getServiceType(String ordLineKey,YFSEnvironment e1) throws YFSException, RemoteException, YIFClientCreationException{
		String sevType="";
		String sSalesOrdLineKey="";
		String sGetOrderLineDetailApi = "getOrderLineDetails";
		YFCDocument ordDet=null;
		YFCDocument salesOrdLine=null;
		YFCDocument tempOrdDet=null;
		YFCDocument tempSalesOrdLine=null;
		Document outputOrdDet=null;
		Document outSalDet=null;
		
		
		/*From return orderlinekey getting the salesorderlinekey. 
		 
		 */
		if(ordLineKey != null || ordLineKey.isEmpty()){
		ordDet = YFCDocument.getDocumentFor("<OrderLineDetail OrderLineKey=\"\"/>");
		ordDet.getDocumentElement().setAttribute("OrderLineKey", ordLineKey);
		System.out.println("#####ordDet"	+ ordDet);
		tempOrdDet = YFCDocument.getDocumentFor("<OrderLine DerivedFromOrderLineKey=\"\" />");
		e1.setApiTemplate(sGetOrderLineDetailApi,tempOrdDet.getDocument());
		outputOrdDet = YIFClientFactory.getInstance().getLocalApi().invoke(e1, sGetOrderLineDetailApi,ordDet.getDocument());
		
		YFCElement outOrderLineDet= YFCDocument.getDocumentFor(outputOrdDet).getDocumentElement();
		if (!YFCElement.isVoid(outOrderLineDet)) {
			System.out.println("###########"+ outputOrdDet);
			sSalesOrdLineKey = outOrderLineDet.getAttribute("DerivedFromOrderLineKey");
			System.out.println("Sales order line key----" +sSalesOrdLineKey );

		}
		/* From the Sales OrderLinekey getting Order_Line.Extn_Service_Type field value*/
		if(sSalesOrdLineKey!=null){
			salesOrdLine = YFCDocument.getDocumentFor("<OrderLineDetail OrderLineKey=\"\"/>");
			salesOrdLine.getDocumentElement().setAttribute("OrderLineKey", sSalesOrdLineKey);
			tempSalesOrdLine = YFCDocument.getDocumentFor("<OrderLine OrderLineKey=\"\"><Extn ServiceType=\"\"/> </OrderLine>");
			e1.setApiTemplate(sGetOrderLineDetailApi,tempSalesOrdLine.getDocument());
			outSalDet = YIFClientFactory.getInstance().getLocalApi().invoke(e1, sGetOrderLineDetailApi,salesOrdLine.getDocument());
			
			YFCElement outSalOrdLineDet= YFCDocument.getDocumentFor(outSalDet).getDocumentElement();
			if (!YFCElement.isVoid(outSalOrdLineDet)){
				
				YFCElement eleExtn = outSalOrdLineDet.getChildElement("Extn");
				if(!YFCElement.isVoid(eleExtn)){
					sevType= eleExtn.getAttribute("ServiceType");
					System.out.println("Sales order ServiceType----" +sevType );
				}
				
			}
		}
		}
		
		return sevType;
	}
	
}

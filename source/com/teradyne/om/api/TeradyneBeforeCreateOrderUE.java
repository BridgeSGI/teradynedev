package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

public class TeradyneBeforeCreateOrderUE extends CustomCodeHelper implements YFSBeforeCreateOrderUE {
	private static YFCLogCategory _cat = YFCLogCategory.instance("com.yantra.CustomCode");

	public Document beforeCreateOrder(YFSEnvironment arg0, Document inXML) throws YFSUserExitException {
		setEnv(arg0);

		int OrderLineListLength = 0;
		YFCDocument inYFCXML = null;
		String sBuyerOrg = null;
		String sEXTNRC = null;
		String sEXTNRPC = null;
		String sEXTNRSC = null;
		String sCountry = null;
		String sItemID=null;

		inYFCXML = YFCDocument.getDocumentFor(inXML);


		if (inYFCXML.getDocumentElement().hasAttribute("BuyerOrganizationCode")) {

			sBuyerOrg = inYFCXML.getDocumentElement().getAttribute("BuyerOrganizationCode");

			if (sBuyerOrg.length() != 0) {

				final YFCDocument getCustListInput = YFCDocument.getDocumentFor(getCustomerIP());
				getCustListInput.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrg);
				final YFCDocument CustomerListOutput = getCustomerListCall(getCustListInput);
				if (CustomerListOutput.getDocumentElement().hasChildNodes()) {
					sEXTNRC = CustomerListOutput.getDocumentElement().getChildElement("Customer").getChildElement("Extn").getAttribute("RepairCenter");
					sCountry = CustomerListOutput.getDocumentElement().getChildElement("Customer").getChildElement("BuyerOrganization").getChildElement("ContactPersonInfo").getAttribute("Country");

				}
			}

		}

		final YFCNodeList<YFCElement> OrderLineNodeList = inYFCXML
				.getDocumentElement().getElementsByTagName("OrderLine");

		OrderLineListLength = OrderLineNodeList.getLength();
		if (OrderLineListLength > 0)

		{
			for (int i = 0; i < OrderLineListLength; i++)

			{

				final YFCNode cNode = OrderLineNodeList.item(i);

				if (cNode.getNodeName().equalsIgnoreCase("OrderLine")) {
					if (cNode instanceof YFCElement) {
						final YFCElement OdrlineElem = (YFCElement) cNode;
						try {

							if(OdrlineElem.getElementsByTagName("Extn").getLength()!=0)
							{
								if(OdrlineElem.getChildElement("Extn").getAttribute("RapidSourceCenter").length()!=0
										&&!OdrlineElem.getChildElement("Extn").getAttribute("RapidSourceCenter").equalsIgnoreCase(" "))
								{
									sEXTNRSC=OdrlineElem.
											getChildElement("Extn").getAttribute("RapidSourceCenter");
									OdrlineElem.setAttribute("ShipNode", sEXTNRSC);


								}

								else if(sCountry!=null&&sEXTNRC!=null)
								{

									sItemID=OdrlineElem.getChildElement("Item").getAttribute("ItemID");	
									String sUOM=OdrlineElem.getChildElement("OrderLineTranQuantity")
											.getAttribute("TransactionalUOM");	
									final YFCDocument getItemListInput = YFCDocument
											.getDocumentFor(getItemListIP());

									getItemListInput.getDocumentElement().setAttribute("ItemID", sItemID);
									getItemListInput.getDocumentElement().setAttribute("UnitOfMeasure", sUOM);

									final YFCDocument ItemListOutput 	=	getItemListCall(getItemListInput);

									if(!ItemListOutput.getDocumentElement().getAttribute("TotalItemList").equalsIgnoreCase("0"))
									{
										sEXTNRPC = ItemListOutput.getDocumentElement()
												.getChildElement("Item")
												.getChildElement("Extn").getAttribute("RepairCode");

									}

									if(sEXTNRPC.length()!=0&&!sEXTNRPC.equalsIgnoreCase(" "))
									{


										OdrlineElem.setAttribute("DistributionRuleId", sEXTNRC+sEXTNRPC+sCountry);


									}

								}

							}

							else if(sCountry!=null&&sEXTNRC!=null)
							{

								sItemID=OdrlineElem.getChildElement("Item").getAttribute("ItemID");	
								String sUOM=OdrlineElem.getChildElement("OrderLineTranQuantity")
										.getAttribute("TransactionalUOM");	
								final YFCDocument getItemListInput = YFCDocument
										.getDocumentFor(getItemListIP());

								getItemListInput.getDocumentElement().setAttribute("ItemID", sItemID);
								getItemListInput.getDocumentElement().setAttribute("UnitOfMeasure", sUOM);

								final YFCDocument ItemListOutput 	=	getItemListCall(getItemListInput);

								if(!ItemListOutput.getDocumentElement().getAttribute("TotalItemList").equalsIgnoreCase("0"))
								{
									sEXTNRPC = ItemListOutput.getDocumentElement()
											.getChildElement("Item")
											.getChildElement("Extn").getAttribute("RepairCode");

								}

								if(sEXTNRPC.length()!=0&&!sEXTNRPC.equalsIgnoreCase(" "))
								{


									OdrlineElem.setAttribute("DistributionRuleId", sEXTNRC+sEXTNRPC+sCountry);


								}

							}





						} catch (final Exception e) {
							e.printStackTrace();
							try {
								throw e;
							} catch (final Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();

							}
						}
					}
				}
			}
		}

		return inYFCXML.getDocument();
	}

	protected YFCDocument getCustomerListCall(YFCDocument inXML)
			throws YFSException {
		YFCDocument CustListOP = null;

		final Document CALDayDtlscallOPTemp = getCustomerOP();
		Document CALDayDtlscallOP = null;
		CALDayDtlscallOP = invokeApi("getCustomerList", inXML.getDocument(),
				CALDayDtlscallOPTemp);
		CustListOP = YFCDocument.getDocumentFor(CALDayDtlscallOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getcalendardaydtls call is:"
					+ CustListOP.toString());
		}

		return CustListOP;

	}
	protected YFCDocument getItemListCall(YFCDocument inXML)
			throws YFSException {
		YFCDocument ItemListOP = null;

		final Document getItemListOPtemp = getItemListOP();
		Document getItemListOP = null;
		getItemListOP = invokeApi("getItemList", inXML.getDocument(),
				getItemListOPtemp);
		ItemListOP = YFCDocument.getDocumentFor(getItemListOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getcalendardaydtls call is:"
					+ ItemListOP.toString());
		}

		return ItemListOP;

	}

	private Document getCustomerIP() throws YFSException {
		final YFCDocument getCustomerIP = YFCDocument
				.getDocumentFor("<Customer  />");
		return getCustomerIP.getDocument();
	}

	private Document getCustomerOP() throws YFSException {
		final YFCDocument getCustomerOP = YFCDocument
				.getDocumentFor("<CustomerList><Customer  CustomerKey=\"\"><Extn RepairCenter=\"\"/><BuyerOrganization "
						+ "OrganizationName=\"\" ParentOrganizationCode=\"\"><ContactPersonInfo Country=\"\" />"
						+ "</BuyerOrganization></Customer></CustomerList>");
		return getCustomerOP.getDocument();
	}
	private Document getItemListIP() throws YFSException {
		final YFCDocument getItemListIP = YFCDocument
				.getDocumentFor("<Item ItemID=\"\" UnitOfMeasure=\"\"/>");
		return getItemListIP.getDocument();
	}

	private Document getItemListOP() throws YFSException {
		final YFCDocument getItemListOP = YFCDocument
				.getDocumentFor("<ItemList TotalItemList=\"\"><Item ItemID=\"\" "
						+ "ItemKey=\"\" OrganizationCode=\"\" UnitOfMeasure=\"\"><Extn RepairCode=\"\"/>"
						+ "</Item></ItemList>");
		return getItemListOP.getDocument();
	}
	@Override
	public String beforeCreateOrder(YFSEnvironment arg0, String arg1)
			throws YFSUserExitException {
		// TODO Auto-generated method stub
		return null;
	}

}

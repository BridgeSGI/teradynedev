/***********************************************************************************************
 * File	Name		: SPWPGetFDLSEmailWebService.java
 *
 * Description		: This class is called when external system invoke SPWP_GetFDLSEmail web
 * 						service. Java code will call getItemList and return 
 * 						Item/Extn/@SystemFailureDistList
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		May 16,2014	  	Tushar Dhaka 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 **********************************************************************************************/
package com.teradyne.om.api;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sterlingcommerce.baseutil.SCXmlUtil;
import com.teradyne.utils.Constants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class SPWPGetFDLSEmailWebService 
{
	private static final YFCLogCategory logger = YFCLogCategory.instance(SPWPGetFDLSEmailWebService.class);
	private String strItemListTemplate = "<ItemList><Item ItemID=\"\"><Extn "+Constants.SYSTEM_FAILURE_DIST_LIST+"=\"\"/></Item></ItemList>";
	
	public Document callGetFDLSEmailService(YFSEnvironment env, Document indoc) throws Exception
	{	
		logger.beginTimer("Start::SPWPGetFDLSEmailWebService callGetFDLSEmailService()");
		logger.debug("Start callGetFDLSEmailService method");
		
		/*
		 * Input XML:-
		 * <Input System_Type_Code="" System_Serial_No=""/>
		 * 
		 * Output XML:-
		 * <Output System_Type_Code="" Failure_Dist_List="" System_Serial_No=""/>
		 */
		
		
		Element eleInput = indoc.getDocumentElement();
		String strSystemTypeCode = eleInput.getAttribute(Constants.SYSTEM_TYPE_CODE);
		String strSystemSerialNo = eleInput.getAttribute(Constants.SYSTEM_SERIAL_NO);
		
		//call getItemList to get SystemFailureDistList for the item
		//<Item ItemID="System_Type_Code"/>
		/*
		 * Output Template
		 * <ItemList>
			    <Item ItemID="">
			        <Extn SystemFailureDistList=""/>
			    </Item>
			</ItemList>

		 */
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document docGetItemList = docBuilder.newDocument();
		Element eleItemInput = docGetItemList.createElement("Item");
		eleItemInput.setAttribute("ItemID", strSystemTypeCode);
		docGetItemList.appendChild(eleItemInput);
		
		Document docGetItemListTemplate = SCXmlUtil.createFromString(strItemListTemplate);
					
		env.setApiTemplate("getItemList", docGetItemListTemplate);
		YIFApi api = YIFClientFactory.getInstance().getApi();
        Document docGetItemListOutput = api.invoke(env, "getItemList", docGetItemList);
        env.clearApiTemplate("getItemList");
        
        Element eleExtn = (Element)docGetItemListOutput.getElementsByTagName("Extn").item(0);

        String strSystemFailureDistList = eleExtn.getAttribute(Constants.SYSTEM_FAILURE_DIST_LIST);
		
        //create output xml and return it
		Document docOutputXML = docBuilder.newDocument();
		Element eleOutput = docOutputXML.createElement("Output");
		eleOutput.setAttribute("System_Type_Code", strSystemTypeCode);
		eleOutput.setAttribute("Failure_Dist_List", strSystemFailureDistList);
		if(!strSystemSerialNo.equals(""))
		{
			eleOutput.setAttribute("System_Serial_No", strSystemSerialNo);
		}
		docOutputXML.appendChild(eleOutput);
		
		logger.endTimer("Start::SPWPGetFDLSEmailWebService callGetFDLSEmailService()");
		logger.debug("End changeStatusCall method");
		
		return docOutputXML;
	}
}
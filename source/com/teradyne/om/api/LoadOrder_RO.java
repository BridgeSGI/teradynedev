// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 9/5/2014 10:15:14 AM
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   LoadOrder.java

package com.teradyne.om.api;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import java.io.PrintStream;
import org.w3c.dom.Document;

public class LoadOrder_RO
{

    public LoadOrder_RO()
    {
        api = null;
    }

    public Document loadOrders(YFSEnvironment env, Document inDoc)
    {
        YFCDocument document = YFCDocument.getDocumentFor(inDoc);
        YFCElement element = document.getDocumentElement();
        if(!XmlUtils.isVoid(element))
        {
        	String recNode=element.getAttribute("ReceivingNode");
        	element.setAttribute("ShipNode", recNode);
            String billToId = element.getAttribute("BillToID");
            String shipToId = element.getAttribute("ShipToID");
            modifyBillToAddress(env, element, billToId, shipToId);
            element.removeAttribute("BillToID");
        }
        return document.getDocument();
    }

    private void modifyBillToAddress(YFSEnvironment env, YFCElement element, String strBillToId, String strShipToId)
    {
        if(!XmlUtils.isVoid(strShipToId) || !XmlUtils.isVoid(strBillToId))
            try
            {
                Document billList = getCustomerSiteBillList(env, strBillToId, strShipToId);
                setBillToValues(element, billList);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
    }

    private Document getCustomerSiteBillList(YFSEnvironment env, String strBillToId, String strShipToId)
        throws Exception
    {
        api = YIFClientFactory.getInstance().getApi();
        YFCDocument inputdoc = YFCDocument.getDocumentFor((new StringBuilder()).append("<TerCustBillingOrg TerBillingID=\"").append(strBillToId).append("\" TerShipToOrg=\"").append(strShipToId).append("\" />").toString());
        return api.executeFlow(env, "getTerCustBillingOrgList", inputdoc.getDocument());
    }

    private void setBillToValues(YFCElement inDocEle, Document billListDoc)
    {
        YFCDocument yfcBillList = YFCDocument.getDocumentFor(billListDoc);
        YFCElement element = yfcBillList.getDocumentElement();
        YFCElement terBillToEle = element.getFirstChildElement();
        if(!XmlUtils.isVoid(terBillToEle))
        {
            YFCElement personBillTo = inDocEle.createChild("PersonInfoBillTo");
            personBillTo.setAttribute("AddressID", terBillToEle.getAttribute("TerBillingID"));
            personBillTo.setAttribute("AddressLine1", terBillToEle.getAttribute("TerAddressLine1"));
            personBillTo.setAttribute("AddressLine2", terBillToEle.getAttribute("TerAddressLine2"));
            personBillTo.setAttribute("AddressLine3", terBillToEle.getAttribute("TerAddressLine3"));
            personBillTo.setAttribute("AddressLine4", terBillToEle.getAttribute("TerAddressLine4"));
            personBillTo.setAttribute("AddressLine5", terBillToEle.getAttribute("TerAddressLine5"));
            personBillTo.setAttribute("AddressLine6", terBillToEle.getAttribute("TerAddressLine6"));
            personBillTo.setAttribute("AlternateEmailID", terBillToEle.getAttribute("TerAlternateEmailID"));
            personBillTo.setAttribute("Beeper", terBillToEle.getAttribute("TerBeeper"));
            personBillTo.setAttribute("City", terBillToEle.getAttribute("TerCity"));
            personBillTo.setAttribute("State", terBillToEle.getAttribute("TerState"));
            personBillTo.setAttribute("Country", terBillToEle.getAttribute("TerCountry"));
            personBillTo.setAttribute("DayFaxNo", terBillToEle.getAttribute("TerDayFaxNo"));
            personBillTo.setAttribute("DayPhone", terBillToEle.getAttribute("TerDayPhone"));
            personBillTo.setAttribute("EMailID", terBillToEle.getAttribute("TerEMailID"));
            personBillTo.setAttribute("EveningFaxNo", terBillToEle.getAttribute("TerEveningFaxNo"));
            personBillTo.setAttribute("EveningPhone", terBillToEle.getAttribute("TerEveningPhone"));
            personBillTo.setAttribute("MobilePhone", terBillToEle.getAttribute("TerMobilePhone"));
            personBillTo.setAttribute("OtherPhone", terBillToEle.getAttribute("TerOtherPhone"));
            personBillTo.setAttribute("ZipCode", terBillToEle.getAttribute("TerZipCode"));
        } else
        {
            System.out.println("Bill to is not configured.");
        }
    }

    private YIFApi api;
}

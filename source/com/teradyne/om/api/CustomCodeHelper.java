package com.teradyne.om.api;

import java.rmi.RemoteException;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * BSG generic Helper class-Custom Code Helper.
 * 
 * @author Pranav Kumar Singh For BSG
 * @version 2.0
 * @since 0.0
 */
public class CustomCodeHelper implements YIFCustomApi {
	protected Properties oProperties;
	private static YFCLogCategory _cat = YFCLogCategory.instance("com.yantra.CustomCode");
	private static YIFApi _Api = null;
	private YFSEnvironment _env = null;
	String ShpDate = null;
	Boolean NonWorkingDay = null;
	YFCDocument calDayDtls = null;
	private YIFApi getApi() throws YFSException {
		if (_Api == null) {
			try {
				_Api = YIFClientFactory.getInstance().getLocalApi();
			} catch (final YIFClientCreationException cex) {
				cex.printStackTrace();
				throw new YFSException(cex.getMessage());
			} catch (final Exception ex) {
				ex.printStackTrace();
				throw new YFSException(ex.getMessage());
			}
		}
		return _Api;
	}

	public void setEnv(YFSEnvironment env) {
		this._env = env;
	}

	protected YFSEnvironment getEnv() {
		return this._env;
	}

	protected Document invokeApi(String apiName, Document inDoc)
			throws YFSException {
		return invokeApi(apiName, inDoc, "");
	}

	protected Document invokeApi(String apiName, Document inDoc,
			Document templateDoc) throws YFSException {
		Document outDoc = null;
		try {
			if (templateDoc != null) {
				getEnv().setApiTemplate(apiName, templateDoc);
			}

			try {
				outDoc = getApi().invoke(getEnv(), apiName, inDoc);

			} catch (final YFSException e) {

				if (_cat.isVerboseEnabled()) {
					_cat.verbose("invoke API error:" + apiName);
				}
				e.printStackTrace();
				throw e;
			}

			if (templateDoc != null) {
				getEnv().clearApiTemplate(apiName);
			}

		}

		catch (final RemoteException rex) {
			rex.printStackTrace();
			throw new YFSException(rex.getMessage());
		} catch (final Exception ex) {
			ex.printStackTrace();
			throw new YFSException(ex.getMessage());

		}
		return outDoc;
	}

	protected Document invokeApi(String apiName, Document inDoc,
			String templateFileName) throws YFSException {
		Document outDoc = null;
		try {
			if (!isVoid(templateFileName)) {
				getEnv().setApiTemplate(apiName, templateFileName);
			}

			outDoc = getApi().invoke(getEnv(), apiName, inDoc);

			if (!isVoid(templateFileName)) {
				getEnv().clearApiTemplate(apiName);
			}

		} catch (final RemoteException rex) {
			rex.printStackTrace();
			throw new YFSException(rex.getMessage());
		} catch (final Exception ex) {
			ex.printStackTrace();
			throw new YFSException(ex.getMessage());
		}
		return outDoc;
	}

	protected boolean isVoid(String value) {
		boolean ret = false;
		if (value == null || value.equals("")) {
			ret = true;
		}
		return ret;
	}

	protected Document executeFlow(String flowName, Document inDoc)
			throws YFSException {
		Document outDoc = null;
		try {
			outDoc = getApi().executeFlow(getEnv(), flowName, inDoc);
		} catch (final RemoteException rex) {
			rex.printStackTrace();
			throw new YFSException(rex.getMessage());
		} catch (final Exception ex) {
			ex.printStackTrace();
			throw new YFSException(ex.getMessage());
		}
		return outDoc;
	}

	@Override
	public void setProperties(Properties properties) {
		oProperties = new Properties();
		if (properties != null) {
			oProperties = properties;
		}
	}

	protected String getParameter(String key) {
		String result = null;

		if (!oProperties.isEmpty()) {
			result = oProperties.getProperty(key);
		}
		return result;
	}
	
	
}
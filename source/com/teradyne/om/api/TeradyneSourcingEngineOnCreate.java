package com.teradyne.om.api;

import java.util.ArrayList;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.api.CustomCodeHelper;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

public class TeradyneSourcingEngineOnCreate extends CustomCodeHelper implements
		YFSBeforeCreateOrderUE {
	private static YFCLogCategory _cat = YFCLogCategory
			.instance("com.yantra.CustomCode");
	String sEXTDOC = null;
	Boolean isExpSrvc = false;
	String sEXTNRPM = null;
	String sRepairCode = null;
	String sItemID = null;
	String sEXTNDIC = null;
	String sExchangeType = null;
	ArrayList<String> param = null;
	ArrayList<String> paramresult = null;

	public Document beforeCreateOrder(YFSEnvironment arg0, Document inXML)
			throws YFSUserExitException {
		setEnv(arg0);
		final TeradyneSourcingHelper teradyneSourcingHelper = new TeradyneSourcingHelper();
		int OrderLineListLength = 0;
		YFCDocument inYFCXML = null;
		String sBuyerOrg = null;

		inYFCXML = YFCDocument.getDocumentFor(inXML);
		/*
		 * to be coded logic for Install base line save and proceed
		 */
		if (_cat.isVerboseEnabled()) {
			_cat.verbose("=============@Author Pranav For BRIDGESGI=================");
			_cat.verbose("in the sourcing engine on create");
			_cat.verbose("==============================" + inYFCXML.toString());

		}

		if (inYFCXML.getDocumentElement().getAttribute("DocumentType")
				.equalsIgnoreCase("0001")) {

			if (!(XmlUtils.isVoid(inYFCXML.getDocumentElement().getAttribute(
					"ExchangeType")))) {

				sExchangeType = inYFCXML.getDocumentElement().getAttribute(
						"ExchangeType");

			}

			if (inYFCXML.getDocumentElement().hasAttribute("ReceivingNode")
					&& inYFCXML.getDocumentElement()
							.getAttribute("ReceivingNode").length() > 0) {

				sBuyerOrg = inYFCXML.getDocumentElement().getAttribute(
						"ReceivingNode");

				if (sBuyerOrg.length() != 0) {

					final YFCDocument getCustListInput = YFCDocument
							.getDocumentFor(teradyneSourcingHelper
									.getCustomerIP());
					getCustListInput.getDocumentElement().setAttribute(
							"CustomerID", sBuyerOrg);
					final YFCDocument CustomerListOutput = teradyneSourcingHelper
							.getCustomerListCall(getCustListInput, arg0);
					if (CustomerListOutput.getDocumentElement().hasChildNodes()) {
						sEXTDOC = CustomerListOutput.getDocumentElement()
								.getChildElement("Customer")
								.getChildElement("Extn")
								.getAttribute("DefaultOrderCenter");

					}
				}

			}

			final YFCNodeList<YFCElement> OrderLineNodeList = inYFCXML
					.getDocumentElement().getElementsByTagName("OrderLine");

			OrderLineListLength = OrderLineNodeList.getLength();
			if (OrderLineListLength > 0)

			{
				for (int i = 0; i < OrderLineListLength; i++)

				{

					final YFCNode cNode = OrderLineNodeList.item(i);

					if (cNode.getNodeName().equalsIgnoreCase("OrderLine")) {
						if (cNode instanceof YFCElement) {
							final YFCElement OdrlineElem = (YFCElement) cNode;
							try {
								
								if (!(OdrlineElem
										.hasAttribute("ShipNode") && OdrlineElem
										.getAttribute("ShipNode")
										.length() > 0)) {

								if (OdrlineElem.getChildElement("Extn")
										.hasAttribute("SystemSerialNo")
										&& OdrlineElem.getChildElement("Extn")
												.getAttribute("SystemSerialNo")
												.length() > 0) {
									String sSysSerialNo = OdrlineElem
											.getChildElement("Extn")
											.getAttribute("SystemSerialNo");
									sItemID = OdrlineElem.getChildElement(
											"Item").getAttribute("ItemID");
									/*
									 * get Rcenter from IB line with serial
									 * number and item id status combination
									 */
									final YFCDocument getIBLineerialInput = YFCDocument
											.getDocumentFor(teradyneSourcingHelper
													.getIBLineSerialIP());
									getIBLineerialInput.getDocumentElement()
											.getElementsByTagName("Item")
											.item(0)
											.setAttribute("ItemID", sItemID);
									getIBLineerialInput
											.getDocumentElement()
											.getElementsByTagName("Extn")
											.item(0)
											.setAttribute("SystemSerialNo",
													sSysSerialNo);
									String RapidSourceCenter = teradyneSourcingHelper
											.getRSCfromIBLinee(
													getIBLineerialInput, arg0);

									if (RapidSourceCenter != null
											&& (RapidSourceCenter.length() > 0)) {

										
											OdrlineElem
													.getChildElement("Extn")
													.setAttribute(
															"RapidSourceCenter",
															RapidSourceCenter);
											OdrlineElem.setAttribute(
													"ShipNode",
													RapidSourceCenter);
										

									}
									/*
									 * if regular exchange stamp sEXTDOC on line
									 * level
									 */
									else if (sEXTDOC != null) {
										if (_cat.isVerboseEnabled()) {

											_cat.verbose("=================rsc invalid============="
													+ OdrlineElem.toString());

										}

										if (!XmlUtils.isVoid(sExchangeType)) {

											if (!XmlUtils.isVoid(OdrlineElem
													.getChildElement("Extn"))) {
												if ((OdrlineElem
														.getChildElement("Extn")
														.hasAttribute("ServiceType"))) {

													if ((OdrlineElem
															.getChildElement(
																	"Extn")
															.getAttribute(
																	"ServiceType")
															.length() > 0)) {

														String sServiceType = OdrlineElem
																.getChildElement(
																		"Extn")
																.getAttribute(
																		"ServiceType");
														isExpSrvc = teradyneSourcingHelper
																.isNonExpress(sServiceType);

													}

												}

											}

										}

										if (!XmlUtils.isVoid(sExchangeType)
												&& isExpSrvc) {
											if (_cat.isVerboseEnabled()) {

												_cat.verbose("in non express service based regular exchange----------------->");

											}
											OdrlineElem.setAttribute(
													"ShipNode", sEXTDOC);

										}

										else if (XmlUtils.isVoid(sExchangeType)
												|| !(isExpSrvc)) {

											paramresult = teradyneSourcingHelper
													.getSourcingParams(
															OdrlineElem, arg0);
											sEXTNRPM = paramresult.get(0);
											sEXTNDIC = paramresult.get(1);
											sRepairCode = paramresult.get(2);

											/*
											 * change logic here to add the USL
											 * constrain
											 */

											if (teradyneSourcingHelper.isUSLR(
													OdrlineElem, sRepairCode)) {

												OdrlineElem.setAttribute(
														"ShipNode", sEXTNDIC);

											}

											else if ((sEXTNRPM.length() != 0 && !sEXTNRPM
													.equalsIgnoreCase(" "))
													&& (sEXTNDIC.length() != 0 && !sEXTNDIC
															.equalsIgnoreCase(" "))) {

												if (teradyneSourcingHelper
														.isDistRule(sEXTDOC
																+ sEXTNDIC
																+ sEXTNRPM,
																arg0)) {
													OdrlineElem
															.setAttribute(
																	"DistributionRuleId",
																	sEXTDOC
																			+ sEXTNDIC
																			+ sEXTNRPM);
												}

											}

										}
									}

									else {

										paramresult = teradyneSourcingHelper
												.getSourcingParams(OdrlineElem,
														arg0);
										sRepairCode = paramresult.get(2);

										if (teradyneSourcingHelper.isUSLR(
												OdrlineElem, sRepairCode)) {

											OdrlineElem.setAttribute(
													"ShipNode", sEXTNDIC);

										}
									}

								}

								else if (sEXTDOC != null) {
									
									if (!XmlUtils.isVoid(sExchangeType)) {

										if (!XmlUtils.isVoid(OdrlineElem
												.getChildElement("Extn"))) {
											if ((OdrlineElem
													.getChildElement("Extn")
													.hasAttribute("ServiceType"))) {

												if ((OdrlineElem
														.getChildElement(
																"Extn")
														.getAttribute(
																"ServiceType")
														.length() > 0)) {

													String sServiceType = OdrlineElem
															.getChildElement(
																	"Extn")
															.getAttribute(
																	"ServiceType");
													isExpSrvc = teradyneSourcingHelper
															.isNonExpress(sServiceType);

												}

											}

										}

									}

									if (!XmlUtils.isVoid(sExchangeType)
											&& isExpSrvc) {
										if (_cat.isVerboseEnabled()) {

											_cat.verbose("in non express service based regular exchange flow2----------------->");

										}
										OdrlineElem.setAttribute(
												"ShipNode", sEXTDOC);

									}

									else if (XmlUtils.isVoid(sExchangeType)
											|| !(isExpSrvc)) {

										paramresult = teradyneSourcingHelper
												.getSourcingParams(OdrlineElem,
														arg0);
										sEXTNRPM = paramresult.get(0);
										sEXTNDIC = paramresult.get(1);
										sRepairCode = paramresult.get(2);

										if (teradyneSourcingHelper.isUSLR(
												OdrlineElem, sRepairCode)) {

											OdrlineElem.setAttribute(
													"ShipNode", sEXTNDIC);

										}

										else if ((sEXTNRPM.length() != 0 && !sEXTNRPM
												.equalsIgnoreCase(" "))
												&& (sEXTNDIC.length() != 0 && !sEXTNDIC
														.equalsIgnoreCase(" "))) {
											if (_cat.isVerboseEnabled()) {

												_cat.verbose("DistributionRuleId----------------->"
														+ sEXTDOC
														+ sEXTNDIC
														+ sEXTNRPM);

											}

											if (teradyneSourcingHelper
													.isDistRule(sEXTDOC
															+ sEXTNDIC
															+ sEXTNRPM, arg0)) {
												OdrlineElem.setAttribute(
														"DistributionRuleId",
														sEXTDOC + sEXTNDIC
																+ sEXTNRPM);
											}

										}
									}
								} else {

									paramresult = teradyneSourcingHelper
											.getSourcingParams(OdrlineElem,
													arg0);
									sRepairCode = paramresult.get(2);

									if (teradyneSourcingHelper.isUSLR(
											OdrlineElem, sRepairCode)) {

										OdrlineElem.setAttribute("ShipNode",
												sEXTNDIC);

									}

								}
							} 
							
						}
							catch (final Exception e) {
								e.printStackTrace();
								try {
									throw e;
								} catch (final Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();

								}
							}
						}
					}
				}
			}
		}

		if (_cat.isVerboseEnabled()) {

			_cat.verbose("final output compiled" + inYFCXML.toString());

		}

		return inYFCXML.getDocument();
	}

	@Override
	public String beforeCreateOrder(YFSEnvironment arg0, String arg1)
			throws YFSUserExitException {
		// TODO Auto-generated method stub
		return null;
	}

}

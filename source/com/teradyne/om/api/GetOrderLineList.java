package com.teradyne.om.api;

import java.rmi.RemoteException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import com.sterlingcommerce.baseutil.SCXmlUtil;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;

public class GetOrderLineList {
	static YIFApi oAPI;

	public Document getOrderLineList(YFSEnvironment env, Document inXML) throws YFSUserExitException, YIFClientCreationException, YFSException, RemoteException, ParserConfigurationException{
		YFCDocument getOrderLineListTpl=null;
		try{
			getOrderLineListTpl=YFCDocument.getDocumentFor(SCXmlUtil.createFromString("<OrderLineList TotalNumberOfRecords=\"\"><OrderLine AllocationDate=\"\"><Item ItemID=\"\" ManufacturerName=\"\" UnitOfMeasure=\"\"><Extn SystemPTExceptionFlag=\"\"  SystemTesterGroup=\"\" /></Item><Order EnterpriseCode=\"\" OrderNo=\"\" OrderType=\"\"/><OrderStatuses><OrderStatus Status=\"\" ></OrderStatus></OrderStatuses></OrderLine></OrderLineList>"));

		} catch (YFSException exception) {
			throw new YFSUserExitException(exception.getMessage());
		}
		env.setApiTemplate("getOrderLineList", getOrderLineListTpl.getDocument());	
		oAPI = YIFClientFactory.getInstance().getApi();
		Document outDoc=oAPI.getOrderLineList(env, inXML);
		env.clearApiTemplate("getOrderLineList");
		YFCDocument outputDoc= YFCDocument.getDocumentFor(outDoc);
		YFCElement rootChild=outputDoc.getDocumentElement().getChildElement("OrderLine");
		YFCElement itemElem=rootChild.getChildElement("Item");
		String unitOfMeasure=itemElem.getAttribute("UnitOfMeasure");
		String item=itemElem.getAttribute("ItemID");
		YFCElement order=rootChild.getChildElement("Order");
		String enterpriseCode=order.getAttribute("EnterpriseCode");
		YFCDocument getItemDetailsTpl=null,inputXML=null;
		try{
			getItemDetailsTpl=YFCDocument.getDocumentFor(SCXmlUtil.createFromString("<Item ItemID=\"\" ><Extn SystemPTExceptionFlag=\"\"  SystemTesterGroup=\"\" /></Item>"));
			inputXML=YFCDocument.getDocumentFor(SCXmlUtil.createFromString("<Item ItemID=\""+item+"\" OrganizationCode=\""+enterpriseCode+"\" UnitOfMeasure=\""+unitOfMeasure+"\"/>"));
		} catch (YFSException exception) {
			throw new YFSUserExitException(exception.getMessage());
		}

		env.setApiTemplate("getItemDetails", getItemDetailsTpl.getDocument());	
		Document outDocument=oAPI.getItemDetails(env, inputXML.getDocument());
		env.clearApiTemplate("getItemDetails");
		YFCElement oElem=YFCDocument.getDocumentFor(outDocument).getDocumentElement().getChildElement("Extn");
		for (YFCElement items : rootChild.getElementsByTagName("Item")){

			items.importNode(oElem);
		}
		return outputDoc.getDocument();	

	}

}

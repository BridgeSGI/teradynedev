package com.teradyne.om.api;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSConnectionHolder;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeChangeOrderUE;

public class TeradyneProcessInstallBase extends CustomCodeHelper implements
		YFSBeforeChangeOrderUE {
	private static YFCLogCategory _cat = YFCLogCategory
			.instance("com.yantra.CustomCode");

	@Override
	public Document beforeChangeOrder(YFSEnvironment arg0, Document inXML)
			throws YFSUserExitException {
		setEnv(arg0);
		YFCDocument inYFCXML = null;
		inYFCXML = YFCDocument.getDocumentFor(inXML);
		/*
		 * to be coded logic for Install base line save and proceed
		 */
		if (_cat.isVerboseEnabled()) {
			_cat.verbose("=============@Author Pranav For BRIDGESGI=================");
			_cat.verbose("in the process IB engine");
			_cat.verbose("==============================" + inYFCXML.toString());

		}

		if (inYFCXML.getDocumentElement().hasAttribute("DocumentType")
				&& inYFCXML.getDocumentElement().getAttribute("DocumentType")
						.equalsIgnoreCase("0017.ex"))

		{

			int OrderLineListLength = 0;
			String ItemType = "FRU";
			String sSerialNumber = null;
			String sItemId = null;
			
			final YFCNodeList<YFCElement> OrderLineNodeList = inYFCXML
					.getDocumentElement().getElementsByTagName("OrderLine");

			OrderLineListLength = OrderLineNodeList.getLength();
			if (OrderLineListLength > 0)

			{
				for (int i = 0; i < OrderLineListLength; i++)

				{

					final YFCNode cNode = OrderLineNodeList.item(i);

					if (cNode.getNodeName().equalsIgnoreCase("OrderLine")) {
						if (cNode instanceof YFCElement) {
							final YFCElement OdrlineElem = (YFCElement) cNode;
							try {

								if (OdrlineElem.hasAttribute("Action")
										&& OdrlineElem.getAttribute("Action")
												.equalsIgnoreCase("CREATE"))

								{

									
									if(!XmlUtils.isVoid(OdrlineElem.getChildElement("Extn"))){
									
									/* check for serial no and duplicate serial */
									if (!OdrlineElem.getChildElement("Extn")
											.hasAttribute("SystemSerialNo")) {

										throw new Exception(
												"Serial Number is mandatory #"
														+ OdrlineElem
																.getChildElement(
																		"Item")
																.getAttribute(
																		"ItemID"));

									} else {
										sSerialNumber = OdrlineElem
												.getChildElement("Extn")
												.getAttribute("SystemSerialNo");
										sItemId = OdrlineElem.getChildElement(
												"Item").getAttribute("ItemID");
										final YFCDocument getDuplicateSerialInput = YFCDocument
												.getDocumentFor(getduplicateSerialIP());
										getDuplicateSerialInput
												.getDocumentElement()
												.getElementsByTagName("Item")
												.item(0)
												.setAttribute("ItemID", sItemId);
										getDuplicateSerialInput
												.getDocumentElement()
												.getElementsByTagName("Extn")
												.item(0)
												.setAttribute("SystemSerialNo",
														sSerialNumber);
										if (isDuplicate(getDuplicateSerialInput)) {
											throw new Exception(
													"Duplicate Serial Number "
															+ sSerialNumber);

										}

									}

									/*
									 * if(OdrlineElem.getChildElement("Extn").
									 * hasAttribute("ItemTypetemp")
									 * &&OdrlineElem
									 * .getChildElement("Extn").getAttribute
									 * ("ItemTypetemp").
									 * equalsIgnoreCase(ItemType)) {
									 * if(!(OdrlineElem.getChildElement("Extn").
									 * hasAttribute("MfgID"))||(!(OdrlineElem.
									 * getChildElement
									 * ("Extn").getAttribute("MfgID"
									 * ).length()>0))) {
									 * 
									 * 
									 * if (_cat.isVerboseEnabled()) {
									 * _cat.verbose("in the if block"); }
									 * //throw new
									 * Exception("MFG ID is mandatory for Line#"
									 * +OdrlineElem.getChildElement("Item").
									 * getAttribute("ItemID")); throw new
									 * Exception
									 * ("MFG ID is mandatory for Line#"+
									 * OdrlineElem
									 * .getChildElement("Item").getAttribute
									 * ("ItemID")); }
									 * 
									 * 
									 * }commented based on last change asked by
									 * teradyne to make mfgID optional
									 */

									if (!(OdrlineElem.getChildElement("Extn")
											.hasAttribute("IBBuildStatus") && OdrlineElem
											.getChildElement("Extn")
											.getAttribute("IBBuildStatus")
											.length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for Ib build status");
										}
										// throw new
										// Exception("MFG ID is mandatory for Line#"+OdrlineElem.getChildElement("Item").getAttribute("ItemID"));
										throw new Exception(
												"IBBuild Status is mandatory for Line#"
														+ OdrlineElem
																.getChildElement(
																		"Item")
																.getAttribute(
																		"ItemID"));

									}

									if (!(OdrlineElem.getChildElement("Extn")
											.hasAttribute("TesterOwnerOrgID") && OdrlineElem
											.getChildElement("Extn")
											.getAttribute("TesterOwnerOrgID")
											.length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for TesterOwnerOrgID");
										}
										// throw new
										// Exception("MFG ID is mandatory for Line#"+OdrlineElem.getChildElement("Item").getAttribute("ItemID"));
										throw new Exception(
												"OwnerOrgID is mandatory for Line");

									}
									if (!(OdrlineElem
											.getChildElement("Extn")
											.hasAttribute("TesterOperatorOrgID") && OdrlineElem
											.getChildElement("Extn")
											.getAttribute("TesterOperatorOrgID")
											.length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for TesterOperatorOrgID");
										}
										// throw new
										// Exception("MFG ID is mandatory for Line#"+OdrlineElem.getChildElement("Item").getAttribute("ItemID"));
										throw new Exception(
												"OperatorOrgID is mandatory for Line");

									}
									if (!(OdrlineElem
											.getChildElement("Extn")
											.hasAttribute("TesterServicerOrgID") && OdrlineElem
											.getChildElement("Extn")
											.getAttribute("TesterServicerOrgID")
											.length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for TesterServicerOrgID");
										}
										// throw new
										// Exception("MFG ID is mandatory for Line#"+OdrlineElem.getChildElement("Item").getAttribute("ItemID"));
										throw new Exception(
												"ServicerOrgID is mandatory for Line");

									}
									
									/* Processing status mandatory check */
									
									if(XmlUtils.isVoid(OdrlineElem
											.getChildElement("Extn")
											.getAttribute("ProcessingStatus")))
									{
										String ItemTypeTemp=null;
										String ItemIDtemp= OdrlineElem.getChildElement("Item").getAttribute("ItemID");
										/*call to get the item type for the item in the message*/
									
										
										
										final YFCDocument getItemIDTypeInput =
												  YFCDocument
												  .getDocumentFor(getItemtypeByItemIDIP());
										
										getItemIDTypeInput.getDocumentElement().setAttribute("OrganizationCode", inYFCXML.getDocumentElement()
												.getAttribute("EnterpriseCode"));
										getItemIDTypeInput.getDocumentElement().setAttribute("ItemID", ItemIDtemp);
										
										ItemTypeTemp=getItemIDType(getItemIDTypeInput);
										
										
												 if(ItemTypeTemp.equalsIgnoreCase(ItemType))
												 {
													 
													 throw new Exception(
																"Processing Status is mandatory for Line with FRUs");

													 
												 }
										
									}
									
									
									
									
									}
									if (!(OdrlineElem
											.hasAttribute("ReceivingNode") && OdrlineElem
											.getAttribute("ReceivingNode")
											.length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for ReceivingNode");
										}
										// throw new
										// Exception("MFG ID is mandatory for Line#"+OdrlineElem.getChildElement("Item").getAttribute("ItemID"));
										throw new Exception(
												"Install Node is mandatory for Line");

									}

								}
								/* do this is when not kit modifications for change*/
								else if (!(OdrlineElem.getElementsByTagName(
										"KitLines").getLength() > 0))

								{
									if(!XmlUtils.isVoid(OdrlineElem.getChildElement(
											"Extn"))){
									if (!(OdrlineElem.getElementsByTagName(
											"KitLines").getLength() > 0)
											&& (OdrlineElem.getChildElement(
													"Extn").hasAttribute(
													"SystemSerialNo") && !(OdrlineElem
													.getChildElement("Extn")
													.getAttribute(
															"SystemSerialNo")
													.length() > 0))) {

										throw new Exception(
												"Serial Number is mandatory for Line#"
														+ OdrlineElem
																.getAttribute("PrimeLineNo"));

									} else if ((OdrlineElem.getChildElement(
											"Extn").hasAttribute(
											"SystemSerialNo") && (OdrlineElem
											.getChildElement("Extn")
											.getAttribute("SystemSerialNo")
											.length() > 0))) {

										sSerialNumber = OdrlineElem
												.getChildElement("Extn")
												.getAttribute("SystemSerialNo");
										// sItemId=OdrlineElem.getChildElement("Item").getAttribute("ItemID");
										final YFCDocument getCustListInput = YFCDocument
												.getDocumentFor(getItemtypeIP());
										String OrderLinekey = OdrlineElem
												.getAttribute("OrderLineKey");
										getCustListInput.getDocumentElement()
												.setAttribute("OrderLineKey",
														OrderLinekey);
										sItemId = getItemID(getCustListInput);
										final YFCDocument getDuplicateSerialInput = YFCDocument
												.getDocumentFor(getduplicateSerialIP());
										getDuplicateSerialInput
												.getDocumentElement()
												.getElementsByTagName("Item")
												.item(0)
												.setAttribute("ItemID", sItemId);
										getDuplicateSerialInput
												.getDocumentElement()
												.getElementsByTagName("Extn")
												.item(0)
												.setAttribute("SystemSerialNo",
														sSerialNumber);
										if (isDuplicate(getDuplicateSerialInput)) {
											throw new Exception(
													"Duplicate Serial Number "
															+ sSerialNumber);

										}
										
										else {
											
											OdrlineElem.getChildElement(
													"Extn").setAttribute("IsReSerial", "Y");
										}

									}
									
									
									
									/* check for serial no and duplicate serial */

									/*
									 * else
									 * if((OdrlineElem.getChildElement("Extn").
									 * hasAttribute
									 * ("MfgID")&&!(OdrlineElem.getChildElement
									 * ("Extn").
									 * getAttribute("MfgID").length()>0))) {
									 * 
									 * 
									 * 
									 * String
									 * OrderLinekey=OdrlineElem.getAttribute
									 * ("OrderLineKey");
									 * 
									 * final YFCDocument getCustListInput =
									 * YFCDocument
									 * .getDocumentFor(getItemtypeIP());
									 * getCustListInput
									 * .getDocumentElement().setAttribute(
									 * "OrderLineKey", OrderLinekey); String
									 * ItemTypeTemp=
									 * getItemtypeall(getCustListInput);
									 * 
									 * 
									 * 
									 * if(ItemTypeTemp.equalsIgnoreCase(ItemType)
									 * ) {
									 * 
									 * 
									 * if (_cat.isVerboseEnabled()) {
									 * _cat.verbose
									 * ("in the elseif if block----------------->"
									 * +ItemTypeTemp); } throw new
									 * Exception("MFG ID is mandatory for Line#"
									 * +
									 * OdrlineElem.getAttribute("PrimeLineNo"));
									 * 
									 * }
									 * 
									 * }
									 */

									if (OdrlineElem.getChildElement("Extn")
											.hasAttribute("IBBuildStatus")
											&& !(OdrlineElem
													.getChildElement("Extn")
													.getAttribute(
															"IBBuildStatus")
													.length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for Ib build status");
										}
										// throw new
										// Exception("MFG ID is mandatory for Line#"+OdrlineElem.getChildElement("Item").getAttribute("ItemID"));
										if (OdrlineElem
												.hasAttribute("PrimeLineNo")) {

											throw new Exception(
													"IBBuild Status is mandatory for Line#"
															+ OdrlineElem
																	.getAttribute("PrimeLineNo"));

										} else {
											throw new Exception(
													"IBBuild Status is mandatory for Line");
										}

									}

									

									if (OdrlineElem.getChildElement("Extn")
											.hasAttribute("TesterOwnerOrgID")
											&& !(OdrlineElem
													.getChildElement("Extn")
													.getAttribute(
															"TesterOwnerOrgID")
													.length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for TesterOwnerOrgID");
										}

										if (OdrlineElem
												.hasAttribute("PrimeLineNo")) {

											throw new Exception(
													"TesterOwnerOrgID is mandatory for Line#"
															+ OdrlineElem
																	.getAttribute("PrimeLineNo"));

										} else {
											throw new Exception(
													"TesterOwnerOrgID is mandatory for Line");
										}

									}
									if (OdrlineElem
											.getChildElement("Extn")
											.hasAttribute("TesterOperatorOrgID")
											&& !(OdrlineElem
													.getChildElement("Extn")
													.getAttribute(
															"TesterOperatorOrgID")
													.length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for TesterOperatorOrgID");
										}

										if (OdrlineElem
												.hasAttribute("PrimeLineNo")) {

											throw new Exception(
													"TesterOperatorOrgID is mandatory for Line#"
															+ OdrlineElem
																	.getAttribute("PrimeLineNo"));

										} else {
											throw new Exception(
													"TesterOperatorOrgID is mandatory for Line");
										}

									}
									if (OdrlineElem
											.getChildElement("Extn")
											.hasAttribute("TesterServicerOrgID")
											&& !(OdrlineElem
													.getChildElement("Extn")
													.getAttribute(
															"TesterServicerOrgID")
													.length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for TesterServicerOrgID");
										}
										if (OdrlineElem
												.hasAttribute("PrimeLineNo")) {

											throw new Exception(
													"TesterServicerOrgID is mandatory for Line#"
															+ OdrlineElem
																	.getAttribute("PrimeLineNo"));

										} else {
											throw new Exception(
													"TesterServicerOrgID is mandatory for Line");
										}

									
									/*processing status mandatory check */
									
									
									
									
									
									}
									if(OdrlineElem
											.getChildElement("Extn")
											.hasAttribute("ProcessingStatus")
											&& !(OdrlineElem
													.getChildElement("Extn")
													.getAttribute(
															"ProcessingStatus")
													.length() > 0))
									{
										String
										  OrderLinekey=OdrlineElem.getAttribute
										  ("OrderLineKey");
										final YFCDocument getCustListInput =
												  YFCDocument
												  .getDocumentFor(getItemtypeIP());
												 getCustListInput
												 .getDocumentElement().setAttribute(
												  "OrderLineKey", OrderLinekey); 
												 String ItemTypeTemp = getItemtypeall(getCustListInput);
												 
												 if(ItemTypeTemp.equalsIgnoreCase(ItemType))
												 {
													 
													 throw new Exception(
																"Processing Status is mandatory for Line with FRUs");

													 
												 }
										
									}
									}
									if (OdrlineElem
											.hasAttribute("ReceivingNode")
											&& !(OdrlineElem.getAttribute(
													"ReceivingNode").length() > 0)) {

										if (_cat.isVerboseEnabled()) {
											_cat.verbose("in the if block for ReceivingNode");
										}
										if (OdrlineElem
												.hasAttribute("PrimeLineNo")) {

											throw new Exception(
													"Install Node is mandatory for Line#"
															+ OdrlineElem
																	.getAttribute("PrimeLineNo"));

										} else {
											throw new Exception(
													"Install Node  is mandatory for Line");
										}

									}

								
								}

								else {

									final YFCNodeList<YFCElement> KitLineNodeList = inYFCXML
											.getDocumentElement()
											.getElementsByTagName("KitLine");
									Map<String, String> SerialNoKitlinekey = new HashMap<String, String>();
									int KitLineListLength = KitLineNodeList
											.getLength();

									if (KitLineListLength > 0)

									{
										for (int k = 0; k < KitLineListLength; k++)

										{

											final YFCNode kNode = KitLineNodeList
													.item(k);

											if (kNode
													.getNodeName()
													.equalsIgnoreCase("KitLine")) {
												if (cNode instanceof YFCElement) {
													final YFCElement KitlineElem = (YFCElement) kNode;

													if (KitlineElem
															.getChildElement(
																	"Extn")
															.hasAttribute(
																	"SystemSerialNo")) {

														String KitserialNo = (KitlineElem
																.getChildElement("Extn")
																.getAttribute("SystemSerialNo"));
														String sOrderKitLineKey = KitlineElem
																.getAttribute("OrderKitLineKey");

														SerialNoKitlinekey
																.put(sOrderKitLineKey,
																		KitserialNo);
													}

												}
											}
										}

										if (!SerialNoKitlinekey.isEmpty()) {

											if (verifyKitSerials(arg0,
													SerialNoKitlinekey)) {

												throw new Exception(
														"Duplicate Serial Number for Component Line");

											}

										}

									}

								}

							} catch (Exception e) {
								// throw new YFSException(e.getMessage());
								throw new YFSException(
										"mandatory Parameters Missing Or Invalid",
										"EXTNTDYN0002", e.getMessage());
							}
						}
					}
				}
			}

		}

		return inYFCXML.getDocument();
	}

	protected String getItemtypeall(YFCDocument inXML) throws YFSException {
		YFCDocument ItemTypeListOP = null;

		final Document getItemtypeOPTemp = getItemtypeOP();
		Document getItemtypeOP = null;
		getItemtypeOP = invokeApi("getOrderLineList", inXML.getDocument(),
				getItemtypeOPTemp);
		ItemTypeListOP = YFCDocument.getDocumentFor(getItemtypeOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getItemtypeall call is:"
					+ ItemTypeListOP.toString());
		}

		return ItemTypeListOP.getDocumentElement()
				.getElementsByTagName("PrimaryInformation").item(0)
				.getAttribute("ItemType");

	}

	protected String getItemID(YFCDocument inXML) throws YFSException {
		YFCDocument ItemTypeListOP = null;

		final Document getItemtypeOPTemp = getItemtypeOP();
		Document getItemtypeOP = null;
		getItemtypeOP = invokeApi("getOrderLineList", inXML.getDocument(),
				getItemtypeOPTemp);
		ItemTypeListOP = YFCDocument.getDocumentFor(getItemtypeOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getItemID call is:"
					+ ItemTypeListOP.toString());
		}

		return ItemTypeListOP.getDocumentElement()
				.getElementsByTagName("ItemDetails").item(0)
				.getAttribute("ItemID");

	}

	private Document getItemtypeIP() throws YFSException {
		final YFCDocument getItemtypeIP = YFCDocument
				.getDocumentFor("<OrderLine  />");
		return getItemtypeIP.getDocument();
	}
	
	private Document getItemtypeByItemIDIP() throws YFSException {
		final YFCDocument getItemIDtypeIP = YFCDocument
				.getDocumentFor("<Item UnitOfMeasure=\"EA\" />");
		return getItemIDtypeIP.getDocument();
	}

	
	private Document getItemtypeByItemIDOP() throws YFSException
	{
		final YFCDocument getItemIDtypeOP = YFCDocument
				.getDocumentFor("<ItemList><Item ItemID=\"\"><PrimaryInformation ItemType=\"\"/></Item></ItemList>");
		return getItemIDtypeOP.getDocument();
	}
	
	
	
	protected String getItemIDType(YFCDocument inXML) throws YFSException {
		YFCDocument ItemIDTypeListOP = null;

		final Document getItemIDtypeOPTemp = getItemtypeByItemIDOP();
		Document getItemIDtypeOP = null;
		getItemIDtypeOP = invokeApi("getItemList", inXML.getDocument(),
				getItemIDtypeOPTemp);
		ItemIDTypeListOP = YFCDocument.getDocumentFor(getItemIDtypeOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getItemIDType call is:"
					+ ItemIDTypeListOP.toString());
		}

		return ItemIDTypeListOP.getDocumentElement()
				.getElementsByTagName("PrimaryInformation").item(0)
				.getAttribute("ItemType");

	}
	
	private Document getItemtypeOP() throws YFSException {
		final YFCDocument getItemtypeOP = YFCDocument
				.getDocumentFor("<OrderLineList TotalNumberOfRecords=\"\">"
						+ "<OrderLine ><ItemDetails ItemID=\"\" ><PrimaryInformation ItemType=\"\" />"
						+ "</ItemDetails ></OrderLine></OrderLineList>");
		return getItemtypeOP.getDocument();

	}

	private Document getduplicateSerialIP() throws YFSException {
		final YFCDocument getduplicateSerialIP = YFCDocument
				.getDocumentFor("<OrderLine StatusQryType=\"BETWEEN\" ToStatus=\"1100.60\" FromStatus=\"1100.10\"><Order DocumentType=\"0017.ex\" EnterpriseCode=\"CSO\"/><Item />"
						+ "<Extn  /></OrderLine>");
		return getduplicateSerialIP.getDocument();

	}



	private Document getorderLineListOPTemp() throws YFSException {
		final YFCDocument getorderLineListOPTemp = YFCDocument
				.getDocumentFor("<OrderLineList TotalLineList=\"\"><OrderLine OrderLineKey=\"\" MaxLineStatus=\"\">"
						+ "<Extn ProcessingStatus=\"\"/></OrderLine></OrderLineList>");
		return getorderLineListOPTemp.getDocument();

	}

	protected Boolean isDuplicate(YFCDocument inXML) throws YFSException {
		YFCDocument getorderLineListOP = null;
		Boolean isDuplicate = false;
		if (_cat.isVerboseEnabled()) {
			_cat.verbose("input to isDuplicate call is:"
					+ inXML.toString());
		}

		final Document getorderLineListOPTemp = getorderLineListOPTemp();
		Document getorderLineListOptemp = null;
		getorderLineListOptemp = invokeApi("getOrderLineList", inXML.getDocument(),
				getorderLineListOPTemp);
		getorderLineListOP = YFCDocument.getDocumentFor(getorderLineListOptemp);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getorderLineListOPTemp call is:"
					+ getorderLineListOP.toString());
		}
		if (getorderLineListOP.getDocumentElement().hasAttribute(
				"TotalLineList")) {
			int TotalLineList = Integer.parseInt(getorderLineListOP
					.getDocumentElement().getAttribute("TotalLineList"));

			if (TotalLineList > 0) {
				isDuplicate = true;

			}

		}

		else
		{ 
			String ItemTypefru="FRU";
			String sIsfruItem=	inXML.getDocumentElement().getElementsByTagName("Item").item(0).getAttribute("ItemID");
			final YFCDocument getItemIDTypeInputfru =
					  YFCDocument
					  .getDocumentFor(getItemtypeByItemIDIP());
			
			getItemIDTypeInputfru.getDocumentElement().setAttribute("OrganizationCode", "CSO");
			getItemIDTypeInputfru.getDocumentElement().setAttribute("ItemID", sIsfruItem);
			
			
			
					 if(getItemIDType(getItemIDTypeInputfru).equalsIgnoreCase(ItemTypefru))
					 {
						 
						
						 
					 		
			/*do this only for FRUs*/
			inXML.getDocumentElement().removeAttribute("StatusQryType");
			inXML.getDocumentElement().removeAttribute("ToStatus");
			inXML.getDocumentElement().removeAttribute("FromStatus");
			getorderLineListOptemp = invokeApi("getOrderLineList", inXML.getDocument(),
					getorderLineListOPTemp);
			getorderLineListOP = YFCDocument.getDocumentFor(getorderLineListOptemp);
			if (getorderLineListOP.getDocumentElement().hasAttribute(
					"TotalLineList")) {
				
				int TotalLineList = Integer.parseInt(getorderLineListOP
						.getDocumentElement().getAttribute("TotalLineList"));
				
				if (TotalLineList > 0) 
				{
					
					
					int count=0;
					final YFCNodeList<YFCElement> OrderLineNodeListdup = getorderLineListOP
							.getDocumentElement().getElementsByTagName("OrderLine");
					
					
					if(OrderLineNodeListdup.getLength() > 0)
					{
						
						
						for(int i = 0; i < OrderLineNodeListdup.getLength(); i++)
						
						{
							final YFCNode cNodeDUP = OrderLineNodeListdup.item(i);

							
						if(cNodeDUP.getNodeName().equalsIgnoreCase("OrderLine"))
						{
							if(cNodeDUP instanceof YFCElement)
							{
								final YFCElement OdrlineElemDUP = (YFCElement) cNodeDUP;
								try {
									
								 OdrlineElemDUP.getAttribute("MaxLineStatus");
								 
								 if(!XmlUtils.isVoid(OdrlineElemDUP.getChildElement("Extn").getAttribute("ProcessingStatus")))
								 {
									
									 
									 if( Double.parseDouble(OdrlineElemDUP.getAttribute("MaxLineStatus"))>1100.60
											 && OdrlineElemDUP.getChildElement("Extn").getAttribute("ProcessingStatus").equalsIgnoreCase("R"))
									 {
										 
										 count++;
										 
									 }
									 
									 
								 }
								 
									
								}
								catch (Exception e) {
									// throw new YFSException(e.getMessage());
									throw new YFSException(
											"mandatory Parameters Missing Or Invalid",
											"EXTNTDYN0002", e.getMessage());
								}
								
							}
							
							
						}	
							
						
						
						
						}
						
						if (count == 0) 
						{
							isDuplicate = true;

						}
					
					
					
					
					
					}
					
					
				}
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					

				}

			}
			
			
			
		}
		
		
		/*
		 * else {
		 * 
		 * 
		 * get all order lines with serial no and check if any line is in processing status R but not active
		 * 
		 * 
		 * 
		 * }
		 */

		/*
		 * if the serial in not present in any active line check if serial is
		 * present in any line with processing status constraint for fru type
		 * item
		 */
			
		return isDuplicate;

	}

	public Boolean verifyKitSerials(YFSEnvironment env,
			Map<String, String> SerialNoKitlinekey)
			throws YFSUserExitException, SQLException {

		setEnv(env);
		Boolean isKitSeralDuplicate = false;

		Connection con = null;
		if (env instanceof YFSConnectionHolder) {
			con = ((YFSConnectionHolder) env).getDBConnection();
		}
		if (con == null) {
			return true;
		}

		CallableStatement stmt;
		CallableStatement stmt2;
		try {
			stmt = con
					.prepareCall("select order_line_Key from yfs_order_kit_line where EXTN_SYSTEM_SERIAL_NO = ? AND ITEM_ID=?");
			stmt2 = con
					.prepareCall("select ITEM_ID from yfs_order_kit_line where ORDER_KIT_LINE_KEY = ?");

			ResultSet rs = null;
			ResultSet rs2 = null;

			for (Map.Entry<String, String> entry : SerialNoKitlinekey
					.entrySet()) {

				stmt2.setString(1, padRight((entry.getKey().toString()), 24));

				rs = stmt2.executeQuery();

				while (rs.next()) {

					String sItemId = rs.getString(1).trim();

					stmt.setString(1, entry.getValue().toString());
					stmt.setString(2, padRight(sItemId, 40));
					rs2 = stmt.executeQuery();

					while (rs2.next()) {

						String sOLkey = rs2.getString(1);

						/* status for the above order line key in install base */

						final YFCDocument getDuplicateKitSerialInput = YFCDocument
								.getDocumentFor(getduplicateSerialIP());
						getDuplicateKitSerialInput.getDocumentElement()
								.removeChild(
										getDuplicateKitSerialInput
												.getDocumentElement()
												.getElementsByTagName("Item")
												.item(0));
						getDuplicateKitSerialInput.getDocumentElement()
								.setAttribute("OrderLineKey", sOLkey);
						isKitSeralDuplicate = isDuplicate(getDuplicateKitSerialInput);

					}

				}

			}

			rs2.close();
			stmt2.close();
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// con.close();
		}

		// select * from yfs_order_kit_line where extn_serial_number='';

		return isKitSeralDuplicate;
	}

	public static String padRight(String s, int n) {
		return String.format("%1$-" + n + "s", s);
	}

}

package com.teradyne.om.api;

import java.util.ArrayList;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.api.CustomCodeHelper;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeChangeOrderUE;

/*@Author Pranav Singh For BRIDGSGI*/
public class TeradyneSourcingEngineOnChange extends CustomCodeHelper implements
		YFSBeforeChangeOrderUE {
	private static YFCLogCategory _cat = YFCLogCategory
			.instance("com.yantra.CustomCode");
	ArrayList<String> custExchange = new ArrayList<String>();

	@Override
	public Document beforeChangeOrder(YFSEnvironment arg0, Document inXML)
			throws YFSUserExitException

	{
		setEnv(arg0);
		YFCDocument inYFCXML = null;
		inYFCXML = YFCDocument.getDocumentFor(inXML);
		if (_cat.isVerboseEnabled()) {
			_cat.verbose("=============@Author Pranav For BRIDGESGI=================");
			_cat.verbose("in the sourcing engine on change");
			_cat.verbose("==============================" + inYFCXML.toString());

		}

		if (inYFCXML.getDocumentElement().hasAttribute("DocumentType")
				&& inYFCXML.getDocumentElement().getAttribute("DocumentType")
						.equalsIgnoreCase("0001")) {
			inYFCXML = sourcingRuleDeteronChange(inYFCXML, arg0);
			/* call for sourcing rule determination */
			if (_cat.isVerboseEnabled()) {

				_cat.verbose("in the sourcing engine on change found sales order");
			}

		}

		return inYFCXML.getDocument();
	}

	protected YFCDocument sourcingRuleDeteronChange(YFCDocument inXML,
			YFSEnvironment arg0) throws YFSException {
		final YFCNodeList<YFCElement> OrderLineNodeList = inXML
				.getDocumentElement().getElementsByTagName("OrderLine");
		setEnv(arg0);
		int OrderLineListLength = OrderLineNodeList.getLength();
		String sItemID = null;
		Boolean isExpSrvc = false;
		String sOdrHdrKey = null;
		String sEXTDOC = null;

		String sRepairCode = null;
		String sEXTNRPM = null;

		ArrayList<String> paramresult = new ArrayList<String>();
		String sEXTNDIC = null;
		String sExchangeType = null;
		if (OrderLineListLength > 0)

		{
			// final TeradyneSourcingEngineOnCreate
			// TeradyneSourcingEngineOnCreate = new
			// TeradyneSourcingEngineOnCreate();

			final TeradyneSourcingHelper teradyneSourcingHelper = new TeradyneSourcingHelper();
			if (_cat.isVerboseEnabled()) {

				_cat.verbose("in the sourcing engine on change--> found line"
						+ OrderLineListLength);

			}
			for (int i = 0; i < OrderLineListLength; i++)

			{

				final YFCNode cNode = OrderLineNodeList.item(i);

				if (cNode.getNodeName().equalsIgnoreCase("OrderLine")) {
					if (cNode instanceof YFCElement) {
						final YFCElement OdrlineElem = (YFCElement) cNode;

						try {

							
							
							
							if (OdrlineElem.hasAttribute("Action")
									&& OdrlineElem.getAttribute("Action")
											.equalsIgnoreCase("CREATE")) {
								sItemID = OdrlineElem.getChildElement("Item")
										.getAttribute("ItemID");

								if (OdrlineElem.hasAttribute("ShipNode")
										&& OdrlineElem.getAttribute("ShipNode")
												.length() > 0) {

									/* do nothing */
								} else {

									if (_cat.isVerboseEnabled()) {

										_cat.verbose("in the sourcing engine on change-->No ShipNode");

									}

									if ((OdrlineElem.getChildElement("Extn") != null)
											&& OdrlineElem.getChildElement(
													"Extn").hasAttribute(
													"SystemSerialNo")
											&& OdrlineElem
													.getChildElement("Extn")
													.getAttribute(
															"SystemSerialNo")
													.length() > 0) {
										String sSysSerialNo = OdrlineElem
												.getChildElement("Extn")
												.getAttribute("SystemSerialNo");

										/*
										 * get Rcenter from IB line with serial
										 * number and item id status combination
										 */
										final YFCDocument getIBLineerialInput = YFCDocument
												.getDocumentFor(teradyneSourcingHelper
														.getIBLineSerialIP());
										getIBLineerialInput
												.getDocumentElement()
												.getElementsByTagName("Item")
												.item(0)
												.setAttribute("ItemID", sItemID);
										getIBLineerialInput
												.getDocumentElement()
												.getElementsByTagName("Extn")
												.item(0)
												.setAttribute("SystemSerialNo",
														sSysSerialNo);
										String RapidSourceCenter = teradyneSourcingHelper
												.getRSCfromIBLinee(
														getIBLineerialInput,
														arg0);

										if (RapidSourceCenter != null
												&& (RapidSourceCenter.length() > 0)) {

											OdrlineElem
													.getChildElement("Extn")
													.setAttribute(
															"RapidSourceCenter",
															RapidSourceCenter);
											OdrlineElem.setAttribute(
													"ShipNode",
													RapidSourceCenter);

										}

										else if (inXML.getDocumentElement()
												.hasAttribute("OrderHeaderKey")) {
											sOdrHdrKey = inXML
													.getDocumentElement()
													.getAttribute(
															"OrderHeaderKey");

											ArrayList<String> csutomerdtls = getCustomerId(sOdrHdrKey);
											/* check if arraylist is empty */
											if (csutomerdtls.size() > 0) {

												final YFCDocument getCustListInput = YFCDocument
														.getDocumentFor(teradyneSourcingHelper
																.getCustomerIP());
												getCustListInput
														.getDocumentElement()
														.setAttribute(
																"CustomerID",
																csutomerdtls
																		.get(0));
												final YFCDocument CustomerListOutput = teradyneSourcingHelper
														.getCustomerListCall(
																getCustListInput,
																arg0);
												if (CustomerListOutput
														.getDocumentElement()
														.getElementsByTagName(
																"Extn")
														.getLength() > 0) {
													sEXTDOC = CustomerListOutput
															.getDocumentElement()
															.getChildElement(
																	"Customer")
															.getChildElement(
																	"Extn")
															.getAttribute(
																	"DefaultOrderCenter");

												}

												if (csutomerdtls.size() > 1) {

													sExchangeType = csutomerdtls
															.get(1);
												}

											}

											if (sEXTDOC != null) {

												/* check for regular exchange */
												if (!XmlUtils.isVoid(sExchangeType)) {

													if (!XmlUtils.isVoid(OdrlineElem
															.getChildElement("Extn"))) {
														if ((OdrlineElem
																.getChildElement("Extn")
																.hasAttribute("ServiceType"))) {

															if ((OdrlineElem
																	.getChildElement(
																			"Extn")
																	.getAttribute(
																			"ServiceType")
																	.length() > 0)) {

																String sServiceType = OdrlineElem
																		.getChildElement(
																				"Extn")
																		.getAttribute(
																				"ServiceType");
																isExpSrvc = teradyneSourcingHelper
																		.isNonExpress(sServiceType);

															}

														}

													}

												}
												if (!XmlUtils.isVoid(sExchangeType)
														&& isExpSrvc) {
													if (_cat.isVerboseEnabled()) {

														_cat.verbose("in non express service based regular exchange----------------->");

													}
													OdrlineElem.setAttribute(
															"ShipNode", sEXTDOC);

												}
												
												
												else if (XmlUtils.isVoid(sExchangeType)
														|| !(isExpSrvc)) {

													paramresult = teradyneSourcingHelper
															.getSourcingParams(
																	OdrlineElem,
																	arg0);
													sEXTNRPM = paramresult
															.get(0);
													sEXTNDIC = paramresult
															.get(1);
													sRepairCode = paramresult
															.get(2);

													/*
													 * change logic here to add
													 * the USL constrain
													 */

													if (teradyneSourcingHelper
															.isUSLR(OdrlineElem,
																	sRepairCode)) {

														OdrlineElem
																.setAttribute(
																		"ShipNode",
																		sEXTNDIC);

													}

													else if ((sEXTNRPM.length() != 0 && !sEXTNRPM
															.equalsIgnoreCase(" "))
															&& (sEXTNDIC
																	.length() != 0 && !sEXTNDIC
																	.equalsIgnoreCase(" "))) {

														if (teradyneSourcingHelper
																.isDistRule(
																		sEXTDOC
																				+ sEXTNDIC
																				+ sEXTNRPM,
																		arg0)) {
															OdrlineElem
																	.setAttribute(
																			"DistributionRuleId",
																			sEXTDOC
																					+ sEXTNDIC
																					+ sEXTNRPM);
														}

													}

												}

											}

											else {

												paramresult = teradyneSourcingHelper
														.getSourcingParams(
																OdrlineElem,
																arg0);
												sRepairCode = paramresult
														.get(2);

												if (teradyneSourcingHelper
														.isUSLR(OdrlineElem,
																sRepairCode)) {

													OdrlineElem.setAttribute(
															"ShipNode",
															sEXTNDIC);

												}

											}

										}

									}

									else

									if (inXML.getDocumentElement()
											.hasAttribute("OrderHeaderKey")) {
										sOdrHdrKey = inXML.getDocumentElement()
												.getAttribute("OrderHeaderKey");
										ArrayList<String> csutomerdtls = getCustomerId(sOdrHdrKey);
										if (csutomerdtls.size() > 0) {

											final YFCDocument getCustListInput = YFCDocument
													.getDocumentFor(teradyneSourcingHelper
															.getCustomerIP());
											getCustListInput
													.getDocumentElement()
													.setAttribute("CustomerID",
															csutomerdtls.get(0));
											final YFCDocument CustomerListOutput = teradyneSourcingHelper
													.getCustomerListCall(
															getCustListInput,
															arg0);
											if (CustomerListOutput
													.getDocumentElement()
													.getElementsByTagName(
															"Extn").getLength() > 0) {
												sEXTDOC = CustomerListOutput
														.getDocumentElement()
														.getChildElement(
																"Customer")
														.getChildElement("Extn")
														.getAttribute(
																"DefaultOrderCenter");

											}

											if (csutomerdtls.size() > 1) {

												sExchangeType = csutomerdtls
														.get(1);
											}

										}

										if (sEXTDOC != null) {

											// String
											// sUOM=OdrlineElem.getChildElement("Item")
											// .getAttribute("UnitOfMeasure");
											if (!XmlUtils.isVoid(sExchangeType)) {

												if (!XmlUtils.isVoid(OdrlineElem
														.getChildElement("Extn"))) {
													if ((OdrlineElem
															.getChildElement("Extn")
															.hasAttribute("ServiceType"))) {

														if ((OdrlineElem
																.getChildElement(
																		"Extn")
																.getAttribute(
																		"ServiceType")
																.length() > 0)) {

															String sServiceType = OdrlineElem
																	.getChildElement(
																			"Extn")
																	.getAttribute(
																			"ServiceType");
															isExpSrvc = teradyneSourcingHelper
																	.isNonExpress(sServiceType);

														}

													}

												}

											}

											if (!XmlUtils.isVoid(sExchangeType)
													&& isExpSrvc) {
												if (_cat.isVerboseEnabled()) {

													_cat.verbose("in non express service based regular exchange----------------->");

												}
												OdrlineElem.setAttribute(
														"ShipNode", sEXTDOC);

											}

											else if (XmlUtils.isVoid(sExchangeType)
													|| !(isExpSrvc)) {

												paramresult = teradyneSourcingHelper
														.getSourcingParams(
																OdrlineElem,
																arg0);

												sEXTNRPM = paramresult.get(0);
												sEXTNDIC = paramresult.get(1);
												sRepairCode = paramresult
														.get(2);

												/*
												 * change logic here to add the
												 * USL constrain
												 */

												if (teradyneSourcingHelper
														.isUSLR(OdrlineElem,
																sRepairCode)) {

													OdrlineElem.setAttribute(
															"ShipNode",
															sEXTNDIC);

												}

												else if ((sEXTNRPM.length() != 0 && !sEXTNRPM
														.equalsIgnoreCase(" "))
														&& (sEXTNDIC.length() != 0 && !sEXTNDIC
																.equalsIgnoreCase(" "))) {

													if (teradyneSourcingHelper
															.isDistRule(sEXTDOC
																	+ sEXTNDIC
																	+ sEXTNRPM,
																	arg0)) {
														OdrlineElem
																.setAttribute(
																		"DistributionRuleId",
																		sEXTDOC
																				+ sEXTNDIC
																				+ sEXTNRPM);
													}

												}

											}

										} else {

											paramresult = teradyneSourcingHelper
													.getSourcingParams(
															OdrlineElem, arg0);
											sRepairCode = paramresult.get(2);

											if (teradyneSourcingHelper.isUSLR(
													OdrlineElem, sRepairCode)) {

												OdrlineElem.setAttribute(
														"ShipNode", sEXTNDIC);

											}

										}
									}
								}

							}

							else if ((OdrlineElem.getChildElement("Extn") != null)
									&& OdrlineElem.getChildElement("Extn")
											.hasAttribute("SystemSerialNo")
									&& OdrlineElem.getChildElement("Extn")
											.getAttribute("SystemSerialNo")
											.length() > 0&& !(OdrlineElem.hasAttribute("ShipNode")
											&& OdrlineElem.getAttribute("ShipNode")
											.length() > 0)) {

								String sSysSerialNo = OdrlineElem
										.getChildElement("Extn").getAttribute(
												"SystemSerialNo");

								sItemID = getItemID(OdrlineElem
										.getAttribute("OrderLineKey"));
								final YFCDocument getIBLineerialInput = YFCDocument
										.getDocumentFor(teradyneSourcingHelper
												.getIBLineSerialIP());
								getIBLineerialInput.getDocumentElement()
										.getElementsByTagName("Item").item(0)
										.setAttribute("ItemID", sItemID);
								getIBLineerialInput
										.getDocumentElement()
										.getElementsByTagName("Extn")
										.item(0)
										.setAttribute("SystemSerialNo",
												sSysSerialNo);
								String RapidSourceCenter = teradyneSourcingHelper
										.getRSCfromIBLinee(getIBLineerialInput,
												arg0);

								if (RapidSourceCenter != null
										&& (RapidSourceCenter.length() > 0)) {

									OdrlineElem.getChildElement("Extn")
											.setAttribute("RapidSourceCenter",
													RapidSourceCenter);
									OdrlineElem.setAttribute("ShipNode",
											RapidSourceCenter);

								}

							}
						}

						catch (Exception e) {
							// throw new YFSException(e.getMessage());
							throw new YFSException(
									"incorrect message processing",
									"EXTNTDYN0010", e.getMessage());
						}
					}

				}

			}

		}

		return inXML;
	}

	protected ArrayList<String> getCustomerId(String oderheaderkey) {
		String sCustomerId = null;
		String sExchangeType = null;

		YFCDocument inputXML = YFCDocument
				.getDocumentFor("<Order OrderHeaderKey='" + oderheaderkey
						+ "' />");
		Document getOdrlist = null;
		YFCDocument getItemListOPtemp = YFCDocument
				.getDocumentFor("<OrderList  TotalNumberOfRecords=\"\" ><Order ReceivingNode=\"\" ExchangeType=\"\"/></OrderList>");
		getOdrlist = invokeApi("getOrderList", inputXML.getDocument(),
				getItemListOPtemp.getDocument());
		if (_cat.isVerboseEnabled()) {

			_cat.verbose("------------------customer id output"
					+ YFCDocument.getDocumentFor(getOdrlist).toString());

		}

		if (YFCDocument.getDocumentFor(getOdrlist).getDocumentElement()
				.getElementsByTagName("Order").getLength() > 0)

		{

			YFCElement CustOrdeElem = YFCDocument.getDocumentFor(getOdrlist)
					.getDocumentElement().getElementsByTagName("Order").item(0);

			if (CustOrdeElem.hasAttribute("ReceivingNode") &&

			CustOrdeElem.getAttribute("ReceivingNode").length() > 0)

			{

				sCustomerId = CustOrdeElem.getAttribute("ReceivingNode");

				custExchange.add(0, sCustomerId);

			}

			if (!XmlUtils.isVoid(CustOrdeElem.getAttribute("ExchangeType"))) {

				sExchangeType = CustOrdeElem.getAttribute("ExchangeType");

				custExchange.add(1, sExchangeType);
			}
		}

		return custExchange;
	}

	protected String getItemID(String sOrderLineKey) throws YFSException {
		YFCDocument ItemTypeListOP = null;
		YFCDocument inputXML = YFCDocument
				.getDocumentFor("<OrderLine OrderLineKey='" + sOrderLineKey
						+ "'/>");
		final Document getItemtypeOPTemp = getItemtypeOP();
		Document getItemtypeOP = null;
		getItemtypeOP = invokeApi("getOrderLineList", inputXML.getDocument(),
				getItemtypeOPTemp);
		ItemTypeListOP = YFCDocument.getDocumentFor(getItemtypeOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("----output to getItemID call is:"
					+ ItemTypeListOP.toString());
		}

		return ItemTypeListOP.getDocumentElement()
				.getElementsByTagName("ItemDetails").item(0)
				.getAttribute("ItemID");

	}

	private Document getItemtypeOP() throws YFSException {
		final YFCDocument getItemtypeOP = YFCDocument
				.getDocumentFor("<OrderLineList TotalNumberOfRecords=\"\">"
						+ "<OrderLine ><ItemDetails ItemID=\"\" ><PrimaryInformation ItemType=\"\" />"
						+ "</ItemDetails ></OrderLine></OrderLineList>");
		return getItemtypeOP.getDocument();

	}

}

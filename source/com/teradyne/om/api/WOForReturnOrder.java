package com.teradyne.om.api;

import java.rmi.RemoteException;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;


public class WOForReturnOrder {
	
	String sEnterpriseCode="";
	String sDocumentType="";
	String sOrderNo="";
	String sShipNode="";
	String sItemID="";
	String sPdtClass="";
	String sUOM="";
	String sOrderedQty="";
	String sSalesOrderLineKey="";
	String sGetOrderLineDetailApi= "getOrderLineDetails";
	String sCreateWOApi="createWorkOrder";
	String sDOAFlag="";
	String sOEMSerialNo="";
	
	YFCDocument inWO=null;
	YFCDocument ordDet=null;
	YFCDocument tempOrdDet=null;
	Document outputOrdDet=null;
	
	public void woForReturnOrder(YFSEnvironment env,Document inDoc) throws RuntimeException, RemoteException, YIFClientCreationException{
		try{
		YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleInput = inYFCDoc.getDocumentElement();
		
		sEnterpriseCode=eleInput.getAttribute("EnterpriseCode");
		sDocumentType=eleInput.getAttribute("DocumentType");
		sOrderNo=eleInput.getAttribute("OrderNo");
		
		YFCElement eleOrderLines = eleInput.getChildElement("OrderLines");
		if(!YFCElement.isVoid(eleOrderLines)){
			
			YFCIterable<YFCElement> allInputline = eleOrderLines.getChildren();
			for (YFCElement eleOrderLine : allInputline){
			//YFCElement eleOrderLine = eleOrderLines.getChildElement("OrderLine");
			
			if (!YFCElement.isVoid(eleOrderLine)){
				
				sShipNode= eleOrderLine.getAttribute("ShipNode");
				sOrderedQty= eleOrderLine.getAttribute("OrderedQty");
				sSalesOrderLineKey =eleOrderLine.getAttribute("DerivedFromOrderLineKey");
				
				YFCElement extnEle = eleOrderLine.getChildElement("Extn");
				if(!YFCElement.isVoid(extnEle)){
				sOEMSerialNo= extnEle.getAttribute("OEMSerialNo");
				sDOAFlag=extnEle.getAttribute("DOAFlag");
				}
				
							
			}
			
			/*ordDet = YFCDocument.getDocumentFor("<OrderLineDetail OrderLineKey=\"\"/>");
			ordDet.getDocumentElement().setAttribute("OrderLineKey", sSalesOrderLineKey);
			System.out.println("#####ordDet"	+ ordDet);
			tempOrdDet = YFCDocument.getDocumentFor("<OrderLine DerivedFromOrderLineKey=\"\" ><Extn OEMSerialNo=\"\" DOAFlag=\"\" /> </OrderLine>");
			env.setApiTemplate(sGetOrderLineDetailApi,tempOrdDet.getDocument());
			outputOrdDet = YIFClientFactory.getInstance().getLocalApi().invoke(env, sGetOrderLineDetailApi,ordDet.getDocument());
			
			YFCElement outOrderLineDet= YFCDocument.getDocumentFor(outputOrdDet).getDocumentElement();
			if(!YFCDocument.isVoid(outOrderLineDet)){
				 
				YFCElement extnEle = outOrderLineDet.getChildElement("Extn");
				if(!YFCDocument.isVoid(extnEle)){
					sOEMSerialNo= extnEle.getAttribute("OEMSerialNo");
					sDOAFlag=extnEle.getAttribute("DOAFlag");
					
				}
		
			}*/
			
			
			YFCElement eleItemLine = eleOrderLine.getChildElement("Item");
			
			if(!YFCElement.isVoid(eleItemLine)){
				sItemID= eleItemLine.getAttribute("ItemID");
				sPdtClass= eleItemLine.getAttribute("ProductClass");
				sUOM= eleItemLine.getAttribute("UnitOfMeasure");
			}
		
		
		inWO = YFCDocument.getDocumentFor("<WorkOrder  EnterpriseCode=\"\" ItemID=\"\" ProductClass=\"\" Uom=\"\" NodeKey=\"\" QuantityRequested=\"\" ServiceItemGroupCode=\"\" ServiceItemID=\"\" ><Order DocumentType=\"\" OrderNo=\"\" EnterpriseCode=\"\"><Extn Status=\"\" DOAFlag=\"\" AutoWIPFlag=\"\" OEMSerialNo=\"\"/></Order><WorkOrderComponents><WorkOrderComponent ComponentQuantity=\"\" ItemID=\"\"   ProductClass=\"\" Uom=\"\"/></WorkOrderComponents></WorkOrder>");
		
		YFCElement woEle = inWO.getDocumentElement();
		woEle.setAttribute("EnterpriseCode", sEnterpriseCode);
		woEle.setAttribute("ItemID", sItemID);
		woEle.setAttribute("Uom", sUOM);
		woEle.setAttribute("NodeKey", sShipNode);
		woEle.setAttribute("QuantityRequested", sOrderedQty);
		woEle.setAttribute("ServiceItemGroupCode", "COMPL");
		
		//check the status of good pdt
		woEle.setAttribute("ProductClass", "GOOD");
		//check for the serviceitemId
		woEle.setAttribute("ServiceItemID", "ServItem");
		woEle.setAttribute("SegmentType", "MTO");
		woEle.setAttribute("Segment", sOrderNo);
		
		YFCElement orderEle = woEle.getChildElement("Order");
		orderEle.setAttribute("DocumentType", sDocumentType);
		orderEle.setAttribute("OrderNo", sOrderNo);
		orderEle.setAttribute("EnterpriseCode", sEnterpriseCode);
		
		//to get the values from Sales order
		YFCElement extnEle = orderEle.getChildElement("Extn");
		extnEle.setAttribute("Status", "O");
		extnEle.setAttribute("DOAFlag", sDOAFlag);
		extnEle.setAttribute("AutoWIPFlag", "Y");
		extnEle.setAttribute("OEMSerialNo", sOEMSerialNo);
		
		YFCElement woCompsEle = woEle.getChildElement("WorkOrderComponents");
		YFCElement woCompEle = woCompsEle.getChildElement("WorkOrderComponent");
		woCompEle.setAttribute("ComponentQuantity", sOrderedQty);
		woCompEle.setAttribute("ItemID", sItemID);
		woCompEle.setAttribute("ProductClass", sPdtClass);
		woCompEle.setAttribute("Uom", sUOM);
		
		System.out.println("Invoking the CraeteWO API++++++++++" + inWO);
		YIFClientFactory.getInstance().getLocalApi().invoke(env, sCreateWOApi,inWO.getDocument());
		env.clearApiTemplate(sCreateWOApi);
			}//end of for orderlines
		}
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("######### Exception Caught : " + e.toString());
		}
		
	}

}

package com.teradyne.om.api;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.StringUtil;

import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;


public class LoadSCIBMapping {
	/**
	 * Create the service contract, install base mapping after importOrder API call.
	 * Expected inDoc -- 
	 * <Order OrderHeaderKey="" ><OrderLines><OrderLine OrderLineKey="">
	 * <Extn SystemSerialNo="" /></OrderLine></OrderLines></Order>
	 * 
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws YIFClientCreationException 
	 * @throws RemoteException 
	 */
	    
	public Document loadSCIBMapping(YFSEnvironment env, Document inputDoc) throws RemoteException, YIFClientCreationException{
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		//System.out.println("Input Doc"+inDoc.toString());
		YFCNodeList<YFCElement> orderLinesNL = inDoc.getDocumentElement().getElementsByTagName(XMLConstants.ORDER_LINE);
		Set<String> serialSet = new HashSet<String>();
		HashMap<String,ArrayList<YFCElement>> serialMapHM = new HashMap<String,ArrayList<YFCElement>>();
		YFCDocument multiInDoc = YFCDocument.createDocument(XMLConstants.MULTI_API_ELE);
		String orderHeaderKey = inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY);
		for(YFCElement orderLine : orderLinesNL){
			YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
			
			String orderLineKey = orderLine.getAttribute(XMLConstants.ORDER_LINE_KEY);
			if(extnEle != null){
				String systemSerialStr = extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO);
				if(systemSerialStr.contains(",")){
					String[] serials = systemSerialStr.split(",");
					for(int i = 0; i < serials.length; i++){
						addToSerialMap(serialMapHM,  serials[i], orderHeaderKey, orderLineKey);
					}
				}else if(!StringUtil.isEmpty(systemSerialStr)){
					addToSerialMap(serialMapHM, systemSerialStr, orderHeaderKey, orderLineKey);
				}
			}
		}//end-for
		if(serialMapHM.isEmpty()){
			return inputDoc;
		}
		fetchIBLineForSerial(env, serialMapHM);
		callMultiApi(env, serialMapHM);
		return inputDoc;
	}

	private void callMultiApi(YFSEnvironment env,
			HashMap<String, ArrayList<YFCElement>> serialMapHM) throws RemoteException, YIFClientCreationException {
		// TODO Auto-generated method stub
		YFCDocument multiInDoc = YFCDocument.createDocument(XMLConstants.MULTI_API_ELE);
		for(Iterator<String> iter = serialMapHM.keySet().iterator(); iter.hasNext();){
			ArrayList<YFCElement> scibMapping = serialMapHM.get(iter.next());
			for(int i = 0; i < scibMapping.size(); i++){
			
				YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
				apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
				apiEle.setAttribute(XMLConstants.NAME, Constants.CREATE_SC_IB_MAPPING_API);
				YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);

				YFCElement newMappingEle = multiInDoc.createElement(XMLConstants.TER_SCIB_MAPPING);
				newMappingEle = multiInDoc.importNode(scibMapping.get(i), false);
				inputEle.appendChild(newMappingEle);

				multiInDoc.getDocumentElement().appendChild(apiEle);
			}
		}
		//System.out.println("Input Doc for multiApi"+multiInDoc.toString());
		SterlingUtil.callAPI(env, Constants.MULTI_API, multiInDoc, "");
	}

	private void fetchIBLineForSerial(YFSEnvironment env,
			HashMap<String, ArrayList<YFCElement>> serialMapHM) throws RemoteException, YIFClientCreationException {
		// TODO Auto-generated method stub
		Set<String> serialSet = serialMapHM.keySet();
		final YFCDocument inputDoc = YFCDocument.getDocumentFor("<OrderLine DocumentType=\"\"> <Extn SystemSerialNo=\"\"/></OrderLine>");
		final YFCDocument templateDoc = YFCDocument.getDocumentFor("<OrderLineList><OrderLine OrderLineKey=\"\" OrderHeaderKey=\"\" /></OrderLineList>");
		inputDoc.getDocumentElement().setAttribute("DocumentType", "0017.ex");
		for(Iterator<String> iter = serialSet.iterator(); iter.hasNext();){
			String systemSerialNo = iter.next();
			inputDoc.getDocumentElement().getChildElement("Extn").setAttribute("SystemSerialNo", systemSerialNo);
			//System.out.println("Serial OrderLineList Input"+inputDoc);
			YFCDocument outDoc = SterlingUtil.callAPI(env, Constants.GET_ORDER_LINE_LIST, inputDoc, templateDoc);
			YFCElement orderLineEle = outDoc.getDocumentElement().getElementsByTagName(XMLConstants.ORDER_LINE).item(0);
			//System.out.println("Serial OrderLineList Output"+outDoc);
			String ibOLKey = orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
			String ibOHKey = orderLineEle.getAttribute(XMLConstants.ORDER_HEADER_KEY);
			
			for(int i = 0; i <  serialMapHM.get(systemSerialNo).size(); i++){
				serialMapHM.get(systemSerialNo).get(i).setAttribute(XMLConstants.TER_IBOL_KEY, ibOLKey);
				serialMapHM.get(systemSerialNo).get(i).setAttribute(XMLConstants.TER_IBOH_KEY, ibOHKey);
			}
			//System.out.println("Serial"+systemSerialNo+":: ArrayList"+serialMapHM.get(systemSerialNo).get(0).toString());
		}
		
	}

	private void addToSerialMap(HashMap<String, ArrayList<YFCElement>> serialMapHM,
			String serialStr, String ohKey, String olKey ) {
		// TODO Auto-generated method stub
		YFCElement scibMapEle = YFCDocument.createDocument(XMLConstants.TER_SCIB_MAPPING).getDocumentElement();
		scibMapEle.setAttribute(XMLConstants.TER_SCOH_KEY, ohKey);
		scibMapEle.setAttribute(XMLConstants.TER_SCOL_KEY, olKey);
		if(serialMapHM.containsKey(serialStr)){
			ArrayList<YFCElement> scibMapping = serialMapHM.get(serialStr);
			scibMapping.add(scibMapEle);
			
		}else{
			ArrayList<YFCElement> scibMapping = new ArrayList<YFCElement>();
			scibMapping.add(scibMapEle);
			serialMapHM.put(serialStr, scibMapping);
		}
		
	}

}

package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class ConfirmSalesOrder {

  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  private SalesOrderUtils orderUtils = new SalesOrderUtils();

  public Document confirmSalesOrder(YFSEnvironment env, Document inDoc)
      throws YIFClientCreationException, Exception {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();

    confirmOrder(env, inDoc);
    scheduleOrder(env, yfcInDocEle);
    Document output = releaseOrder(env, yfcInDocEle);

    return output;

  }

  protected Document confirmOrder(YFSEnvironment env, Document inDoc) throws Exception {

    log.debug("Confirm draft Order");
    YFCDocument template = YFCDocument.getDocumentFor("<Order OrderHeaderKey=\"\" />");
    return orderUtils.callApi(env, inDoc, template.getDocument(),
        Constants.CONFIRM_DRAFT_ORDER_API, Constants.TRUE);
  }

  private Document scheduleOrder(YFSEnvironment env, YFCElement element)
      throws YIFClientCreationException, Exception {

    log.debug("Scheduling Order");
    String strInput =
        "<ScheduleOrder OrderHeaderKey=\"" + element.getAttribute("OrderHeaderKey") + "\" />";
    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);

    Document outputDoc =
        orderUtils.callApi(env, inputDoc.getDocument(), null, Constants.SCHEDULEORDER_API,
            Constants.TRUE);
    return outputDoc;

  }

  private Document releaseOrder(YFSEnvironment env, YFCElement element)
      throws YIFClientCreationException, Exception {

    log.debug("Releasing Order");
    String strInput =
        "<ReleaseOrder CheckInventory=\"N\" OrderHeaderKey=\""
            + element.getAttribute("OrderHeaderKey") + "\" />";
    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);

    Document outputDoc =
        orderUtils.callApi(env, inputDoc.getDocument(), null, Constants.RELEASEORDER_API,
            Constants.TRUE);
    return outputDoc;
  }

}

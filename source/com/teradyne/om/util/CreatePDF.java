package com.teradyne.om.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRXmlDataSource;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.DateTimeUtil;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;


public class CreatePDF {
	
	private Properties properties = null;
	
	public void setProperties(Properties prop) throws Exception {
	    properties = prop;
	  }  

	public void exportPdf(Document output, String jrxmlpath, boolean isStatic, int count,
		      ArrayList<String> list,String logoImagePath) throws JRException {
		    
		    Map<String, Object> parameters = new HashMap<String, Object>();
		    setLogoImageInMap(parameters, logoImagePath);

		    JasperReport jasperReport =
		        JasperCompileManager.compileReport(this.getClass().getClassLoader()
		            .getResourceAsStream(jrxmlpath));
		    String filepath = getFilePath(output, count);

		    if (!isStatic) {
		      JRXmlDataSource dataSource = new JRXmlDataSource(output);
		      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
		      JasperExportManager.exportReportToPdfFile(jasperPrint, filepath);
		    } else {
		      JREmptyDataSource dataSource = new JREmptyDataSource();
		      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
		      JasperExportManager.exportReportToPdfFile(jasperPrint, filepath);
		    }

		    list.add(filepath);

	}
	
	public String doMerge(Document output, ArrayList<String> fileNameList) throws IOException, DocumentException {

	    String filename = getFilePath(output, -1);
	    
	    OutputStream out =
	        new FileOutputStream(new File(filename));

	    com.itextpdf.text.Document document = new com.itextpdf.text.Document();

	    PdfCopy copy = new PdfCopy(document, out);

	    document.open();

	    PdfReader reader = null;

	    for (String fileName : fileNameList) {
	      reader = new PdfReader(fileName);
	      int n = reader.getNumberOfPages();
	      for (int page = 0; page < n;) {
	        copy.addPage(copy.getImportedPage(reader, ++page));
	      }
	      copy.freeReader(reader);
	      reader.close();
	      File file = new File(fileName);
	      if(file.exists()){
	        file.delete();
	      }
	    }

	    document.close();
	    return filename;
	  }

	
	private String getFilePath(Document inDoc, int count) {

	    YFCDocument document = YFCDocument.getDocumentFor(inDoc);
	    YFCElement element = document.getDocumentElement();
	    String filename = null;
	    
	    if (!XmlUtils.isVoid(element)) {
	      String strOrderHeaderKey = element.getAttribute("OrderHeaderKey");

	      if (count == 0) {
	        filename = strOrderHeaderKey + "_temp.pdf";
	      } else if (count == 1) {
	        filename = strOrderHeaderKey + "_terms.pdf";
	      } else if (count == 2) {
	        filename = strOrderHeaderKey + "_terms1.pdf";
	      } else {
	    	String docType = element.getAttribute(XMLConstants.DOCUMENT_TYPE);
	    	if(docType.equals("0018.ex")){
	    		String enterprise = element.getAttribute(XMLConstants.ENTERPRISE_CODE);
	    		String orderNo = element.getAttribute(XMLConstants.ORDER_NO);
	    		filename = enterprise +"_"+ orderNo +"_"+ getSysDate()+".pdf";
	    	}else{
	    		filename = "Quote_" + strOrderHeaderKey + ".pdf";
	    	}
	      }
	    }
	    
	    return properties.getProperty("filePath") + filename;
	}
	
	

	private String getSysDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyddMMHHmmss");
		java.util.Date now = new java.util.Date();
		String strDate = sdfDate.format(now);
		//String strTime = sdfTime.format(now);
		return strDate;
	}

	private void setLogoImageInMap(Map<String, Object> parameters, String logoImagePath){
	    
	    if(!XmlUtils.isVoid(logoImagePath)){
	      InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(logoImagePath);
	      parameters.put(Constants.PARAM_TITLE, inputStream);
	    }
	  }
	  

}

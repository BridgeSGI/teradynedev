package com.teradyne.om.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.bridge.sterling.utils.SterlingUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class SalesOrderUtils {

  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  
  public void setBuyer(YFSEnvironment env, YFCDocument inYFCXML) throws Exception {
	  YFCElement order = inYFCXML.getDocumentElement();
	  String docType = order.getAttribute(XMLConstants.DOCUMENT_TYPE);
	  System.out.println("Document Type: " + docType);
	  if (docType.equals(Constants.DOCUMENT_TYPE_SO)) {
		  String receivingNode = order.getAttribute(XMLConstants.RECEIVING_NODE);
		  if (XmlUtils.isVoid(receivingNode)) {
			  System.out.println("Calling getOrderList");
			  receivingNode = fetchReceivingNodeUsingAPICall(env, order.getAttribute(XMLConstants.ORDER_HEADER_KEY));
		  }
		  System.out.println("Receiving Node: " + receivingNode);
		  order.setAttribute(XMLConstants.BUYER_ORGANIZATION_CODE, receivingNode);
	  }
	  if (docType.equals(Constants.DOCUMENT_TYPE_TO)) {
		  order.setAttribute(XMLConstants.BUYER_ORGANIZATION_CODE, Constants.CSO);
	  }
}

  private String fetchReceivingNodeUsingAPICall(YFSEnvironment env,	String orderHeaderKey) throws Exception {
	  YFCDocument inputDoc = YFCDocument.parse("<Order OrderHeaderKey=\"" + orderHeaderKey + "\" />");
	  System.out.println("GetOrderList input: " + inputDoc);
	  YFCDocument template = YFCDocument.parse("<OrderList><Order OrderNo=\"\" ReceivingNode=\"\" /></OrderList>");
	  YFCDocument output = SterlingUtil.callAPI(env, Constants.GET_ORDER_LIST, inputDoc, template);
	  System.out.println("GetOrderList output: " + output);
	  YFCElement orderList = output.getDocumentElement();
	  YFCElement order = orderList.getChildElement(Constants.ORDER);
	  return order.getAttribute(XMLConstants.RECEIVING_NODE);
}

public YFCDocument updateOpenBlanketPOBalanceinLine(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInEle = yfcInDoc.getDocumentElement();

    String strRecvNode = yfcInEle.getAttribute(XMLConstants.RECEIVING_NODE);
    String strPurchaseOrderNo = yfcInEle.getAttribute(XMLConstants.CUSTOMER_PO_NO);

    if (!XmlUtils.isVoid(strPurchaseOrderNo) && !XmlUtils.isVoid(strRecvNode)) {
      YFCElement orderlinesEle = yfcInEle.getChildElement(XMLConstants.ORDER_LINES);

      if (!XmlUtils.isVoid(orderlinesEle)) {
        YFCIterable<YFCElement> orderlineList = orderlinesEle.getChildren(XMLConstants.ORDER_LINE);
        for (YFCElement orderLine : orderlineList) {
          setPOBalance(env, orderLine, strRecvNode, strPurchaseOrderNo);
        }
      }
    }
    return yfcInDoc;
  }

  private void setPOBalance(YFSEnvironment env, YFCElement orderLine, String strRecvNode,
      String strPurchaseOrderNo) {

    String strPOBalance = "";
    YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
    if (!XmlUtils.isVoid(extnEle)) {
      strPOBalance = extnEle.getAttribute(XMLConstants.OPEN_BALANCE);
    } else {
      extnEle = orderLine.createChild(XMLConstants.EXTN);
    }

    if (XmlUtils.isVoid(strPOBalance)) {
      String input =
          "<Input xmlns=\"" + XMLConstants.SPWP_GET_OPEN_BLANKET_PO_BALANCE_NAMESPACE
              + "\"><GetOpenBlanketPOBalance ShipToCustomerNumber=\"" + strRecvNode
              + "\" ShipToCustomerSiteNumber=\"" + strRecvNode + "\" BPONo=\"" + strPurchaseOrderNo
              + "\" /></Input>";

      YFCDocument inputDoc = YFCDocument.getDocumentFor(input);

      try {
        Document output =
            callApi(env, inputDoc.getDocument(), null, Constants.SPWP_GET_OPEN_BLANKET_PO_BALANCE,
                Constants.FALSE);
        if (!XmlUtils.isVoid(output)) {
          YFCDocument document = YFCDocument.getDocumentFor(output);
          YFCElement element = document.getDocumentElement();

          if (element != null && element.hasChildNodes()) {

            YFCElement elePOBalance = element.getFirstChildElement();
            strPOBalance = elePOBalance.getAttribute(XMLConstants.PO_BALANCE);
          }
        }
      } catch (Exception e) {
        log.debug(e);
        strPOBalance = "";
      }
    }

    extnEle.setAttribute(XMLConstants.OPEN_BALANCE, strPOBalance);
  }

  public boolean canOrderConsummable(YFSEnvironment env, YFCDocument yfcInDoc) throws YFSException {

    YFCElement yfcElement = yfcInDoc.getDocumentElement();

    String strEnterpriseCode = yfcElement.getAttribute(XMLConstants.ENTERPRISE_CODE);
    YFCElement orderLinesEle = yfcElement.getChildElement(XMLConstants.ORDER_LINES);


    if (!XmlUtils.isVoid(orderLinesEle)) {

      YFCIterable<YFCElement> orderLineList = orderLinesEle.getChildren(XMLConstants.ORDER_LINE);
      for (YFCElement orderLine : orderLineList) {

        YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
        if (!XmlUtils.isVoid(extnEle)) {

          String strAgreementNo = extnEle.getAttribute(XMLConstants.AGREEMENT_NO);
          if (!XmlUtils.isVoid(strAgreementNo)) {
            String orderType =
                getOrderType(env, strAgreementNo, strEnterpriseCode, Constants.DOCUMENT_TYPE_SC);

            if (!XmlUtils.isVoid(orderType))
              validatecanOrderConsummable(env, orderType);
          }
        }
      }
    }
    return true;
  }

  private String getOrderType(YFSEnvironment env, String orderNo, String strEnterpriseCode,
      String documentType) {

    String strOrderType = null;
    String input =
        "<Order OrderNo=\"" + orderNo + "\" EnterpriseCode=\"" + strEnterpriseCode
            + "\" DocumentType=\"" + documentType + "\" />";
    String template = "<OrderList><Order OrderType=\"\" /></OrderList>";

    YFCDocument inputDoc = YFCDocument.getDocumentFor(input);
    YFCDocument templateDoc = YFCDocument.getDocumentFor(template);

    Document output;
    try {
      output =
          callApi(env, inputDoc.getDocument(), templateDoc.getDocument(), Constants.GET_ORDER_LIST,
              Constants.TRUE);
    } catch (Exception e) {
      throw (YFSException) e;
    }

    if (!XmlUtils.isVoid(output)) {

      YFCDocument document = YFCDocument.getDocumentFor(output);
      YFCElement element = document.getDocumentElement();

      if (element.hasChildNodes()) {
        YFCElement orderEle = element.getFirstChildElement();
        strOrderType = orderEle.getAttribute(XMLConstants.ORDER_TYPE);
      } else {
        YFSException ex = new YFSException();
        ex.setErrorCode("TD000021");
        ex.setErrorDescription("waranty No " + orderNo + " is invalid");
      }
    }
    return strOrderType;
  }

  public void isRPDEligible(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInEle = yfcInDoc.getDocumentElement();

    String strRecvNode = yfcInEle.getAttribute(XMLConstants.RECEIVING_NODE);

    YFCElement orderLinesEle = yfcInEle.getChildElement(XMLConstants.ORDER_LINES);
    if (!XmlUtils.isVoid(orderLinesEle)) {

      YFCIterable<YFCElement> list = orderLinesEle.getChildren(XMLConstants.ORDER_LINE);
      for (YFCElement orderLine : list) {

        YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
        YFCElement itemEle = orderLine.getChildElement(XMLConstants.ITEM);

        if (!XmlUtils.isVoid(extnEle)) {
          String strServiceType = extnEle.getAttribute(XMLConstants.SERVICE_TYPE);

          if (!XmlUtils.isVoid(strServiceType) && "RPD".equalsIgnoreCase(strServiceType)) {
            String strSystemType = "";
            String strSystemSerialNo = "";
            String strItemId = "";
            if (!XmlUtils.isVoid(extnEle)) {
              strSystemSerialNo = extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO);
              strSystemType = extnEle.getAttribute(XMLConstants.SYSTEM_TYPE);
            }

            if (!XmlUtils.isVoid(itemEle)) {
              strItemId = itemEle.getAttribute(XMLConstants.ITEM_ID);
            }

            validateServiceTypeRPD(env, strRecvNode, strItemId, strSystemSerialNo, strSystemType);
          }
        }

      }
    }
  }

  /**
   * <Input> <CheckOPPOSupport SystemSerialNoAndServiceType="" /></Input> value for
   * SystemSerialNoAndServiceType will be in this format XYZ:123
   * 
   * @param env
   * @param yfcInDoc
   * @return
   */
  public boolean checkOPPOSupport(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInEle = yfcInDoc.getDocumentElement();

    YFCElement orderLinesEle = yfcInEle.getChildElement(XMLConstants.ORDER_LINES);
    if (!XmlUtils.isVoid(orderLinesEle)) {

      YFCIterable<YFCElement> list = orderLinesEle.getChildren(XMLConstants.ORDER_LINE);
      for (YFCElement orderLine : list) {

        YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
        if (!XmlUtils.isVoid(extnEle)) {
          String strServiceType = extnEle.getAttribute(XMLConstants.SERVICE_TYPE);

          if (!XmlUtils.isVoid(strServiceType)) {

            String strSystemSerialNo = "";

            if (!XmlUtils.isVoid(extnEle)) {
              strSystemSerialNo = extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO);
            }

            validateCheckOPPOSupport(env, strSystemSerialNo, strServiceType);
          }
        }

      }
    }

    return true;
  }

  private void validateServiceTypeRPD(YFSEnvironment env, String strRecvNode, String ItemId,
      String systemSerial, String systemType) {
    Document output = null;
    if (!XmlUtils.isVoid(systemType) && !XmlUtils.isVoid(strRecvNode)) {
      String input =
          "<Input xmlns=\"" + XMLConstants.SPWP_IS_RPD_ELIGIBLE_NAMESPACE
              + "\"><IsRPDEligible CustomerNumber=\"" + strRecvNode + "\" CustomerSiteNo=\""
              + strRecvNode + "\" SystemType=\"" + systemType + "\" SystemSerialNumber=\""
              + systemSerial + "\" PartNumber=\"" + ItemId + "\" /></Input>";

      YFCDocument document = YFCDocument.getDocumentFor(input);
      try {
        output =
            callApi(env, document.getDocument(), null, Constants.SPWP_IS_RPD_ELIGIBLE,
                Constants.FALSE);
      } catch (Exception e) {
        log.debug(e);
        YFSException ex = (YFSException) e;
        throw ex;
      }
      if (!validateOutputForBoService(output)) {
        YFSException ex = new YFSException();
        ex.setErrorCode("TD00011");
        ex.setErrorDescription("Service Type RPD is not supported for this Customer / System");
        throw ex;
      }
    }

  }

  private void validatecanOrderConsummable(YFSEnvironment env, String orderType) {

    Document output = null;

    if (!XmlUtils.isVoid(orderType)) {
      String input =
          "<Input><CanOrderConsummable PartWarranty=\"" + orderType + "\" EntitlementType=\""
              + orderType + "\"/></Input>";
      YFCDocument inputDoc = YFCDocument.getDocumentFor(input);
      try {
        output =
            callApi(env, inputDoc.getDocument(), null, Constants.SPWP_CAN_ORDER_CONSUMMABLE,
                Constants.FALSE);
      } catch (Exception e) {
        log.debug(e);
        YFSException ex = (YFSException) e;
        throw ex;
      }
      if (!validateOutputForBoService(output)) {
        YFSException ex = new YFSException();
        ex.setErrorCode("TD00013");
        ex.setErrorDescription("Order is Not consummable");
        throw ex;
      }
    }
  }

  private void validateCheckOPPOSupport(YFSEnvironment env, String systemserialNo,
      String serviceType) {
    Document output = null;

    if (!XmlUtils.isVoid(systemserialNo) && !XmlUtils.isVoid(serviceType)) {
      String value = systemserialNo + ":" + serviceType;
      String input =
          "<Input xmlns=\"" + XMLConstants.SPWP_CHECK_OPPO_SUPPORT_NAMESPACE
              + "\"><CheckOPPOSupport SystemSerialNoAndServiceType=\"" + value + "\" /></Input>";
      YFCDocument document = YFCDocument.getDocumentFor(input);
      try {
        output =
            callApi(env, document.getDocument(), null, Constants.SPWP_CHECK_OPPO_SUPPORT,
                Constants.FALSE);
      } catch (Exception e) {
        log.debug(e);
        YFSException ex = (YFSException) e;
        throw ex;
      }
      if (!validateOutputForBoService(output)) {
        YFSException ex = new YFSException();
        ex.setErrorCode("TD00012");
        ex.setErrorDescription("CheckOPPOSupport Service -- Validation failed for the values "
            + systemserialNo + "," + serviceType);
        throw ex;
      }
    }
  }

  private boolean validateOutputForBoService(Document output) {

    if (!XmlUtils.isVoid(output)) {

      YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
      YFCElement element = outputDoc.getDocumentElement();

      /*
       * Bo service which is used for validation will have child nodes otherwise it will have root
       * element alone. Root element is "output".
       */
      return element.hasChildNodes();
    }
    return true;
  }

  public YFCDocument setTeradyneBusGrpAndSystemType(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement element = yfcInDoc.getDocumentElement();

    YFCElement orderLines = element.getChildElement("OrderLines");

    if (!XmlUtils.isVoid(orderLines)) {
      YFCIterable<YFCElement> orderLineList = orderLines.getChildren("OrderLine");

      for (YFCElement orderLine : orderLineList) {
        if (XMLConstants.CREATE.equalsIgnoreCase(orderLine.getAttribute("Action"))) {
          YFCElement extnEle = orderLine.getChildElement(Constants.EXTN);
          String strBusinessGrp = "";
          String strSystemType = "";
          if (!XmlUtils.isVoid(extnEle)) {
            strBusinessGrp = extnEle.getAttribute("BusinessGrp");
            strSystemType = extnEle.getAttribute("SystemType");
          } else {
            extnEle = orderLine.createChild(Constants.EXTN);
          }

          if (XmlUtils.isVoid(strBusinessGrp) || XmlUtils.isVoid(strSystemType)) {
            log.debug("Business group and system type is setting");
            Map<String, String> outputMap = getTeradynBusGrpAndSystemType(env, orderLine);
            extnEle.setAttribute("BusinessGrp", outputMap.get(Constants.TD_BUS_GROUP));
            extnEle.setAttribute("SystemType", outputMap.get(Constants.SO_SYSTEM_TYPE));
          }
        }
      }
    }
    return yfcInDoc;
  }


  public YFCDocument setAutoNoChargeCode(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();

    YFCElement orderLines = yfcInDocEle.getChildElement("OrderLines");
    if (!XmlUtils.isVoid(orderLines)) {

      YFCIterable<YFCElement> orderLineList = orderLines.getChildren("OrderLine");
      for (YFCElement orderLine : orderLineList) {
        if ("0001".equalsIgnoreCase(yfcInDocEle.getAttribute(XMLConstants.DOCUMENT_TYPE))) {

          String strOrderType = yfcInDocEle.getAttribute(XMLConstants.ORDER_TYPE);
          YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
          if (!XmlUtils.isVoid(extnEle)) {

            String strHandlingNoChargeCode =
                extnEle.getAttribute(XMLConstants.HANDLING_NO_CHARGE_CODE);
            if ((Constants.Y).equalsIgnoreCase(strHandlingNoChargeCode)) {

              if ((XMLConstants.PIP_ORDER_TYPE).equalsIgnoreCase(strOrderType)) {
                extnEle.setAttribute(XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "H");
              } else if (isOutOfBoxFailureOrderLine(orderLine)) {
                extnEle.setAttribute(XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "K");
              } else {
                extnEle.setAttribute(XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "J");
              }

            } else {
              extnEle.setAttribute(XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "");
            }
          }
        }
      }
    }
    return yfcInDoc;
  }

  private boolean isOutOfBoxFailureOrderLine(YFCElement orderLine) {

    boolean flag = false;
    YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);

    if (!XmlUtils.isVoid(extnEle)) {

      String strOutOfBoxFailure = extnEle.getAttribute(XMLConstants.OUT_OF_BOX_FAILURE);
      if ((Constants.N).equalsIgnoreCase(strOutOfBoxFailure))
        flag = true;
    }
    return flag;
  }

  public YFCDocument updateQtyOrderedToday(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInEle = yfcInDoc.getDocumentElement();

    String strRecvNode = yfcInEle.getAttribute("ReceivingNode");
    String strOrgCode = yfcInEle.getAttribute("EnterpriseCode");
    YFCElement yfcOrderLinesEle = yfcInEle.getChildElement("OrderLines");

    if (!XmlUtils.isVoid(yfcOrderLinesEle)) {

      YFCIterable<YFCElement> orderLineList = yfcOrderLinesEle.getChildren("OrderLine");

      for (YFCElement orderLine : orderLineList) {
        setQtyOrderedToday(env, orderLine, strRecvNode, strOrgCode);
      }
    }

    return yfcInDoc;
  }

  private void setQtyOrderedToday(YFSEnvironment env, YFCElement orderLineEle, String strRecvNode,
      String strOrgCode) {

    YFCElement extnEle = orderLineEle.getChildElement(Constants.EXTN);
    YFCElement itemEle = orderLineEle.getChildElement(XMLConstants.ITEM);

    if (!XmlUtils.isVoid(itemEle) && !XmlUtils.isVoid(itemEle.getAttribute("ItemID"))
        && !XmlUtils.isVoid(itemEle.getAttribute("ProductClass"))) {

      if (XmlUtils.isVoid(extnEle)) {
        extnEle = orderLineEle.createChild(Constants.EXTN);
      }

      extnEle.setAttribute("QTYOrderToday",
          calculateQtyOrderedToday(env, strRecvNode, orderLineEle, strOrgCode));
    }
  }

  private String calculateQtyOrderedToday(YFSEnvironment env, String strRecvNode,
      YFCElement orderLineEle, String strOrgCode) {

    SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
    String today = dateFormat.format(new Date());

    YFCElement itemEle = orderLineEle.getChildElement(XMLConstants.ITEM);
    String strItemId = itemEle.getAttribute("ItemID");
    String strPC = itemEle.getAttribute("ProductClass");

    YFCElement orderLinetranQtyEle =
        orderLineEle.getChildElement(XMLConstants.ORDER_LINE_TRAN_QUANTITY);

    String orderLinetranqty = "0.00";
    if (!XmlUtils.isVoid(orderLinetranQtyEle)
        && !XmlUtils.isVoid(orderLinetranQtyEle.getAttribute(XMLConstants.ORDERED_QTY))) {
      orderLinetranqty = orderLinetranQtyEle.getAttribute(XMLConstants.ORDERED_QTY);
    }

    double orderedToday = Double.parseDouble(orderLinetranqty);
    String strInput =
        "<OrderLine ReceivingNode=\"" + strRecvNode + "\"><Item ItemID=\"" + strItemId
            + "\" ProductClass=\"" + strPC + "\"/><Order EnterpriseCode=\"" + strOrgCode
            + "\" DocumentType=\"0001\" FromOrderDate=\"" + today + Constants.FROM_TIME_STAMP
            + "\" ToOrderDate=\"" + today + Constants.TO_TIME_STAMP + "\" OrderDateQryType=\""
            + Constants.BETWEEN + "\"/></OrderLine>";
    String strTemp =
        "<OrderLineList><OrderLine OrderLineKey=\"\" OrderedQty=\"\" Status=\"\"><Order OrderHeaderKey=\"\" /></OrderLine></OrderLineList>";

    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);
    YFCDocument template = YFCDocument.getDocumentFor(strTemp);

    try {
      Document outputDoc =
          callApi(env, inputDoc.getDocument(), template.getDocument(), "getOrderLineList", true);

      YFCDocument yfcOutput = YFCDocument.getDocumentFor(outputDoc);
      YFCElement yfcOutputEle = yfcOutput.getDocumentElement();

      if (!XmlUtils.isVoid(yfcOutputEle)) {
        YFCNodeList<YFCElement> orderList = yfcOutputEle.getElementsByTagName("OrderLine");

        for (int i = 0; i < orderList.getLength(); i++) {
          YFCElement yfcOrderLine = orderList.item(i);

          if ("CANCELLED".equalsIgnoreCase(yfcOrderLine.getAttribute("Status"))) {
            continue;
          }

          String modifiedLineKey = orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
          String lineKey = yfcOrderLine.getAttribute(XMLConstants.ORDER_LINE_KEY);

          if (!(!XmlUtils.isVoid(modifiedLineKey) && modifiedLineKey.equalsIgnoreCase(lineKey))) {
            String strOrderQty = yfcOrderLine.getAttribute("OrderedQty");
            orderedToday += Double.parseDouble(strOrderQty);
          }
        }
      }

    } catch (Exception e) {
      if (log.isVerboseEnabled()) {
        log.verbose(e);
      }
    }
    log.debug("Ordered Quantity Today for customer-site is " + orderedToday);
    return Double.toString(orderedToday);
  }

  private Map<String, String> getTeradynBusGrpAndSystemType(YFSEnvironment env, YFCElement orderLine) {

    Map<String, String> outputMap = new HashMap<String, String>();

    try {
      Document output = callGetOrderListForGettingreqValues(env, orderLine);

      YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
      YFCElement outputEle = outputDoc.getDocumentElement();

      YFCIterable<YFCElement> orderList = outputEle.getChildren("Order");
      for (YFCElement order : orderList) {
        YFCElement orderLinesEle = order.getChildElement("OrderLines");
        YFCIterable<YFCElement> outOrderLineList = orderLinesEle.getChildren("OrderLine");

        for (YFCElement outOrderLine : outOrderLineList) {
          // It always have only one order-line.
          YFCElement eleItemDetails = outOrderLine.getChildElement("ItemDetails");
          YFCElement eleExtnItem = eleItemDetails.getChildElement(Constants.EXTN);

          if (!XmlUtils.isVoid(eleItemDetails)) {
            outputMap.put(Constants.SO_SYSTEM_TYPE, eleItemDetails.getAttribute("ItemID"));
          } else {
            outputMap.put(Constants.SO_SYSTEM_TYPE, "");
          }

          if (!XmlUtils.isVoid(eleExtnItem)) {
            outputMap.put(Constants.TD_BUS_GROUP, eleExtnItem.getAttribute("SystemTesterGroup"));
          } else {
            outputMap.put(Constants.TD_BUS_GROUP, "");
          }
        }

      }
    } catch (Exception e) {
      outputMap.put(Constants.SO_SYSTEM_TYPE, "");
      outputMap.put(Constants.TD_BUS_GROUP, "");
      e.printStackTrace();
    }
    return outputMap;
  }

  private Document callGetOrderListForGettingreqValues(YFSEnvironment env, YFCElement orderLine)
      throws Exception {

    // TODO: I need to add status. Need to discuss with pranav

    YFCElement extnEle = orderLine.getChildElement(Constants.EXTN);
    String strInput =
        "<Order DocumentType=\"" + Constants.DOCUMENT_TYPE_IB + "\"><OrderLine>"
            + "<Extn SystemSerialNo=\"" + extnEle.getAttribute("SystemSerialNo") + "\" />"
            + "<Item ItemType=\"SYSTEM\" /></OrderLine></Order>";
    String template =
        "<OrderList><Order DocumentType=\"\"><OrderLines><OrderLine OrderedQty=\"\">"
            + "<ItemDetails ItemID=\"\"><Extn SystemTesterGroup=\"\" /></ItemDetails>"
            + "</OrderLine></OrderLines></Order></OrderList>";

    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);
    YFCDocument templateDoc = YFCDocument.getDocumentFor(template);

    return callApi(env, inputDoc.getDocument(), templateDoc.getDocument(),
        Constants.GET_ORDER_LIST, true);
  }

  /*public YFCDocument setCurrency(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInEle = yfcInDoc.getDocumentElement();
    YFCElement yfcPriceInfo = yfcInEle.getChildElement("PriceInfo");

    String strCurrency = getCurrency(env, yfcInEle);

    if (XmlUtils.isVoid(yfcPriceInfo)) {
      yfcPriceInfo = yfcInEle.createChild("PriceInfo");
    }
    yfcPriceInfo.setAttribute("Currency", strCurrency);

    return yfcInDoc;
  }

  private String getCurrency(YFSEnvironment env, YFCElement orderEle) {

    String currency = Constants.CUR_US; // Default

    YFCElement yfcPriceInfo = orderEle.getChildElement("PriceInfo");
    if (!XmlUtils.isVoid(yfcPriceInfo) && !XmlUtils.isVoid(yfcPriceInfo.getAttribute("Currency"))) {
      currency = yfcPriceInfo.getAttribute("Currency");;
    } else {
      String strReceivingCode = orderEle.getAttribute("ReceivingNode");

      String getLocaleCode = getLocaleCodeForOrganization(env, strReceivingCode);

      if ("zh_CN".equalsIgnoreCase(getLocaleCode)) {
        currency = Constants.CUR_CHINA;
      } else if ("ja_JP".equalsIgnoreCase(getLocaleCode)) {
        currency = Constants.CUR_JAPAN;
      } else if ("ko_KR".equalsIgnoreCase(getLocaleCode)) {
        currency = Constants.CUR_KOREA;
      } else if ("es_ES".equalsIgnoreCase(getLocaleCode)) {
        currency = Constants.CUR_SPANISH;
      }

    }

    return currency;

  }

  private String getLocaleCodeForOrganization(YFSEnvironment env, String strReceNode) {

    String localeCode = "";
    YFCDocument input =
        YFCDocument.getDocumentFor("<Organization OrganizationKey=\"" + strReceNode + "\" />");
    YFCDocument template =
        YFCDocument.getDocumentFor("<Organization OrganizationKey=\"\" LocaleCode=\"\" />");

    Document output;
    try {
      output =
          callApi(env, input.getDocument(), template.getDocument(), "getOrganizationHierarchy",
              true);
      YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
      YFCElement outputEle = outputDoc.getDocumentElement();

      if (!XmlUtils.isVoid(outputEle)) {
        localeCode = outputEle.getAttribute("LocaleCode");
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

    return (localeCode == null) ? "" : localeCode;
  }*/

  public YFCDocument setPlannerCode(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();

    String strEnterpriseCode = yfcInDocEle.getAttribute(XMLConstants.ENTERPRISE_CODE);
    YFCElement orderlinesEle = yfcInDocEle.getChildElement(XMLConstants.ORDER_LINES);
    if (!XmlUtils.isVoid(orderlinesEle)) {

      YFCNodeList<YFCElement> orderLineList =
          orderlinesEle.getElementsByTagName(XMLConstants.ORDER_LINE);
      for (int i = 0; i < orderLineList.getLength(); i++) {
        YFCElement orderline = orderLineList.item(i);
        YFCElement itemEle = orderline.getChildElement(XMLConstants.ITEM);
        if (!XmlUtils.isVoid(itemEle)
            && !XmlUtils.isVoid(itemEle.getAttribute(XMLConstants.ITEM_ID))) {
          String plannerCode = getPlannerCode(env, orderline, strEnterpriseCode);
          YFCElement extnEle = orderline.getChildElement(XMLConstants.EXTN);
          if (XmlUtils.isVoid(extnEle)) {
            extnEle = orderline.createChild(XMLConstants.EXTN);
          }
          extnEle.setAttribute(XMLConstants.PLANNER_CODE, plannerCode);
        }
      }
    }
    return yfcInDoc;
  }


  public boolean validateUser(YFSEnvironment env, String strEnteredBy) {

    boolean isValidUser = false;
    String input = "<User Loginid=\"" + strEnteredBy + "\" />";
    String template = "<UserList> <User Loginid=\"\"/></UserList>";

    YFCDocument inputDoc = YFCDocument.getDocumentFor(input);
    YFCDocument templateDoc = YFCDocument.getDocumentFor(template);

    try {
      Document output =
          callApi(env, inputDoc.getDocument(), templateDoc.getDocument(),
              Constants.GET_USER_LIST_API, Constants.TRUE);

      YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
      YFCElement outputEle = outputDoc.getDocumentElement();

      YFCNodeList<YFCElement> list = outputEle.getElementsByTagName(XMLConstants.USER);
      return (list.getLength() == 0);
    } catch (Exception e) {
      if (log.isVerboseEnabled()) {
        log.verbose(e);
      }
    }

    return isValidUser;
  }

  private String getPlannerCode(YFSEnvironment env, YFCElement orderLineEle, String orgCode) {

    String strPlannerCode = "";
    if (!XmlUtils.isVoid(orderLineEle)) {
      YFCElement itemEle = orderLineEle.getChildElement(XMLConstants.ITEM);
      YFCElement tranQty = orderLineEle.getChildElement(XMLConstants.ORDER_LINE_TRAN_QUANTITY);
      String strItemId = "";
      String strUOM = "";
      if (!XmlUtils.isVoid(itemEle))
        strItemId = itemEle.getAttribute(XMLConstants.ITEM_ID);

      if (!XmlUtils.isVoid(tranQty))
        strUOM = tranQty.getAttribute(XMLConstants.TRANSACTIONAL_UOM);

      if (!XmlUtils.isVoid(strItemId) && !XmlUtils.isVoid(strUOM)) {
        YFCDocument input =
            YFCDocument.getDocumentFor("<Item UnitOfMeasure=\"" + strUOM + "\" ItemID=\""
                + strItemId + "\" OrganizationCode=\"" + orgCode + "\" />");
        YFCDocument template =
            YFCDocument.getDocumentFor("<Item ItemID=\"\"><Extn PlannerCode=\"\" /></Item>");

        try {
          Document output =
              callApi(env, input.getDocument(), template.getDocument(),
                  Constants.GET_ITEM_DETAILS_API, true);

          YFCDocument yfcDoc = YFCDocument.getDocumentFor(output);
          YFCElement yfcDocEle = yfcDoc.getDocumentElement();

          YFCElement extnEle = yfcDocEle.getChildElement(XMLConstants.EXTN);
          strPlannerCode = extnEle.getAttribute(XMLConstants.PLANNER_CODE);

        } catch (Exception e) {
          if (log.isVerboseEnabled()) {
            log.verbose(e);
          }
        }
      }
    }
    return strPlannerCode;

  }

  public YFCDocument setDueDate(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();

    String strEnterpriseCode = yfcInDocEle.getAttribute(XMLConstants.ENTERPRISE_CODE);
    String strOrderDate = yfcInDocEle.getAttribute(XMLConstants.ORDER_DATE);
    YFCElement orderlinesEle = yfcInDocEle.getChildElement(XMLConstants.ORDER_LINES);

    if (!XmlUtils.isVoid(orderlinesEle)) {
      YFCNodeList<YFCElement> orderLineList =
          orderlinesEle.getElementsByTagName(XMLConstants.ORDER_LINE);
      for (int i = 0; i < orderLineList.getLength(); i++) {
        String dueDate = "";
        YFCElement orderline = orderLineList.item(i);
        YFCElement itemEle = orderline.getChildElement(XMLConstants.ITEM);
        if (XMLConstants.CREATE.equalsIgnoreCase(orderline.getAttribute(XMLConstants.ACTION))
            && !XmlUtils.isVoid(itemEle)
            && !XmlUtils.isVoid(itemEle.getAttribute(XMLConstants.ITEM_ID))) {

          if (!XmlUtils.isVoid(strOrderDate)) {
            dueDate = calculateDueDate(env, orderline, strEnterpriseCode, strOrderDate);
          }

          orderline.setAttribute(XMLConstants.REQUEST_SHIP_DATE, dueDate);
        }
      }
    }
    return yfcInDoc;
  }

  private String calculateDueDate(YFSEnvironment env, YFCElement orderLine,
      String strEnterpriseCode, String strOrderDate) {

    String strServiceDays = getServiceDays(env, orderLine, strEnterpriseCode);
    try {
      SimpleDateFormat dateTimeFormat = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
      SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
      SimpleDateFormat timeFormat = new SimpleDateFormat(Constants.TIME_FORMAT);

      String strDate = strOrderDate.substring(0, strOrderDate.indexOf('T'));
      String strTime = strOrderDate.substring(strOrderDate.indexOf('T') + 1, strOrderDate.length());

      Calendar calendar = Calendar.getInstance();
      calendar.setTime(dateTimeFormat.parse(strDate + " " + strTime));

      calendar.add(Calendar.DATE, Integer.parseInt(strServiceDays));

      return dateFormat.format(calendar.getTime()) + "T" + timeFormat.format(calendar.getTime());
    } catch (ParseException e) {
      if (log.isVerboseEnabled()) {
        log.verbose(e);
      }
    }

    return "";

  }

  private String getServiceDays(YFSEnvironment env, YFCElement orderLineEle, String orgCode) {

    String strServiceDays = "";
    if (!XmlUtils.isVoid(orderLineEle)) {
      YFCElement itemEle = orderLineEle.getChildElement(XMLConstants.ITEM);
      YFCElement tranQty = orderLineEle.getChildElement(XMLConstants.ORDER_LINE_TRAN_QUANTITY);
      String strItemId = "";
      String strUOM = "";
      if (!XmlUtils.isVoid(itemEle))
        strItemId = itemEle.getAttribute(XMLConstants.ITEM_ID);

      if (!XmlUtils.isVoid(tranQty))
        strUOM = tranQty.getAttribute(XMLConstants.TRANSACTIONAL_UOM);

      if (!XmlUtils.isVoid(strItemId) && !XmlUtils.isVoid(strUOM)) {
        YFCDocument input =
            YFCDocument.getDocumentFor("<Item UnitOfMeasure=\"" + strUOM + "\" ItemID=\""
                + strItemId + "\" OrganizationCode=\"" + orgCode + "\" />");
        YFCDocument template =
            YFCDocument
                .getDocumentFor("<Item ItemID=\"\"><Extn PlannerCode=\"\" ServiceDays=\"\" /></Item>");

        try {
          Document output =
              callApi(env, input.getDocument(), template.getDocument(),
                  Constants.GET_ITEM_DETAILS_API, Constants.TRUE);

          YFCDocument yfcDoc = YFCDocument.getDocumentFor(output);
          YFCElement yfcDocEle = yfcDoc.getDocumentElement();

          YFCElement extnEle = yfcDocEle.getChildElement(XMLConstants.EXTN);
          strServiceDays = extnEle.getAttribute(XMLConstants.SERVICE_DAYS);

        } catch (Exception e) {
          if (log.isVerboseEnabled()) {
            log.verbose(e);
          }
        }
      }
    }

    if (!XmlUtils.isVoid(strServiceDays)) {
      strServiceDays = "0";
    }
    return strServiceDays;

  }

  public YFCDocument stampControlNo(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();
    String strOrderNo = yfcInDocEle.getAttribute("OrderNo");
    String strApiName = yfcInDocEle.getAttribute(XMLConstants.API_NAME);

    YFCElement orderLinesEle = yfcInDocEle.getChildElement("OrderLines");
    if (!XmlUtils.isVoid(orderLinesEle)) {

      int nextPrimeLineNo = nextPrimeLineNo(env, yfcInDocEle);

      YFCIterable<YFCElement> orderLineList = orderLinesEle.getChildren();
      for (YFCElement orderLine : orderLineList) {
        if ((XMLConstants.CREATE).equalsIgnoreCase(orderLine.getAttribute(XMLConstants.ACTION))
            || (XMLConstants.COPY_ORDER).equalsIgnoreCase(strApiName)) {
          YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
          if (XmlUtils.isVoid(extnEle)) {
            extnEle = orderLine.createChild(XMLConstants.EXTN);
          } else if (XmlUtils.isVoid(extnEle.getAttribute(XMLConstants.CONTROL_NO))) {
            extnEle.setAttribute(XMLConstants.CONTROL_NO, strOrderNo + nextPrimeLineNo);
          }
        }
      }

    }

    return yfcInDoc;

  }

  private int nextPrimeLineNo(YFSEnvironment env, YFCElement orderEle) {

    int maxPrimeLineNo = 1;
    try {
      String strEnterpriseCode = orderEle.getAttribute("EnterpriseCode");
      String strDocumentType = orderEle.getAttribute("DocumentType");
      String strOrderNo = orderEle.getAttribute("OrderNo");

      String strInputXML =
          "<OrderLine><Order OrderNo=\"" + strOrderNo + "\" DocumentType=\"" + strDocumentType
              + "\"" + " EnterpriseCode=\"" + strEnterpriseCode
              + "\" /><OrderBy><Attribute Desc=\"Y\" Name=\"PrimeLineNo\"/>"
              + "</OrderBy></OrderLine >";
      String strTemplate =
          "<OrderLineList TotalLineList=\"\"><OrderLine PrimeLineNo=\"\" /></OrderLineList>";

      YFCDocument inputDoc = YFCDocument.getDocumentFor(strInputXML);
      YFCDocument templateDoc = YFCDocument.getDocumentFor(strTemplate);

      Document output =
          callApi(env, inputDoc.getDocument(), templateDoc.getDocument(),
              Constants.GET_ORDER_LINE_LIST, Constants.TRUE);

      if (!XmlUtils.isVoid(output)) {

        YFCDocument yfcOutput = YFCDocument.getDocumentFor(output);
        YFCElement outputEle = yfcOutput.getDocumentElement();

        if (!XmlUtils.isVoid(outputEle)) {
          YFCElement orderLineEle = outputEle.getFirstChildElement();
          if (!XmlUtils.isVoid(orderLineEle)) {
            maxPrimeLineNo += orderLineEle.getIntAttribute(XMLConstants.PRIME_LINE_NO);
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return maxPrimeLineNo;
  }


  public Document callApi(YFSEnvironment env, Document input, Document template, String strApiName,
      boolean isAPI) throws Exception {

    YIFApi api = YIFClientFactory.getInstance().getApi();

    if (template != null) {
      env.setApiTemplate(strApiName, template);
    }
    log.debug("Api Name:" + strApiName);
    log.debug("Is It Api? " + isAPI + " (true: API ; False : Service)");
    Document output = null;
    if (isAPI) {
      output = api.invoke(env, strApiName, input);
    } else {
      output = api.executeFlow(env, strApiName, input);
    }
    env.clearApiTemplate(strApiName);
    return output;
  }

  public String getProperty(YFSEnvironment env, String property) {

    YFCDocument inDoc =
        YFCDocument.getDocumentFor("<GetProperty PropertyName=\"" + property + "\" />");
    YFCDocument template = YFCDocument.getDocumentFor("<GetProperty PropertyValue=\"\" />");

    try {
      Document output =
          callApi(env, inDoc.getDocument(), template.getDocument(), Constants.GET_PROPERTY_API,
              Constants.TRUE);

      if (!XmlUtils.isVoid(output)) {

        YFCDocument outptDoc = YFCDocument.getDocumentFor(output);
        YFCElement outputEle = outptDoc.getDocumentElement();

        if (!XmlUtils.isVoid(outputEle)) {
          return outputEle.getAttribute(XMLConstants.PROPERTY_VALUE);
        }
      }

    } catch (Exception e) {
      if (log.isVerboseEnabled()) {
        log.verbose(e);
      }
    }
    return "";
  }

  public void IsVaildSystemSerial(YFSEnvironment env, YFCDocument yfcInDoc) {

    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();

    String strshipToId = yfcInDocEle.getAttribute(XMLConstants.RECEIVING_NODE);
    YFCElement orderLines = yfcInDocEle.getChildElement(XMLConstants.ORDER_LINES);

    if (!XmlUtils.isVoid(orderLines) && !XmlUtils.isVoid(strshipToId)) {

      YFCIterable<YFCElement> orderlineList = orderLines.getChildren(XMLConstants.ORDER_LINE);

      for (YFCElement orderLine : orderlineList) {

        YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);

        if (!XmlUtils.isVoid(extnEle)
            && !XmlUtils.isVoid(extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO))) {
          String input =
              "<Input xmlns=\"" + XMLConstants.SPWP_IS_VAILD_SYSTEM_SERIAL_NAMESPACE + "\">"
                  + "<IsVaildSystemSerial SYSTEM_CUSTOMER_NO=\"" + strshipToId
                  + "\" SYSTEM_SERIAL_NO=\"" + extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO)
                  + "\"/>" + "</Input>";
          YFCDocument inputDoc = YFCDocument.getDocumentFor(input);
          Document output;
          try {
            output =
                callApi(env, inputDoc.getDocument(), null, Constants.SPWP_IS_VAILD_SYSTEM_SERIAL,
                    Constants.FALSE);
            NodeList list = output.getElementsByTagName(XMLConstants.IS_VALID_SYSTEM_SERIAL);

            if (list.getLength() == 0) {

              YFSException ex = new YFSException();
              ex.setErrorCode("TD00004");
              ex.setErrorDescription(extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO)
                  + " is not Valid serial");
              throw ex;
            }
          } catch (Exception ex) {
            if (ex instanceof YFSException) {
              throw (YFSException) ex;
            } else {
              // need to be logged
              ex.printStackTrace();
            }

          }
        }
      }
    }
  }

}

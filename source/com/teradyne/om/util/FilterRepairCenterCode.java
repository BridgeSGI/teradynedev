package com.teradyne.om.util;

import java.util.Set;
import java.util.TreeSet;

import org.w3c.dom.Document;

import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;

public class FilterRepairCenterCode {

	public Document filter(YFSEnvironment env, Document inXML) {
		YFCDocument inYFCDocXML = YFCDocument.getDocumentFor(inXML);
		YFCElement terRepairModelList = inYFCDocXML.getDocumentElement();
		YFCIterable<YFCElement> allTerRepairModel = terRepairModelList.getChildren(XMLConstants.TER_REPAIR_MODEL);
		Set<String> centerCodes = new TreeSet<String>();
		for (YFCElement terRepairModel: allTerRepairModel) {
			String centerCode = terRepairModel.getAttribute(XMLConstants.CENTER_CODE);
			centerCodes.add(centerCode);
		}
		YFCDocument outDoc = YFCDocument.createDocument(XMLConstants.TER_REPAIR_MODEL_LIST);
		YFCElement outTerRepairModelList = outDoc.getDocumentElement();
		for (String code : centerCodes) {
			YFCElement terRepairModel = outTerRepairModelList.createChild(XMLConstants.TER_REPAIR_MODEL);
			terRepairModel.setAttribute(XMLConstants.CENTER_CODE, code);
		}
		return outDoc.getDocument();
	}

}

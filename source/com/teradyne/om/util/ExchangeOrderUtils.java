package com.teradyne.om.util;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.bridge.sterling.utils.CustomLogCategory;
import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.teradyne.om.event.OnSuccessOfTransferReceipt;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;


public class ExchangeOrderUtils {
	private static CustomLogCategory log = CustomLogCategory.instance(ExchangeOrderUtils.class);
	public boolean anyNonRepairableConsumableParts(YFCDocument inDoc) throws ParserConfigurationException, TransformerException {
		// TODO Auto-generated method stub
		
		YFCNodeList<YFCElement> orderlinesNL = inDoc.getDocumentElement().getElementsByTagName(XMLConstants.ORDER_LINE);
		for(Iterator<YFCElement> iter = orderlinesNL.iterator(); iter.hasNext(); ){
			YFCElement orderLineEle = iter.next();
			String repairCodeStr = XPathUtil.getXpathAttribute(orderLineEle, "/OrderLine/ItemDetails/Extn/@RepairCode");
			if("N".equals(repairCodeStr) || "C".equals(repairCodeStr)){
				return true;
			}
		}
		return false;
	}
	
	public void changeOrderStatusForReturn(YFSEnvironment env,
			String orderHeaderKey, ArrayList<String> orderLineList) throws RemoteException, YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		YFCDocument cosInDoc = YFCDocument.createDocument(XMLConstants.ORDER_STATUS_CHANGE);
		cosInDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY,orderHeaderKey);
		cosInDoc.getDocumentElement().setAttribute(XMLConstants.TRANSACTION_ID, "EO_CREATED.0003.ex");
		
		YFCElement cosOrderLinesEle = cosInDoc.createElement(XMLConstants.ORDER_LINES);
		for(int i=0; i < orderLineList.size(); i++){
			YFCElement cosOrderLineEle = cosInDoc.createElement(XMLConstants.ORDER_LINE);
			cosOrderLineEle.setAttribute(XMLConstants.BASE_DROP_STATUS,	"1100.100");
			cosOrderLineEle.setAttribute(XMLConstants.CHANGE_FOR_ALL_AVAILABLE_QTY, Constants.YES);
			cosOrderLineEle.setAttribute(XMLConstants.ORDER_LINE_KEY, orderLineList.get(i));
			
			cosOrderLinesEle.appendChild(cosOrderLineEle);
		}
		cosInDoc.getDocumentElement().appendChild(cosOrderLinesEle);
		String templateStr = "<OrderStatusChange OrderHeaderKey=\"\" />";
		YFCDocument outDoc = SterlingUtil.callAPI(env,Constants.CHANGE_ORDER_STATUS, cosInDoc,YFCDocument.parse(templateStr));
		
	}
	
	public void changeOrderForReturn(YFSEnvironment env, String orderHeaderKey, ArrayList<String> orderLineList) throws RemoteException, YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		YFCDocument changeOrderIn = YFCDocument.createDocument(XMLConstants.ORDER);
		changeOrderIn.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, orderHeaderKey);
		YFCElement orderLinesEle = changeOrderIn.getDocumentElement().createChild(XMLConstants.ORDER_LINES);
		for(int i = 0; i< orderLineList.size(); i++){
			YFCElement orderLineEle = changeOrderIn.createElement(XMLConstants.ORDER_LINE);
			orderLineEle.setAttribute(XMLConstants.ORDER_LINE_KEY, orderLineList.get(i));
			orderLineEle.setAttribute(XMLConstants.SHIP_NODE, "");
			orderLinesEle.appendChild(orderLineEle);
		}
		String templateStr = "<Order OrderHeaderKey=\"\" ><OrderLines><OrderLine ShipNode=\"\" PrimeLineNo=\"\" /></OrderLines></Order>";
		//System.out.println("ChangeOrder IN:"+changeOrderIn.toString());
		log.verbose("changeOrderIn", changeOrderIn.getDocument());
		YFCDocument changeOrderOut = SterlingUtil.callAPI(env, Constants.CHANGE_ORDER_API, changeOrderIn, YFCDocument.parse(templateStr));
		//System.out.println("ChangeOrder Out:"+changeOrderOut.toString());
		log.verbose("changeOrderOut", changeOrderOut.getDocument());
		
	}
	
	public boolean checkIfDerivedParentisPIP(YFSEnvironment env,
			YFCDocument inDoc) throws RemoteException, YIFClientCreationException {
		// TODO Auto-generated method stub
		YFCElement rootEle = inDoc.getDocumentElement();
		YFCNodeList<YFCElement> orderLinesNL = rootEle.getElementsByTagName(XMLConstants.ORDER_LINE);
		if(orderLinesNL.getLength() > 0){
			String soOHK = orderLinesNL.item(0).getAttribute("DerivedFromOrderHeaderKey");
			if(!StringUtil.isEmpty(soOHK)){
				String orderTemplateStr = "<OrderList><Order OrderType=\"\" OrderHeaderKey=\"\" /></OrderList>";
				YFCDocument orderListIn = YFCDocument.createDocument(XMLConstants.ORDER);
				orderListIn.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, soOHK);
				YFCDocument orderListOut = SterlingUtil.callAPI(env, "getOrderList", orderListIn, orderTemplateStr);
				YFCElement orderEle = orderListOut.getDocumentElement().getElementsByTagName(XMLConstants.ORDER).item(0);
				if("PIP".equals(orderEle.getAttribute(XMLConstants.ORDER_TYPE))){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean checkIfOrderisDOA(YFSEnvironment env,
			YFCDocument inDoc) throws RemoteException, YIFClientCreationException {
		// TODO Auto-generated method stub
		YFCElement rootEle = inDoc.getDocumentElement();
		YFCNodeList<YFCElement> orderLinesNL = rootEle.getElementsByTagName(XMLConstants.ORDER_LINE);
		if(orderLinesNL.getLength() > 0){
			String soOHK = orderLinesNL.item(0).getAttribute("DerivedFromOrderHeaderKey");
			if(!StringUtil.isEmpty(soOHK)){
				String orderTemplateStr = "<OrderList><Order OrderType=\"\" OrderHeaderKey=\"\" /></OrderList>";
				YFCDocument orderListIn = YFCDocument.createDocument(XMLConstants.ORDER);
				orderListIn.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, soOHK);
				YFCDocument orderListOut = SterlingUtil.callAPI(env, "getOrderList", orderListIn, orderTemplateStr);
				YFCElement orderEle = orderListOut.getDocumentElement().getElementsByTagName(XMLConstants.ORDER).item(0);
				if("DOA".equals(orderEle.getAttribute(XMLConstants.ORDER_TYPE))){
					return true;
				}
			}
		}
		return false;
	}
	
	public YFCDocument getOrderList(YFSEnvironment env, String orderHeaderKey, String orderListTemplate) throws RemoteException, YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		YFCDocument orderListIn = YFCDocument.createDocument(XMLConstants.ORDER);
		orderListIn.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, orderHeaderKey);
		//System.out.println("orderListIn :"+orderListIn.toString());
		YFCDocument orderListOut = SterlingUtil.callAPI(env, Constants.GET_ORDER_LIST, orderListIn, YFCDocument.parse(orderListTemplate));
		//System.out.println("orderListOut :"+orderListOut.toString());
		return orderListOut;
	}

	public void changeDatesOnExchange(YFSEnvironment env, String roOHK, YFCDocument inDoc, String callFromEvent) throws RemoteException, YIFClientCreationException, SAXException, IOException, TransformerException, ParserConfigurationException {
		// TODO Auto-generated method stub
		String orderListTemplate = "<OrderList><Order OrderHeaderKey=\"\" ><ExchangeOrders><ExchangeOrder "
				+ "OrderHeaderKey=\"\" /></ExchangeOrders></Order></OrderList>";
		ExchangeOrderUtils eoUtils = new ExchangeOrderUtils();
		YFCDocument returnorderListOut = eoUtils.getOrderList(env, roOHK, orderListTemplate);
		String orderHeaderKey = XPathUtil.getXpathAttribute(returnorderListOut, "//ExchangeOrder/@OrderHeaderKey");
		
		String eoTemplate = "<OrderList><Order OrderHeaderKey=\"\" OrderDate=\"\">"
				+ "<OrderLines><OrderLine MaxLineStatus=\"\" OrderLineKey=\"\" ReqShipDate=\"\" ReqDeliveryDate=\"\" "
				+ "EarliestScheduleDate=\"\" ><Item ItemID=\"\" /><Extn ServiceType=\"\" /></OrderLine></OrderLines></Order></OrderList>";
		YFCDocument orderListOut = eoUtils.getOrderList(env, orderHeaderKey, eoTemplate);
		//System.out.println("ExchangeOrderOut:"+orderListOut.toString());
		log.verbose("ExchangeOrderOut", orderListOut.getDocument());
		callChangeOrder(env, orderListOut, inDoc, callFromEvent);
		
	}
	
	private void callChangeOrder(YFSEnvironment env, YFCDocument orderListOut, YFCDocument inDoc, String callFromEvent) throws TransformerException, ParserConfigurationException, RemoteException, YIFClientCreationException, SAXException, IOException {
		// TODO Auto-generated method stub
		YFCDocument changeOrderIn = YFCDocument.createDocument(XMLConstants.ORDER);
		YFCElement coRootEle = changeOrderIn.getDocumentElement();
		
		String orderHeaderKey = XPathUtil.getXpathAttribute(orderListOut, "//Order/@OrderHeaderKey");
		String orderDateStr = XPathUtil.getXpathAttribute(orderListOut, "//Order/@OrderDate");
		Date orderDate = DateTimeUtil.convertDate(orderDateStr);
		coRootEle.setAttribute(XMLConstants.ORDER_HEADER_KEY, orderHeaderKey);
		
		GregorianCalendar gc = new GregorianCalendar();
		String sysDateStr = DateTimeUtil.formatDate(gc.getTime(), "yyyy-MM-dd'T'HH:mm:ss");
		
		YFCElement coLinesEle = coRootEle.createChild(XMLConstants.ORDER_LINES);
		for(YFCElement orderLineEle : orderListOut.getElementsByTagName(XMLConstants.ORDER_LINES).item(0).getChildren()){
			String itemID = XPathUtil.getXpathAttribute(orderLineEle, "//Item/@ItemID");
			YFCNode inNode = null;
			if("WO".equals(callFromEvent)){
				inNode = XPathUtil.getXpathNode(inDoc, "/WorkOrder[@ItemID='"+itemID+"']");
			}else{
				inNode = XPathUtil.getXpathNode(inDoc, "//ReceiptLine[@ItemID='"+itemID+"']");
			}
			if("1100".equals(orderLineEle.getAttribute("MaxLineStatus")) && inNode != null){
				String serviceTypeStr = XPathUtil.getXpathAttribute(orderLineEle, "//Extn/@ServiceType");
				if("WO".equals(callFromEvent) || "TO".equals(callFromEvent)) {
					if("ESWP".equals(serviceTypeStr)||"SCAL".equals(serviceTypeStr)){
						YFCElement coLineEle = changeOrderIn.createElement(XMLConstants.ORDER_LINE);
						coLinesEle.appendChild(coLineEle);
						coLineEle.setAttribute(XMLConstants.ORDER_LINE_KEY, orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY));
						coLineEle.setAttribute("ReqShipDate", sysDateStr);
						coLineEle.setAttribute("ReqDeliveryDate", sysDateStr);
						coLineEle.setAttribute("EarliestScheduleDate", sysDateStr);
					}
				}else {
					if("ESWP".equals(serviceTypeStr)||"SCAL".equals(serviceTypeStr)){
						YFCElement coLineEle = changeOrderIn.createElement(XMLConstants.ORDER_LINE);
						coLinesEle.appendChild(coLineEle);
						coLineEle.setAttribute(XMLConstants.ORDER_LINE_KEY, orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY));
						String reqDelDate = orderLineEle.getAttribute("ReqDeliveryDate");
						if(!StringUtil.isEmpty(reqDelDate)){
							int serviceDays = DateTimeUtil.getDifferenceInDays(orderDate, DateTimeUtil.convertDate(reqDelDate));
							Date serviceDate = DateTimeUtil.addToDate(gc.getTime(), Calendar.DAY_OF_MONTH, serviceDays);
							String serviceDateStr = DateTimeUtil.formatDate(serviceDate, "yyyy-MM-dd'T'HH:mm:ss");
							coLineEle.setAttribute("ReqShipDate", serviceDateStr);
							coLineEle.setAttribute("ReqDeliveryDate", serviceDateStr);
							coLineEle.setAttribute("EarliestScheduleDate", serviceDateStr);
						}
					}else{
						YFCElement coLineEle = changeOrderIn.createElement(XMLConstants.ORDER_LINE);
						coLinesEle.appendChild(coLineEle);
						coLineEle.setAttribute(XMLConstants.ORDER_LINE_KEY, orderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY));
						coLineEle.setAttribute("ReqShipDate", sysDateStr);
						coLineEle.setAttribute("ReqDeliveryDate", sysDateStr);
						coLineEle.setAttribute("EarliestScheduleDate", sysDateStr);
					}
				}
			}
		}
		//System.out.println("ChangeOrderIn :"+changeOrderIn.getString());
		log.verbose("changeOrderIn",changeOrderIn.getDocument());
		if(coLinesEle.hasChildNodes()){
			SterlingUtil.callAPI(env, Constants.CHANGE_ORDER_API, changeOrderIn, YFCDocument.parse("<Order OrderNo=\"\" />"));
		}
		
	}



}

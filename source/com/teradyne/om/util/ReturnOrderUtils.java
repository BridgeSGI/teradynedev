package com.teradyne.om.util;

import java.io.IOException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;

public class ReturnOrderUtils {
	
	public static void stampBuyer(YFCDocument inYFCXML) {
		YFCElement order = inYFCXML.getDocumentElement();
		String buyer = order.getAttribute(XMLConstants.BUYER_ORGANIZATION_CODE);
		String receivingNode = order.getAttribute(XMLConstants.RECEIVING_NODE);
		if (!XmlUtils.isVoid(receivingNode)) {
			if (XmlUtils.isVoid(buyer) || !buyer.equals(receivingNode)) {
				order.setAttribute(XMLConstants.BUYER_ORGANIZATION_CODE, receivingNode);
			}
		}
	}

	public static Document stampControlNo(YFSEnvironment env, YFCDocument inDoc) throws Exception {
		YFCElement yfcInDocEle = inDoc.getDocumentElement();
		String strOrderNo = yfcInDocEle.getAttribute(XMLConstants.ORDER_NO);
		YFCElement orderLinesEle = yfcInDocEle.getChildElement(XMLConstants.ORDER_LINES);
		if (!XmlUtils.isVoid(orderLinesEle)) {
			int maxPrimeLineNo = getMaxPrimeLineNo(env, yfcInDocEle);
			YFCIterable<YFCElement> orderLineList = orderLinesEle.getChildren();
			for (YFCElement orderLine : orderLineList) {
				if ((XMLConstants.CREATE).equalsIgnoreCase(orderLine.getAttribute(XMLConstants.ACTION))) {
					YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
					if (XmlUtils.isVoid(extnEle)) {
						extnEle = orderLine.createChild(XMLConstants.EXTN);
					}
					extnEle.setAttribute(XMLConstants.CONTROL_NO, strOrderNo + maxPrimeLineNo);
				}
			}
		}
		return inDoc.getDocument();
	}
	
	public static void setAutoNoChargeCode(YFSEnvironment env, YFCDocument inYFCXML) throws SAXException, IOException, YIFClientCreationException {
		YFCElement order = inYFCXML.getDocumentElement();
		YFCElement orderLines = order.getChildElement(XMLConstants.ORDER_LINES);
		if (!XmlUtils.isVoid(orderLines)) {
			YFCIterable<YFCElement> allOrderLines = orderLines.getChildren(XMLConstants.ORDER_LINE);
			for (YFCElement orderLine : allOrderLines) {
				String derivedFromOrderHeaderKey = orderLine.getAttribute(XMLConstants.DERIVED_FROM_ORDER_HEADER_KEY);
				if (!XmlUtils.isVoid(derivedFromOrderHeaderKey)) {
					YFCDocument getOrderListInputDoc = YFCDocument.parse("<Order OrderHeaderKey=\"" + derivedFromOrderHeaderKey + "\" />");
					YFCDocument getOrderListOutputTemplateDoc = YFCDocument.parse("<OrderList><Order OrderHeaderKey=\"\" OrderNo=\"\" OrderType=\"\"/></OrderList>");
					YFCDocument getOrderListOuputDoc = SterlingUtil.callAPI(env, Constants.GET_ORDER_LIST, getOrderListInputDoc, getOrderListOutputTemplateDoc);
					YFCElement orderList = getOrderListOuputDoc.getDocumentElement();
					YFCElement derivedOrder = orderList.getChildElement(XMLConstants.ORDER);
					String orderType = derivedOrder.getAttribute(XMLConstants.ORDER_TYPE);
					if (orderType.equalsIgnoreCase("PIP")) {
						setOrderlineExtendedAttributeValue(orderLine, XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "H");
					} else {
						setAutoNoChargeCodeAccordingToOOTBFaliure(orderLine);
					}
				} else {
					setAutoNoChargeCodeAccordingToOOTBFaliure(orderLine);
				}
			}
		}
	}
	
	public static void setShipNodes(YFSEnvironment env, YFCDocument inYFCXML) throws Exception {
		YFCElement order = inYFCXML.getDocumentElement();
		YFCElement orderLines = order.getChildElement(XMLConstants.ORDER_LINES);
		if (!XmlUtils.isVoid(orderLines)) {
			String derivedFromOrderHeaderKey = getDerivedFromOrderHeaderKey(orderLines);
			if (!XmlUtils.isVoid(derivedFromOrderHeaderKey)) {
				boolean isPIPOrder = isPIPOrder(env, derivedFromOrderHeaderKey);
				YFCIterable<YFCElement> allOrderLines = orderLines.getChildren(XMLConstants.ORDER_LINE);
				boolean flag = true;
				for (YFCElement orderLine : allOrderLines) {
					if (isPIPOrder) {
						stampPIPDestinationCenterAsShipNode(env, order, orderLine, order.getAttribute(XMLConstants.ENTERPRISE_CODE));
						flag = false;
					}
				}
				if (flag) {
					checkAndStampRepairCenterAsShipNode(env, order, orderLines);
				}
			} else {
				checkAndStampRepairCenterAsShipNode(env, order, orderLines);
			}
		}
	}
	
	private static void checkAndStampRepairCenterAsShipNode(YFSEnvironment env, YFCElement order, YFCElement orderLines) throws Exception {
		String isIBDirectShipAllowed = getInboundDirectShipFlagForCustomer(env, order.getAttribute(XMLConstants.RECEIVING_NODE));
		if (isIBDirectShipAllowed.equals(Constants.Y)) {
			YFCIterable<YFCElement> allOrderLines = orderLines.getChildren(XMLConstants.ORDER_LINE);
			for (YFCElement orderLine : allOrderLines) {
				stampRepairCenterAsShipNode(env, orderLine, order.getAttribute(XMLConstants.ENTERPRISE_CODE));
			}
		}
	}

	public static String getDerivedFromOrderHeaderKey(YFCElement orderLines) {
		YFCIterable<YFCElement> allOrderLines = orderLines.getChildren(XMLConstants.ORDER_LINE);
		for (YFCElement orderLine : allOrderLines) {
			String derivedFromOrderHeaderKey = orderLine.getAttribute(XMLConstants.DERIVED_FROM_ORDER_HEADER_KEY);
			if (!XmlUtils.isVoid(derivedFromOrderHeaderKey)) {
				return derivedFromOrderHeaderKey;
			}
		}
		return null;
	}
	
	private static void stampPIPDestinationCenterAsShipNode(YFSEnvironment env, YFCElement order, YFCElement orderLine, String orgCode) throws Exception {
		String isIBDirectShipAllowed = getInboundDirectShipFlagForCustomer(env, order.getAttribute(XMLConstants.RECEIVING_NODE));
		if (isIBDirectShipAllowed.equals(Constants.Y)) {
			String[] pipDetails = determinePIPDestinationCenterUsingItem(env, orderLine, orgCode);
			if (XmlUtils.isVoid(pipDetails)) {
				pipDetails = determinePIPDestinationCenterUsingSystemType(env, orderLine);
			}
			if (!XmlUtils.isVoid(pipDetails)) {
				orderLine.setAttribute(XMLConstants.SHIP_NODE, pipDetails[0]);
				YFCElement orderLineExtn = orderLine.getChildElement(XMLConstants.EXTN);
				if (XmlUtils.isVoid(orderLineExtn)) {
					orderLineExtn = orderLine.createChild(XMLConstants.EXTN);
				}
				orderLineExtn.setAttribute(XMLConstants.SPR_NAME, pipDetails[1]);
			}
		}
	}

	private static String[] determinePIPDestinationCenterUsingSystemType(YFSEnvironment env, YFCElement orderLine) throws Exception {
		String[] pipDetails = new String[2];
		YFCElement orderLineExtn = orderLine.getChildElement(XMLConstants.EXTN);
		if (!XmlUtils.isVoid(orderLineExtn)) {
			String systemType = orderLineExtn.getAttribute(XMLConstants.SYSTEM_TYPE);
			if (!XmlUtils.isVoid(systemType)) {
				String divisionCode = fetchDivisionCodeFromSystemType(env, systemType);
				if (!XmlUtils.isVoid(divisionCode)) {
					String pipDestinationCode = fetchPIPDestinationCenter(env, divisionCode);
					if (XmlUtils.isVoid(pipDestinationCode)) {
						return null;
					}
					String pipCoordinatorName = fetchPIPCoordinatorName(env, divisionCode);
					pipDetails[0] = pipDestinationCode;
					pipDetails[1] = pipCoordinatorName;
					return pipDetails;
				}
			}
		}
		return null;
	}

	public static String fetchDivisionCodeFromSystemType(YFSEnvironment env, String systemType) throws Exception {
		if (!XmlUtils.isVoid(systemType)) {
			YFCDocument inputXMLDoc = YFCDocument.parse("<TerPIPCoordinators SystemType=\"" + systemType + "\"/>");
			System.out.println("DivCodeIn"+inputXMLDoc.toString());
			YFCDocument getPIPWorkCoordinatorsOutput = SterlingUtil.callService(env, "GetPIPWorkCoordinators", inputXMLDoc, "");
			System.out.println("DivCodeOut"+getPIPWorkCoordinatorsOutput.toString());
			YFCElement terPIPCoordinatorsList = getPIPWorkCoordinatorsOutput.getDocumentElement();
			YFCIterable<YFCElement> allTerPIPCoordinators = terPIPCoordinatorsList.getChildren("TerPIPCoordinators");
			for (YFCElement terPIPCoordinators : allTerPIPCoordinators) {
				String pipDivisionCode = terPIPCoordinators.getAttribute("PIPDivisionCode");
				if (!XmlUtils.isVoid(pipDivisionCode)) {
					return pipDivisionCode;
				}
			}
		}
		return null;
	}

	private static String[] determinePIPDestinationCenterUsingItem(YFSEnvironment env, YFCElement orderLine, String orgCode) throws Exception {
		String[] pipDetails = new String[2];
		YFCElement item = orderLine.getChildElement(XMLConstants.ITEM);
		String itemId = item.getAttribute(XMLConstants.ITEM_ID);
		String uom = item.getAttribute(XMLConstants.UNIT_OF_MEASURE);
		YFCDocument getItemListInputDoc = YFCDocument.parse("<Item ItemID=\"" + itemId + "\" OrganizationCode=\"" + orgCode + "\" UnitOfMeasure=\"" + uom + "\" />");
		YFCDocument getItemListOutputTemplate = YFCDocument.parse("<ItemList><Item ItemID=\"\"><Extn RespProdMfgDivCode=\"\"/></Item></ItemList>");
		YFCDocument getItemListOuputDoc = SterlingUtil.callAPI(env, Constants.GET_ITEM_LIST, getItemListInputDoc, getItemListOutputTemplate);
		YFCElement itemList = getItemListOuputDoc.getDocumentElement();
		YFCElement itemInOutput = itemList.getChildElement(XMLConstants.ITEM);
		YFCElement extn = itemInOutput.getChildElement(XMLConstants.EXTN);
		if (!XmlUtils.isVoid(extn)) {
			String respProdMfgDivCode = extn.getAttribute(XMLConstants.RESPONSIBLE_MANUFACTURER_DIVISION_CODE);
			if (!XmlUtils.isVoid(respProdMfgDivCode)) {
				String pipDestinationCode = fetchPIPDestinationCenter(env, respProdMfgDivCode);
				if (XmlUtils.isVoid(pipDestinationCode)) {
					return null;
				}
				String pipCoordinatorName = fetchPIPCoordinatorName(env, respProdMfgDivCode);
				pipDetails[0] = pipDestinationCode;
				pipDetails[1] = pipCoordinatorName;
			}
		}
		return pipDetails;
	}

	private static String fetchPIPCoordinatorName(YFSEnvironment env, String respProdMfgDivCode) throws Exception {
		if (!XmlUtils.isVoid(respProdMfgDivCode)) {
			YFCDocument inputXMLDoc = YFCDocument.parse("<TerPIPCoordinators PIPDivisionCode=\"" + respProdMfgDivCode + "\"/>");
			
			YFCDocument getPIPWorkCoordinatorsOutput = SterlingUtil.callService(env, "GetPIPWorkCoordinators", inputXMLDoc, "");
			YFCElement terPIPCoordinatorsList = getPIPWorkCoordinatorsOutput.getDocumentElement();
			YFCIterable<YFCElement> allTerPIPCoordinators = terPIPCoordinatorsList.getChildren("TerPIPCoordinators");
			for (YFCElement terPIPCoordinators : allTerPIPCoordinators) {
				String pipCoordinatorName = terPIPCoordinators.getAttribute("PIPCoordinatorName");
				if (!XmlUtils.isVoid(pipCoordinatorName)) {
					return pipCoordinatorName;
				}
			}
		}
		return "";
	}

	private static boolean isPIPOrder(YFSEnvironment env, String derivedFromOrderHeaderKey) throws Exception {
		YFCDocument getOrderListInputDoc = YFCDocument.parse("<Order OrderHeaderKey=\""	+ derivedFromOrderHeaderKey + "\" />");
		YFCDocument getOrderListOutputTemplateDoc = YFCDocument.parse("<OrderList><Order OrderHeaderKey=\"\" OrderNo=\"\" OrderType=\"\"/></OrderList>");
		YFCDocument getOrderListOuputDoc = SterlingUtil.callAPI(env, Constants.GET_ORDER_LIST, getOrderListInputDoc, getOrderListOutputTemplateDoc);
		YFCElement orderList = getOrderListOuputDoc.getDocumentElement();
		YFCElement derivedOrder = orderList.getChildElement(XMLConstants.ORDER);
		String orderType = derivedOrder.getAttribute(XMLConstants.ORDER_TYPE);
		return orderType.equals("PIP");
	}

	private static String getInboundDirectShipFlagForCustomer(YFSEnvironment env, String customer) throws Exception {
		YFCDocument getCustomerListInputDoc = YFCDocument.parse("<Customer CustomerID=\"" + customer + "\"/>");
		YFCDocument getCustomerListOutputTemplateDoc = YFCDocument.parse(
				"<CustomerList>" +
					"<Customer CustomerID=\"\">" +
						"<Extn IsInboundDirectShipAllowed=\"\"/>" +
					"</Customer>" +
				"</CustomerList>");
		YFCDocument getCustomerListOutputDoc = SterlingUtil.callAPI(env, Constants.GET_CUSTOMER_LIST, getCustomerListInputDoc, getCustomerListOutputTemplateDoc);
		YFCElement customerList = getCustomerListOutputDoc.getDocumentElement();
		YFCElement customerFromOutput = customerList.getChildElement(Constants.CUSTOMER);
		if (!XmlUtils.isVoid(customerFromOutput)) {
			YFCElement extn = customerFromOutput.getChildElement(XMLConstants.EXTN);
			return extn.getAttribute(XMLConstants.IS_INBOUND_DIRECT_SHIP_ALLOWED);
		}
		return Constants.N;
	}

	
	private static void stampRepairCenterAsShipNode(YFSEnvironment env, YFCElement orderLine, String orgCode) throws Exception {
	String repairCenterCode = orderLine.getAttribute(XMLConstants.SHIP_NODE);
		if (!XmlUtils.isVoid(repairCenterCode)) {
			YFCElement item = orderLine.getChildElement(XMLConstants.ITEM);
			String itemId = item.getAttribute(XMLConstants.ITEM_ID);
			YFCElement orderLineTranQty = orderLine.getChildElement(XMLConstants.ORDER_LINE_TRAN_QUANTITY);
			String uom = orderLineTranQty.getAttribute(XMLConstants.TRANSACTIONAL_UOM);
			YFCDocument getItemListInputDoc = YFCDocument.parse("<Item ItemID=\"" + itemId + "\" OrganizationCode=\"" + orgCode + "\" UnitOfMeasure=\"" + uom + "\" />");
			YFCDocument getItemListOutputTemplate = YFCDocument.parse("<ItemList><Item ItemID=\"\"><Extn RepairModel=\"\"/></Item></ItemList>");
			YFCDocument getItemListOuputDoc = SterlingUtil.callAPI(env, Constants.GET_ITEM_LIST, getItemListInputDoc, getItemListOutputTemplate);
			YFCElement itemList = getItemListOuputDoc.getDocumentElement();
			YFCElement itemInOutput = itemList.getChildElement(XMLConstants.ITEM);
			YFCElement extn = itemInOutput.getChildElement(XMLConstants.EXTN);
			if (!XmlUtils.isVoid(extn)) {
				String repairModel = extn.getAttribute(XMLConstants.REPAIR_MODEL);
				if (!XmlUtils.isVoid(repairModel)) {
					YFCDocument getRepairCenterListInputDoc = YFCDocument.parse("<TerRepairModel CenterCode=\"" + repairCenterCode + "\" RepairModelCode=\"" + repairModel +"\"/>");
					YFCDocument getRepairCenterListOutputDoc = SterlingUtil.callService(env, "GetRepairCenterList", getRepairCenterListInputDoc, "");
					YFCElement terRepairModelList = getRepairCenterListOutputDoc.getDocumentElement();
					YFCElement terRepairModel = terRepairModelList.getChildElement(XMLConstants.TER_REPAIR_MODEL);
					if (!XmlUtils.isVoid(terRepairModel)) {
						String repairCenter = terRepairModel.getAttribute(XMLConstants.REPAIR_CENTER);
						orderLine.setAttribute(XMLConstants.SHIP_NODE, repairCenter);
					}
				}
			}
		}
	}

	public static String fetchPIPDestinationCenter(YFSEnvironment env, String respProdMfgDivCode) throws SAXException, IOException, YIFClientCreationException {
		YFCDocument inputXMLDoc = YFCDocument.parse("<TerPIPWorkCenters PIPDivisionCode=\"" + respProdMfgDivCode + "\"/>");
		System.out.println("PIPWorkCenterIn"+inputXMLDoc.toString());
		YFCDocument getPIPWorkCentersOutput = SterlingUtil.callService(env, "GetPIPWorkCenters", inputXMLDoc, "");
		System.out.println("PIPWorkCenterOut"+getPIPWorkCentersOutput.toString());
		YFCElement terPIPWorkCentersList = getPIPWorkCentersOutput.getDocumentElement();
		YFCIterable<YFCElement> allTerPIPWorkCenters = terPIPWorkCentersList.getChildren("TerPIPWorkCenters");
		for (YFCElement terPIPWorkCenters : allTerPIPWorkCenters) {
			String pipDestinationCenter = terPIPWorkCenters.getAttribute("PIPWorkCenter");
			if (!XmlUtils.isVoid(pipDestinationCenter)) {
				return pipDestinationCenter;
			}
		}
		return null;
	}
	
	private static void setAutoNoChargeCodeAccordingToOOTBFaliure(YFCElement orderLine) {
		YFCElement extn = orderLine.getChildElement(XMLConstants.EXTN);
		if (!XmlUtils.isVoid(extn)) {
			String ootbFail = extn.getAttribute(XMLConstants.OUT_OF_BOX_FAILURE);
			if (XmlUtils.isVoid(ootbFail)) {
				setOrderlineExtendedAttributeValue(orderLine, XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "K");
			} else {
				if (ootbFail.equals(Constants.Y)) {
					setOrderlineExtendedAttributeValue(orderLine, XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "J");
				} else {
					setOrderlineExtendedAttributeValue(orderLine, XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "K");
				}
			}
		} else {
			setOrderlineExtendedAttributeValue(orderLine, XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "K");
		}
	}
	
	private static void setOrderlineExtendedAttributeValue(YFCElement orderLine, String attributeName,	String attributeValue) {
		YFCElement extn = orderLine.getChildElement(XMLConstants.EXTN);
		if (XmlUtils.isVoid(extn)) {
			extn = orderLine.createChild(XMLConstants.EXTN);
		}
		extn.setAttribute(attributeName, attributeValue);
	}

	private static int getMaxPrimeLineNo(YFSEnvironment env, YFCElement orderEle) throws Exception {
		int maxPrimeLineNo = 1;
		String strEnterpriseCode = orderEle.getAttribute(XMLConstants.ENTERPRISE_CODE);
		String strDocumentType = orderEle.getAttribute(XMLConstants.DOCUMENT_TYPE);
		String strOrderNo = orderEle.getAttribute(XMLConstants.ORDER_NO);
		String strInputXML = "<OrderLine>" +
								"<Order OrderNo=\""	+ strOrderNo +
									"\" DocumentType=\"" + strDocumentType
									+ "\" EnterpriseCode=\"" + strEnterpriseCode + "\" />" +
								"<OrderBy>"	+
									"<Attribute Desc=\"Y\" Name=\"PrimeLineNo\"/>" +
								"</OrderBy>" +
							 "</OrderLine >";
		String strTemplate = "<OrderLineList TotalLineList=\"\"><OrderLine PrimeLineNo=\"\" /></OrderLineList>";
		YFCDocument inputDoc = YFCDocument.getDocumentFor(strInputXML);
		YFCDocument templateDoc = YFCDocument.getDocumentFor(strTemplate);
		SalesOrderUtils salesOrderUtils = new SalesOrderUtils();
		Document output = salesOrderUtils.callApi(env, inputDoc.getDocument(), templateDoc.getDocument(), Constants.GET_ORDER_LINE_LIST, Constants.TRUE);
		if (!XmlUtils.isVoid(output)) {
			YFCDocument yfcOutput = YFCDocument.getDocumentFor(output);
			YFCElement outputEle = yfcOutput.getDocumentElement();
			if (!XmlUtils.isVoid(outputEle)) {
				YFCElement orderLineEle = outputEle.getFirstChildElement();
				if (!XmlUtils.isVoid(orderLineEle)) {
					maxPrimeLineNo += orderLineEle.getIntAttribute(XMLConstants.PRIME_LINE_NO);
				}
			}
		}
		return maxPrimeLineNo;
	}
}
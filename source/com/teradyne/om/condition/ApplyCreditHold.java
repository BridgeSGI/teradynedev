package com.teradyne.om.condition;

import java.util.Map;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.ycp.japi.YCPDynamicCondition;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class ApplyCreditHold implements YCPDynamicCondition {

  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  private SalesOrderUtils orderUtils = new SalesOrderUtils();

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String desc, Map arg2, String inXML) {
    
    if (!XmlUtils.isVoid(inXML)) {
      YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
      YFCElement inDocEle = inDoc.getDocumentElement();

      String strCustomerID = inDocEle.getAttribute(XMLConstants.RECEIVING_NODE);
      if (XmlUtils.isVoid(strCustomerID)) {
    	  strCustomerID = inDocEle.getAttribute(XMLConstants.BUYER_ORGANIZATION_CODE);
      }
      String strEnterpriseCode = inDocEle.getAttribute(XMLConstants.ENTERPRISE_CODE);

      try {
        String hold = isCustomerHaveCreditHold(env, strCustomerID, strEnterpriseCode);
        return (hold.equalsIgnoreCase(Constants.Y));
      } catch (Exception e) {
        if(log.isVerboseEnabled()){
          log.verbose(e);
        }
        throw new YFCException(e);
      }
    }

    return false;
  }

  private String isCustomerHaveCreditHold(YFSEnvironment env, String strCustomerId,
      String strOrgCode) throws Exception {

    String strHold = Constants.N;
    YFCDocument inputDoc =
        YFCDocument.getDocumentFor("<Customer CustomerID=\"" + strCustomerId
            + "\" OrganizationCode=\"" + strOrgCode + "\"></Customer>");
    YFCDocument template =
        YFCDocument
            .getDocumentFor("<Customer CustomerID=\"\" CustomerKey=\"\"><Extn IsCreditHold=\"\" IsServiceHold=\"\"/></Customer>");

    Document output =
        orderUtils.callApi(env, inputDoc.getDocument(), template.getDocument(),
            Constants.API_GET_CUSTOMER_DETAILS,Constants.TRUE);

    YFCDocument document = YFCDocument.getDocumentFor(output);
    YFCElement yfcoutputEle = document.getDocumentElement();

    if (!XmlUtils.isVoid(yfcoutputEle)) {

      YFCElement element = yfcoutputEle.getChildElement(XMLConstants.EXTN);
      if (!XmlUtils.isVoid(element)) {
        strHold = element.getAttribute(XMLConstants.IS_CREDIT_HOLD);
      }
    }

    return strHold;
  }

}

package com.teradyne.om.condition;

import java.util.Map;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.XMLConstants;
import com.yantra.ycp.japi.YCPDynamicCondition;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class OpenBlanketPOBalanceHold implements YCPDynamicCondition{

  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
    
  @Override
  public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, String inXML) {
    
    boolean flag = false;
    YFCDocument document = YFCDocument.getDocumentFor(inXML);
    YFCElement element = document.getDocumentElement();
    
    YFCElement orderLines = element.getChildElement(XMLConstants.ORDER_LINES);
    
    if(!XmlUtils.isVoid(orderLines)){
      YFCIterable<YFCElement> orderLineList = orderLines.getChildren(XMLConstants.ORDER_LINE);
      for (YFCElement orderLine : orderLineList) {
        YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
        String strOpenBalance = null;
        if(!XmlUtils.isVoid(extnEle)) strOpenBalance = extnEle.getAttribute(XMLConstants.OPEN_BALANCE);
        
        if(XmlUtils.isVoid(strOpenBalance)){
          flag =true;
        }else{
          Double openBalance = Double.parseDouble(strOpenBalance);
          if(openBalance <= 0.00){
            flag  = true;
          }
        } 
        
        log.debug("Open balance for OrderLine key "+orderLine.getAttribute(XMLConstants.ORDER_LINE_KEY)+" is "+strOpenBalance);        
        
        if(flag) break;
        
      }
    }
    return flag;
  }

}

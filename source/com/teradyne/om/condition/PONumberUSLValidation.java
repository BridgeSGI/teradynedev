package com.teradyne.om.condition;

import java.util.Map;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.ycp.japi.YCPDynamicCondition;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;

public class PONumberUSLValidation implements YCPDynamicCondition {

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, String inXML) {
    
    boolean isPONoExist = false;
    
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inXML);
    YFCElement yfcInEle = yfcInDoc.getDocumentElement();
    
    String strCustomerPONo = yfcInEle.getAttribute(XMLConstants.CUSTOMER_PO_NO);
        
    YFCElement orderLinesEle = yfcInEle.getChildElement(XMLConstants.ORDER_LINES);
    
    if(!XmlUtils.isVoid(orderLinesEle)){
      
      YFCIterable<YFCElement> orderLineList = orderLinesEle.getChildren(XMLConstants.ORDER_LINE);
      for (YFCElement orderLine : orderLineList) {
        
        YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
        if(Constants.USL_SERVICE_TYPE.equalsIgnoreCase(extnEle.getAttribute(XMLConstants.SERVICE_TYPE))){
          
          if(XmlUtils.isVoid(strCustomerPONo)) 
            isPONoExist = true;
        }
      }
    }
    
    return isPONoExist;
  }

}

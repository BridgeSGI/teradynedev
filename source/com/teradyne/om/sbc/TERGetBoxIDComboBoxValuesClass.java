package com.teradyne.om.sbc;

/**
 *BSG : To display boxid-item short desc pair values in dropdown
 */

import java.rmi.RemoteException;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.sterlingcommerce.baseutil.SCXmlUtil;

/**
 * @author Sourabh
 *
 */
public class TERGetBoxIDComboBoxValuesClass {
	private static YFCLogCategory log;
	static {
		log = YFCLogCategory.instance(com.teradyne.om.sbc.TERGetBoxIDComboBoxValuesClass.class);
	}

	/**
	 * @param env
	 * @param inXML
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws TransformerException
	 * @throws XPathExpressionException
	 * @throws YIFClientCreationException
	 * @throws Exception
	 */
	public Document terGetBoxIDValuesDescListMethod(final YFSEnvironment env, final Document inXML) throws YFSException, RemoteException,TransformerException, XPathExpressionException, YIFClientCreationException,Exception{
		/*Input to TERGetSupportStatusCodeDescription i.e. Output from getItemList in Configurator:
            <ItemList  TotalItemList="">
            	<Item ItemID=""  UnitOfMeasure="" ItemGroupCode="" ItemType="">
					<AdditionalAttributeList>
						<AdditionalAttribute Name="" Value="" AttributeGroupID="" AttributeDomainID=""/>
					</AdditionalAttributeList>
               		<PrimaryInformation  ItemType="" ShortDescription=""/>
            	</Item>
            	<Item ItemID=""  UnitOfMeasure="" ItemGroupCode="" ItemType="">
					<AdditionalAttributeList>
						<AdditionalAttribute Name="" Value="" AttributeGroupID="" AttributeDomainID=""/>
					</AdditionalAttributeList>
               		<PrimaryInformation  ItemType="" ShortDescription=""/>
            	</Item>
	    	</ItemList>
		 */
		log.info("\n\n Input XML for TERGetBoxIDComboBoxValuesClass::::::  \n\n" + YFCDocument.getDocumentFor(inXML).toString());

		final YFCDocument inXmlDoc = YFCDocument.getDocumentFor(inXML);
		final YFCNodeList<YFCElement> itemNodeList = inXmlDoc.getElementsByTagName("Item");
		if(itemNodeList.getLength()>0){
			for(int i=0;i<itemNodeList.getLength();i++){
				YFCElement itemElement=(YFCElement)itemNodeList.item(i);
				String boxIDDescorItemShortDesc = itemElement.getChildElement("PrimaryInformation").getAttribute("ShortDescription");

				/*Start:Code To Handle Short Descriptions containing double quotes in them like: e.g. MODULE CONTAINER, 3"X5"X1-3/8". 
				 * Replace " by inch since JSON will not be able to decode it using Ext.decode method in JSP file.This change is just for display 
				 * and is not going to persist anywhere in database. 
				 */
				boxIDDescorItemShortDesc = boxIDDescorItemShortDesc.replaceAll("\"","inch");
				itemElement.getChildElement("PrimaryInformation").setAttribute("ShortDescription",boxIDDescorItemShortDesc);
				/*End:Code To Handle Short Descriptions containing double quotes in them like: e.g. MODULE CONTAINER, 3"X5"X1-3/8".*/

				final YFCNodeList<YFCElement> itemAttriNodeList = itemElement.getElementsByTagName("AdditionalAttribute");
				String itemAddAttrValueorBoxID=itemAttriNodeList.item(0).getAttribute("Value");

				String boxIDValueDesc="";
				if("".equals(boxIDDescorItemShortDesc)){
					boxIDValueDesc=itemAddAttrValueorBoxID;
				} else{
					boxIDValueDesc = itemAddAttrValueorBoxID.concat("-").concat(boxIDDescorItemShortDesc);	
				}	
				itemElement.setAttribute("BoxIDValuePlusItemShortDesc",boxIDValueDesc);
			}
		}
		log.info("\n\n Output XML for TERGetBoxIDComboBoxValuesClass :::: \n\n" + YFCDocument.getDocumentFor(inXML).toString());
		return inXML;
	}
}

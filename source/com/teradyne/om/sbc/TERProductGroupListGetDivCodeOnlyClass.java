package com.teradyne.om.sbc;
/**
 * BSG : To collect unique TER_DIVISION_CODE values to polulate in first drop down in SBC.
 * @author Sourabh Goyal, Bridge Solutions Group
 */
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class TERProductGroupListGetDivCodeOnlyClass {
	private static YFCLogCategory log;
	static {
		log = YFCLogCategory.instance(com.teradyne.om.sbc.TERProductGroupListGetDivCodeOnlyClass.class);
	}

	public Document terGetDivCodeMethod(final YFSEnvironment env, final Document inXML) throws YFSException, RemoteException,TransformerException, XPathExpressionException, YIFClientCreationException{
		/*Input to TERProductGroupListGetDivCodeOnlyClass:
		<TerProductGroupList>
			<TerProductGroup TerDivisionCode="VTD" TerProductGroupKey="12345678910" TerResponsibleID="21CL" TerResponsibleName="CR J921 CALIBRATION" /> 
  			<TerProductGroup TerDivisionCode="VTD" TerProductGroupKey="12345678911" TerResponsibleID="325R" TerResponsibleName="J325 - PHILIPPINES" /> 
  			<TerProductGroup TerDivisionCode="VEND" TerProductGroupKey="12345678912" TerResponsibleID="3COR" TerResponsibleName="3COM INC." /> 
  			<TerProductGroup TerDivisionCode="VEND" TerProductGroupKey="12345678913" TerResponsibleID="3MMA" TerResponsibleName="3M" /> 
  			<TerProductGroup TerDivisionCode="VEND" TerProductGroupKey="12345678914" TerResponsibleID="3MRP" TerResponsibleName="3M" /> 
 			<TerProductGroup TerDivisionCode="VTD" TerProductGroupKey="12345678915" TerResponsibleID="73CL" TerResponsibleName="CR/H2 J973 CALIBRATI" /> 
  		</TerProductGroupList>
		 */

		log.info("Input XML for TERProductGroupListGetDivCodeOnlyClass::::::  \n" + YFCDocument.getDocumentFor(inXML).toString());
		Set<String> divCodeSet = new HashSet<String>();

		final YFCDocument inXmlYfcDoc = YFCDocument.getDocumentFor(inXML);
		final YFCNodeList<YFCElement> terProductGroupNodeList = inXmlYfcDoc.getElementsByTagName("TerProductGroup");
		for (YFCElement terProductGroupElement:terProductGroupNodeList) {
			final String terDivisionCode = terProductGroupElement.getAttribute("TerDivisionCode");
			divCodeSet.add(terDivisionCode);
		}
		log.info("divCodeSet::::::  \n" + divCodeSet.toString());

		final YFCDocument tempDivCodeList = YFCDocument.createDocument("TerDivCodeList");
		
		for (String divCode : divCodeSet) {
			final YFCElement tempDivCodeListChild = tempDivCodeList.getDocumentElement().createChild("TerDivCode");
			tempDivCodeListChild.setAttribute("TerDivCode", divCode);
		}
		log.info("tempDivCodeList::::::  \n" + tempDivCodeList.toString());

		return tempDivCodeList.getDocument();	
	}
}

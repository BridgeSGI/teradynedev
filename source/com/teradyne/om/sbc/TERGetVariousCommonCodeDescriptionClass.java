package com.teradyne.om.sbc;

/**
 * BSG : In Teradyne Item Attributes screen, For an Item that already exists, when initially displaying the screen the Box-ID description, 
 * 		 Planner Code description, Support Status description,MGF Div Code and Resp ID desdription, Repair Div Code and Resp ID desdription, 
 * 		 Distribution Code description, repairCode Description should display either outside(without code value) or within dropdown(along with code value)
 * Note: Harmonized Code Desc Is used at different Screen "Manage Item Classificatios", but we can calculate it here only and reuse this class
 */

import java.rmi.RemoteException;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.sterlingcommerce.baseutil.SCXmlUtil;

/**
 * @author Sourabh
 *
 */
public class TERGetVariousCommonCodeDescriptionClass {
	private static YFCLogCategory log;
	static {
		log = YFCLogCategory.instance(com.teradyne.om.sbc.TERGetVariousCommonCodeDescriptionClass.class);
	}

	/**
	 * @param env
	 * @param inXML
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws TransformerException
	 * @throws XPathExpressionException
	 * @throws YIFClientCreationException
	 * @throws Exception
	 */
	public Document terGetDescFromCode(final YFSEnvironment env, final Document inXML) throws YFSException, RemoteException,TransformerException, XPathExpressionException, YIFClientCreationException,Exception{
		/*Input to TERGetSupportStatusCodeDescription i.e. Output from getItemDetails in Configurator:
			<Item ItemID="" OrganizationCode="">
				<ClassificationCodes HarmonizedCode=""/>
				<Extn BoxID="" DistributionCode="" PlannerCode="" RepairCode="" RespProdMfgDivCode="" RespProductMfgID="" RespProdRepairDivCode="" 
					  RespProductRepairID="" SupportStatusCode="" />
			</Item>
		 */
		log.info("\n\n Input XML for TERGetVariousCommonCodeDescriptionClass::::::  \n\n" + YFCDocument.getDocumentFor(inXML).toString());
		final YIFApi api = YIFClientFactory.getInstance().getLocalApi();
		String supportStatusCodeDesc ="";
		String plannerCodeDesc ="";
		String distributionCodeDesc ="";
		String repairCodeDesc ="";
		String harmonizedCodeDesc ="";
		String boxIDDesc="";
		String mfgRespName ="";
		String repairRespName ="";

		final YFCDocument inXmlDoc = YFCDocument.getDocumentFor(inXML);
		final YFCElement inXmlRoot = inXmlDoc.getDocumentElement();
		final String itemID = inXmlRoot.getAttribute("ItemID");
		final String orgCode = inXmlRoot.getAttribute("OrganizationCode");

		/*Start:Calculate Code Values*/
		final String supportStatusCode = inXmlRoot.getChildElement("Extn").getAttribute("SupportStatusCode");
		final String plannerCode = inXmlRoot.getChildElement("Extn").getAttribute("PlannerCode");
		final String distributionCode = inXmlRoot.getChildElement("Extn").getAttribute("DistributionCode");
		final String repairCode = inXmlRoot.getChildElement("Extn").getAttribute("RepairCode");
		final String harmonizedCode = inXmlRoot.getChildElement("ClassificationCodes").getAttribute("HarmonizedCode");
		final String boxIDCode = inXmlRoot.getChildElement("Extn").getAttribute("BoxID");
		final String mfgDivCode = inXmlRoot.getChildElement("Extn").getAttribute("RespProdMfgDivCode");
		final String mfgRespID = inXmlRoot.getChildElement("Extn").getAttribute("RespProductMfgID");
		final String repairDivCode = inXmlRoot.getChildElement("Extn").getAttribute("RespProdRepairDivCode");
		final String repairRespID = inXmlRoot.getChildElement("Extn").getAttribute("RespProductRepairID");
		/*End:Calculate Code Values*/

		/*Start:Calculate Code Descriptions if Code Values are non-blank and not null*/
		if(!("".equals(supportStatusCode) || null==supportStatusCode) ){
			supportStatusCodeDesc = getSupportStatusDescMethod(env,api,orgCode,supportStatusCode);
		}

		if(!("".equals(plannerCode) || null==plannerCode) ){
			plannerCodeDesc = getPlannerCodeDescMethod(env,api,orgCode,plannerCode);
		}

		if(!("".equals(distributionCode) || null==distributionCode) ){
			distributionCodeDesc = getDistCodeDescMethod(env,api,orgCode,distributionCode);
		}

		if(!("".equals(repairCode) || null==repairCode) ){
			repairCodeDesc = getRepairCodeDescMethod(env,api,orgCode,repairCode);
		}

		if(!("".equals(harmonizedCode) || null==harmonizedCode) ){
			harmonizedCodeDesc = getHarmonizedCodeDescMethod(env,api,orgCode,harmonizedCode);
		}

		if(!("".equals(boxIDCode) || null==boxIDCode)){
			boxIDDesc = getBoxIDDescMethod(env,api,orgCode,itemID,boxIDCode);
		}

		if(!("".equals(mfgDivCode) || null==mfgDivCode) && !("".equals(mfgRespID) || null==mfgRespID) ){
			mfgRespName = getMfgRespNameDescMethod(env,api,orgCode,mfgDivCode,mfgRespID);
		}

		if(!("".equals(repairDivCode) || null==repairDivCode) && !("".equals(repairRespID) || null==repairRespID)){
			repairRespName = getRepairRespNameDescMethod(env,api,orgCode,repairDivCode,repairRespID);
		}

		/*End:Calculate Code Descriptions if Code Values are non-blank and not null*/

		/*Start:Calculate Code Value-Descriptions pair if Code Descriptions are non-blank*/
		String supportStatusCodeValueDesc="";
		if("".equals(supportStatusCodeDesc)){
			supportStatusCodeValueDesc=supportStatusCode;
		} else{
			supportStatusCodeValueDesc = supportStatusCode.concat("-").concat(supportStatusCodeDesc);
		}		

		String plannerCodeValueDesc="";
		if("".equals(plannerCodeDesc)){
			plannerCodeValueDesc=plannerCode;
		} else{
			plannerCodeValueDesc = plannerCode.concat("-").concat(plannerCodeDesc);
		}		

		String distributionCodeValueDesc="";
		if("".equals(distributionCodeDesc)){
			distributionCodeValueDesc=distributionCode;
		} else{
			distributionCodeValueDesc = distributionCode.concat("-").concat(distributionCodeDesc);
		}		

		String repairCodeValueDesc="";
		if("".equals(repairCodeDesc)){
			repairCodeValueDesc=repairCode;
		} else{
			repairCodeValueDesc = repairCode.concat("-").concat(repairCodeDesc);
		}		

		String harmonizedCodeValueDesc="";
		if("".equals(harmonizedCodeDesc)){
			harmonizedCodeValueDesc=harmonizedCode;
		} else{
			harmonizedCodeValueDesc = harmonizedCode.concat("-").concat(harmonizedCodeDesc);
		}		

		String boxIDValueDesc="";
		if("".equals(boxIDDesc)){
			boxIDValueDesc=boxIDCode;
		} else{
			boxIDValueDesc = boxIDCode.concat("-").concat(boxIDDesc);	
		}		


		String mfgDivCodePlusRespName="";
		if("".equals(mfgRespName)){
			mfgDivCodePlusRespName=mfgRespID;
		} else{
			mfgDivCodePlusRespName = mfgRespID.concat("-").concat(mfgRespName);
		}		

		String repairDivCodePlusRespName="";
		if("".equals(repairRespName)){
			repairDivCodePlusRespName=repairRespID;
		} else{
			repairDivCodePlusRespName = repairRespID.concat("-").concat(repairRespName);
		}
		/*End:Calculate Code Value-Descriptions pair if Code Descriptions are non-blank*/


		/*Start: Create Return XML:All the Code Values,descriptions and value-description pair in an user created XML. xxxValue is an optional thing
		<CommonCodeAndTableDescXml SupportStatusCodeValue="" SupportStatusCodeDesc="" PlannerCodeValue="" PlannerCodeDesc="" etc. />
		 */
		final YFCDocument retDoc = YFCDocument.createDocument("CommonCodeAndTableDescXml");
		final YFCElement retDocRoot = retDoc.getDocumentElement();

		// Start: Mandatory Descriptions that will be used by SBC screen to display only descriptions if outside dropdowns.
		retDocRoot.setAttribute("SupportStatusCodeDesc", supportStatusCodeDesc);
		retDocRoot.setAttribute("PlannerCodeDesc", plannerCodeDesc);
		retDocRoot.setAttribute("DistributionCodeDesc", distributionCodeDesc);
		retDocRoot.setAttribute("RepairCodeDesc", repairCodeDesc);
		retDocRoot.setAttribute("HarmonizedCodeDesc", harmonizedCodeDesc);
		retDocRoot.setAttribute("BoxIDDesc", boxIDDesc);
		retDocRoot.setAttribute("MfgRespName", mfgRespName);
		retDocRoot.setAttribute("RepairRespName", repairRespName);
		// End: Mandatory Descriptions that will be used by SBC screen to display only descriptions if outside dropdowns.

		// Start: Mandatory Value-Descriptions that will be used by SBC screen to display various value-descriptions pair if inside DropDowns.
		retDocRoot.setAttribute("SupportStatusCodeValueDesc", supportStatusCodeValueDesc);
		retDocRoot.setAttribute("PlannerCodeValueDesc", plannerCodeValueDesc);
		retDocRoot.setAttribute("DistributionCodeValueDesc", distributionCodeValueDesc);
		retDocRoot.setAttribute("RepairCodeValueDesc", repairCodeValueDesc);
		retDocRoot.setAttribute("HarmonizedCodeValueDesc", harmonizedCodeValueDesc);
		retDocRoot.setAttribute("BoxIDValueDesc", boxIDValueDesc);
		retDocRoot.setAttribute("MfgDivCodePlusRespName", mfgDivCodePlusRespName);
		retDocRoot.setAttribute("RepairDivCodePlusRespName", repairDivCodePlusRespName);
		// End: These are Mandatory Value-Descriptions that will be used by SBC screen to display various value-descriptions pair inside DropDowns.

		// Start: Optional value attributes
		retDocRoot.setAttribute("SupportStatusCodeValue", supportStatusCode);
		retDocRoot.setAttribute("PlannerCodeValue", plannerCode);
		retDocRoot.setAttribute("DistributionCodeValue", distributionCode);
		retDocRoot.setAttribute("RepairCodeValue", repairCode);
		retDocRoot.setAttribute("HarmonizedCodeValue", harmonizedCode);
		retDocRoot.setAttribute("BoxIDValue", boxIDCode);
		retDocRoot.setAttribute("RespProdMfgDivCodeValue", mfgDivCode);
		retDocRoot.setAttribute("RespProductMfgIDValue", mfgRespID);
		retDocRoot.setAttribute("RespProdRepairDivCodeValue", repairDivCode);
		retDocRoot.setAttribute("RespProductRepairIDValue", repairRespID);
		// End: Optional value attributes
		/*End: Create Return XML:All the Code Values,descriptions and value-description pair in an user created XML. xxxValue is an optional thing*/

		log.info("\n\n About to return:::::\n\n" +SCXmlUtil.getString(retDoc.getDocument()));
		return retDoc.getDocument();	
	}

	/**
	 * @param env
	 * @param api
	 * @param orgCode
	 * @param supportStatusCode
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 */
	private String getSupportStatusDescMethod(YFSEnvironment env,YIFApi api,String orgCode,String supportStatusCode) 
			throws YFSException, RemoteException{

		/*Start:Input for getCommonCodeList API
			<CommonCode CallingOrganizationCode="" CodeValue="" CodeType="Ord_Service_Type_Det"/>
		 */
		String supportStatusCodeDescription="";
		final YFCDocument inDocCommonCodeList = YFCDocument.createDocument("CommonCode");
		final YFCElement tempDocRootElement = inDocCommonCodeList.getDocumentElement();
		tempDocRootElement.setAttribute("CallingOrganizationCode", orgCode);
		tempDocRootElement.setAttribute("CodeValue", supportStatusCode);
		tempDocRootElement.setAttribute("CodeType", "Ord_Service_Type_Det");
		env.setApiTemplate("getCommonCodeList", inDocCommonCodeList.getDocument());
		log.info("\n\n inDocCommonCodeList getSupportStatusDescMethod::::::  \n\n" +inDocCommonCodeList.toString());

		/*End:Input for getCommonCodeList API

		/*Start:Template for getCommonCodeList API
		<CommonCodeList>  
			<CommonCode CodeValue="" CodeShortDescription="" CodeType="" CommonCodeKey=""/>  
		</CommonCodeList>
		 */
		final YFCDocument tempDocCommonCodeList = YFCDocument.createDocument("CommonCodeList");
		final YFCElement tempDocRootElementChild = tempDocCommonCodeList.getDocumentElement().createChild("CommonCode");
		tempDocRootElementChild.setAttribute("CodeValue", "");
		tempDocRootElementChild.setAttribute("CodeShortDescription", "");
		tempDocRootElementChild.setAttribute("CodeType", "");
		tempDocRootElementChild.setAttribute("CommonCodeKey", "");
		env.setApiTemplate("getCommonCodeList", tempDocCommonCodeList.getDocument());
		log.info("\n\n tempDocCommonCodeList getSupportStatusDescMethod::::::  \n\n" +tempDocCommonCodeList.toString());

		/*End:Template for getCommonCodeList API*/

		final Document docOutXmlgetCommonCodeList = api.getCommonCodeList(env, inDocCommonCodeList.getDocument());
		log.info("\n\n Output of getCommonCodeList API getSupportStatusDescMethod::::::\n\n" +SCXmlUtil.getString(docOutXmlgetCommonCodeList));

		final YFCDocument yfcDocOutXmlgetCommonCodeList = YFCDocument.getDocumentFor(docOutXmlgetCommonCodeList);
		final YFCElement yfcDocOutXmlgetCommonCodeListRoot = yfcDocOutXmlgetCommonCodeList.getDocumentElement();
		if(yfcDocOutXmlgetCommonCodeListRoot.hasChildNodes()){
			supportStatusCodeDescription = yfcDocOutXmlgetCommonCodeListRoot.getChildElement("CommonCode").getAttribute("CodeShortDescription");
		}
		return supportStatusCodeDescription;
	}

	/**
	 * @param env
	 * @param api
	 * @param orgCode
	 * @param plannerCode
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 */
	private String getPlannerCodeDescMethod(YFSEnvironment env, YIFApi api,String orgCode, String plannerCode) throws YFSException, RemoteException {

		/*Create Input XML for Database extended API getTerPlannerCodeList (through service getTerPlannerCodeList):
		 * <TerPlannerCode TerPlannerCode="01"/>
		 */
		
		String plannerCodeName = "";
		final YFCDocument tempPlannerCodeListInputDoc = YFCDocument.createDocument("TerPlannerCode");
		tempPlannerCodeListInputDoc.getDocumentElement().setAttribute("TerPlannerCode",plannerCode);
		log.info("tempPlannerCodeListInputDoc in getPlannerCodeDescMethod::::::  \n" + tempPlannerCodeListInputDoc.toString());
		/*	  <TerPlannerCodeList>
				  <TerPlannerCode Createprogid="SterlingHttpTester" Createts="2014-10-29T00:00:00-04:00" Createuserid="admin" Lockid="1" Modifyprogid="Console" Modifyts="2014-10-29T00:00:00-04:00" Modifyuserid="admin" TerBusinessUnit="OMS2" TerEmailAddress="ankit.Kumar@gmail.com" TerPlannerCode="123426" TerPlannerCodeKey="201410291104481078578" TerPlannerCodeName="Test1CodeAnkit" TerPlannerName="TestAnkit" TerPlatform="Sterling" TerProductFamily="Testfamily" /> 
			  </TerPlannerCodeList>*/

		final Document docOutXmlgetPlannerCodeList = api.executeFlow(env, "getTerPlannerCodeList",tempPlannerCodeListInputDoc.getDocument());
		log.info("\n\n Output of getTerPlannerCodeList API getPlannerCodeDescMethod::::::\n\n" +SCXmlUtil.getString(docOutXmlgetPlannerCodeList));

		final YFCDocument yfcdocOutXmlgetPlannerCodeList = YFCDocument.getDocumentFor(docOutXmlgetPlannerCodeList);
		final YFCElement yfcdocOutXmlgetPlannerCodeListRoot = yfcdocOutXmlgetPlannerCodeList.getDocumentElement();
		if(yfcdocOutXmlgetPlannerCodeListRoot.hasChildNodes()){
			plannerCodeName = yfcdocOutXmlgetPlannerCodeListRoot.getChildElement("TerPlannerCode").getAttribute("TerPlannerName");
		}		
		return plannerCodeName;
	}

	/**
	 * @param env
	 * @param api
	 * @param orgCode
	 * @param distributionCode
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 */
	private String getDistCodeDescMethod(YFSEnvironment env, YIFApi api,String orgCode, String distributionCode) throws YFSException, RemoteException {
		/*Start:Input for getCommonCodeList API
		<CommonCode CallingOrganizationCode="" CodeValue="" CodeType="Ord_Service_Type_Det"/>
		 */
		String distCodeDescription="";
		final YFCDocument inDocCommonCodeList = YFCDocument.createDocument("CommonCode");
		final YFCElement tempDocRootElement = inDocCommonCodeList.getDocumentElement();
		tempDocRootElement.setAttribute("CallingOrganizationCode", orgCode);
		tempDocRootElement.setAttribute("CodeValue", distributionCode);
		tempDocRootElement.setAttribute("CodeType", "Distribution_Code");
		env.setApiTemplate("getCommonCodeList", inDocCommonCodeList.getDocument());
		log.info("\n\n inDocCommonCodeList getDistCodeDescMethod::::::  \n\n" +inDocCommonCodeList.toString());

		/*End:Input for getCommonCodeList API

	/*Start:Template for getCommonCodeList API
	<CommonCodeList>  
		<CommonCode CodeValue="" CodeShortDescription="" CodeType="" CommonCodeKey=""/>  
	</CommonCodeList>
		 */
		final YFCDocument tempDocCommonCodeList = YFCDocument.createDocument("CommonCodeList");
		final YFCElement tempDocRootElementChild = tempDocCommonCodeList.getDocumentElement().createChild("CommonCode");
		tempDocRootElementChild.setAttribute("CodeValue", "");
		tempDocRootElementChild.setAttribute("CodeShortDescription", "");
		tempDocRootElementChild.setAttribute("CodeType", "");
		tempDocRootElementChild.setAttribute("CommonCodeKey", "");
		env.setApiTemplate("getCommonCodeList", tempDocCommonCodeList.getDocument());
		log.info("\n\n tempDocCommonCodeList getDistCodeDescMethod::::::  \n\n" +tempDocCommonCodeList.toString());

		/*End:Template for getCommonCodeList API*/

		final Document docOutXmlgetCommonCodeList = api.getCommonCodeList(env, inDocCommonCodeList.getDocument());
		log.info("\n\n Output of getCommonCodeList API getDistCodeDescMethod::::::\n\n" +SCXmlUtil.getString(docOutXmlgetCommonCodeList));

		final YFCDocument yfcDocOutXmlgetCommonCodeList = YFCDocument.getDocumentFor(docOutXmlgetCommonCodeList);
		final YFCElement yfcDocOutXmlgetCommonCodeListRoot = yfcDocOutXmlgetCommonCodeList.getDocumentElement();
		if(yfcDocOutXmlgetCommonCodeListRoot.hasChildNodes()){
			distCodeDescription = yfcDocOutXmlgetCommonCodeListRoot.getChildElement("CommonCode").getAttribute("CodeShortDescription");
		}
		return distCodeDescription;
	}

	/**
	 * @param env
	 * @param api
	 * @param orgCode
	 * @param repairCode
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 */
	private String getRepairCodeDescMethod(YFSEnvironment env, YIFApi api,String orgCode, String repairCode) throws YFSException, RemoteException {
		/*Start:Input for getCommonCodeList API
		<CommonCode CallingOrganizationCode="" CodeValue="" CodeType="Ord_Service_Type_Det"/>
		 */
		String repairCodeDescription="";
		final YFCDocument inDocCommonCodeList = YFCDocument.createDocument("CommonCode");
		final YFCElement tempDocRootElement = inDocCommonCodeList.getDocumentElement();
		tempDocRootElement.setAttribute("CallingOrganizationCode", orgCode);
		tempDocRootElement.setAttribute("CodeValue", repairCode);
		tempDocRootElement.setAttribute("CodeType", "Repair_Code");
		env.setApiTemplate("getCommonCodeList", inDocCommonCodeList.getDocument());
		log.info("\n\n inDocCommonCodeList getRepairCodeDescMethod::::::  \n\n" +inDocCommonCodeList.toString());

		/*End:Input for getCommonCodeList API

	/*Start:Template for getCommonCodeList API
	<CommonCodeList>  
		<CommonCode CodeValue="" CodeShortDescription="" CodeType="" CommonCodeKey=""/>  
	</CommonCodeList>
		 */
		final YFCDocument tempDocCommonCodeList = YFCDocument.createDocument("CommonCodeList");
		final YFCElement tempDocRootElementChild = tempDocCommonCodeList.getDocumentElement().createChild("CommonCode");
		tempDocRootElementChild.setAttribute("CodeValue", "");
		tempDocRootElementChild.setAttribute("CodeShortDescription", "");
		tempDocRootElementChild.setAttribute("CodeType", "");
		tempDocRootElementChild.setAttribute("CommonCodeKey", "");
		env.setApiTemplate("getCommonCodeList", tempDocCommonCodeList.getDocument());
		log.info("\n\n tempDocCommonCodeList getRepairCodeDescMethod::::::  \n\n" +tempDocCommonCodeList.toString());

		/*End:Template for getCommonCodeList API*/

		final Document docOutXmlgetCommonCodeList = api.getCommonCodeList(env, inDocCommonCodeList.getDocument());
		log.info("\n\n Output of getCommonCodeList API getRepairCodeDescMethod::::::\n\n" +SCXmlUtil.getString(docOutXmlgetCommonCodeList));

		final YFCDocument yfcDocOutXmlgetCommonCodeList = YFCDocument.getDocumentFor(docOutXmlgetCommonCodeList);
		final YFCElement yfcDocOutXmlgetCommonCodeListRoot = yfcDocOutXmlgetCommonCodeList.getDocumentElement();
		if(yfcDocOutXmlgetCommonCodeListRoot.hasChildNodes()){
			repairCodeDescription = yfcDocOutXmlgetCommonCodeListRoot.getChildElement("CommonCode").getAttribute("CodeShortDescription");
		}
		return repairCodeDescription;
	}

	/**
	 * @param env
	 * @param api
	 * @param orgCode
	 * @param harmonizedCode
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 */
	private String getHarmonizedCodeDescMethod(YFSEnvironment env, YIFApi api,String orgCode, String harmonizedCode) throws YFSException, RemoteException {
		/*Start:Input for getCommonCodeList API
		<CommonCode CallingOrganizationCode="" CodeValue="" CodeType="Ord_Service_Type_Det"/>
		 */
		String harmonizedCodeDescription="";
		final YFCDocument inDocCommonCodeList = YFCDocument.createDocument("CommonCode");
		final YFCElement tempDocRootElement = inDocCommonCodeList.getDocumentElement();
		tempDocRootElement.setAttribute("CallingOrganizationCode", orgCode);
		tempDocRootElement.setAttribute("CodeValue", harmonizedCode);
		tempDocRootElement.setAttribute("CodeType", "Tariff_US");
		env.setApiTemplate("getCommonCodeList", inDocCommonCodeList.getDocument());
		log.info("\n\n inDocCommonCodeList getHarmonizedCodeDescMethod::::::  \n\n" +inDocCommonCodeList.toString());

		/*End:Input for getCommonCodeList API

		/*Start:Template for getCommonCodeList API
			<CommonCodeList>  
				<CommonCode CodeValue="" CodeShortDescription="" CodeType="" CommonCodeKey=""/>  
			</CommonCodeList>
		 */
		final YFCDocument tempDocCommonCodeList = YFCDocument.createDocument("CommonCodeList");
		final YFCElement tempDocRootElementChild = tempDocCommonCodeList.getDocumentElement().createChild("CommonCode");
		tempDocRootElementChild.setAttribute("CodeValue", "");
		tempDocRootElementChild.setAttribute("CodeShortDescription", "");
		tempDocRootElementChild.setAttribute("CodeType", "");
		tempDocRootElementChild.setAttribute("CommonCodeKey", "");
		env.setApiTemplate("getCommonCodeList", tempDocCommonCodeList.getDocument());
		log.info("\n\n tempDocCommonCodeList getHarmonizedCodeDescMethod::::::  \n\n" +tempDocCommonCodeList.toString());

		/*End:Template for getCommonCodeList API*/

		final Document docOutXmlgetCommonCodeList = api.getCommonCodeList(env, inDocCommonCodeList.getDocument());
		log.info("\n\n Output of getCommonCodeList API getHarmonizedCodeDescMethod::::::\n\n" +SCXmlUtil.getString(docOutXmlgetCommonCodeList));

		final YFCDocument yfcDocOutXmlgetCommonCodeList = YFCDocument.getDocumentFor(docOutXmlgetCommonCodeList);
		final YFCElement yfcDocOutXmlgetCommonCodeListRoot = yfcDocOutXmlgetCommonCodeList.getDocumentElement();
		if(yfcDocOutXmlgetCommonCodeListRoot.hasChildNodes()){
			harmonizedCodeDescription = yfcDocOutXmlgetCommonCodeListRoot.getChildElement("CommonCode").getAttribute("CodeShortDescription");
		}
		return harmonizedCodeDescription;
	}

	/**
	 * @param env
	 * @param api
	 * @param orgCode
	 * @param itemID
	 * @param boxIDCode
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 */
	private String getBoxIDDescMethod(YFSEnvironment env, YIFApi api,String orgCode,String itemID, String boxIDCode) throws YFSException, RemoteException {

		/*Create Input XML for getItemList:
		<Item CallingOrganizationCode="xml:CurrentContextOrg:/Organization/@OrganizationCode" GetUnpublishedItems="Y" IgnoreIsSoldSeparately="Y">
			<PrimaryInformation ItemType="BOXID"/>
			<AdditionalAttributeList>
				<AdditionalAttribute Name="BoxReference"/>
			</AdditionalAttributeList>
		</Item>		 
		 */
		String boxIDDesc="";
		final YFCDocument getItemListInputDoc = YFCDocument.createDocument("Item");
		final YFCElement getItemListRootElement = getItemListInputDoc.getDocumentElement();
		getItemListRootElement.setAttribute("CallingOrganizationCode",orgCode);
		getItemListRootElement.setAttribute("GetUnpublishedItems","Y");
		getItemListRootElement.setAttribute("IgnoreIsSoldSeparately","Y");

		final YFCElement getItemListInputDocPrimInfoEle = getItemListRootElement.createChild("PrimaryInformation");
		getItemListInputDocPrimInfoEle.setAttribute("ItemType", "BOXID");

		final YFCElement getItemListInputDocAddAttrListEle = getItemListRootElement.createChild("AdditionalAttributeList");
		getItemListInputDocAddAttrListEle.createChild("AdditionalAttribute").setAttribute("Name", "BoxReference");
		getItemListInputDocAddAttrListEle.createChild("AdditionalAttribute").setAttribute("Value", boxIDCode);

		log.info("getItemListInputDoc in getBoxIDDescMethod::::::  \n" + getItemListInputDoc.toString());

		/*Start:Template for getItemList API
		<ItemList>  
			<Item ItemID="" OrganizationCode="" UnitOfMeasure="" ItemKey=""> 
			<PrimaryInformation ShortDescription=""/> 
		</Item>
		</ItemList>
		 */
		final YFCDocument templDocItemList = YFCDocument.createDocument("ItemList");
		final YFCElement templDocItemListChild = templDocItemList.getDocumentElement().createChild("Item");
		templDocItemListChild.setAttribute("ItemID", "");
		templDocItemListChild.setAttribute("OrganizationCode", "");
		templDocItemListChild.setAttribute("UnitOfMeasure", "");
		templDocItemListChild.setAttribute("ItemKey", "");

		final YFCElement templDocItemChild = templDocItemListChild.createChild("PrimaryInformation");
		templDocItemChild.setAttribute("ShortDescription", "");

		env.setApiTemplate("getItemList", templDocItemList.getDocument());
		log.info("\n\n getItemList getBoxIDDescMethod::::::  \n\n" +templDocItemList.toString());
		/*End:Template for getItemList API*/

		final Document docOutXmlgetItemList = api.getItemList(env, getItemListInputDoc.getDocument());
		log.info("\n\n Output of getItemList API getBoxIDDescMethod::::::\n\n" +SCXmlUtil.getString(docOutXmlgetItemList));

		final YFCDocument yfcDocOutXmlgetItemList = YFCDocument.getDocumentFor(docOutXmlgetItemList);
		final YFCElement yfcDocOutXmlgetItemListRoot = yfcDocOutXmlgetItemList.getDocumentElement();
		if(yfcDocOutXmlgetItemListRoot.hasChildNodes()){
			boxIDDesc = yfcDocOutXmlgetItemListRoot.getChildElement("Item").getChildElement("PrimaryInformation").getAttribute("ShortDescription");
		}
		return boxIDDesc;		
	}

	/**
	 * @param env
	 * @param api
	 * @param orgCode
	 * @param mfgDivCode
	 * @param mfgRespID
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 */
	private String getMfgRespNameDescMethod(YFSEnvironment env, YIFApi api,String orgCode,String mfgDivCode, String mfgRespID)
			throws YFSException, RemoteException {

		/*Create Input XML for Database extended API getTerProductGroupList (through service getTerProductGroupListService):
		 * <TerProductGroup TerDivisionCode="750" TerResponsibleID="750C"/>
		 */
		String mfgRespName = "";
		final YFCDocument tempRespIDListInputDoc = YFCDocument.createDocument("TerProductGroup");
		tempRespIDListInputDoc.getDocumentElement().setAttribute("TerDivisionCode",mfgDivCode);
		tempRespIDListInputDoc.getDocumentElement().setAttribute("TerResponsibleID",mfgRespID);
		log.info("tempRespIDListInputDoc in getMfgRespNameDescMethod::::::  \n" + tempRespIDListInputDoc.toString());
		/*Template is set in Configurator and is:
			  <TerProductGroupList>
	    		<TerProductGroup TerDivisionCode="" TerProductGroupKey="" TerResponsibleID="" TerResponsibleName=""/>
			  </TerProductGroupList>*/

		final Document docOutXmlgetProductGroupList = api.executeFlow(env, "getTerProductGroupListService",tempRespIDListInputDoc.getDocument());
		log.info("\n\n Output of getCommonCodeList API getMfgRespNameDescMethod::::::\n\n" +SCXmlUtil.getString(docOutXmlgetProductGroupList));

		final YFCDocument yfcDocOutXmlgetProductGroupList = YFCDocument.getDocumentFor(docOutXmlgetProductGroupList);
		final YFCElement yfcDocOutXmlgetProductGroupListRoot = yfcDocOutXmlgetProductGroupList.getDocumentElement();
		if(yfcDocOutXmlgetProductGroupListRoot.hasChildNodes()){		
			mfgRespName = yfcDocOutXmlgetProductGroupListRoot.getChildElement("TerProductGroup").getAttribute("TerResponsibleName");
		}
		return mfgRespName;	
	}

	/**
	 * @param env
	 * @param api
	 * @param orgCode
	 * @param repairDivCode
	 * @param repairRespID
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 */
	private String getRepairRespNameDescMethod(YFSEnvironment env, YIFApi api,String orgCode,String repairDivCode, String repairRespID) throws YFSException, RemoteException {

		/*Create Input XML for Database extended API getTerProductGroupList (through service getTerProductGroupListService):
		 * <TerProductGroup TerDivisionCode="750" TerResponsibleID="750C"/>
		 */
		String repairRespName="";
		final YFCDocument tempRespIDListInputDoc = YFCDocument.createDocument("TerProductGroup");
		tempRespIDListInputDoc.getDocumentElement().setAttribute("TerDivisionCode",repairDivCode);
		tempRespIDListInputDoc.getDocumentElement().setAttribute("TerResponsibleID",repairRespID);
		log.info("tempRespIDListInputDoc in getRepairRespNameDescMethod::::::  \n" + tempRespIDListInputDoc.toString());
		/*Template is set in Configurator and is:
			  <TerProductGroupList>
	    		<TerProductGroup TerDivisionCode="" TerProductGroupKey="" TerResponsibleID="" TerResponsibleName=""/>
			  </TerProductGroupList>*/

		final Document docOutXmlgetProductGroupList = api.executeFlow(env, "getTerProductGroupListService",tempRespIDListInputDoc.getDocument());
		log.info("\n\n Output of getCommonCodeList API getRepairRespNameDescMethod::::::\n\n" +SCXmlUtil.getString(docOutXmlgetProductGroupList));

		final YFCDocument yfcDocOutXmlgetProductGroupList = YFCDocument.getDocumentFor(docOutXmlgetProductGroupList);
		final YFCElement yfcDocOutXmlgetProductGroupListRoot = yfcDocOutXmlgetProductGroupList.getDocumentElement();
		if(yfcDocOutXmlgetProductGroupListRoot.hasChildNodes()){		
			repairRespName = yfcDocOutXmlgetProductGroupListRoot.getChildElement("TerProductGroup").getAttribute("TerResponsibleName");
		}
		return repairRespName;
	}
}

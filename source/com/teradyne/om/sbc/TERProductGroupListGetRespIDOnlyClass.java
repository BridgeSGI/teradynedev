package com.teradyne.om.sbc;
/**
 * BSG : To collect unique TER_RESPONSIBLE_ID values to polulate in second drop down based on selection of DIV_CODE in first DDown in SBC.
 * @author Sourabh Goyal, Bridge Solutions Group
 */
import java.rmi.RemoteException;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class TERProductGroupListGetRespIDOnlyClass {
	private static YFCLogCategory log;
	static {
		log = YFCLogCategory.instance(com.teradyne.om.sbc.TERProductGroupListGetRespIDOnlyClass.class);
	}

	public Document terGetRespIdMethod(final YFSEnvironment env, final Document inXML) throws YFSException, RemoteException,TransformerException, XPathExpressionException, YIFClientCreationException{
		/*Input to TERProductGroupListGetRespIDOnlyClass(Cropped Input Only):
		<ItemList>
    		<Item>
        		<Extn DivCodeComboSelectedValue="MT"/>
    		</Item>
		</ItemList>
		 */
		log.info("Input XML for TERProductGroupListGetRespIDOnlyClass::::::  \n" + YFCDocument.getDocumentFor(inXML).toString());
		final YFCDocument inXmlYfcDoc = YFCDocument.getDocumentFor(inXML);
		final YFCNodeList<YFCElement> terProductGroupNodeList = inXmlYfcDoc.getElementsByTagName("Extn");
		final String terDivisionCode = terProductGroupNodeList.item(0).getAttribute("DivCodeComboSelectedValue");

		/* Output XML should be::::::<TerProductGroup TerDivisionCode="VEND"/>*/
		final YFCDocument tempRespIDListInputDoc = YFCDocument.createDocument("TerProductGroup");
		tempRespIDListInputDoc.getDocumentElement().setAttribute("TerDivisionCode",terDivisionCode);
		log.info("tempRespIDListInputDoc::::::  \n" + tempRespIDListInputDoc.toString());
		/**Actual Output:
		 <TerProductGroupList>
		    <TerProductGroup TerDivisionCode="MT"
		        TerProductGroupKey="20140521150744398032"
		        TerResponsibleID="VEGA" TerResponsibleName="VEGA"/>
		    <TerProductGroup TerDivisionCode="MT"
		        TerProductGroupKey="20140521150744398037"
		        TerResponsibleID="VGPH" TerResponsibleName="VEGA CEBU REPAIR"/>
		</TerProductGroupList>*/
		return tempRespIDListInputDoc.getDocument();	
	}
}

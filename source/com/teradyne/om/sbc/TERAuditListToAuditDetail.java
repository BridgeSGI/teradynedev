package com.teradyne.om.sbc;
/**
 * BSG : getAuditList called and formatted to display details on Audit Detail Popup.
 * @author Sourabh Goyal, Bridge Solutions Group
 */
import java.io.StringReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.sterlingcommerce.baseutil.SCXmlUtil;

public class TERAuditListToAuditDetail {
	private static YFCLogCategory log;
	final String prodGroupValuesSeparator="-";
	static {
		log = YFCLogCategory.instance(com.teradyne.om.sbc.TERAuditListToAuditDetail.class);
	}

	public Document terItemAuditList(final YFSEnvironment env, final Document inXML) throws YFSException, RemoteException,TransformerException, XPathExpressionException, YIFClientCreationException,Exception{
		/*Input to TERAuditListToAuditDetail:
	   		<Audit TableName="YFS_ITEM" Reference1="xml:CurrentContextOrg:/Organization/@OrganizationCode" Reference2="xml:ItemContext:/Item/@ItemID"/>
		 */
		log.info("\n\n Input XML for TERAuditListToAuditDetail::::::  \n\n" + YFCDocument.getDocumentFor(inXML).toString());
		final YIFApi api = YIFClientFactory.getInstance().getLocalApi();

		/*Start:Template for getAuditList API for Modifyuserid && Modifyts & Other Attributes:
		 * <AuditList>
				<Audit TableName="" TableKey="" AuditKey="" Reference2="" Reference1="" Operation="" AuditXml="" Modifyuserid="" Modifyts=""/> 
		   </AuditList>
		 */
		final YFCDocument tempDocAuditList = YFCDocument.createDocument("AuditList");
		final YFCElement tempDocRootElementChild = tempDocAuditList.getDocumentElement().createChild("Audit");
		tempDocRootElementChild.setAttribute("TableName", "");
		tempDocRootElementChild.setAttribute("TableKey", "");
		tempDocRootElementChild.setAttribute("AuditKey", "");
		tempDocRootElementChild.setAttribute("Reference2", "");
		tempDocRootElementChild.setAttribute("Reference1", "");
		tempDocRootElementChild.setAttribute("Operation", "");
		tempDocRootElementChild.setAttribute("AuditXml", "");
		tempDocRootElementChild.setAttribute("Modifyuserid", "");
		tempDocRootElementChild.setAttribute("Modifyts", "");
		env.setApiTemplate("getAuditList", tempDocAuditList.getDocument());
		/*End:Template for getAuditList API for Modifyuserid && Modifyts & Other Attributes*/

		final Document docOutXmlgetAuditList = api.getAuditList(env, inXML);
		log.info("\n\n Output of getAuditList API::::::\n\n" +SCXmlUtil.getString(docOutXmlgetAuditList));

		/*Start: XML Document to Return*/
		YFCDocument auditDetailDoc = YFCDocument.createDocument("AuditDetailListOutput");;
		/*End: XML Document to Return*/

		final YFCDocument docOutXmlgetAuditListYFC = YFCDocument.getDocumentFor(docOutXmlgetAuditList);
		final YFCNodeList<YFCElement> auditNodeList = docOutXmlgetAuditListYFC.getElementsByTagName("Audit");
		for (YFCElement auditElement:auditNodeList) {
			final String tableName = auditElement.getAttribute("TableName");
			final String tableKey = auditElement.getAttribute("TableKey");
			final String auditKey = auditElement.getAttribute("AuditKey");
			final String modifyuserid = auditElement.getAttribute("Modifyuserid");
			final String modifyts = auditElement.getAttribute("Modifyts");
			final String operation = auditElement.getAttribute("Operation");
			final String reference1 = auditElement.getAttribute("Reference1");
			final String reference2 = auditElement.getAttribute("Reference2");
			final String auditXml = auditElement.getAttribute("AuditXml");
			log.info("\n\n auditXml String::::::\n\n" +auditXml);

			final YFCElement auditDetailDocRootEle = auditDetailDoc.getDocumentElement();
			final YFCElement auditElementOutput = auditDetailDocRootEle.createChild("Audit");
			auditElementOutput.setAttribute("TableName", tableName);
			auditElementOutput.setAttribute("TableKey", tableKey);
			auditElementOutput.setAttribute("AuditKey", auditKey);
			auditElementOutput.setAttribute("Modifyuserid", modifyuserid);
			auditElementOutput.setAttribute("Modifyts", modifyts);
			auditElementOutput.setAttribute("Operation", operation);
			auditElementOutput.setAttribute("Reference1", reference1);
			auditElementOutput.setAttribute("Reference2", reference2);

			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource inputStream = new InputSource();
			inputStream.setCharacterStream(new StringReader(auditXml));

			Document auditXmlParseDoc = db.parse(inputStream);
			final YFCDocument auditXmlParseYFCDoc = YFCDocument.getDocumentFor(auditXmlParseDoc);
			log.info("\n\n AuditXml XML::::::\n\n " +SCXmlUtil.getString(auditXmlParseDoc));

			/*Start:Extract Only Attributes Element from AuditXml and convert it into New Doc/XML*/
			final YFCElement auditXmlParseAttributes = auditXmlParseYFCDoc.getElementsByTagName("Attributes").item(0);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document auditXmlParseAttributesDoc = builder.newDocument();
			YFCDocument auditXmlParseAttributesYFCDoc = YFCDocument.getDocumentFor(auditXmlParseAttributesDoc);
			auditXmlParseAttributesYFCDoc.appendChild(auditXmlParseAttributesYFCDoc.importNode(auditXmlParseAttributes, true));			
			log.info("\n\n auditXmlParseAttributesYFCDoc::::::\n\n " +SCXmlUtil.getString(auditXmlParseAttributesYFCDoc.getDocument()));
			/*End:Extract Only Attributes Element from AuditXml and convert it into New Doc/XML*/

			List<YFCElement> removalList = new ArrayList<YFCElement>();
			final YFCNodeList<YFCElement> auditXmlParseNodeList = auditXmlParseAttributesYFCDoc.getElementsByTagName("Attribute");
			for (YFCElement auditXmlParseElement:auditXmlParseNodeList) {				
				final String nameParseXml = auditXmlParseElement.getAttribute("Name");
				if(null!=nameParseXml && (nameParseXml.equals("Modifyts") || nameParseXml.equals("Createts") || nameParseXml.equals("Lockid") || nameParseXml.equals("MaxModifyTS"))){		
					removalList.add(auditXmlParseElement);
				}
			}
			log.info("\n\n Final Attribute removalList::: " +removalList);

			for (int i=0;i<removalList.size();i++){
				auditXmlParseAttributesYFCDoc.getDocumentElement().removeChild(removalList.get(i));
			}
			log.info("\n\n auditXmlParseAttributesYFCDoc After Removal::::::\n\n " +SCXmlUtil.getString(auditXmlParseAttributesYFCDoc.getDocument()));

			/**Start: Copy 'Attributes' element from AuditXmlParseAttributesYFCDoc to Output XML's 'Audit' element*/
			final YFCElement auditXmlParseAttributesEleNew = auditXmlParseAttributesYFCDoc.getDocumentElement();
			auditElementOutput.appendChild(auditDetailDoc.importNode(auditXmlParseAttributesEleNew, true));
			/**End: Copy 'Attributes' element from AuditXmlParseAttributesYFCDoc to Output XML's 'Audit' element*/
		}
		log.info("\n\n About to return:::::\n\n" +SCXmlUtil.getString(auditDetailDoc.getDocument()));
		return auditDetailDoc.getDocument();	
	}
}

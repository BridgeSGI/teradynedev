package com.teradyne.om.sbc;
/**
 * BSG : To collect TER_RESPONSIBLE_NAME corresponding to selected TER_RESPONSIBLE_ID.
 * @author Sourabh Goyal, Bridge Solutions Group
 */
import java.rmi.RemoteException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class TERProductGroupListGetRespNameOnlyClass {
	private static YFCLogCategory log;
	static {
		log = YFCLogCategory.instance(com.teradyne.om.sbc.TERProductGroupListGetRespNameOnlyClass.class);
	}

	public Document terGetRespNameMethod(final YFSEnvironment env, final Document inXML) throws YFSException, RemoteException,TransformerException, XPathExpressionException, YIFClientCreationException{
		/*Input to TERProductGroupListGetRespIDOnlyClass(Cropped Input Only):
		<ItemList>
    		<Item>
        		<Extn RespIDComboSelectedValue="SMT"/>
    		</Item>
		</ItemList>
		 */

		log.info("Input XML for TERProductGroupListGetRespNameOnlyClass::::::  \n" + YFCDocument.getDocumentFor(inXML).toString());
		final YFCDocument inXmlYfcDoc = YFCDocument.getDocumentFor(inXML);
		final YFCNodeList<YFCElement> terProductGroupNodeList = inXmlYfcDoc.getElementsByTagName("Extn");
		final String terRespID = terProductGroupNodeList.item(0).getAttribute("RespIDComboSelectedValue");

		/* Output XML should be::::::<TerProductGroup TerResponsibleName="VEGA CEBU REPAIR"/>*/
		final YFCDocument tempRespNameListInputDoc = YFCDocument.createDocument("TerProductGroup");
		tempRespNameListInputDoc.getDocumentElement().setAttribute("TerResponsibleID",terRespID);
		log.info("tempRespIDListInputDoc::::::  \n" + tempRespNameListInputDoc.toString());
		/**Actual Output should contain only one TerProductGroup element corresponding to unique TerResponsibleID passed as input:
		 <TerProductGroupList>
		    <TerProductGroup TerDivisionCode="MT"
		        TerProductGroupKey="20140521150744398032"
		        TerResponsibleID="VEGA" TerResponsibleName="VEGA"/>
		</TerProductGroupList>*/
		return tempRespNameListInputDoc.getDocument();	
	}
}

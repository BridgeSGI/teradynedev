package com.teradyne.om.sbc.servlets;

/**
 * BSG : To Register Custom JSBs and Mashup XMLs for Teradyne SBC Dev.
 * @author Sourabh Goyal, Bridge Solutions Group
 */
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.sterlingcommerce.ui.web.framework.helpers.SCUIJSLibraryHelper;
import com.sterlingcommerce.ui.web.framework.helpers.SCUIMashupHelper;

public class TeradyneJSBsMashupsRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public synchronized void init(final ServletConfig config) throws ServletException {
		SCUIJSLibraryHelper.loadJSLibraryXml("/extn/tera_item_jsb.jsb", config.getServletContext());
		SCUIMashupHelper.loadMashupXml("/extn/mashupxmls/sbc/item/extn_tera_itemattributes_mashup.xml", config.getServletContext());
		SCUIMashupHelper.loadMashupXml("/extn/mashupxmls/sbc/item/extn_tera_itemattributes_terProdGroup_mashup.xml", config.getServletContext());
		SCUIMashupHelper.loadMashupXml("/extn/mashupxmls/sbc/item/extn_tera_itemclassification_mashup.xml", config.getServletContext());
		SCUIMashupHelper.loadMashupXml("/extn/mashupxmls/sbc/item/extn_tera_itemaudit_mashup.xml", config.getServletContext());
		SCUIMashupHelper.loadMashupXml("/extn/mashupxmls/sbc/pricing/tera_getPricelistLineListForItem_mashup.xml", config.getServletContext());
	}
}

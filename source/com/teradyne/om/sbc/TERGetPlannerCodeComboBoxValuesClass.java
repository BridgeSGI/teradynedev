package com.teradyne.om.sbc;

/**
 *BSG : To display plannercodeplusplannercodename values in sbc dropdown
 */

import java.rmi.RemoteException;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import com.sterlingcommerce.baseutil.SCXmlUtil;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author Sourabh
 *
 */
public class TERGetPlannerCodeComboBoxValuesClass {
	private static YFCLogCategory log;
	static {
		log = YFCLogCategory.instance(com.teradyne.om.sbc.TERGetPlannerCodeComboBoxValuesClass.class);
	}

	/**
	 * @param env
	 * @param inXML
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws TransformerException
	 * @throws XPathExpressionException
	 * @throws YIFClientCreationException
	 * @throws Exception
	 */
	public Document terGetPlannerCodeComboValuesMethod(final YFSEnvironment env, final Document inXML) throws YFSException, RemoteException,TransformerException, XPathExpressionException, YIFClientCreationException,Exception{
		/*Input to TERGetPlannerCodeComboBoxValuesClass:
			<TerPlannerCodeList>
				  <TerPlannerCode Createprogid="SterlingHttpTester" Createts="2014-10-29T00:00:00-04:00" Createuserid="admin" Lockid="1" Modifyprogid="Console" Modifyts="2014-10-29T00:00:00-04:00" Modifyuserid="admin" TerBusinessUnit="OMS2" TerEmailAddress="ankit.Kumar@gmail.com" TerPlannerCode="123426" TerPlannerCodeKey="201410291104481078578" TerPlannerCodeName="Test1CodeAnkit" TerPlannerName="TestAnkit" TerPlatform="Sterling" TerProductFamily="Testfamily" /> 
				  <TerPlannerCode Createprogid="SterlingHttpTester" Createts="2014-10-29T00:00:00-04:00" Createuserid="admin" Lockid="0" Modifyprogid="SterlingHttpTester" Modifyts="2014-10-29T00:00:00-04:00" Modifyuserid="admin" TerBusinessUnit="TIBCO" TerEmailAddress="ankit.Kumar@gmail.com" TerPlannerCode="123416" TerPlannerCodeKey="201410291106541078581" TerPlannerCodeName="TestCodeAnkit" TerPlannerName="PlannerAnkit" TerPlatform="EBS" TerProductFamily="Testfamily" /> 
				  <TerPlannerCode Createprogid="SterlingHttpTester" Createts="2014-10-29T00:00:00-04:00" Createuserid="admin" Lockid="0" Modifyprogid="SterlingHttpTester" Modifyts="2014-10-29T00:00:00-04:00" Modifyuserid="admin" TerBusinessUnit="TIBCO" TerEmailAddress="ankit1.Kumar@gmail.com" TerPlannerCode="123411" TerPlannerCodeKey="201410291106541078582" TerPlannerCodeName="TestCodeAnkit1" TerPlannerName="PlannerAnkit1" TerPlatform="EBS" TerProductFamily="Testfamily" /> 
				  <TerPlannerCode Createprogid="SterlingHttpTester" Createts="2014-10-29T00:00:00-04:00" Createuserid="admin" Lockid="0" Modifyprogid="SterlingHttpTester" Modifyts="2014-10-29T00:00:00-04:00" Modifyuserid="admin" TerBusinessUnit="TIBCO" TerEmailAddress="ankit2.Kumar@gmail.com" TerPlannerCode="123412" TerPlannerCodeKey="201410291106541078583" TerPlannerCodeName="TestCodeAnkit2" TerPlannerName="PlannerAnkit2" TerPlatform="EBS" TerProductFamily="Testfamily" /> 
				  <TerPlannerCode Createprogid="SterlingHttpTester" Createts="2014-10-29T00:00:00-04:00" Createuserid="admin" Lockid="0" Modifyprogid="SterlingHttpTester" Modifyts="2014-10-29T00:00:00-04:00" Modifyuserid="admin" TerBusinessUnit="TIBCO" TerEmailAddress="ankit3.Kumar@gmail.com" TerPlannerCode="123413" TerPlannerCodeKey="201410291106541078584" TerPlannerCodeName="TestCodeAnkit3" TerPlannerName="PlannerAnkit3" TerPlatform="EBS" TerProductFamily="Testfamily" /> 
				  <TerPlannerCode Createprogid="SterlingHttpTester" Createts="2014-10-29T00:00:00-04:00" Createuserid="admin" Lockid="0" Modifyprogid="SterlingHttpTester" Modifyts="2014-10-29T00:00:00-04:00" Modifyuserid="admin" TerBusinessUnit="TIBCO" TerEmailAddress="ankit4.Kumar@gmail.com" TerPlannerCode="123414" TerPlannerCodeKey="201410291106541078585" TerPlannerCodeName="TestCodeAnkit4" TerPlannerName="PlannerAnkit4" TerPlatform="EBS" TerProductFamily="Testfamily" /> 
			</TerPlannerCodeList>
		 */
		log.info("\n\n Input XML for TERGetPlannerCodeComboBoxValuesClass::::::  \n\n" + YFCDocument.getDocumentFor(inXML).toString());
		final YIFApi api = YIFClientFactory.getInstance().getLocalApi();

		final Document docOutXmlgetPlannerCodeList = api.executeFlow(env, "getTerPlannerCodeList",inXML);
		log.info("\n\n Output of getTerPlannerCodeList API ::::::\n\n" +SCXmlUtil.getString(docOutXmlgetPlannerCodeList));

		final YFCDocument docOutXmlgetPlannerCodeListDoc = YFCDocument.getDocumentFor(docOutXmlgetPlannerCodeList);
		final YFCNodeList<YFCElement> plannerCodeList = docOutXmlgetPlannerCodeListDoc.getElementsByTagName("TerPlannerCode");
		if(plannerCodeList.getLength()>0){
			for(int i=0;i<plannerCodeList.getLength();i++){
				YFCElement terPlannerCodeElement=(YFCElement)plannerCodeList.item(i);
				String terPlannerCode = terPlannerCodeElement.getAttribute("TerPlannerCode");
				String terPlannerName = terPlannerCodeElement.getAttribute("TerPlannerName");

				String plannerCodePlusName="";
				if("".equals(terPlannerName)){
					plannerCodePlusName=terPlannerCode;
				} else{
					plannerCodePlusName = terPlannerCode.concat("-").concat(terPlannerName);	
				}	
				terPlannerCodeElement.setAttribute("PlannerCodePlusPlannerCodeName",plannerCodePlusName);
			}
		}
		log.info("\n\n Output XML for TERGetPlannerCodeComboBoxValuesClass :::: \n\n" + YFCDocument.getDocumentFor(inXML).toString());
		return docOutXmlgetPlannerCodeList;
	}
}

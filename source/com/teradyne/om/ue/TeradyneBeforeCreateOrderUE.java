package com.teradyne.om.ue;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.SterlingUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.api.CustomCodeHelper;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

public class TeradyneBeforeCreateOrderUE extends CustomCodeHelper implements YFSBeforeCreateOrderUE {

  private SalesOrderUtils salesOrderUtils = new SalesOrderUtils();
  private YIFApi api;
  private static YFCLogCategory _cat = YFCLogCategory.instance("com.yantra.CustomCode");

  @Override
  public Document beforeCreateOrder(YFSEnvironment arg0, Document inXML)
      throws YFSUserExitException {
    setEnv(arg0);

    YFCDocument inYFCXML = null;
    inYFCXML = YFCDocument.getDocumentFor(inXML);
    
    //if(_cat.isVerboseEnabled()){
	      System.out.println("Input for beforeCreateOrder: " + inYFCXML);
	 //}
    

    if (inYFCXML.getDocumentElement().getAttribute(XMLConstants.DOCUMENT_TYPE).equalsIgnoreCase(Constants.DOCUMENT_TYPE_SO) && !("EXCHANGE".equalsIgnoreCase(inYFCXML.getDocumentElement().getAttribute(XMLConstants.ORDER_PURPOSE)))
        || inYFCXML.getDocumentElement().getAttribute(XMLConstants.DOCUMENT_TYPE).equalsIgnoreCase(Constants.DOCUMENT_TYPE_TO)) {
    	inYFCXML.getDocumentElement().setAttribute("DefaultCustomerInformation", "Y");
      
      setBilltoAddress(arg0,inYFCXML);
      setEnteredBy(arg0, inYFCXML);
      try {
    	  salesOrderUtils.setBuyer(arg0, inYFCXML);
		setCostCenter(arg0, inYFCXML);
	} catch (Exception e) {
		 if(_cat.isVerboseEnabled()){
		      _cat.verbose(e);
		 }
		 throw new YFCException(e);
	}
            
      salesOrderUtils.IsVaildSystemSerial(arg0, inYFCXML);
      //salesOrderUtils.checkOPPOSupport(arg0, inYFCXML);
      //salesOrderUtils.isRPDEligible(arg0, inYFCXML);
      //salesOrderUtils.canOrderConsummable(arg0, inYFCXML);
      inYFCXML = salesOrderUtils.updateOpenBlanketPOBalanceinLine(arg0, inYFCXML);
      inYFCXML = salesOrderUtils.stampControlNo(arg0, inYFCXML);
      inYFCXML = salesOrderUtils.setPlannerCode(arg0, inYFCXML);
      inYFCXML = salesOrderUtils.setDueDate(arg0, inYFCXML);
      inYFCXML = salesOrderUtils.setTeradyneBusGrpAndSystemType(arg0, inYFCXML);
      inYFCXML = salesOrderUtils.updateQtyOrderedToday(arg0, inYFCXML);
      inYFCXML = salesOrderUtils.setAutoNoChargeCode(arg0, inYFCXML);
    }
    return inYFCXML.getDocument();
  }

  private void setEnteredBy(YFSEnvironment arg0, YFCDocument inYFCXML){
    
    YFCElement inYFCXMLEle = inYFCXML.getDocumentElement();
    String strEnteredBy = inYFCXMLEle.getAttribute(XMLConstants.ENTERED_BY);
    
    if(XmlUtils.isVoid(strEnteredBy)){
      strEnteredBy = arg0.getUserId();
      inYFCXMLEle.setAttribute(XMLConstants.ENTERED_BY, strEnteredBy);
    }
  }
  
  // For creating Sales Order
  private void setCostCenter(YFSEnvironment env, YFCDocument inDoc) throws Exception {/*
    YFCElement inDocEle = inDoc.getDocumentElement();
    YFCDocument input =
        YFCDocument.getDocumentFor("<Input xmlns=\"" + XMLConstants.SPWP_GET_COST_CENTER_NAMESPACE
            + "\"><GetCostCenter EMPLOYEE_ID=\"" + inDocEle.getAttribute(XMLConstants.ENTERED_BY)
            + "\"/></Input>");

    Document output;
    try {
      output = salesOrderUtils.callApi(env, input.getDocument(), null, Constants.SPWP_GET_COST_CENTER_SERVICE, Constants.FALSE);
    } catch (Exception e) {
      if(_cat.isVerboseEnabled()){
        _cat.verbose(e);
      }
      throw new YFCException(e);
    }

    YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
    YFCElement element = outputDoc.getDocumentElement();
    YFCElement costEle = element.getFirstChildElement();

    if(!XmlUtils.isVoid(costEle)){
      YFCElement extnEle = inDocEle.createChild(XMLConstants.EXTN);
      extnEle.setAttribute(XMLConstants.COST_CENTER, costEle.getAttribute(XMLConstants.BO_COST_CENTER));
    }
    
  */
	  YFCElement inDocEle = inDoc.getDocumentElement();
	  String enteredBy = inDocEle.getAttribute(XMLConstants.ENTERED_BY);
	  YFCDocument inputDoc = YFCDocument.parse("<User Loginid=\"" + enteredBy + "\"/>");
		YFCDocument templateDoc = YFCDocument.parse("<User Username=\"\"><ContactPersonInfo AddressLine6=\"\"/></User>");
		YFCDocument outputDoc = SterlingUtil.callAPI(env, Constants.GET_USER_LIST_API, inputDoc, templateDoc);
		YFCElement userList = outputDoc.getDocumentElement();
		YFCElement user = userList.getChildElement(XMLConstants.USER);
		if (!XmlUtils.isVoid(user)) {
			YFCElement contactPersonInfo = user.getChildElement(XMLConstants.CONTACT_PERSON_INFO);
			if (!XmlUtils.isVoid(contactPersonInfo)) {
				String costCenter = contactPersonInfo.getAttribute(XMLConstants.ADDRESS_LINE6);
				if (!XmlUtils.isVoid(costCenter)) {
					YFCElement extn = inDocEle.getChildElement(XMLConstants.EXTN);
					extn.setAttribute(XMLConstants.COST_CENTER, costCenter);
				}
			}
		}
	  }

  private void setBilltoAddress(YFSEnvironment env, YFCDocument inDoc) {

    YFCElement inDocEle = inDoc.getDocumentElement();
    try {
      if (inDocEle.hasAttribute(XMLConstants.RECEIVING_NODE)) {

        String receivingNode = inDocEle.getAttribute(XMLConstants.RECEIVING_NODE);
        Document teradyneCustsiteBillList = getCustomerSiteBillList(env, receivingNode);

        setBillToValues(inDocEle, teradyneCustsiteBillList);
      }

    } catch (Exception e) {
      if(_cat.isVerboseEnabled()){
        _cat.verbose("setBillToAddress:" + e);
      }
    }

  }

  private Document getCustomerSiteBillList(YFSEnvironment env, String recvNode) throws Exception {

    api = YIFClientFactory.getInstance().getApi();
    YFCDocument inputdoc =
        YFCDocument.getDocumentFor("<TerCustBillingOrg TerOrganizationCodeKey=\"" + recvNode
            + "\" TerDefaultFLag=\"Y\" />");
    return api.executeFlow(env, Constants.GET_TER_CUST_BILLING_ORG_LIST_API, inputdoc.getDocument());
  }

  private void setBillToValues(YFCElement inDocEle, Document billListDoc) {

    YFCDocument yfcBillList = YFCDocument.getDocumentFor(billListDoc);
    YFCElement element = yfcBillList.getDocumentElement();

    YFCElement terBillToEle = element.getFirstChildElement();
    if (!XmlUtils.isVoid(terBillToEle)) {
      YFCElement personBillTo = inDocEle.createChild(XMLConstants.PERSON_INFO_BILL_TO);

      personBillTo.setAttribute(XMLConstants.ADDRESS_ID, terBillToEle.getAttribute(XMLConstants.TER_BILLING_ID));
      personBillTo.setAttribute(XMLConstants.ADDRESS_LINE1, terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE1));
      personBillTo.setAttribute(XMLConstants.ADDRESS_LINE2, terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE2));
      personBillTo.setAttribute(XMLConstants.ADDRESS_LINE3, terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE3));
      personBillTo.setAttribute(XMLConstants.ADDRESS_LINE4, terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE4));
      personBillTo.setAttribute(XMLConstants.ADDRESS_LINE5, terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE5));
      personBillTo.setAttribute(XMLConstants.ADDRESS_LINE6, terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE6));
      personBillTo.setAttribute(XMLConstants.ALTERNATE_EMAIL_ID,
          terBillToEle.getAttribute(XMLConstants.TER_ALTERNATE_EMAIL_ID));
      personBillTo.setAttribute(XMLConstants.BEEPER, terBillToEle.getAttribute(XMLConstants.TER_BEEPER));
      personBillTo.setAttribute(XMLConstants.CITY, terBillToEle.getAttribute(XMLConstants.TER_CITY));
      personBillTo.setAttribute(XMLConstants.STATE, terBillToEle.getAttribute(XMLConstants.TER_STATE));
      personBillTo.setAttribute(XMLConstants.COUNTRY, terBillToEle.getAttribute(XMLConstants.TER_COUNTRY));
      personBillTo.setAttribute(XMLConstants.DAY_FAX_NO, terBillToEle.getAttribute(XMLConstants.TER_DAY_FAX_NO));
      personBillTo.setAttribute(XMLConstants.DAY_PHONE, terBillToEle.getAttribute(XMLConstants.TER_DAY_PHONE));
      personBillTo.setAttribute(XMLConstants.EMAIL_ID, terBillToEle.getAttribute(XMLConstants.TER_EMAIL_ID));
      personBillTo.setAttribute(XMLConstants.EVENING_FAX_NO, terBillToEle.getAttribute(XMLConstants.TER_EVENING_FAX_NO));
      personBillTo.setAttribute(XMLConstants.EVENING_PHONE, terBillToEle.getAttribute(XMLConstants.TER_EVENING_PHONE));
      personBillTo.setAttribute(XMLConstants.MOBILE_PHONE, terBillToEle.getAttribute(XMLConstants.TER_MOBILE_PHONE));
      personBillTo.setAttribute(XMLConstants.OTHER_PHONE, terBillToEle.getAttribute(XMLConstants.TER_OTHER_PHONE));
      personBillTo.setAttribute(XMLConstants.ZIP_CODE, terBillToEle.getAttribute(XMLConstants.TER_ZIP_CODE));
    } else {
      _cat.verbose("Bill to is not configured.");
      // TODO we need to throw error. or just logging is enough
    }
  }


  @Override
  public String beforeCreateOrder(YFSEnvironment arg0, String arg1) throws YFSUserExitException {
    // TODO Auto-generated method stub
    return null;
  }

}

package com.teradyne.om.ue;

import org.w3c.dom.Document;
import com.teradyne.om.api.CustomCodeHelper;
import com.teradyne.om.util.SalesOrderUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeChangeOrderUE;

public class TeradyneBeforeChangeSalesOrderUE extends CustomCodeHelper implements
    YFSBeforeChangeOrderUE {
  SalesOrderUtils salesOrderUtils = new SalesOrderUtils();

  @Override
  public Document beforeChangeOrder(YFSEnvironment env, Document inDoc) throws YFSUserExitException {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    try {
		salesOrderUtils.setBuyer(env, yfcInDoc);
	} catch (Exception e) {
		 throw new YFCException(e);
	}
    salesOrderUtils.IsVaildSystemSerial(env, yfcInDoc);
    //salesOrderUtils.checkOPPOSupport(env, yfcInDoc);
    //salesOrderUtils.isRPDEligible(env, yfcInDoc);
    //salesOrderUtils.canOrderConsummable(env, yfcInDoc);
    yfcInDoc = salesOrderUtils.updateOpenBlanketPOBalanceinLine(env, yfcInDoc);
    yfcInDoc = salesOrderUtils.stampControlNo(env, yfcInDoc);
    yfcInDoc = salesOrderUtils.setPlannerCode(env, yfcInDoc);
    yfcInDoc = salesOrderUtils.setDueDate(env, yfcInDoc);
    yfcInDoc = salesOrderUtils.setTeradyneBusGrpAndSystemType(env, yfcInDoc);
    yfcInDoc = salesOrderUtils.updateQtyOrderedToday(env, yfcInDoc);
    yfcInDoc = salesOrderUtils.setAutoNoChargeCode(env, yfcInDoc);
    
    return yfcInDoc.getDocument();
  }

  /*
   * private boolean isRPDEligible(YFSEnvironment env, Document inDoc){
   * 
   * YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc); YFCElement yfcInEle =
   * yfcInDoc.getDocumentElement();
   * 
   * YFCElement orderLinesEle = yfcInEle.getChildElement("OrderLines");
   * if(!XmlUtils.isVoid(orderLinesEle)){
   * 
   * YFCNodeList<YFCElement> list = orderLinesEle.getElementsByTagName("OrderLine");
   * 
   * for (int i = 0; i < list.getLength(); i++) { YFCElement element = list.item(i);
   * 
   * //TODO code pending... What to do.. thr } } return true; }
   */
}


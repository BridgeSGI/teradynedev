package com.teradyne.om.ue;

import java.io.IOException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.api.CustomCodeHelper;
import com.teradyne.om.util.ReturnOrderUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

public class TeradyneBeforeCreateOrderUEForRO extends CustomCodeHelper
		implements YFSBeforeCreateOrderUE {

	private YIFApi					api;

	private static YFCLogCategory	_cat	= YFCLogCategory
													.instance("com.yantra.CustomCode");

	@Override
	public Document beforeCreateOrder(YFSEnvironment env, Document inXML) throws YFSUserExitException {
		setEnv(env);
		YFCDocument inYFCXML = null;
		inYFCXML = YFCDocument.getDocumentFor(inXML);
		SalesOrderUtils salesOrderUtils = new SalesOrderUtils();

		try {
			ReturnOrderUtils.stampBuyer(inYFCXML);
			ReturnOrderUtils.stampControlNo(env, inYFCXML);
			ReturnOrderUtils.setAutoNoChargeCode(env, inYFCXML);
//			ReturnOrderUtils.getDerivedFromOrderHeaderKey(orderLines)
//			stampDOCForRODerivedFromSO(env, inYFCXML);
			ReturnOrderUtils.setShipNodes(env, inYFCXML);
			setEnteredByAndCostCenter(env, inYFCXML);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setBilltoAddress(env, inYFCXML);
		
		salesOrderUtils.IsVaildSystemSerial(env, inYFCXML);
//		salesOrderUtils.checkOPPOSupport(env, inYFCXML);
		salesOrderUtils.isRPDEligible(env, inYFCXML);
		//salesOrderUtils.canOrderConsummable(env, inYFCXML);
		inYFCXML = salesOrderUtils.updateOpenBlanketPOBalanceinLine(env, inYFCXML);
		inYFCXML = salesOrderUtils.updateQtyOrderedToday(env, inYFCXML);
		//inYFCXML = salesOrderUtils.setCurrency(env, inYFCXML);
	    inYFCXML = salesOrderUtils.setDueDate(env, inYFCXML);

		return inYFCXML.getDocument();
	}

	private void setEnteredByAndCostCenter(YFSEnvironment env, YFCDocument inYFCXML) throws Exception {
		YFCElement order = inYFCXML.getDocumentElement();
		String enteredBy = order.getAttribute(XMLConstants.ENTERED_BY);
		if (!XmlUtils.isVoid(enteredBy)) {
			enteredBy = env.getUserId();
			order.setAttribute(XMLConstants.ENTERED_BY, enteredBy);
			YFCDocument inputDoc = YFCDocument.parse("<User Loginid=\"" + enteredBy + "\"/>");
			YFCDocument templateDoc = YFCDocument.parse("<User Username=\"\"><ContactPersonInfo AddressLine6=\"\"/></User>");
			YFCDocument outputDoc = SterlingUtil.callAPI(env, Constants.GET_USER_LIST_API, inputDoc, templateDoc);
			YFCElement userList = outputDoc.getDocumentElement();
			YFCElement user = userList.getChildElement(XMLConstants.USER);
			if (!XmlUtils.isVoid(user)) {
				YFCElement contactPersonInfo = user.getChildElement(XMLConstants.CONTACT_PERSON_INFO);
				if (!XmlUtils.isVoid(contactPersonInfo)) {
					String costCenter = contactPersonInfo.getAttribute(XMLConstants.ADDRESS_LINE6);
					if (!XmlUtils.isVoid(costCenter)) {
						YFCElement extn = order.getChildElement(XMLConstants.EXTN);
						if (XmlUtils.isVoid(extn)) {
							extn = order.createChild(XMLConstants.EXTN);
						}
						extn.setAttribute(XMLConstants.COST_CENTER, costCenter);
					}
				}
			}
		}
	}

	private void setBilltoAddress(YFSEnvironment env, YFCDocument inDoc) {
		YFCElement inDocEle = inDoc.getDocumentElement();
		try {
			if (inDocEle.hasAttribute(XMLConstants.RECEIVING_NODE)) {
				String receivingNode = inDocEle
						.getAttribute(XMLConstants.RECEIVING_NODE);
				Document teradyneCustsiteBillList = getCustomerSiteBillList(
						env, receivingNode);
				setBillToValues(inDocEle, teradyneCustsiteBillList);
			}
		} catch (Exception e) {
			if (_cat.isVerboseEnabled()) {
				_cat.verbose("setBillToAddress:" + e);
			}
		}

	}

	private Document getCustomerSiteBillList(YFSEnvironment env, String recvNode)
			throws Exception {
		api = YIFClientFactory.getInstance().getApi();
		YFCDocument inputdoc = YFCDocument
				.getDocumentFor("<TerCustBillingOrg TerOrganizationCodeKey=\""
						+ recvNode + "\" TerDefaultFLag=\"Y\" />");
		return api.executeFlow(env,
				Constants.GET_TER_CUST_BILLING_ORG_LIST_API,
				inputdoc.getDocument());
	}

	private void setBillToValues(YFCElement inDocEle, Document billListDoc) {
		YFCDocument yfcBillList = YFCDocument.getDocumentFor(billListDoc);
		YFCElement element = yfcBillList.getDocumentElement();
		YFCElement terBillToEle = element.getFirstChildElement();
		if (!XmlUtils.isVoid(terBillToEle)) {
			YFCElement personBillTo = inDocEle
					.createChild(XMLConstants.PERSON_INFO_BILL_TO);
			personBillTo.setAttribute(XMLConstants.ADDRESS_ID,
					terBillToEle.getAttribute(XMLConstants.TER_BILLING_ID));
			personBillTo.setAttribute(XMLConstants.ADDRESS_LINE1,
					terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE1));
			personBillTo.setAttribute(XMLConstants.ADDRESS_LINE2,
					terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE2));
			personBillTo.setAttribute(XMLConstants.ADDRESS_LINE3,
					terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE3));
			personBillTo.setAttribute(XMLConstants.ADDRESS_LINE4,
					terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE4));
			personBillTo.setAttribute(XMLConstants.ADDRESS_LINE5,
					terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE5));
			personBillTo.setAttribute(XMLConstants.ADDRESS_LINE6,
					terBillToEle.getAttribute(XMLConstants.TER_ADDRESS_LINE6));
			personBillTo.setAttribute(XMLConstants.ALTERNATE_EMAIL_ID,
					terBillToEle
							.getAttribute(XMLConstants.TER_ALTERNATE_EMAIL_ID));
			personBillTo.setAttribute(XMLConstants.BEEPER,
					terBillToEle.getAttribute(XMLConstants.TER_BEEPER));
			personBillTo.setAttribute(XMLConstants.CITY,
					terBillToEle.getAttribute(XMLConstants.TER_CITY));
			personBillTo.setAttribute(XMLConstants.STATE,
					terBillToEle.getAttribute(XMLConstants.TER_STATE));
			personBillTo.setAttribute(XMLConstants.COUNTRY,
					terBillToEle.getAttribute(XMLConstants.TER_COUNTRY));
			personBillTo.setAttribute(XMLConstants.DAY_FAX_NO,
					terBillToEle.getAttribute(XMLConstants.TER_DAY_FAX_NO));
			personBillTo.setAttribute(XMLConstants.DAY_PHONE,
					terBillToEle.getAttribute(XMLConstants.TER_DAY_PHONE));
			personBillTo.setAttribute(XMLConstants.EMAIL_ID,
					terBillToEle.getAttribute(XMLConstants.TER_EMAIL_ID));
			personBillTo.setAttribute(XMLConstants.EVENING_FAX_NO,
					terBillToEle.getAttribute(XMLConstants.TER_EVENING_FAX_NO));
			personBillTo.setAttribute(XMLConstants.EVENING_PHONE,
					terBillToEle.getAttribute(XMLConstants.TER_EVENING_PHONE));
			personBillTo.setAttribute(XMLConstants.MOBILE_PHONE,
					terBillToEle.getAttribute(XMLConstants.TER_MOBILE_PHONE));
			personBillTo.setAttribute(XMLConstants.OTHER_PHONE,
					terBillToEle.getAttribute(XMLConstants.TER_OTHER_PHONE));
			personBillTo.setAttribute(XMLConstants.ZIP_CODE,
					terBillToEle.getAttribute(XMLConstants.TER_ZIP_CODE));
		} else {
			_cat.verbose("Bill to is not configured.");
			// TODO we need to throw error. or just logging is enough
		}
	}

	@Override
	public String beforeCreateOrder(YFSEnvironment arg0, String arg1)
			throws YFSUserExitException {
		// TODO Auto-generated method stub
		return null;
	}

}

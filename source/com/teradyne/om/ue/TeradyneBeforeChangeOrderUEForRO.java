package com.teradyne.om.ue;

import java.io.IOException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.teradyne.om.api.CustomCodeHelper;
import com.teradyne.om.util.ReturnOrderUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeChangeOrderUE;

public class TeradyneBeforeChangeOrderUEForRO extends CustomCodeHelper implements YFSBeforeChangeOrderUE {

	@Override
	public Document beforeChangeOrder(YFSEnvironment env, Document inXML)
			throws YFSUserExitException {
		SalesOrderUtils salesOrderUtils = new SalesOrderUtils();
		YFCDocument inYFCXML = null;
		try {
			setEnv(env);
			inYFCXML = YFCDocument.getDocumentFor(inXML);
			ReturnOrderUtils.stampBuyer(inYFCXML);
			ReturnOrderUtils.stampControlNo(env, inYFCXML);
			ReturnOrderUtils.setAutoNoChargeCode(env, inYFCXML);
			ReturnOrderUtils.setShipNodes(env, inYFCXML);
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (YIFClientCreationException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		salesOrderUtils.IsVaildSystemSerial(env, inYFCXML);
//		salesOrderUtils.checkOPPOSupport(env, inYFCXML);
		salesOrderUtils.isRPDEligible(env, inYFCXML);
		//salesOrderUtils.canOrderConsummable(env, inYFCXML);
		inYFCXML = salesOrderUtils.updateOpenBlanketPOBalanceinLine(env, inYFCXML);
		inYFCXML = salesOrderUtils.updateQtyOrderedToday(env, inYFCXML);
		//inYFCXML = salesOrderUtils.setCurrency(env, inYFCXML);
	    inYFCXML = salesOrderUtils.setDueDate(env, inYFCXML);
		return inYFCXML.getDocument();
	}

}

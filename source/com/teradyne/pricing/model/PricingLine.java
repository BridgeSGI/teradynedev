package com.teradyne.pricing.model;

public class PricingLine {

	private String itemId;
	private String unitOfMeasure;
	private String status;
	private String listPrice;
	
	private String expeditingPrice;
	private String handlingCharge;
	
	private String headerKey;
	private String listName;
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}
	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getListPrice() {
		return listPrice;
	}
	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}
	public String getExpeditingPrice() {
		return expeditingPrice;
	}
	public void setExpeditingPrice(String expeditingPrice) {
		this.expeditingPrice = expeditingPrice;
	}
	public String getHandlingCharge() {
		return handlingCharge;
	}
	public void setHandlingCharge(String handlingCharge) {
		this.handlingCharge = handlingCharge;
	}
	public String getHeaderKey() {
		return headerKey;
	}
	public void setHeaderKey(String headerKey) {
		this.headerKey = headerKey;
	}
	public String getListName() {
		return listName;
	}
	public void setListName(String listName) {
		this.listName = listName;
	}
	
	
	
}


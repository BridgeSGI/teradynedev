package com.teradyne.pricing.model;

import org.w3c.dom.Document;

public class PricingHeader {

	private Document storage;
	private String currency;
	private String description;
	private String activeStartDate;
	private String activeEndDate;
	private String organizationCode;
	private String key;
	private String listName;
	private String status;
	
	
	private String isCustomerSpecific;
	private String originalAssignedCustomerId;
	private String originalAssignedZone;
	
	
	public Document getStorage() {
		return storage;
	}
	public void setStorage(Document storage) {
		this.storage = storage;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getActiveStartDate() {
		return activeStartDate;
	}
	public void setActiveStartDate(String activeStartDate) {
		this.activeStartDate = activeStartDate;
	}
	public String getActiveEndDate() {
		return activeEndDate;
	}
	public void setActiveEndDate(String activeEndDate) {
		this.activeEndDate = activeEndDate;
	}
	public String getOrganizationCode() {
		return organizationCode;
	}
	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getListName() {
		return listName;
	}
	public void setListName(String listName) {
		this.listName = listName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsCustomerSpecific() {
		return isCustomerSpecific;
	}
	public void setIsCustomerSpecific(String isCustomerSpecific) {
		this.isCustomerSpecific = isCustomerSpecific;
	}
	public String getOriginalAssignedCustomerId() {
		return originalAssignedCustomerId;
	}
	public void setOriginalAssignedCustomerId(String originalAssignedCustomerId) {
		this.originalAssignedCustomerId = originalAssignedCustomerId;
	}
	public String getOriginalAssignedZone() {
		return originalAssignedZone;
	}
	public void setOriginalAssignedZone(String originalAssignedZone) {
		this.originalAssignedZone = originalAssignedZone;
	}
	
	
	
	
	
	
	
}

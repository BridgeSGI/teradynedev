/**
 * 
 */
package com.teradyne.pricing.ue;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

import com.bridge.sterling.utils.CustomLogCategory;
import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * @author Tamilarasan
 * 
 * Service Arguments:
 * =================
 * AutomaticNoChargeCodesForWaiveOffRepairAndExpediteCharges = H,J,K,I
 * PricingModificationType = Teradyne Pricing Modification.ex
 * NoChargeCodeModificationType = Teradyne No Charge Code.ex
 * CustomerSpecificPriceListNamePrefix = PLC
 * PriceZoneSpecificPriceListNamePrefix = PLZ
 * HandlingNoChargeCodesForWaiveOffList = Y
 * ExpediteNoChargeCodesForWaiveOffList = Io
 * RepairNoChargeCodesForWaiveOffList = I
 * 
 */

public class TeradyneOrderRepricing implements YIFCustomApi {

	private static CustomLogCategory log = CustomLogCategory.instance(TeradyneOrderRepricing.class);

	private Properties prop;

	private YFSEnvironment env;

	private YFCDocument yInDoc;

	public Document invoke(YFSEnvironment env, Document inDoc) {
		log.beginTimer("TeradyneOrderRepricing.invoke");
		try {
			this.env = env;
			this.yInDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement order = yInDoc.getDocumentElement();
			String documentType = order.getAttribute("DocumentType");
			String buyerOrgCode = order.getAttribute("BuyerOrganizationCode");
			CustomerInfo customer = getCustomerInformation(buyerOrgCode);
			if(isCreateOrderCall()) {
				log.verbose("Executing Logic for CreateOrder transaction...");
				executeOrderRepricing(customer,documentType);
			} else {
				log.verbose("Executing Logic for ChangeOrder transaction...");
				YFCNodeList<YFCElement> orderLines = yInDoc.getElementsByTagName("OrderLine");
				for(int i=0;i<orderLines.getLength();i++) {
					YFCElement orderLine = orderLines.item(i);
					log.verbose("Calculting price for the line : " + orderLine.getAttribute("OrderLineKey"));
					if(isNewOrderLine(orderLine)) {
						doOrderLinePricing(orderLine,documentType,customer);
					} else if(isModificationRequiresNoChargeCodeCalutaions(orderLine)) {
						log.verbose("NoChargeCodes Modification requires price calculations for the Line : " + orderLine.getAttribute("OrderLineKey"));
						calculateOrderLineTotalPricing(orderLine,orderLine.getChildElement("Extn"),getPriceDTOFromOrderLineExtn(orderLine.getChildElement("Extn")),
								yInDoc.getDocumentElement().getAttribute("DocumentType"),"",customer);
					}
				}
			}
		} // Consume Any exception during the flow and dont modify price on OrderLine
		catch (RemoteException e) {
			log.error("RemoteException Occured in TeradyneOrderRepricing : ");
		} catch (YFCException e) {
			log.error("Exception Occured in TeradyneOrderRepricing : ");
		} catch (TransformerException e) {
			log.error("TransformerException Occured in TeradyneOrderRepricing : ");
		} catch (ParserConfigurationException e) {
			log.error("ParserConfigurationException Occured in TeradyneOrderRepricing : ");
		} catch (YIFClientCreationException e) {
			log.error("YIFClientCreationException Occured in TeradyneOrderRepricing : ");
		}
		log.endTimer("TeradyneOrderRepricing.invoke");
		return yInDoc.getDocument();
	}

	private boolean isNewOrderLine(YFCElement orderLine) {
		YFCElement modificationTypes = orderLine.getChildElement("ModificationTypes");
		if(!XmlUtils.isVoid(modificationTypes) && !modificationTypes.hasChildNodes()) {
			return true;
		}
		return false;
	}

	private PriceDTO getPriceDTOFromOrderLineExtn(YFCElement OrdlnExtn) {
		PriceDTO itemPrices = new PriceDTO();
		itemPrices.setExpiditingPrice(OrdlnExtn.getDoubleAttribute("PartExpiditingPrice", 0.0));
		itemPrices.setHandlingCharge(OrdlnExtn.getDoubleAttribute("PartHandlingCharge", 0.0));
		itemPrices.setRepairPrice(OrdlnExtn.getDoubleAttribute("PartRepairPrice", 0.0));
		return itemPrices;
	}

	private void calculateOrderLineTotalPricing(YFCElement orderLine,YFCElement OrdlnExtn,PriceDTO itemPrices, 
			String documentType, String customerDefaultCurrency, CustomerInfo customer) throws RemoteException, YFCException, YIFClientCreationException {
		double orderedQty = orderLine.getDoubleAttribute("OrderedQty");
		if("0006".equals(documentType)) {
			OrdlnExtn.setAttribute("AutomaticNoChargeCode", "I");
		}
		String automaticNoChargeCode =  OrdlnExtn.getAttribute("AutomaticNoChargeCode","");
		String expediteNoChargeCode = OrdlnExtn.getAttribute("ExpediteNoChargeCode","");
		String handlingNoChargeCode = OrdlnExtn.getAttribute("HandlingNoChargeCode","");
		String repairNoChargeCode = OrdlnExtn.getAttribute("RepairNoChargeCode","");
		double partExpiditingPrice = convertToCustCurrencyIfReqd(itemPrices.getExpiditingPrice(),customerDefaultCurrency);
		double partHandlingCharge = convertToCustCurrencyIfReqd(itemPrices.getHandlingCharge(),customerDefaultCurrency);
		double partRepairPrice = convertToCustCurrencyIfReqd(itemPrices.getRepairPrice(),customerDefaultCurrency);
		OrdlnExtn.setAttribute("PartExpiditingPrice", partExpiditingPrice);
		OrdlnExtn.setAttribute("PartHandlingCharge", partHandlingCharge);
		OrdlnExtn.setAttribute("PartRepairPrice", partRepairPrice);
		if(noChargeCodeRequiresWaiveOff("HandlingNoChargeCodesForWaiveOffList",handlingNoChargeCode)) {
			partHandlingCharge = 0.0;
		}
		if(noChargeCodeRequiresWaiveOff("ExpediteNoChargeCodesForWaiveOffList",expediteNoChargeCode)) {
			partExpiditingPrice = 0.0;
		}
		if(noChargeCodeRequiresWaiveOff("RepairNoChargeCodesForWaiveOffList",repairNoChargeCode)) {
			partRepairPrice = 0.0;
		}
		if(automaticNoChargeCodesrequiresWaiveOff(automaticNoChargeCode,documentType)) {
			partRepairPrice = 0.0;
			partExpiditingPrice = 0.0;
		}
		String repairCode = getItemRepairCode(orderLine.getChildElement("Item"));
		double lineTotal = getOrderLineTotalPrice(repairCode,partExpiditingPrice,partHandlingCharge,partRepairPrice,orderedQty);
		log.verbose("Line Total : " + lineTotal);
		YFCElement linePriceInfo = orderLine.getChildElement("LinePriceInfo", true);
		linePriceInfo.setAttribute("UnitPrice",lineTotal/orderedQty);
		linePriceInfo.setAttribute("IsPriceLocked","Y");
		OrdlnExtn.setAttribute("OLChargedTotalPrice", lineTotal);
		OrdlnExtn.setAttribute("OLTotalPrice", lineTotal);
		if("0001".equals(documentType) && isCustomerInternalOrCoveredUnderWarranty(customer,OrdlnExtn)) {
			log.verbose("isCustomerInternalOrCoveredUnderWarranty : true Making untiPrice to 0.0");
			linePriceInfo.setAttribute("UnitPrice",0.0);
			OrdlnExtn.setAttribute("OLChargedTotalPrice", 0.0);
		}
		if(isInternalCustomer(customer)) {
			OrdlnExtn.setAttribute("NoChargeCode", "Internal");
		}
	}

	private boolean noChargeCodeRequiresWaiveOff(String propertyName, String noChargeCode) {
		String[] noChargeCodesForWaiveOff = prop.getProperty(propertyName,"").split(",");
		for(String noChargeCodeForWaiveOff : noChargeCodesForWaiveOff) {
			if(noChargeCodeForWaiveOff.equals(noChargeCode)) {
				return true;
			}
		}
		return false;
	}

	private String getItemRepairCode(YFCElement itemEle) throws RemoteException, YFCException, YIFClientCreationException {
		YFCElement itemExtn = itemEle.getChildElement("Extn");
		if(!XmlUtils.isVoid(itemExtn)) {
			return itemExtn.getAttribute("RepairCode");
		}
		String strGetItemListInput = "<Item ItemID='" + itemEle.getAttribute("ItemID") + "' ProductClass='" + itemEle.getAttribute("ProductClass") + "' UnitOfMeasure='" + itemEle.getAttribute("UnitOfMeasure") + "' OrganizationCode='CSO'/>";
		String strGetItemListTemplate = "<ItemList><Item ItemID=''><Extn RepairCode=''/></Item></ItemList>";
		YFCDocument docGetItemListOutput = SterlingUtil.callAPI(env, "getItemList", YFCDocument.getDocumentFor(strGetItemListInput),
				YFCDocument.getDocumentFor(strGetItemListTemplate));
		if(docGetItemListOutput.getDocumentElement().hasChildNodes()) {
			YFCElement eleItem = docGetItemListOutput.getDocumentElement().getChildElement("Item");
			return eleItem.getChildElement("Extn").getAttribute("RepairCode");
		}
		return "";
	}

	private boolean automaticNoChargeCodesrequiresWaiveOff(String automaticNoChargeCode, String documentType) {
		if("0006".equals(documentType)) {
			return true;
		}
		return noChargeCodeRequiresWaiveOff("AutomaticNoChargeCodesForWaiveOffRepairAndExpediteCharges",automaticNoChargeCode);
	}

	private double convertToCustCurrencyIfReqd(double priceValue, String currency) throws RemoteException, YFCException, YIFClientCreationException {
		if(XmlUtils.isVoid(currency) || "USD".equals(currency.trim())){
			return priceValue;
		}
		log.verbose("Requires Currency conversion...");
		String strConvertCurrencyInput = "<convertCurrency ConversionDate='" + DateTimeUtil.getXMLCurrentTime() + "' " +
				"EnterpriseCode='CSO' FromCurrency='USD' InputAmount='" + priceValue + "' ToCurrency='" + currency + "'/>";
		String strConvertCurrencyTemplate = "<convertCurrency OutputAmount=''/>";
		YFCDocument convertCurrencyOutput = SterlingUtil.callAPI(env, "convertCurrency", YFCDocument.getDocumentFor(strConvertCurrencyInput),
				YFCDocument.getDocumentFor(strConvertCurrencyTemplate));
		return convertCurrencyOutput.getDocumentElement().getDoubleAttribute("OutputAmount", priceValue);
	}

	private double getOrderLineTotalPrice(String repairCode, double partExpiditingPrice, double partHandlingCharge, double partRepairPrice, double orderedQty) {
		log.verbose("RepairCode : " + repairCode);
		log.verbose("PartExpiditingPrice : " + partExpiditingPrice);
		log.verbose("PartHandlingCharge : " + partHandlingCharge);
		log.verbose("PartRepairPrice : " + partRepairPrice);
		log.verbose("OrderedQty : " + orderedQty);
		if("R".equalsIgnoreCase(repairCode)) {
			return ((partRepairPrice + partExpiditingPrice) * orderedQty) + partHandlingCharge;
		}
		return (partRepairPrice * orderedQty) + partExpiditingPrice + partHandlingCharge;
	}

	private void executeOrderRepricing(CustomerInfo customer,String documentType) throws RemoteException, YFCException, YIFClientCreationException {
		YFCNodeList<YFCElement> orderLines = yInDoc.getElementsByTagName("OrderLine");
		for(int i=0;i<orderLines.getLength();i++) {
			YFCElement orderLine = orderLines.item(i);
			log.verbose("Calculting price for the line : " + orderLine.getAttribute("OrderLineKey"));
			doOrderLinePricing(orderLine,documentType,customer);
		}
	}

	private void doOrderLinePricing(YFCElement orderLine, String documentType, CustomerInfo customer) throws RemoteException, YFCException, YIFClientCreationException {
		YFCElement orderLineItem = orderLine.getChildElement("Item");
		YFCElement orderLineExtn = orderLine.getChildElement("Extn");
		String itemId = orderLineItem.getAttribute("ItemID");
		String serviceType = orderLineExtn.getAttribute("ServiceType");
		String systemType = orderLineExtn.getAttribute("SystemType");
		boolean isCustomerSpecificPL = false;
		YFCDocument assignedPriceList = getPriceListAssignedToCustomer(customer.getCustomerId());
		YFCElement elePricelistHeaderList = assignedPriceList.getDocumentElement();
		YFCDocument docPriceListLineList = null;
		if(elePricelistHeaderList.hasChildNodes()) {
			List<String> priceLists = getPriceLists(elePricelistHeaderList);
			String customerAssignedPriceList = getCustomerAssignedPriceListIfAvailable(priceLists,serviceType,systemType);
			log.verbose("Customer Assigned Price List : " + customerAssignedPriceList);
			if(!XmlUtils.isVoid(customerAssignedPriceList)) {
				log.verbose("Getting Customer specific Price List Line List..");
				docPriceListLineList = getCustomerSpecificPriceListLineList(itemId,customerAssignedPriceList,customer.getDefaultCurreny());
				isCustomerSpecificPL = true;
			}
		} 
		if(docPriceListLineList == null || !docPriceListLineList.getDocumentElement().hasChildNodes()) {
			log.verbose("Getting Zone Specific Price List Line List..");
			docPriceListLineList = getCustomerPriceZoneSpecificPriceListLineList(itemId,serviceType,customer.getCustPriceZone(),customer.getDefaultCurreny());
		}
		if(docPriceListLineList != null) {
			YFCElement lowestPriceLineList = getLowestPriceListLineList(docPriceListLineList.getDocumentElement());
			log.verbose("The Price List Line List with Lowest Repair Price : " , lowestPriceLineList);
			PriceDTO itemPrices = getItemPricesDTO(lowestPriceLineList);
			if(!isCustomerSpecificPL) {
				applyDiscounts(itemPrices,customer.getCustomerId(),serviceType,systemType);
			}
			calculateOrderLineTotalPricing(orderLine,orderLineExtn,itemPrices,documentType,customer.getDefaultCurreny(),customer);
		}
	}

	private boolean isInternalCustomer(CustomerInfo customer) {
		if(customer.getIsInternalCustomer()) {
			return true;
		}
		return false;
	}

	private boolean isCustomerInternalOrCoveredUnderWarranty(CustomerInfo customer, YFCElement ordlnExtn) {
		if(isInternalCustomer(customer) || isCustomerCoveredUnderWarranty(ordlnExtn)) {
			return true;
		}
		return false;
	}

	private boolean isCustomerCoveredUnderWarranty(YFCElement ordlnExtn) {
		if(!XmlUtils.isVoid(ordlnExtn.getAttribute("AgreementNo"))) {
			return true;
		}
		return false;
	}

	private void applyDiscounts(PriceDTO itemPrices, String customerId, String serviceType, String systemType) 
			throws RemoteException, YFCException, YIFClientCreationException {
		YFCDocument docDiscountGrpList = getDiscountGrpList(customerId,serviceType);
		YFCElement eleDiscountGrpList = docDiscountGrpList.getDocumentElement();
		YFCElement discountGrpToBeApplied = null;
		if(eleDiscountGrpList.hasChildNodes()) {
			YFCNodeList<YFCElement> nlDiscountGrp = eleDiscountGrpList.getElementsByTagName("TerDiscountGroup");
			for(int i=0;i<nlDiscountGrp.getLength();i++) {
				YFCElement discountGrp = nlDiscountGrp.item(i);
				String discountGrpSystemType = discountGrp.getAttribute("TerSystemType","");
				if(systemType.equals(discountGrpSystemType)) {
					discountGrpToBeApplied = discountGrp;
					log.verbose("Found SystemType specific discount group record..");
					break;
				}
				if("ALL".equalsIgnoreCase(discountGrpSystemType)) {
					discountGrpToBeApplied = discountGrp;
				}
			}
			if(discountGrpToBeApplied!=null) {
				log.verbose("Applying Discounts : " , discountGrpToBeApplied);
				double discountPercentage = discountGrpToBeApplied.getDoubleAttribute("TerDiscountPercentage");
				if(discountPercentage != 0.0) {
					double currRepairPrice = itemPrices.getRepairPrice();
					double currExpiditingPrice = itemPrices.getExpiditingPrice();
					itemPrices.setRepairPrice(currRepairPrice + ((currRepairPrice*discountPercentage)/100));
					itemPrices.setExpiditingPrice(currExpiditingPrice + ((currExpiditingPrice*discountPercentage)/100));
					log.verbose("Repair Price After discount : " + itemPrices.getRepairPrice());
					log.verbose("ExpiditingPrice after discount : " + itemPrices.getExpiditingPrice());
				}
			}
		}
	}

	private YFCDocument getDiscountGrpList(String customerId, String serviceType) throws RemoteException, YFCException, YIFClientCreationException {
		String strDiscountGrpListInput = "<TerDiscountGroup TerCustomerNo='" + customerId + "' TerServiceType='" + serviceType + "'/>";
		return executeFlow(env, "getTDDiscountGroupList", YFCDocument.getDocumentFor(strDiscountGrpListInput));
	}
	
	private YFCDocument executeFlow(YFSEnvironment env,String flowName, YFCDocument inpDoc) {
		Document outDoc = null;
		try {
			outDoc = getApi().executeFlow(env, flowName, inpDoc.getDocument());
		} catch (final RemoteException rex) {
			rex.printStackTrace();
			throw new YFSException(rex.getMessage());
		} catch (final Exception ex) {
			ex.printStackTrace();
			throw new YFSException(ex.getMessage());
		}
		return YFCDocument.getDocumentFor(outDoc);
	}
	
	private static YIFApi _Api = null;
	
	private YIFApi getApi() throws YFSException {
		if (_Api == null) {
			try {
				_Api = YIFClientFactory.getInstance().getLocalApi();
			} catch (final YIFClientCreationException cex) {
				cex.printStackTrace();
				throw new YFSException(cex.getMessage());
			} catch (final Exception ex) {
				ex.printStackTrace();
				throw new YFSException(ex.getMessage());
			}
		}
		return _Api;
	}

	private PriceDTO getItemPricesDTO(YFCElement lowestPriceLineList) {
		PriceDTO itemPrices = new PriceDTO();
		if(lowestPriceLineList != null) {
			YFCElement lowestPriceLineListExtn = lowestPriceLineList.getChildElement("Extn");
			itemPrices.setRepairPrice(lowestPriceLineList.getDoubleAttribute("ListPrice"));
			itemPrices.setUnitPrice(lowestPriceLineList.getDoubleAttribute("UnitPrice"));
			itemPrices.setHandlingCharge(lowestPriceLineListExtn.getDoubleAttribute("HandlingCharge"));
			itemPrices.setExpiditingPrice(lowestPriceLineListExtn.getDoubleAttribute("ExpiditingPrice"));
		}
		return itemPrices;
	}

	private YFCElement getLowestPriceListLineList(YFCElement pricelistLineList) {
		if(!pricelistLineList.hasChildNodes()) {
			log.verbose("Netiher Customer Specific nor Zone Specific Active Price Lists found");
			return null; 
		}
		YFCNodeList<YFCElement> nlPriceListLine = pricelistLineList.getElementsByTagName("PricelistLine");
		return nlPriceListLine.item(0);
	}

	private YFCDocument getCustomerSpecificPriceListLineList(String itemId,String customerAssignedPriceList, String defaultCurreny) 
			throws RemoteException, YFCException, YIFClientCreationException {
		String strPriceListLineListInput = "<PricelistLine ItemID='" + itemId + "' PricingStatus='ACTIVE'>" +
				"<Item OrganizationCode='CSO'/>" +
				"<PricelistHeader OrganizationCode='CSO' PricelistName='" + customerAssignedPriceList + "' PricingStatus='ACTIVE' />" +
				"<OrderBy>" +
				"<Attribute Name='ListPrice'/>" +
				"</OrderBy>" +
				"</PricelistLine>";
		String strPriceListLineListTemplate = "<PricelistLineList>" +
				"<PricelistLine ListPrice='' UnitPrice=''>" +
				"<Extn HandlingCharge='' ExpiditingPrice=''/>" +
				"</PricelistLine>" +
				"</PricelistLineList>";
		return SterlingUtil.callAPI(env, "getPricelistLineList", YFCDocument.getDocumentFor(strPriceListLineListInput), 
				YFCDocument.getDocumentFor(strPriceListLineListTemplate));
	}

	private YFCDocument getCustomerPriceZoneSpecificPriceListLineList(String itemId, String serviceType, String custPriceZone,String defaultCurreny) 
			throws RemoteException, YFCException, YIFClientCreationException {
		String priceListName = prop.getProperty("PriceZoneSpecificPriceListNamePrefix", "PLZ") + "_" + serviceType + "_" + custPriceZone;
		String strPriceListLineListInput = "<PricelistLine ItemID='" + itemId + "' PricingStatus='ACTIVE'>" +
				"<Item OrganizationCode='CSO'/>" +
				"<PricelistHeader Currency='" + defaultCurreny + "' OrganizationCode='CSO' PricelistNameQryType='FLIKE' PricelistName='" + priceListName + "' " +
				"PricingStatus='ACTIVE' EndDateActiveQryType='GE' EndDateActive='" + DateTimeUtil.getXMLCurrentTime() + "' " +
				"StartDateActiveQryType='LE' StartDateActive='" + DateTimeUtil.getXMLCurrentTime() + "'/>" +
				"<OrderBy>" +
				"<Attribute Name='ListPrice'/>" +
				"</OrderBy>" +
				"</PricelistLine>";
		String strPriceListLineListTemplate = "<PricelistLineList>" +
				"<PricelistLine ListPrice='' UnitPrice=''>" +
				"<Extn HandlingCharge='' ExpiditingPrice=''/>" +
				"</PricelistLine>" +
				"</PricelistLineList>";
		return SterlingUtil.callAPI(env, "getPricelistLineList", YFCDocument.getDocumentFor(strPriceListLineListInput), 
				YFCDocument.getDocumentFor(strPriceListLineListTemplate));
	}

	private String getCustomerAssignedPriceListIfAvailable(List<String> priceLists, String serviceType, String systemType) {
		String customerAssignedPriceList = "";
		for(String priceList : priceLists) {
			String[] priceListTokens = priceList.split("_");
			if(priceListTokens.length >= 3) {
				if(prop.getProperty("CustomerSpecificPriceListNamePrefix", "PLC").equals(priceListTokens[0]) && serviceType.equals(priceListTokens[1])) {
					if(systemType.equals(priceListTokens[2])) {
						return priceList;
					}
					if (priceListTokens.length >= 4 && "ALL".equals(priceListTokens[3])) {
						if(systemType.startsWith(priceListTokens[2])){
							customerAssignedPriceList = priceList;
						}
					} else {
						if("ALL".equals(priceListTokens[2])) {
							customerAssignedPriceList = priceList;
						}
					}
				}
			}
		}
		return customerAssignedPriceList;
	}

	private List<String> getPriceLists(YFCElement elePricelistHeaderList) {
		List<String> priceLists = new ArrayList<String>();
		YFCNodeList<YFCElement> nlPricelistHeader = elePricelistHeaderList.getElementsByTagName("PricelistHeader");
		for(int i=0; i<nlPricelistHeader.getLength();i++) {
			YFCElement pricelistHeader = nlPricelistHeader.item(i);
			String pricelistName = pricelistHeader.getAttribute("PricelistName");
			if("ACTIVE".equals(pricelistHeader.getAttribute("PricingStatus")) && !priceLists.contains(pricelistName)) {
				priceLists.add(pricelistName);
			}
		}
		return priceLists;
	}

	private YFCDocument getPriceListAssignedToCustomer(String customerId) throws RemoteException, YFCException, YIFClientCreationException {
		String strGetAssignedPriceListInput = "<AssignedPricelist Currency='USD' CustomerID='" + customerId + "' OrganizationCode='CSO' " +
				"PricingDate='" + DateTimeUtil.getXMLCurrentTime() + "'/>";
		String strGetAssignedPriceListTemplate = "<PricelistHeaderList> <PricelistHeader PricelistName='' " +
				"PricingStatus='' PricelistHeaderKey=''/> </PricelistHeaderList>";
		return SterlingUtil.callAPI(env,"getAssignedPricelistHeaderList",YFCDocument.getDocumentFor(strGetAssignedPriceListInput), 
				YFCDocument.getDocumentFor(strGetAssignedPriceListTemplate));
	}

	private CustomerInfo getCustomerInformation(String buyerOrgCode) throws RemoteException, YFCException, YIFClientCreationException {
		CustomerInfo customer = new CustomerInfo();
		YFCDocument docCust = getCustomerInfoFromBuyerOrgCode(buyerOrgCode);
		YFCElement eleCustList = docCust.getDocumentElement();
		if(eleCustList.hasChildNodes()) {
			YFCElement eleCust = eleCustList.getChildElement("Customer");
			customer.setCustomerId(getCustomerId(eleCust));
			customer.setIsInternalCustomer(getCustInternalFlag(eleCust));
			customer.setDefaultCurreny(getDefaultCurreny(eleCust));
			customer.setCustPriceZone(getCustomerPriceZone(eleCust));
		} else {
			YFCException ex = new YFCException();
			ex.setAttribute("BuyerOrganizationCode", buyerOrgCode);
			ex.setErrorDescription("No Customer Information found");
			throw ex;
		}
		return customer;
	}

	private String getCustomerPriceZone(YFCElement eleCust) {
		return eleCust.getChildElement("Extn").getAttribute("PriceZone","");
	}

	private String getDefaultCurreny(YFCElement eleCust) {
		return eleCust.getChildElement("Extn").getAttribute("DefaultCurrency","USD");
	}

	private String getCustInternalFlag(YFCElement eleCust) {
		return eleCust.getChildElement("Extn").getAttribute("InternalCustomerFlag","N");
	}

	private String getCustomerId(YFCElement eleCust) {
		return eleCust.getAttribute("CustomerID","");
	}

	private YFCDocument getCustomerInfoFromBuyerOrgCode(String buyerOrgCode) throws RemoteException, YFCException, YIFClientCreationException {
		String strCustListInput = "<Customer BuyerOrganizationCode='" + buyerOrgCode + "'/>";
		String strCustListTemplate = "<CustomerList> " +
				"<Customer CustomerID='' RelationshipType=''> " +
				"<Extn InternalCustomerFlag='' DefaultCurrency='' PriceZone=''/> " +
				"</Customer> " +
				"</CustomerList>";
		return SterlingUtil.callAPI(env, "getCustomerList", YFCDocument.getDocumentFor(strCustListInput), YFCDocument.getDocumentFor(strCustListTemplate));
	}

	private boolean isModificationRequiresNoChargeCodeCalutaions(YFCElement orderLine) throws TransformerException, ParserConfigurationException {
		List<String> listPricingModificationTypes = getOrderLineModificationTypesImpactsPricing(orderLine);
		if(listPricingModificationTypes.contains(prop.getProperty("NoChargeCodeModificationType"))) {
			return true;
		}
		return false;
	}

	private List<String> getOrderLineModificationTypesImpactsPricing(YFCElement orderLine) throws TransformerException, ParserConfigurationException {
		List<String> listPricingModificationTypes = new ArrayList<String>();
		YFCNodeList<YFCElement> modificationTypes = orderLine.getElementsByTagName("ModificationType");
		for(int i=0;i<modificationTypes.getLength();i++) {
			YFCElement modificationType = (YFCElement) modificationTypes.item(i);
			if("Y".equalsIgnoreCase(modificationType.getAttribute("ImpactsPricing"))) {
				listPricingModificationTypes.add(modificationType.getAttribute("Name"));
			}
		}
		return listPricingModificationTypes;
	}

	private boolean isCreateOrderCall() {
		YFCElement order = yInDoc.getDocumentElement();
		String isNewOrder = order.getAttribute("IsNewOrder");
		YFCElement modificationTypes = order.getChildElement("ModificationTypes");
		if("Y".equalsIgnoreCase(isNewOrder) || (!XmlUtils.isVoid(modificationTypes) && !modificationTypes.hasChildNodes())) {
			return true;
		}
		return false;
	}

	@Override
	public void setProperties(Properties prop) throws Exception {
		this.prop = prop;
	}

	public class PriceDTO {
		private double repairPrice = 0.0;

		private double unitPrice = 0.0;

		private double handlingCharge = 0.0;

		private double expiditingPrice = 0.0;

		public double getRepairPrice() {
			return repairPrice;
		}

		public void setRepairPrice(double repairPrice) {
			this.repairPrice = repairPrice;
		}

		public double getUnitPrice() {
			return unitPrice;
		}

		public void setUnitPrice(double unitPrice) {
			this.unitPrice = unitPrice;
		}

		public double getHandlingCharge() {
			return handlingCharge;
		}

		public void setHandlingCharge(double handlingCharge) {
			this.handlingCharge = handlingCharge;
		}

		public double getExpiditingPrice() {
			return expiditingPrice;
		}

		public void setExpiditingPrice(double expiditingPrice) {
			this.expiditingPrice = expiditingPrice;
		}
	}

	private class CustomerInfo {
		private String customerId = "";

		private String isInternalCustomer = "";

		private String defaultCurreny = "";

		private String custPriceZone = "";

		public String getCustomerId() {
			return customerId;
		}

		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}

		public boolean getIsInternalCustomer() {
			if(!XmlUtils.isVoid(isInternalCustomer) && ("Y".equalsIgnoreCase(isInternalCustomer) || "Yes".equalsIgnoreCase(isInternalCustomer))) {
				return true;
			}
			return false;
		}

		public void setIsInternalCustomer(String custRelationshipType) {
			this.isInternalCustomer = custRelationshipType;
		}

		public String getDefaultCurreny() {
			return defaultCurreny;
		}

		public void setDefaultCurreny(String defaultCurreny) {
			this.defaultCurreny = defaultCurreny;
		}

		public String getCustPriceZone() {
			return custPriceZone;
		}

		public void setCustPriceZone(String custPriceZone) {
			this.custPriceZone = custPriceZone;
		}
	}
}

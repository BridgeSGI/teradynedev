package com.teradyne.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.junit.Test;
import org.w3c.dom.Document;

import com.teradyne.pricing.model.PricingHeader;
import com.teradyne.pricing.transformer.DocumentToPricingHeaderTransformer;
import com.teradyne.pricing.transformer.DocumentToStringTransformer;
import com.teradyne.pricing.transformer.StringToDocumentTransformer;

public class PricingHeaderTest {

	
	@Test
	public void testPricingHeaderTransformer() throws Exception
	{
		BufferedReader br = null;
		String xml = "";
		try {
 
			String sCurrentLine;
			
 
			InputStream in = PricingHeaderTest.class.getResourceAsStream("/MultiApi.xml");
			Reader r = new InputStreamReader(in, "UTF-8");
			
			br = new BufferedReader(r);
 
			while ((sCurrentLine = br.readLine()) != null) {
				xml +=sCurrentLine;
			}
			
			//System.out.println(xml);
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		
		StringToDocumentTransformer stringTransformer = new StringToDocumentTransformer();
		
		Document inXml = stringTransformer.transform(xml);
		
		DocumentToPricingHeaderTransformer pricingTransformer = new DocumentToPricingHeaderTransformer();
		PricingHeader header = pricingTransformer.transform(inXml);
		
		System.out.println("header key ="+header.getKey() + ":isCustomerPricing?=" + header.getIsCustomerSpecific());
		
		DocumentToStringTransformer documentTransformer = new DocumentToStringTransformer();
		
		System.out.println(documentTransformer.transform(header.getStorage()));
		
	}
	
}

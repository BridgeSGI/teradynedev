package com.teradyne.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.junit.Test;
import org.w3c.dom.Document;

import com.teradyne.pricing.model.PricingLine;
import com.teradyne.pricing.model.PricingLines;
import com.teradyne.pricing.transformer.DocumentToPricingLinesTransformer;
import com.teradyne.pricing.transformer.DocumentToStringTransformer;
import com.teradyne.pricing.transformer.StringToDocumentTransformer;

public class PricingLinesTest {

	
	
	@Test
	public void testPricingLinesTransformer() throws Exception
	{
		BufferedReader br = null;
		String xml = "";
		try {
 
			String sCurrentLine;
			
 
			InputStream in = PricingHeaderTest.class.getResourceAsStream("/MultiApi.xml");
			Reader r = new InputStreamReader(in, "UTF-8");
			
			br = new BufferedReader(r);
 
			while ((sCurrentLine = br.readLine()) != null) {
				xml +=sCurrentLine;
			}
			
			//System.out.println(xml);
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		
		StringToDocumentTransformer stringTransformer = new StringToDocumentTransformer();
		
		Document inXml = stringTransformer.transform(xml);
		
		DocumentToPricingLinesTransformer pricingTransformer = new DocumentToPricingLinesTransformer();
		PricingLines pricingLines = pricingTransformer.transform(inXml);
		
		System.out.println(pricingLines.getOrganizationCode());
		
		
		
		for(PricingLine pricingLine:pricingLines.getPriceLines())
		{
			System.out.println("header=" + pricingLine.getHeaderKey() +":itemid=" +pricingLine.getItemId() + ":expeditingPrice=" + pricingLine.getExpeditingPrice());
		}
		
		DocumentToStringTransformer documentTransformer = new DocumentToStringTransformer();
		
		System.out.println(documentTransformer.transform(pricingLines.getStorage()));
	}
}

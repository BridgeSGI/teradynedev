package com.teradyne.pricing.transformer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.teradyne.pricing.model.PricingHeader;

public class DocumentToPricingHeaderTransformer {

	public PricingHeader transform(Document document)
	{
		
		Element root = document.getDocumentElement();
		
		NodeList apiNodes = root.getElementsByTagName("API");
		
		Element apiEle = null;
		for(int i=0;i<apiNodes.getLength();i++)
		{
			apiEle = (Element)apiNodes.item(i);
			
			if(apiEle.getAttribute("Name").equals("managePricelistHeader"))
			{
				System.out.println("found managePricelistHeader");
				break;
			}
		
		}
		
		
		
		NodeList inputNodes = apiEle.getElementsByTagName("Input");
		
		Element inputEle = null;
		for(int i=0;i<inputNodes.getLength();i++)
		{
			inputEle = (Element)inputNodes.item(i);
			
			break;
		
		}
		
		NodeList pricingHeaderNodes = inputEle.getElementsByTagName("PricelistHeader");
		
		Element pricingHeaderEle = null;
		for(int i=0;i<pricingHeaderNodes.getLength();i++)
		{
			pricingHeaderEle = (Element)pricingHeaderNodes.item(i);
			
			break;
		
		}
		
		
		
		
		
		PricingHeader header = new PricingHeader();
		
		
		NodeList extnNodes = pricingHeaderEle.getElementsByTagName("Extn");
		
		for(int i=0;i<extnNodes.getLength();i++)
		{
			Element extnEle = (Element)extnNodes.item(i);
			
			header.setIsCustomerSpecific(extnEle.getAttribute("IsCustomerSpecificPL"));
			header.setOriginalAssignedCustomerId(extnEle.getAttribute("OriginalAssignedCustomerID"));
			header.setOriginalAssignedZone(extnEle.getAttribute("OriginalAssignedZone"));
		}
		
		
		header.setActiveStartDate(pricingHeaderEle.getAttribute("StartDateActive"));
		header.setActiveStartDate(pricingHeaderEle.getAttribute("EndDateActive"));
		header.setCurrency(pricingHeaderEle.getAttribute("Currency"));
		header.setDescription(pricingHeaderEle.getAttribute("Description"));
		header.setKey(pricingHeaderEle.getAttribute("PricelistHeaderKey"));
		header.setListName(pricingHeaderEle.getAttribute("PricelistName"));
		header.setOrganizationCode(pricingHeaderEle.getAttribute("OrganizationCode"));
		header.setStatus(pricingHeaderEle.getAttribute("PricingStatus"));
		
		header.setStorage(transformElementToDocument(pricingHeaderEle));
		
		
		return header;
	}
	
	
	private Document transformElementToDocument(Element element) 
	{
		
		DocumentBuilderFactory docBuilderFactory =DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder =null;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			//throw new TransformerException();	
		}
		Document doc = docBuilder.newDocument();
		
		Node node = element.cloneNode(true);
		//Element rootElement = doc.createElement("Order");
		
		doc.adoptNode(node);
		doc.appendChild(node);
		
		return doc;
		
	}
}

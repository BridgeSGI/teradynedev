package com.teradyne.pricing.transformer;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.teradyne.pricing.model.PricingLine;

public class ElementToPricingLineTransformer {

	
	// <PricelistLine ItemID="100001" PricingStatus="ACTIVE" UnitOfMeasure="EACH" 
    //ListPrice="339.00">
    //<Extn ExpiditingPrice="120" HandlingCharge="15" />
    //<PricelistHeader PricelistHeaderKey="PL_EAR_JP" PricelistName="PL_EAR_JP" /> 
//</PricelistLine>
	
	public PricingLine transform(Element element)
	{
		
		PricingLine pricingLine = new PricingLine();
	
		
		NodeList extnNodes = element.getElementsByTagName("Extn");
		
		for(int i=0;i<extnNodes.getLength();i++)
		{
			Element extnEle = (Element)extnNodes.item(i);
			
			pricingLine.setExpeditingPrice(extnEle.getAttribute("ExpiditingPrice"));
			pricingLine.setHandlingCharge(extnEle.getAttribute("HandlingCharge"));
			
		}
		
		
		NodeList pricelistHeadernodes = element.getElementsByTagName("PricelistHeader");
		
		for(int i=0;i<pricelistHeadernodes.getLength();i++)
		{
			Element priceListHeaderEle = (Element)pricelistHeadernodes.item(i);
			
			pricingLine.setHeaderKey(priceListHeaderEle.getAttribute("PricelistHeaderKey"));
			pricingLine.setListName(priceListHeaderEle.getAttribute("PricelistName"));
		}
		
		

		
		pricingLine.setItemId(element.getAttribute("ItemID"));
		pricingLine.setListPrice(element.getAttribute("ListPrice"));
		pricingLine.setStatus(element.getAttribute("PricingStatus"));
		pricingLine.setUnitOfMeasure(element.getAttribute("UnitOfMeasure"));
		
		
		return pricingLine;
		
		
	}
	
}

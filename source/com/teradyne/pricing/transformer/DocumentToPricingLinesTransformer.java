package com.teradyne.pricing.transformer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.teradyne.pricing.model.PricingLines;

public class DocumentToPricingLinesTransformer {

	public PricingLines transform(Document document)
	{
		
		Element root = document.getDocumentElement();
		
		NodeList apiNodes = root.getElementsByTagName("API");
		
		Element apiEle = null;
		for(int i=0;i<apiNodes.getLength();i++)
		{
			apiEle = (Element)apiNodes.item(i);
			
			if(apiEle.getAttribute("Name").equals("managePricelistLine"))
			{
				System.out.println("found managePricelistLine");
				break;
			}
		
		}
		
		NodeList inputNodes = apiEle.getElementsByTagName("Input");
		
		Element inputEle = null;
		for(int i=0;i<inputNodes.getLength();i++)
		{
			inputEle = (Element)inputNodes.item(i);
			
			break;
		
		}
		
		NodeList pricingListNodes = inputEle.getElementsByTagName("PricelistLineList");
		
		Element pricingListEle = null;
		for(int i=0;i<pricingListNodes.getLength();i++)
		{
			pricingListEle = (Element)pricingListNodes.item(i);
			
			break;
		
		}
		
		PricingLines pricingLines = new PricingLines();
		
		
		pricingLines.setOrganizationCode(pricingListEle.getAttribute("OrganizationCode"));
		
	
		ElementToPricingLineTransformer transformer = new ElementToPricingLineTransformer();
		NodeList pricingLineNodes = pricingListEle.getElementsByTagName("PricelistLine");
		
		for(int i=0;i<pricingLineNodes.getLength();i++)
		{
			Element pricingLineEle = (Element)pricingLineNodes.item(i);
			pricingLines.getPriceLines().add(transformer.transform(pricingLineEle));
			
			
		
		}
		
		pricingLines.setStorage(transformElementToDocument(pricingListEle));
		
		
		
		return pricingLines;
	}
	
	
	private Document transformElementToDocument(Element element) 
	{
		
		DocumentBuilderFactory docBuilderFactory =DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder =null;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			//throw new TransformerException();	
		}
		Document doc = docBuilder.newDocument();
		
		Node node = element.cloneNode(true);
		//Element rootElement = doc.createElement("Order");
		
		doc.adoptNode(node);
		doc.appendChild(node);
		
		return doc;
		
	}
	
}

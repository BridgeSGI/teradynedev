package com.teradyne.boservices;

import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;


public class ActiveSystemSerialNoServiceType_Changes {

	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	public Document activeSystemSerialNoServiceType(YFSEnvironment env,	Document inDoc) {
		
		String sSystemSerialNo="";
		String sOrderLineKey="";
		String sExpDate="";
		String sItemType="";
		String sItemID="";
		String sOrderNo="";
		String sRespProdMfgDivCode="";
		String sSystemTesterGroup="";
		String sSystemPTExceptionFlag="";
		String sSystemType="";
		String sNonrepairPartsCovered="";
		String sRepairablePartsCovered="";
		String sCovFlag="";
		String stringc="";
		String stringd="";
		String sMaxLineStatus="";
		
		YFCDocument outDoc = YFCDocument.createDocument("Output");
		java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");

		Document outDoc1 =null;
		Document outDoc2=null;
		Document terOutDoc=null;
		try{
		
		YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleInput = inYFCDoc.getDocumentElement();
			
		if (eleInput != null) {
			
			YFCIterable<YFCElement> allInputline = eleInput.getChildren();
			for (YFCElement inputEle : allInputline){
				if(inputEle.hasAttribute("SystemSerialNo")){
					sSystemSerialNo=inputEle.getAttribute("SystemSerialNo");	
				}
				
				if(sSystemSerialNo !=null){
					
					final YFCDocument inputDoc = YFCDocument.getDocumentFor("<OrderLine DocumentType=\"\" StatusQryType=\"\" ToStatus=\"\"> <Extn SystemSerialNo=\"\"/></OrderLine>");
					inputDoc.getDocumentElement().setAttribute("DocumentType", "0017.ex");
					inputDoc.getDocumentElement().setAttribute("StatusQryType", "BETWEEN");
					inputDoc.getDocumentElement().setAttribute("ToStatus", "1100.30");
					inputDoc.getDocumentElement().getChildElement("Extn").setAttribute("SystemSerialNo", sSystemSerialNo);
					
					final YFCDocument templateDoc = YFCDocument.getDocumentFor("<OrderLineList><OrderLine OrderLineKey=\"\"/></OrderLineList>");
					env.setApiTemplate("getOrderLineList", templateDoc.getDocument());
					outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,"getOrderLineList",inputDoc.getDocument());
					System.out.println("######### Output of OrderlineList--------->"+outDoc1.toString());
					YFCElement sOrderLineList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
					
					if(!YFCElement.isVoid(sOrderLineList)){
						YFCIterable<YFCElement> allOrderLines = sOrderLineList.getChildren();
						for (YFCElement sOrderLineEle : allOrderLines){
							sOrderLineKey = sOrderLineEle.getAttribute("OrderLineKey");
							System.out.println("#########OrderLineKey taken from IB");
							if(!YFCElement.isVoid(sOrderLineKey)){
								
								final YFCDocument terDoc = YFCDocument.getDocumentFor("<TerSCIBMapping TerIBOLKey=\"\" ></TerSCIBMapping>");
								terDoc.getDocumentElement().setAttribute("TerIBOLKey",sOrderLineKey);
															
								terOutDoc = YIFClientFactory.getInstance().getLocalApi().executeFlow(env,"GetIBForLine",terDoc.getDocument());
								YFCElement sTerSCIBMappingList = YFCDocument.getDocumentFor(terOutDoc).getDocumentElement();
								System.out.println("######### Called GetIBForLine");
								if(!YFCElement.isVoid(sTerSCIBMappingList)){
									
									YFCIterable<YFCElement> terLines = sTerSCIBMappingList.getChildren();
									for (YFCElement sTerSCIBLines : terLines){
										YFCElement sSCLines = sTerSCIBLines.getChildElement("YFSSCOrderLine");
										if(!YFCElement.isVoid(sSCLines)){
											sMaxLineStatus=sSCLines.getAttribute("MaxLineStatus");
											if(sMaxLineStatus.equals("1100.200")|| sMaxLineStatus.equals("1100.300")){
												System.out.println("######### Status Check done");
												YFCElement extnOrdSCEle = sSCLines.getChildElement("Extn");
												if(!YFCElement.isVoid(extnOrdSCEle)){
													//sSystemType= extnOrdSCEle.getAttribute("SystemType");
													sRepairablePartsCovered=extnOrdSCEle.getAttribute("RepairablePartsCovered");
													sNonrepairPartsCovered=extnOrdSCEle.getAttribute("NonrepairPartsCovered");
													if("Y".equalsIgnoreCase(sRepairablePartsCovered) && "Y".equalsIgnoreCase(sNonrepairPartsCovered))
														sCovFlag="B";
													if("Y".equalsIgnoreCase(sRepairablePartsCovered) && "N".equalsIgnoreCase(sNonrepairPartsCovered))
														sCovFlag="R";
													if("N".equalsIgnoreCase(sRepairablePartsCovered) && "Y".equalsIgnoreCase(sNonrepairPartsCovered))
														sCovFlag="C";
												}
												YFCElement ordSCEle = sSCLines.getChildElement("Order");
												if(!YFCElement.isVoid(ordSCEle)){
													sOrderNo= ordSCEle.getAttribute("OrderNo");
													String[] str_array1 = sOrderNo.split("-");
													int sLength = str_array1.length;
													if(sLength > 1){
													stringc = str_array1[0];
													stringd = str_array1[1];
													}
													if (sLength == 1){
														stringc = str_array1[0];
														stringd = "";
													}
		
									
													YFCElement extEle=ordSCEle.getChildElement("Extn");
													if(extEle !=null){
														sExpDate = extEle.getAttribute("ExpirationDate");
													}
												}
											
												if(sExpDate !=null){
													Date currentDate = java.util.Calendar.getInstance().getTime();
													Date curDate = dateFormat.parse(dateFormat.format(currentDate));;
													System.out.println("Current Date:" + currentDate) ;
													Date ExpDate = dateFormat.parse(sExpDate.substring(0, 10));
													System.out.println("ExpDate (after dateformat): " + ExpDate);								
													System.out.println("Comparing: " + curDate.compareTo(ExpDate));
													
													if ((curDate.compareTo(ExpDate) <= 0)) {
														System.out.println("Inside if condition checking date ");
														YFCElement itemDetSCEle = sSCLines.getChildElement("ItemDetails");
														if(!YFCElement.isVoid(itemDetSCEle)){
															sItemID = itemDetSCEle.getAttribute("ItemID");
															YFCElement itemExtnSCEle = itemDetSCEle.getChildElement("Extn");
															if(!YFCElement.isVoid(itemExtnSCEle)){
																sRespProdMfgDivCode=  itemExtnSCEle.getAttribute("RespProdMfgDivCode");
																sSystemTesterGroup=  itemExtnSCEle.getAttribute("SystemTesterGroup");
																sSystemPTExceptionFlag=  itemExtnSCEle.getAttribute("SystemPTExceptionFlag");
															}
															YFCElement priInfoSCEle = itemDetSCEle.getChildElement("PrimaryInformation");
																if(!YFCElement.isVoid(priInfoSCEle)){
																sItemType=  priInfoSCEle.getAttribute("ItemType");
																if("Service".equalsIgnoreCase(sItemType)){
																	
																	//To get System Type from IB
																	final YFCDocument inputDoc1 = YFCDocument.getDocumentFor("<OrderLineDetail OrderLineKey=\"\"/>");
																	inputDoc1.getDocumentElement().setAttribute("OrderLineKey", sOrderLineKey);
																													
																	final YFCDocument templateDoc1 = YFCDocument.getDocumentFor("<OrderLine OrderLineKey=\"\"><Item ItemID=\"\"/></OrderLine>");
																	env.setApiTemplate("getOrderLineDetails", templateDoc1.getDocument());
																	outDoc2 = YIFClientFactory.getInstance().getLocalApi().invoke(env,"getOrderLineDetails",inputDoc1.getDocument());
																	System.out.println("######### Output of OrderlineDetails-------->"+outDoc2.toString());
																	YFCElement sOrderLineDet = YFCDocument.getDocumentFor(outDoc2).getDocumentElement();
																	YFCElement itemDet = sOrderLineDet.getChildElement("Item");
																	if(!YFCElement.isVoid(itemDet)){
																		sSystemType= itemDet.getAttribute("ItemID");
																		System.out.println("Item ID from IB = " + sSystemType);
																	}
																
																	System.out.println("######### Output generation");
																	YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("ActiveSystemSerialNoServiceType");
																	finalOutputEle.setAttribute("Service_Type", sItemID);
																	finalOutputEle.setAttribute("Entitlement_Number", stringc);
																	finalOutputEle.setAttribute("Entitlement_Type", stringd);
																	finalOutputEle.setAttribute("Resp_Part_Mfg_Div", sRespProdMfgDivCode);
																	finalOutputEle.setAttribute("Tester_Group", sSystemTesterGroup);
																	finalOutputEle.setAttribute("PT_Exception_Flag", sSystemPTExceptionFlag);
																	finalOutputEle.setAttribute("System_Type", sSystemType);
																	finalOutputEle.setAttribute("Part_Coverage_Flag", sCovFlag);
																}
																}
															}
														}//end of if for date comp
													}
											}// end of check of status of the orderline in SC
										}
									}//end of FOR for each Service Contract OrderLines
				
								}
							}
							
						}//end of FOR  for each orderline in install Base
					}
					
					
				}
				
			}//end of FOR foe each input of SystemSerialNo
		}
		}
		catch(Exception e)
		 {
			//throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		 }
		
		

		return outDoc.getDocument();
}
}

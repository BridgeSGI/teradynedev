package com.teradyne.boservices;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GetOpenCompleteQuotes implements YIFCustomApi {

	YFCDocument inputDoc = null;
	YFCDocument inputOrder=null;
	YFCDocument inputOrganization=null;
	YFCDocument inputCurrency=null;
	YFCDocument inputPrice=null;
	Document outputOrder=null;
	Document outputOrganization=null;
	Document outputCurrency=null;
	Document outputPrice=null;
	Document outDoc5=null;
	YFCDocument finalOutput = YFCDocument.createDocument("Output");
	YFCDocument templateOrder=null;
	YFCDocument templateOrganization=null;
	YFCDocument templatePrice=null;
	YFCDocument outDoc=null;
	String apiName1="getOrderList";
	String apiName2="getOrganizationHierarchy";
	String apiName3="getCurrencyList";
	String apiName4="getPricelistLineList";
	String CustNo="";
	String ReceivingNode ="";
	String DraftFlag="";
	String CustSiteNo="";
	String OrderHold="";
	String OrderStatus="";
	String OrderNo="";
	String OrderDate="";
	String TotalAmount = "";
	String Currency = "";
	String DraftOrderFlag = "";
	String OrderType="";
	String OrgCode="";
	String ServiceTypeDescn="";
	String sReceivingNode="";
	Boolean Go=false;
	long diffInDaysOrd = 0;
	//Boolean Go2=false;
	Double OrderedQty=null;
	Double ReceivedQty=null;
	Double TotalQty=null;
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}
	public Document getOpenCompleteQuotes(YFSEnvironment env, Document inDoc) throws ParseException{
		
		//YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement inEle =  YFCDocument.getDocumentFor(inDoc).getDocumentElement();
		YFCElement Node = inEle.getChildElement("GetOpenCompleteQuotes");
		
		
		try {
			if(Node!=null){
				
				
					CustNo = inEle.getChildElement("GetOpenCompleteQuotes").getAttribute("ShipToCustomerNumber");
					CustSiteNo = inEle.getChildElement("GetOpenCompleteQuotes").getAttribute("ShipToCustomerSiteNumber");
					if (!CustNo.isEmpty() && !CustSiteNo.isEmpty())
					ReceivingNode= CustNo+"-"+CustSiteNo;
					else
					ReceivingNode=CustNo;
				
				
				
				inputOrder=YFCDocument.getDocumentFor("<Order ReceivingNodeQryType=\"FLIKE\" ReceivingNode=\"\" DocumentType=\"\" Status=\"\" />");
				inputOrder.getDocumentElement().setAttribute("ReceivingNode",ReceivingNode );
				inputOrder.getDocumentElement().setAttribute("DocumentType","0001" );
				inputOrder.getDocumentElement().setAttribute("Status","1000" );
				
				templateOrder=YFCDocument.getDocumentFor("<OrderList> <Order OrderDate=\"\" OrderNo=\"\" ReceivingNode=\"\" Status=\"\"  DraftOrderFlag=\"\" HoldFlag=\"\" EnterpriseCode=\"\" OrderType=\"\">" +
						"<PriceInfo Currency=\"\" TotalAmount=\"\" />" +
						"<OrderLines><OrderLine ReceivingNode=\"\" OrderedQty=\"\"  PrimeLineNo=\"\" > <Item ItemID=\"\" ItemShortDesc=\"\" />" +
						"<LineCharges> <LineCharge ChargePerLine=\"\" /> </LineCharges>"+
						"<Extn ServiceType=\"\" SystemType=\"\" SystemSerialNo=\"\" LeadTime=\"\" />" +
						"</OrderLine></OrderLines>" +
						"</Order></OrderList>");
				
				env.setApiTemplate(apiName1, templateOrder.getDocument());
				
				outputOrder = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputOrder.getDocument());
				if(outputOrder != null){
					
					YFCElement outputOrderEle = YFCDocument.getDocumentFor(outputOrder).getDocumentElement();
					if(outputOrderEle.getChildElement("Order")!=null){
						
						//for loop
						YFCIterable<YFCElement> allOrder = outputOrderEle.getChildren();
						for(YFCElement orderEle : allOrder){
						
						OrderNo = orderEle.getAttribute("OrderNo");
						OrderDate=orderEle.getAttribute("OrderDate");
						
						java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
						Date currentDate = java.util.Calendar.getInstance().getTime();
						currentDate = dateFormat.parse(dateFormat.format(currentDate));
						System.out.println("Order DAte ------>" + OrderDate);
						if(OrderDate !=null ){
							Date OrdDate = dateFormat.parse(OrderDate.substring(0,10));
							long diffInMillisecOrd = 0;
							diffInMillisecOrd = currentDate.getTime()- OrdDate.getTime();
							diffInDaysOrd = diffInMillisecOrd/ (24 * 60 * 60 * 1000);
							System.out.println("Diff in order date "+ diffInDaysOrd);
						
						}										
						
						sReceivingNode = orderEle.getAttribute("ReceivingNode");
						OrderType = orderEle.getAttribute("OrderType");
						OrgCode = orderEle.getAttribute("EnterpriseCode");
						OrderStatus = orderEle.getAttribute("Status");
						OrderHold = orderEle.getAttribute("HoldFlag");
						DraftOrderFlag = orderEle.getAttribute("DraftOrderFlag");
						Currency = orderEle.getChildElement("PriceInfo").getAttribute("Currency");
						TotalAmount = orderEle.getChildElement("PriceInfo").getAttribute("TotalAmount");
						//OrderType.equals("Q") , && !OrderHold.equals("Y") check removed 
						if( !OrderStatus.equalsIgnoreCase("cancelled") ){
							inputOrganization=YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\" />");
							inputOrganization.getDocumentElement().setAttribute("OrganizationCode", sReceivingNode);
								
							templateOrganization=YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"> <Extn CustomerSiteStatus=\"\"/></Organization>");
							env.setApiTemplate(apiName2, templateOrganization.getDocument());
							outputOrganization = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName2, inputOrganization.getDocument());
							if(YFCDocument.getDocumentFor(outputOrganization).getDocumentElement().getChildElement("Extn").hasAttribute("CustomerSiteStatus")){
								if(YFCDocument.getDocumentFor(outputOrganization).getDocumentElement().getChildElement("Extn").getAttribute("CustomerSiteStatus").equals("A")){
									Go=true;
								}
								else
									Go=false;
							
							
							}
						}
					
					
					if(Go){
						
					YFCNodeList<YFCElement> OrderNodes = orderEle.getChildElement("OrderLines").getElementsByTagName("OrderLine");
					int OrderLength= OrderNodes.getLength();
					for (int i=0;i<OrderLength;i++){

						YFCElement OrderNode = OrderNodes.item(i);
						String ServiceType=  "";
						String LeadTime=  "";
						int leadtTimeint=0;
						String CurrencyDescription=  "";
						String UnitPriceRepair=  "";
						String UnitPriceExpediting = "";
						String ServiceTypeDescription =  "";
						String SystemType = "";
						String SystemSerialNo = "";
						String ChargePerLine="";
						double chargePerLineDouble=0.00;
				
						String OrderLineNumber = OrderNode.getAttribute("PrimeLineNo");
						String ItemID = OrderNode.getChildElement("Item").getAttribute("ItemID");
						String QuantityOrdered = OrderNode.getAttribute("OrderedQty");
					//  ServiceTypeDescription = OrderNode.getChildElement("Item").getAttribute("ItemShortDesc");
					//	String Currency = OrderNode.getAttribute("Currency");
					//	String TotalPrice = OrderNode.getAttribute("TotalPrice");
						if(OrderNode.getChildElement("Extn")!=null){
							YFCElement Extn= OrderNode.getChildElement("Extn");
							
							ServiceType= Extn.getAttribute("ServiceType");
							if(ServiceType != null && !ServiceType.isEmpty()){
								String apiName1 = "getCommonCodeList";
								YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" OrganizationCode=\"\"/>");
								inputDoc.getDocumentElement().setAttribute("CodeValue", ServiceType);
								inputDoc.getDocumentElement().setAttribute("CodeType", "Service_Type");
								inputDoc.getDocumentElement().setAttribute("OrganizationCode", OrgCode);
								outDoc5 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
								env.clearApiTemplate(apiName1);
								
								if (outDoc5 !=null){
									YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc5).getDocumentElement();
									if (OutputCommList != null){
										YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
										if(eleComm !=null){
											ServiceTypeDescription= eleComm.getAttribute("CodeShortDescription");
											System.out.println("Serv Desc@@@@@@@@@@ "+ServiceTypeDescn);
										}
									}
								}
							}
							
							SystemType = Extn.getAttribute("SystemType");
							SystemSerialNo = Extn.getAttribute("SystemSerialNo");
							LeadTime = Extn.getAttribute("LeadTime");
							if(!XmlUtils.isVoid(LeadTime))
							leadtTimeint= Integer.parseInt(LeadTime);	
							System.out.println("Lead Time :" + leadtTimeint);
						}
						if (!Currency.isEmpty()){
						inputCurrency = YFCDocument.createDocument("Currency");
						inputCurrency.getDocumentElement().setAttribute("Currency", Currency);
						
						outputCurrency = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName3, inputCurrency.getDocument());
						if(YFCDocument.getDocumentFor(outputCurrency).getDocumentElement().getChildElement("Currency")!=null){
							CurrencyDescription = YFCDocument.getDocumentFor(outputCurrency).getDocumentElement().getChildElement("Currency").getAttribute("CurrencyDescription");
						}
						}
						
						YFCElement lineChargesEle= OrderNode.getChildElement("LineCharges");
						if(!YFCElement.isVoid(lineChargesEle)){
							YFCElement lineChargeEle= lineChargesEle.getChildElement("LineCharge");
							if(!YFCElement.isVoid(lineChargeEle)){
								ChargePerLine = lineChargeEle.getAttribute("ChargePerLine");
								if(!XmlUtils.isVoid(ChargePerLine))
								chargePerLineDouble = Double.parseDouble(ChargePerLine);
								System.out.println("chargePerLineDouble :" + chargePerLineDouble);
							}
						}
						
						inputPrice=YFCDocument.getDocumentFor("<PricelistLine ItemID=\"\"><Item OrganizationCode=\"\"/></PricelistLine>");
						inputPrice.getDocumentElement().setAttribute("ItemID", ItemID);
						inputPrice.getDocumentElement().getChildElement("Item").setAttribute("OrganizationCode",OrgCode);
						
						
						templatePrice=YFCDocument.getDocumentFor("<PricelistLineList><PricelistLine ListPrice=\"\" ><Extn ExpiditingPrice=\"\" /></PricelistLine></PricelistLineList>");
						env.setApiTemplate(apiName4, templatePrice.getDocument());
						outputPrice = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName4, inputPrice.getDocument());
						
						YFCElement priceListEle = YFCDocument.getDocumentFor(outputPrice).getDocumentElement();
						if (!YFCElement.isVoid(priceListEle)){
							YFCElement priceLineEle = priceListEle.getChildElement("PricelistLine");
							if(!YFCElement.isVoid(priceLineEle)){
								UnitPriceRepair = priceLineEle.getAttribute("ListPrice");
								YFCElement priceLineExtnEle = priceLineEle.getChildElement("Extn");
								if(!YFCElement.isVoid(priceLineExtnEle))
								UnitPriceExpediting = priceLineExtnEle.getAttribute("ExpiditingPrice");
							}
						}
						
					    //adding the conditions leadtime>0 and chargeperline >0 then check if sysdate-orderdate <=60
						if(leadtTimeint > 0 && chargePerLineDouble > 0 &&  diffInDaysOrd <= 60 )
						{
						YFCDocument Output = YFCDocument.createDocument("GetOpenCompleteQuotes");
						Output.getDocumentElement().setAttribute("OrderNumber", OrderNo);
						Output.getDocumentElement().setAttribute("OrderLineNumber", OrderLineNumber);
						Output.getDocumentElement().setAttribute("ServiceType", ServiceType);
						Output.getDocumentElement().setAttribute("ServiceTypeDescription", ServiceTypeDescription);
						Output.getDocumentElement().setAttribute("PartNumber", ItemID);
						Output.getDocumentElement().setAttribute("QuantityOrdered", QuantityOrdered);
						Output.getDocumentElement().setAttribute("SystemType", SystemType);
						Output.getDocumentElement().setAttribute("SystemSerialNumber", SystemSerialNo);
						Output.getDocumentElement().setAttribute("QuoteConversionIndicator", DraftOrderFlag);
						Output.getDocumentElement().setAttribute("LeadTime", LeadTime);
						Output.getDocumentElement().setAttribute("CurrencyCode", Currency);
						Output.getDocumentElement().setAttribute("CurrencyCodeDescription", CurrencyDescription);
						Output.getDocumentElement().setAttribute("TotalPrice", TotalAmount);
						Output.getDocumentElement().setAttribute("UnitPriceRepair", UnitPriceRepair);
						Output.getDocumentElement().setAttribute("UnitPriceExpediting", UnitPriceExpediting);
						String[] str_array1 = sReceivingNode.split("-");

						String c = str_array1[0];
						String d = str_array1[1];
						Output.getDocumentElement().setAttribute("ShipToCustomerNumber", c);
						Output.getDocumentElement().setAttribute("ShipToCustomerSiteNumber", d);
						
						finalOutput.getDocumentElement().addXMLToNode(Output.getDocumentElement().toString());
						}
					}
				}
			}
		}
	}
					
		}
	}catch (YFSException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (YIFClientCreationException e) {
			// TODO Auto-generated catch block
			//throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		}
		return finalOutput.getDocument();
	}

}

package com.teradyne.boservices;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
//import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetOverDueQtyAndCustomerStatus implements YIFCustomApi {

	YFCDocument inYFCDoc = null;
	YFCElement inEle = null;
	int NodeLength = 0;
	YFCDocument inputOrder = null;
	YFCDocument inputOrganization = null;
	YFCDocument inputCustomer = null;
	Document outputOrder = null;
	Document outputOrganization = null;
	Document outputCustomer = null;
	YFCDocument finalOutput = null;
	YFCDocument templateOrder = null;
	YFCDocument templateOrganization = null;
	YFCDocument templateCustomer = null;
	YFCDocument outDoc = null;
	Double TotalReceiptDueQuantity = null;
	String TotalReceiptDueQuantityString = "";
	String apiName1 = "getOrderList";
	String apiName2 = "getOrganizationHierarchy";
	String apiName3 = "getCustomerList";
	String CustNo = "";
	String CustSiteNo = "";
	String IsCreditHold = "";
	String IsServiceHold = "";
	String OrderType = "";
	String OrgCode = "";
	//Boolean Go = false;
	//Boolean Go2 = false;
	Double OrderedQty = null;
	Double ReceivedQty = null;
	Double TotalQty = null;
	String receivingNode = "";
	YFCDocument out = YFCDocument.createDocument("Output");

	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	public Document getOverDueQtyAndCustomerStatus(YFSEnvironment env,
			Document inDoc) {
		try{
		//YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement inEle = YFCDocument.getDocumentFor(inDoc)
				.getDocumentElement();
		YFCElement Node = inEle.getChildElement("GetOverDueQtyAndCustomerStatus");
		outDoc = YFCDocument.createDocument("Output");

			if (Node != null) {
				if (Node.hasAttribute("CustomerNumber")
						&& Node.hasAttribute("CustomerSiteNumber")) {
					CustNo = Node.getAttribute("CustomerNumber");
					CustSiteNo = Node.getAttribute("CustomerSiteNumber");
				}
				inputOrder = YFCDocument.createDocument("Order");
				YFCElement inputOrderEle = inputOrder.getDocumentElement();

				inputOrderEle.setAttribute("ReceivingNode", CustNo+"-"+CustSiteNo);
				inputOrderEle.setAttribute("DocumentType", "0003");
				
				YFCElement orderLineEle = inputOrderEle.createChild("OrderLine");
				YFCElement itemEle = orderLineEle.createChild("Item");
				itemEle.setAttribute("ProductClass", "DEFECTIVE");
				
				templateOrder = YFCDocument
						.getDocumentFor("<OrderList> <Order  EnterpriseCode=\"\" OrderType=\"\" ReceivingNode=\"\">"
								+ "<OrderLines><OrderLine ReceivedQty=\"\" OrderedQty=\"\" OriginalOrderedQty=\"\" Status=\"\" >"
								+ "</OrderLine></OrderLines>"
								+ "</Order></OrderList>");

				env.setApiTemplate(apiName1, templateOrder.getDocument());

				outputOrder = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputOrder.getDocument());
				System.out.println("Order Returned====================="+outputOrder);
				
				if (outputOrder != null) {
					
					YFCElement outputOrderEle = YFCDocument.getDocumentFor(outputOrder).getDocumentElement();
					System.out.println("Order Returned====================="+outputOrderEle);
					if (outputOrderEle != null) {
						
					YFCIterable<YFCElement> allOrder = outputOrderEle.getChildren();
					
					for (YFCElement orderLine : allOrder) {
						
						//YFCElement eleOrder = orderLine.getChildElement("Order");
						if (orderLine != null) {
							OrderType = orderLine.getAttribute("OrderType");
							OrgCode = orderLine.getAttribute("EnterpriseCode");
							receivingNode = orderLine.getAttribute("ReceivingNode");
							System.out.println("Receiving Node:::::::::::::"+receivingNode);
						}
						
						YFCElement eleOrderLines = orderLine.getChildElement("OrderLines");
						if (eleOrderLines != null) {
							
							//YFCElement eleOrderLn = eleOrderLines.getChildElement("OrderLine");
							YFCIterable<YFCElement> allOrderLines = eleOrderLines.getChildren();
							
							for (YFCElement orderLines : allOrderLines){
								
							//if(eleOrderLn !=null){
		
					
								TotalQty = Double.parseDouble(orderLines.getAttribute("OriginalOrderedQty"));
								OrderedQty = Double.parseDouble(orderLines.getAttribute("OrderedQty"));
								ReceivedQty = Double.parseDouble(orderLines.getAttribute("ReceivedQty"));
								TotalReceiptDueQuantity = OrderedQty - ReceivedQty;
								if (OrderedQty.compareTo(ReceivedQty) > 0 && OrderedQty.compareTo(TotalQty) == 0 && TotalReceiptDueQuantity > 0) {
									
									System.out.println("After the checking of qty:::::::::::::");
									inputOrganization = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\" />");
									inputOrganization.getDocumentElement().setAttribute("OrganizationCode", receivingNode);

									templateOrganization = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"> <Extn CustomerSiteStatus=\"\"/></Organization>");
									env.setApiTemplate(apiName2,templateOrganization.getDocument());
									outputOrganization = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName2,inputOrganization.getDocument());
						
									if (YFCDocument.getDocumentFor(outputOrganization).getDocumentElement().getChildElement("Extn").hasAttribute("CustomerSiteStatus")) {
										if (YFCDocument.getDocumentFor(outputOrganization).getDocumentElement().getChildElement("Extn").getAttribute("CustomerSiteStatus").equals("A")) {
												
							
											TotalReceiptDueQuantityString = String.valueOf(OrderedQty- ReceivedQty);
											inputCustomer = YFCDocument.getDocumentFor("<Customer CustomerID=\"\" />");
											inputCustomer.getDocumentElement().setAttribute("CustomerID", receivingNode);
											templateOrganization = YFCDocument.getDocumentFor("<CustomerList><Customer CustomerID=\"\" ><Extn IsCreditHold=\"\" IsServiceHold=\"\" /></Customer></CustomerList>");
											env.setApiTemplate(apiName3,templateOrganization.getDocument());
											outputOrganization = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName3, inputCustomer.getDocument());

											if (outputOrganization != null) {
												if(YFCDocument.getDocumentFor(outputOrganization).getDocumentElement().getChildElement("Customer")!=null){
													YFCElement Extn = YFCDocument.getDocumentFor(outputOrganization).getDocumentElement().getChildElement("Customer").getChildElement("Extn");
										
													if (Extn != null) {
														IsCreditHold = Extn.getAttribute("IsCreditHold");
														IsServiceHold = Extn.getAttribute("IsServiceHold");
													}
										
												} 
						
											}

											finalOutput = YFCDocument.createDocument("GetOverDueQtyAndCustomerStatus");
											finalOutput.getDocumentElement().setAttribute("IsCreditHold", IsCreditHold);
											finalOutput.getDocumentElement().setAttribute("IsServiceHold", IsServiceHold);
											finalOutput.getDocumentElement().setAttribute("TotalReceiptDueQuantity",TotalReceiptDueQuantityString);
											outDoc.getDocumentElement().addXMLToNode(finalOutput.getDocumentElement().toString());
										}

									}
								}
								//}
							}// for each orderlines
							}
						}//for each orders
					}
			}
			}
		
		System.out.println("Output:::" + outDoc.toString());
		
		HashMap<String, YFCElement> unique = new HashMap<String,YFCElement>();
		YFCElement outputEle = outDoc.getDocumentElement();
		System.out.println("Output Element:::" + outputEle.toString());
		YFCIterable<YFCElement> allInputline = outputEle.getChildren();
		for (YFCElement inputEle : allInputline){
			String S1 = inputEle.getAttribute("IsCreditHold")+":"+inputEle.getAttribute("IsServiceHold")+":"+ inputEle.getAttribute("TotalReceiptDueQuantity");
			unique.put(S1, inputEle);
			
		}
		 
		YFCElement OutOrderEle = out.getDocumentElement();
		 for (Map.Entry<String, YFCElement> entry : unique.entrySet()) {
			 	
			 System.out.println("Inside the distict thing");
		        String key = entry.getKey();
		        YFCElement value = entry.getValue();
		        YFCElement temp = OutOrderEle.createChild("GetOverDueQtyAndCustomerStatus");
		        temp.setAttribute("IsCreditHold", value.getAttribute("IsCreditHold"));
		        temp.setAttribute("IsServiceHold", value.getAttribute("IsServiceHold"));
		        temp.setAttribute("TotalReceiptDueQuantity", value.getAttribute("TotalReceiptDueQuantity"));
		 
		    }

}
		catch(Exception ex){
			//throw new YFSException("Record Not Found", "BOSERVICES-01", "Exception Thrown");
		}
	
			return out.getDocument();
		

}
}

package com.teradyne.boservices;

import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class IsOrderDuplicate {

	java.text.DateFormat dateFormat = new java.text.SimpleDateFormat(
			"yyyy-MM-dd");
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}
	
	public Document isOrderDuplicate(YFSEnvironment env,Document inDoc) {
		Document outDoc = null;
		YIFApi yifc;
		try {
			
			String apiName1="multiApi";
			yifc = YIFClientFactory.getInstance().getLocalApi();
			YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
			// YFCElement IterEle = null;
			System.out.println("Invoking API...MultiAPI");
			outDoc = yifc.invoke(env, apiName1, inputDoc.getDocument());

			YFCNodeList<YFCElement> NodeList = YFCDocument.getDocumentFor(
					outDoc).getElementsByTagName("OrderLine");
			for (int i = 0; i < NodeList.getLength(); i++) {
				System.out.println("Iteration: " + i);
				YFCNode cNode = NodeList.item(i);
				YFCElement element = (YFCElement) cNode;
				System.out.println(element.toString());

				String sOrdDate = element.getChildElement("Order").getAttribute("OrderDate");
				System.out.println("sOrdDate: " + sOrdDate);

				Date currentDate = java.util.Calendar.getInstance().getTime();
				currentDate = dateFormat.parse(dateFormat.format(currentDate));
				Date OrdDate = dateFormat.parse(sOrdDate.substring(0,10));
				long diffInMillisecOrd = 0;
				long diffInDaysOrd = 0;
				diffInMillisecOrd = currentDate.getTime()- OrdDate.getTime();
				diffInDaysOrd = diffInMillisecOrd/ (24 * 60 * 60 * 1000);
								
				if (diffInDaysOrd>7) {
					System.out.println("Inside if condition");
					element.getParentNode().removeChild(cNode);
					i--;
				}

			}

			

		}

		catch (Exception exception) {
			//throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}

		return outDoc;
	}
}




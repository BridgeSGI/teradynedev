package com.teradyne.boservices;

//import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

//import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
//import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class DefReceiptDueBackToTer implements YIFCustomApi {

	YFCDocument outDoc = YFCDocument.createDocument("Output");
	YFCDocument inYFCDoc = null;
	YFCElement inEle = null;
	int NodeLength = 0;
	YFCDocument inputOrder = null;
	YFCDocument inputOrganization = null;
	Document outputOrder = null;
	Document outputOrganization = null;
	YFCDocument finalOutput = null;
	YFCDocument templateOrder = null;
	YFCDocument templateOrganization = null;
	YFCDocument templateCustomer = null;
	Document outDoc4 = null;

	String apiName1 = "getOrderList";
	String apiName2 = "getOrganizationHierarchy";
	String apiName3 ="getCommonCodeList";
	String PartyNo = "";
	String PartySiteNo = "";
	String ServiceType = "";
	String IsCreditHold = "";
	String IsServiceHold = "";
	String OrderType = "";
	String OrgCode = "";

	Double OrderedQty = null;
	Double ReceivedQty = null;
	Double TotalQty = null;
	Double TotalReceiptDueQuantity = null;	
	String status="";
	String ShipToID = "";
	String orderedQty = "";
	String primeLineNo = "";
	String itemShortDesc = "";
	String shortDescription = "";
	String itemId = "";
	String receivingNode = "";
	String serviceType = "";
	String organizationName ="";
	//String sBuyerOrganizationCode="";
	String stringa="";
	String stringb="";
	String billCity = "";
	String billCountry = "";
	String billstate="";
	String orderDate = "";
	String orderNo = "";
	String siteNo = "";
	String dueQuantity = "";
	String customerNo ="";
	String ShipNode="";
	String contactName = "";
	String Buyer="";
	String CustomerSiteStatus="";
	String ServiceTypeCodeDesc="";
	String getOrganizationHierarchyApi="getOrganizationHierarchy";
	Document outputOrg = null;
	YFCDocument TemplateOrganization = null;
	YFCDocument inputOrg = null;
	String productClass="";
	String shipname="";
	String minLineStatus="";
	Boolean expressFlag= false;
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	public Document defReceiptDueBackToTer(YFSEnvironment env, Document inDoc) {

		try{
		YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement inEle = inYFCDoc.getDocumentElement();
		YFCElement Node = inEle.getChildElement("DefReceiptDueBackToTer");
		
		
		
			if (Node != null) {
				if (Node.hasAttribute("PartyNumber") && Node.hasAttribute("PartyAddressNumber")) {
					PartyNo = Node.getAttribute("PartyNumber");
					PartySiteNo = Node.getAttribute("PartyAddressNumber");
					Buyer= PartyNo + "-" + PartySiteNo;
					ServiceType = Node.getAttribute("ServiceType");
				}
				if(Node.hasAttribute("PartyNumber") && Node.getAttribute("PartyAddressNumber").isEmpty()){
					PartyNo = Node.getAttribute("PartyNumber");
					Buyer = PartyNo + "-";
					ServiceType = Node.getAttribute("ServiceType");
				}

				inputOrder = YFCDocument
						.getDocumentFor("<Order BuyerOrganizationCodeQryType=\"FLIKE\" BuyerOrganizationCode=\"\" DocumentType=\"\">"
											+ "<OrderLine> "
												+ "<Extn ServiceType=\"\"/>"
											+"</OrderLine>"
										+ "</Order>");
				inputOrder.getDocumentElement().setAttribute("BuyerOrganizationCode", Buyer);
				inputOrder.getDocumentElement().setAttribute("DocumentType", "0003");
				inputOrder.getDocumentElement().getChildElement("OrderLine").getChildElement("Extn").setAttribute("ServiceType", ServiceType);

				
				templateOrder = YFCDocument
						.getDocumentFor("<OrderList>"
											+"<Order OrderNo=\"\" BuyerOrganizationCode=\"\" EnterpriseCode=\"\" OrderType=\"\" OrderDate=\"\"><Extn ContactName=\"\"/>"	
												+"<OrderLines>"
												+"<OrderLine MinLineStatus=\"\" ShipNode=\"\" ShipToID=\"\" ReceivedQty=\"\" OrderedQty=\"\" OriginalOrderedQty=\"\" Status=\"\" PrimeLineNo=\"\" ReceivingNode=\"\">"
														+"<Item ItemShortDesc=\"\" ProductClass=\"\" />"
														+"<ItemDetails ItemID=\"\">"
															+"<PrimaryInformation ShortDescription=\"\"/>"
														+"</ItemDetails>"
														+"<Extn ServiceType=\"\"/>"
													+"</OrderLine>"
												+"</OrderLines>"
											+"</Order>"
										+"</OrderList>");

				env.setApiTemplate(apiName1, templateOrder.getDocument());

				outputOrder = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputOrder.getDocument());
				if (outputOrder != null) {
					YFCElement outputOrderEle = YFCDocument.getDocumentFor(outputOrder).getDocumentElement();
									
					YFCIterable<YFCElement> allOrderList = outputOrderEle.getChildren();
					
					for (YFCElement orderEle : allOrderList) {
					
					
						OrderType = orderEle.getAttribute("OrderType");
						OrgCode = orderEle.getAttribute("BuyerOrganizationCode");
						orderDate = orderEle.getAttribute("OrderDate");
						orderNo = orderEle.getAttribute("OrderNo");
						YFCElement extnEle = (YFCElement) orderEle.getChildElement("Extn");
						if(extnEle != null){
							contactName=extnEle.getAttribute("ContactName");
						}
				
						YFCElement OrderLinesEle = (YFCElement) orderEle.getChildElement("OrderLines");
						YFCIterable<YFCElement> allOrdLines = OrderLinesEle.getChildren();
						for (YFCElement orderLineEle : allOrdLines){
			
							ShipNode = orderLineEle.getAttribute("ReceivingNode");
							//OrgCode=orderLineEle.getAttribute("ShipNode");
							ShipToID=orderLineEle.getAttribute("ShipToID");
							if(ShipNode !=null){
								inputOrg = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"/>");
								inputOrg.getDocumentElement().setAttribute("OrganizationCode", ShipNode);
								
								TemplateOrganization = YFCDocument.getDocumentFor("<Organization OrganizationName=\"\"><CorporatePersonInfo  City=\"\" State=\"\" ZipCode=\"\" Country=\"\"/></Organization>");
								env.setApiTemplate(getOrganizationHierarchyApi,TemplateOrganization.getDocument());
								outputOrg = YIFClientFactory.getInstance().getLocalApi().invoke(env, getOrganizationHierarchyApi,inputOrg.getDocument());
								if (outputOrganization != null) {
									System.out.println("###########"+ outputOrg);
									YFCElement OutputOrganizationElementList = YFCDocument.getDocumentFor(outputOrg).getDocumentElement();
									if (OutputOrganizationElementList != null){
										shipname = OutputOrganizationElementList.getAttribute("OrganizationName");
										
									YFCElement corpEle = OutputOrganizationElementList.getChildElement("CorporatePersonInfo");
									if(corpEle != null){
										billCity=corpEle.getAttribute("City");
										billstate=corpEle.getAttribute("State");
										billCountry=billCity +","+billstate;
									}
								}
								}
						
							}
							minLineStatus= orderLineEle.getAttribute("MinLineStatus");
							int statusId = Integer.parseInt(minLineStatus);
							primeLineNo=orderLineEle.getAttribute("PrimeLineNo");
							if(orderLineEle.getChildElement("Extn").hasAttribute("ServiceType"))
								serviceType=orderLineEle.getChildElement("Extn").getAttribute("ServiceType");
							
								if("B20".equalsIgnoreCase(serviceType) || "MPS".equalsIgnoreCase(serviceType) || "BPS".equalsIgnoreCase(serviceType)
									|| "RPD".equalsIgnoreCase(serviceType) ||"EPS".equalsIgnoreCase(serviceType) || "SDS".equalsIgnoreCase(serviceType) || 
									"ECAL".equalsIgnoreCase(serviceType) || "EAR".equalsIgnoreCase(serviceType) ){
									expressFlag= true;
								}
							if(serviceType != null && !serviceType.isEmpty()){
								YFCDocument inputDoc4 = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" OrganizationCode=\"\" />");
							
							  	inputDoc4.getDocumentElement().setAttribute("CodeType", "Service_Type");
								inputDoc4.getDocumentElement().setAttribute("CodeValue", serviceType);
								inputDoc4.getDocumentElement().setAttribute("OrganizationCode", "CSO");
								outDoc4 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName3, inputDoc4.getDocument());
								env.clearApiTemplate(apiName3);
								
								if(outDoc4 !=null){
									YFCElement OutCommList = YFCDocument.getDocumentFor(outDoc4).getDocumentElement();
									if(OutCommList.hasChildNodes()){
										YFCElement outComm = OutCommList.getChildElement("CommonCode");
										ServiceTypeCodeDesc =outComm.getAttribute("CodeShortDescription");
										System.out.println("ServiceTypeCodeDesc#########" +ServiceTypeCodeDesc);
									}
								}
							}
							YFCElement itemEle = (YFCElement) orderLineEle.getChildElement("Item");
							if(itemEle !=null){
							itemShortDesc = itemEle.getAttribute("ItemShortDesc");
							productClass = itemEle.getAttribute("ProductClass");
							System.out.println("PDT Class" + productClass);
							}
							YFCElement itemDetEle = (YFCElement) orderLineEle.getChildElement("ItemDetails");
							if(itemDetEle !=null){
							itemId =itemDetEle.getAttribute("ItemID");
							shortDescription = itemDetEle.getChildElement("PrimaryInformation").getAttribute("ShortDescription");
							}
							TotalQty = Double.parseDouble(orderLineEle.getAttribute("OriginalOrderedQty"));
							OrderedQty = Double.parseDouble(orderLineEle.getAttribute("OrderedQty"));
							ReceivedQty = Double.parseDouble(orderLineEle.getAttribute("ReceivedQty"));
							status = orderLineEle.getAttribute("Status");
							TotalReceiptDueQuantity = OrderedQty - ReceivedQty;
							dueQuantity = String.valueOf(TotalReceiptDueQuantity);
							
							inputOrganization = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\" />");
							inputOrganization.getDocumentElement().setAttribute("OrganizationCode", OrgCode);
							
							templateOrganization = YFCDocument.getDocumentFor("<Organization OrganizationName=\"\">" 
														+"<Extn CustomerSiteStatus=\"\"/></Organization>");
							env.setApiTemplate(apiName2,templateOrganization.getDocument());
							outputOrganization = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName2,inputOrganization.getDocument());
						    
							if(outputOrganization != null){
								YFCElement orgEle = YFCDocument.getDocumentFor(outputOrganization).getDocumentElement();
								if(orgEle != null){
									organizationName= orgEle.getAttribute("OrganizationName");
								}
								YFCElement extnorgEle = orgEle.getChildElement("Extn");
								if(extnorgEle !=null){
									CustomerSiteStatus=extnorgEle.getAttribute("CustomerSiteStatus");
								}
							
							if ("A".equals(CustomerSiteStatus)&& OrderedQty.compareTo(ReceivedQty) > 0 && OrderedQty.compareTo(TotalQty) == 0
									 && TotalReceiptDueQuantity > 0 && expressFlag && !"DEFECTIVE".equalsIgnoreCase(productClass) && statusId<= 3700 || "DOA".equalsIgnoreCase(OrderType)) {
											
										System.out.println("In the output generatio");							
										YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("DefReceiptDueBackToTer");
										finalOutputEle.setAttribute("OrderNo", orderNo);
										
										finalOutputEle.setAttribute("OrderLine", primeLineNo);
										finalOutputEle.setAttribute("OrderDueQty", dueQuantity);
										String[] str_array = OrgCode.split("-");
										int sLength = str_array.length;
										if(sLength > 1){
										stringa = str_array[0];
										stringb = str_array[1];
										}
										if (sLength == 1){
											stringa = str_array[0];
											stringb = "";
										}
										
										finalOutputEle.setAttribute("CustomerNumber", stringa);
										finalOutputEle.setAttribute("SiteNumber", stringb);
										finalOutputEle.setAttribute("CustomerName", organizationName);
										finalOutputEle.setAttribute("OrderContactName", contactName);
										String oDate= modifyDateLayout(orderDate);
										finalOutputEle.setAttribute("OrderDate", oDate);
										finalOutputEle.setAttribute("ServiceType", serviceType);
										finalOutputEle.setAttribute("ServiceTypeDescription", ServiceTypeCodeDesc);
										finalOutputEle.setAttribute("ServiceCenter", ShipToID);
										finalOutputEle.setAttribute("ResponsibleCenter", ShipNode);
										finalOutputEle.setAttribute("ServiceCenterName", shipname);
										finalOutputEle.setAttribute("City", billCity);

										finalOutputEle.setAttribute("Location", billCountry);

										finalOutputEle.setAttribute("DisplayPartNumber", itemId);
										finalOutputEle.setAttribute("PartDescription", shortDescription);
										finalOutputEle.setAttribute("LineStatusDerived", status);
										
									
							
								}
							
							}
							}//end of order lines
						}//end of orders
					}// if end for orderlist
				}// input if


				}//try
		

		catch(Exception ex){
			ex.printStackTrace();
			System.out.println("######### Exception Caught : " + ex.toString());
			throw new YFSException("Exception Thrown", "E-01", "Exception Thrown");
		}
		return outDoc.getDocument();
	}
	
	public String modifyDateLayout(String inputDate) throws Exception{
		String newDateString = "";
		System.out.println("DateInput to the method----------->" + inputDate);
		if(!inputDate.isEmpty() || inputDate != null ){
		    String subString = inputDate.substring(0,10);
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date newFormatedDate = formatter.parse(subString);
			newDateString=dateFormat.format(newFormatedDate);
			}
		System.out.println("newDateString ----------->" + newDateString);
		return newDateString;
	}
}

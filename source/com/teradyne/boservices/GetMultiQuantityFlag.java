package com.teradyne.boservices;

import java.rmi.RemoteException;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GetMultiQuantityFlag implements YIFCustomApi {

	
	String sReceivingNode;
	String apiName1 = "getOrderLineList";
	String apiName2 = "getOrganizationHierarchy";
	Document outDoc1 = null;
	Document outDoc2 = null;
	YFCDocument outputDoc = YFCDocument.createDocument("Output");
	Double dMaxqty = 0.0;
	String MaxQty = null;
	
	
	
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}
	public Document getMultiQuantity(YFSEnvironment env, Document inDoc){
		
			try {
			YFCDocument inxml = YFCDocument.getDocumentFor(inDoc);
			YFCElement inEle = inxml.getDocumentElement();
			
			//YFCDocument finalOutput = null;
			
			if(inEle.getChildElement("GetMultiQuantityFlag")!= null && inEle.getChildElement("GetMultiQuantityFlag").hasAttribute("OrderCenter")){
				
				
				sReceivingNode = inEle.getChildElement("GetMultiQuantityFlag").getAttribute("OrderCenter");
				YFCDocument inputDoc1 = YFCDocument.getDocumentFor("<OrderLine> <Order BuyerOrganizationCode=\"\" /> </OrderLine>");
				if(sReceivingNode != null){
					inputDoc1.getDocumentElement().getChildElement("Order").setAttribute("BuyerOrganizationCode", sReceivingNode);
				
					YFCDocument templateDoc1 = YFCDocument
							.getDocumentFor("<OrderLineList>"
									+ "<OrderLine>"
									+ "<ItemDetails>"
									+ "<PrimaryInformation MaxOrderQuantity=\"\" />"
									+ "</ItemDetails>"						
									+ "</OrderLine>"
									+ "</OrderLineList>");
				env.setApiTemplate(apiName1, templateDoc1.getDocument());
				outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName1,inputDoc1.getDocument());
				//YFCElement eleInput = (YFCElement) outDoc1.getDocumentElement();
				YFCElement eleInput = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
				if(eleInput.getChildElement("OrderLine") != null ){
					
					YFCIterable<YFCElement> allOrdline = eleInput.getChildren();
					for (YFCElement inputEle : allOrdline){
					
					MaxQty = inputEle.getChildElement("ItemDetails").getChildElement("PrimaryInformation").getAttribute("MaxOrderQuantity");	
					dMaxqty = Double.parseDouble(MaxQty);
				
				YFCDocument inputDoc2 = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\" />");
				inputDoc2.getDocumentElement().setAttribute("OrganizationCode", sReceivingNode);
				
				YFCDocument templateDoc2 = YFCDocument
						.getDocumentFor("<Organization>"
								+ "<Extn CustomerSiteStatus=\"\" />"
								+ "</Organization>");
				
				env.setApiTemplate(apiName2, templateDoc2.getDocument());
				outDoc2 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName2,inputDoc2.getDocument());
				
				YFCElement Extn = YFCDocument.getDocumentFor(outDoc2).getDocumentElement().getChildElement("Extn");
				if(Extn != null)
				{
					YFCElement finalOutputEle = outputDoc.getDocumentElement().createChild("GetMultiQuantityFlag");
					if(Extn.getAttribute("CustomerSiteStatus").equalsIgnoreCase("A")){
						if(Double.compare(dMaxqty, 0.0) > 0){
							finalOutputEle.setAttribute("MultiQuantityOrderFlag","Y");
						}
						else{
							finalOutputEle.setAttribute("MultiQuantityOrderFlag","N");
						}
							
				}
					
				}
					}//for FOR loop
				}
			}
		}		
		} catch (YFSException e) {
			// TODO Auto-generated catch block
			//throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (YIFClientCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return outputDoc.getDocument();
	}

}

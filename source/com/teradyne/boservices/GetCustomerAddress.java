package com.teradyne.boservices;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetCustomerAddress {

	String apiName1="getOrganizationList";
	String CusType=""; String CustId="";String CustNo="";String OrgCode="";String sCustomerSiteStatus="";
	String sOrgCode=""; String sAdd1="";String sAdd2="";String sAdd3="";String sAdd4="";String sAdd5="";
	String sState=""; String sZipCode=""; String sCountry=""; String sCity=""; String sCountryName="";String sBillId="";
	Document outDoc1 =null;
	YFCDocument inYFCXML = null;
	YFCDocument outDoc = YFCDocument.createDocument("Output");
	public Document getCustAddress(YFSEnvironment env, Document inxml){
		try {
		inYFCXML = YFCDocument.getDocumentFor(inxml);
		YFCElement NodeList = inYFCXML.getDocumentElement();
		
		YFCElement Node = NodeList.getChildElement("GetCustomerAddress");
	
		
		if(Node != null){
			if (Node.hasAttribute("CustomerNumber") && Node.hasAttribute("SiteID")&& !Node.getAttribute("SiteID").isEmpty() && Node.hasAttribute("AddressType")){
					CustNo = Node.getAttribute("CustomerNumber");
					OrgCode = Node.getAttribute("SiteID");
					CusType = Node.getAttribute("AddressType");
					CustId= CustNo +"-"+ OrgCode;
					
			}
			if(Node.hasAttribute("CustomerNumber")&& Node.hasAttribute("AddressType")&& Node.getAttribute("SiteID").isEmpty()){
			CustId =Node.getAttribute("CustomerNumber");
			CustId=CustId + "-";
			CusType = Node.getAttribute("AddressType");
			
			}
		}

		if(CusType.equalsIgnoreCase("SHIP TO")){
	
			final YFCDocument inputDoc = YFCDocument.getDocumentFor("<Organization OrganizationCodeQryType=\"FLIKE\"  OrganizationCode=\"\"/>");
			YFCElement inputOrgEle = inputDoc.getDocumentElement();
	
			inputOrgEle.setAttribute("OrganizationCode", CustId);
			
			//YFCElement extnEle = inputOrgEle.getChildElement("Extn");
			//extnEle.setAttribute("CustomerSiteType", CusType);
			//inputDoc.getDocumentElement().setAttribute("CustomerSiteType", CusType);
			
			final YFCDocument templateDoc = YFCDocument.getDocumentFor("<OrganizationList><Organization OrganizationCode=\"\"><Extn CustomerSiteStatus=\"\"/><CorporatePersonInfo ZipCode=\"\" State=\"\" Country=\"\" City=\"\" AddressLine5=\"\" AddressLine4=\"\" AddressLine3=\"\" AddressLine2=\"\" AddressLine1=\"\" /></Organization></OrganizationList>");
			env.setApiTemplate(apiName1, templateDoc.getDocument());
			outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName1,inputDoc.getDocument());
			
			YFCElement orgListElement = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
			if(!YFCElement.isVoid(orgListElement)){
				
				YFCIterable<YFCElement> allOrglist = orgListElement.getChildren();
				
				for (YFCElement orgElement : allOrglist){
					sOrgCode= orgElement.getAttribute("OrganizationCode");
					YFCElement orgExtnEle = orgElement.getChildElement("Extn");
					if(!YFCElement.isVoid(orgExtnEle)){
						sCustomerSiteStatus=orgExtnEle.getAttribute("CustomerSiteStatus");
					}
				
					YFCElement corpEle = orgElement.getChildElement("CorporatePersonInfo");
					if(!YFCElement.isVoid(corpEle)){
						sAdd1=corpEle.getAttribute("AddressLine1");
						sAdd2=corpEle.getAttribute("AddressLine2");
						sAdd3=corpEle.getAttribute("AddressLine3");
						sAdd4=corpEle.getAttribute("AddressLine4");
						sAdd5=corpEle.getAttribute("AddressLine5");
						sCity=corpEle.getAttribute("City");
						sCountry=corpEle.getAttribute("Country");
						sZipCode=corpEle.getAttribute("ZipCode");
						sState=corpEle.getAttribute("State");
						}
					if(sCountry!=null){
						sCountryName=countryName(sCountry,env);
					}
					
					if("A".equalsIgnoreCase(sCustomerSiteStatus)){
					YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetCustomerAddress");
					
					String[] str_array1 = sOrgCode.split("-");
					//String stringc = str_array1[0];
					String stringd = str_array1[1];
					finalOutputEle.setAttribute("SiteID",stringd);
					finalOutputEle.setAttribute("Address-Country",sCountryName);
					finalOutputEle.setAttribute("Address-ZipCode",sZipCode);
					finalOutputEle.setAttribute("Address-State",sState);
					finalOutputEle.setAttribute("Address-City",sCity);
					finalOutputEle.setAttribute("Address-Line1",sAdd1);
					finalOutputEle.setAttribute("Address-Line2",sAdd2);
					finalOutputEle.setAttribute("Address-Line3",sAdd3);
					finalOutputEle.setAttribute("Address-Line4",sAdd4);
					finalOutputEle.setAttribute("Address-Line5",sAdd5);
					}
				}//end FOR
			}
		}//end of SHIP TO
		
		if(CusType.equalsIgnoreCase("BILL TO")){
			final YFCDocument inputDoc = YFCDocument.getDocumentFor("<Organization OrganizationCodeQryType=\"FLIKE\"  OrganizationCode=\"\"/>");
			YFCElement inputOrgEle = inputDoc.getDocumentElement();
	
			inputOrgEle.setAttribute("OrganizationCode", CustId);
			
			//YFCElement extnEle = inputOrgEle.getChildElement("Extn");
			//extnEle.setAttribute("CustomerSiteType", CusType);
			//inputDoc.getDocumentElement().setAttribute("CustomerSiteType", CusType);
			
			final YFCDocument TemplateOrg = YFCDocument.getDocumentFor("<OrganizationList><Organization OrganizationCode=\"\"><Extn CustomerSiteStatus=\"\"><TerCustBillingOrgList><TerCustBillingOrg TerBillingID=\"\" TerDefaultFLag=\"\" TerAddressLine1=\"\" TerAddressLine2=\"\" TerAddressLine3=\"\" TerAddressLine4=\"\" TerAddressLine5=\"\" TerCity=\"\" TerState=\"\" TerCountry=\"\" TerZipCode=\"\"/></TerCustBillingOrgList></Extn></Organization></OrganizationList>");
			env.setApiTemplate(apiName1,TemplateOrg.getDocument());
			outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1,inputDoc.getDocument());
			env.clearApiTemplate(apiName1);
			
			YFCElement OutputOrgElementList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
			if(!YFCElement.isVoid(OutputOrgElementList)){
				
			
				YFCIterable<YFCElement> allOrglist = OutputOrgElementList.getChildren();
				for (YFCElement orgElement : allOrglist){
					sOrgCode= orgElement.getAttribute("OrganizationCode");
					
					YFCElement extnElelist = orgElement.getChildElement("Extn");
						
					if (!YFCElement.isVoid(extnElelist)){
						sCustomerSiteStatus= extnElelist.getAttribute("CustomerSiteStatus");
						YFCElement terElelist = extnElelist.getChildElement("TerCustBillingOrgList");
						if(!YFCElement.isVoid(terElelist)){
						YFCIterable<YFCElement> allTerOrglist = terElelist.getChildren();
						for (YFCElement terlineEle : allTerOrglist){
							sBillId=terlineEle.getAttribute("TerBillingID");
							sAdd1 = terlineEle.getAttribute("TerAddressLine1");
							sAdd2 = terlineEle.getAttribute("TerAddressLine2");
							sAdd3 = terlineEle.getAttribute("TerAddressLine3");
							sAdd4 = terlineEle.getAttribute("TerAddressLine4");
							sAdd5 = terlineEle.getAttribute("TerAddressLine5");
							sCity = terlineEle.getAttribute("TerCity");
							sState = terlineEle.getAttribute("TerState");
							sCountry = terlineEle.getAttribute("TerCountry");
							sZipCode = terlineEle.getAttribute("TerZipCode");
							
							if(sCountry !=null){
								sCountryName=countryName(sCountry,env);
							}
							
							if("A".equalsIgnoreCase(sCustomerSiteStatus)){
							YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetCustomerAddress");
							
							String[] str_array1 = sBillId.split("-");
							//String stringc = str_array1[0];
							String stringd = str_array1[1];
							finalOutputEle.setAttribute("SiteID",stringd);
							finalOutputEle.setAttribute("Address-Country",sCountryName);
							finalOutputEle.setAttribute("Address-ZipCode",sZipCode);
							finalOutputEle.setAttribute("Address-State",sState);
							finalOutputEle.setAttribute("Address-City",sCity);
							finalOutputEle.setAttribute("Address-Line1",sAdd1);
							finalOutputEle.setAttribute("Address-Line2",sAdd2);
							finalOutputEle.setAttribute("Address-Line3",sAdd3);
							finalOutputEle.setAttribute("Address-Line4",sAdd4);
							finalOutputEle.setAttribute("Address-Line5",sAdd5);
							}
							}
						}//end of ter FOR
					  }
				}//end of Organization FOR
			}
		}
	
}
		catch(Exception e)
		 {
			e.printStackTrace();
			System.out.println("######### Exception Caught : " + e.toString());
			//throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		 }
		return outDoc.getDocument();
	}
		
		private String countryName(String countryCode, YFSEnvironment env1) throws Exception{
		String apiName1 = "getCommonCodeList";
		String CountryName="";
		YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" />");
		inputDoc.getDocumentElement().setAttribute("CodeValue", countryCode);
		inputDoc.getDocumentElement().setAttribute("CodeType", "COUNTRY");
		outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env1, apiName1, inputDoc.getDocument());
		env1.clearApiTemplate(apiName1);
		
		if (outDoc1 !=null){
			YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
			if (OutputCommList != null){
				YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
				if(eleComm !=null){
					CountryName= eleComm.getAttribute("CodeLongDescription");
					System.out.println("CountryName Desc@@@@@@@@@@ "+CountryName);
				}
			}
		}
			return CountryName;
		}
}

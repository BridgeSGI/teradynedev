package com.teradyne.boservices;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetShipVia {

	public Document getShipVia(YFSEnvironment env, Document inDoc)
	{
		
		YFCDocument outDoc = null;
		YFCDocument inputScac = null;
		YFCDocument inputScacServ=null;
		YFCDocument TemplateScac=null;
		YFCDocument TemplateScacServ=null;
		String ScacApi ="getScacList";
		String ScaServApi="getScacAndServiceList";
		Document outputScac = null;
		Document outputScacServ=null;

		
		
		outDoc = YFCDocument.createDocument("Output");
		try{

		inputScac = YFCDocument.getDocumentFor("<Scac OrganizationCode=\"\" />");
		inputScac.getDocumentElement().setAttribute("OrganizationCode", "CSO");
		TemplateScac = YFCDocument.getDocumentFor("<ScacList><Scac ScacKey=\"\" ScacDesc=\"\"/></ScacList>");
		env.setApiTemplate(ScacApi,TemplateScac.getDocument());
		outputScac = YIFClientFactory.getInstance().getLocalApi().invoke(env, ScacApi,inputScac.getDocument());
		env.clearApiTemplate(ScacApi);
		
		YFCElement OutputScacList = YFCDocument.getDocumentFor(outputScac).getDocumentElement();
		
		if(OutputScacList != null){
			
			YFCIterable<YFCElement> allScacline = OutputScacList.getChildren();
			
			for (YFCElement scacLineEle : allScacline) {
					
			String ScacKey=scacLineEle.getAttribute("ScacKey"); 
			String ScacDesc=scacLineEle.getAttribute("ScacDesc"); 
		
			inputScacServ = YFCDocument.getDocumentFor("<ScacAndService ScacKey=\"\" />");
			inputScacServ.getDocumentElement().setAttribute("ScacKey",ScacKey);
			TemplateScacServ = YFCDocument.getDocumentFor("<ScacAndServiceList><ScacAndService ScacAndService=\"\"/></ScacAndServiceList>");
			
			env.setApiTemplate(ScaServApi,TemplateScacServ.getDocument());
			outputScacServ = YIFClientFactory.getInstance().getLocalApi().invoke(env, ScaServApi,inputScacServ.getDocument());
			env.clearApiTemplate(ScaServApi);
			
			YFCElement OutputScacServList = YFCDocument.getDocumentFor(outputScacServ).getDocumentElement();
			if(OutputScacServList != null){
				
				YFCIterable<YFCElement> allScacServline = OutputScacServList.getChildren();
				
				for (YFCElement scacservLineEle : allScacServline) {
					
					String ScacAndService=scacservLineEle.getAttribute("ScacAndService");
					
					YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetShipVia");
					finalOutputEle.setAttribute("ShipViaService", ScacAndService);
					finalOutputEle.setAttribute("ShipViaDescription", ScacDesc);
					finalOutputEle.setAttribute("ShipViaCarrier", ScacKey);
				}
					
				}
	
			}

		}
	}
		catch (Exception e) {
			//throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}
		
		return outDoc.getDocument();
		
	}
	
}

package com.teradyne.boservices;       

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetBillingAssociationWithAddress {

	/**
	 * @param args
	 */
	YFCDocument outDoc = YFCDocument.createDocument("Output");
	public Document getBillingAssociationWithAddress(YFSEnvironment env,Document inDoc) {
		try{
		//YFCDocument inYFCXML = null;
		YFCDocument inputOrg=null;
		YFCDocument TemplateOrg=null; 
		String CustNo =null; String OrgCode=null; String CustId=null; String Org = null;
		String BuyOrgCode=null; 
		//String CustSiteStatus=null;
		String OrgName =null;
		String TerAddressLine1=null;		String TerAddressLine2=null;	
		String TerAddressLine3=null;		String TerAddressLine4=null;
		String TerAddressLine5=null; 		String TerCity=null;String CountryName=null;
		String TerState=null;	String TerCountry=null;	String TerZipCode=null;
		
		String apiName1 ="getCustomerDetails";String getOrgHierarchyApi = "getOrganizationHierarchy";
		Document outDoc1 = null;Document outputOrg=null;
		Document outDoc5=null;
				
		YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleInput = inYFCDoc.getDocumentElement();
		
		
		if (eleInput != null) {
			
			YFCIterable<YFCElement> allInputline = eleInput.getChildren();
			for (YFCElement inputEle : allInputline)
		 {
				
			
			if (inputEle.hasAttribute("CustomerNumber") && inputEle.hasAttribute("ShipToSiteID") ){
					CustNo = inputEle.getAttribute("CustomerNumber");
					OrgCode = inputEle.getAttribute("ShipToSiteID");
					CustId= CustNo +"-"+ OrgCode;
					Org="CSO";
				}
		
			final YFCDocument inputDoc = YFCDocument.getDocumentFor("<Customer CustomerID=\"\" OrganizationCode=\"\" />");
			inputDoc.getDocumentElement().setAttribute("CustomerID", CustId);
			inputDoc.getDocumentElement().setAttribute("OrganizationCode", Org);
			
			final YFCDocument templateDoc = YFCDocument.getDocumentFor("<Customer BuyerOrganizationCode=\"\" CustomerID=\"\"><BuyerOrganization OrganizationName=\"\"><Extn CustomerSiteStatus=\"\"/></BuyerOrganization></Customer>");
			env.setApiTemplate(apiName1, templateDoc.getDocument());
			outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName1,inputDoc.getDocument());
			
			YFCElement CustElementList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
			if (CustElementList != null){
				
			CustId = CustElementList.getAttribute("CustomerID");
			BuyOrgCode =CustElementList.getAttribute("BuyerOrganizationCode");
				
			YFCElement custBuyOrgEle = CustElementList.getChildElement("BuyerOrganization");
			
			if(custBuyOrgEle!=null){
			OrgName = custBuyOrgEle.getAttribute("OrganizationName");
			//CustSiteStatus =custBuyOrgEle.getChildElement("Extn").getAttribute("CustomerSiteStatus");
			}
				
			inputOrg = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"/>");
			inputOrg.getDocumentElement().setAttribute("OrganizationCode", BuyOrgCode);
			TemplateOrg = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"><Extn><TerCustBillingOrgList><TerCustBillingOrg TerBillingID=\"\" TerDefaultFLag=\"\" TerAddressLine1=\"\" TerAddressLine2=\"\" TerAddressLine3=\"\" TerAddressLine4=\"\" TerAddressLine5=\"\" TerCity=\"\" TerState=\"\" TerCountry=\"\" TerZipCode=\"\"/></TerCustBillingOrgList></Extn></Organization>");

			env.setApiTemplate(getOrgHierarchyApi,TemplateOrg.getDocument());
			outputOrg = YIFClientFactory.getInstance().getLocalApi().invoke(env, getOrgHierarchyApi,inputOrg.getDocument());
			env.clearApiTemplate(getOrgHierarchyApi);
			
			YFCElement OutputOrgElementList = YFCDocument.getDocumentFor(outputOrg).getDocumentElement();
			if(OutputOrgElementList != null){
				YFCElement extnEle = OutputOrgElementList.getChildElement("Extn");
				if(extnEle !=null){
					YFCElement terElelist = extnEle.getChildElement("TerCustBillingOrgList");
					
					YFCIterable<YFCElement> allOrglist = terElelist.getChildren();
					
					for (YFCElement terlineEle : allOrglist){
						String TerDefaultFLag= terlineEle.getAttribute("TerDefaultFLag");
						String TerBillingID=terlineEle.getAttribute("TerBillingID");
						
												
							TerAddressLine1 = terlineEle.getAttribute("TerAddressLine1");
							TerAddressLine2 = terlineEle.getAttribute("TerAddressLine2");
							TerAddressLine3 = terlineEle.getAttribute("TerAddressLine3");
							TerAddressLine4 = terlineEle.getAttribute("TerAddressLine4");
							TerAddressLine5 = terlineEle.getAttribute("TerAddressLine5");
							TerCity = terlineEle.getAttribute("TerCity");
							TerState = terlineEle.getAttribute("TerState");
							TerCountry = terlineEle.getAttribute("TerCountry");
							TerZipCode = terlineEle.getAttribute("TerZipCode");
							
							if(TerCountry !=null){
								String apiName5 = "getCommonCodeList";
								YFCDocument inputDoc5 = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" />");
								inputDoc5.getDocumentElement().setAttribute("CodeValue", TerCountry);
								inputDoc5.getDocumentElement().setAttribute("CodeType", "COUNTRY");
								outDoc5 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName5, inputDoc5.getDocument());
								env.clearApiTemplate(apiName5);
								
								if (outDoc5 !=null){
									YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc5).getDocumentElement();
									if (OutputCommList != null){
										YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
										if(eleComm !=null){
											CountryName= eleComm.getAttribute("CodeLongDescription");
											System.out.println("CountryName Desc@@@@@@@@@@ "+CountryName);
										}
									}
								}
							
							}
							
							
						YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetBillingAssociationWithAddress");
						finalOutputEle.setAttribute("Address-ZipCode", TerZipCode);
						finalOutputEle.setAttribute("Address-Country", CountryName);
						finalOutputEle.setAttribute("Address-State", TerState);
						finalOutputEle.setAttribute("Address-City", TerCity);
						finalOutputEle.setAttribute("Address-Line5", TerAddressLine5);
						finalOutputEle.setAttribute("Address-Line4", TerAddressLine4);
						finalOutputEle.setAttribute("Address-Line3", TerAddressLine3);
						finalOutputEle.setAttribute("Address-Line2", TerAddressLine2);
						finalOutputEle.setAttribute("Address-Line1", TerAddressLine1);
						String[] str_array1 = CustId.split("-");
						String stringc = str_array1[0];
						String stringd = str_array1[1];
						finalOutputEle.setAttribute("ShipToCustomerNumber",	stringc);
						finalOutputEle.setAttribute("ShipToSiteID",	stringd);
						String[] str_array2 = TerBillingID.split("-");
						String stringa = str_array2[0];
						String stringb = str_array2[1];
						finalOutputEle.setAttribute("BillToCustomerName",OrgName);
						finalOutputEle.setAttribute("BillToCustomerNumber",	stringa);
						finalOutputEle.setAttribute("BillToSiteID",	stringb);
						if("Y".equalsIgnoreCase(TerDefaultFLag))
							finalOutputEle.setAttribute("DefaultFlag",	TerDefaultFLag);
							else
								finalOutputEle.setAttribute("DefaultFlag",	"");
						
					}
					}
			
					}
			}//cust
					
	}

}
		}
		catch(Exception e){
			//throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}
		return outDoc.getDocument();
		}
}
		

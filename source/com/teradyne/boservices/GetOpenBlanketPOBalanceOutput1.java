package com.teradyne.boservices;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;



public class GetOpenBlanketPOBalanceOutput1 {

	public Document getOpenBlanketPOBalance(YFSEnvironment env, Document inDoc) {
		
		YFCDocument out = YFCDocument.createDocument("Output");
		try {
			
			//YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
			
			HashMap<String, YFCElement> unique = new HashMap<String,YFCElement>();
			YFCElement outputEle = YFCDocument.getDocumentFor(inDoc).getDocumentElement();
			YFCIterable<YFCElement> allInputline = outputEle.getChildren();
			for (YFCElement inputEle : allInputline){
				String S1 = inputEle.getAttribute("ShipToCustomerNo")+":"+inputEle.getAttribute("ShipToCustomerSiteNo")+":"+ inputEle.getAttribute("BPONo")+":"+inputEle.getAttribute("CurrencyCode")+":"+inputEle.getAttribute("POBalance");
				unique.put(S1, inputEle);
				
			}
			
			YFCElement OutOrderEle = out.getDocumentElement();
			 for (Map.Entry<String, YFCElement> entry : unique.entrySet()) {
			        String key = entry.getKey();
			        YFCElement value = entry.getValue();
			        YFCElement temp = OutOrderEle.createChild("GetOpenBlanketPOBalance");
			        temp.setAttribute("ShipToCustomerNo", value.getAttribute("ShipToCustomerNo"));
			        temp.setAttribute("ShipToCustomerSiteNo", value.getAttribute("ShipToCustomerSiteNo"));
			        temp.setAttribute("BPONo", value.getAttribute("BPONo"));
			        temp.setAttribute("CurrencyCode", value.getAttribute("CurrencyCode"));
			        temp.setAttribute("POBalance", value.getAttribute("POBalance"));
			 
			    }
		}
		catch (Exception exception) {

			exception.printStackTrace();
			System.out.println("######### Exception Caught : " + exception.toString());
			//throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		}
		return out.getDocument();
	}
}

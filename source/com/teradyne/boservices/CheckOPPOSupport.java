package com.teradyne.boservices;

import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;


public class CheckOPPOSupport {

	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	public Document checkOPPOSupport(YFSEnvironment env,	Document inDoc) {
		YFCDocument outDoc = YFCDocument.createDocument("Output");
		String sSystemSerialNo="";
		String sServiceType="";
		String sOrderLineKey="";
		String sInput="";
		String sMaxLineStatus="";
		//String sOrderNo="";
		
	//	String sOrdPart1="";
		//String sOrdPart2="";
		String sKitCode="";
		String sOrderType="";
		String sItemID="";
		String sComponentItemID="";
		Document outDoc1=null;
		Document terOutDoc=null;
		try{
			
			YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleInput = inYFCDoc.getDocumentElement();
				
			if (eleInput != null) {
				
				YFCIterable<YFCElement> allInputline = eleInput.getChildren();
				for (YFCElement inputEle : allInputline){
					if(inputEle.hasAttribute("SystemSerialNoAndServiceType")){
						sInput=inputEle.getAttribute("SystemSerialNoAndServiceType");	
						String[] str_array1 = sInput.split(":");
						sSystemSerialNo =str_array1[0];
						sServiceType = str_array1[1];
					}
					
					if(sSystemSerialNo !=null){
						
						final YFCDocument inputDoc = YFCDocument.getDocumentFor("<OrderLine DocumentType=\"\" StatusQryType=\"\" ToStatus=\"\"> <Extn SystemSerialNo=\"\"/></OrderLine>");
						inputDoc.getDocumentElement().setAttribute("DocumentType", "0017.ex");
						inputDoc.getDocumentElement().setAttribute("StatusQryType", "BETWEEN");
						inputDoc.getDocumentElement().setAttribute("ToStatus", "1100.30");
						inputDoc.getDocumentElement().getChildElement("Extn").setAttribute("SystemSerialNo", sSystemSerialNo);
						
						final YFCDocument templateDoc = YFCDocument.getDocumentFor("<OrderLineList><OrderLine OrderLineKey=\"\"/></OrderLineList>");
						env.setApiTemplate("getOrderLineList", templateDoc.getDocument());
						outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,"getOrderLineList",inputDoc.getDocument());
						System.out.println("######### Output of OrderlineList--------->"+outDoc1.toString());
						YFCElement sOrderLineList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
						
						if(!YFCElement.isVoid(sOrderLineList)){
							YFCIterable<YFCElement> allOrderLines = sOrderLineList.getChildren();
							for (YFCElement sOrderLineEle : allOrderLines){
								sOrderLineKey = sOrderLineEle.getAttribute("OrderLineKey");
								System.out.println("#########OrderLineKey taken from IB");
								if(!YFCElement.isVoid(sOrderLineKey)){
									
									final YFCDocument terDoc = YFCDocument.getDocumentFor("<TerSCIBMapping TerIBOLKey=\"\" ></TerSCIBMapping>");
									terDoc.getDocumentElement().setAttribute("TerIBOLKey",sOrderLineKey);
																
									terOutDoc = YIFClientFactory.getInstance().getLocalApi().executeFlow(env,"GetIBForLine",terDoc.getDocument());
									YFCElement sTerSCIBMappingList = YFCDocument.getDocumentFor(terOutDoc).getDocumentElement();
									System.out.println("######### Called GetIBForLine");
									if(!YFCElement.isVoid(sTerSCIBMappingList)){
										
										YFCIterable<YFCElement> terLines = sTerSCIBMappingList.getChildren();
										for (YFCElement sTerSCIBLines : terLines){
											YFCElement sSCLines = sTerSCIBLines.getChildElement("YFSSCOrderLine");
											if(!YFCElement.isVoid(sSCLines)){
												sMaxLineStatus=sSCLines.getAttribute("MaxLineStatus");
												sKitCode=sSCLines.getAttribute("KitCode");
												if(sMaxLineStatus.equals("1100.200")|| sMaxLineStatus.equals("1100.300")|| sMaxLineStatus.equals("1100.400")){
													System.out.println("######### Status Check done");
													
													YFCElement itemDetSCEle = sSCLines.getChildElement("Item");
													if(!YFCElement.isVoid(itemDetSCEle)){
														sItemID = itemDetSCEle.getAttribute("ItemID");
													}
													YFCElement ordSCEle = sSCLines.getChildElement("Order");
													if(!YFCElement.isVoid(ordSCEle)){
														//sOrderNo= ordSCEle.getAttribute("OrderNo");
														/*String[] str_array1 = sOrderNo.split("-");
														int sLength = str_array1.length;
														if(sLength > 1){
															sOrdPart1 = str_array1[0];
															sOrdPart2 = str_array1[1];
														}
														if (sLength == 1){
															sOrdPart1 = str_array1[0];
															sOrdPart2 = "";
														}
														*/
														sOrderType=ordSCEle.getAttribute("OrderType");
													}
														if("OPPO".equalsIgnoreCase(sOrderType)){
															if("BUNDLE".equalsIgnoreCase(sKitCode)){
																//getting the components of the item
																YFCElement componentsEle = itemDetSCEle.getChildElement("Components");
																if(!YFCElement.isVoid(componentsEle)){
																	YFCIterable<YFCElement> compLines = componentsEle.getChildren();
																	for (YFCElement componentEle : compLines){
																		sComponentItemID= componentEle.getAttribute("ComponentItemID");
																		if(sComponentItemID.equalsIgnoreCase(sServiceType)){
																			System.out.println("######### Output generation from the kit code");
																			YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("CheckOPPOSupport");
																			finalOutputEle.setAttribute("ServiceType", sComponentItemID);
																			finalOutputEle.setAttribute("SystemSerialNo", sSystemSerialNo);
																			finalOutputEle.setAttribute("EntitlementTypeCode", sOrderType);
																		}
																		
																	}//for each components
																}
																
																
																}
															else{
																if(sItemID.equalsIgnoreCase(sServiceType)){
																	System.out.println("######### Output generation");
																	YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("CheckOPPOSupport");
																	finalOutputEle.setAttribute("ServiceType", sItemID);
																	finalOutputEle.setAttribute("SystemSerialNo", sSystemSerialNo);
																	finalOutputEle.setAttribute("EntitlementTypeCode", sOrderType);
																}
													
															}
														}//end of OPPO check
												}// end of status check
											}
										}// for each SC lines in the ter table
									}
								}
							}//end of FOR Loop for each order lines in IB
						}
					}
				}// end of FOR loop for each input
			}
		}
		catch(Exception e)
		{
			//throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		}
		return outDoc.getDocument();
	}
}

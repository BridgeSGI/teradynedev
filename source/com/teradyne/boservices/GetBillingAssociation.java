package com.teradyne.boservices;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetBillingAssociation {


	YFCDocument outDoc = YFCDocument.createDocument("Output");
	public Document getBillingAssociation(YFSEnvironment env,Document inDoc) {
		try{
		//YFCDocument inYFCXML = null;
		YFCDocument inputOrg=null;
		YFCDocument TemplateOrg=null; 
		String CustNo =null; String OrgCode=null; String CustId=null; String Org = null;
		String BuyOrgCode=null; 
		//String CustSiteStatus=null;
		String OrgName =null;
		String TerBillingID=null;
		String TerDefaultFLag=null;
		
		
		String apiName1 ="getCustomerDetails";String getOrgHierarchyApi = "getOrganizationHierarchy";
		Document outDoc1 = null;Document outputOrg=null;
		

		
		YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleInput = inYFCDoc.getDocumentElement();
		
		
		if (eleInput != null) {
			
			YFCIterable<YFCElement> allInputline = eleInput.getChildren();
			for (YFCElement inputEle : allInputline)
		 {
				
			
			if (inputEle.hasAttribute("CustomerNumber") && inputEle.hasAttribute("ShipToSiteId") ){
					CustNo = inputEle.getAttribute("CustomerNumber");
					OrgCode = inputEle.getAttribute("ShipToSiteId");
					CustId= CustNo +"-"+ OrgCode;
					Org="CSO";
				}
		
			final YFCDocument inputDoc = YFCDocument.getDocumentFor("<Customer CustomerID=\"\" OrganizationCode=\"\" />");
			inputDoc.getDocumentElement().setAttribute("CustomerID", CustId);
			inputDoc.getDocumentElement().setAttribute("OrganizationCode", Org);
			
			final YFCDocument templateDoc = YFCDocument.getDocumentFor("<Customer BuyerOrganizationCode=\"\" CustomerID=\"\"><BuyerOrganization OrganizationName=\"\"><Extn CustomerSiteStatus=\"\"/></BuyerOrganization></Customer>");
			env.setApiTemplate(apiName1, templateDoc.getDocument());
			outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName1,inputDoc.getDocument());
			
			YFCElement CustElementList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
			if (CustElementList != null){
				
			CustId = CustElementList.getAttribute("CustomerID");
			BuyOrgCode =CustElementList.getAttribute("BuyerOrganizationCode");
				
			YFCElement custBuyOrgEle = CustElementList.getChildElement("BuyerOrganization");
			
			if(custBuyOrgEle!=null){
			OrgName = custBuyOrgEle.getAttribute("OrganizationName");
			//CustSiteStatus =custBuyOrgEle.getChildElement("Extn").getAttribute("CustomerSiteStatus");
			}
				
			inputOrg = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"/>");
			inputOrg.getDocumentElement().setAttribute("OrganizationCode", BuyOrgCode);
			TemplateOrg = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"><Extn><TerCustBillingOrgList><TerCustBillingOrg TerBillingID=\"\" TerDefaultFLag=\"\"/></TerCustBillingOrgList></Extn></Organization>");

			env.setApiTemplate(getOrgHierarchyApi,TemplateOrg.getDocument());
			outputOrg = YIFClientFactory.getInstance().getLocalApi().invoke(env, getOrgHierarchyApi,inputOrg.getDocument());
			env.clearApiTemplate(getOrgHierarchyApi);
			
			YFCElement OutputOrgElementList = YFCDocument.getDocumentFor(outputOrg).getDocumentElement();
			if(OutputOrgElementList != null){
				YFCElement extnEle = OutputOrgElementList.getChildElement("Extn");
				if(extnEle !=null){
					YFCElement terElelist = extnEle.getChildElement("TerCustBillingOrgList");
					
					YFCIterable<YFCElement> allOrglist = terElelist.getChildren();
					
					for (YFCElement terlineEle : allOrglist){
						TerBillingID = terlineEle.getAttribute("TerBillingID");
						TerDefaultFLag = terlineEle.getAttribute("TerDefaultFLag");
						
						
						YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetBillingAssociation");
						
						String[] str_array1 = CustId.split("-");
						String stringc = str_array1[0];
						String stringd = str_array1[1];
						finalOutputEle.setAttribute("CustomerNumber",	stringc);
						finalOutputEle.setAttribute("CustomerAddressNo",	stringd);
						String[] str_array2 = TerBillingID.split("-");
						String stringa = str_array2[0];
						String stringb = str_array2[1];
						finalOutputEle.setAttribute("BillToCustomerName",OrgName);
						finalOutputEle.setAttribute("BillToCustomerNumber",	stringa);
						finalOutputEle.setAttribute("BillToSiteID",	stringb);
						if("Y".equalsIgnoreCase(TerDefaultFLag))
						finalOutputEle.setAttribute("DefaultFlag",	TerDefaultFLag);
						else
							finalOutputEle.setAttribute("DefaultFlag",	"");
					}
					}
			
					}
			}//cust
					
	}

}
		}
		catch(Exception e){
			//throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}
		return outDoc.getDocument();
		}
	
	
	
}

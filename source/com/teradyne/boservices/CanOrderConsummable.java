package com.teradyne.boservices;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class CanOrderConsummable implements YIFCustomApi {

	String apiName = "getOrderList";
	Document outDoc = null;
	java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
	//DateFormat dateFormat1 = new SimpleDateFormat();
	int Counter = 0;
	YFCDocument outputDoc = YFCDocument.createDocument("Output");
	
	
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}
	public Document Consume(YFSEnvironment env, Document inDoc) throws ParseException{
		try {
		YFCDocument inxml = YFCDocument.getDocumentFor(inDoc);
		YFCElement inEle = inxml.getDocumentElement();
		inEle.getChildElement("CanOrderConsummable").getAttribute("PartWarranty");
		
		
		final YFCDocument inputDoc = YFCDocument.getDocumentFor("<Order OrderType=\"\" DocumentType=\"\"/>");
		inputDoc.getDocumentElement().setAttribute("OrderType", inEle.getChildElement("CanOrderConsummable").getAttribute("PartWarranty"));
		inputDoc.getDocumentElement().setAttribute("DocumentType","0018.ex");
		final YFCDocument templateDoc = YFCDocument
				.getDocumentFor("<OrderList>"
						+ "<Order>"
						+ "<Extn ExpirationDate=\"\" />" +
						"</Order>" +
						"</OrderList>");

		env.setApiTemplate(apiName, templateDoc.getDocument());
		
			outDoc=YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName, inputDoc.getDocument());

		
		YFCNodeList<YFCElement> OrderList = YFCDocument.getDocumentFor(outDoc).getDocumentElement().getElementsByTagName("Order"); 
		int OrderLength = OrderList.getLength();
		
		for(int i=0; i < OrderLength; i++){
			
			final YFCNode Node = OrderList.item(i);
				YFCElement EleNode = (YFCElement) Node;
				String sExpDate = EleNode.getChildElement("Extn").getAttribute("ExpirationDate");
				
				
				Date currentDate = java.util.Calendar.getInstance().getTime();
				Date ExpDate = dateFormat.parse(sExpDate.substring(0,10));
				
			    if(currentDate.compareTo(ExpDate) <= 0){
			    	Counter = Counter+1;
			    }  			
			}
				outputDoc = YFCDocument.getDocumentFor("<Output>"
				+ "<CanOrderCOnsummable CanOrderFlag=\"\" />" +
				"</Output>");	
				
		if(Counter >= 2){
			outputDoc.getDocumentElement().getChildElement("CanOrderCOnsummable").setAttribute("CanOrderFlag", "Y");
		}
		} catch (YFSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		} catch (NullPointerException e) {
		// TODO Auto-generated catch block
			//throw new YFSException("Exception Thrown", "E-01","Exception Thrown");
		} 
			catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (YIFClientCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return outputDoc.getDocument();
	}
}

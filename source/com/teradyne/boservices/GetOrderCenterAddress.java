package com.teradyne.boservices;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetOrderCenterAddress {

	YFCDocument outDoc = YFCDocument.createDocument("Output");
	public Document getOrderCenterAddress(YFSEnvironment env,Document inDoc) {
		try{
		
		YFCDocument inputOrg=null;
		Document outputOrg=null;
		YFCDocument TemplateOrg=null;
		
		Document outDoc1=null;
		String OrgCode=""; 
		//String Org="";
		String ActivateFlag="";String AddressLine3="";
		String AddressLine4=""; String AddressLine5=""; String City=""; String State="";
		String Country=""; String CountryName="";String ZipCode="";
		String AddressLine1=""; String AddressLine2=""; 
		String OrgHierarchyApi ="getOrganizationHierarchy";
		YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement eleInput = inYFCDoc.getDocumentElement();
		
		
		if (eleInput != null) {

			YFCIterable<YFCElement> allInputline = eleInput.getChildren();
			for (YFCElement inputEle : allInputline){
				
			
			if (inputEle.hasAttribute("Service_Center_Code")){
					
					OrgCode = inputEle.getAttribute("Service_Center_Code");
					//Org="CSO";
				}
		
			inputOrg = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"/>");
			inputOrg.getDocumentElement().setAttribute("OrganizationCode", OrgCode);
			TemplateOrg = YFCDocument.getDocumentFor("<Organization ActivateFlag=\"\"><CorporatePersonInfo AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\" City=\"\" State=\"\" Country=\"\" ZipCode=\"\" /> </Organization>");

			env.setApiTemplate(OrgHierarchyApi,TemplateOrg.getDocument());
			outputOrg = YIFClientFactory.getInstance().getLocalApi().invoke(env, OrgHierarchyApi,inputOrg.getDocument());
			env.clearApiTemplate(OrgHierarchyApi);
			
			YFCElement OutputOrgElementList = YFCDocument.getDocumentFor(outputOrg).getDocumentElement();
			if(OutputOrgElementList != null){
				ActivateFlag= OutputOrgElementList.getAttribute("ActivateFlag");
				
				if("Y".equalsIgnoreCase(ActivateFlag)){
				YFCElement corpEle = OutputOrgElementList.getChildElement("CorporatePersonInfo");
				
				if(!(XmlUtils.isVoid(corpEle)) ){
					AddressLine1=corpEle.getAttribute("AddressLine1");
					AddressLine2=corpEle.getAttribute("AddressLine2");
					AddressLine3=corpEle.getAttribute("AddressLine3");
					AddressLine4=corpEle.getAttribute("AddressLine4");
					AddressLine5=corpEle.getAttribute("AddressLine5");
					State=corpEle.getAttribute("State");
					City=corpEle.getAttribute("City");
					Country=corpEle.getAttribute("Country");
					ZipCode=corpEle.getAttribute("ZipCode");
					
					if(Country !=null){
							String apiName1 = "getCommonCodeList";
							YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" />");
							inputDoc.getDocumentElement().setAttribute("CodeValue", Country);
							inputDoc.getDocumentElement().setAttribute("CodeType", "COUNTRY");
							outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
							env.clearApiTemplate(apiName1);
							
							if (outDoc1 !=null){
								YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
								if (OutputCommList != null){
									YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
									if(eleComm !=null){
										CountryName= eleComm.getAttribute("CodeLongDescription");
										System.out.println("CountryName Desc@@@@@@@@@@ "+CountryName);
									}
								}
							}
						
						}
						
					YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetOrderCenterAddress");
					finalOutputEle.setAttribute("Address-Line1",AddressLine1);
					finalOutputEle.setAttribute("Address-Line2",AddressLine2);
					finalOutputEle.setAttribute("Address-Line3",AddressLine3);
					finalOutputEle.setAttribute("Address-Line4",AddressLine4);
					finalOutputEle.setAttribute("Address-Line5",AddressLine5);
					finalOutputEle.setAttribute("Address-City",City);
					finalOutputEle.setAttribute("Address-State",State);
					finalOutputEle.setAttribute("Address-Country",CountryName);
					finalOutputEle.setAttribute("Address-ZipCode",ZipCode);
					}
					
				}
				}
			}// end of for each ip
		
			}

		}
		
		catch (Exception exception) {
			exception.printStackTrace();
			System.out.println("######### Exception Caught : " + exception.toString());
			//throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}

		return outDoc.getDocument();
	}
}

package com.teradyne.boservices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
//import com.yantra.ycp.greex.library.IsVoid;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;


public class SystemSerialNoServiceType_changes {
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("null")
	public Document systemSerialNoServiceType(YFSEnvironment env,	Document inDoc) {
		
		YFCDocument outDoc = YFCDocument.createDocument("Output");
		String sSystemSerialNo="";
		String sItemID="";
		String sOrderLineKey="";
		String sDate="";
		String sOrderNo="";
		String stringc="";
		String stringd="";
		String sSystemTesterGroup="";
		String sSystemPTExceptionFlag="";
		String sRespProdMfgDivCode="";
		//String sSCLineStatus="";
		String sExpirationDate="";
		String sSystemType="";
		String sRepairablePartsCovered="";
		String sNonrepairPartsCovered="";
		String sCovFlag="";
		String sMaxLineStatus="";
		//String sMktStatus="";
		
		
		Document outDoc1=null;
		Document outDoc2=null;
		Document terOutDoc=null;
		try{
			
			YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleInput = inYFCDoc.getDocumentElement();
				
			if (eleInput != null) {
				
				YFCIterable<YFCElement> allInputline = eleInput.getChildren();
				for (YFCElement inputEle : allInputline){
					if(inputEle.hasAttribute("System_Serial_No")){
						sSystemSerialNo=inputEle.getAttribute("System_Serial_No");	
						sDate=inputEle.getAttribute("Expiration_Date");
						System.out.println("#####sDate --- "+ sDate); 
					}
					
					if(sSystemSerialNo !=null){
						
						final YFCDocument inputDoc = YFCDocument.getDocumentFor("<OrderLine DocumentType=\"\"> <Extn SystemSerialNo=\"\"/></OrderLine>");
						inputDoc.getDocumentElement().setAttribute("DocumentType", "0017.ex");
						inputDoc.getDocumentElement().getChildElement("Extn").setAttribute("SystemSerialNo", sSystemSerialNo);
						
						final YFCDocument templateDoc = YFCDocument.getDocumentFor("<OrderLineList><OrderLine OrderLineKey=\"\"/></OrderLineList>");
						env.setApiTemplate("getOrderLineList", templateDoc.getDocument());
						outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,"getOrderLineList",inputDoc.getDocument());
						System.out.println("######### Output of OrderlineList--------->"+outDoc1.toString());
						YFCElement sOrderLineList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
						
						if(!YFCElement.isVoid(sOrderLineList)){
							YFCIterable<YFCElement> allOrderLines = sOrderLineList.getChildren();
							for (YFCElement sOrderLineEle : allOrderLines){
								sOrderLineKey = sOrderLineEle.getAttribute("OrderLineKey");
								System.out.println("#########OrderLineKey taken from IB");
								if(!YFCElement.isVoid(sOrderLineKey)){
									final YFCDocument terDoc = YFCDocument.getDocumentFor("<TerSCIBMapping TerIBOLKey=\"\" ></TerSCIBMapping>");
									terDoc.getDocumentElement().setAttribute("TerIBOLKey",sOrderLineKey);
																
									terOutDoc = YIFClientFactory.getInstance().getLocalApi().executeFlow(env,"GetIBForLine",terDoc.getDocument());
									YFCElement sTerSCIBMappingList = YFCDocument.getDocumentFor(terOutDoc).getDocumentElement();
									System.out.println("######### Called GetIBForLine");
									if(!YFCElement.isVoid(sTerSCIBMappingList)){
										
										YFCIterable<YFCElement> terLines = sTerSCIBMappingList.getChildren();
										for (YFCElement sTerSCIBLines : terLines){
											YFCElement sSCLines = sTerSCIBLines.getChildElement("YFSSCOrderLine");
											if(!YFCElement.isVoid(sSCLines)){
												sMaxLineStatus=sSCLines.getAttribute("MaxLineStatus");
												if(sMaxLineStatus.equals("1100.200")|| sMaxLineStatus.equals("1100.300")|| sMaxLineStatus.equals("1100.400")){
													System.out.println("######### Status Check done");
												
												YFCElement extnOrdSCEle = sSCLines.getChildElement("Extn");
												
												if(!YFCElement.isVoid(extnOrdSCEle)){
													//sMktStatus = extnOrdSCEle.getAttribute("MktStatus");
													//sSystemType= extnOrdSCEle.getAttribute("SystemType");
													sRepairablePartsCovered=extnOrdSCEle.getAttribute("RepairablePartsCovered");
													sNonrepairPartsCovered=extnOrdSCEle.getAttribute("NonrepairPartsCovered");
													if("Y".equalsIgnoreCase(sRepairablePartsCovered) && "Y".equalsIgnoreCase(sNonrepairPartsCovered))
														sCovFlag="B";
													if("Y".equalsIgnoreCase(sRepairablePartsCovered) && "N".equalsIgnoreCase(sNonrepairPartsCovered))
														sCovFlag="R";
													if("N".equalsIgnoreCase(sRepairablePartsCovered) && "Y".equalsIgnoreCase(sNonrepairPartsCovered))
														sCovFlag="C";
													//sSCLineStatus=extnOrdSCEle.getAttribute("SCLineStatus");
												}
												YFCElement orderSCEle = sSCLines.getChildElement("Order");
												if(!YFCElement.isVoid(orderSCEle)){
													sOrderNo= orderSCEle.getAttribute("OrderNo");
													String[] str_array1 = sOrderNo.split("-");
													int sLength = str_array1.length;
													if(sLength > 1){
													stringc = str_array1[0];
													stringd = str_array1[1];
													}
													if (sLength == 1){
														stringc = str_array1[0];
														stringd = "";
													}
													
													YFCElement orderExtnSCEle = orderSCEle.getChildElement("Extn");
													if(!YFCElement.isVoid(orderExtnSCEle)){
														sExpirationDate=orderExtnSCEle.getAttribute("ExpirationDate");
														System.out.println("######### ExpirationDate::::::" +sExpirationDate);
													}
												}
												//  for checking with the input date
												if(sExpirationDate != null || !sExpirationDate.isEmpty()){
												String subString = sExpirationDate.substring(0,10);
												SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
												SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
												Date newFormatedDate = formatter.parse(subString);
												String expDateString=dateFormat.format(newFormatedDate);
												
												//for checking the expiration date to current date
												java.text.DateFormat dateComp = new java.text.SimpleDateFormat("yyyy-MM-dd");
											    Date currentDate = java.util.Calendar.getInstance().getTime();
												Date curDate = dateComp.parse(dateComp.format(currentDate));;	
												Date ExpDate = dateComp.parse(sExpirationDate.substring(0, 10));
												System.out.println("######### date com1:: " + expDateString.compareTo(sDate) +"expDateString::::"+expDateString + "sDate::"+sDate );
												System.out.println("######### date com2 " + curDate.compareTo(ExpDate) + "curDate:::"+ curDate + "ExpDate::::"+ ExpDate);
												//"A".compareToIgnoreCase(sSCLineStatus) == 0 && --- removed this condition check
												if(expDateString.compareTo(sDate)==0 && curDate.compareTo(ExpDate) <= 0){
													System.out.println("######### Inside if condition");
													YFCElement itemDetSCEle = sSCLines.getChildElement("ItemDetails");
													if(!YFCElement.isVoid(itemDetSCEle)){
														sItemID = itemDetSCEle.getAttribute("ItemID");
														YFCElement itemExtnSCEle = itemDetSCEle.getChildElement("Extn");
														if(!YFCElement.isVoid(itemExtnSCEle)){
															sRespProdMfgDivCode=  itemExtnSCEle.getAttribute("RespProdMfgDivCode");
															sSystemTesterGroup=  itemExtnSCEle.getAttribute("SystemTesterGroup");
															sSystemPTExceptionFlag=  itemExtnSCEle.getAttribute("SystemPTExceptionFlag");
														}
													}
													
													//To get System Type from IB
													final YFCDocument inputDoc1 = YFCDocument.getDocumentFor("<OrderLineDetail OrderLineKey=\"\"/>");
													inputDoc1.getDocumentElement().setAttribute("OrderLineKey", sOrderLineKey);
																									
													final YFCDocument templateDoc1 = YFCDocument.getDocumentFor("<OrderLine OrderLineKey=\"\"><Item ItemID=\"\"/></OrderLine>");
													env.setApiTemplate("getOrderLineDetails", templateDoc1.getDocument());
													outDoc2 = YIFClientFactory.getInstance().getLocalApi().invoke(env,"getOrderLineDetails",inputDoc1.getDocument());
													System.out.println("######### Output of OrderlineDetails-------->"+outDoc2.toString());
													YFCElement sOrderLineDet = YFCDocument.getDocumentFor(outDoc2).getDocumentElement();
													YFCElement itemDet = sOrderLineDet.getChildElement("Item");
													if(!YFCElement.isVoid(itemDet)){
														sSystemType= itemDet.getAttribute("ItemID");
														System.out.println("Item ID from IB = " + sSystemType);
													}
															
													System.out.println("######### Output generation");
													YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("SystemSerialNoServiceType");
													finalOutputEle.setAttribute("Service_Type", sItemID);
													finalOutputEle.setAttribute("Entitlement_Number", stringc);
													finalOutputEle.setAttribute("Entitlement_Type", stringd);
													finalOutputEle.setAttribute("Resp_Part_Mfg_Div", sRespProdMfgDivCode);
													finalOutputEle.setAttribute("Tester_Group", sSystemTesterGroup);
													finalOutputEle.setAttribute("PT_Exception_Flag", sSystemPTExceptionFlag);
													finalOutputEle.setAttribute("System_Type", sSystemType);
													finalOutputEle.setAttribute("Part_Coverage_Flag", sCovFlag);
													
												}
												}
												}
											}
										}// End of FOR loop for each SC lines 
									}
								}
								
							}//end of FOR loop for each order line from IB
						}
					} //end of IF for each system serial no
					
				}// end of FOR for each Input line
			}
		}
		catch(Exception e)
		 {
			//throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		 }
		
		
		return outDoc.getDocument();
	}
		
}

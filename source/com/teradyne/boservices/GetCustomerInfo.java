package com.teradyne.boservices;

//import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;


public class GetCustomerInfo {

	YFCDocument outDoc = YFCDocument.createDocument("Output");
	YFCDocument inYFCXML = null;
	String CustNo = "";
	String OrgCode = "";
	String CusType = "";
	String apiName1 = "getCustomerList";
	String apiName2 = "getLocaleList";
	Document outDoc1 = null;
	Document outDoc2 = null;
	Document outDoc5 =null;
	
	YFCDocument outputDoc =null;
	
	String LocaleDesc = "";
	String CustId="";
	String Org="";
	
	String SpecialCustInstr="";
	String SpecialInstrVisible="";
	String IsTransExceptionAllowed="";
	String InternalCustomerFlag="";
	
	
	String SiteID="";
	String TimeZoneOffset="";
	String OrganizationName="";
	String CustomerSiteStatus="";
	
	String CustomerID="";
	

	public Document getCustomerInfo(YFSEnvironment env, Document inxml){
		try {
		inYFCXML = YFCDocument.getDocumentFor(inxml);
		YFCElement NodeList = inYFCXML.getDocumentElement();
		
		YFCElement Node = NodeList.getChildElement("GetCustomerInfo");
	
		
		if(Node != null){
			if (Node.hasAttribute("PartyNumber") && Node.hasAttribute("PartyAddressNumber") ){
					CustNo = Node.getAttribute("PartyNumber");
					SiteID = Node.getAttribute("PartyAddressNumber");
					CustId= CustNo +"-"+ SiteID;
					 Org="CSO";
			}
			
		}

	
		final YFCDocument inputDoc = YFCDocument.getDocumentFor("<Customer CustomerIDQryType=\"FLIKE\" CustomerID=\"\" OrganizationCode=\"\" CustomerType=\"\" />");
		inputDoc.getDocumentElement().setAttribute("CustomerID", CustId);
		inputDoc.getDocumentElement().setAttribute("OrganizationCode", Org);
		

		final YFCDocument templateDoc = YFCDocument
				.getDocumentFor("<CustomerList><Customer CustomerID=\"\">"
						+ "<Extn TimeZoneOffset=\"\" SpecialCustInstr=\"\" SpecialInstrVisible=\"\" IsTransExceptionAllowed=\"\" InternalCustomerFlag=\"\"  />"
						+ "<BuyerOrganization  OrganizationName=\"\">" 
						+ "<Extn CustomerSiteStatus=\"\" />"
						+ "</BuyerOrganization>"
						+ "</Customer></CustomerList>");
				env.setApiTemplate(apiName1, templateDoc.getDocument());
			

				outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName1,inputDoc.getDocument());
	
			if (outDoc1 != null){
				
				YFCElement OutputCustList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
				if (OutputCustList != null) {

					// for loop for each customer
					YFCIterable<YFCElement> custline = OutputCustList.getChildren();
				
					for (YFCElement custLineEle : custline){
						CustomerID = custLineEle.getAttribute("CustomerID");
						YFCElement custExtnEle = custLineEle.getChildElement("Extn");
						 if(custExtnEle !=null){
							 
							 SpecialCustInstr = custExtnEle.getAttribute("SpecialCustInstr");
							 SpecialInstrVisible = custExtnEle.getAttribute("SpecialInstrVisible");
							 IsTransExceptionAllowed = custExtnEle.getAttribute("IsTransExceptionAllowed");
							 InternalCustomerFlag = custExtnEle.getAttribute("InternalCustomerFlag");
							 TimeZoneOffset=custExtnEle.getAttribute("TimeZoneOffset");
						 }
						
						 YFCElement custBuyerEle = custLineEle.getChildElement("BuyerOrganization");
						 if(custBuyerEle!=null){
							
							 OrganizationName=custBuyerEle.getAttribute("OrganizationName");
							 
						 }						
			
						final YFCDocument localeInput = YFCDocument.getDocumentFor("<TerTimeZone TerTimeZone=\"\" />");
						localeInput.getDocumentElement().setAttribute("TerTimeZone",TimeZoneOffset);
						outDoc2=YIFClientFactory.getInstance().getLocalApi().executeFlow(env,"getTerTimeZoneList",localeInput.getDocument());
						

						if (outDoc1 != null){
							LocaleDesc=YFCDocument.getDocumentFor(outDoc2).getDocumentElement().getChildElement("TerTimeZone").getAttribute("TerOffsetHours");
						}
			
						YFCElement buyerExtnEle = custBuyerEle.getChildElement("Extn");
						if(buyerExtnEle!=null){
							CustomerSiteStatus= buyerExtnEle.getAttribute("CustomerSiteStatus");
						}
						
					
				
						
					
				if(CustomerSiteStatus.equalsIgnoreCase("A")){
					
					YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetCustomerInfo");
				
					finalOutputEle.setAttribute("TimeZoneForCustomerSite", LocaleDesc);
					
					finalOutputEle.setAttribute("InvoicePrompt",SpecialCustInstr);
					finalOutputEle.setAttribute("InvoicePromptCustomerDisplayableFlag", SpecialInstrVisible);
					finalOutputEle.setAttribute("PTExceptionFlag", IsTransExceptionAllowed);
					finalOutputEle.setAttribute("CustomerName", OrganizationName);
					finalOutputEle.setAttribute("InternalCustomerFlag", InternalCustomerFlag);

				}
				}	//for end
				}
				
			}
		
		
		}
		catch(Exception e)
		 {
			//throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		 }
		return outDoc.getDocument();
	}

}

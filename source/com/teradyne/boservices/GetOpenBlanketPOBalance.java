package com.teradyne.boservices;

import org.w3c.dom.Document;
import java.util.Date;
import java.util.Properties;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetOpenBlanketPOBalance implements YIFCustomApi {

	String sExpDate = null;
	String sEffdate = null;
	String apiName1 = "getOrderLineList";
	String sEffDate = null;
	String poBal = "";
	java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");

	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	public Document getOpenBlanketPOBalance(YFSEnvironment env, Document inDoc) {
		Document outDoc = null;
		YIFApi yifc;
		try {
			yifc = YIFClientFactory.getInstance().getLocalApi();
			YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);

			YFCDocument templateDoc1 = YFCDocument
					.getDocumentFor("<OrderLineList> <OrderLine OrderLineKey=\"\"> "
							+ "<Order ReceivingNode=\"\" OrderNo=\"\" OrderType=\"\"> <Extn EffectiveDate=\"\""
							+ " ExpirationDate=\"\"/> <PriceInfo Currency=\"\"/> </Order>" 
							+ "<Extn OpenBalance=\"\" />"
							+ "</OrderLine></OrderLineList>");
			env.setApiTemplate(apiName1, templateDoc1.getDocument());

			System.out.println("Invoking API...OrderLineListAPI");
			outDoc = yifc.invoke(env, apiName1, inputDoc.getDocument());
			if (outDoc != null) {
				YFCNodeList<YFCElement> NodeList = YFCDocument.getDocumentFor(outDoc).getElementsByTagName("OrderLine");

				for (int i = 0; i < NodeList.getLength(); i++) {
					System.out.println("Iteration: " + i);
					YFCNode cNode = NodeList.item(i);
					YFCElement element = (YFCElement) cNode;
					System.out.println(element.toString());
					YFCElement ordEle = element.getChildElement("Order");

					Date currentDate = java.util.Calendar.getInstance().getTime();
					Date curDate = dateFormat.parse(dateFormat.format(currentDate));
					Boolean bInvalidOrderLine = true;

					if (ordEle != null) {
						YFCElement extEle = ordEle.getChildElement("Extn");
						if (extEle != null) {
							sEffDate = extEle.getAttribute("EffectiveDate");
							sExpDate = extEle.getAttribute("ExpirationDate");

							if (sEffDate != null && sExpDate != null) {
								Date ExpDate = dateFormat.parse(sExpDate.substring(0, 10));
								Date EffDate = dateFormat.parse(sEffDate.substring(0, 10));

								if ((curDate.compareTo(ExpDate) <= 0) && (EffDate.compareTo(curDate) <= 0)) {
									YFCElement ordLineExtnEle = element.getChildElement("Extn");

									if (ordLineExtnEle != null) {
											poBal = ordLineExtnEle.getAttribute("OpenBalance");
											if (poBal != null && Double.parseDouble(poBal) > 0) {
												bInvalidOrderLine = false;
											}

										}
									}

								}
							}
						}
					

					if (bInvalidOrderLine) {
						element.getParentNode().removeChild(cNode);
						i--;
					}

				}

			}
			
			

		} catch (Exception exception) {

			exception.printStackTrace();
			System.out.println("######### Exception Caught : " + exception.toString());
			//throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		}
		return outDoc;
	}

}

package com.teradyne.boservices;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfs.japi.YFSEnvironment;


public class ErrorSurpress implements YIFCustomApi {
	String apiName=""; String filename="";
	public void setProperties(Properties prop) throws Exception {
		apiName= prop.getProperty("apiName");
		filename=prop.getProperty("fileName");
		//System.out.println("apiName form the arg"+ apiName);
	}

	public Document errorSurpress(YFSEnvironment env, Document inDoc){
		YIFApi yifc;
		Document outDoc=null;
		//Document templateDoc=null;
	
		try{
			
			 /*File file = new File(filename);
			// System.out.println("file to read from "+ file);
			 FileReader fin = new FileReader(file);*/
			if(!"multiApi".equals(apiName)){
			
			 InputStream file = ErrorSurpress.class.getResourceAsStream(filename);

			 BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file));
			 
			 StringBuilder sb = new StringBuilder();
			 String str= "";
			 while ((str = bufferedReader.readLine()) != null) {
		          sb.append(str);
		      }
			// System.out.println("File output"+ sb.toString());
			 YFCDocument templateDoc = YFCDocument.getDocumentFor(sb.toString());
			// System.out.println("template doc-----"+ templateDoc.toString());
			 
		
			
			env.setApiTemplate(apiName, templateDoc.getDocument());
			//System.out.println("Inside template set");
			}
			yifc = YIFClientFactory.getInstance().getLocalApi();
			outDoc = yifc.invoke(env,apiName,inDoc);
			//System.out.println("Called the api ........");
			
		}
		catch (Exception E)
		{
			
			outDoc=  YFCDocument.createDocument("Output").getDocument();
			//System.out.println("In the catch block ........");
			E.printStackTrace();
		}
	
		
		return outDoc;
	}

	
	
}

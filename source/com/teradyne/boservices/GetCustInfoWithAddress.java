package com.teradyne.boservices;

//import java.rmi.RemoteException;
import java.util.Properties;

import org.w3c.dom.Document;

//import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
//import com.yantra.pca.ycd.jasperreports.returnOrderSummaryReportScriptlet;
//import com.yantra.shared.wms.WMSLiterals;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetCustInfoWithAddress implements YIFCustomApi {

	YFCDocument outDoc = YFCDocument.createDocument("Output");
	YFCDocument inYFCXML = null;
	String CustNo = "";
	String OrgCode = "";
	String CusType = "";
	String apiName1 = "getCustomerList";
	String apiName2 = "getLocaleList";
	Document outDoc1 = null;
	Document outDoc2 = null;
	Document outDoc5 =null;
	String locale = "";
	YFCDocument outputDoc =null;
	String sCID="";
	String LocaleDesc = "";
	String CustId="";
	String Org="";
	String IsServiceHold="";
	String SpecialCustInstr="";
	String SpecialInstrVisible="";
	String IsTransExceptionAllowed="";
	String InternalCustomerFlag="";
	String IsCreditHold="";
	String LocaleCode="";
	String TimeZoneOffset="";
	String OrganizationName="";
	String CustomerSiteStatus="";
	String AddressCountry=""; String AddressLine1=""; String AddressLine2="";
	String AddressLine3="";String AddressLine4="";String AddressLine5="";
	String AddressCity=""; String AddressZipCode=""; String AddressState="";
	String CustomerID="";String CountryName="";String sBillId="";
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	public Document getCustInfo(YFSEnvironment env, Document inxml){
		try {
		inYFCXML = YFCDocument.getDocumentFor(inxml);
		YFCElement NodeList = inYFCXML.getDocumentElement();
		
		YFCElement Node = NodeList.getChildElement("GetCustomerInfoWithAddress");
	//	YFCElement Node = inYFCXML.getElementById("GetCustomerInfoWithAddress");
		
		if(Node != null){
			if (Node.hasAttribute("CustomerNumber") && Node.hasAttribute("SiteID") && Node.hasAttribute("SiteType")){
					CustNo = Node.getAttribute("CustomerNumber");
					OrgCode = Node.getAttribute("SiteID");
					CusType = Node.getAttribute("SiteType");
					CustId= CustNo +"-"+ OrgCode;
					 Org="CSO";
			}
			if(Node.hasAttribute("CustomerNumber")&& Node.hasAttribute("SiteType")&& Node.getAttribute("SiteID").isEmpty()){
			CustId =Node.getAttribute("CustomerNumber");
			CustId = CustId +"-";
			CusType = Node.getAttribute("SiteType");
			Org="CSO";
			}
		}

	
		final YFCDocument inputDoc = YFCDocument.getDocumentFor("<Customer CustomerIDQryType=\"FLIKE\" CustomerID=\"\" OrganizationCode=\"\" CustomerType=\"\" />");
		inputDoc.getDocumentElement().setAttribute("CustomerID", CustId);
		inputDoc.getDocumentElement().setAttribute("OrganizationCode", Org);
		//inputDoc.getDocumentElement().setAttribute("CustomerType", CusType);

		final YFCDocument templateDoc = YFCDocument
				.getDocumentFor("<CustomerList><Customer CustomerID=\"\">"
						+ "<Extn TimeZoneOffset=\"\" SpecialCustInstr=\"\" SpecialInstrVisible=\"\" IsTransExceptionAllowed=\"\" InternalCustomerFlag=\"\" IsCreditHold=\"\" IsServiceHold=\"\" />"
						+ "<BuyerOrganization LocaleCode=\"\" OrganizationName=\"\">" 
						+ "<Extn CustomerSiteStatus=\"\"><TerCustBillingOrgList><TerCustBillingOrg TerBillingID=\"\" TerDefaultFLag=\"\" TerAddressLine1=\"\" TerAddressLine2=\"\" TerAddressLine3=\"\" TerAddressLine4=\"\" TerAddressLine5=\"\" TerCity=\"\" TerState=\"\" TerCountry=\"\" TerZipCode=\"\"/></TerCustBillingOrgList></Extn>"
						+ "<CorporatePersonInfo AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\" City=\"\" "
						+	"State=\"\" ZipCode=\"\" Country=\"\" />"
						+ "</BuyerOrganization>"
						+ "</Customer></CustomerList>");
				env.setApiTemplate(apiName1, templateDoc.getDocument());
			

				outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName1,inputDoc.getDocument());
	
			if (outDoc1 != null){
				
				YFCElement OutputCustList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
				if (OutputCustList != null) {
					
					if(CusType.equalsIgnoreCase("SHIP TO")){
					
					

					// for loop for each customer
					YFCIterable<YFCElement> custline = OutputCustList.getChildren();
				
					for (YFCElement custLineEle : custline){
						CustomerID = custLineEle.getAttribute("CustomerID");
						YFCElement custExtnEle = custLineEle.getChildElement("Extn");
						 if(custExtnEle !=null){
							 
							 SpecialCustInstr = custExtnEle.getAttribute("SpecialCustInstr");
							 SpecialInstrVisible = custExtnEle.getAttribute("SpecialInstrVisible");
							 IsTransExceptionAllowed = custExtnEle.getAttribute("IsTransExceptionAllowed");
							 InternalCustomerFlag = custExtnEle.getAttribute("InternalCustomerFlag");
							 IsCreditHold = custExtnEle.getAttribute("IsCreditHold");
							 IsServiceHold = custExtnEle.getAttribute("IsServiceHold");
							 TimeZoneOffset=custExtnEle.getAttribute("TimeZoneOffset");
						 }
						
						 YFCElement custBuyerEle = custLineEle.getChildElement("BuyerOrganization");
						 if(custBuyerEle!=null){
						
							 OrganizationName=custBuyerEle.getAttribute("OrganizationName");
							 
						 }						
			
							final YFCDocument localeInput = YFCDocument.getDocumentFor("<TerTimeZone TerTimeZone=\"\" />");
							localeInput.getDocumentElement().setAttribute("TerTimeZone",TimeZoneOffset);
							final YFCDocument templateDoc1 = YFCDocument.getDocumentFor("<TerTimeZoneList><TerTimeZone TerOffsetHours=\"\" TerTimeZone=\"\" TerTimeZoneKey=\"\"/></TerTimeZoneList>");
							env.setApiTemplate("getTerTimeZoneList", templateDoc1.getDocument());
							outDoc2=YIFClientFactory.getInstance().getLocalApi().executeFlow(env,"getTerTimeZoneList",localeInput.getDocument());
							

							if (outDoc1 != null){
								LocaleDesc=YFCDocument.getDocumentFor(outDoc2).getDocumentElement().getChildElement("TerTimeZone").getAttribute("TerOffsetHours");
							}
				
			
						YFCElement buyerExtnEle = custBuyerEle.getChildElement("Extn");
						if(buyerExtnEle!=null){
							CustomerSiteStatus= buyerExtnEle.getAttribute("CustomerSiteStatus");
						}
						
						YFCElement billEle = custBuyerEle.getChildElement("CorporatePersonInfo");
						if(billEle !=null){
							AddressLine1=billEle.getAttribute("AddressLine1");
							AddressLine2=billEle.getAttribute("AddressLine2");
							AddressLine3=billEle.getAttribute("AddressLine3");
							AddressLine4=billEle.getAttribute("AddressLine4");
							AddressLine5=billEle.getAttribute("AddressLine5");
							AddressCity=billEle.getAttribute("City");
							AddressState=billEle.getAttribute("State");
							AddressZipCode=billEle.getAttribute("ZipCode");
							AddressCountry=billEle.getAttribute("Country");
						}
						
						if(AddressCountry !=null){
							CountryName=countryName(AddressCountry,env);
						}
						
					
				if(CustomerSiteStatus.equalsIgnoreCase("A")){
					
					YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetCustomerInfoWithAddress");
				
					finalOutputEle.setAttribute("TimeZoneOffsetFromGMT", LocaleDesc);
					finalOutputEle.setAttribute("InvoicePrompt",SpecialCustInstr);
					finalOutputEle.setAttribute("InvoicePromptCustomerDisplayableFlag", SpecialInstrVisible);
					if("Y".equalsIgnoreCase(IsTransExceptionAllowed))
						finalOutputEle.setAttribute("PTExceptionFlag", IsTransExceptionAllowed);
					else
						finalOutputEle.setAttribute("PTExceptionFlag", "");
					finalOutputEle.setAttribute("CustomerName", OrganizationName);
					finalOutputEle.setAttribute("InternalCustomerFlag", InternalCustomerFlag);
					finalOutputEle.setAttribute("CreditHoldFlag",IsCreditHold);
					finalOutputEle.setAttribute("ServiceHoldFlag",IsServiceHold);
					finalOutputEle.setAttribute("AddressLine1", AddressLine1);
					finalOutputEle.setAttribute("AddressLine2", AddressLine2);
					finalOutputEle.setAttribute("AddressLine3", AddressLine3);
					finalOutputEle.setAttribute("AddressLine4", AddressLine4);
					finalOutputEle.setAttribute("AddressLine5", AddressLine5);
					finalOutputEle.setAttribute("AddressCity", AddressCity);
					finalOutputEle.setAttribute("AddressState", AddressState);
					finalOutputEle.setAttribute("AddressZipCode", AddressZipCode);
					finalOutputEle.setAttribute("AddressCountry", CountryName);
				
					String[] str_array = CustomerID.split("-");
				
					 String CID = str_array[0]; 
					String  sCID = str_array[1];
			
					finalOutputEle.setAttribute("CustomerNumber", CID);
					finalOutputEle.setAttribute("CustomerSiteId", sCID);

				}
				}	//for end
					}//End of SHIP to Check
					
					if(CusType.equalsIgnoreCase("BILL TO")){
						YFCIterable<YFCElement> custline = OutputCustList.getChildren();
						
						for (YFCElement custLineEle : custline){
							CustomerID = custLineEle.getAttribute("CustomerID");
							YFCElement custExtnEle = custLineEle.getChildElement("Extn");
							 if(custExtnEle !=null){
								 
								 SpecialCustInstr = custExtnEle.getAttribute("SpecialCustInstr");
								 SpecialInstrVisible = custExtnEle.getAttribute("SpecialInstrVisible");
								 IsTransExceptionAllowed = custExtnEle.getAttribute("IsTransExceptionAllowed");
								 InternalCustomerFlag = custExtnEle.getAttribute("InternalCustomerFlag");
								 IsCreditHold = custExtnEle.getAttribute("IsCreditHold");
								 IsServiceHold = custExtnEle.getAttribute("IsServiceHold");
								 TimeZoneOffset=custExtnEle.getAttribute("TimeZoneOffset");
							 }
							
							 YFCElement custBuyerEle = custLineEle.getChildElement("BuyerOrganization");
							 if(custBuyerEle!=null){
							
								 OrganizationName=custBuyerEle.getAttribute("OrganizationName");
								 
							 }						
				
								final YFCDocument localeInput = YFCDocument.getDocumentFor("<TerTimeZone TerTimeZone=\"\" />");
								localeInput.getDocumentElement().setAttribute("TerTimeZone",TimeZoneOffset);
								final YFCDocument templateDoc1 = YFCDocument.getDocumentFor("<TerTimeZoneList><TerTimeZone TerOffsetHours=\"\" TerTimeZone=\"\" TerTimeZoneKey=\"\"/></TerTimeZoneList>");
								env.setApiTemplate("getTerTimeZoneList", templateDoc1.getDocument());
								
								outDoc2=YIFClientFactory.getInstance().getLocalApi().executeFlow(env,"getTerTimeZoneList",localeInput.getDocument());
								

								if (outDoc1 != null){
									LocaleDesc=YFCDocument.getDocumentFor(outDoc2).getDocumentElement().getChildElement("TerTimeZone").getAttribute("TerOffsetHours");
								}
					
				
							YFCElement buyerExtnEle = custBuyerEle.getChildElement("Extn");
							if(buyerExtnEle!=null){
								CustomerSiteStatus= buyerExtnEle.getAttribute("CustomerSiteStatus");
							}
							YFCElement terElelist = buyerExtnEle.getChildElement("TerCustBillingOrgList");
							if(!YFCElement.isVoid(terElelist)){
							YFCIterable<YFCElement> allTerOrglist = terElelist.getChildren();
							for (YFCElement terlineEle : allTerOrglist){
								sBillId=terlineEle.getAttribute("TerBillingID");
								AddressLine1 = terlineEle.getAttribute("TerAddressLine1");
								AddressLine2 = terlineEle.getAttribute("TerAddressLine2");
								AddressLine3 = terlineEle.getAttribute("TerAddressLine3");
								AddressLine4 = terlineEle.getAttribute("TerAddressLine4");
								AddressLine5 = terlineEle.getAttribute("TerAddressLine5");
								AddressCity = terlineEle.getAttribute("TerCity");
								AddressState = terlineEle.getAttribute("TerState");
								AddressCountry = terlineEle.getAttribute("TerCountry");
								AddressZipCode = terlineEle.getAttribute("TerZipCode");
								
								if(AddressCountry !=null){
									CountryName=countryName(AddressCountry,env);
								}
								
								if(CustomerSiteStatus.equalsIgnoreCase("A")){
									
									YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetCustomerInfoWithAddress");
								
									finalOutputEle.setAttribute("TimeZoneOffsetFromGMT", LocaleDesc);
									finalOutputEle.setAttribute("InvoicePrompt",SpecialCustInstr);
									finalOutputEle.setAttribute("InvoicePromptCustomerDisplayableFlag", SpecialInstrVisible);
									if("Y".equalsIgnoreCase(IsTransExceptionAllowed))
										finalOutputEle.setAttribute("PTExceptionFlag", IsTransExceptionAllowed);
									else
										finalOutputEle.setAttribute("PTExceptionFlag", "");
									finalOutputEle.setAttribute("CustomerName", OrganizationName);
									finalOutputEle.setAttribute("InternalCustomerFlag", InternalCustomerFlag);
									finalOutputEle.setAttribute("CreditHoldFlag",IsCreditHold);
									finalOutputEle.setAttribute("ServiceHoldFlag",IsServiceHold);
									finalOutputEle.setAttribute("AddressLine1", AddressLine1);
									finalOutputEle.setAttribute("AddressLine2", AddressLine2);
									finalOutputEle.setAttribute("AddressLine3", AddressLine3);
									finalOutputEle.setAttribute("AddressLine4", AddressLine4);
									finalOutputEle.setAttribute("AddressLine5", AddressLine5);
									finalOutputEle.setAttribute("AddressCity", AddressCity);
									finalOutputEle.setAttribute("AddressState", AddressState);
									finalOutputEle.setAttribute("AddressZipCode", AddressZipCode);
									finalOutputEle.setAttribute("AddressCountry", CountryName);
									String[] str_array1 = sBillId.split("-");
									String stringc = str_array1[0];
									String stringd = str_array1[1];
									finalOutputEle.setAttribute("CustomerNumber", stringc);
									finalOutputEle.setAttribute("CustomerSiteId", stringd);
								}
						
							}//for each ters
							}
				
						}//For Each cust
					}// end of Bill To Check
		
		
		}
			}
		}//end of try
		catch(Exception e)
		 {
			//throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		 }
		return outDoc.getDocument();
	}
				
				
				private String countryName(String countryCode, YFSEnvironment env1) throws Exception{
					String apiName1 = "getCommonCodeList";
					String CountryName="";
					YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" />");
					inputDoc.getDocumentElement().setAttribute("CodeValue", countryCode);
					inputDoc.getDocumentElement().setAttribute("CodeType", "COUNTRY");
					outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env1, apiName1, inputDoc.getDocument());
					env1.clearApiTemplate(apiName1);
					
					if (outDoc1 !=null){
						YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
						if (OutputCommList != null){
							YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
							if(eleComm !=null){
								CountryName= eleComm.getAttribute("CodeLongDescription");
								System.out.println("CountryName Desc@@@@@@@@@@ "+CountryName);
							}
						}
					}
						return CountryName;
					}
}

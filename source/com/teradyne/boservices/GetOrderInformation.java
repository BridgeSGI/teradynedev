package com.teradyne.boservices;

//import java.text.SimpleDateFormat;
//import java.util.Date;

import org.w3c.dom.Document;

//import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetOrderInformation {

	public Document getOrderInformation(YFSEnvironment env, Document inDoc)
			throws Exception {
		
		YFCDocument outDoc = YFCDocument.createDocument("Output");
		Document outDoc1 = null;
		YFCDocument inputOrder = null;
		YFCDocument TemplateOrder=null;
		Document outputOrder = null;
		String OrderNo="";
		String OrderApi="getOrderDetails";
		//String ShortDescription ="";
		String PrimeLineNo=""; String OrderedQty="";
		String CustomerPONo="";String ShipNode="";String Status="";
		String OriginalTotalAmount="";String ItemID="";String RepairCode="";
		String SystemFailureDistList="";
		//String DefaultInventoryCenter="";
		String Currency="";
		String SystemSerialNo="";String SystemType ="";String STOExpediteListDiscount="";
		//String DurationDays="";
		String PartRepairPrice="";String PartHandlingCharge="";String ServiceTypeDescn="";
		String BusinessDaysToDueDate="";
		String WarrantyNo="";
		//String OLTotalPrice="";
		String ServiceType="";
		String sDefaultOrderCenter="";
		String sReceivingNode="";
		String ProdSerialNumber="";String InstructionText="";String InstructionType="";String CurrencyDescription="";
		//String CodeShortDescription="";
		String WarrantyCode="";String WarrantyCodeDescn="";String RepairCodeDescription="";
		try {
			
			
			YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleInput = inYFCDoc.getDocumentElement();
			
			
			YFCElement eleGetOrderInfo = eleInput.getChildElement("GetOrderInformation");
			if (eleGetOrderInfo != null){
				OrderNo = eleGetOrderInfo.getAttribute("OrderNumber");
			}
			
			inputOrder = YFCDocument.getDocumentFor("<Order EnterpriseCode=\"\" DocumentType=\"\" OrderNo=\"\"/>");
			
			inputOrder.getDocumentElement().setAttribute("OrderNo", OrderNo);
			inputOrder.getDocumentElement().setAttribute("EnterpriseCode", "CSO");
			inputOrder.getDocumentElement().setAttribute("DocumentType", "0001");
			
			TemplateOrder = YFCDocument.getDocumentFor("<Order ReceivingNode=\"\"><OrderLines><OrderLine PrimeLineNo=\"\"  OrderedQty=\"\" ShipNode=\"\"  CustomerPONo=\"\" Status=\"\"><Order OriginalTotalAmount=\"\"><PriceInfo Currency=\"\"/><Extn DurationDays=\"\"/></Order><Extn SystemSerialNo=\"\" SystemType=\"\" STOExpediteListDiscount=\"\" PartRepairPrice=\"\" PartHandlingCharge=\"\" BusinessDaysToDueDate=\"\" WarrantyNo=\"\" OLTotalPrice=\"\" ServiceType=\"\" ProdSerialNumber=\"\"/>" 
			     +			 "<ItemDetails ItemID=\"\"><PrimaryInformation ShortDescription=\"\"/><Extn WarrantyCode=\"\" RepairCode=\"\" SystemFailureDistList=\"\" DefaultInventoryCenter=\"\"/><ItemInstructionList><ItemInstruction InstructionText=\"\" InstructionType=\"\"/></ItemInstructionList></ItemDetails></OrderLine></OrderLines> </Order>");

			env.setApiTemplate(OrderApi,TemplateOrder.getDocument());
			outputOrder = YIFClientFactory.getInstance().getLocalApi().invoke(env, OrderApi,inputOrder.getDocument());
			env.clearApiTemplate(OrderApi);
			YFCElement OutputOrderEleList = YFCDocument.getDocumentFor(outputOrder).getDocumentElement();
			if (OutputOrderEleList != null) {
				 
				 sReceivingNode= OutputOrderEleList.getAttribute("ReceivingNode");
				 String apiName="getCustomerDetails";
				 final YFCDocument inDoc1 = YFCDocument.getDocumentFor("<Customer CustomerID=\"\" OrganizationCode=\"\" />");
				 inDoc1.getDocumentElement().setAttribute("CustomerID", sReceivingNode);
				 inDoc1.getDocumentElement().setAttribute("OrganizationCode", "CSO");
					
					final YFCDocument templateDoc = YFCDocument.getDocumentFor("<Customer CustomerID=\"\"><Extn DefaultOrderCenter=\"\"/></Customer>");
					env.setApiTemplate(apiName, templateDoc.getDocument());
					outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName,inDoc1.getDocument());
					
					YFCElement CustElementList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
					if (CustElementList != null){
						
										
					YFCElement custExtnEle = CustElementList.getChildElement("Extn");
					
					if(custExtnEle!=null){
						sDefaultOrderCenter = custExtnEle.getAttribute("DefaultOrderCenter");
					
					}
					}
							

				 
				YFCElement eleOrderLines = OutputOrderEleList.getChildElement("OrderLines");
				
				if(!YFCElement.isVoid(eleOrderLines)){
					
					YFCIterable<YFCElement> allOrdlines = eleOrderLines.getChildren();
					for (YFCElement eleOrderLine : allOrdlines){
			
					PrimeLineNo = eleOrderLine.getAttribute("PrimeLineNo");
					OrderedQty = eleOrderLine.getAttribute("OrderedQty");
					ShipNode = eleOrderLine.getAttribute("ShipNode");
					CustomerPONo = eleOrderLine.getAttribute("CustomerPONo");
					Status = eleOrderLine.getAttribute("Status");
					
				
				
					YFCElement eleOrder = eleOrderLine.getChildElement("Order");
					if(eleOrder != null){
				
						OriginalTotalAmount = eleOrder.getAttribute("OriginalTotalAmount");
					}
				
					YFCElement elePriceInf = eleOrder.getChildElement("PriceInfo");
					if(elePriceInf != null){
						Currency =elePriceInf.getAttribute("Currency");
					}
				
					if(Currency !=null){
						
						String apiName1 = "getCurrencyList";
						YFCDocument inputDoc = YFCDocument.getDocumentFor("<Currency Currency=\"\" />");
						inputDoc.getDocumentElement().setAttribute("Currency", Currency);
						outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
						env.clearApiTemplate(apiName1);
						
						if (outDoc1 !=null){
							YFCElement OutputCurrList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
							if (OutputCurrList != null){
								YFCElement eleCurr = OutputCurrList.getChildElement("Currency");
								if(eleCurr !=null){
									CurrencyDescription= eleCurr.getAttribute("CurrencyDescription");
									System.out.println("Curr Desc@@@@@@@@@@ "+CurrencyDescription);
								}
							}
						
						}
					}
				
					/*YFCElement elePriExt = elePriceInf.getChildElement("Extn");
					if(elePriExt !=null){
						DurationDays=elePriExt.getAttribute("DurationDays");
					}*/
					YFCElement eleOrdlExt = eleOrderLine.getChildElement("Extn");
					if(eleOrdlExt !=null){
						SystemSerialNo=eleOrdlExt.getAttribute("SystemSerialNo");
						SystemType=eleOrdlExt.getAttribute("SystemType");
						STOExpediteListDiscount=eleOrdlExt.getAttribute("STOExpediteListDiscount");
						PartRepairPrice=eleOrdlExt.getAttribute("PartRepairPrice");
						PartHandlingCharge=eleOrdlExt.getAttribute("PartHandlingCharge");
						BusinessDaysToDueDate=eleOrdlExt.getAttribute("BusinessDaysToDueDate");
						WarrantyNo=eleOrdlExt.getAttribute("WarrantyNo");
						//OLTotalPrice=eleOrdlExt.getAttribute("OLTotalPrice");
						ServiceType=eleOrdlExt.getAttribute("ServiceType");
						ProdSerialNumber=eleOrdlExt.getAttribute("ProdSerialNumber");
						
					}
				
					if(ServiceType !=null && !ServiceType.isEmpty()){
						String apiName1 = "getCommonCodeList";
						YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" OrganizationCode=\"\"/>");
						inputDoc.getDocumentElement().setAttribute("CodeValue", ServiceType);
						inputDoc.getDocumentElement().setAttribute("CodeType", "Service_Type");
						inputDoc.getDocumentElement().setAttribute("OrganizationCode", "CSO");
						outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
						env.clearApiTemplate(apiName1);
						
						if (outDoc1 !=null){
							YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
							if (OutputCommList != null){
								YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
								if(eleComm !=null){
									ServiceTypeDescn= eleComm.getAttribute("CodeShortDescription");
									System.out.println("Serv Desc@@@@@@@@@@ "+ServiceTypeDescn);
								}
							}
						}
					
					}
				
				
					YFCElement eleItem = eleOrderLine.getChildElement("ItemDetails");
					if(eleItem !=null){
						ItemID= eleItem.getAttribute("ItemID");
								
					}
					/*YFCElement elePri = eleItem.getChildElement("PrimaryInformation");
					if(elePri !=null){
						 ShortDescription = elePri.getAttribute("ShortDescription");
					}*/
					
					YFCElement eleExtn = eleItem.getChildElement("Extn");
					if(eleExtn !=null){
						RepairCode = eleExtn.getAttribute("RepairCode");
						SystemFailureDistList = eleExtn.getAttribute("SystemFailureDistList");
						//DefaultInventoryCenter = eleExtn.getAttribute("DefaultInventoryCenter");
						WarrantyCode = eleExtn.getAttribute("WarrantyCode");
					}
					
					if(WarrantyCode !=null && !WarrantyCode.isEmpty()){
						String apiName1 = "getCommonCodeList";
						YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" OrganizationCode=\"\" />");
						inputDoc.getDocumentElement().setAttribute("CodeValue", WarrantyCode);
						inputDoc.getDocumentElement().setAttribute("CodeType", "Warranty_Code");
						inputDoc.getDocumentElement().setAttribute("OrganizationCode", "CSO");
						outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
						env.clearApiTemplate(apiName1);
						
						if (outDoc1 !=null){
							YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
							if (OutputCommList != null){
								YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
								if(eleComm !=null){
									WarrantyCodeDescn= eleComm.getAttribute("CodeShortDescription");
									System.out.println("WarrantyCodeDescn Desc@@@@@@@@@@ "+WarrantyCodeDescn);
									
								}
							}
						}
					
					}
					if(RepairCode !=null && !RepairCode.isEmpty()){
						String apiName2 = "getCommonCodeList";
						YFCDocument inputDoc2 = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" OrganizationCode=\"\" />");
						inputDoc2.getDocumentElement().setAttribute("CodeValue", RepairCode);
						inputDoc2.getDocumentElement().setAttribute("CodeType", "Repair_Code");
						inputDoc2.getDocumentElement().setAttribute("OrganizationCode", "CSO");
						outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName2, inputDoc2.getDocument());
						env.clearApiTemplate(apiName2);
						
						if (outDoc1 !=null){
							YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
							if (OutputCommList != null){
								YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
								if(eleComm !=null){
									RepairCodeDescription= eleComm.getAttribute("CodeShortDescription");
									System.out.println("RepairCodeDescription Desc@@@@@@@@@@ "+RepairCodeDescription);
								}
							}
						}
					}
					
				
					YFCElement eleInstList = eleItem.getChildElement("ItemInstructionList");
					if(eleInstList !=null){
						
						YFCIterable<YFCElement> allInstrline = eleInstList.getChildren();
						for (YFCElement instrEle : allInstrline){
							InstructionType=instrEle.getAttribute("InstructionType");
							
							String noteText= instrEle.getAttribute("InstructionText");
							InstructionText= InstructionText +"  " +noteText;
					
						}
					}
					
				
			
				YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetOrderInformation");
				finalOutputEle.setAttribute("SystemSerialNo", SystemSerialNo);
				finalOutputEle.setAttribute("OrderLineNo", PrimeLineNo);
				finalOutputEle.setAttribute("SystemTypeCode", SystemType);
				finalOutputEle.setAttribute("PartNumber", ItemID);
				finalOutputEle.setAttribute("LineOrderQuantity", OrderedQty);
				finalOutputEle.setAttribute("UnitPriceExpediting", STOExpediteListDiscount);
				finalOutputEle.setAttribute("UnitPriceRepair", PartRepairPrice);
				finalOutputEle.setAttribute("UnitPriceHandling", PartHandlingCharge);
				finalOutputEle.setAttribute("TotalAmount", OriginalTotalAmount);
				finalOutputEle.setAttribute("CurrencyCode", Currency);
				finalOutputEle.setAttribute("CurrencyCodeDesc", CurrencyDescription);
				finalOutputEle.setAttribute("PartRepairCodeDescn", RepairCode);
				finalOutputEle.setAttribute("PartRepairDescn", RepairCodeDescription);
				finalOutputEle.setAttribute("ResponsibleCenter", ShipNode);
				
				//String bdueDate =modifyDateLayout(BusinessDaysToDueDate);
				finalOutputEle.setAttribute("BusinessDaystoDueDate", BusinessDaysToDueDate);
				
				finalOutputEle.setAttribute("EntitlementNumber", WarrantyNo);
				finalOutputEle.setAttribute("WarrantyCodeDescn", WarrantyCodeDescn);
				finalOutputEle.setAttribute("FailureDistList", SystemFailureDistList);
				finalOutputEle.setAttribute("OrderServiceCenter", sDefaultOrderCenter);
				finalOutputEle.setAttribute("PartSerialNumber", ProdSerialNumber);
				finalOutputEle.setAttribute("PartNotes", InstructionText);
				finalOutputEle.setAttribute("CustomerDisplayablePartNotesFlag", InstructionType);
				if("Backordered".equalsIgnoreCase(Status))
					finalOutputEle.setAttribute("BackOrderFlag", "Y");
				else
					finalOutputEle.setAttribute("BackOrderFlag", "");	
				finalOutputEle.setAttribute("PurchaseOrderNumber", CustomerPONo);
				finalOutputEle.setAttribute("ServiceTypeCode", ServiceType);
				finalOutputEle.setAttribute("ServiceTypeDescription", ServiceTypeDescn);
				
				
				
				}//end of for orderlines
				}
			}//end of if orderdetails
		}//end of try
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("######### Exception Caught : " + e.toString());
			//throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}
		
		return outDoc.getDocument();
	}


}

package com.teradyne.boservices;

import java.util.Properties;

import org.w3c.dom.Document;
//import org.w3c.dom.Element;

import com.yantra.interop.japi.YIFApi;
//import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
//import com.yantra.yfs.japi.YFSException;

public class GetValidPartInfo implements YIFCustomApi {

	
	String sItemId = null;
	String apiName1 = "getItemList";
	String apiName2 = "getCommonCodeList";
	YFCDocument outDoc = YFCDocument.createDocument("Output");
	Document outDoc1 = null;
	Document outDoc2 = null;
	Document outDoc3 = null;
	Document outDoc4 = null;
	YFCDocument outputDoc = null;
	YFCDocument finalOutDoc = null;
	String StatusCode = null;
	String ExpFlag = null;
	String StatusDesc = null;
	String ServiceTypeCode =null;
	String ServiceTypeCodeDesc="";
	String ItemID=null;
	String ShortDescription=null;
	String  MaxOrderQuantity= null;
	String RepairCode =null;
	String ReplacedByProduct=null;
	String OemSerialRequiredFlag =null;
	String RespProdMfgDivCode=null;
	String StandardsWithOverhead=null;
	String InstructionType =null;
	String InstructionText = null;
	int NodeLength = 0;
	
	
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}
	
	public Document getValidPartInfo(YFSEnvironment env, Document inDoc){
		
		YIFApi yifc;
		try {
			yifc = YIFClientFactory.getInstance().getLocalApi();

			YFCElement inEle = YFCDocument.getDocumentFor(inDoc).getDocumentElement();
			YFCElement IterEle = null;
			YFCNodeList<YFCElement> NodeList = inEle.getElementsByTagName("GetValidPartInfo");
			
			NodeLength = NodeList.getLength();
			//YFCElement NodeList = inEle.getChildElement("GetValidPartInfo");
			finalOutDoc = YFCDocument.createDocument("Output");
			
			if(NodeList != null){
				
				for (int i = 0; i < NodeLength; i++){
					
					final YFCNode cNode = NodeList.item(i);
					
					if (cNode.getNodeName().equalsIgnoreCase("GetValidPartInfo")) {
					IterEle= (YFCElement) cNode;
					//if(inEle.getChildElement("GetValidPartInfo").hasAttribute("PartNumber")){
						sItemId = IterEle.getAttribute("PartNumber");
						
						YFCDocument inputDoc1 = YFCDocument.getDocumentFor("<Item ItemID=\"\" OrganizationCode=\"\" />");
						inputDoc1.getDocumentElement().setAttribute("ItemID", sItemId);
						inputDoc1.getDocumentElement().setAttribute("OrganizationCode", "CSO");
					
						YFCDocument templateDoc1 = YFCDocument.getDocumentFor("<ItemList>" +
								"<Item ItemID=\"\">" +
								"<PrimaryInformation MaxOrderQuantity=\"\" ShortDescription=\"\" />" +
								"<ItemInstructionList>" +
								"<ItemInstruction InstructionType=\"\" InstructionText=\"\"/>" +
								"</ItemInstructionList>" +
								"<Extn RepairCode=\"\" SupportStatusCode=\"\"" +
								" ReplacedByProduct=\"\" RespProdMfgDivCode=\"\"" +
								" OemSerialRequiredFlag=\"\"  " +
								" StandardsWithOverhead=\"\" />" +
								"</Item></ItemList>");
						env.setApiTemplate(apiName1, templateDoc1.getDocument());
						outDoc1 = yifc.invoke(env, apiName1, inputDoc1.getDocument());
						YFCElement YFCoutEle1 = YFCDocument.getDocumentFor(outDoc1).getDocumentElement();
						
						YFCElement itemEle = YFCoutEle1.getChildElement("Item");
						 if(itemEle !=null) {
						
							ItemID = itemEle.getAttribute("ItemID");
							
							YFCElement elePriInfo = itemEle.getChildElement("PrimaryInformation");
							if(elePriInfo !=null){
								MaxOrderQuantity = elePriInfo.getAttribute("MaxOrderQuantity");
								ShortDescription = elePriInfo.getAttribute("ShortDescription");
							}
							YFCElement eleExtn = itemEle.getChildElement("Extn");
							if(eleExtn !=null){
								RepairCode =eleExtn.getAttribute("RepairCode");
								ReplacedByProduct=eleExtn.getAttribute("ReplacedByProduct");
								RespProdMfgDivCode=eleExtn.getAttribute("RespProdMfgDivCode");
								OemSerialRequiredFlag=eleExtn.getAttribute("OemSerialRequiredFlag");
								StandardsWithOverhead=eleExtn.getAttribute("StandardsWithOverhead");
								StatusCode = eleExtn.getAttribute("SupportStatusCode");
							}
							YFCElement eleItemInstrList = itemEle.getChildElement("ItemInstructionList");
							if (!YFCElement.isVoid(eleItemInstrList)){
								YFCIterable<YFCElement> allInstrline = eleItemInstrList.getChildren();
								for (YFCElement eleItemInstr : allInstrline){
								
									InstructionType =eleItemInstr.getAttribute("InstructionType");
									String sInstructionText =eleItemInstr.getAttribute("InstructionText");
									InstructionText= InstructionText +"  "+ sInstructionText;
								}
								
								
							}
							
								if(StatusCode !=null && !StatusCode.isEmpty()){
								YFCDocument inputDoc2 = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" />");
								inputDoc2.getDocumentElement().setAttribute("CodeType", "Ord_Service_Type_Det");
								inputDoc2.getDocumentElement().setAttribute("CodeValue", StatusCode);
								
								outDoc2 = yifc.invoke(env, apiName2, inputDoc2.getDocument());
								env.clearApiTemplate(apiName2);
								
								if(YFCDocument.getDocumentFor(outDoc2).getDocumentElement().getChildElement("CommonCode").hasAttribute("CodeShortDescription")){
									StatusDesc = YFCDocument.getDocumentFor(outDoc2).getDocumentElement().getChildElement("CommonCode").getAttribute("CodeShortDescription");
									
									YFCDocument inputDoc3 = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" OrganizationCode=\"\" />");
									inputDoc3.getDocumentElement().setAttribute("CodeType", StatusCode);
									inputDoc3.getDocumentElement().setAttribute("OrganizationCode", "CSO");
									
									outDoc3 = yifc.invoke(env, apiName2, inputDoc3.getDocument());
									env.clearApiTemplate(apiName2);
									
									if (outDoc3 !=null){
										YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc3).getDocumentElement();
										if (OutputCommList != null) {

											// for loop for each common code
											YFCIterable<YFCElement> allCommline = OutputCommList.getChildren();
										
											for (YFCElement commLineEle : allCommline){
											  ServiceTypeCode= commLineEle.getAttribute("CodeValue");
											  System.out.println("Before the service type code thing");
											  YFCDocument inputDoc4 = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" />");
												// not wrking for Service_Type
											  	inputDoc4.getDocumentElement().setAttribute("CodeType", "Service_Type");
												inputDoc4.getDocumentElement().setAttribute("CodeValue", ServiceTypeCode);
												outDoc4 = yifc.invoke(env, apiName2, inputDoc4.getDocument());
												env.clearApiTemplate(apiName2);
												System.out.println("Inside the service type code thing");
												if(outDoc4 !=null){
													YFCElement OutCommList = YFCDocument.getDocumentFor(outDoc4).getDocumentElement();
													if(OutCommList.hasChildNodes()){
														YFCElement outComm = OutCommList.getChildElement("CommonCode");
														ServiceTypeCodeDesc =outComm.getAttribute("CodeShortDescription");
														System.out.println("ServiceTypeCodeDesc#########" +ServiceTypeCodeDesc);
													}
												}
												// need to check this
												if( ServiceTypeCode.equals("RPD") || ServiceTypeCode.equals("EPUR") 	|| ServiceTypeCode.equals("EPS") || ServiceTypeCode.equals("SDS") ||
														ServiceTypeCode.equals("EAR") ){
													ExpFlag = "E";
												}
												else{
													ExpFlag = "N";
												}
													
												YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetValidPartInfo");	
												finalOutputEle.setAttribute("PartNumber", ItemID);
												finalOutputEle.setAttribute("PartDescription", ShortDescription);
												
												if(itemEle.getChildElement("ItemInstructionList")!= null && itemEle.getChildElement("ItemInstructionList").getChildElement("ItemInstruction")!=null && itemEle.getChildElement("ItemInstructionList").getChildElement("ItemInstruction").hasAttribute("InstructionText"))
													finalOutputEle.setAttribute("Notes", InstructionText);
												else
													finalOutputEle.setAttribute("Notes","");
																							
												if(itemEle.getChildElement("ItemInstructionList")!= null && itemEle.getChildElement("ItemInstructionList").getChildElement("ItemInstruction")!=null && itemEle.getChildElement("ItemInstructionList").getChildElement("ItemInstruction").hasAttribute("InstructionType"))
													finalOutputEle.setAttribute("PartNotesCustomerDisplayableFlag", InstructionType);
												else
													finalOutputEle.setAttribute("PartNotesCustomerDisplayableFlag", "");
												
												finalOutputEle.setAttribute("RepairCode", RepairCode);
												finalOutputEle.setAttribute("ServiceTypeCode", ServiceTypeCode);
												finalOutputEle.setAttribute("ServiceTypeDescription", ServiceTypeCodeDesc);
												finalOutputEle.setAttribute("ReplacementPart", ReplacedByProduct);
												finalOutputEle.setAttribute("RespProductMfgDivision", RespProdMfgDivCode);
												finalOutputEle.setAttribute("MaxAllowableOrderQuantity", MaxOrderQuantity);
												finalOutputEle.setAttribute("SupportStatusCode", StatusDesc);
												finalOutputEle.setAttribute("OEMSerialReq", OemSerialRequiredFlag);
												finalOutputEle.setAttribute("SWOHUnitPrice", StandardsWithOverhead);
												
												finalOutputEle.setAttribute("NoAgreementCoverageFlag", "A");//nedd to get mapping
												finalOutputEle.setAttribute("ExpressOrNonExpress", ExpFlag);//nedd to get mapping
		
											
											}// for each service in the item
											}
											}
										
										
										
									
											}// status code check end
								}
						 }
					}
				}// for multiple inputs
			}
		} // end of try
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("######### Exception Caught : " + e.toString());
			//throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}
	
		
		
		
		return outDoc.getDocument();
	}

}

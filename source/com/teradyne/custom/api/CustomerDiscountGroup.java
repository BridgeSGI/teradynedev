package com.teradyne.custom.api;

import java.io.IOException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class CustomerDiscountGroup {
	
	/** Input to this method 
	 *<TerDiscountGroup TerSystemType="ALL" TerServiceType="EAR" 
	 * TerCustomerName="" TerCustomerNo="24765-00" TerDiscountPercentage="" TerDiscountGroupId="H"/>
	
	public void createCustomerDiscountRecords(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		YFCDocument discountGroupDoc = YFCDocument.getDocumentFor(inputDoc);
		YFCElement discountGroupEle = discountGroupDoc.getDocumentElement();
		String customerNoStr = discountGroupEle.getAttribute("TerCustomerNo");
		
		String getOrgListTemplate = "<OrganizationList><Organization OrganizationCode=\"\" OrganizationName=\"\" /></OrganizationList>";
		YFCDocument getOrgListInDoc = YFCDocument.createDocument("Organization");
		getOrgListInDoc.getDocumentElement().setAttribute("OrganizationCode", customerNoStr);
		YFCDocument getOrgListOutDoc = SterlingUtil.callAPI(env, "getOrganizationList", getOrgListInDoc, getOrgListTemplate);
		
		String customerNameStr = getOrgListOutDoc.getElementsByTagName("Organization").item(0).getAttribute("OrganizationName");
		discountGroupEle.setAttribute("TerCustomerName", customerNameStr);
		
		String systemTypeStr = discountGroupEle.getAttribute("TerSystemType");
		systemTypeStr.replaceAll("%", "ALL");
		discountGroupEle.setAttribute("TerSystemType", systemTypeStr);
		
		
		YFCDocument outDoc = SterlingUtil.callAPI(env, "createTerDiscountGroup",discountGroupDoc, "");
		 
		return;
	}*/
}

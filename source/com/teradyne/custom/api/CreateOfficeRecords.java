package com.teradyne.custom.api;

import java.io.IOException;
import java.rmi.RemoteException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.XMLConstants;
import com.teradyne.utils.Constants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class CreateOfficeRecords {
	/** Input to this method 
	 * <TEROfficeRecordsList>
    		<TEROfficeRecords TerClarifyWorkgroup="" TerEngineerLaborRate=""
        		TerGsoRevenueRegion="" TerOfficeCode="1" TerOfficeCountry=""
        		TerOfficeName="" TerOfficeStatus="A" TerOfficeType="X"
        		TerOpsCoordinator="" TerPriceZone="US" TerRegionalManager=""
        		TerServiceLocation="" TerServiceOffice="" TerServiceRegion="" TerWorldwideRegion=""/>
		</TEROfficeRecordsList> 
	
	public void createOfficeRecords(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		YFCElement officeRecordEle = YFCDocument.getDocumentFor(inputDoc).getElementsByTagName(XMLConstants.TER_OFFICE_RECORDS).item(0);
		
		String codeValue = officeRecordEle.getAttribute(XMLConstants.TER_OFFICE_COUNTRY);
		String countryCodeStr = getCommonCodeShortDesc(env, "MERLIN_COUNTRY", codeValue, Constants.CSO);
		officeRecordEle.setAttribute(XMLConstants.TER_OFFICE_COUNTRY, countryCodeStr);
		
		YFCDocument outDoc = SterlingUtil.callAPI(env, Constants.CREATE_TER_OFFICE_RECORDS, officeRecordEle.getOwnerDocument(), "");
		return;
	}
	
	private String getCommonCodeShortDesc(YFSEnvironment env, String codeType, String codeValue, String orgCode) throws YIFClientCreationException, SAXException, IOException{
		YFCDocument inDoc = YFCDocument.createDocument(XMLConstants.COMMON_CODE);
		inDoc.getDocumentElement().setAttribute(XMLConstants.CODE_TYPE, codeType);
		inDoc.getDocumentElement().setAttribute(XMLConstants.CODE_VALUE, codeValue);
		inDoc.getDocumentElement().setAttribute(XMLConstants.ORGANIZATION_CODE, orgCode);
		
		
		String commonCodeStr = "<CommonCodeList><CommonCode CodeType=\"\" CodeValue=\"\" "
				+ "CodeShortDescription=\"\" /></CommonCodeList>";
		YFCDocument commonCodeTemplate = YFCDocument.parse(commonCodeStr); 
		YFCDocument outDoc = SterlingUtil.callAPI(env, Constants.GET_COMMON_CODE_LIST, inDoc, commonCodeTemplate);
		
		return outDoc.getElementsByTagName(XMLConstants.COMMON_CODE).item(0).getAttribute(XMLConstants.CODE_SHORT_DESC);
	}
	
*/
}

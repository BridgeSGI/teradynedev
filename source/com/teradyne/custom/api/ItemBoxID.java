package com.teradyne.custom.api;

import java.io.IOException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class ItemBoxID {
	
	/** Input to this method Ask Mark if all the hard-coding/logic is required to be done by us.
	 *
	
	public void createItemBoxID(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		YFCDocument itemBoxDoc = YFCDocument.getDocumentFor(inputDoc);
		YFCElement itemBoxEle = itemBoxDoc.getElementsByTagName(XMLConstants.ITEM).item(0);
		itemBoxEle.setAttribute(XMLConstants.ORGANIZATION_CODE, Constants.CSO);
		itemBoxEle.setAttribute(XMLConstants.IS_SHIPPING_CNTR, Constants.YES);
		itemBoxEle.setAttribute(XMLConstants.UNIT_OF_MEASURE, Constants.EACH);
		
		YFCElement primaryInfoEle = itemBoxEle.getElementsByTagName(XMLConstants.PRIMARY_INFORMATION).item(0);
		primaryInfoEle.setAttribute(XMLConstants.DEFAULT_PRODUCT_CLASS, Constants.GOOD);
		primaryInfoEle.setAttribute(XMLConstants.IS_AIR_SHIP_ALLOWED, Constants.YES);
		primaryInfoEle.setAttribute(XMLConstants.IS_SHIP_ALLOWED, Constants.YES);
		primaryInfoEle.setAttribute(XMLConstants.IS_DEL_ALLOWED, Constants.YES);
		primaryInfoEle.setAttribute(XMLConstants.IS_PICKUP_ALLOWED, Constants.YES);
		primaryInfoEle.setAttribute(XMLConstants.IS_RETURNABLE, Constants.YES);
		primaryInfoEle.setAttribute(XMLConstants.IS_SUB_ON_ORD_ALLOWED, Constants.YES);
		primaryInfoEle.setAttribute(XMLConstants.ITEM_TYPE, Constants.BOXID);
		return;
	}*/
}

package com.teradyne.utils;

public class Constants {

 	public static final String DOCUMENT_TYPE_IB = "0017.ex";
  public static final String DOCUMENT_TYPE_SC = "0018.ex";
 	public static final String DOCUMENT_TYPE_SO = "0001";
	public static final String DOCUMENT_TYPE_TO = "0006";
	public static final Object DOCUMENT_TYPE_RO = "0003";
  public static final String RELEASED = "3200";
	
	
 	//SERVICE TYPES
	public static final String SDS_SERVICE_TYPE = "SDS";
	public static final String USL_SERVICE_TYPE = "USL";
  
	public static final int NO_OF_DAYS_BEFORE = -7;
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm:ss";
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String TIME_FORMAT = "hh:mm:ss";
	public static final String FROM_TIME_STAMP = "T00:00:00";
	public static final String TO_TIME_STAMP = "T23:59:59";
  
	public static final String TD_BUS_GROUP = "TeradyneBusinessGroup";
	public static final String SO_SYSTEM_TYPE = "SystemType";
	// Error Description
  public static final String OFFLINE_SHIP_NODE_ERROR =
      "Infinite Inventory Ship Node is not configured for this EnterpriseCode";

	// Bo Service
	public static final String SPWP_GET_EMPLOYEE_COST_CENTER_SERVICE = "SPWPGetEmployeeCostCenter";
	public static final String SPWP_GET_COST_CENTER_SERVICE = "SPWPGetCostCenter";
  public static final String SPWP_IS_VAILD_SYSTEM_SERIAL = "SPWPIsValidSystemSerial";
  public static final String SPWP_GET_OPEN_BLANKET_PO_BALANCE = "SPWPGetOpenBlanketPOBalance";
  public static final String SPWP_IS_RPD_ELIGIBLE = "SPWPIsRPDEligible";
  public static final String SPWP_CAN_ORDER_CONSUMMABLE = "SPWPCanOrderConsummable";
  public static final String SPWP_CHECK_OPPO_SUPPORT = "SPWPCheckOPPOSupport";
  
	// Currency
	public static final String CUR_US = "USD";
	public static final String CUR_CHINA = "CNY";
	public static final String CUR_KOREA = "KRW";
	public static final String CUR_JAPAN = "JPY";
	public static final String CUR_SPANISH = "EUR";
  
	public static final String ORDER_ENTRY_MANUAL = "Manual";
	public static final String GET_LOCATION_INVENTORY_AUDIT_LIST = "getLocationInventoryAuditList";
	public static final String GET_SHIP_NODE_INVENTORY = "getShipNodeInventory";
	public static final String GET_ITEM_NODE_DEFN_LIST = "getItemNodeDefnList";
	public static final String GET_TER_SCT_VW_LIST = "getTerSCTVwList";
	public static final String GET_SC_TEMPLATE_LIST = "GetSCTemplateList";
	public static final String GET_SC_TEMPLATE = "GetSCTemplate";
	public static final String MANAGE_SC_TEMPLATE = "ChangeSCTemplate";
	public static final String CREATE_SC_TEMPLATE = "CreateSCTemplate";
	public static final String CREATE_SERVICE_ITEM = "createTerServiceItem";
	public static final String MODIFY_SERVICE_ITEM = "changeTerServiceItem";
	public static final String DEL_SER_ITEM_API = "deleteTerServiceItem";
	public static final String CHANGE_ORDER_API = "changeOrder";
	public static final String GET_TER_CUST_BILLING_ORG_LIST_API = "getTerCustBillingOrgList";
	public static final String GET_USER_LIST_API = "getUserList";
	public static final String DEL_SC_TEMPLATE_API = "deleteTerSerContTemplate";
	public static final String DEL_SER_ITEM_SERVICE = "DeleteServiceItemsFromSCT";
	public static final String GET_SERVICE_ITEM_LIST = "GetServiceItemsList";
	public static final String MULTI_API = "multiApi";
	public static final String GET_ITEM_DETAILS_API = "getItemDetails";
	public static final String GET_SR_LIST = "GetSRList";
	public static final String GET_TER_SR_VW_LIST = "GetTERSRVwList";
	public static final String GET_PROPERTY_API = "getProperty";
		
	public static final String ZERO = "0";
	public static final String ONHAND = "ONHAND";
	public static final String INTRANSIT = "INTRANSIT";
	public static final String OUTBOUND = "OUTBOUND.ex";
	public static final String WO_PLACED = "WO_PLACED";
	public static final String ALLOCATED = "ALLOCATED";
	public static final String BACKORDERED = "BACKORDERED";
	
	public static final String ORDER_LINE = "OrderLine";
	public static final String EXCHANGE_TYPE = "ExchangeType";
	public static final String ORDER_DATE = "OrderDate";
	public static final String ORDER_DATE_QRY_TYPE = "OrderDateQryType";
	public static final String DRAFT_ORDER_FLAG = "DraftOrderFlag";
	public static final String MAXIMUM_RECORDS = "MaximumRecords";
	public static final String DOCUMENT_TYPE = "DocumentType";
	public static final String ORDER = "Order";
	public static final String PO_RELEASED = "PO_RELEASED";
	public static final String PO_SCHEDULED = "PO_SCHEDULED";
	public static final String PO_BACKORDER = "PO_BACKORDER";
	public static final String PO_PLACED = "PO_PLACED";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	public static final String PRODUCT_CLASS = "ProductClass";
	public static final String SHIP_DATE = "ShipDate";
	public static final String CONSIDER_ALL_SEGMENTS = "ConsiderAllSegments";
	public static final String CONSIDER_ALL_NODES = "ConsiderAllNodes";
	public static final String ENTERPRISE_CODE = "EnterpriseCode";
	
	public static final boolean TRUE = true;
	public static final boolean FALSE = false;
	public static final String NO = "N";
	public static final String GE = "GE";
	public static final String ADVANCED = "ADVANCED";
	public static final String GET_ORDER_LIST = "getOrderList";
  public static final String GET_ORDER_LINE_LIST = "getOrderLineList";
	public static final String ORDERING_UOM = "OrderingUOM";
	public static final String EACH = "EACH";
  	public static final String BETWEEN = "BETWEEN";
	
	public static final String ORDERLINE = "OrderLine";
	public static final String API_GET_COMMON_CODE_LIST = "getCommonCodeList";
	public static final String CODESHORTDESCRIPTION = "CodeShortDescription";
	public static final String CODEVALUE = "CodeValue";
	public static final String ITEMDETAILS = "ItemDetails";
	public static final String REPAIRCODE = "RepairCode";
	public static final String EXPIRATIONDATEQRYTYPE = "ExpirationDateQryType";
	public static final String EXPIRATIONDATE = "ExpirationDate";
	public static final String COMMONCODELIST = "CommonCodeList";
	public static final String CODETYPE = "CodeType";
	public static final String COMMONCODE = "CommonCode";
	public static final String REPAIR_CODE = "Repair_Code";
	public static final String ORGANIZATIONCODE = "OrganizationCode";
	public static final String WARRANTEE_CODE = "Warrantee_Code";
	public static final String WARRANTEECODE = "WarranteeCode";
	public static final String REPAIRCODEDESC = "RepairCodeDesc";
	public static final String WARRANTYCODEDESC = "WarrantyCodeDesc";
	public static final String REQSHIPDATE = "ReqShipDate";
	public static final String BUSINESSDAYSTODUEDATE = "BusinessDaysToDueDate";
	public static final String ENTERPRISECODE = "EnterpriseCode";
	
  // BEGIN :: constants used by SPWPGetFDLSEmailWebService java code [Tushar]
	public static final String SYSTEM_TYPE_CODE = "System_Type_Code";
	public static final String SYSTEM_SERIAL_NO = "System_Serial_No";
	public static final String SYSTEM_FAILURE_DIST_LIST = "SystemFailureDistList";
  // END
	
	public static final String YES = "Y";
	public static final String CSO = "CSO";

	
	public static final String N = "N";
	public static final String Y = "Y";
	
	public static final String OPERATION = "Operation";
	public static final String OPERATION_CREATE = "Create";
	public static final String OPERATION_DELETE = "Delete";
	
	public static final String CATALOG_ORGANIZATION_CODE = "CatalogOrganizationCode";
	public static final String CREATOR_ORGANIZATION_KEY = "CreatorOrganizationKey";
	public static final String INVENTORY_PUBLISHED = "InventoryPublished";
	public static final String PRIMARY_ENTERPRISE_KEY = "PrimaryEnterpriseKey";
	public static final String CORPORATE_PERSON_INFO = "CorporatePersonInfo";
	
  // Bill
	public static final String EXTN = "Extn";
	public static final String EXTN_Customer_Billing_org = "TerCustBillingOrg";
	public static final String PREFIX = "Ter";
	public static final String COUNTRY = "Country";
	public static final String ORGANIZATION = "Organization";
	public static final String ORGANIZATION_KEY = "OrganizationKey";
	public static final String ORGANIZATION_CODE = "OrganizationCode";
	public static final String ORG_ROLE_LIST = "OrgRoleList";
	public static final String ORG_ROLE = "OrgRole";
	public static final String ROLE_KEY = "RoleKey";
	public static final String BUYER = "BUYER";
	public static final String LOCALE_CODE = "LocaleCode";
	public static final String ORGANIZATION_NAME = "OrganizationName";
	public static final String PARENT_ORGANIZATION_CODE = "ParentOrganizationCode";
	public static final String PARENT_ORGANIZATION_NAME = "ParentOrganizationName";
	public static final String PERSON_INFO_KEY = "PersonInfoKey";
	
	public static final String COUNTRY_US = "US";

	public static final String API_MANAGE_ORGANIZATION_HIERARCHY = "manageOrganizationHierarchy";
	public static final String API_GET_ORGANIZATION_HIERARCHY = "getOrganizationHierarchy";
	public static final String API_MANAGE_CUSTOMER = "manageCustomer";
	public static final String API_GET_CUSTOMER_LIST = "getCustomerList";
	public static final String API_GET_CUSTOMER_DETAILS = "getCustomerDetails";
	public static final String API_GET_ORGANIZATION_LIST = "getOrganizationList";
	public static final String SCHEDULEORDER_API = "scheduleOrder";
	public static final String RELEASEORDER_API = "releaseOrder";
	public static final String CREATESHIPMENT_API = "createShipment";
	public static final String CONFIRMSHIPMENT_API = "confirmShipment";
  public static final String CONFIRM_DRAFT_ORDER_API="confirmDraftOrder";
	
	public static final String BILLING_PERSON_INFO = "BillingPersonInfo";
	public static final String CUSTOMER = "Customer";
	public static final String CUSTOMER_ID = "CustomerID";
	public static final String CUSTOMER_TYPE = "CustomerType";
	public static final String BUYER_ORGANIZATION_CODE = "BuyerOrganizationCode";
	public static final String CUSTOMER_ADDITIONAL_ADDRESS_LIST = "CustomerAdditionalAddressList";
	public static final String CUSTOMER_ADDITIONAL_ADDRESS = "CustomerAdditionalAddress";
	public static final String IS_BILL_TO = "IsBillTo";
	public static final String IS_DEFAULT_BILL_TO = "IsDefaultBillTo";
	public static final String IS_SHIP_TO = "IsShipTo";
	public static final String IS_DEFAULT_SHIP_TO = "IsDefaultShipTo";
	public static final String PERSON_INFO = "PersonInfo";
	
	public static final String TEMP = "Temp";
	public static final String ADDRESS_TYPE = "AddressType";
	public static final String ADDRESS = "Address";
	public static final String NODE = "Node";
	public static final String NODE_TYPE = "NodeType";
	public static final String INTERFACE_TYPE = "InterfaceType";
	public static final String ENTERPRISE_ORG_LIST = "EnterpriseOrgList";
	public static final String ORG_ENTERPRISE = "OrgEnterprise";
	public static final String ENTERPRISE_ORGANIZATION_KEY = "EnterpriseOrganizationKey";
	public static final String ENTERPRISE_ORGANIZATION_NAME = "EnterpriseOrganizationName";
	public static final String SUB_ORGANIZATION = "SubOrganization";
	public static final String ROLES = "Roles";
	public static final String IS_NODE = "IsNode";
	
	public static final String TER_SERVICE_ITEM = "TER_SERVICE_ITEM";
	public static final String TER_SERVICE_CONTRACT_TEMPLATE = "TER_SERVICE_CONTRACT_TEMPLATE";
	public static final String TER_SCT_SELECTION_RULE = "TER_SCT_SELECTION_RULE";
	public static final String TER_SCT_SELECTION_RULE_LINE = "TER_SCT_SELECTION_RULE_LINE";
	
	public static final String GET_AUDIT_LIST = "getAuditList";
	public static final String MODIFICATION_TYPE = "ModificationType";
	public static final String MODIFICATION_LEVEL = "ModificationLevel";
	public static final String OUTPUT = "Output";
	public static final String SEARCH_CUSTOMER = "SearchCustomer";
	public static final String ROLE_LIST = "RoleList";
	public static final String CUSTOMER_PREFIX = "Cust";
	public static final String MANAGE_SEL_RULE = "ManageSelectionRule";
	public static final String CREATE_SEL_RULE_LINE = "createTerSelectionRuleLine";
	public static final String CREATE_ORDER = "createOrder";
	
	public static final String PATH = "global/template/jasper/Quote.jrxml";
  public static final String LOGO_IMAGE_PATH = "global/template/jasper/Quote_logo.jpg";
	public static final String TERMS_FIRST_PAGE = "global/template/jasper/terms.jrxml";
	public static final String TERMS_SECOND_PAGE = "global/template/jasper/terms1.jrxml";
  
  public static final String PARAM_TITLE = "title";

	public static final String CONFIRM_DRAFT_ORDER = "confirmDraftOrder";
	public static final String ACTIVE = "1100.200";
	public static final String CREATED_AWAITING_ACTIVATION = "1100.100";
	public static final String CHANGE_ORDER_STATUS = "changeOrderStatus";
	public static final String POST_CONFIRM = "POST_CONFIRM.0018.ex.ex";
	public static final String REGISTER_PROCESS_COMPLETION = "registerProcessCompletion";
	public static final String SC_EXPIRED ="MARK_EXPIRED.0018.ex.ex";
	public static final String ACTIVATE = "ACTIVATE";
	public static final String EXPIRED = "1100.500";
	public static final String EXPIRE = "EXPIRE";
	public static final String STATUS_DESC_CREATED = "Created";
	public static final String SC_EXPIRATION_DATE = "EXPIRATION_DATE";

	public static final String BUNDLE = "BUNDLE";
	public static final String CREATE = "CREATE";
	public static final String SC_DISCOUNT = "SCDiscount";
	public static final String GET_ITEM_LIST = "getItemList";
	
	
	public static final String GET_IB_FOR_LINE = "GetIBForLine";
	public static final String DELETE_TER_SCIB_MAPPING = "deleteTerSCIBMapping";
	public static final String STATUS_DESC_DRAFT_CREATED = "Draft Order Created";
	public static final String GET_CUSTOMER_LIST	= "getCustomerList";
	public static final String	CREATE_SC_IB_MAPPING_API	= "createTerSCIBMapping";
	
	public static final String	START_RENEWAL	= "START_RENEWAL.0018.ex.ex";
	public static final String	PENDING_RENEWAL	= "1100.300";
	public static final String	COPY_ORDER	= "copyOrder";
	public static final String	GET_ORDER_LINE_DETAILS	= "getOrderLineDetails";
	public static final String	OPERATION_MODIFY	= "Modify";
	
}

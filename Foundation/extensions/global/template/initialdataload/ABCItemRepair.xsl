<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0">
	<xsl:template match="/ABCItemRepairList">
	<xsl:element name="ABCItemRepair">
		<xsl:for-each select="ABCItemRepair">
			<xsl:attribute name="Modifyts" >
				<xsl:value-of select="format-date($@Modifyts, "[Y0001]-[M01]-[D01]")" />
			</xsl:attribute>
		</xsl:for-each>
	</xsl:element>
	</xsl:template>
</xsl:stylesheet>

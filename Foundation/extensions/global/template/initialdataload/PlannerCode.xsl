<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0">
	<xsl:template match="/PlannerCodeList">
	<xsl:element name="CommonCode">
		<xsl:for-each select="PlannerCode">
			<xsl:attribute name="OrganizationCode">
				<xsl:text>CSO</xsl:text>
      			</xsl:attribute>
			<xsl:attribute name="CodeType">
				<xsl:text>PLANNER_CODE</xsl:text>
      			</xsl:attribute>
			<xsl:attribute name="CodeValue">
		         	<xsl:value-of select="@PlannerCode"/>
					<xsl:text>|</xsl:text>
					<xsl:value-of select="@ProductFamily"/>
					<xsl:text>|</xsl:text>
					<xsl:value-of select="@Platform"/>
					<xsl:text>|</xsl:text>
					<xsl:value-of select="@BusinessUnit"/>
			</xsl:attribute>
			<xsl:attribute name="CodeShortDescription">
		         	<xsl:value-of select="@PlannerName"/>
			</xsl:attribute>
			<xsl:attribute name="CodeLongDescription">
		         	<xsl:value-of select="@EmailAddress"/>
					<xsl:text>|</xsl:text>
					<xsl:value-of select="@PlannerCodeName"/>
			</xsl:attribute>
			<xsl:attribute name="MeantForEnterprise">
		         	<xsl:text>Y</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="MeantForInternal">
		         	<xsl:text>N</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="MeantForSupplier">
		         	<xsl:text>N</xsl:text>
			</xsl:attribute>
		</xsl:for-each>
	</xsl:element>
	</xsl:template>
</xsl:stylesheet>

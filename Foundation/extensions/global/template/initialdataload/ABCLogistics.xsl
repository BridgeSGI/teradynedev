<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0">
	<xsl:template match="/ABCLogistics">
	<xsl:element name="CommonCode">
		<xsl:attribute name="OrganizationCode">
			<xsl:text>CSO</xsl:text>
      	</xsl:attribute>
		<xsl:attribute name="CodeType">
			<xsl:text>ABC_LOGISTICS</xsl:text>
      	</xsl:attribute>
		<xsl:attribute name="CodeValue">
		   	<xsl:value-of select="@CenterCode"/>
      	</xsl:attribute>
		<xsl:attribute name="CodeShortDescription">
		   	<xsl:value-of select="@CenterCostAmnt"/>
   		</xsl:attribute>
		<xsl:attribute name="CodeLongDescription">
	       	<xsl:value-of select="@TeradyneQuarter"/>
			<xsl:text>|</xsl:text>
			<xsl:value-of select="@CenterCostQty"/>
			<xsl:text>|</xsl:text>
			<xsl:value-of select="@DateLastUpdated"/>
			<xsl:text>|</xsl:text>
			<xsl:value-of select="@UserName"/>
      	</xsl:attribute>
	</xsl:element>
	</xsl:template>
</xsl:stylesheet>

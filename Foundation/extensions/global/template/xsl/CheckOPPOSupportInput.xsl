<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPCheckOPPOSupport/SPWPCheckOPPOSupport/input">
<xsl:output indent="yes"/>
<xsl:template match="/in:Input">
	<xsl:element name="MultiApi">
	<xsl:for-each select="in:CheckOPPOSupport">
	<xsl:if test="@SystemSerialNoAndServiceType !=''">
		<xsl:element name="API">
			<xsl:attribute name="Name">getOrderLineList</xsl:attribute>
		<xsl:element name="Input">
			<xsl:element name="OrderLine">
				<xsl:attribute name="DocumentType">0018.ex</xsl:attribute>
				<xsl:element name="Order">
					<xsl:attribute name="OrderType">OPPO</xsl:attribute>
				</xsl:element>
				<xsl:element name="Extn">
					<xsl:attribute name="SystemSerialNo"><xsl:value-of select="substring-before(@SystemSerialNoAndServiceType,':')"/></xsl:attribute>
					<xsl:attribute name="ServiceType"><xsl:value-of select="substring-after(@SystemSerialNoAndServiceType,':')"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:element name="Template">
			<xsl:element name="OrderLineList">
				<xsl:attribute name="TotalLineList"></xsl:attribute>
				<xsl:element name="OrderLine">
					<xsl:attribute name="OrderHeaderKey"></xsl:attribute>
					<xsl:element name="Extn">
						<xsl:attribute name="SystemSerialNo"></xsl:attribute>
						<xsl:attribute name="ServiceType"></xsl:attribute>
					</xsl:element>
					<xsl:element name="Order">
						<xsl:attribute name="OrderType"></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		</xsl:element>
	</xsl:if>
	</xsl:for-each>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>

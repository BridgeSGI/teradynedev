<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:for-each select="/OrganizationList/Organization">
				<xsl:element name="Site">
					<xsl:for-each select="Extn/TerCustBillingOrgList/TerCustBillingOrg">
					<xsl:element name="BillTo">
							<xsl:attribute name="Default_Flag"><xsl:value-of select="./@TerDefaultFLag"/>
							</xsl:attribute>
							<xsl:attribute name="Customer_Name">
								<xsl:value-of select="./@TerCompany"/>
							</xsl:attribute>	
							<xsl:attribute name="Bill_To_Cust_Add_No">
								<xsl:value-of select="substring-after(./@TerOrganizationCodeKey,'-')"/>
							</xsl:attribute>
							<xsl:attribute name="Bill_To_Cust">
								<xsl:value-of select="substring-before(./@TerOrganizationCodeKey,'-')"/>
							</xsl:attribute>
							<xsl:attribute name="Cust_Add_No">
								<xsl:value-of select="substring-after(./@TerOrganizationCodeKey,'-')"/>
							</xsl:attribute>
							<xsl:attribute name="Cust_No">
								<xsl:value-of select="substring-before(./@TerOrganizationCodeKey,'-')"/>
							</xsl:attribute>
						</xsl:element>
					</xsl:for-each>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>


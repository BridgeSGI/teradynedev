<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 
   <xsl:key name="keyEmpByName" match="//SystemTypeTesterGroup" use="@Tester_Group"/>
 
   <xsl:template match="/">
    <xsl:element name="Output">
                  <xsl:for-each select="//SystemTypeTesterGroup[generate-id() =
                        generate-id(key('keyEmpByName',@Tester_Group)[1])]">
                        <xsl:if test="@Tester_Group != ''" >
                        <xsl:element name="SystemTypeTesterGroup">
                        	<xsl:attribute name="Tester_Group">
                			<xsl:value-of select="@Tester_Group"/> 
                			</xsl:attribute>
                			<xsl:attribute name="System_Type">
                			 <xsl:value-of select ="@System_Type"/>
                			 </xsl:attribute>
                			</xsl:element>
						</xsl:if>
            </xsl:for-each>
         </xsl:element>
   </xsl:template>
</xsl:stylesheet>
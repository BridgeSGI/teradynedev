<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetMultiQuantityFlag/SPWPGetMultiQuantityFlag/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="Item">
			<xsl:element name="Extn">
				<xsl:attribute name="PrimaryRepairCenter">
					<xsl:value-of select="@Service_Center_Code"/>
				</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:for-each select="OrderLineList/OrderLine">
				<xsl:element name="GetFDLSEmail">
				
					
					<xsl:attribute name="SYSTEM_TYPE_CODE">
						<xsl:value-of select="ItemDetails/@ItemID" />
					</xsl:attribute>
					<xsl:attribute name="SYSTEM_SERIAL_NO">
						<xsl:value-of select="Extn/@SystemSerialNo" />
					</xsl:attribute>
					<xsl:attribute name="FAILURE_DIST_LIST">
						<xsl:value-of select="ItemDetails/Extn/@SystemFailureDistList"/>
					</xsl:attribute>
				
				</xsl:element>
			</xsl:for-each>  
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

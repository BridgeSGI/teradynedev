<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
      <xsl:element name="Output">
         <xsl:attribute name="COST_CENTER">
            <xsl:value-of select="PersonInfoList/PersonInfo/@AddressLine6" />
         </xsl:attribute>         
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPActiveSystemSerialNoServiceType/SPWPActiveSystemSerialNoServiceType/input">
	
	<xsl:template match="/in:Input">
		<xsl:element name="MultiApi">
			<xsl:for-each select="in:ActiveSystemSerialNoServiceType">
				<xsl:element name="API">
					<xsl:attribute name="Name">getOrderLineList</xsl:attribute>
						<xsl:element name="Input">
							<xsl:element name="OrderLine">
									<xsl:attribute name="DocumentType">
										<xsl:text>0018.ex</xsl:text>
									</xsl:attribute>
									<xsl:element name="Extn">
									
									<xsl:attribute name="SystemSerialNo">
										<xsl:value-of select="@SystemSerialNo"/>
									</xsl:attribute>	
									</xsl:element>

							</xsl:element>		
						</xsl:element>		
					
						<xsl:element name="Template">
								<xsl:element name="OrderLineList">
										<xsl:element name="OrderLine">
											<xsl:attribute name="OrderLineKey">
															</xsl:attribute>
												<xsl:element name="Extn">
															
															<xsl:attribute name="RepairablePartsCovered">
															</xsl:attribute>
															<xsl:attribute name="NonrepairPartsCovered">
															</xsl:attribute>
															<xsl:attribute name="SystemType">
															</xsl:attribute>

												</xsl:element>
												
												<xsl:element name="ItemDetails">
													<xsl:attribute name="ItemID"></xsl:attribute>
													<xsl:element name="PrimaryInformation">
																<xsl:attribute name="ItemType"></xsl:attribute>
													</xsl:element>
													<xsl:element name="Extn">
															<xsl:attribute name="RespProdMfgDivCode">
															</xsl:attribute>
															<xsl:attribute name="SystemTesterGroup">
															</xsl:attribute>
															<xsl:attribute name="SystemPTExceptionFlag">
															</xsl:attribute>
													</xsl:element>
												</xsl:element>
												
												<xsl:element name="Order">
													<xsl:attribute name="OrderNo">
													</xsl:attribute>
													<xsl:attribute name="OrderType">
													</xsl:attribute>	
													<xsl:element name="Extn">
														<xsl:attribute name="ExpirationDate">
														</xsl:attribute>
													</xsl:element>		
													
												</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							</xsl:element>		
			</xsl:for-each>
		</xsl:element>	
	</xsl:template>
</xsl:stylesheet>

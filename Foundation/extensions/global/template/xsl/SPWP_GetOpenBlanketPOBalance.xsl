<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Order">
<xsl:attribute name="EnterpriseCode"><xsl:value-of select="/Input/@ShipToCustomerNumber"/></xsl:attribute>
<xsl:attribute name="OrderNo"><xsl:value-of select="/Input/@BPONo"/></xsl:attribute>
<xsl:attribute name="ReceivingNode"><xsl:value-of select="/Input/@ShipToCustomerSiteNumber"/></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
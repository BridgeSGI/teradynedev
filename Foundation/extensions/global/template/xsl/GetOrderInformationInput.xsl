<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetOrderInformation/SPWPGetOrderInformation/input">
   <xsl:template match="/in:Input">
   <xsl:element name="Order">
  	<xsl:attribute name="EnterpriseCode">CSO</xsl:attribute>
         <xsl:attribute name="DocumentType">0001</xsl:attribute>	  
      <xsl:attribute name="OrderNo">
		    <xsl:value-of select="in:GetOrderInformation/@OrderNumber"/>
	</xsl:attribute>		
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>

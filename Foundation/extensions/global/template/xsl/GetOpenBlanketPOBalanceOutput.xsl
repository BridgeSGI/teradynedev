<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Output">
		<xsl:for-each select="OrderLineList/OrderLine">
			<xsl:element name="GetOpenBlanketPOBalance">
				
					<xsl:attribute name="ShipToCustomerNo">
						<xsl:value-of select="substring-before(Order/@ReceivingNode,'-')"/>
					</xsl:attribute>
					<xsl:attribute name="ShipToCustomerSiteNo">
						<xsl:value-of select="substring-after(Order/@ReceivingNode,'-')"/>
						
						
					</xsl:attribute>
					
					<xsl:attribute name="BPONo">
						<xsl:value-of select="Order/@OrderNo" />
					</xsl:attribute>
					
					<xsl:attribute name="CurrencyCode">
						<xsl:value-of select="Order/PriceInfo/@Currency" />
					</xsl:attribute>
					<xsl:attribute name="POBalance">
						<xsl:value-of select="Extn/@OpenBalance"/>
					</xsl:attribute>
		
				
			</xsl:element>
			</xsl:for-each>  
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
		<xsl:element name="Receipt">
				<xsl:attribute name="DocumentType">
					<xsl:value-of select="/Shipment/@DocumentType" />
				</xsl:attribute>
				<xsl:attribute name="OrganizationCode">
					<xsl:value-of select="/Shipment/@EnterpriseCode" />
				</xsl:attribute>
				<xsl:attribute name="ReceivingNode">
					<xsl:value-of select="/Shipment/@ReceivingNode" />
				</xsl:attribute>
				<xsl:attribute name="ShipmentKey">
					<xsl:value-of select="/Shipment/@ShipmentKey" />
				</xsl:attribute>
				<xsl:attribute name="IgnoreOrdering">
					<xsl:text>Y</xsl:text>
				</xsl:attribute>
				<xsl:attribute name="ReceivingDock">
					<xsl:text>D1-010101</xsl:text>
				</xsl:attribute>
				
				<xsl:element name="ReceiptLines">
					<xsl:element name="ReceiptLine">
						<xsl:attribute name="ItemID">
							<xsl:value-of select="/Shipment/ShipmentLines/ShipmentLine/@ItemID" />
						</xsl:attribute>
						<xsl:attribute name="ShipmentLineKey">
							<xsl:value-of select="/Shipment/ShipmentLines/ShipmentLine/@ShipmentLineKey" />
						</xsl:attribute>
						<xsl:attribute name="ProductClass">
							<xsl:value-of select="/Shipment/ShipmentLines/ShipmentLine/@ProductClass" />
						</xsl:attribute>
						<xsl:attribute name="Quantity">
							<xsl:value-of select="/Shipment/ShipmentLines/ShipmentLine/Extn/@RecvdQty" />
						</xsl:attribute>
						<xsl:attribute name="UnitOfMeasure">
							<xsl:value-of select="/Shipment/ShipmentLines/ShipmentLine/@UnitOfMeasure" />
						</xsl:attribute>
						
				</xsl:element>
				</xsl:element>
				
	</xsl:element>

	
</xsl:template>
</xsl:stylesheet>
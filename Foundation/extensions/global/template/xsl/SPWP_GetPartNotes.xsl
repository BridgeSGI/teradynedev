<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>

<xsl:template match="/GetPartNotes">
<xsl:element name="Item">
	<xsl:attribute name="ItemID"><xsl:value-of select="/GetPartNotes/@PartNumber" /></xsl:attribute>
	<xsl:attribute name="OrganizationCode"><xsl:text>DEFAULT</xsl:text></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetOpenBlanketPOBalance/SPWPGetOpenBlanketPOBalance/input">

	<xsl:template match="/in:Input">
		<xsl:element name="OrderLine">
			
			<xsl:element name="Order">
				<xsl:attribute name="DocumentType">0018.ex</xsl:attribute>
	
				<xsl:attribute name="OrderNo">
					<xsl:value-of select="in:GetOpenBlanketPOBalance/@BPONo" />
				</xsl:attribute>
				<xsl:attribute name ="ReceivingNodeQryType">FLIKE</xsl:attribute>
				<xsl:if test="in:GetOpenBlanketPOBalance/@ShipToCustomerSiteNumber = ''">
				<xsl:attribute name="ReceivingNode">
					<xsl:value-of select="in:GetOpenBlanketPOBalance/@ShipToCustomerNumber" />
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="in:GetOpenBlanketPOBalance/@ShipToCustomerSiteNumber != ''">
				<xsl:attribute name="ReceivingNode">
					<xsl:value-of select="concat(in:GetOpenBlanketPOBalance/@ShipToCustomerNumber,'-',in:GetOpenBlanketPOBalance/@ShipToCustomerSiteNumber)"/>
				</xsl:attribute>
			</xsl:if>
			
				<!-- <xsl:attribute name="OrderType">BO</xsl:attribute> -->
				
			</xsl:element>		

		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

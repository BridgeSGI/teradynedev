<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Output">
			
				<xsl:for-each select="OrderList/Order/OrderLines/OrderLine">
					<xsl:if test=" ItemDetails/PrimaryInformation/@ItemType = 'SYSTEM' and Extn/@MktStatus='A'">
					<xsl:element name="GetAllSystemSerialNumbers">
					<xsl:attribute name="SYSTEM_TYPE">
						<xsl:value-of select="ItemDetails/@ItemID" />
					</xsl:attribute>				
					<xsl:attribute name="SYSTEM_SERIAL_NUMBER">
						<xsl:value-of select="Extn/@SystemSerialNo"/>
					</xsl:attribute> 
				</xsl:element>
						</xsl:if>
					</xsl:for-each>  
				  
			</xsl:element>
		
	</xsl:template>
</xsl:stylesheet>

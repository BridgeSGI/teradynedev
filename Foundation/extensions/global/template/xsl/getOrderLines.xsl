<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml"/>	
	<xsl:template match="/">
		
		<MultiApi>
			<API FlowName="DoNothingService">
				<Input>
					<xsl:copy-of select="*"/>
				</Input>
			</API>
			
			<API Name="getOrderLineList" Version="">
				<Input>
					<xsl:element name="OrderLine">
		
						<xsl:attribute name="DocumentType">
							<xsl:value-of select="Shipment/@DocumentType"/>
						</xsl:attribute>
										
						<xsl:element name="Order">
							<xsl:attribute name="OrderNo">
								<xsl:value-of select="Shipment/ShipmentLines/ShipmentLine/@OrderNo"/>
							</xsl:attribute>
						</xsl:element>
			
						<xsl:element name="Item">
							<xsl:attribute name="ItemID">
								<xsl:value-of select="Shipment/ShipmentLines/ShipmentLine/@ItemID"/>
							</xsl:attribute>
						</xsl:element>
		
					</xsl:element>
				</Input>
				<xsl:element name="Template">
					<xsl:element name="OrderLineList">  
						<xsl:element name="OrderLine">  
							<xsl:attribute name="OrderedQty"/> 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</API>
		</MultiApi>
	</xsl:template>
</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	<xsl:element name="Output">
	<xsl:element name="GetDOAOrderHistory">
	<xsl:for-each select="MultiApi/API/Output/Order">
		
				<xsl:if test="@DocumentType='0003'">
				
						<xsl:attribute name="DOAOrderNumber">
							<xsl:value-of select="@OrderNo" />
						</xsl:attribute>
						<xsl:attribute name="DefPartSerialNo">
							<xsl:value-of select="OrderLines/OrderLine/@SerialNo" />
						</xsl:attribute>
						<xsl:attribute name="ShipmentDate">
							<xsl:value-of select="OrderLines/OrderLine/@ReqShipDate" />
						</xsl:attribute>
						<xsl:attribute name="NoChargeCode">
							<xsl:value-of select="OrderLines/OrderLine/Extn/@RepairNoChargeCode" />
						</xsl:attribute>
				
					</xsl:if>
				<xsl:if test="@DocumentType='0001'">
				
						<xsl:attribute name="OrderNumber">
							<xsl:value-of select="@OrderNo" />
						</xsl:attribute>
						<xsl:attribute name="IssuedPartSerialNo">
							<xsl:value-of select="OrderLines/OrderLine/@SerialNo" />
						</xsl:attribute>				
				
				</xsl:if>			
		
	</xsl:for-each>	
</xsl:element>	
</xsl:element>
</xsl:template>
</xsl:stylesheet>



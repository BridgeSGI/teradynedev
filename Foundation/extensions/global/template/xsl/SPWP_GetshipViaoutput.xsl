<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/> 
<xsl:template match="/">
<xsl:element name="Output">
<xsl:for-each select="ScacAndServiceList/ScacAndService"> 
<CarrierLines> 
   <xsl:variable name="Pos" select="number(position())"/>
    <xsl:variable name="ScacAndServiceDesc" select="/ScacAndServiceList/ScacAndService[$Pos]/@ScacAndServiceDesc"/>
    <xsl:variable name="CarrierType" select="/ScacAndServiceList/ScacAndService[$Pos]/@CarrierType"/>
    <xsl:variable name="ScacKey" select="/ScacAndServiceList/ScacAndService[$Pos]/@ScacKey"/>
     <xsl:attribute name="CarrierType">
     <xsl:value-of select="$CarrierType"/>
    </xsl:attribute> 
    <xsl:attribute name="ScacKey">
    <xsl:value-of select="$ScacKey"/>                                   
    </xsl:attribute> 
	<xsl:attribute name="ScacAndServiceDesc">
    <xsl:value-of select="$ScacAndServiceDesc"/>
    </xsl:attribute>
</CarrierLines>
</xsl:for-each>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
   <xsl:element name="OrderLine">
      <xsl:element name="Order">
         <xsl:attribute name="BuyerOrganizationCode">
            <xsl:value-of select="Input/@SYSTEM_CUSTOMER_NO" />
         </xsl:attribute>
      </xsl:element>
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>

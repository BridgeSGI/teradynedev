<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:template match="/">
   <xsl:element name="PersonInfo">
         <xsl:attribute name="AddressLine6">
            <xsl:value-of select="Input/@COST_CENTER" />
         </xsl:attribute>
      </xsl:element>
 
   </xsl:template>
</xsl:stylesheet>
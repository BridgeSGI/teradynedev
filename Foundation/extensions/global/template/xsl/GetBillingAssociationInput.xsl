<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetBillingAssociation/SPWPGetBillingAssociation/input" >
<xsl:output indent="yes"/>
<xsl:template match="/in:Input">
	<xsl:element name="MultiApi">
	<xsl:for-each select="in:GetBillingAssociation">
		<xsl:element name="API">
			<xsl:attribute name="Name">getCustomerDetails</xsl:attribute>
		<xsl:element name="Input">
			<xsl:element name="Customer">
				<xsl:attribute name="CustomerID">
				<xsl:value-of select="concat(@CustomerNumber,'-',@ShiptoSiteId)"/>
				</xsl:attribute>
				<xsl:attribute name="OrganizationCode">CSO</xsl:attribute>
			</xsl:element>
		</xsl:element>
		<xsl:element name="Template">
			<xsl:element name="Customer">
				<xsl:attribute name="BuyerOrganizationCode"></xsl:attribute>
				<xsl:attribute name="CustomerID"></xsl:attribute>
				<xsl:attribute name="Status"></xsl:attribute>
				<xsl:element name="BuyerOrganization">
					<xsl:attribute name="OrganizationName"></xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		</xsl:element>
	</xsl:for-each>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>

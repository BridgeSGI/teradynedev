<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
      <xsl:element name="Output">
         <xsl:attribute name="SERVICE_TYPE_CODE">
            <xsl:value-of select="OrderLineList/OrderLine/Extn/@ServiceTypeCode" />
         </xsl:attribute>
         <xsl:attribute name="ENTITLEMENT_NUMBER">
            <xsl:value-of select="OrderLineList/OrderLine/Order/@OrderNo" />
         </xsl:attribute>
         <xsl:attribute name="ENTITLEMENT_TYPE_CODE">
            <xsl:value-of select="OrderLineList/OrderLine/Order/@OrderType" />
         </xsl:attribute>
         <xsl:attribute name="RESP_SYS_MFG_DIVISION_CODE">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@RespProdMfgDivCode" />
         </xsl:attribute>
         <xsl:attribute name="TESTER_GROUP">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@SystemTesterGroup" />
         </xsl:attribute>
         <xsl:attribute name="PT_EXCEPTION_FLAG">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@SystemPTExceptionFlag" />
         </xsl:attribute>
         <xsl:attribute name="SYSTEM_TYPE_CODE">
            <xsl:value-of select="OrderLineList/OrderLine/Order/Extn/@SystemType" />
         </xsl:attribute>
         <xsl:attribute name="PART_TYPE_ENTITLEMENT_FLAG">
            <xsl:value-of select="OrderLineList/OrderLine/Extn/@PartTypeEntitlementFlag" />
         </xsl:attribute>
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>


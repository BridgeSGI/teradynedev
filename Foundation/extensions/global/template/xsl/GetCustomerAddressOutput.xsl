<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetCustomerAddress/SPWPGetCustomerAddress/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
		<xsl:element name="GetCustomerAddress">
			<xsl:attribute name="Address-Line1"><xsl:value-of select="OrganizationList/Organization/BillingPersonInfo/@AddressLine1" /></xsl:attribute>
			<xsl:attribute name="Address-Line2"><xsl:value-of select="OrganizationList/Organization/BillingPersonInfo/@AddressLine2" /></xsl:attribute>
			<xsl:attribute name="Address-Line3"><xsl:value-of select="OrganizationList/Organization/BillingPersonInfo/@AddressLine3" /></xsl:attribute>
			<xsl:attribute name="Address-Line4"><xsl:value-of select="OrganizationList/Organization/BillingPersonInfo/@AddressLine4" /></xsl:attribute>
			<xsl:attribute name="Address-Line5"><xsl:value-of select="OrganizationList/Organization/BillingPersonInfo/@AddressLine5" /></xsl:attribute>
			<xsl:attribute name="Address-City"><xsl:value-of select="OrganizationList/Organization/BillingPersonInfo/@City" /></xsl:attribute>
			<xsl:attribute name="Address-State"><xsl:value-of select="OrganizationList/Organization/BillingPersonInfo/@State" /></xsl:attribute>
			<xsl:attribute name="Address-ZipCode"><xsl:value-of select="OrganizationList/Organization/BillingPersonInfo/@ZipCode" /></xsl:attribute>
			<xsl:attribute name="Address-Country"><xsl:value-of select="OrganizationList/Organization/BillingPersonInfo/@Country" /></xsl:attribute>
			<xsl:attribute name="SiteID"><xsl:value-of select="substring-after(OrganizationList/Organization/@OrganizationCode,'-')" /></xsl:attribute>
		</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

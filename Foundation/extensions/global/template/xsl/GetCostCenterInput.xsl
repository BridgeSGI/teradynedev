<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetCostCenter/SPWPGetCostCenter/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="MultiApi">
			<xsl:for-each select="in:GetCostCenter">
				<xsl:element name="API">
					<xsl:attribute name="Name">getUserHierarchy</xsl:attribute>
					<xsl:element name="Input">
						<xsl:element name="User">
							<xsl:attribute name="Loginid"><xsl:value-of select="@EMPLOYEE_ID"/></xsl:attribute>
						</xsl:element>
					</xsl:element>
					<xsl:element name="Template">
						<xsl:element name="User">
							<xsl:attribute name="Loginid"/>
							<xsl:element name="ContactPersonInfo">
								<xsl:attribute name="AddressLine6"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 
   <xsl:key name="keyEmpByName" match="//IsRPDEligible" use="@ServiceTypeEligible"/>
 
   <xsl:template match="/">
    <xsl:element name="Output">
                  <xsl:for-each select="//IsRPDEligible[generate-id() =
                        generate-id(key('keyEmpByName',@ServiceTypeEligible)[1])]">
                        
                        <xsl:element name="IsRPDEligible">
                        	<xsl:attribute name="ServiceTypeEligible">
                			<xsl:value-of select="@ServiceTypeEligible"/> 
                			</xsl:attribute>
                		
                		</xsl:element>
						
            </xsl:for-each>
         </xsl:element>
   </xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
       <xsl:output method="xml"/>	
       <xsl:template match="/">	   
      <xsl:for-each select="CommonCode/CommonCode">
        <xsl:element name="CommonCode">
          <xsl:attribute name="Action">
            <xsl:text>Manage</xsl:text>
          </xsl:attribute>
   		<xsl:attribute name="CodeLongDescription">
   				<xsl:value-of select="@TARIFF_DESCRIPTION"/> 					
		</xsl:attribute>
		  <xsl:attribute name="CodeShortDescription">
   				<xsl:value-of select="@TARIFF_DESCRIPTION"/> 				
          </xsl:attribute>		  	  
          <xsl:attribute name="CodeValue">
				<xsl:value-of select="@TARIFF_CODE"/> 					
          </xsl:attribute>
          <xsl:attribute name="CodeType">
            <xsl:text>Tariff_US</xsl:text>
           </xsl:attribute>  
           <xsl:attribute name="MeantForEnterprise">
          <xsl:text>Y</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="MeantForInternal">
          <xsl:text>N</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="MeantForSupplier">
          <xsl:text>N</xsl:text>
            </xsl:attribute>
          <xsl:attribute name="OrganizationCode">
          <xsl:text>CSO</xsl:text>
            </xsl:attribute>
        </xsl:element>
      </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
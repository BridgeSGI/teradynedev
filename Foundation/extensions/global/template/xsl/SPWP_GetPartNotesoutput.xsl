<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/Item">
<xsl:element name="Output">
<xsl:attribute name="PartNumber"><xsl:value-of select="/Item/@ItemID" /></xsl:attribute>
<xsl:attribute name="ServiceCenterCode"><xsl:value-of select="/Item/@OrganizationCode" /></xsl:attribute>
<xsl:attribute name="PartDescription"><xsl:value-of select="/Item/PrimaryInformation/@DisplayItemDescription" /></xsl:attribute> 
</xsl:element>
</xsl:template>
</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
      <xsl:element name="Output">
         <xsl:attribute name="SYSTEM_SERIAL_NO">
            <xsl:value-of select="OrderLineList/OrderLine/Extn/@SerialNumber" />
         </xsl:attribute>
         <xsl:attribute name="SYSTEM_TYPE_CODE">
            <xsl:value-of select="OrderLineList/OrderLine/Extn/@SystemType" />
         </xsl:attribute>
         <xsl:attribute name="TESTER_GROUP">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@SystemTesterGroup" />
         </xsl:attribute>
         <xsl:attribute name="PT_EXCEPTION_FLAG">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@SystemPTExceptionFlag" />
         </xsl:attribute>
         <xsl:attribute name="INSTALL_DATE">
            <xsl:value-of select="OrderLineList/OrderLine/Extn/@InstallDate" />
         </xsl:attribute>

      </xsl:element>
   </xsl:template>
</xsl:stylesheet>
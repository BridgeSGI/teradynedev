<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/Input">
<xsl:element name="Customer">
<xsl:attribute name="CustomerID"><xsl:value-of select="/Input/@CenterID" /></xsl:attribute>
<xsl:attribute name="OrganizationCode"><xsl:value-of select="/Input/@ServiceCenterCode" /></xsl:attribute>	 
</xsl:element>
</xsl:template>
</xsl:stylesheet>
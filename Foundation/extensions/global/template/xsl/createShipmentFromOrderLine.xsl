<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	
		

	<xsl:element name="Shipment">
		
				<xsl:attribute name="ShipNode">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@ShipNode" />
				</xsl:attribute>
				<xsl:attribute name="SellerOrganizationcode">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@SellerOrganizationcode" />
				</xsl:attribute>
				<xsl:attribute name="ReceivingNode">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@ReceivingNode" />
				</xsl:attribute>
				<xsl:attribute name="EnterpriseCode">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@EnterpriseCode" />
				</xsl:attribute>
				<xsl:attribute name="DocumentType">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@DocumentType" />
				</xsl:attribute>
				<xsl:attribute name="DoNotVerifyPalletContent">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@DoNotVerifyPalletContent" />
				</xsl:attribute>
				<xsl:attribute name="DoNotVerifyCaseContent">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@DoNotVerifyCaseContent" />
				</xsl:attribute>
				<xsl:attribute name="ConfirmShip">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@ConfirmShip" />
				</xsl:attribute>
				<xsl:attribute name="BuyerOrganizationCode">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@BuyerOrganizationCode" />
				</xsl:attribute>
				<xsl:attribute name="ActualShipmentDate">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@ActualShipmentDate" />
				</xsl:attribute>
				<xsl:attribute name="ActualDeliveryDate">
					<xsl:value-of select="MultiApi/API/Output/Shipment/@ActualDeliveryDate" />
				</xsl:attribute>
				
				<xsl:element name="ShipmentLines">
					<xsl:element name="ShipmentLine">
							<xsl:attribute name="PrimeLineNo">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@PrimeLineNo" />
							</xsl:attribute>
							<xsl:attribute name="RequestedSerialNo">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@RequestedSerialNo" />
							</xsl:attribute>
							<xsl:attribute name="UnitOfMeasure">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@UnitOfMeasure" />
							</xsl:attribute>
							<xsl:attribute name="ShipmentSubLineNo">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@ShipmentSubLineNo" />
							</xsl:attribute>
		
							<xsl:attribute name="Quantity">
								<xsl:value-of select="MultiApi/API/Output/OrderLineList/OrderLine/@OrderedQty" />
							</xsl:attribute>
	
							<xsl:attribute name="ShipmentLineNo">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@ShipmentLineNo" />
							</xsl:attribute>
							<xsl:attribute name="ProductClass">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@ProductClass" />
							</xsl:attribute>
							<xsl:attribute name="OrderNo">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@OrderNo" />
							</xsl:attribute>
							<xsl:attribute name="ItemID">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@ItemID" />
							</xsl:attribute>
							<xsl:attribute name="DocumentType">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@DocumentType" />
							</xsl:attribute>
							<xsl:attribute name="CustomerPoNo">
								<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/@CustomerPoNo" />
							</xsl:attribute>
							
							<xsl:element name="Extn">
								<xsl:attribute name="RecvdQty">
									<xsl:value-of select="MultiApi/API/Output/Shipment/ShipmentLines/ShipmentLine/Extn/@RecvdQty" />
								</xsl:attribute>
							
								
							</xsl:element>	
							
					</xsl:element>
				</xsl:element>
			
				</xsl:element>

	
</xsl:template>
</xsl:stylesheet>
				
				
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPAllActiveSystemTypes/SPWPAllActiveSystemTypes/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
	 <xsl:element name="OrderLine">  
					
			<xsl:element name="Extn"> 
				<xsl:attribute name="MktStatus">
				<xsl:text>A</xsl:text>
			</xsl:attribute>
			</xsl:element>  
			<xsl:element name="Order">  
				<xsl:attribute name="DocumentType">
					<xsl:text>0017.ex</xsl:text>
				</xsl:attribute>
				<xsl:attribute name ="ShipToIDQryType">FLIKE</xsl:attribute>
				<xsl:attribute name="ShipToID">
					<xsl:value-of select="in:AllActiveSystemTypes/@CustomerNumber"/>
				</xsl:attribute>
			</xsl:element>  
      </xsl:element>
	</xsl:template>
</xsl:stylesheet>

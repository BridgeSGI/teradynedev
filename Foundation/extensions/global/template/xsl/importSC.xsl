<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output method="xml" encoding="utf-8"/>
<xsl:template match="Order/OrderLines/OrderLine">

<xsl:copy>

   
      <xsl:attribute name="PipelineId">Service Contract</xsl:attribute>
	  <xsl:attribute name="PipelineOwnerKey">DEFAULT</xsl:attribute>
	  <xsl:apply-templates select="@*|node()"/>
     <xsl:element name="OrderStatuses">
				
				<xsl:element name="OrderStatus">
				<xsl:attribute name="Status">
      
        <xsl:value-of select="Extn/@SCLineStatus"/>
      </xsl:attribute>
      <xsl:attribute name="StatusQty">
      
        <xsl:value-of select="@OrderedQty"/>
      </xsl:attribute>
<xsl:element name="Schedule">
				</xsl:element>
				</xsl:element>
				</xsl:element>
     
    </xsl:copy>
  </xsl:template>

<xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
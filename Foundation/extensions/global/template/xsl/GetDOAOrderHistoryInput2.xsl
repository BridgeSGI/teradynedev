<?xml version = "1.0" encoding = "UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  version="1.0">
	<xsl:output indent="yes" />
	<xsl:template match="/">
		<MultiApi>
			<API FlowName="DoNothingService">
				<Input>
					<xsl:copy-of select="*"/>
				</Input>
			</API>
			<API Name="getOrderDetails" Version="">
					<Input>
						<xsl:element name="Order">
							<xsl:attribute name="EnterpriseCode">CSO</xsl:attribute>
		       					<xsl:attribute name="DocumentType">0001</xsl:attribute>	  
								
							<xsl:attribute name="OrderHeaderKey">
	    							<xsl:value-of select="Order/OrderLines/OrderLine/@DerivedFromOrderHeaderKey"/>
							</xsl:attribute>		
						</xsl:element>	
					</Input>	
					<xsl:element name="Template">
						<xsl:element name="Order">  
							<xsl:attribute name="OrderNo"/> 
							<xsl:attribute name="DocumentType"/>  
							<xsl:attribute name="OrderType"/>  	
							<xsl:element name="OrderLines">
								<xsl:element name="OrderLine">
                                       <xsl:attribute name="SerialNo"/>
								 </xsl:element>
							</xsl:element>
						</xsl:element>								
					</xsl:element>
			</API>
			</MultiApi>
</xsl:template>
</xsl:stylesheet>

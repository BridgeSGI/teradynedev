<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	<xsl:element name="Output">
		<xsl:for-each select="Order/OrderLines/OrderLine">
		<xsl:if test="@Status='Backordered'">			
					<xsl:element name="GetOrderInformation">
					<xsl:attribute name="SystemSerialNumber">
						<xsl:value-of select="Extn/@SystemSerialNo" />
					</xsl:attribute>
					<xsl:attribute name="LineNumber">
						<xsl:value-of select="@PrimeLineNo" />
					</xsl:attribute>
					<xsl:attribute name="SystemType">
						<xsl:value-of select="Extn/@SystemType" />
					</xsl:attribute>
			
					<xsl:attribute name="PartNumber">
						<xsl:value-of select="ItemDetails/@ItemID" />
					</xsl:attribute>			
					<xsl:attribute name="Quantity">
						<xsl:value-of select="@OrderedQty" />
					</xsl:attribute>
					<xsl:attribute name="UnitPriceExpediting">
						<xsl:value-of select="Extn/@STOExpediteListDiscount" />
					</xsl:attribute>
					<xsl:attribute name="UnitPriceRepair">
						<xsl:value-of select="Extn/@PartRepairPrice" />
					</xsl:attribute>
					<xsl:attribute name="UnitPriceHandling">
						<xsl:value-of select="Extn/@PartHandlingCharge" />
					</xsl:attribute>
					<xsl:attribute name="TotalAmount">
						<xsl:value-of select="Order/@OriginalTotalAmount" />
					</xsl:attribute>
					<xsl:attribute name="CurrencyCode">
						<xsl:value-of select="Order/PriceInfo/@Currency" />
					</xsl:attribute>
					<xsl:attribute name="CurrencyDescription">
						<xsl:value-of select="Order/PriceInfo/@Currency" />
					</xsl:attribute>
			
			<xsl:attribute name="RepairCode">
				<xsl:value-of select="ItemDetails/PrimaryInformation/Extn/@RepairCode" />
			</xsl:attribute>

			<xsl:attribute name="RepairCodeDescrition">
				<xsl:value-of select="ItemDetails/PrimaryInformation/@ShortDescription" />
			</xsl:attribute>
			
			<xsl:attribute name="ResponsibleCenter">
				<xsl:value-of select="@ShipNode" />
			</xsl:attribute>
			
			<xsl:attribute name="BusinessDaysToDueDate">
				<xsl:value-of select="Extn/@BusinessDaysToDueDate" />
			</xsl:attribute>
			<xsl:attribute name="WarrentyNumber">
				<xsl:value-of select="Extn/@WarrantyNo" />
			</xsl:attribute>
			<xsl:attribute name="NumberOfWarrentyDays">
				<xsl:value-of select="Order/Extn/@DurationDays" />
			</xsl:attribute>
			<xsl:attribute name="FailureDatalogEmailAddress">
				<xsl:value-of select="ItemDetails/Extn/@SystemFailureDistList" />
			</xsl:attribute>
			<xsl:attribute name="OrderServiceCenter">
				<xsl:value-of select="ItemDetails/Extn/@DefaultInventoryCenter" />
			</xsl:attribute>
			<xsl:attribute name="PartSerialNumber">
				<xsl:value-of select="Extn/@ProdSerialNumber" />
			</xsl:attribute>
			<xsl:attribute name="PartNotes">
				<xsl:value-of select="ItemDetails/ItemInstructionList/ItemInstruction/@InstructionText" />
			</xsl:attribute>
			<xsl:attribute name="CustomerDisplayablePartNotesFlag">
				<xsl:value-of select="ItemDetails/ItemInstructionList/ItemInstruction/@InstructionType" />
			</xsl:attribute>
			<xsl:attribute name="BackOrderFlag">Y</xsl:attribute>
			<xsl:attribute name="PurchaseOrderNumber">
				<xsl:value-of select="@CustomerPONo" />
			</xsl:attribute>	
			<xsl:attribute name="ServiceTypeCode">
				<xsl:value-of select="Extn/@ServiceTypeCode" />
			</xsl:attribute>
			<xsl:attribute name="ServiceTypeDescrition">
				<xsl:value-of select="ItemDetails/PrimaryInformation/@ShortDescription" />
			</xsl:attribute>						
				</xsl:element>
						</xsl:if>
		<xsl:if test="@Status!='Backordered'">			
					<xsl:element name="GetOrderInformation">
					<xsl:attribute name="SystemSerialNumber">
						<xsl:value-of select="Extn/@SystemSerialNo" />
					</xsl:attribute>
					<xsl:attribute name="LineNumber">
						<xsl:value-of select="@PrimeLineNo" />
					</xsl:attribute>
					<xsl:attribute name="SystemType">
						<xsl:value-of select="Extn/@SystemType" />
					</xsl:attribute>
			
					<xsl:attribute name="PartNumber">
						<xsl:value-of select="ItemDetails/@ItemID" />
					</xsl:attribute>			
					<xsl:attribute name="Quantity">
						<xsl:value-of select="@OrderedQty" />
					</xsl:attribute>
					<xsl:attribute name="UnitPriceExpediting">
						<xsl:value-of select="Extn/@STOExpediteListDiscount" />
					</xsl:attribute>
					<xsl:attribute name="UnitPriceRepair">
						<xsl:value-of select="Extn/@PartRepairPrice" />
					</xsl:attribute>
					<xsl:attribute name="UnitPriceHandling">
						<xsl:value-of select="Extn/@PartHandlingCharge" />
					</xsl:attribute>
					<xsl:attribute name="TotalAmount">
						<xsl:value-of select="Order/@OriginalTotalAmount" />
					</xsl:attribute>
						<xsl:attribute name="CurrencyCode">
						<xsl:value-of select="Order/PriceInfo/@Currency" />
					</xsl:attribute>
					<xsl:attribute name="CurrencyDescription">
						<xsl:value-of select="Order/PriceInfo/@Currency" />
					</xsl:attribute>
			
			<xsl:attribute name="RepairCode">
				<xsl:value-of select="ItemDetails/PrimaryInformation/Extn/@RepairCode" />
			</xsl:attribute>

			<xsl:attribute name="RepairCodeDescrition">
				<xsl:value-of select="ItemDetails/PrimaryInformation/@ShortDescription" />
			</xsl:attribute>
			
			<xsl:attribute name="ResponsibleCenter">
				<xsl:value-of select="@ShipNode" />
			</xsl:attribute>
			
			<xsl:attribute name="BusinessDaysToDueDate">
				<xsl:value-of select="Extn/@BusinessDaysToDueDate" />
			</xsl:attribute>
			<xsl:attribute name="WarrentyNumber">
				<xsl:value-of select="Extn/@WarrantyNo" />
			</xsl:attribute>
			<xsl:attribute name="NumberOfWarrentyDays">
				<xsl:value-of select="Order/Extn/@DurationDays" />
			</xsl:attribute>
			<xsl:attribute name="FailureDatalogEmailAddress">
				<xsl:value-of select="ItemDetails/Extn/@SystemFailureDistList" />
			</xsl:attribute>
			<xsl:attribute name="OrderServiceCenter">
				<xsl:value-of select="ItemDetails/Extn/@DefaultInventoryCenter" />
			</xsl:attribute>
			<xsl:attribute name="PartSerialNumber">
				<xsl:value-of select="Extn/@ProdSerialNumber" />
			</xsl:attribute>
			<xsl:attribute name="PartNotes">
				<xsl:value-of select="ItemDetails/ItemInstructionList/ItemInstruction/@InstructionText" />
			</xsl:attribute>
			<xsl:attribute name="CustomerDisplayablePartNotesFlag">
				<xsl:value-of select="ItemDetails/ItemInstructionList/ItemInstruction/@InstructionType" />
			</xsl:attribute>
			<xsl:attribute name="BackOrderFlag">N</xsl:attribute>
			<xsl:attribute name="PurchaseOrderNumber">
				<xsl:value-of select="@CustomerPONo" />
			</xsl:attribute>	
			<xsl:attribute name="ServiceTypeCode">
				<xsl:value-of select="Extn/@ServiceTypeCode" />
			</xsl:attribute>
			<xsl:attribute name="ServiceTypeDescrition">
				<xsl:value-of select="ItemDetails/PrimaryInformation/@ShortDescription" />
			</xsl:attribute>						
				</xsl:element>
						</xsl:if>	
								</xsl:for-each>  
						     </xsl:element>
  							 </xsl:template>
								</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetOrderCenterAddress/SPWPGetOrderCenterAddress/input" >
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="MultiApi">
			<xsl:for-each select="in:GetOrderCenterAddress">
				<xsl:element name="API">
					<xsl:attribute name="Name">getOrganizationHierarchy</xsl:attribute>
					<xsl:element name="Input">
						<xsl:element name="Organization">
							<xsl:attribute name="OrganizationCode">
								<xsl:value-of select="@Service_Center_Code"/>
							</xsl:attribute>							
						</xsl:element>	
					</xsl:element>					
					<xsl:element name="Template">
						<xsl:element name="Organization">
								<xsl:element name="CorporatePersonInfo">
								<xsl:attribute name="AddressLine1"/>
								<xsl:attribute name="AddressLine2"/>
								<xsl:attribute name="AddressLine3"/>
								<xsl:attribute name="AddressLine4"/>
								<xsl:attribute name="AddressLine5"/>
								<xsl:attribute name="City"/>
								<xsl:attribute name="State"/>
								<xsl:attribute name="Country"/> 
								<xsl:attribute name="ZipCode"/>
							</xsl:element>
						</xsl:element>								
					</xsl:element>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
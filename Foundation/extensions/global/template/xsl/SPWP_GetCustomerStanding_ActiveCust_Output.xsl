<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:element name="GetCustomerStanding">
				<!-- xsl:attribute name="CustomerID">
					<xsl:value-of select="Customer/@CustomerID" />
				</xsl:attribute>
				<xsl:attribute name="Status">
					<xsl:text>Active</xsl:text>
				</xsl:attribute -->
				<xsl:attribute name="CreditHoldFlag">
					<xsl:value-of select="Customer/Extn/@IsCreditHold" />
				</xsl:attribute>
				<xsl:attribute name="ServiceHoldFlag">
					<xsl:value-of select="Customer/Extn/@IsServiceHold" />
				</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

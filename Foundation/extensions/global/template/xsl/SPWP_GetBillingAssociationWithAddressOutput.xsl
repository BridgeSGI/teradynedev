<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:attribute name="Ship-ToCustomerNumber"><xsl:value-of select="/CustomerContactList/CustomerContact/@CustomerKey"/></xsl:attribute>
<xsl:attribute name="IsBillTo"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsBillTo"/></xsl:attribute>
<xsl:attribute name="IsDefaultBillTo"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsDefaultBillTo"/></xsl:attribute>
<xsl:attribute name="IsDefaultShipTo"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsDefaultShipTo"/></xsl:attribute>
<xsl:attribute name="IsDefaultSoldTo"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsDefaultSoldTo"/></xsl:attribute>
<xsl:attribute name="IsShipTo"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsShipTo"/></xsl:attribute>
<xsl:attribute name="IsSoldTo"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsSoldTo"/></xsl:attribute>
<xsl:attribute name="AddressLine1"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine1"/></xsl:attribute>
<xsl:attribute name="AddressLine2"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine2"/></xsl:attribute>
<xsl:attribute name="AddressLine3"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine3"/></xsl:attribute>
<xsl:attribute name="AddressLine4"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine4"/></xsl:attribute>
<xsl:attribute name="AddressLine5"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine5"/></xsl:attribute>
<xsl:attribute name="AddressLine6"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine6"/></xsl:attribute>
<xsl:attribute name="AddressCity"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@City"/></xsl:attribute>
<xsl:attribute name="AddressCountry"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@Country"/></xsl:attribute>
<xsl:attribute name="AddressState"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@State"/></xsl:attribute>
<xsl:attribute name="AddressZipCode"><xsl:value-of select="/CustomerContactList/CustomerContact/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@ZipCode"/></xsl:attribute>
<xsl:attribute name="AddressLine1"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@AddressLine1"/></xsl:attribute>
<xsl:attribute name="AddressLine2"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@AddressLine2"/></xsl:attribute>
<xsl:attribute name="AddressLine3"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@AddressLine3"/></xsl:attribute>
<xsl:attribute name="AddressLine4"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@AddressLine4"/></xsl:attribute>
<xsl:attribute name="AddressLine5"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@AddressLine5"/></xsl:attribute>
<xsl:attribute name="AddressLine6"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@AddressLine6"/></xsl:attribute>
<xsl:attribute name="AddressCity"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@City"/></xsl:attribute>
<xsl:attribute name="AddressCountry"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@Country"/></xsl:attribute>
<xsl:attribute name="AddressState"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@State"/></xsl:attribute>
<xsl:attribute name="AddressZipCode"><xsl:value-of select="/CustomerContactList/CustomerPaymentMethodList/CustomerPaymentMethod/PersonInfoBillTo/@ZipCode"/></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
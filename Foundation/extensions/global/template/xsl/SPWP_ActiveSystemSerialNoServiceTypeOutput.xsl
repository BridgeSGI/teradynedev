<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:attribute name="TotalNumberOfRecords"><xsl:value-of select="/OrderLineList/@TotalNumberOfRecords"/></xsl:attribute>
<xsl:attribute name="AllocationDate"><xsl:value-of select="/OrderLineList/OrderLine/@AllocationDate"/></xsl:attribute>
<xsl:attribute name="SystemType"><xsl:value-of select="/OrderLineList/OrderLine/Item/@ItemID"/></xsl:attribute>
<xsl:attribute name="RespPartMfgDiv"><xsl:value-of select="/OrderLineList/OrderLine/Item/@ManufacturerName"/></xsl:attribute>
<xsl:attribute name="UnitOfMeasure"><xsl:value-of select="/OrderLineList/OrderLine/Item/@UnitOfMeasure"/></xsl:attribute>
<xsl:attribute name="SystemPTExceptionFlag"><xsl:value-of select="/OrderLineList/OrderLine/Item/Extn/@SystemPTExceptionFlag"/></xsl:attribute>
<xsl:attribute name="SystemTesterGroup"><xsl:value-of select="/OrderLineList/OrderLine/Item/Extn/@SystemTesterGroup"/></xsl:attribute>
<xsl:attribute name="CustomerNumber"><xsl:value-of select="/OrderLineList/OrderLine/Order/@EnterpriseCode"/></xsl:attribute>
<xsl:attribute name="EntitlementNumber"><xsl:value-of select="/OrderLineList/OrderLine/Order/@OrderNo"/></xsl:attribute>
<xsl:attribute name="EntitlementType"><xsl:value-of select="/OrderLineList/OrderLine/Order/@OrderType"/></xsl:attribute>
<xsl:attribute name="Status"><xsl:value-of select="/OrderLineList/OrderLine/OrderStatuses/OrderStatus/@Status"/></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetOrderCenter/SPWPGetOrderCenter/input">
	<xsl:template match="/in:Input">
		<xsl:element name="Customer">
			<xsl:attribute name="OrganizationCode">CSO</xsl:attribute>	  
			<xsl:attribute name="CustomerID">
				<xsl:value-of select="concat(in:GetOrderCenter/@CustomerNumber,'-',in:GetOrderCenter/@SiteID)"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	<xsl:element name="Output">
	<xsl:for-each select="OrderLineList/OrderLine">
						<xsl:if test="ItemDetails/PrimaryInformation/@ItemType='SYSTEM' ">
						<xsl:element name="AllActiveSystemTypes">
								<xsl:attribute name="SystemType">
									<xsl:value-of select="ItemDetails/@ItemID"/>
								</xsl:attribute>
								<xsl:attribute name="TesterGroup">
									<xsl:value-of select="ItemDetails/Extn/@SystemTesterGroup"/>
								</xsl:attribute>
						 </xsl:element>
				</xsl:if>
			</xsl:for-each>		
		</xsl:element>
  	</xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetOptionNumber/SPWPGetOptionNumber/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="OrderLine">
			<xsl:element name="Extn">
				<xsl:attribute name="ProdSerialNumber">
					<xsl:value-of select="@Prod_Serial_No"/>
				</xsl:attribute>
			</xsl:element>
			<xsl:element name="Item">
				<xsl:attribute name="ItemID">
					<xsl:value-of select="@Prod_No"/>
				</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
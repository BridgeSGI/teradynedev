<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:for-each select="MultiApi/API">
				<xsl:choose>
					<xsl:when test="Output/User/ContactPersonInfo">
						<xsl:element name="GetCostCenter">
							<xsl:attribute name="EMPLOYEE_ID"><xsl:value-of select="Output/User/@Loginid"/></xsl:attribute>
							<xsl:attribute name="COST_CENTER"><xsl:value-of select="Output/User/ContactPersonInfo/@AddressLine6"/></xsl:attribute>
						</xsl:element>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
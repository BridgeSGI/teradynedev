<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetOrderCenter/SPWPGetOrderCenter/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="Customer">
			<xsl:attribute name="CustomerID">
				<xsl:value-of select="@Cust_No" />-<xsl:value-of select="@Cust_Add_No" />
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
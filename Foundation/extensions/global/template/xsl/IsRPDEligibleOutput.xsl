<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:for-each select="OrderLineList/OrderLine">
				<xsl:if test="Extn/@RapidSourceCenter !='' ">
					<xsl:element name="IsRPDEligible">
								<xsl:attribute name="ServiceTypeEligible">
									<xsl:text>Y</xsl:text>
								</xsl:attribute>
				</xsl:element>
				</xsl:if>
				<!-- <xsl:if test="Extn/@RapidSourceCenter ='' ">
					<xsl:element name="IsRPDEligible">
								<xsl:attribute name="ServiceTypeEligible">
									<xsl:text></xsl:text>
								</xsl:attribute>
				</xsl:element>
				</xsl:if>  -->
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

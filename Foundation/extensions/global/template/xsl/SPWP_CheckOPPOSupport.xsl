<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="OrderLine">
<xsl:element name="Item">
<xsl:attribute name="ItemID"><xsl:value-of select="/Input/@SERVICETYPE"/></xsl:attribute>
</xsl:element>
<xsl:element name="Extn">
<xsl:attribute name="SerialNumber"><xsl:value-of select="/Input/@SYSTEMSERIALNO"/></xsl:attribute>
</xsl:element>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPPartNumberServiceType/SPWPPartNumberServiceType/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="Item">
			<xsl:attribute name="ItemID">
				<xsl:value-of select="@Part_Number"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:element name="GetOrderCenter">				
				<xsl:attribute name="OrderCenter">
					<xsl:value-of select="Customer/Extn/@DefaultOrderCenter" />
				</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

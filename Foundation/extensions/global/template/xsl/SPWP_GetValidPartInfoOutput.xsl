<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
      <xsl:element name="Output">
         <xsl:attribute name="PART_NUMBER">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/@ItemID" />
         </xsl:attribute>
         <xsl:attribute name="PART_DESCRIPTION">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/PrimaryInformation/@Description" />
         </xsl:attribute>
         <xsl:attribute name="PART_NOTES_DESCN_1">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/ItemInstructionList/ItemInstruction/@InstructionText" />
         </xsl:attribute>
         <xsl:attribute name="REPAIR_CODE_DESCN">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@RepairCode" />
         </xsl:attribute>
         <xsl:attribute name="SERVICE_TYPE_CODE">
            <xsl:value-of select="OrderLineList/OrderLine/Extn/@ServiceTypeCode" />
         </xsl:attribute>
         <xsl:attribute name="SERVICE_TYPE_DESCN">
            <xsl:value-of select="OrderLineList/OrderLine/Instructions/Instruction/@InstructionText" />
         </xsl:attribute>
         <xsl:attribute name="REPLACED_BY_PART_NUMBER">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@ReplacedByProduct" />
         </xsl:attribute>
         <xsl:attribute name="VISIBLE_TO_CUSTOMER_1">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/ItemInstructionList/ItemInstruction/@InstructionType" />
         </xsl:attribute>
         <xsl:attribute name="PART_MFG_DIVISION_CODE">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@RespProdMfgDivCode" />
         </xsl:attribute>
         <xsl:attribute name="MAX_ORDER_QTY">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/PrimaryInformation/@MaxOrderQuantity" />
         </xsl:attribute>
         <xsl:attribute name="SUPPORT_STATUS_DESCN">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@SupportStatusCode" />
         </xsl:attribute>
         <xsl:attribute name="OEM_SERIAL_REQUIRED_FLAG">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@OemSerialRequiredFlag" />
         </xsl:attribute>
         <xsl:attribute name="NO_AGREEMENT_COVERAGE_FLAG">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/Extn/@NoAgreementCoverFlag" />
         </xsl:attribute>
         
         <xsl:variable name="serviceType" select="OrderLineList/OrderLine/Extn/@ServiceTypeCode"/>
         <xsl:attribute name="EXPRESS_OR_NON_EXPRESS">
           <xsl:choose>
             <xsl:when test = "$serviceType = 'RPD' or $serviceType = 'EPS' or $serviceType = 'SDS' or $serviceType = 'ECAL' or $serviceType = 'EAR'">
                  <xsl:text>Express</xsl:text>
             </xsl:when>
             <xsl:otherwise>
                  <xsl:text>Non Express</xsl:text>
             </xsl:otherwise>
           </xsl:choose>
            
         </xsl:attribute>
         <xsl:attribute name="STDS_WITH_OVERHEAD_USD">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/PrimaryInformation/@UnitCost" />
         </xsl:attribute>
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>
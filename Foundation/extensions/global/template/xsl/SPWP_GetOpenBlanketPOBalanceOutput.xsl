 <?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:key name="RmngQty" match="OrderLine" use="@OrderLineKey"/>
<xsl:key name="RCVNode" match="OrderLine" use="@OrderLineKey"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:attribute name="ShipToCustomer"><xsl:value-of select="/Order/@EnterpriseCode"/></xsl:attribute>
<xsl:attribute name="BPONumber"><xsl:value-of select="/Order/@OrderNo"/></xsl:attribute>
<xsl:attribute name="CurrencyCode"><xsl:value-of select="/Order/PriceInfo/@Currency"/></xsl:attribute>
<xsl:for-each select="Order/OrderLines/OrderLine"> 
<OrderLine> 
   <xsl:variable name="Pos" select="number(position())"/>
    <xsl:variable name="OrderLineKey" select="/Order/OrderLines/OrderLine[$Pos]/@OrderLineKey"/>
    <xsl:variable name="RemainingQty" select="key('RmngQty',$OrderLineKey)/@RemainingQty"/>
    <xsl:variable name="ReceivingNode" select="key('RCVNode',$OrderLineKey)/@ReceivingNode"/>
    
    <xsl:attribute name="POBalance">
     <xsl:value-of select="$RemainingQty"/>
    </xsl:attribute> 
    <xsl:attribute name="ShipToCustomerSiteNo">
     <xsl:value-of select="$ReceivingNode"/>
    </xsl:attribute> 
</OrderLine>    
</xsl:for-each>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
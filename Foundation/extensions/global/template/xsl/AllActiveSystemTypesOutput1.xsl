<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 
   <xsl:key name="keyEmpByName" match="//AllActiveSystemTypes" use="@SystemType"/>
 
   <xsl:template match="/">
    <xsl:element name="Output">
                  <xsl:for-each select="//AllActiveSystemTypes[generate-id() =
                        generate-id(key('keyEmpByName',@SystemType)[1])]">
                        
                        <xsl:element name="AllActiveSystemTypes">
                        	<xsl:attribute name="SystemType">
                			<xsl:value-of select="@SystemType"/> 
                			</xsl:attribute>
                			<xsl:attribute name="TesterGroup">
                			 <xsl:value-of select ="@TesterGroup"/>
                			 </xsl:attribute>
                			</xsl:element>
            </xsl:for-each>
         </xsl:element>
   </xsl:template>
</xsl:stylesheet>
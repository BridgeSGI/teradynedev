<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:attribute name="PartyNumber"><xsl:value-of select="/CustomerList/Customer/@CustomerID"/></xsl:attribute>
<xsl:attribute name="CustomerAdditionalAddressID"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/@CustomerAdditionalAddressID"/></xsl:attribute>
<xsl:attribute name="AddressType"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/@AddressType"/></xsl:attribute>
<xsl:attribute name="IsBillTo"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsBillTo"/></xsl:attribute>
<xsl:attribute name="IsDefaultBillTo"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsDefaultBillTo"/></xsl:attribute>
<xsl:attribute name="IsDefaultShipTo"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsDefaultShipTo"/></xsl:attribute>
<xsl:attribute name="IsDefaultSoldTo"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsDefaultSoldTo"/></xsl:attribute>
<xsl:attribute name="IsInherited"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsInherited"/></xsl:attribute>
<xsl:attribute name="IsShipTo"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsShipTo"/></xsl:attribute>
<xsl:attribute name="IsSoldTo"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/@IsSoldTo"/></xsl:attribute>
<xsl:attribute name="AddressLine1"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine1"/></xsl:attribute>
<xsl:attribute name="AddressLine2"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine2"/></xsl:attribute>
<xsl:attribute name="AddressLine3"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine3"/></xsl:attribute>
<xsl:attribute name="AddressLine4"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine4"/></xsl:attribute>
<xsl:attribute name="AddressLine5"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine5"/></xsl:attribute>
<xsl:attribute name="AddressLine6"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressLine6"/></xsl:attribute>
<xsl:attribute name="AddressCity"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressCity"/></xsl:attribute>
<xsl:attribute name="AddressCountry"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressCountry"/></xsl:attribute>
<xsl:attribute name="AddressState"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressState"/></xsl:attribute>
<xsl:attribute name="AddressZipCode"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AddressZipCode"/></xsl:attribute>
<xsl:attribute name="AlternateEmailID"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@AlternateEmailID"/></xsl:attribute>
<xsl:attribute name="Beeper"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@Beeper"/></xsl:attribute>
<xsl:attribute name="Company"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@Company"/></xsl:attribute>
<xsl:attribute name="DayPhone"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@DayPhone"/></xsl:attribute>
<xsl:attribute name="Department"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@Department"/></xsl:attribute>
<xsl:attribute name="EMailID"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@EMailID"/></xsl:attribute>
<xsl:attribute name="ErrorTxt"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@ErrorTxt"/></xsl:attribute>
<xsl:attribute name="EveningFaxNo"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@EveningFaxNo"/></xsl:attribute>
<xsl:attribute name="EveningPhone"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@EveningPhone"/></xsl:attribute>
<xsl:attribute name="FirstName"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@FirstName"/></xsl:attribute>
<xsl:attribute name="HttpUrl"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@HttpUrl"/></xsl:attribute>
<xsl:attribute name="LastName"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@LastName"/></xsl:attribute>
<xsl:attribute name="MiddleName"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@MiddleName"/></xsl:attribute>
<xsl:attribute name="MobilePhone"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@MobilePhone"/></xsl:attribute>
<xsl:attribute name="OtherPhone"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@OtherPhone"/></xsl:attribute>
<xsl:attribute name="PersonInfoKey"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@PersonInfoKey"/></xsl:attribute>
<xsl:attribute name="PreferredShipAddress"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@PreferredShipAddress"/></xsl:attribute>
<xsl:attribute name="Suffix"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@Suffix"/></xsl:attribute>
<xsl:attribute name="Title"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@Title"/></xsl:attribute>
<xsl:attribute name="UseCount"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@UseCount"/></xsl:attribute>
<xsl:attribute name="VerificationStatus"><xsl:value-of select="/CustomerList/Customer/CustomerAdditionalAddressList/CustomerAdditionalAddress/PersonInfo/@VerificationStatus"/></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
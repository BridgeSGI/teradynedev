<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetCustomerStanding/SPWPGetCustomerStanding/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="Organization">
			<xsl:attribute name="OrganizationCode">
				<xsl:value-of select="concat(in:GetCustomerStanding/@CustomerNumber,'-',in:GetCustomerStanding/@SiteID)" />
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
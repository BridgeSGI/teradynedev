<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPIsOrderDuplicate/SPWPIsOrderDuplicate/input">
	<xsl:template match="/in:Input">
		<xsl:element name="MultiApi">
			<xsl:for-each select="in:IsOrderDuplicate">
				<xsl:element name="API">
					<xsl:attribute name="Name">getOrderLineList</xsl:attribute>
					<xsl:element name="Input">
						<xsl:element name="OrderLine">
							<xsl:element name="Item">
								<xsl:attribute name="ItemId">
									<xsl:value-of select="@PART_NUMBER"/>
								</xsl:attribute>
							</xsl:element>
							<xsl:element name="Order">
								<xsl:attribute name="ReceivingNode">
									<xsl:value-of select="concat(@CUSTOMER_NO,'-',@SITE_ID)" />
								</xsl:attribute>
									<xsl:attribute name="OrderDateQryType">DATERANGE</xsl:attribute>
								<xsl:attribute name="FromOrderDate">
									<xsl:value-of select="@ORDER_START_DATE" />
								</xsl:attribute>
								<xsl:attribute name="ToOrderDate">
									<xsl:value-of select="@ORDER_END_DATE" />
								</xsl:attribute>
							</xsl:element>
							<xsl:element name="Extn">
								<xsl:attribute name="SystemSerialNo">
									<xsl:value-of select="@SYSTEM_SERIAL_NO" />
								</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<xsl:element name="Template">
						<xsl:element name="OrderLineList">
							<xsl:element name="OrderLine">
								<xsl:attribute name="OrderLineKey"/>
								<xsl:element name="Order">
									<xsl:attribute name="OrderNo"/>
									<xsl:attribute name="OrderDate"/>
									<xsl:attribute name="ReqDeliveryDate"/>
								</xsl:element>
								
							</xsl:element>
						</xsl:element>
					</xsl:element>
					
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

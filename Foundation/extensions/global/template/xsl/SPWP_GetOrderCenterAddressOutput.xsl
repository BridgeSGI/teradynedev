<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:attribute name="ServiceCenterCode"><xsl:value-of select="/OrganizationList/Organization/@OrganizationKey"/></xsl:attribute>
<xsl:attribute name="AddressLine1"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@AddressLine1"/></xsl:attribute>
<xsl:attribute name="AddressLine2"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@AddressLine2"/></xsl:attribute>
<xsl:attribute name="AddressLine3"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@AddressLine3"/></xsl:attribute>
<xsl:attribute name="AddressLine4"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@AddressLine4"/></xsl:attribute>
<xsl:attribute name="AddressLine5"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@AddressLine5"/></xsl:attribute>
<xsl:attribute name="AddressLine6"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@AddressLine6"/></xsl:attribute>
<xsl:attribute name="AddressCity"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@City"/></xsl:attribute>
<xsl:attribute name="AddressCountry"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@Country"/></xsl:attribute>
<xsl:attribute name="AddressState"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@State"/></xsl:attribute>
<xsl:attribute name="AddressZipCode"><xsl:value-of select="/OrganizationList/Organization/CorporatePersonInfo/@ZipCode"/></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
 <xsl:element name="Output">
 <xsl:for-each select="MultiApi/API/Output">
  <xsl:element name="GetBillingAssociationWithAddress">
				   <xsl:attribute name="ShipToCustomerNumber">
				   <xsl:value-of select="substring-before(Customer/@CustomerID,'-')"/>
				   </xsl:attribute>
				   <xsl:attribute name="ShipToSiteID">
				   <xsl:value-of select="substring-after(Customer/@CustomerID,'-')"/>
				   </xsl:attribute>
				   <xsl:attribute name="BillToCustomerNumber">
                     <xsl:value-of select="substring-before(Customer/@BuyerOrganizationCode,'-')" />
                  </xsl:attribute>

                  <xsl:attribute name="BillToSiteID">
                     <xsl:value-of select="substring-after(Customer/@BuyerOrganizationCode,'-')" />
                  </xsl:attribute>

                  <xsl:attribute name="BillToCustomerName">
                     <xsl:value-of select="Customer/BuyerOrganization/@OrganizationName" />
                  </xsl:attribute>

                  <xsl:attribute name="DefaultFlag">
                     <xsl:value-of select="Customer/BuyerOrganization/Extn/@CustomerSiteStatus" />
                  </xsl:attribute>

               
                           <xsl:attribute name="Address-Line1">
                              <xsl:value-of select="Customer/BuyerOrganization/BillingPersonInfo/@AddressLine1" />
                           </xsl:attribute>

                           <xsl:attribute name="Address-Line2">
                              <xsl:value-of select="Customer/BuyerOrganization/BillingPersonInfo/@AddressLine2" />
                           </xsl:attribute>

                           <xsl:attribute name="Address-Line3">
                              <xsl:value-of select="Customer/BuyerOrganization/BillingPersonInfo/@AddressLine3" />
                           </xsl:attribute>

                           <xsl:attribute name="Address-Line4">
                              <xsl:value-of select="Customer/BuyerOrganization/BillingPersonInfo/@AddressLine4" />
                           </xsl:attribute>

                           <xsl:attribute name="Address-Line5">
                              <xsl:value-of select="Customer/BuyerOrganization/BillingPersonInfo/@AddressLine5" />
                           </xsl:attribute>

                           <xsl:attribute name="Address-City">
                              <xsl:value-of select="Customer/BuyerOrganization/BillingPersonInfo/@City" />
                           </xsl:attribute>

                           <xsl:attribute name="Address-State">
                              <xsl:value-of select="Customer/BuyerOrganization/BillingPersonInfo/@State" />
                           </xsl:attribute>

                           <xsl:attribute name="Address-Country">
                              <xsl:value-of select="Customer/BuyerOrganization/BillingPersonInfo/@Country" />
                           </xsl:attribute>

                           <xsl:attribute name="Address-ZipCode">
                              <xsl:value-of select="Customer/BuyerOrganization/BillingPersonInfo/@ZipCode" />
                           </xsl:attribute>
                  
  </xsl:element>
 </xsl:for-each>
 </xsl:element>
</xsl:template>
</xsl:stylesheet>

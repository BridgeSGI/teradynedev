<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:attribute name="TotalNumberOfRecords"><xsl:value-of select="OrderLineList/@TotalNumberOfRecords"/></xsl:attribute>
<xsl:attribute name="PrimeLineNo"><xsl:value-of select="OrderLineList/OrderLine/@PrimeLineNo"/></xsl:attribute>
<xsl:attribute name="SYSTEM_SERIAL_NO"><xsl:value-of select="OrderLineList/OrderLine/Extn/@SerialNumber"/></xsl:attribute>
<xsl:attribute name="ENTITLEMENT_TYPE_CODE"><xsl:value-of select="OrderLineList/OrderLine/Item/@ItemID"/></xsl:attribute>
<xsl:attribute name="SERVICE_TYPE"><xsl:value-of select="OrderLineList/OrderLine/Order/@OrderType"/></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
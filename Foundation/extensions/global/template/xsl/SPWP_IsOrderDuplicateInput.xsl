<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
   <xsl:element name="OrderLine">
   	<xsl:element name="Item">
         <xsl:attribute name="ItemId">
            <xsl:value-of select="Input/@PART_NUMBER" />
         </xsl:attribute>
      </xsl:element>
      <xsl:element name="Order">
         <xsl:attribute name="EnterpriseCode">
            <xsl:value-of select="Input/@PARTY_ADDRESS_NUMBER" />
         </xsl:attribute>
         <xsl:attribute name="BuyerOrganizationCode">
            <xsl:value-of select="Input/@PARTY_NUMBER" />
         </xsl:attribute>
         <xsl:attribute name="ReqDeliveryDate">
            <xsl:value-of select="Input/@ORDER_END_DATE" />
         </xsl:attribute>
         <xsl:attribute name="FromOrderDate">
            <xsl:value-of select="concat(Input/@ORDER_START_DATE,'T00:00:00')" />
         </xsl:attribute>
         <xsl:attribute name="ToOrderDate">
            <xsl:value-of select="concat(Input/@ORDER_START_DATE,'T23:59:59')" />
         </xsl:attribute>
         <xsl:attribute name="OrderDateQryType">
            <xsl:text>BETWEEN</xsl:text>
         </xsl:attribute>
      </xsl:element>
       <xsl:element name="Extn">
         <xsl:attribute name="SerialNumber">
            <xsl:value-of select="Input/@SYSTEM_SERIAL_NO" />
         </xsl:attribute>
      </xsl:element>
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	<xsl:element name="Output">
		<xsl:for-each select="MultiApi/API/Output/OrderLineList/OrderLine">
				<!-- <xsl:if test="@MinLineStatus='1100.30' or @MinLineStatus='1100.50'"> -->
				<xsl:if test="Extn/@MktStatus='A'">
					<xsl:element name="SystemTypeTesterGroup">
					<xsl:attribute name="System_Type">
						<xsl:value-of select="ItemDetails/@ItemID"/>
					</xsl:attribute>
					<xsl:attribute name="Tester_Group">
						<xsl:value-of select="ItemDetails/Extn/@SystemTesterGroup"/>
					</xsl:attribute>
					</xsl:element>
				</xsl:if>
<!-- 			</xsl:if> -->
		</xsl:for-each>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>

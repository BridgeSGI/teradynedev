<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
				<xsl:attribute name="Can_Order_Flag">
					<xsl:value-of select="/OrderLineList/OrderLine/Extn/@NonRepPartsCovered"/>
				</xsl:attribute>			
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
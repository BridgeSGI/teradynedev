<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:for-each select="MultiApi/API">
				<xsl:for-each select="Output/OrderLineList/OrderLine">
					<xsl:element name="IsOrderDuplicate">
						<xsl:attribute name="ORDER_NUMBER">
							<xsl:value-of select="Order/@OrderNo" />
						</xsl:attribute>
					</xsl:element>
				</xsl:for-each>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
				<xsl:attribute name="Service_Type_Code">
					<xsl:value-of select="/ItemList/Item/PrimaryInformation/@ItemType"/>
				</xsl:attribute>	
				<xsl:attribute name="Service_Type_Descn">
					<xsl:value-of select="/ItemList/Item/PrimaryInformation/@Description"/>
				</xsl:attribute>		
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
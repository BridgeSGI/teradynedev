<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetBillingAssociation/SPWPGetBillingAssociation/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="Organization">
			<ComplexQuery Operator="And">
				<And>
					<Or>
						<xsl:for-each select="in:Site">
							<Exp>
								<xsl:attribute name="Name">OrganizationCode</xsl:attribute>
								<xsl:attribute name="Value"><xsl:value-of select="@Cust_No"/>-<xsl:value-of select="@Cust_Add_No" /></xsl:attribute>
							</Exp>
						</xsl:for-each>
					</Or>
				</And>
			</ComplexQuery>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
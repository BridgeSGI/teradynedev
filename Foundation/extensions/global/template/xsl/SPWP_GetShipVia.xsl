<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:template match="/ScacAndServiceList">
<xsl:element name="Carrier">
<xsl:attribute name="ShipViaCarrier"><xsl:value-of select="/ScacAndServiceList/ScacAndService/@CarrierType" /></xsl:attribute>
<xsl:attribute name="ShipViaService"><xsl:value-of select="/ScacAndServiceList/ScacAndService/@ScacKey" /></xsl:attribute>
<xsl:attribute name="ShipViaDescription"><xsl:value-of select="/ScacAndServiceList/ScacAndService/@ScacAndServiceDesc" /></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
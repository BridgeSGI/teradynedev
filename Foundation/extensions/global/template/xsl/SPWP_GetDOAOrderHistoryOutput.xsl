<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:attribute name="OrderNo"><xsl:value-of select="Order/@OrderNo"/></xsl:attribute>
<xsl:attribute name="DefPartSerialNo"><xsl:value-of select="Order/OrderLines/OrderLine/@SerialNo"/></xsl:attribute>
<xsl:attribute name="HandlingNoChargeCode"><xsl:value-of select="Order/OrderLines/OrderLine/Extn/@HandlingNoChargeCode"/></xsl:attribute>
<xsl:attribute name="ShipmentDate"><xsl:value-of select="Order/OrderLines/OrderLine/Extn/Schedules/Schedule/@ExpectedShipmentDate"/></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml"/>	
<xsl:template match="/">
 	<xsl:element name="Shipment">
  		<xsl:attribute name="EnterpriseCode">
         			<xsl:value-of select="/Order/@EnterpriseCode"/>
      		</xsl:attribute>
		<xsl:attribute name="SellerOrganizationCode">
         			<xsl:value-of select="/Order/@SellerOrganizationCode"/>
      		</xsl:attribute>
		
      		<xsl:element name="ShipmentLines">
  		
      		<xsl:for-each select="/Order/OrderLines/OrderLine">
			<xsl:element name="ShipmentLine">
				
				<xsl:attribute name="OrderHeaderKey">
						<xsl:value-of select="@OrderHeaderKey"/>
				</xsl:attribute>
				<xsl:attribute name="OrderLineKey">
				<xsl:value-of select="@OrderLineKey"/>
			</xsl:attribute>
				<xsl:attribute name="Quantity">
				<xsl:value-of select="@OrderedQty"/>
				</xsl:attribute>
				
			</xsl:element>
      		</xsl:for-each>





                </xsl:element>
                </xsl:element>

</xsl:template>
</xsl:stylesheet>

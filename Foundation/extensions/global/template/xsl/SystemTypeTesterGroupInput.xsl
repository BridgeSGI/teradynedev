<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPSystemTypeTesterGroup/SPWPSystemTypeTesterGroup/input">
	
	<xsl:template match="/in:Input">
		<xsl:element name="MultiApi">
			<xsl:for-each select="in:SystemTypeTesterGroup">
				<xsl:element name="API">
					<xsl:attribute name="Name">getOrderLineList</xsl:attribute>
						<xsl:element name="Input">
							<xsl:element name="OrderLine">
								<xsl:element name="Order">
									<xsl:attribute name="DocumentType">0017.ex</xsl:attribute>
								</xsl:element>
								<xsl:element name="Extn">
										<xsl:attribute name="MktStatus">
										<xsl:text>A</xsl:text>
										</xsl:attribute>
									
								</xsl:element>
								<xsl:element name="Item">
										<xsl:attribute name="ItemID">
											<xsl:value-of select="@System_Type"/>
										</xsl:attribute>	
									
								</xsl:element>
							</xsl:element>
						</xsl:element>
					
					<xsl:element name="Template">
						<xsl:element name="OrderLineList">
							<xsl:element name="OrderLine">
								<xsl:attribute name="OrderLineKey"> </xsl:attribute>
								<xsl:attribute name="MinLineStatus"> </xsl:attribute>
									<xsl:element name="Extn">
										
										<xsl:attribute name="MktStatus">
										</xsl:attribute>
										
									</xsl:element>
									<xsl:element name="ItemDetails">
										<xsl:attribute name="ItemID">
										</xsl:attribute>
										<xsl:element name="Extn">
										<xsl:attribute name="SystemTesterGroup">
										</xsl:attribute>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						
					</xsl:element>

					</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

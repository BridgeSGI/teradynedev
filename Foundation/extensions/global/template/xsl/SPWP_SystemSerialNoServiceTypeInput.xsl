<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
   <xsl:element name="OrderLine">
       <xsl:element name="Extn">
         <xsl:attribute name="SerialNumber">
            <xsl:value-of select="Input/@SYSTEM_SERIAL_NO" />
         </xsl:attribute>
      </xsl:element>
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:attribute name="Party Number"><xsl:value-of select="Customer/@CustomerID"/></xsl:attribute>
<xsl:attribute name="InternalCustomerFlag"><xsl:value-of select="Customer/Extn/@InternalCustomerFlag"/></xsl:attribute>
<xsl:attribute name="SpecialInstructionsVisible"><xsl:value-of select="Customer/Extn/@SpecialInstructionsVisible"/></xsl:attribute>
<xsl:attribute name="TimeZone"><xsl:value-of select="Customer/BuyerOrganization/@LocaleCode"/></xsl:attribute>
<xsl:attribute name="Company"><xsl:value-of select="Customer/BuyerOrganization/CorporatePersonInfo/@Company"/></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
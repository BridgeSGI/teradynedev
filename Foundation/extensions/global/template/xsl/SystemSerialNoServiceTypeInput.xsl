<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPSystemSerialNoServiceType/SPWPSystemSerialNoServiceType/input">
		<xsl:template match="/in:Input">
		<xsl:element name="MultiApi">
			<xsl:for-each select="in:SystemSerialNoServiceType">
				<xsl:element name="API">
					<xsl:attribute name="Name">getOrderLineList</xsl:attribute>
					<xsl:element name="Input">
						<xsl:element name="OrderLine">
							<xsl:attribute name="DocumentType">0018.ex</xsl:attribute>
							<xsl:element name="Order">
								<xsl:element name="Extn">
								<xsl:attribute name="ExpirationDateQryType">DATERANGE</xsl:attribute>
								<xsl:attribute name="FromExpirationDate">
									<xsl:value-of select="@Expiration_Date"/>
								</xsl:attribute>
								<xsl:attribute name="ToExpirationDate">
									<xsl:value-of select="@Expiration_Date"/>
								</xsl:attribute>
							</xsl:element>
							</xsl:element>
							<xsl:element name="Extn">
								<xsl:attribute name="SystemSerialNo">
									<xsl:value-of select="@System_Serial_No"/>
								</xsl:attribute>	
								 <xsl:attribute name="SCLineStatus">A</xsl:attribute>
							</xsl:element>	
						</xsl:element>	
					</xsl:element>	
					
				
					<xsl:element name="Template">
						<xsl:element name="OrderLineList">
							<xsl:element name="OrderLine">
								<xsl:attribute name="OrderLineKey"/>
								<xsl:element name="Extn">
									<xsl:attribute name="ServiceType">
									</xsl:attribute>
									<xsl:attribute name="SystemType">
									</xsl:attribute>
									<xsl:attribute name="RepairablePartsCovered">
									</xsl:attribute>
									<xsl:attribute name="NonrepairPartsCovered">
									</xsl:attribute>
									<xsl:attribute name="SCLineStatus">
									</xsl:attribute>
									
								</xsl:element>
								<xsl:element name="ItemDetails">
									<xsl:attribute name="ItemID">
										</xsl:attribute>
									<xsl:element name="Extn">
										<xsl:attribute name="RespProdRepairDivCode">
										</xsl:attribute>
										<xsl:attribute name="SystemTesterGroup">
										</xsl:attribute>
										<xsl:attribute name="SystemPTExceptionFlag">
										</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<xsl:element name="Order">
									<xsl:attribute name="OrderNo">
									</xsl:attribute>
									<xsl:attribute name="OrderType">
									</xsl:attribute>
									<xsl:element name="Extn">
									<xsl:attribute name="ExpirationDate">
									</xsl:attribute>
									</xsl:element>
								</xsl:element>
						
							</xsl:element>
						</xsl:element>
					</xsl:element>
					
				</xsl:element>	
					
				</xsl:for-each>
		</xsl:element>	
	</xsl:template>
</xsl:stylesheet>

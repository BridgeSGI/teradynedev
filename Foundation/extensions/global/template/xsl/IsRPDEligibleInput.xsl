<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPIsRPDEligible/SPWPIsRPDEligible/input">
   <xsl:template match="/in:Input">
   <xsl:element name="OrderLine">   
		<xsl:attribute name="ShipToID">
			<xsl:value-of select="concat(in:IsRPDEligible/@CustomerNumber,'-',in:IsRPDEligible/@CustomerSiteNo)"/>
		</xsl:attribute>
		
		<xsl:element name="Order"> 
			<xsl:attribute name="DocumentType">
				<xsl:text>0018.ex</xsl:text>
			</xsl:attribute>
		</xsl:element>
			<xsl:element name="Extn">
				<xsl:attribute name="MfgID">
					<xsl:value-of select="in:IsRPDEligible/@RespProductMfgDivisionCode"/>
				</xsl:attribute>
				<xsl:attribute name="SystemSerialNo">
					<xsl:value-of select="in:IsRPDEligible/@SystemSerialNumber"/>
				</xsl:attribute>
				<xsl:attribute name="SystemType">
					<xsl:value-of select="in:IsRPDEligible/@SystemType"/>
				</xsl:attribute>
				<!--<xsl:attribute name="RapidSourceCenter">Y</xsl:attribute>-->
			</xsl:element>
				
			<xsl:element name="Item">
				<xsl:attribute name="ItemId">
					<xsl:value-of select="in:IsRPDEligible/@PartNumber"/>
				</xsl:attribute>
			</xsl:element>
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>

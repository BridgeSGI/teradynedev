<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
      <xsl:element name="Output">
         <xsl:attribute name="SYSTEM_SERIAL_NO">
            <xsl:value-of select="OrderLineList/OrderLine/Extn/@SerialNumber" />
         </xsl:attribute>
         <xsl:attribute name="SYSTEM_TYPE_CODE">
            <xsl:value-of select="OrderLineList/OrderLine/ItemDetails/PrimaryInformation/@ItemType" />
         </xsl:attribute>    
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	<xsl:element name="Output">
	<xsl:element name="GetShipViaList">
    	<xsl:for-each select="ScacAndServiceList/ScacAndService">
		<xsl:element name="GetShipVia">
			<xsl:attribute name="ShipViaCarrier">
				<xsl:value-of select="@ScacKey" />
			</xsl:attribute>
			<xsl:attribute name="ShipViaService">
				<xsl:value-of select="@ScacAndService" />
			</xsl:attribute>
			<xsl:attribute name="ShipViaDescription">
				<xsl:value-of select="@ScacAndServiceDesc" />
			</xsl:attribute>			
		</xsl:element>
	</xsl:for-each>
	</xsl:element>
	</xsl:element>
  </xsl:template>
</xsl:stylesheet>

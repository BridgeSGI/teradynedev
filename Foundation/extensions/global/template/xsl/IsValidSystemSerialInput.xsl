<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPIsValidSystemSerial/SPWPIsValidSystemSerial/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="MultiApi">
			<xsl:for-each select="in:IsVaildSystemSerial">
				<xsl:element name="API">
					<xsl:attribute name="Name">getOrderLineList</xsl:attribute>
					<xsl:element name="Input">
						<xsl:element name="OrderLine">
							
							
							
							<xsl:element name="Order">
								<xsl:attribute name ="ShipToIDQryType">FLIKE</xsl:attribute>
								<xsl:attribute name="DocumentType">0017.ex</xsl:attribute>
									
								<xsl:attribute name="ShipToID">
									<xsl:value-of select="@SYSTEM_CUSTOMER_NO"/>
								</xsl:attribute>
							</xsl:element>
			 				 <xsl:element name="Extn">
								 <xsl:attribute name="SystemSerialNo">
								    <xsl:value-of select="@SYSTEM_SERIAL_NO" />
								 </xsl:attribute>
								<xsl:attribute name="ProcessingStatus">A</xsl:attribute>
						      </xsl:element>
						</xsl:element>
					</xsl:element>
					<xsl:element name="Template">
						<xsl:element name="OrderLineList">
							<xsl:element name="OrderLine">
								<xsl:attribute name="OrderLineKey"/>
								<xsl:element name="Extn">
									
									<xsl:attribute name="SystemSerialNo"/>
									<xsl:attribute name="ActualInsDate"/>
								</xsl:element>
								<xsl:element name="Order">
								<xsl:element name="OrderLines">
								<xsl:element name="OrderLine">
								<xsl:attribute name="OrderLineKey"/>
								<xsl:element name="ItemDetails">
									<xsl:attribute name="ItemID"/>
									<xsl:element name="PrimaryInformation">
									<xsl:attribute name="ItemType"/>
									</xsl:element>
									<xsl:element name="Extn">
										<xsl:attribute name="SystemPTExceptionFlag"/>
										<xsl:attribute name="SystemTesterGroup"/>
									</xsl:element>
								</xsl:element>	
								</xsl:element>
								</xsl:element>
								</xsl:element>								
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:attribute name="SystemType"><xsl:value-of select="/ItemList/Item/@ItemID"/></xsl:attribute>
<xsl:attribute name="SystemTesterGroup"><xsl:value-of select="/ItemList/Item/Extn/@SystemTesterGroup"/></xsl:attribute>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	<xsl:element name="Output">
		<xsl:for-each select="MultiApi/API/Output/OrderLineList/OrderLine">
			<xsl:if test=" ItemDetails/PrimaryInformation/@ItemType = 'Service' ">
			<xsl:element name="ActiveSystemSerialNoServiceType">
		
					<xsl:attribute name="Service_Type">
						<xsl:value-of select="ItemDetails/@ItemID"/>
					</xsl:attribute>
					<xsl:attribute name="Entitlement_Number">
						<xsl:value-of select="substring-before(Order/@OrderNo,'-')"/>
					</xsl:attribute>
					<xsl:attribute name="Entitlement_Type">
						<xsl:value-of select="substring-after(Order/@OrderNo,'-')"/>
					</xsl:attribute>
					<xsl:attribute name="Resp_Part_Mfg_Div">
							<xsl:value-of select="ItemDetails/Extn/@RespProdMfgDivCode"/>
					</xsl:attribute>
					<xsl:attribute name="Tester_Group">
							<xsl:value-of select="ItemDetails/Extn/@SystemTesterGroup"/>
					</xsl:attribute>
					<xsl:attribute name="PT_Exception_Flag">
							<xsl:value-of select="ItemDetails/Extn/@SystemPTExceptionFlag"/>
					</xsl:attribute>
					<xsl:attribute name="System_Type">
						<xsl:value-of select="Extn/@SystemType"/>
					</xsl:attribute>
			
					<xsl:if test="Extn/@RepairablePartsCovered ='Y' and Extn/@NonrepairPartsCovered ='Y'">
						<xsl:attribute name="Part_Coverage_Flag">B</xsl:attribute>
					</xsl:if>
					<xsl:if test="Extn/@RepairablePartsCovered ='Y' and Extn/@NonrepairPartsCovered ='N'">
						<xsl:attribute name="Part_Coverage_Flag">R</xsl:attribute>
					</xsl:if>
					<xsl:if test="Extn/@RepairablePartsCovered ='N' and Extn/@NonrepairPartsCovered ='Y'">
						<xsl:attribute name="Part_Coverage_Flag">C</xsl:attribute>
					</xsl:if>					
									
				</xsl:element>
				</xsl:if>
			</xsl:for-each>	
			</xsl:element>
			</xsl:template>
</xsl:stylesheet>

				
				
			
			
			
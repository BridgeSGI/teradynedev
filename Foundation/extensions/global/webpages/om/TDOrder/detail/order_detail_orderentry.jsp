<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script language="Javascript" >
	IgnoreChangeNames();
	yfcDoNotPromptForChanges(true);
</script>

<%
    // Default the enterprise code if it is not passed
    String enterpriseCode = (String) request.getParameter("xml:/Order/@EnterpriseCode");
    if (isVoid(enterpriseCode)) {
        enterpriseCode = getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@PrimaryEnterpriseKey");
        request.setAttribute("xml:/Order/@EnterpriseCode", enterpriseCode);
    }

	// Default the seller to logged in organization if it plays a role of seller
    String sellerOrgCode = (String) request.getParameter("xml:/Order/@SellerOrganizationCode");
    if (isVoid(sellerOrgCode)) {
        if(isRoleDefaultingRequired((YFCElement) request.getAttribute("CurrentOrgRoleList"))){
            sellerOrgCode = getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@OrganizationCode");
            //System.out.println("org:" + orgCode);
            //request.setAttribute("xml:/Order/@SellerOrganizationCode", orgCode);
        }
    }
    
    //prepareMasterDataElements(enterpriseCode, (YFCElement) request.getAttribute("OrganizationList"),
    //                        (YFCElement) request.getAttribute("EnterpriseParticipationList"),
    //                        (YFCElement) request.getAttribute("CurrencyList"),
    //                        (YFCElement) request.getAttribute("OrderTypeList"),getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@IsHubOrganization"));

	String exchangeOrderForReturn = resolveValue("xml:/ReturnOrder/@ReturnOrderHeaderKeyForExchange");
	String orderHeaderKeyVal = resolveValue("xml:/Order/@OrderHeaderKey");	
%>

<script language="javascript">
    <% if (!equals(exchangeOrderForReturn, "Y")) {

        if (!isVoid(orderHeaderKeyVal)) {	 
            YFCDocument orderDoc = YFCDocument.createDocument("Order");
            orderDoc.getDocumentElement().setAttribute("OrderHeaderKey",resolveValue("xml:/Order/@OrderHeaderKey"));

            // If this screen is shown as a popup, then open the order detail view for the new order
            // as a popup as well (instead of refreshing the same screen).
            if (equals(request.getParameter(YFCUIBackendConsts.YFC_IN_POPUP), "Y")) {
            %>
                function showOrderDetailPopup() {
					window.CloseOnRefresh = "Y";
		            callPopupWithEntity('order', '<%=orderDoc.getDocumentElement().getString(false)%>');
					window.close();
				}
                window.attachEvent("onload", showOrderDetailPopup);
            <%
            } else {
            %>
                function showOrderDetail() {
				    showDetailFor('<%=orderDoc.getDocumentElement().getString(false)%>');
				}
                window.attachEvent("onload", showOrderDetail);
            <% }
        }
    }
	%>
</script>
<%
	//exchange order processing
	boolean isExchangeOrderCreation = false;
	if(!isVoid(exchangeOrderForReturn)){
		isExchangeOrderCreation = true;
		//call getOrderDetails api for defaulting information onto exchange order.
%>
	    <yfc:callAPI apiID="AP5"/>
<%	} %>

<script>
function showCustomerLookupPopup(BuyerOrganizationCode, entityname)
	{
		//BuyerOrganizationCode attribute of <Order /> is used as the customer #
		var oObj = new Object();
		oObj.field1 = document.all(BuyerOrganizationCode);
		
		
		yfcShowSearchPopupWithParams('','customerlookup',900,550,oObj,entityname);
		
	}
		
</script>

<table class="view" width="100%">
    <tr>
        <td>
            <input type="hidden" name="xml:/Order/@DraftOrderFlag" value="Y"/>
            <input type="hidden" name="xml:/Order/@EnteredBy" value="<%=resolveValue("xml:CurrentUser:/User/@Loginid")%>"/>
			<% if(isExchangeOrderCreation){ %>
				<input type="hidden" name="xml:/Order/@ReturnOrderHeaderKeyForExchange" value='<%=exchangeOrderForReturn%>'/>
				<input type="hidden" name="xml:/Order/@OrderPurpose" value='EXCHANGE'/>
				<input type="hidden" name="xml:/Order/@DocumentType" value='0001'/>
								

			<% } %>
				<input type="hidden" name="xml:/Order/@ShipToKey" value="<%=resolveValue("xml:/Order/@ShipToKey")%>"/>
				<input type="hidden" name="xml:/Order/@BillToKey" value="<%=resolveValue("xml:/Order/@BillToKey")%>"/>								
				<input type="hidden" name="xml:/Order/OrderLine/@ItemID" value="<%=resolveValue("xml:/Order/OrderLine/@ItemID")%>"/>				
		</td>
    </tr>
    
	
    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="ScreenType" value="detail"/>
        <jsp:param name="RefreshOnDocumentType" value="true"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
		<jsp:param name="ShowEnterpriseCode" value="false"/>
    </jsp:include>
	
    <% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code) %>
    <yfc:callAPI apiID="AP1"/>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP4"/>

    <tr>
		<% String enterpriseCodeFromCommon = getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode");%>		

        <td class="detaillabel" ><yfc:i18n>Customer_NO</yfc:i18n></td>
        <td>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@CustomerPONo")%>/>
        </td> 
		<td class="detaillabel" ><yfc:i18n>Order_Type</yfc:i18n></td>
        <td>
            <select class="combobox" <%=getComboOptions("xml:/Order/@OrderType")%>>
                <yfc:loopOptions binding="xml:OrderTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" isLocalized="Y"/>
            </select>
        </td>
        <td class="detaillabel" ><yfc:i18n>Order_Date</yfc:i18n></td>
        <td nowrap="true">
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/@OrderDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/@OrderDate_YFCTIME")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
        </td>		

    </tr>
    <tr>

        <td class="detaillabel" ><yfc:i18n>Order_#</yfc:i18n></td>
        <td>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderNo")%>/>
        </td>  
		<td class="detaillabel" ><yfc:i18n>Currency</yfc:i18n></td>
        <td>
            <select class="combobox" <%=getComboOptions("xml:/Order/PriceInfo/@Currency")%>>
                <yfc:loopOptions binding="xml:/CurrencyList/@Currency" name="CurrencyDescription"
                value="Currency" selected="xml:/Order/PriceInfo/@Currency" isLocalized="Y"/>
            </select>
        </td>		
    </tr>
    <tr>
        <td class="detaillabel"><yfc:i18n>Customer</yfc:i18n></td>
        <td>
            <input type="text" id="Customer" class="unprotectedinput" <%=getTextOptions("xml:/Order/@BuyerOrganizationCode")%> />	
			<img class="lookupicon" name="search" 
			onclick="showCustomerLookupPopup('xml:/Order/@BuyerOrganizationCode', 'TDorglookup')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_#") %> />
        </td>										
        		
    </tr>
	</table>
	<br />
	<table class="view" width="100%">
	<tr>
		<td class="detaillabel" ><yfc:i18n>Transfer_Order</yfc:i18n></td>
		<td></td>
		<td class="detaillabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
		<td class="tablecolumn" nowrap="true" >
				<input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Order/OrderLine/Item/@ItemID")%>/>
				<img class="lookupicon" name="search" onclick="callItemLookup('xml:/Order/OrderLine/Item/@ItemID','xml:/Order/OrderLine/Item/@ProductClass','xml:/Order/OrderLine/Item/@UnitOfMeasure',
				'TERitem','xml:/Item/@CallingOrganizationCode=<%=enterpriseCode%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
			</td>
		<td class="detaillabel" ><yfc:i18n>Qty</yfc:i18n></td>
			<td nowrap="true" >
				<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/OrderLine/@OrderedQty")%>/>
			</td>
			<td class="detaillabel" ><yfc:i18n>Product_Class</yfc:i18n></td>
			<td nowrap="true" class="tablecolumn">
				<select <%=getComboOptions("xml:/Order/OrderLine/Item/@ProductClass")%> class="combobox">
					<yfc:loopOptions binding="xml:WorkOrderProductClassList:/CommonCodeList/@CommonCode" name="CodeValue"
					value="CodeValue" selected="xml:/Order/OrderLine/Item/@ProductClass"/>
				</select>
			</td>
			<td class="detaillabel" ><yfc:i18n>Ship_From</yfc:i18n></td>
			<td nowrap="true" >
				<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@ShipNode")%>/>
				<img class="lookupicon" onclick="callLookupForOrder(this,'BUYER','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
			</td>
			<td class="detaillabel" ><yfc:i18n>Ship_To</yfc:i18n></td>
			<td nowrap="true" >
				<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@ReceivingNode")%>/>
				<img class="lookupicon" onclick="callLookupForOrder(this,'BUYER','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
			</td>
	</tr>
	
</table>

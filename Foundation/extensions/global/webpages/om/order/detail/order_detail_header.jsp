<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/yfsjspcommon/editable_util_header.jspf" %>
 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/orgName1.js"></script>

<script language="javascript">

	function validateMandatoryFields(){
		
		var attnTo = document.getElementById("attnTo").value;
		var phone = document.getElementById("phone").value;
		var email = document.getElementById("email").value;
		if((trim(attnTo) == "") || (trim(phone) == "") || (trim(email) == "")){
			alert("Attn To,Phone & Email are mandatory fields. ");
			return false;
		}else{
			return enterModificationReason('YMRD001','xml:/Order/@ModificationReasonCode','xml:/Order/@ModificationReasonText','xml:/Order/@Override');
		}
	}
	
	function callLookupforCS(entity, customerSite){
		var obj = new Object();
		obj.field1 = document.all(customerSite);
		obj.lookup = true;
		yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
		(document.all(customerSite)).focusout=callAjaxFunctionOrgChange(customerSite,'NODE');
	}
	
	function validateOrderType(orderType){
		var inputType = "<%=resolveValue("xml:/Order/@OrderType")%>";
		
		if(trim(orderType) == ""){
			if(trim(inputType).toLowerCase() === "Offline".toLowerCase()){
				alert('For Offline orders, please Choose Offline Shipment button.');
				return false;
			}
		}else{  /*Offline*/
			if(trim(inputType).toLowerCase() != trim(orderType).toLowerCase()){
				if(confirm('This button only for offline Orders. Order type will change to Offline') == true){
					return true;
				}else{
				return false;
			}
			}
		}
		return true;
	}
	
	function disableOutOfBoxFailure(){
	/*	
		var e = document.getElementById("TDOrderType");
		if(e.disabled == false){
			var strOrderType = e.options[e.selectedIndex].value;
			if(strOrderType == 'DOA'){
				alert('hi');
			}else{
				alert('hihi');
			}
		}*/
	}
	
	//window.attachEvent("onload", disableOutOfBoxFailure);
</Script>
<%  
	YFCElement orderEle = getElement("Order");
	YFCElement orderLinesEle = orderEle.getChildElement("OrderLines");
	boolean isChildPresent = orderLinesEle.hasChildNodes();

	String sRequestDOM = request.getParameter("getRequestDOM");
    String modifyView = request.getParameter("ModifyView");
    modifyView = modifyView == null ? "" : modifyView;

	String sHiddenDraftOrderFlag = getValue("Order", "xml:/Order/@DraftOrderFlag");
    String driverDate = getValue("Order", "xml:/Order/@DriverDate");
	String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("Order", "xml:/Order/@EnterpriseCode"));
	extraParams += "&" + getExtraParamsForTargetBinding("xml:/Order/@OrderHeaderKey", resolveValue("xml:/Order/@OrderHeaderKey"));
	extraParams += "&" + getExtraParamsForTargetBinding("IsStandaloneService", "Y");
	extraParams += "&" + getExtraParamsForTargetBinding("hiddenDraftOrderFlag", sHiddenDraftOrderFlag);
%>

<script language="javascript">
	// this method is used by 'Add Service Request' action on order header detail innerpanel
	function callPSItemLookup()	{
		yfcShowSearchPopupWithParams('','itemlookup',900,550,new Object(), 'psItemLookup', '<%=extraParams%>');
	}
</script>
	
<%
	String uploadPath = (String) session.getAttribute("FilePath");
	if(isVoid(uploadPath)){
		YFCDocument inputDoc = YFCDocument.getDocumentFor("<GetProperty PropertyName=\"pdf.dump.folder\" />");
		YFCDocument template = YFCDocument.getDocumentFor("<GetProperty PropertyValue=\"\" />");
%>
		<yfc:callAPI apiName='getProperty' inputElement='<%=inputDoc.getDocumentElement()%>' templateElement='<%=template.getDocumentElement()%>' outputNamespace='FolderPath'/>
<%
		YFCElement outputEle = getElement("FolderPath");
		
		if(isVoid(outputEle)){
			session.setAttribute("FilePath","");
		}else{
			session.setAttribute("FilePath",outputEle.getAttribute("PropertyValue"));
		}
	}
%>
	
<table class="view" width="100%">
	
	<%
		String strEnteredBy = resolveValue("xml:/Order/@EnteredBy");
		String strEmail = "";
		String strPhone = "";
		if(!isVoid(strEnteredBy)){
			YFCDocument userInputDoc = YFCDocument.getDocumentFor("<User Loginid=\""+strEnteredBy+"\" />");
			YFCDocument userTemplateDoc = YFCDocument.getDocumentFor("<UserList><User Loginid=\"\" Username=\"\"><ContactPersonInfo DayPhone=\"\" EMailID=\"\" /></User></UserList>");

	%>
		<yfc:callAPI apiName='getUserList' inputElement='<%=userInputDoc.getDocumentElement()%>'
			templateElement='<%=userTemplateDoc.getDocumentElement()%>' outputNamespace='User'/>
	<%
			YFCElement userListEle = getElement("User");
			YFCElement userEle = userListEle.getChildElement("User");
			
			if(!isVoid(userEle)){
				strEnteredBy = userEle.getAttribute("Username");
				YFCElement personInfo = userEle.getChildElement("ContactPersonInfo");
				if(!isVoid(personInfo)){
					strEmail = personInfo.getAttribute("EMailID");
					strPhone = personInfo.getAttribute("DayPhone");
				}
			}
		}
	%>
    <yfc:makeXMLInput name="orderKey">
        <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
    </yfc:makeXMLInput>
    <tr>
        <td>
            <input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
            <input type="hidden" name="xml:/Order/@ModificationReasonCode" />
            <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
            <input type="hidden" name="xml:/Order/@Override" value="N"/>
            <input type="hidden" name="hiddenDraftOrderFlag" value='<%=sHiddenDraftOrderFlag%>'/>
            <input type="hidden" name="chkWOEntityKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>'/>
            <input type="hidden" name="chkCopyOrderEntityKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>' />
			<input type="hidden" name="chkOrderHeaderKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>' />
			<input type="hidden" name="xml:/Order/@EnterpriseCode" value='<%=getValue("Order", "xml:/Order/@EnterpriseCode")%>'/>
			<input type="hidden" name="xml:/Order/@DocumentType" value='<%=getValue("Order", "xml:/Order/@DocumentType")%>'/>
			<input type="hidden" name="xml:/Order/@OrderNo" value='<%=getValue("Order", "xml:/Order/@OrderNo")%>'/>
        </td>
    </tr>
    <tr>
        <td class="detaillabel" ><yfc:i18n>TD_SO_Order#</yfc:i18n></td>
        <td class="protectedtext"><yfc:getXMLValue binding="xml:/Order/@OrderNo"/></td>
        <td class="detaillabel" ><yfc:i18n>TD_SO_Order_Type</yfc:i18n></td>
		<td class="">
			<select OldValue="" class="combobox" onchange="disableOutOfBoxFailure();" id="TDOrderType" <%=yfsGetComboOptions("xml:/Order/@OrderType","xml:/Order/AllowedModifications")%>>
				<yfc:loopOptions binding="xml:OrderTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" isLocalized="Y" selected="xml:/Order/@OrderType"/>
			</select>
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SO_Ship_To</yfc:i18n></td>
		<td class="protectedtext">
			<%if(!isChildPresent){%>
				<input type="text" id="customerSiteLookup" <%=getTextOptions("xml:/Order/@ReceivingNode")%> onblur= "callAjaxFunctionOrgChange('customerSiteLookup','NODE');"/><img class="lookupicon" name="search" onclick="callLookupforCS('TDCustSiteOrg','customerSiteLookup');" <%=yfsGetImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_Site", "xml:/Order/@ReceivingNode", "xml:/Order/AllowedModifications")%>/>
			<%}else{%>
				<yfc:getXMLValue binding="xml:/Order/@ReceivingNode" />
			<%}%>
		</td>
		<td class="detaillabel" align="left"><yfc:i18n>Customer_Site_Org_Name</yfc:i18n></td>
		<td  nowrap="true">
			<input size="50" type="text" id="customerSiteLookupName" class="protectedinput"  <%=getTextOptions("xml:/Order/Extn/@CustOrgName")%> readonly />
		</td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SO_Status</yfc:i18n></td>
        <td class="protectedtext">
            <% if (isVoid(getValue("Order", "xml:/Order/@Status"))) {%>
                [<yfc:i18n>Draft</yfc:i18n>]
            <% } else { %>
                <a <%=getDetailHrefOptions("L01", getParameter("orderKey"), "ShowReleaseNo=Y")%>><%=displayOrderStatus(getValue("Order","xml:/Order/@MultipleStatusesExist"),getValue("Order","xml:/Order/@MaxOrderStatusDesc"),true)%></a>
            <% } %>
            <% if (equals("Y", getValue("Order", "xml:/Order/@HoldFlag"))) { %>

	            <% if (isVoid(modifyView) || isTrue("xml:/Rules/@RuleSetValue")) {%>
					<img onmouseover="this.style.cursor='default'" class="columnicon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_is_held")%>>
				<%	}	else	{	%>
					<a <%=getDetailHrefOptions("L05", getParameter("orderKey"), "")%>><img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_is_held\nclick_to_add/remove_hold")%>></a>
				<%	}	%>

            <% } %>
            <% if (equals("Y", getValue("Order", "xml:/Order/@SaleVoided"))) { %>
                <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.SALE_VOIDED, "This_sale_is_voided")%>/>
            <% } %>
            <% if (equals("Y", getValue("Order","xml:/Order/@isHistory") )){ %>
                <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.HISTORY_ORDER, "This_is_an_archived_order")%>/>
            <% } %>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_SO_Entered_by</yfc:i18n></td>
		<td class="protectedtext"><%=strEnteredBy%></td>
		<td class="detaillabel"><yfc:i18n>TD_SO_Email</yfc:i18n></td>
		<td class="protectedtext"><%=strEmail%></td>
		<td class="detaillabel"><yfc:i18n>TD_SO_Phone</yfc:i18n></td>
		<td class="protectedtext"><%=strPhone%></td>
    </tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_SO_Created_Date</yfc:i18n></td>
		<td class="protectedtext">
			<yfc:getXMLValue binding="xml:/Order/@OrderDate"/>
			<input type="hidden" <%=getTextOptions("xml:/Order/@OrderDate")%> />
			<input type="hidden" <%=getTextOptions("xml:/Order/@ReceivingNode")%> />
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SO_Cost_Center</yfc:i18n></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:/Order/Extn/@CostCenter"/></td>
		<td class="detaillabel"><yfc:i18n>TD_SO_Attn_To</yfc:i18n><span style="color:red">*</span></td>
		<td class="protectedtext"><input id="attnTo" type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@ContactAttnTo")%>/></td>
		<td class="detaillabel" ><yfc:i18n>Currency</yfc:i18n></td>
		<td>
			<input type="text" id="tdCurrency" class="protectedinput" <%=getTextOptions("xml:/Order/PriceInfo/@Currency")%> readOnly />
        </td>
	</tr>
    <tr>
		<td class="detaillabel"><yfc:i18n>TD_SO_Customer_Reference</yfc:i18n></td>
		<td> <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@CustomerPONo")%> /></td>
        <td class="detaillabel" ><yfc:i18n>TD_SO_Contact</yfc:i18n></td>
        <td><input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@ContactName")%> /></td>
		<td class="detaillabel" ><yfc:i18n>TD_SO_Phone</yfc:i18n><span style="color:red">*</span></td>
        <td><input id="phone" type="text" class="unprotectedinput"  <%=getTextOptions("xml:/Order/Extn/@ContactPhone")%> /></td>
		<td class="detaillabel" ><yfc:i18n>TD_SO_Email</yfc:i18n><span style="color:red">*</span></td>
        <td><input id="email" type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@ContactEMailID")%> /></td>
    </tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_Special_Customer_Instructions</yfc:i18n></td>
		<td><textarea class="unprotectedtextareainput" rows="4" cols="75" <%=getTextOptions("xml:/Order/Extn/@SplCustomerInstructions")%>><yfc:getXMLValue binding="xml:/Order/Extn/@SplCustomerInstructions"/></textarea></td>
	</tr>
</table>

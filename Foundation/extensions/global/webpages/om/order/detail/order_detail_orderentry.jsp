<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/orgName1.js"></script>

<script language="Javascript" >
	IgnoreChangeNames();
	yfcDoNotPromptForChanges(true);
		
	function validateCustomerSite(){
		
		var customer = document.getElementById("customerSiteLookup").value;
		if(trim(customer) == ""){
			alert("Customer-Site can't be empty");
			return false;
		}else{
			return true;
		}
	}
	
	function callLookupforCS(entity, customerSite){
	
		var obj = new Object();
		obj.field1 = document.all(customerSite);
		obj.lookup = true;
		obj.viewDetails = 'N';
		obj.createCustomerOrg = 'N';
		obj.createCustomerSiteOrg = 'N';
		obj.viewAudits = 'N';
		obj.addChildOrg = 'N';
		yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
		(document.all(customerSite)).focusout=callAjaxFunctionOrgChange(customerSite,'NODE');
	}
	
	function displayForTO(){
		var docTypeEle = document.all("xml:/Order/@DocumentType");
		if("0006" != docTypeEle.options[docTypeEle.selectedIndex].value){
			var e = document.getElementById("TO");
			e.style.display = "none";
		}
	}
	
	function modifySeller(){
		var enterprise = document.all("xml:/Order/@EnterpriseCode");
		var docTypeEle = document.all("xml:/Order/@DocumentType");
		var draftFlag = document.all("xml:/Order/@DraftOrderFlag");
		
			var e = document.all("xml:/Order/@SellerOrganizationCode");		
			e.value = enterprise.options[enterprise.selectedIndex].value; 
		if("0006" == docTypeEle.options[docTypeEle.selectedIndex].value){
			//var e = document.all("xml:/Order/@SellerOrganizationCode");		
			draftFlag.value = "N";
			//e.value = enterprise.options[enterprise.selectedIndex].value; 
		}else{
			draftFlag.value = "Y";
		}
	}	
	
	window.attachEvent('onload', removeExtendedDocumentTypeForSO);
	window.attachEvent('onload', displayForTO);
	window.attachEvent('onload', modifySeller);
	
</script>

<%
    // Default the enterprise code if it is not passed
    String enterpriseCode = (String) request.getParameter("xml:/Order/@EnterpriseCode");
	String strDocTypeEle = resolveValue("xml:/Order/@DocumentType");
    if (isVoid(enterpriseCode)) {
        enterpriseCode = getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@PrimaryEnterpriseKey");
        request.setAttribute("xml:/Order/@EnterpriseCode", enterpriseCode);
    }

	// Default the seller to logged in organization if it plays a role of seller
    String sellerOrgCode = (String) request.getParameter("xml:/Order/@SellerOrganizationCode");
    if (isVoid(sellerOrgCode)) {
        if(isRoleDefaultingRequired((YFCElement) request.getAttribute("CurrentOrgRoleList"))){
            sellerOrgCode = getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@OrganizationCode");
            //System.out.println("org:" + orgCode);
            //request.setAttribute("xml:/Order/@SellerOrganizationCode", orgCode);
        }
    }
    
    //prepareMasterDataElements(enterpriseCode, (YFCElement) request.getAttribute("OrganizationList"),
    //                        (YFCElement) request.getAttribute("EnterpriseParticipationList"),
    //                        (YFCElement) request.getAttribute("CurrencyList"),
    //                        (YFCElement) request.getAttribute("OrderTypeList"),getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@IsHubOrganization"));

	String exchangeOrderForReturn = resolveValue("xml:/ReturnOrder/@ReturnOrderHeaderKeyForExchange");
	String orderHeaderKeyVal = resolveValue("xml:/Order/@OrderHeaderKey");	
%>

<script language="javascript">
    <% if (!equals(exchangeOrderForReturn, "Y")) {

        if (!isVoid(orderHeaderKeyVal)) {	 
            YFCDocument orderDoc = YFCDocument.createDocument("Order");
            orderDoc.getDocumentElement().setAttribute("OrderHeaderKey",resolveValue("xml:/Order/@OrderHeaderKey"));

            // If this screen is shown as a popup, then open the order detail view for the new order
            // as a popup as well (instead of refreshing the same screen).
            if (equals(request.getParameter(YFCUIBackendConsts.YFC_IN_POPUP), "Y")) {
            %>
                function showOrderDetailPopup() {
					window.CloseOnRefresh = "Y";
		            callPopupWithEntity('order', '<%=orderDoc.getDocumentElement().getString(false)%>');
					window.close();
				}
                window.attachEvent("onload", showOrderDetailPopup);
            <%
            } else {
            %>
                function showOrderDetail() {
				    showDetailFor('<%=orderDoc.getDocumentElement().getString(false)%>');
				}
                window.attachEvent("onload", showOrderDetail);
            <% }
        }
    }
	%>
</script>
<%
	//exchange order processing
	boolean isExchangeOrderCreation = false;
	if(!isVoid(exchangeOrderForReturn)){
		isExchangeOrderCreation = true;
		//call getOrderDetails api for defaulting information onto exchange order.
%>
	    <yfc:callAPI apiID="AP5"/>
<%	} %>


<table class="view" width="100%">
    <tr>
        <td>
            <input type="hidden" name="xml:/Order/@DraftOrderFlag" value="Y"/>
            <input type="hidden" name="xml:/Order/@EnteredBy" value="<%=resolveValue("xml:CurrentUser:/User/@Loginid")%>"/>
			<% if(isExchangeOrderCreation){ %>
				<input type="hidden" name="xml:/Order/@ReturnOrderHeaderKeyForExchange" value='<%=exchangeOrderForReturn%>'/>
				<input type="hidden" name="xml:/Order/@OrderPurpose" value='EXCHANGE'/>
				<input type="hidden" name="xml:/Order/@DocumentType" value='0001'/>
				<input type="hidden" name="xml:/Order/@ShipToKey" value="<%=resolveValue("xml:/Order/@ShipToKey")%>"/>
				<input type="hidden" name="xml:/Order/@BillToKey" value="<%=resolveValue("xml:/Order/@BillToKey")%>"/>
			<% } %>
		</td>
    </tr>

	<% if(isExchangeOrderCreation){	%>
    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="ScreenType" value="detail"/>
        <jsp:param name="HardCodeDocumentType" value="0001"/>
        <jsp:param name="ApplicationCode" value="omd"/>
        <jsp:param name="DisableEnterprise" value="Y"/>
    </jsp:include>
	<% } else {%>
    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="ScreenType" value="detail"/>
        <jsp:param name="RefreshOnDocumentType" value="true"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
    </jsp:include>
	<% } %>
    <% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code) %>
    <yfc:callAPI apiID="AP1"/>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP4"/>
	<yfc:callAPI apiID="AP6"/>

    <tr>
		<td class="detaillabel"><yfc:i18n>TD_SO_Customer_Reference</yfc:i18n></td>
		<td>	
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@CustomerPONo")%> />
		</td>
		<td class="detaillabel"><yfc:i18n>Order_Type</yfc:i18n></td>
		<td>
			<select class="combobox" <%=getComboOptions("xml:/Order/@OrderType")%>>
				<yfc:loopOptions binding="xml:OrderTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" isLocalized="Y" selected="NORMAL"/>
			</select>
		</td>
		<td class="detaillabel" ><yfc:i18n>Order_Date</yfc:i18n></td>
        <td nowrap="true">
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/@OrderDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/@OrderDate_YFCTIME")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
        </td>
	</tr>	
    <tr>
        <td class="detaillabel" ><yfc:i18n>Order_#</yfc:i18n></td>
        <td>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderNo")%>/>
        </td>
		<td class="detaillabel" ><yfc:i18n>Currency</yfc:i18n></td>
        <td>
			<input type="text" id="tdCurrency" class="protectedinput" name="xml:/Order/PriceInfo/@Currency" value="" readOnly />
        </td>
		<% if(isExchangeOrderCreation){ %>
        <td class="detaillabel" >
            <yfc:i18n>Exchange_Type</yfc:i18n>
        </td>
        <td>
            <select name="xml:/Order/@ExchangeType" class="combobox">
                <yfc:loopOptions binding="xml:ExchangeTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/Order/@ExchangeType" isLocalized="Y"/>
            </select>
        </td>
		<% } %>
    </tr>
	<!-- <input type="hidden" name="xml:/Order/@OrderEntry" value="Manual"/> -->
    <tr>
		<%if("0006".equalsIgnoreCase(strDocTypeEle)){%>
			<td class="detaillabel"><yfc:i18n>Ship To</yfc:i18n></td>
		<%}else{%>
			<td class="detaillabel"><yfc:i18n>TD_SO_Customer_Site</yfc:i18n></td>
		<%}%>
		<td>
			<input type="text" id="customerSiteLookup" <%=getTextOptions("xml:/Order/@ReceivingNode")%> onblur= "callAjaxFunctionOrgChange('customerSiteLookup','NODE')"/><img class="lookupicon" name="search" onclick="callLookupforCS('TDCustSiteOrg','customerSiteLookup');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer")%> />
		</td>
		<td class="detaillabel" align="left"><yfc:i18n>Customer_Site_Org_Name</yfc:i18n></td>
		<td  nowrap="true">
			<input size="50" type="text" id="customerSiteLookupName" class="protectedinput"  <%=getTextOptions("xml:/Order/Extn/@CustOrgName")%> readonly />
		</td>
    </tr>
</table>
<table id="TO" class="view" width="100%">
	<tr>
		<input type="hidden" name="xml:/Order/@SellerOrganizationCode" value="CSO" />  
		<!-- input type="hidden" name="xml:/Order/@BuyerOrganizationCode" value="CSO" / -->  
		<td class="detaillabel">Transfer Order</td>
		<td class="detaillabel">Part</td>
		<td class="tablecolumn" nowrap="true">
			<input type="text" name="xml:/Order/OrderLines/OrderLine/Item/@ItemID"/>
			<img class="lookupicon" onclick="templateRowCallItemLookup(this,'ItemID','ProductClass','TransactionalUOM','item',' ')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item")%> />
		</td>
		<td class="detaillabel" >Qty</td>
		<td><input type="text" name="xml:/Order/OrderLines/OrderLine/OrderLineTranQuantity/@OrderedQty" /></td>
		<td class="detaillabel">Product Class</td>
		<td>
			<select name="xml:/Order/OrderLines/OrderLine/Item/@ProductClass">
				<yfc:loopOptions binding="xml:ProductClassList:/CommonCodeList/@CommonCode" name="CodeValue" 
				value="CodeValue" />
			</select>
		</td>
		<td class="detaillabel">Unit Of Measure</td>
		<td>
			<select name="xml:/Order/OrderLines/OrderLine/OrderLineTranQuantity/@TransactionalUOM">
			   <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
				value="UnitOfMeasure" />
			</select>
		</td>
		<td class="detaillabel">Ship From</td>
		<td nowrap="">
			<input type="text" <%=getTextOptions("xml:/Order/OrderLines/OrderLine/@ShipNode", "")%>/>
			<img class="lookupicon" onclick="callLookup(this,'shipnode')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Ship_Node")%>/>
		</td>
		<!--<td class="detaillabel">Ship To</td>
		<td><input type="text" name="xml:/OrderLines/OrderLine/@ReceivingNode"/></td>-->
	</tr>
</table>

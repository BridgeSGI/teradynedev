<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<table class="table" editable="false" width="100%" cellspacing="0">
    <thead>
	<tr>
            <td sortable="no" class="checkboxheader">
                <input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
                <input type="hidden" name="xml:/TDOfficeRecords/@ModificationReasonCode" />
                <input type="hidden" name="xml:/TDOfficeRecords/@ModificationReasonText"/>
                <!-- <input type="hidden" name="xml:/TDOfficeRecords/@Override" value="N"/> -->
                <input type="hidden" name="ResetDetailPageDocumentType" value="Y"/>	<%-- cr 35413 --%>
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader"><yfc:i18n>Office_Code</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Office_Name</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Office_Type</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Status</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Country</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Price_Zone</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>GSO_Revenue_Region</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Worldwide</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service_Region</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service_Location</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service_Office</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Clarify_Work_Group</yfc:i18n></td>
    </tr>
	</thead>
	
	<tbody>
	<yfc:loopXML binding="xml:/TDOfficeRecordsList/@TDOfficeRecords" id="TDOfficeRecords">
	<tr>
			<yfc:makeXMLInput name="OfficeKey">
                    <yfc:makeXMLKey binding="xml:/TDOfficeRecords/@OfficeKey" value="xml:/TDOfficeRecords/@OfficeKey" />
            </yfc:makeXMLInput>
			<td class="checkboxcolumn">                     
                    <input type="checkbox" value='<%=getParameter("OfficeKey")%>' name="EntityKey"/>
            </td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@OfficeCode"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@OfficeName"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@OfficeType"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@OfficeStatus"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@OfficeCountry"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@PriceZone"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@GSORevenueRegion"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@WorldwideOffice"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@ServiceRegion"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@ServiceLocation"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@ServiceOffice"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDOfficeRecords/@ClarifyWorkgroup"/></td>
	</tr>		
	</yfc:loopXML>
	</tbody>
</table>
	
	
	
	
	
	
	
	
	
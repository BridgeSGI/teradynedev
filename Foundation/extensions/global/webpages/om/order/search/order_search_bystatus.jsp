<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>

<script language="javascript">

function callLookupforserial(entity,field1){
	var obj = new Object();
	obj.field1 = document.all(field1);
	obj.lookup = true;
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
}
	
function setOrderStateFlags(value1, value2){
	var oOrderComplete = document.all("xml:/Order/@OrderComplete");
	if (oOrderComplete != null)
		oOrderComplete.value = value1;
	var oReadFromHistory = document.all("xml:/Order/@ReadFromHistory");
	if (oReadFromHistory != null)
		oReadFromHistory.value = value2;
}

function processExchangeType(checkboxObj) {
    var exchangeType = document.all("xml:/Order/@ExchangeType");
	if(!checkboxObj.checked){
		exchangeType.value = " ";
		exchangeType.disabled = true;
        exchangeType.selectedIndex = -1;
	}else{
		exchangeType.disabled = false;
	}
}

function showUserLookupPopup(usernameInput, entityname)
	{
		var oObj = new Object();
		oObj.field1 = document.all(usernameInput);
		yfcShowSearchPopup('','userlookup',900,550,oObj,entityname);
		
	}

function setByAddressSearchCriteria(){
    
    var radAddress = document.all("RadAddress");
    var billToKey = document.all("xml:/Order/SearchByAddress/@BillToKey");
    var shipToKey = document.all("xml:/Order/SearchByAddress/@ShipToKey");
    var personInfoKey = document.all("xml:/PersonInfo/@PersonInfoKey");
	var addressValue = "B";
	if(radAddress[1].checked)
		addressValue = "S";
	if(radAddress[2].checked)
		addressValue = "E";

	billToKey.value = "";
	shipToKey.value = "";
	if(personInfoKey.value != null){
		if (addressValue == "B" || addressValue == "E") {
			billToKey.value = personInfoKey.value;
		}
		if (addressValue == "S" || addressValue == "E") {
			shipToKey.value = personInfoKey.value;
		}
	}
}
function refreshSearchScreen(controlObj) {
    if (yfcHasControlChanged(controlObj)) {
        changeSearchView(getCurrentSearchViewId());
	}
}

function callLookupforCS(entity, customerSite){
	var obj = new Object();
	obj.field1 = document.all(customerSite);
	obj.lookup = true;
	obj.viewDetails = 'N';
	obj.createCustomerOrg = 'N';
	obj.createCustomerSiteOrg = 'N';
	obj.viewAudits = 'N';
	obj.addChildOrg = 'N';
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
}

function callLookupforSC(entity, agreementNo){
	var obj = new Object();
	obj.field1 = document.all(agreementNo);
	obj.lookup = true;
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
}

window.attachEvent('onload', removeExtendedDocumentTypeForSO);
</script>
<%
	// Values for radiobinding
	// open : OrderComplete="N" ReadFromHistory="N" radiobinding="open"
	// recent : OrderComplete=" " ReadFromHistory="N" radiobinding="recent"
	// history : OrderComplete=" " ReadFromHistory="Y" radiobinding="history"
	// all : OrderComplete=" " ReadFromHistory="B" radiobinding="all"

	// default is open : OrderComplete="N" ReadFromHistory="N" radiobinding="open"

	String bReadFromHistory = resolveValue("xml:/Order/@ReadFromHistory");
	String sOrderComplete = resolveValue("xml:/Order/@OrderComplete");

	String radiobinding = "open";
	if(isVoid(sOrderComplete) && equals(bReadFromHistory, "N")){
		radiobinding = "recent";
	}
	else if(isVoid(sOrderComplete) && equals(bReadFromHistory, "Y")){
		radiobinding = "history";
	}
	else if(isVoid(sOrderComplete) && equals(bReadFromHistory, "B")){
		radiobinding = "all";
	}
	else{
		radiobinding = "open";
		YFCElement orderElem = (YFCElement)request.getAttribute("Order");
		if(orderElem == null){
			orderElem = YFCDocument.createDocument("Order").getDocumentElement();
		}
		orderElem.setAttribute("OrderComplete", "N");
		orderElem.setAttribute("ReadFromHistory", "N");
		bReadFromHistory = "N";
		sOrderComplete = "N";

	}
	
	String isExchange = " ";
	String orderPurpose = resolveValue("xml:/Order/@OrderPurpose");
    if (equals(orderPurpose, "EXCHANGE")) {
        isExchange = "Y";
    }

    preparePaymentStatusList(getValue("Order", "xml:/Order/@PaymentStatus"), (YFCElement) request.getAttribute("PaymentStatusList"));
	
	//processing for the address lookup and address keys
	
	String billToKey = resolveValue("xml:/Order/SearchByAddress/@BillToKey");
	String shipToKey = resolveValue("xml:/Order/SearchByAddress/@ShipToKey");
	String radAddress = "";

	String personInfoKey ="";
	if(!isVoid(billToKey) && !isVoid(shipToKey)){
		radAddress = "E";
		personInfoKey = billToKey; 	
	}
	else if(!isVoid(billToKey)){
		radAddress = "B";
		personInfoKey = billToKey; 	
	}
	else if(!isVoid(shipToKey)){
		radAddress = "S";
		personInfoKey = shipToKey; 	
	}

	//set the address radiobuttons
	if(isVoid(radAddress)){
		radAddress = "E";
	}
	
	YFCElement personInfoElement = (YFCElement)request.getAttribute("PersonInfo");
	if(personInfoElement == null){
		personInfoElement = YFCDocument.createDocument("PersonInfo").getDocumentElement();
		request.setAttribute("PersonInfo",personInfoElement);
	}
	personInfoElement.setAttribute("PersonInfoKey", personInfoKey);
	
	//setup date variable for searching between dates
	YFCDate oEndDate = new YFCDate(); 
	oEndDate.setEndOfDay();

%>

<table class="view">
    <tr>
        <td>
            <input type="hidden" name="xml:/Order/@StatusQryType" value="BETWEEN"/>
            <input type="hidden" name="xml:/Order/@DraftOrderFlag" value="N"/>
            <input type="hidden" <%=getTextOptions("xml:/PersonInfo/@PersonInfoKey", personInfoKey)%>/>
			<input type="hidden" name="xml:/Order/OrderHoldType/@Status" value=""/>
			<input type="hidden" name="xml:/Order/OrderHoldType/@StatusQryType" value="" />
			<input type="hidden" name="xml:/Order/SearchByAddress/@BillToKey" value="<%=billToKey%>"/>
            <input type="hidden" name="xml:/Order/SearchByAddress/@ShipToKey" value="<%=shipToKey%>"/>
        </td>
    </tr>

    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="RefreshOnDocumentType" value="true"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
    </jsp:include>
    <% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code) %>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP4"/>
    <yfc:callAPI apiID="AP5"/>
	<yfc:callAPI apiID="AP9"/>
	 <% 
		if(!isVoid(personInfoKey)){
	%>
			<yfc:callAPI apiID="AP8"/>
    <%
		}
    %>
    <%
		if(!isTrue("xml:/Rules/@RuleSetValue") )	{
    %>
			<yfc:callAPI apiID="AP7"/>
    
	<%
			YFCElement listElement = (YFCElement)request.getAttribute("HoldTypeList");

			YFCDocument document = listElement.getOwnerDocument();
			YFCElement newElement = document.createElement("HoldType");

			newElement.setAttribute("HoldType", " ");
			newElement.setAttribute("HoldTypeDescription", getI18N("All_Held_Orders"));

			YFCElement eFirst = listElement.getFirstChildElement();
			if(eFirst != null)	{
				listElement.insertBefore(newElement, eFirst);
			}	else	{
				listElement.appendChild(newElement);
			}
			request.setAttribute("defaultHoldType", newElement);
		}

        //Remove Statuses 'Draft Order Created' and 'Held' from the Status Search Combobox.
		prepareOrderSearchByStatusElement((YFCElement) request.getAttribute("StatusList"));
    %>

    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@OrderNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@OrderNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderNo")%>/>
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Customer_Site</yfc:i18n>
        </td>
    </tr>
	<% String enterpriseCode = getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode");
		//Grab the current document type being searched for
		String sDocType = getValue("Order","xml:/Order/@DocumentType");
		//if no xml:/Order/DocumentType is found, then set the default to sales order (0001)
		if(sDocType.length() < 2)
		{
			sDocType = "0001";
		}
	%>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@ReceivingNodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@ReceivingNodeQryType"/>
            </select>
            <input type="text" id="customerSiteLookup" class="unprotectedinput" <%=getTextOptions("xml:/Order/@ReceivingNode")%>/><img class="lookupicon" name="search" onclick="callLookupforCS('TDCustSiteOrg','customerSiteLookup');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_Site")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchcriteriacell">
            <input type="checkbox" <%=getCheckBoxOptions("xml:/Order/@OrderPurpose", "EXCHANGE", "xml:/Order/@OrderPurpose")%> onClick="processExchangeType(this)" yfcCheckedValue='EXCHANGE' yfcUnCheckedValue=' '> <yfc:i18n>Exchange_Order_With_Type</yfc:i18n></input>&nbsp;
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell">
            <select name="xml:/Order/@ExchangeType" class="combobox"  <% if(!equals(isExchange, "Y")){ %> disabled="true" <% } %> >
                <yfc:loopOptions binding="xml:ExchangeTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/Order/@ExchangeType" isLocalized="Y"/>
            </select>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SO_Customer_Reference</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@CustomerPONoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@CustomerPONoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@CustomerPONo")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell" nowrap="true" >
            <select name="xml:/Order/OrderLine/Item/@ItemIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/Item/@ItemIDQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Order/OrderLine/Item/@ItemID")%>/>
            <% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>
            <img class="lookupicon" name="search" 
			onclick="callItemLookup('xml:/Order/OrderLine/Item/@ItemID','xml:/Order/OrderLine/Item/@ProductClass','xml:/Order/OrderLine/@OrderingUOM',
			'TDSitem','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Serial_Number</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/OrderLine/Extn/@SystemSerialNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/Extn/@SystemSerialNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Extn/@SystemSerialNo")%>/>
			<img class="lookupicon" name="search" onclick="callLookupforserial('INBorder','xml:/OrderLine/Extn/@SystemSerialNo');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_serial")%> />
		</td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Agreement_Number</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/OrderLine/Extn/@AgreementNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/Extn/@AgreementNoQryType"/>
            </select>
			<input type="text" id="serviceContractLookup" class="unprotectedinput" <%=getTextOptions("xml:/Order/OrderLine/Extn/@AgreementNo")%>/><img class="lookupicon" name="search" onclick="callLookupforSC('SC','serviceContractLookup');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Agreement_No")%>/>
		</td>
    </tr>		
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Planner_Code</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/OrderLine/Extn/@PlannerCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/Extn/@PlannerCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/OrderLine/Extn/@PlannerCode")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Service_Type_Code</yfc:i18n>
        </td>
    </tr>
    <tr>
		<td nowrap="true" class="searchcriteriacell" >
			<select name="xml:/Order/OrderLine/Extn/@ServiceType" class="combobox" >
				<yfc:loopOptions binding="xml:ServcieTypes:/CommonCodeList/@CommonCode" 
				name="CodeValue" value="CodeValue" selected="xml:/Order/OrderLine/Extn/@ServiceType"/>
			</select>
		</td>
    </tr>			
    <tr>
        <td>
            <br/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_Line_Status</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <select name="xml:/Order/@FromStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
                value="Status" selected="xml:/Order/@FromStatus" isLocalized="Y"/>
            </select>
            <span class="searchlabel" ><yfc:i18n>To</yfc:i18n></span>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@ToStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
                value="Status" selected="xml:/Order/@ToStatus" isLocalized="Y"/>
            </select>
        </td>
    </tr>
	<tr>
        <td>
            <br/>
        </td>
    </tr>
	<tr>
		<td class="searchlabel">
			<fieldset>
				<legend class="searchlabel"><yfc:i18n>Address</yfc:i18n>
					<img class="lookupicon" onclick="callAddressLookup('xml:/PersonInfo/@PersonInfoKey','addresslookup','');setByAddressSearchCriteria();refreshSearchScreen(document.all('xml:/PersonInfo/@PersonInfoKey'))" <%=getImageOptions(YFSUIBackendConsts.ADDRESS_DETAILS, "Search_for_Address") %> />
				</legend>
			<span class="protectedtext">
			<%			
			if(!isVoid(personInfoKey)){
			%>
			<% //display the address details %>
				<jsp:include page="/yfsjspcommon/address.jsp" flush="true">
					<jsp:param name="Path" value="xml:/PersonInfoList/PersonInfo"/>
					<jsp:param name="DataXML" value="PersonInfoList"/>
		        </jsp:include>
			</span>
			<% }else{ %>
			<br/>
			<%}%>
			<br/>
            <input type="radio" onclick="setByAddressSearchCriteria()" <%=getRadioOptions("RadAddress", radAddress, "B")%>>
                <yfc:i18n>Bill_To</yfc:i18n>
            </input>
            <input type="radio" onclick="setByAddressSearchCriteria()" <%=getRadioOptions("RadAddress", radAddress, "S")%>>
                <yfc:i18n>Ship_To</yfc:i18n>
            </input>
            <input type="radio" onclick="setByAddressSearchCriteria()" <%=getRadioOptions("RadAddress", radAddress, "E")%>>
                <yfc:i18n>Either</yfc:i18n>
            </input>
			</fieldset>
		</td>
	</tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_Date</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Order/@FromOrderDate_YFCDATE","xml:/Order/@FromOrderDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
            <input class="dateinput" type="hidden" <%=getTextOptions("xml:/Order/@FromOrderDate_YFCTIME","xml:/Order/@FromOrderDate_YFCTIME","")%>/>
			<yfc:i18n>To</yfc:i18n>
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Order/@ToOrderDate_YFCDATE","xml:/Order/@ToOrderDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="hidden" <%=getTextOptions("xml:/Order/@ToOrderDate_YFCTIME","xml:/Order/@ToOrderDate_YFCTIME",oEndDate.getString(getLocale().getTimeFormat()))%>/>
        </td>
    </tr>
    

	<%	if(isTrue("xml:/Rules/@RuleSetValue") )	{	%>
		<tr>
			<td class="searchcriteriacell">
				<input type="checkbox" onclick="manageHoldOpts(this)" <%=getCheckBoxOptions("xml:/Order/@HoldFlag", "xml:/Order/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' ><yfc:i18n>Held_Orders</yfc:i18n></input>
			</td>
		</tr>    
		<tr>
			<td class="searchlabel" >
				<yfc:i18n>Hold_Reason_Code</yfc:i18n>
			</td>
		</tr>
		<tr>
			<td class="searchcriteriacell">
				<select name="xml:/Order/@HoldReasonCode" class="combobox">
					<yfc:loopOptions binding="xml:HoldReasonCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue" selected="xml:/Order/@HoldReasonCode" isLocalized="Y"/>
				</select>
			</td>
		</tr>
	<%	}	else	{	%>
		<tr>
			<td class="searchcriteriacell">
				<input type="checkbox" <%=getCheckBoxOptions("xml:/Order/@HoldFlag", "xml:/Order/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' onclick="manageHoldOpts(this)" ><yfc:i18n>Held_Orders_With_Hold_Type</yfc:i18n></input>
			</td>
		</tr>
		<tr>
	        <td class="searchcriteriacell">
				<select resetName="<%=getI18N("All_Held_Orders")%>" onchange="resetObjName(this, 'xml:/Order/OrderHoldType/@HoldType')" name="xml:/Order/OrderHoldType/@HoldType" class="combobox" <%if(isTrue("xml:/Order/@HoldFlag") ) {%> ENABLED <%} else {%> disabled="true" <%}%> >
					<yfc:loopOptions binding="xml:/HoldTypeList/@HoldType" name="HoldTypeDescription" value="HoldType" suppressBlank="Y" selected="xml:/Order/OrderHoldType/@HoldType" isLocalized="Y"/>
				</select>

			</td>
		</tr>
	<%	}	%>

	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_State</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell">
			<input type="radio" onclick="setOrderStateFlags('N','N')" <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "open")%>><yfc:i18n>Open</yfc:i18n> 
			<input type="radio" onclick="setOrderStateFlags(' ','N')"  <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "recent")%>><yfc:i18n>Recent</yfc:i18n><!-- The use of 'NO' is done intentionally, getOrderList API returns history orders only if ReadFromHistory =='Y'  -->
			<input type="radio" onclick="setOrderStateFlags(' ','Y')" <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "history")%>><yfc:i18n>History</yfc:i18n>
            <input type="radio" onclick="setOrderStateFlags(' ','B')" <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "all")%>><yfc:i18n>All</yfc:i18n>
            <input type="hidden" name="xml:/Order/@OrderComplete" value="<%=sOrderComplete%>"/>
            <input type="hidden" name="xml:/Order/@ReadFromHistory" value="<%=bReadFromHistory%>"/>
        </td>
    </tr>
	
    <tr>
        <td class="searchlabel">
            <yfc:i18n>Selecting_All_may_be_slow</yfc:i18n>
        </td>
    </tr>
</table>
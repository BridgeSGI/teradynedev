<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>

<%
	String strItemId = request.getParameter("text");
	String strOrgCode = request.getParameter("orgCode");
	String strUom = request.getParameter("uom");
	
	//Prepare input statement for getItemDetails API
	String strInput =  "<Item ItemID=\"" + strItemId + "\" OrganizationCode=\"" + strOrgCode
            + "\" UnitOfMeasure=\"" + strUom + "\">" + "<ComplexQuery Operator=\"AND\">" + "<Or>"
            + "<Exp Name=\"ItemType\" Value=\"SPAREPART\"/>"
            + "<Exp Name=\"ItemType\" Value=\"COMPONENT\"/>"
            + "<Exp Name=\"ItemType\" Value=\"FRU\"/>" 
			+ "</Or></ComplexQuery></Item>";

	YFCDocument yfcInputDoc = YFCDocument.getDocumentFor(strInput);
	
	String strTemplate = "<Item ItemID=\"\"><PrimaryInformation ShortDescription=\"\" /><Extn SupportStatusCode=\"\" /></Item>";
	YFCDocument yfcTemplateDoc = YFCDocument.getDocumentFor(strTemplate);
%>
	<yfc:callAPI apiName="getItemDetails" inputElement='<%=yfcInputDoc.getDocumentElement()%>' templateElement='<%=yfcTemplateDoc.getDocumentElement()%>' outputNamespace="TDItem"/>
<%
	YFCElement itemEle = (YFCElement)request.getAttribute("TDItem");
	String itemDesc = "";
	YFCDocument temp = YFCDocument.getDocumentFor("<CommonCodeList />");
	if(itemEle == null){
		YFCElement item = temp.getDocumentElement();
		item.setAttribute("Error","InvalidItem");
		out.println(item.getString(false));
	}else{
		YFCElement infoEle = itemEle.getChildElement("PrimaryInformation");	
		itemDesc = infoEle.getAttribute("ShortDescription");
	}
	
	YFCElement extnEle = itemEle.getChildElement("Extn");
	String supportStatusCode = extnEle.getAttribute("SupportStatusCode");
	
	
	if(!isVoid(supportStatusCode)){
		//Prepare inputEle for getCommonCodeList
		String inputDoc = "<CommonCode CodeType=\""+supportStatusCode+"\" OrganizationCode=\""+strOrgCode+"\" />";
		String template = "<CommonCodeList><CommonCode CodeValue=\"\" /></CommonCodeList>";
		
		YFCDocument yfcInput = YFCDocument.getDocumentFor(inputDoc);
		YFCDocument yfcTemplate = YFCDocument.getDocumentFor(template);
		
%>
	<yfc:callAPI apiName="getCommonCodeList" inputElement='<%=yfcInput.getDocumentElement()%>' templateElement='<%=yfcTemplate.getDocumentElement()%>' outputNamespace="ServiceType"/>
<%
	YFCElement yfcServiceType = (YFCElement)request.getAttribute("ServiceType");
		
	if(yfcServiceType == null){
		yfcServiceType = temp.getDocumentElement();
		yfcServiceType.setAttribute("ItemDesc",itemDesc);
	}else{
		yfcServiceType.setAttribute("ItemDesc",itemDesc);
	}
	out.println(yfcServiceType.getString(false));
}%>
<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.core.*" %>
<%@ page import="com.sterlingcommerce.tools.datavalidator.*" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf" %>
<%@ include file="/extn/console/jsp/LoadServiceContractinSOLine.jspf" %>
<%
	String strSerialno = request.getParameter("text");
	YFCDocument orderLineInput = YFCDocument.getDocumentFor("<OrderLine StatusQryType=\"BETWEEN\"  ToStatus=\"1100.30\"><Order DocumentType=\"0017.ex\"/><Extn SystemSerialNo=\""+strSerialno+"\" /> </OrderLine>");
	YFCDocument orderLinetemplate = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=\"\"><OrderLine OrderLineKey=\"\" /></OrderLineList>");
%>
	<yfc:callAPI apiName="getOrderLineList" inputElement='<%=orderLineInput.getDocumentElement()%>' templateElement='<%=orderLinetemplate.getDocumentElement()%>' outputNamespace="IBorderLine"/>
<%
	YFCElement iborderLineEle = (YFCElement)request.getAttribute("IBorderLine");
	String itemDesc = "";
	YFCDocument temp = YFCDocument.getDocumentFor("<CommonCodeList />");
	if(iborderLineEle == null || !iborderLineEle.hasChildNodes()){
		YFCElement item = temp.getDocumentElement();
		item.setAttribute("Error","InvalidSerialNo");
		out.println(item.getString(false));
	}
	
	YFCElement orderLineEle = iborderLineEle.getFirstChildElement();
	String strOrderLineKey = orderLineEle.getAttribute("OrderLineKey");
	
	if(!isVoid(strOrderLineKey)){
		//Prepare inputEle for getCommonCodeList
		String inputDoc = "<TerSCIBMapping TerIBOLKey=\""+strOrderLineKey+"\" ><YFSSCOrderLine FromStatus=\"1100.200\" StatusQryType=\"BETWEEN\" ToStatus=\"1100.400\"/></TerSCIBMapping>";
				
		YFCDocument yfcInput = YFCDocument.getDocumentFor(inputDoc);
%>
	<yfc:callAPI serviceName="GetIBForLine" inputElement='<%=yfcInput.getDocumentElement()%>'  outputNamespace="SCIBLines"/>
<%
	YFCElement yfcSCIBLines = (YFCElement)request.getAttribute("SCIBLines");
	YFCElement SCIBLines = getElement("SCIBLines");
	if(!XmlUtils.isVoid(SCIBLines) && SCIBLines.hasChildNodes()){ 
		YFCIterable<YFCElement> TerSCIBMappingList = SCIBLines.getChildren("TerSCIBMapping");	
		
		long minPriNo = -1;
		String minPriOrderNo = "";
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		for (YFCElement TerSCIBMapping : TerSCIBMappingList) {
			YFCElement YFSSCOrderLine = TerSCIBMapping.getChildElement("YFSSCOrderLine");
			String maxLineDesc = YFSSCOrderLine.getAttribute("MaxLineStatusDesc");
			if(!XmlUtils.isVoid(YFSSCOrderLine) && ( ("ACTIVE".equalsIgnoreCase(maxLineDesc)) || ("RENEWAL PENDING".equalsIgnoreCase(maxLineDesc)) )){
				YFCElement orderEle = YFSSCOrderLine.getChildElement("Order");
				if(!XmlUtils.isVoid(orderEle)){
					String orderNo = orderEle.getAttribute("OrderNo");
					String strPriorityNo = orderEle.getAttribute("PriorityNumber");
					if(!XmlUtils.isVoid(orderNo)) {
						if(!XmlUtils.isVoid(strPriorityNo)){
							long priorityNo = Integer.parseInt(strPriorityNo);
							if(minPriNo == -1 || minPriNo > priorityNo){
								minPriNo = priorityNo;
								minPriOrderNo = orderNo;
							}
						}
						map.put(orderNo,orderNo);
					}
				}
			}
		}
		yfcSCIBLines = prepareAgreemetNoXML(map,minPriOrderNo);
	}else{
		yfcSCIBLines = temp.getDocumentElement();
	}
	out.println(yfcSCIBLines.getString(false));
}%>
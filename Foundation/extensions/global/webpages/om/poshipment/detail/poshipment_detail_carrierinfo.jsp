<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<%!  String sLoadNo = "";
     String sIsBreakBulkLoad = ""; %>
<%String sAppCode = resolveValue("xml:/CurrentEntity/@ApplicationCode");
	boolean outboundShipment = false;
	if(equals(sAppCode,"omd")){
		outboundShipment = true;
	}
%>

<%if("1".equals(resolveValue("xml:/Shipment/LoadShipments/@TotalNumberOfRecords"))) {
    sLoadNo = resolveValue("xml:/Shipment/LoadShipments/LoadShipment/Load/@LoadNo"); 
	sIsBreakBulkLoad = resolveValue("xml:/Shipment/LoadShipments/LoadShipment/Load/@IsBreakBulkLoad");
	}else { %>	
		<yfc:loopXML binding="xml:/Shipment/LoadShipments/@LoadShipment" id="LoadShipment">
			<%if((equals(resolveValue("xml:/LoadShipment/Load/@DestinationNode"),
				  resolveValue("xml:/Shipment/@ReceivingNode")))||
				  (equals(resolveValue("xml:/LoadShipment/Load/@DestinationAddressKey"),
				  resolveValue("xml:/Shipment/@ToAddressKey"))) ) {
				  sLoadNo = resolveValue("xml:/Shipment/LoadShipments/LoadShipment/Load/@LoadNo"); }%>	
		</yfc:loopXML>
<%}%>	

<table width="100%" class="view">
	<tr>
		<td class="detaillabel" >
			<yfc:i18n>Delivery_Method</yfc:i18n>
		</td>
		<td  class="protectedtext">	<%=getComboText("xml:DeliveryMethodList:/CommonCodeList/@CommonCode","CodeShortDescription","CodeValue","xml:/Shipment/@DeliveryMethod",true)%>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" >
			<yfc:i18n>Carrier_Service</yfc:i18n>
		</td>
		<% if(YFCCommon.isVoid(sLoadNo)) { %>
			<td class="protectedtext">
				<%if(equals(resolveValue("xml:/Shipment/@HasOneManifestedContainer"),"Y")){ %> <yfc:getXMLValueI18NDB binding="xml:/Shipment/ScacAndService/@ScacAndServiceDesc"/>
				<%}else{%>
				<select   <%=yfsGetComboOptions("xml:/Shipment/ScacAndService/@ScacAndServiceKey", "xml:/Shipment/ScacAndService/@ScacAndServiceKey", "xml:/Shipment/AllowedModifications")%>>
					<yfc:loopOptions binding="xml:/ScacAndServiceList/@ScacAndService" name="ScacAndServiceDesc"
					 value="ScacAndServiceKey" selected="xml:/Shipment/ScacAndService/@ScacAndServiceKey" isLocalized="Y"/>
				</select>
				<%}%>
			</td>
		<%}else { %>
			<td  class="protectedtext">	
			<%
			if(!YFCCommon.isVoid(sIsBreakBulkLoad) && !equals(resolveValue("xml:/Shipment/@HasOneManifestedContainer"),"Y")){%>
			<select   <%=yfsGetComboOptions("xml:/Shipment/ScacAndService/@ScacAndServiceKey", "xml:/Shipment/ScacAndService/@ScacAndServiceKey", "xml:/Shipment/AllowedModifications")%>>
					<yfc:loopOptions binding="xml:/ScacAndServiceList/@ScacAndService" name="ScacAndServiceDesc"
					 value="ScacAndServiceKey" selected="xml:/Shipment/ScacAndService/@ScacAndServiceKey" isLocalized="Y"/>
				</select>
			<%}else{%>
				<yfc:getXMLValueI18NDB binding="xml:/Shipment/ScacAndService/@ScacAndServiceDesc"/>
			<%}%>
			</td>
		<%}%>
	</tr>
	<tr>
		<% sLoadNo=""; %>
		<td class="detaillabel" >
			<yfc:i18n>Trailer_#</yfc:i18n> 
		</td>
		<td nowrap="true">
			<input type="text" <%=yfsGetTextOptions("xml:/Shipment/@TrailerNo", "xml:/Shipment/AllowedModifications")%>/>
		</td>	
	</tr>
	<tr>
		<td class="detaillabel">
			<yfc:i18n>BOL_#</yfc:i18n> 
		</td>
		<td nowrap="true">
			<input type="text" <%=yfsGetTextOptions("xml:/Shipment/@BolNo", "xml:/Shipment/AllowedModifications")%>/>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" >
			<yfc:i18n>Routing_Source</yfc:i18n> 
		</td>
		<td  class="protectedtext">			
			<%
				String str="";
				String sRoutSrc=resolveValue("xml:/Shipment/@RoutingSource");
				if(equals(sRoutSrc,"01"))
				{
					str="Yantra_Assigned";
				}
				else if(equals(sRoutSrc,"02"))
				{
					str="Pre_Assigned";
				}
				else if(equals(sRoutSrc,"03"))
				{
					str="Assigned_Externally";
				}
				else if(equals(sRoutSrc,"04"))
				{
					str="User_Assigned";
				}
			%>
			<yfc:i18n><%=str%></yfc:i18n> 
		</td>
	</tr>

</table>
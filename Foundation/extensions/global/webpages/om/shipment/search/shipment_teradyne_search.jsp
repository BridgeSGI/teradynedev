<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/dm.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script>
window.attachEvent('onload', removeExtendedDocumentTypeShipment);
</script>
<table class="view">
 <tr>
        <td>
            <input type="hidden" name="xml:/Shipment/@StatusQryType" value="BETWEEN"/>
          <!--   <input type="hidden" name="xml:/Shipment/@DraftOrderFlag" value="N"/> -->

					<input type="hidden" name="xml:/Shipment/ShipmentHoldTypes/ShipmentHoldType/@Status" value=""/>
			<input type="hidden" name="xml:/Shipment/ShipmentHoldTypes/ShipmentHoldType/@StatusQryType" value="" />
        </td>
    </tr>
<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
  <jsp:param name="ShowDocumentType" value="true"/>
  <jsp:param name="RefreshOnDocumentType" value="true"/>
  <jsp:param name="DocumentTypeBinding" value="xml:/Shipment/@DocumentType"/>
  <jsp:param name="EnterpriseCodeBinding" value="xml:/Shipment/@EnterpriseCode"/>
  <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
  <jsp:param name="ScreenType" value="search"/>
 </jsp:include>

<% // Now call the APIs that are dependent on the common fields (Doc Type , Enterprise Code , & Carrier Service) %>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP3"/>
    <yfc:callAPI apiID="AP6"/>

   
    <%
		String docType = resolveValue("xml:CommonFields:/CommonFields/@DocumentType");
		
		if(!isTrue("xml:/Rules/@RuleSetValue") )	{
    %>
			<yfc:callAPI apiID="AP4"/>
    
	<%
			YFCElement listElement = (YFCElement)request.getAttribute("HoldTypeList");

			YFCDocument document = listElement.getOwnerDocument();
			YFCElement newElement = document.createElement("HoldType");

			newElement.setAttribute("HoldType", " ");
			newElement.setAttribute("HoldTypeDescription", getI18N("All_Held_Shipments"));

			YFCElement eFirst = listElement.getFirstChildElement();
			if(eFirst != null)	{
				listElement.insertBefore(newElement, eFirst);
			}	else	{
				listElement.appendChild(newElement);
			}
			request.setAttribute("defaultHoldType", newElement);
		}

        //Remove Statuses 'Draft Order Created' and 'Held' from the Status Search Combobox.
		//prepareOrderSearchByStatusElement((YFCElement) request.getAttribute("StatusList"));
		
		//setup date variable for searching between dates
		YFCDate oEndDate = new YFCDate(); 
		oEndDate.setEndOfDay();
    %>


  <yfc:callAPI apiID="AP2"/>
  <tr> 
        <td>
            <input type="hidden" name="xml:/Shipment/@StatusQryType" value="BETWEEN"/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Control_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@OrderNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@OrderNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@OrderNo")%>/>
        </td>
    </tr>    
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Shipment_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ShipmentNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@ShipmentNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@ShipmentNo")%>/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Ship_From_Node</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ShipNodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@ShipNodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@ShipNode")%>/>
			<% String extraParams = getExtraParamsForTargetBinding("xml:/ShipNode/Organization/EnterpriseOrgList/OrgEnterprise/@EnterpriseOrganizationKey", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>
			<img class="lookupicon" onclick="callLookup(this,'nodelookup','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_ShipNode") %> />
        </td>
    </tr>

	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Ship_To_Node</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ReceivingNodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@ReceivingNodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@ReceivingNode")%>/>
			<% String sExtraParams = getExtraParamsForTargetBinding("xml:/ShipNode/Organization/EnterpriseOrgList/OrgEnterprise/@EnterpriseOrganizationKey", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>
			<img class="lookupicon" onclick="callLookup(this,'nodelookup','<%=sExtraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_ShipNode") %> />
        </td>
    </tr>

    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Status</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <select name="xml:/Shipment/@FromStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description" value="Status" selected="xml:/Shipment/@FromStatus" isLocalized="Y"/>
            </select>
            <span class="searchlabel" ><yfc:i18n>To</yfc:i18n></span>
        </td>
    </tr>		
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ToStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description" value="Status" selected="xml:/Shipment/@ToStatus" isLocalized="Y"/>
            </select>
        </td>
    </tr>

	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Customer_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
			<select name="xml:/Shipment/@BuyerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@BuyerOrganizationCodeQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Shipment/@BuyerOrganizationCode")%>/>
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Service_Type</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
			<select name="xml:/Shipment/@ServiceTypeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@ServiceTypeQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/Extn/@ServiceType")%>/>

        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Part_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
			<select name="xml:/Shipment/ShipmentLines/ShipmentLine/@ItemIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/ShipmentLines/ShipmentLine/@ItemIDQryType"/>
            </select>
            <% String extraParams2 = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>

            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/@ItemID")%>/>
			<img class="lookupicon" name="search" 
			onclick="callItemLookup('xml:/Shipment/ShipmentLines/ShipmentLine/@ItemID',' ',' ',
			'item','<%=extraParams2%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Serial_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
			<select name="xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/@SerialNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/@SerialNoQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/@SerialNo")%>/>
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Vendor_Serial_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
			<select name="xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/Extn/@OEMSerialNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/Extn/@OEMSerialNoQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/Extn/@OEMSerialNo")%>/>
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Expected_Ship_Date</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@FromExpectedShipmentDate_YFCDATE","xml:/Shipment/@FromExpectedShipmentDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
            <input class="dateinput" type="hidden" <%=getTextOptions("xml:/Shipment/@FromExpectedShipmentDate_YFCDATE","xml:/Shipment/@FromExpectedShipmentDate_YFCDATE","")%>/>
			<yfc:i18n>To</yfc:i18n>
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@ToExpectedShipmentDate_YFCDATE","xml:/Shipment/@ToExpectedShipmentDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="hidden" <%=getTextOptions("xml:/Shipment/@ToExpectedShipmentDate_YFCDATE","xml:/Shipment/@ToExpectedShipmentDate_YFCDATE",oEndDate.getString(getLocale().getTimeFormat()))%>/>
        </td>
    </tr>
    
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Carrier_Service</yfc:i18n>
        </td>
    </tr>
    <tr>
		<td nowrap="true" class="searchcriteriacell">
			<select name="xml:/Shipment/ScacAndService/@ScacAndServiceKey" class="combobox">
				<yfc:loopOptions binding="xml:ScacAndServiceList:/ScacAndServiceList/@ScacAndService" name="ScacAndServiceDesc" value="ScacAndServiceKey" selected="xml:/Shipment/ScacAndService/@ScacAndServiceKey" isLocalized="Y"/>
        </td>
    </tr>

    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Trailer_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@TrailerNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                    value="QueryType" selected="xml:/Shipment/@TrailerNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@TrailerNo")%>/>
        </td>
    </tr>


</table>
<input type="hidden" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/@WaveNo")%>/>
<input type="hidden" name="xml:/Shipment/@OrderAvailableOnSystem" value=" "/>
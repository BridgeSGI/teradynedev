<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/dm.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script>
window.attachEvent('onload', removeExtendedDocumentTypeShipment);
</script>
<table class="view">
 <tr>
        <td>
            <input type="hidden" name="xml:/Shipment/@StatusQryType" value="BETWEEN"/>
          <!--   <input type="hidden" name="xml:/Shipment/@DraftOrderFlag" value="N"/> -->

			<input type="hidden" name="xml:/Shipment/ShipmentHoldTypes/ShipmentHoldType/@Status" value=""/>
			<input type="hidden" name="xml:/Shipment/@ActualShipmentDateQryType" value="BETWEEN"/>
			
			<input type="hidden" name="xml:/Shipment/ShipmentHoldTypes/ShipmentHoldType/@StatusQryType" value="" />
        </td>
    </tr>
	<%String apptype=resolveValue("xml:/CurrentEntity/@ApplicationCode");
if(apptype.equals("omd")){
	
	%>
  <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="RefreshOnDocumentType" value="true"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
        <jsp:param name="DocumentTypeBinding" value="xml:/Shipment/@DocumentType"/>
		  <jsp:param name="EnterpriseCodeBinding" value="xml:/Shipment/@EnterpriseCode"/>
			<jsp:param name="ShowDocumentType" value="true"/>
		 <jsp:param name="ScreenType" value="search"/>
		</jsp:include>
		
<%
		
	}

else if(apptype.equals("oms")){ 
		
	%>
<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
 <jsp:param name="RefreshOnDocumentType" value="true"/>
  <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
   <jsp:param name="DocumentTypeBinding" value="xml:/Shipment/@DocumentType"/>
  <jsp:param name="EnterpriseCodeBinding" value="xml:/Shipment/@EnterpriseCode"/>
 <jsp:param name="ShowDocumentType" value="true"/>
  <jsp:param name="ScreenType" value="search"/>
 </jsp:include>

<%}
else if(apptype.equals("omr")){


%>
<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
  <jsp:param name="RefreshOnDocumentType" value="true"/>
   <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
   <jsp:param name="ShowDocumentType" value="false"/>
  <jsp:param name="DocumentTypeBinding" value="xml:/Shipment/@DocumentType"/>
  <jsp:param name="EnterpriseCodeBinding" value="xml:/Shipment/@EnterpriseCode"/>
  <jsp:param name="ScreenType" value="search"/>
 </jsp:include>
 <input type="hidden" name="xml:/Shipment/@DocumentType" value="0003" />
 <input type="hidden" name="xml:CommonFields:/CommonFields/@DocumentType" value="0003" />
 <%}
%>
  <yfc:callAPI apiID="AP6"/>

<% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code) %>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP3"/>
   
    <%
		String docType = resolveValue("xml:CommonFields:/CommonFields/@DocumentType");
		
		if(!isTrue("xml:/Rules/@RuleSetValue") )	{
    %>
			<yfc:callAPI apiID="AP4"/>
    
	<%
			YFCElement listElement = (YFCElement)request.getAttribute("HoldTypeList");

			YFCDocument document = listElement.getOwnerDocument();
			YFCElement newElement = document.createElement("HoldType");

			newElement.setAttribute("HoldType", " ");
			newElement.setAttribute("HoldTypeDescription", getI18N("All_Held_Shipments"));

			YFCElement eFirst = listElement.getFirstChildElement();
			if(eFirst != null)	{
				listElement.insertBefore(newElement, eFirst);
			}	else	{
				listElement.appendChild(newElement);
			}
			request.setAttribute("defaultHoldType", newElement);
		}

        //Remove Statuses 'Draft Order Created' and 'Held' from the Status Search Combobox.
		//prepareOrderSearchByStatusElement((YFCElement) request.getAttribute("StatusList"));
    %>


  <yfc:callAPI apiID="AP2"/>
  <tr> 
        <td>
            <input type="hidden" name="xml:/Shipment/@StatusQryType" value="BETWEEN"/>
        </td>
    </tr>    
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Shipment_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ShipmentNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@ShipmentNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@ShipmentNo")%>/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">            
			<select name="xml:/Shipment/ShipmentLines/ShipmentLine/@OrderNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/ShipmentLines/ShipmentLine/@OrderNoQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/@OrderNo")%>/>
        </td>
    </tr>
	 <tr>
        <td class="searchlabel" >
            <yfc:i18n>Control_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">            
			<select name="xml:/Shipment/ShipmentLines/ShipmentLine/Order/OrderLine/Extn/@ControlNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/ShipmentLines/ShipmentLine/Order/OrderLine/Extn/@ControlNoQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/Order/OrderLine/Extn/@ControlNo")%>/>
        </td>
    </tr>
	 <tr>
        <td class="searchlabel" >
            <yfc:i18n>Ship_From_Node</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ShipNodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@ShipNodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@ShipNode")%>/>
			 <% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>
			<img class="lookupicon" onclick="callLookup(this,'nodelookup','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_ShipNode") %> />
        </td>
    </tr>

	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Ship_To_Node</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ReceivingNodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@ReceivingNodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@ReceivingNode")%>/>
			<% String sExtraParams = getExtraParamsForTargetBinding("xml:/ShipNode/Organization/EnterpriseOrgList/OrgEnterprise/@EnterpriseOrganizationKey", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>
			<img class="lookupicon" onclick="callLookup(this,'nodelookup','<%=sExtraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_ShipNode") %> />
        </td>
    </tr>
  <tr>
        <td class="searchlabel" >
            <yfc:i18n>Status</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <select name="xml:/Shipment/@FromStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description" value="Status" selected="xml:/Shipment/@FromStatus" isLocalized="Y"/>
            </select>
            <span class="searchlabel" ><yfc:i18n>To</yfc:i18n></span>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ToStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description" value="Status" selected="xml:/Shipment/@ToStatus" isLocalized="Y"/>
            </select>
        </td>
    </tr>
	<tr>
	<%if (apptype.equals("omd")){%>
    <tr> 
        <td class="searchlabel" ><yfc:i18n>Customer_PO_#</yfc:i18n></td>
    </tr>    
    <tr>
         <td nowrap="true" class="searchcriteriacell">
            <select class="combobox" name="xml:/Shipment/ShipmentLines/ShipmentLine/@CustomerPoNoQryType">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc" value="QueryType" 
                        selected="xml:/Shipment/ShipmentLines/ShipmentLine/@CustomerPoNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/@CustomerPoNo")%> />
        </td>
    </tr>
	<tr> 
	<%}%>
        <td class="searchlabel" ><yfc:i18n>Customer_Name</yfc:i18n></td>
    </tr>    
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select class="combobox" name="xml:/Shipment/Extn/@TdynCustNameQryType">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc" value="QueryType" 
                        selected="xml:/Shipment/Extn/@TdynCustNameQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/Extn/@TdynCustName")%> />
        </td>
    </tr>
	<tr> 
        <td class="searchlabel" ><yfc:i18n>Customer_ID</yfc:i18n></td>
    </tr>    
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select class="combobox" name="xml:/Shipment/@BuyerOrganizationCodeQryType">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc" value="QueryType" 
                        selected="xml:/Shipment/@BuyerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@BuyerOrganizationCode")%> />
        </td>
    </tr>
	<tr> 
        <td class="searchlabel" ><yfc:i18n>Service_Type</yfc:i18n></td>
    </tr>    
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select class="combobox" name="xml:/Shipment/ShipmentLines/ShipmentLine/Extn/@ServiceTypeQryType">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc" value="QueryType" 
                        selected="xml:/Shipment/ShipmentLines/ShipmentLine/Extn/@ServiceTypeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/Extn/@ServiceType")%> />
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/ShipmentLines/ShipmentLine/@ItemIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                    value="QueryType" selected="xml:/Shipment/ShipmentLines/ShipmentLine/@ItemIDQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/@ItemID")%>/>
			
            <img class="lookupicon" onclick="callItemLookup('xml:/Shipment/ShipmentLines/ShipmentLine/@ItemID','xml:/Shipment/ShipmentLines/ShipmentLine/@ProductClass','xml:/Shipment/ShipmentLines/ShipmentLine/@UnitOfMeasure','item','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item")%>/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Serial_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/@SerialNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/@SerialNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/@SerialNo")%>/>
        </td>
    </tr>

	 <tr>
        <td class="searchlabel" >
            <yfc:i18n>Vendor_Serial_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/Extn/@OEMSerialNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/Extn/@OEMSerialNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/ShipmentTagSerials/ShipmentTagSerial/Extn/@OEMSerialNo")%>/>
        </td>
    </tr>
	<tr>
					<td class="searchlabel" >
						<yfc:i18n>Ship_Date</yfc:i18n>
					</td>
				</tr>
				<tr>
					<td nowrap="true">
						<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@FromActualShipmentDate_YFCDATE")%>/>
						<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> /><yfc:i18n>To</yfc:i18n>
						<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@ToActualShipmentDate_YFCDATE")%>/>
						<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
						
					</td>
				</tr>
				<%if (!apptype.equals("omr")){%>
				<tr>
					<td class="searchlabel" >
						<yfc:i18n>Due_Date</yfc:i18n>
					</td>
				</tr>
				<tr>
					<td nowrap="true">
						<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@FromRequestedShipmentDate_YFCDATE")%>/>
						<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> /><yfc:i18n>To</yfc:i18n>
						<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@ToRequestedShipmentDate_YFCDATE")%>/>
						<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
						
					</td>
				</tr>
				
  
        <td class="searchlabel" >
            <yfc:i18n>Carrier_</yfc:i18n>
        </td>
    </tr>
    <tr>
		<td nowrap="true" class="searchcriteriacell">
			<select name="xml:/Shipment/ScacAndService/@ScacAndServiceKey" class="combobox">
				<yfc:loopOptions binding="xml:ScacAndServiceList:/ScacAndServiceList/@ScacAndService" name="ScacAndServiceDesc" value="ScacAndServiceKey" selected="xml:/Shipment/ScacAndService/@ScacAndServiceKey" isLocalized="Y"/>
        </td>
    </tr>
	<%}%>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>AWB_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@BolNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@BolNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@BolNo")%>/>
        </td>
    </tr>
		<%	if(isTrue("xml:/Rules/@RuleSetValue") )	{	%>
		<tr>
			<td class="searchcriteriacell">
				<input type="checkbox" onclick="manageHoldOpts(this)" <%=getCheckBoxOptions("xml:/Shipment/@HoldFlag", "xml:/Shipment/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' ><yfc:i18n>Held_Shipments</yfc:i18n></input>
			</td>
		</tr>    
		<tr>
			<td class="searchlabel" >
				<yfc:i18n>Hold_Reason_Code</yfc:i18n>
			</td>
		</tr>
		<tr>
			<td class="searchcriteriacell">
				<select name="xml:/Shipment/@HoldReasonCode" class="combobox">
					<yfc:loopOptions binding="xml:HoldReasonCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue" selected="xml:/Shipment/@HoldReasonCode" isLocalized="Y"/>
				</select>
			</td>
		</tr>
	<%	}	else	{	%>
		<tr>
			<td class="searchcriteriacell">
				<input type="checkbox" <%=getCheckBoxOptions("xml:/Shipment/@HoldFlag", "xml:/Shipment/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' onclick="manageHoldOpts(this)" ><yfc:i18n>Held_Shipments_With_Hold_Type</yfc:i18n></input>
			</td>
		</tr>
		<tr>
	        <td class="searchcriteriacell">
						<select resetName="<%=getI18N("All_Held_Shipments")%>" onchange="resetObjName(this, 'xml:/Shipment/ShipmentHoldTypes/ShipmentHoldType/@HoldType')" name="xml:/Shipment/ShipmentHoldTypes/ShipmentHoldType/@HoldType" class="combobox" <%if(isTrue("xml:/Shipment/@HoldFlag") ) {%> ENABLED <%} else {%> disabled="true" <%}%> >
					<yfc:loopOptions binding="xml:/HoldTypeList/@HoldType" name="HoldTypeDescription" value="HoldType" suppressBlank="Y" selected="xml:/Shipment/ShipmentHoldTypes/ShipmentHoldType/@HoldType" isLocalized="Y"/>
				</select>

			</td>
		</tr>
	<%	}	%>
 

</table>
<input type="hidden" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/@WaveNo")%>/>
<input type="hidden" name="xml:/Shipment/@OrderAvailableOnSystem" value=" "/>
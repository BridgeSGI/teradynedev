<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>

<script language="javascript">

function callLookupforserial(entity,field1){
	var obj = new Object();
	obj.field1 = document.all(field1);
	obj.lookup = true;
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
}

function callLookupforCS(entity, customerSite){
	var obj = new Object();
	obj.field1 = document.all(customerSite);
	obj.lookup = true;
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
}

function callLookupforSC(entity, agreementNo){
	var obj = new Object();
	obj.field1 = document.all(agreementNo);
	obj.lookup = true;
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
}

window.attachEvent('onload', removeExtendedDocumentTypeForSOLine);

</script>
<%
	preparePaymentStatusList(getValue("OrderLine", "xml:/OrderLine/Order/@PaymentStatus"), (YFCElement) request.getAttribute("PaymentStatusList"));
%>

<table class="view">
    <tr>
        <td>
            <input type="hidden" name="xml:/OrderLine/@StatusQryType" value="BETWEEN"/>
            <input type="hidden" name="xml:/OrderLine/Order/@DraftOrderFlag" value="N"/>
			<input type="hidden" name='xml:/OrderLine/@ItemGroupCode' value='PROD'  />
			<input type="hidden" name="xml:/OrderLine/Order/OrderHoldType/@Status" value=""/>
			<input type="hidden" name="xml:/OrderLine/Order/OrderHoldType/@StatusQryType" value="" />
        </td>
    </tr>

    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="RefreshOnDocumentType" value="true"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
        <jsp:param name="DocumentTypeBinding" value="xml:/OrderLine/Order/@DocumentType"/>
        <jsp:param name="EnterpriseCodeBinding" value="xml:/OrderLine/Order/@EnterpriseCode"/>
    </jsp:include>
    <% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code) %>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP4"/>
	<yfc:callAPI apiID="AP9"/>
	<% 
		if(!isTrue("xml:/Rules/@RuleSetValue") )	{
	%>
		<yfc:callAPI apiID="AP6"/>

	<%
		YFCElement listElement = (YFCElement)request.getAttribute("HoldTypeList");

		YFCDocument document = listElement.getOwnerDocument();
		YFCElement newElement = document.createElement("HoldType");

		newElement.setAttribute("HoldType", " ");
		newElement.setAttribute("HoldTypeDescription", getI18N("All_Held_Orders"));

		YFCElement eFirst = listElement.getFirstChildElement();
		if(eFirst != null)	{
			listElement.insertBefore(newElement, eFirst);
		}	else	{
			listElement.appendChild(newElement);
		}
		request.setAttribute("defaultHoldType", newElement);
	}
        //Remove Statuses 'Draft Order Created' and 'Held' from the Status Search Combobox.
        prepareOrderSearchByStatusElement((YFCElement) request.getAttribute("StatusList"));
    %>

    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@OrderNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@OrderNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/@OrderNo")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SO_Ship_From</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/@ShipNodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/@ShipNodeQryType"/>
            </select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/@ShipNode")%>/>
			<img class="lookupicon" onclick="callLookup(this,'shipnode')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Ship_Node")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SO_Customer_Site</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@ReceivingNodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@ReceivingNodeQryType"/>
            </select>
            <input type="text" id="customerSiteLookup" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/@ReceivingNode")%>/><img class="lookupicon" name="search" onclick="callLookupforCS('TDCustSiteOrg','customerSiteLookup');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_Site")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SO_Customer_Reference</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/@CustomerPONoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/@CustomerPONoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/@CustomerPONo")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell" nowrap="true" >
            <select name="xml:/OrderLine/Item/@ItemIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Item/@ItemIDQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/OrderLine/Item/@ItemID")%>/>
            <% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>
            <img class="lookupicon" name="search" 
			onclick="callItemLookup('xml:/OrderLine/Item/@ItemID','xml:/OrderLine/Item/@ProductClass','xml:/OrderLine/@OrderingUOM',
			'TDSitem','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SO_OEM</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell" nowrap="true" >
            <select name="xml:/OrderLine/ItemDetails/PrimaryInformation/@ManufacturerItemQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/ItemDetails/PrimaryInformation/@ManufacturerItemQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/OrderLine/ItemDetails/PrimaryInformation/@ManufacturerItem")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Serial_Number</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Extn/@SystemSerialNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@SystemSerialNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Extn/@SystemSerialNo")%>/>
			<img class="lookupicon" name="search" onclick="callLookupforserial('INBorder','xml:/OrderLine/Extn/@SystemSerialNo');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_serial")%> />
		</td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Agreement_Number</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Extn/@AgreementNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@AgreementNoQryType"/>
            </select>
			<input type="text" id="serviceContractLookup" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Extn/@AgreementNo")%>/><img class="lookupicon" name="search" onclick="callLookupforSC('SC','serviceContractLookup');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Agreement_No")%>/>
		</td>
    </tr>		
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Planner_Code</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Extn/@PlannerCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@PlannerCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Extn/@PlannerCode")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Service_Type_Code</yfc:i18n>
        </td>
    </tr>
    <tr>
		<td nowrap="true" class="searchcriteriacell" >
			<select name="xml:/OrderLine/Extn/@ServiceType" class="combobox" >
				<yfc:loopOptions binding="xml:ServcieTypes:/CommonCodeList/@CommonCode" 
				name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@ServiceType"/>
			</select>
		</td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_Line_Status</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <select name="xml:/OrderLine/@FromStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
                value="Status" selected="xml:/OrderLine/@FromStatus" isLocalized="Y"/>
            </select>
            <span class="searchlabel" ><yfc:i18n>To</yfc:i18n></span>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/@ToStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
                value="Status" selected="xml:/OrderLine/@ToStatus" isLocalized="Y"/>
            </select>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Payment_Status</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell" nowrap="true">
            <select name="xml:/OrderLine/Order/@PaymentStatus" class="combobox">
                <yfc:loopOptions binding="xml:/PaymentStatusList/@PaymentStatus" name="DisplayDescription"
                value="CodeType" selected="xml:/OrderLine/Order/@PaymentStatus"/>
            </select>
        </td>
    </tr>

	<%	if(isTrue("xml:/Rules/@RuleSetValue") )	{	%>
    <tr>
        <td class="searchcriteriacell" nowrap="true">
            <input type="checkbox" <%=getCheckBoxOptions("xml:/OrderLine/Order/@HoldFlag", "xml:/OrderLine/Order/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' '><yfc:i18n>Held_Orders</yfc:i18n></input>
        </td>
    </tr>

    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Hold_Reason_Code</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell" nowrap="true">
            <select name="xml:/OrderLine/Order/@HoldReasonCode" class="combobox">
                <yfc:loopOptions binding="xml:HoldReasonCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/OrderLine/Order/@HoldReasonCode" isLocalized="Y"/>
            </select>
        </td>
    </tr>
	<%	}	else	{	%>
		<tr>
			<td class="searchcriteriacell">
				<input type="checkbox" <%=getCheckBoxOptions("xml:/OrderLine/Order/@HoldFlag", "xml:/OrderLine/Order/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' onclick="manageHoldOpts(this, 'xml:/OrderLine/Order/OrderHoldType/')" ><yfc:i18n>Held_Orders_With_Hold_Type</yfc:i18n></input>
			</td>
		</tr>
		<tr>
	        <td class="searchcriteriacell">
				<select resetName="<%=getI18N("All_Held_Orders")%>" onchange="resetObjName(this, 'xml:/OrderLine/Order/OrderHoldType/@HoldType')" name="xml:/OrderLine/Order/OrderHoldType/@HoldType" class="combobox" <%if(isTrue("xml:/OrderLine/Order/@HoldFlag") ) {%> ENABLED <%} else {%> disabled="true" <%}%> >
					<yfc:loopOptions binding="xml:/HoldTypeList/@HoldType" name="HoldTypeDescription" value="HoldType" suppressBlank="Y" selected="xml:/OrderLine/Order/OrderHoldType/@HoldType" isLocalized="Y"/>
				</select>

			</td>
		</tr>
	<%	}	%>
    
	<tr>
        <td class="searchcriteriacell" nowrap="true">
			<input type="checkbox" <%=getCheckBoxOptions("xml:/OrderLine/@FutureAvailabilityDateQryType", "xml:/OrderLine/@FutureAvailabilityDateQryType", "ISNULL")%> yfcUnCheckedValue='' yfcCheckedValue="ISNULL"><yfc:i18n>Only_Lines_With_No_Future_Availability_Date</yfc:i18n></input>
        </td>
    </tr>
</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<table class="table" editable="false" width="100%" cellspacing="0">
    <thead> 
        <tr>
            <td sortable="no" class="checkboxheader">
                <input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
                <input type="hidden" name="xml:/Order/@ModificationReasonCode" />
                <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
                <input type="hidden" name="xml:/Order/@Override" value="N"/>
                <input type="hidden" name="ResetDetailPageDocumentType" value="Y"/>	<%-- cr 35413 --%>
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader"><yfc:i18n>Contract_Template_ID</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Contract_Type</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Template_Name</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Contract_Priority</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Contract_Duration_Days</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Template Status</yfc:i18n></td>
			<!-- Could be deleted - suggested Andrea-->
            <td class="tablecolumnheader"><yfc:i18n>Install_Base_Required?</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML binding="xml:/OrderList/@Order" id="Order">
            <tr>
                <yfc:makeXMLInput name="orderKey">
                    <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey" />
                </yfc:makeXMLInput>                
                <td class="checkboxcolumn">                     
                    <input type="checkbox" value='<%=getParameter("orderKey")%>' name="EntityKey" isHistory='<%=getValue("Order","xml:/Order/@isHistory")%>' 	/>
                </td>
                <td class="tablecolumn">
                    <a href="javascript:showDetailFor('<%=getParameter("orderKey")%>');">
                        <yfc:getXMLValue binding="xml:/Order/@DocumentType"/>
                    </a>               
                </td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/@OrderType"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/@OrderName"/></td>
                <td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/@PriorityNumber"/></td> 
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/Extn/@DurationDays"/></td> 
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/Extn/@ContractStatus"/></td>
				<!-- Could be deleted - suggested Andrea-->
				<yfc:loopXML binding="xml:/OrderLineList/@OrderLine" id="OrderLine">
				<%
					String OrderNodeKey = resolveValue("xml:/Order/@OrderHeaderKey");
					String OrderHeaderNodeKey = resolveValue("xml:OrderLine:/OrderLine/@OrderHeaderKey");
						if(OrderNodeKey.equals(OrderHeaderNodeKey)){
				%>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@RequiredTemplate"/></td>
				<%
				}
				%>								
				</yfc:loopXML>
            </tr>
        </yfc:loopXML>
   </tbody>
</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table width="100%" class="view">

<tr>
    <td class="searchlabel" ><yfc:i18n>Service_Contract_Template_ID</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/Order/@DocumentTypeQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Order/@DocumentTypeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@DocumentType") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Service_Contract_Template_Name</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/Order/@OrderNameQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Order/@OrderNameQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderName") %> />
    </td>
</tr>

<tr>
    <td class="searchlabel" ><yfc:i18n>Service_Contract_Type</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/Order/@OrderTypeQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Order/@OrderTypeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderType") %> />
		<!-- Include Magnifying Glass Here! -->
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Service_Contract_Priority</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/Order/@PriorityNumberQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Order/@PriorityNumberQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@PriorityNumber") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Service_Contract_Duration_Days</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/Order/Extn/@DurationDaysQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Order/Extn/@DurationDaysQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@DurationDays") %> />
    </td>
</tr>

<!-- Here is a dropdown list-->
<tr>
    <td class="searchlabel" ><yfc:i18n>Service_Contract_Template_Status</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/Order/Extn/@ContractStatus" class="combobox">
            <yfc:loopOptions binding="xml:ServiceContractStatusList:/CommonCodeList/@CommonCode" 
                name="CodeValue" value="CodeValue" selected="xml:/Order/Extn/@ContractStatus"/>
        </select>
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Service_Item_ID</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/Order/OrderLine/Item/@ItemIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Order/OrderLine/Item/@ItemIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/OrderLine/Item/@ItemID") %> />
		<!-- Include Magnifying Glass Here! -->
    </td>
</tr>
</table>

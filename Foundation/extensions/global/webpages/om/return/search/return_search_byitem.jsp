<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>

<script language="javascript">
function callLookupforCS(entity, customerSite){
	var obj = new Object();
	obj.field1 = document.all(customerSite);
	obj.lookup = true;
	obj.viewDetails = 'N';
	obj.createCustomerOrg = 'N';
	obj.createCustomerSiteOrg = 'N';
	obj.viewAudits = 'N';
	obj.addChildOrg = 'N';
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
}
</script>

<%
	// Values for radiobinding
	// open : OrderComplete="N" ReadFromHistory="N" radiobinding="open"
	// recent : OrderComplete=" " ReadFromHistory="N" radiobinding="recent"
	// history : OrderComplete=" " ReadFromHistory="Y" radiobinding="history"
	// all : OrderComplete=" " ReadFromHistory="B" radiobinding="all"

	// default is open : OrderComplete="N" ReadFromHistory="N" radiobinding="open"

	String bReadFromHistory = resolveValue("xml:/Order/@ReadFromHistory");
	String sOrderComplete = resolveValue("xml:/Order/@OrderComplete");

	String radiobinding = "open";
	if(isVoid(sOrderComplete) && equals(bReadFromHistory, "N")){
		radiobinding = "recent";
	}
	else if(isVoid(sOrderComplete) && equals(bReadFromHistory, "Y")){
		radiobinding = "history";
	}
	else if(isVoid(sOrderComplete) && equals(bReadFromHistory, "B")){
		radiobinding = "all";
	}
	else{
		radiobinding = "open";
		YFCElement orderElem = (YFCElement)request.getAttribute("Order");
		if(orderElem == null){
			orderElem = YFCDocument.createDocument("Order").getDocumentElement();
		}
		orderElem.setAttribute("OrderComplete", "N");
		orderElem.setAttribute("ReadFromHistory", "N");
		bReadFromHistory = "N";
		sOrderComplete = "N";

	}
	
	String isExchange = " ";
	String orderPurpose = resolveValue("xml:/Order/@OrderPurpose");
    if (equals(orderPurpose, "EXCHANGE")) {
        isExchange = "Y";
    }

    preparePaymentStatusList(getValue("Order", "xml:/Order/@PaymentStatus"), (YFCElement) request.getAttribute("PaymentStatusList"));
	
	//processing for the address lookup and address keys
	
	String billToKey = resolveValue("xml:/Order/SearchByAddress/@BillToKey");
	String shipToKey = resolveValue("xml:/Order/SearchByAddress/@ShipToKey");
	String radAddress = "";

	String personInfoKey ="";
	if(!isVoid(billToKey) && !isVoid(shipToKey)){
		radAddress = "E";
		personInfoKey = billToKey; 	
	}
	else if(!isVoid(billToKey)){
		radAddress = "B";
		personInfoKey = billToKey; 	
	}
	else if(!isVoid(shipToKey)){
		radAddress = "S";
		personInfoKey = shipToKey; 	
	}

	//set the address radiobuttons
	if(isVoid(radAddress)){
		radAddress = "E";
	}
	
	YFCElement personInfoElement = (YFCElement)request.getAttribute("PersonInfo");
	if(personInfoElement == null){
		personInfoElement = YFCDocument.createDocument("PersonInfo").getDocumentElement();
		request.setAttribute("PersonInfo",personInfoElement);
	}
	personInfoElement.setAttribute("PersonInfoKey", personInfoKey);
	
	//setup date variable for searching between dates
	YFCDate oEndDate = new YFCDate(); 
	oEndDate.setEndOfDay();

%>

<table class="view">
    <tr>
        <td>
            <input type="hidden" name="xml:/Order/@DraftOrderFlag" value="N"/>
			<input type="hidden" name="xml:/Order/@StatusQryType" value="BETWEEN"/>
            <input type="hidden" name="xml:/Order/@OrderDateQryType" value="BETWEEN" OldValue=""/>
        </td>
    </tr>

    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="RefreshOnDocumentType" value="true"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
    </jsp:include>
    <% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code)
       // Unit of Measures, Product Classes, and Order Line Types are refreshed. %>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP3"/>
    <yfc:callAPI apiID="AP4"/>
	<yfc:callAPI apiID="AP5"/>
	<yfc:callAPI apiID="AP6"/>

    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@OrderNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@OrderNoQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Order/@OrderNo")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>PO_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@CustomerPONoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@CustomerPONoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@CustomerPONo")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_Line_Status</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <select name="xml:/Order/@FromStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
                value="Status" selected="xml:/Order/@FromStatus" isLocalized="Y"/>
            </select>
            <span class="searchlabel" ><yfc:i18n>To</yfc:i18n></span>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@ToStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
                value="Status" selected="xml:/Order/@ToStatus" isLocalized="Y"/>
            </select>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_Customer_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@ShipToIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@ShipToIDQryType"/>
            </select>
            <input type="text" id="customerSiteLookup" class="unprotectedinput" <%=getTextOptions("xml:/Order/@ShipToID")%>/>
			<img class="lookupicon" name="search" 
			onclick="callLookupforCS('TDCustSiteOrg','customerSiteLookup');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_#") %> />
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_Vendor_Part_Num</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/OrderLine/Item/@ItemIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/Item/@ItemIDQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/OrderLine/Item/@ItemID")%>/>
			<% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>
			<img class="lookupicon" name="search" 
			onclick="callItemLookup('xml:/Order/OrderLine/Item/@ItemID','xml:/Order/OrderLine/Item/@ProductClass','xml:/Order/OrderLine/@OrderingUOM',
			'TDSitem','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_Part_Serial_Num</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/OrderLine/@SerialNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/@SerialNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/OrderLine/@SerialNo")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_OEM_Serial_Num</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/OrderLine/Extn/@OEMSerialNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/Extn/@OEMSerialNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/OrderLine/Extn/@OEMSerialNo")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_Control_Num</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/OrderLine/Extn/@ControlNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/Extn/@ControlNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/OrderLine/Extn/@ControlNo")%>/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_Created_Date</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Order/@FromOrderDate_YFCDATE","xml:/Order/@FromOrderDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
            <input class="dateinput" type="hidden" <%=getTextOptions("xml:/Order/@FromOrderDate_YFCTIME","xml:/Order/@FromOrderDate_YFCTIME","")%>/>
			<yfc:i18n>To</yfc:i18n>
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Order/@ToOrderDate_YFCDATE","xml:/Order/@ToOrderDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="hidden" <%=getTextOptions("xml:/Order/@ToOrderDate_YFCTIME","xml:/Order/@ToOrderDate_YFCTIME",oEndDate.getString(getLocale().getTimeFormat()))%>/>
        </td>
    </tr>
	<%	if(isTrue("xml:/Rules/@RuleSetValue") )	{	%>
		<tr>
			<td class="searchcriteriacell">
				<input type="checkbox" onclick="manageHoldOpts(this)" <%=getCheckBoxOptions("xml:/Order/@HoldFlag", "xml:/Order/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' ><yfc:i18n>Held_Orders</yfc:i18n></input>
			</td>
		</tr>    
		<tr>
			<td class="searchlabel" >
				<yfc:i18n>Hold_Reason_Code</yfc:i18n>
			</td>
		</tr>
		<tr>
			<td class="searchcriteriacell">
				<select name="xml:/Order/@HoldReasonCode" class="combobox">
					<yfc:loopOptions binding="xml:HoldReasonCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue" selected="xml:/Order/@HoldReasonCode" isLocalized="Y"/>
				</select>
			</td>
		</tr>
	<%	}	else	{	%>
		<tr>
			<td class="searchcriteriacell">
				<input type="checkbox" <%=getCheckBoxOptions("xml:/Order/@HoldFlag", "xml:/Order/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' onclick="manageHoldOpts(this)" ><yfc:i18n>Held_Orders_With_Hold_Type</yfc:i18n></input>
			</td>
		</tr>
		<tr>
	        <td class="searchcriteriacell">
				<select resetName="<%=getI18N("All_Held_Orders")%>" onchange="resetObjName(this, 'xml:/Order/OrderHoldType/@HoldType')" name="xml:/Order/OrderHoldType/@HoldType" class="combobox" <%if(isTrue("xml:/Order/@HoldFlag") ) {%> ENABLED <%} else {%> disabled="true" <%}%> >
					<yfc:loopOptions binding="xml:/HoldTypeList/@HoldType" name="HoldTypeDescription" value="HoldType" suppressBlank="Y" selected="xml:/Order/OrderHoldType/@HoldType" isLocalized="Y"/>
				</select>

			</td>
		</tr>
	<%	}	%>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Order_State</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell">
			<input type="radio" onclick="setOrderStateFlags('N','N')" <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "open")%>><yfc:i18n>Open</yfc:i18n> 
			<input type="radio" onclick="setOrderStateFlags(' ','N')"  <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "recent")%>><yfc:i18n>Recent</yfc:i18n><!-- The use of 'NO' is done intentionally, getOrderList API returns history orders only if ReadFromHistory =='Y'  -->
			<input type="radio" onclick="setOrderStateFlags(' ','Y')" <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "history")%>><yfc:i18n>History</yfc:i18n>
            <input type="radio" onclick="setOrderStateFlags(' ','B')" <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "all")%>><yfc:i18n>All</yfc:i18n>
            <input type="hidden" name="xml:/Order/@OrderComplete" value="<%=sOrderComplete%>"/>
            <input type="hidden" name="xml:/Order/@ReadFromHistory" value="<%=bReadFromHistory%>"/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel">
            <yfc:i18n>Selecting_All_may_be_slow</yfc:i18n>
        </td>
    </tr>
</table>

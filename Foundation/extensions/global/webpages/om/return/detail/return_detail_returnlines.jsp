<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.core.*" %>
<%@ page import="com.yantra.shared.inv.*" %>
<%@ page import="com.sterlingcommerce.tools.datavalidator.*" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf" %>
<%@ include file="/extn/console/jsp/LoadServiceContractinSOLine.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/ajaxItemServiceType.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/ajaxWarranty.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/css/scripts/editabletbl.js"></script>

<script language="javascript">
    document.body.attachEvent("onunload", processSaveRecordsForChildNode);
	
	function enableDisableCode(repairCodeId,expediteCodeId,handleCodeId){
		var repair = document.getElementById(repairCodeId);
		var expedite = document.getElementById(expediteCodeId);
		var handle = document.getElementById(handleCodeId);
				
		var op = handle.options[handle.selectedIndex].text;
		if(op == 'Y'){
			repair.removeAttribute("disabled");
			expedite.removeAttribute("disabled");
		}else{
			repair.setAttribute("disabled","disabled");
			expedite.setAttribute("disabled","disabled");
		}
	}
	
	function callLookupforserial(elemId,entity,field1,field2,field3,recvNode){
		if(waranty){
			getwarranty(elemId,entity,field1,field2,field3,recvNode);
		}else{
		var obj = new Object();
		obj.field1 = document.all(field1);
		obj.field2 = document.all(field2);
		obj.field3 = document.all(field3);
		obj.recvNode = recvNode;
		obj.lookup = true;
		yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'xml:/OrderLine/Extn/@ItemType=FRU');
			waranty = true;
		}
	}
	
	function validateFile(id,hname,hVal,keyId, keyVal){	
		submitForm(document.getElementById(id).value,hname,hVal,keyId, keyVal);
		return true;
	}

	function submitForm(val,hName,hVal,keyId, keyVal){
		var hiddenElement = document.all(hName);
	    hiddenElement.value = hVal;
		var hiddenKeyElement = document.all(keyId);
	    hiddenKeyElement.value = keyVal;	
		
		var containerForm= document.all("containerform")
		window.document.forms.containerform.setAttribute("method","post");
		window.document.forms.containerform.setAttribute("encoding","multipart/form-data");
		window.document.forms.containerform.setAttribute("enctype","multipart/form-data");
		window.document.forms.containerform.action=contextPath+'/extn/pdf';
		window.document.forms.containerform.submit();
		
	}
	
	function Validate(sFileName) {
		var sExtension = ".pdf";
		if (sFileName.length > 0) {
            if (sFileName.substr(sFileName.length - sExtension.length, sExtension.length).toLowerCase() == sExtension.toLowerCase()) {
				return true;
			}else{	
				return false;	
			}
		}
	}
	
	function loadFile(){
		var btn = document.getElementsByClassName("button");
		var reqBtn = null;
		if(document.getElementsByClassName("button")[0].value == 'Save'){
			reqBtn = document.getElementsByClassName("button")[0];
		}else{
			reqBtn = document.getElementsByClassName("button")[1];
		}
		
		var url = document.URL;
		var orderLineKey = '<%=request.getAttribute("orderLineKey")%>';
		var quoteId = '<%=request.getAttribute("quoteOrderLineId")%>';
		var confirmId = '<%=request.getAttribute("confirmOrderLineId")%>';
		var fdlsId = '<%=request.getAttribute("fdlsOrderLineId")%>';
		if(quoteId != 'null'){
			var filename = '<%=request.getAttribute("quote")%>';
			var id = orderLineKey+"_q";
			document.getElementById(id).value='<%=request.getContextPath()%>/extn/pdfDownload?file='+filename+'';
			reqBtn.click();
		}
		
		if(confirmId != 'null'){
			var filename = '<%=request.getAttribute("confirm")%>';
			var id = orderLineKey+"_c";
			document.getElementById(id).value='<%=request.getContextPath()%>/extn/pdfDownload?file='+filename+'';
			reqBtn.click();
		}
		
		if(fdlsId != 'null'){
			var filename = '<%=request.getAttribute("fdls")%>';
			var id = orderLineKey+"_f";
			document.getElementById(id).value='<%=request.getContextPath()%>/extn/pdfDownload?file='+filename+'';
			reqBtn.click();
		}
	}
	
	function templateRowCallItemLookup_td(obj,ItemID,pc,uom,entity,extraParams,id,descId){
		if(service){
			getItemService(obj,ItemID,pc,uom,entity,extraParams,id,descId);
		}else{
			var itemEle = document.getElementById(id);
			extraParams = extraParams+"&itemField="+ itemEle.value;
			templateRowCallItemLookup(obj,ItemID,pc,uom,entity,extraParams);
				service = true;
		}	
	}
	
	window.attachEvent("onload", loadFile);
</script>

<%
	String uploadPath = (String) session.getAttribute("FilePath");
	//System.out.println(uploadPath);
	if(isVoid(uploadPath)){
		YFCDocument inputDoc = YFCDocument.getDocumentFor("<GetProperty PropertyName=\"pdf.dump.folder\" />");
		YFCDocument template = YFCDocument.getDocumentFor("<GetProperty PropertyValue=\"\" />");
		//System.out.println("Null or white Space");
%>
		<yfc:callAPI apiName='getProperty' inputElement='<%=inputDoc.getDocumentElement()%>' templateElement='<%=template.getDocumentElement()%>' outputNamespace='FolderPath'/>
<%
		YFCElement outputEle = getElement("FolderPath");
		
		if(isVoid(outputEle)){
			session.setAttribute("FilePath","");
			//System.out.println("empty");
		}else{
			session.setAttribute("FilePath",outputEle.getAttribute("PropertyValue"));
			//System.out.println("folder:"+outputEle.getAttribute("PropertyValue"));
		}
	}
%>

<%
	String strKey = (request.getAttribute("orderHeaderKey")).toString(); 
	session.setAttribute("EKey",strKey);
	session.setAttribute("docType", "0003");
%>

<%
	appendBundleRootParent((YFCElement)request.getAttribute("Order"));
	boolean bAppendOldValue = false;
	if(!isVoid(errors) || equals(sOperation,"Y") || equals(sOperation,"DELETE")) 
		bAppendOldValue = true;
   String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("Order", "xml:/Order/@EnterpriseCode"));
   
   String status = getValue("Order","xml:/Order/@Status");
%>

<%
String recvNode = resolveValue("xml:/Order/@ReceivingNode");
if (XmlUtils.isVoid(recvNode)) {
	recvNode = resolveValue("xml:/Order/@BuyerOrganizationCode");
}
%>
<table class="table" ID="OrderLines" cellspacing="0" width="100%" >
    <thead>
        <tr>
            <td class="checkboxheader" sortable="no">
                <input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>
                <input type="hidden" id="userOperation" name="userOperation" value="" />
                <input type="hidden" id="numRowsToAdd" name="numRowsToAdd" value="" />
            </td>
            <td class="tablecolumnheader" nowrap="true" style="width:30px">&nbsp;</td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Line</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Cust_Line</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Cust_RMA</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Customer_Reference_Num</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Control_Num</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Customer_Purchase_Order_Num</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Item</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Description</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Product_Class</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>UOM</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no"><yfc:i18n>TD_System_Serial_Num</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no"><yfc:i18n>TD_System_Type</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Agreement_Warranty_Num</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Out_of_Box_failure</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Serial</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_OEM_Serial_Num</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_ROHS</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service_Type</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Ship_From</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Ship_To</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Qty_Ordered_Today</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Qty</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Lead_Time</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Confirmation</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_FDLS</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_RO_Ship_Date</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Expected_Delivery</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Ship_Late</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Handling_Chrge_Applied</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Repair_No_Chrge_Code</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Expedite_No_Chrge_Code</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Auto_No_Charges_Code</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Payment</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Open_Blanket_PO_Balance</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Status</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Specific_Review_Contact</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML name="Order" binding="xml:/Order/OrderLines/@OrderLine" id="OrderLine">
        <%if(!isVoid(resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey"))) {
            if (equals(getValue("OrderLine","xml:/OrderLine/@ItemGroupCode"), INVConstants.ITEM_GROUP_CODE_SHIPPING) ) {
				if(bAppendOldValue) 
				{
					String sOrderLineKey = resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey");
					if(oMap.containsKey(sOrderLineKey))
						request.setAttribute("OrigAPIOrderLine",(YFCElement)oMap.get(sOrderLineKey));
				} 
				else 
					request.setAttribute("OrigAPIOrderLine",(YFCElement)pageContext.getAttribute("OrderLine"));		
				%>
            <tr>
                <yfc:makeXMLInput name="orderLineKey">
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>                
                </yfc:makeXMLInput>

				<yfc:makeXMLInput name="derivedFromLineKey">
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@DerivedFromOrderLineKey"/>
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@PrimeLineNo" value="xml:/OrderLine/DerivedFromOrderLine/@PrimeLineNo"/>
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@SubLineNo" value="xml:/OrderLine/DerivedFromOrderLine/@SubLineNo"/>
                </yfc:makeXMLInput>
                <yfc:makeXMLInput name="derivedFromOrderKey">
                    <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/OrderLine/@DerivedFromOrderHeaderKey"/>
                </yfc:makeXMLInput>

					<yfc:makeXMLInput name="bundleRootParentKey">
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@BundleRootParentKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@IsBundleParent" value="xml:/OrderLine/@IsBundleParent"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OriginalLineItemClicked" value="xml:/OrderLine/@PrimeLineNo"/>
					</yfc:makeXMLInput>
                <td class="checkboxcolumn" >
                    <input type="checkbox" value='<%=getParameter("orderLineKey")%>' name="chkEntityKey"
				<% 
					if( !showOrderLineNo("Order","Order") ){%> disabled="true" <%}%>
					/>
                    <%/*This hidden input is required by yfc to match up each line attribute that is editable in this row 
                        against the appropriate order line # on the server side once you save.  */%>
                    <input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@PrimeLineNo", "xml:/OrderLine/@PrimeLineNo")%> />
                    <input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@SubLineNo", "xml:/OrderLine/@SubLineNo")%> />
                    <input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@OrderLineKey", "xml:/OrderLine/@OrderLineKey")%> />
                </td>
                <td class="tablecolumn" nowrap="true">
                    <% if( ! equals("Y", getValue("Order","xml:/OrderLine/@isHistory")) ) { %>
                        <yfc:hasXMLNode binding="xml:/OrderLine/Exceptions/Exception">
                            <a <%=getDetailHrefOptions("L05", getParameter("orderLineKey"), "")%>>
                                <img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.YANTRA_TITLE_ALERT_EXCEPTIONS, "Exceptions")%>></a>
                        </yfc:hasXMLNode>
                    <% } %>
                    <yfc:hasXMLNode binding="xml:/OrderLine/KitLines/KitLine">
                        <a <%=getDetailHrefOptions("L06", getParameter("orderLineKey"), "")%>>
                            <img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Kit_Components")%>></a>
                    </yfc:hasXMLNode>
						<% if (equals(getValue("OrderLine","xml:/OrderLine/@IsBundleParent"),"Y")) { %>
						<a <%=getDetailHrefOptions("L06", getParameter("bundleRootParentKey"), "")%>>
								<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Bundle_Components")%>></a>
								<%}%>

                    <% if (equals(getValue("OrderLine","xml:/OrderLine/@HasDerivedChild"),"Y") || (!isVoid(getValue("OrderLine","xml:/OrderLine/@DerivedFromOrderLineKey"))) || equals(getValue("OrderLine","xml:/OrderLine/@HasChainedChild"),"Y") || (!isVoid(getValue("OrderLine","xml:/OrderLine/@ChainedFromOrderLineKey")))) { %>                        
                        <a <%=getDetailHrefOptions("L09", getParameter("orderLineKey"), "")%>>
                            <img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.CHAINED_ORDERLINES_COLUMN, "Related_Order_Lines")%>></a>
                    <% }%>

					<%	if (equals(getValue("OrderLine","xml:/OrderLine/@HasServiceLines"),"Y")) { %>
								<a <%=getDetailHrefOptions("L13", getParameter("orderLineKey"), "")%>><img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/providedservicecol.gif", "Line_Has_Associated_Service_Requests")%> ></a>

						<%	}	else	if (equals(getValue("OrderLine","xml:/OrderLine/@CanAddServiceLines"),"Y") && ! equals(getValue("Order","xml:/Order/@isHistory"),"Y")) { %>  
								<a <%=getDetailHrefOptions("L12", getParameter("orderLineKey"), "")%>><img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/addprovidedservice.gif", "Line_Has_Service_Requests_That_Can_Be_Added")%> ></a>
						<%	}	%>
					
					<% if (isTrue("xml:/OrderLine/@AwaitingDeliveryRequest") 
							&& ! equals(getValue("Order","xml:/Order/@isHistory"),"Y")) { %>
						<yfc:makeXMLInput name="orderKey">
							<yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
						</yfc:makeXMLInput>
						<a <%=getDetailHrefOptions("L10", getParameter("orderKey"), "")%>>
							<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/deliveryitem.gif", "Delivery_Request_needs_to_be_added")%>></a>
					<% } %>
                    <% String currentWorkOrderKey = getValue("OrderLine", "xml:/OrderLine/@CurrentWorkOrderKey");
                       if (!isVoid(currentWorkOrderKey)) { %>
                            <yfc:makeXMLInput name="workOrderKey">
                                <yfc:makeXMLKey binding="xml:/WorkOrder/@WorkOrderKey" value="xml:/OrderLine/@CurrentWorkOrderKey"/>
                            </yfc:makeXMLInput>
                            <a <%=getDetailHrefOptions("L11", getParameter("workOrderKey"), "")%>>
                                <img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/workorders.gif", "View_Work_Order")%>>
                            </a>
                    <% } %>
                </td>
                <td class="tablecolumn" sortValue="<%=getNumericValue("xml:OrderLine:/OrderLine/@PrimeLineNo")%>">
					<% if(showOrderLineNo("Order","Order")) {%>
	                    <a <%=getDetailHrefOptions("L01", getParameter("orderLineKey"), "")%>>
		                    <yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/>
						</a>
					<%} else {%>
						<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/>
					<%}%>
                </td>
				<td class="tablecolumn">
					<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/@CustomerLinePONo")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@CustomerLinePONo", "xml:/OrderLine/@CustomerLinePONo", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn">
					<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/Extn/@RMA")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/Extn/@RMA", "xml:/OrderLine/Extn/@RMA", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn">
					<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/Extn/@CutomerRef")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/Extn/@CutomerRef", "xml:/OrderLine/Extn/@CutomerRef", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ControlNo" />
				</td>
				<td class="tablecolumn">
					<input type="text" <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/@CustomerPONo")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@CustomerPONo", "xml:/OrderLine/@CustomerPONo", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn" nowrap="true">
					<input type="hidden" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Item/@ItemID" value='<%=resolveValue("xml:/OrderLine/Item/@ItemID")%>' />
					<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID" />
                </td>
				<td class="tablecolumn"><%=getLocalizedOrderLineDescription("OrderLine")%></td>
				<td class="tablecolumn">
					 <select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ProductClass","xml:/OrderLine/Item/@ProductClass", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:ProductClassList:/CommonCodeList/@CommonCode" name="CodeValue" 
						value="CodeValue" selected="xml:/OrderLine/Item/@ProductClass"/>
					</select>
				</td>
				<td class="tablecolumn">
					<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@TransactionalUOM","xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
					   <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
						value="UnitOfMeasure" selected="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM"/>
					</select>
				</td>				
				<td class="tablecolumn">
					<input type="hidden" id="ibLineKey_<%=OrderLineCounter%>" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@IBLineKey" value='<%=resolveValue("xml:/OrderLine/Extn/@IBLineKey")%>'/>
					<input type="text" class="protectedinput" id='tdSerialNo_<%=OrderLineCounter%>' OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SystemSerialNo","xml:/OrderLine/Extn/@SystemSerialNo","xml:/Order/AllowedModifications","ADD_LINE","text")%> onblur="getwarranty('tdSerialNo_<%=OrderLineCounter%>','INBorder','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemSerialNo','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemType','<%=recvNode%>');" /><img class="lookupicon" id='tdSerialNo_<%=OrderLineCounter%>_img' name="search" onclick="callLookupforserial('tdSerialNo_<%=OrderLineCounter%>','INBorder','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemSerialNo','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemType','ibLineKey_<%=OrderLineCounter%>','<%=recvNode%>');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_serial")%>/>
				</td>
				<td class="tablecolumn">
					<input type="text" class="protectedinput" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemType" value='<%=resolveValue("xml:/OrderLine/Extn/@SystemType")%>' />
				</td>
				<td class="tablecolumn">
					<%
						String lineStatus = resolveValue("xml:/OrderLine/@MaxLineStatusDesc");
						String strSerialno = resolveValue("xml:/OrderLine/Extn/@SystemSerialNo");
						if("Draft Created".equalsIgnoreCase(lineStatus)){
							
							YFCDocument orderLineInput = YFCDocument.getDocumentFor("<OrderLine StatusQryType=\"BETWEEN\" ToStatus=\"1100.30\"><Order DocumentType=\"0017.ex\"/><Extn SystemSerialNo=\""+strSerialno+"\" /> </OrderLine>");
							YFCDocument orderLinetemplate = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=\"\"><OrderLine OrderLineKey=\"\" /></OrderLineList>");
					%>
						<yfc:callAPI apiName='getOrderLineList' inputElement='<%=orderLineInput.getDocumentElement()%>'
							templateElement='<%=orderLinetemplate.getDocumentElement()%>' outputNamespace='OrderLines'/>
					<%
						YFCElement orderLines = getElement("OrderLines");
						
						if(!XmlUtils.isVoid(orderLines)){
						YFCNodeList<YFCElement> list = orderLines.getElementsByTagName("OrderLine");
						if(list.getLength() == 1){
							YFCElement tOrderLineElement = list.item(0);
							
							String strIBOrderLineKey = tOrderLineElement.getAttribute("OrderLineKey");
							YFCDocument scInput = YFCDocument.getDocumentFor("<TerSCIBMapping TerIBOLKey=\""+strIBOrderLineKey+"\"><YFSSCOrderLine StatusQryType=\"BETWEEN\" ToStatus=\"1100.200\"/></TerSCIBMapping>");
					%>
							<yfc:callAPI serviceName="GetIBForLine" inputElement='<%=scInput.getDocumentElement()%>'  outputNamespace="SCIBLines"/>
					<%	
						YFCElement SCIBLines = getElement("SCIBLines");
						if(!XmlUtils.isVoid(SCIBLines)){
							YFCIterable<YFCElement> TerSCIBMappingList = SCIBLines.getChildren("TerSCIBMapping");	
						
							long minPriNo = -1;
							String minPriOrderNo = "";
							LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
							for (YFCElement TerSCIBMapping : TerSCIBMappingList) {
								YFCElement YFSSCOrderLine = TerSCIBMapping.getChildElement("YFSSCOrderLine");
								String maxLineDesc = YFSSCOrderLine.getAttribute("MaxLineStatusDesc");
								if(!XmlUtils.isVoid(YFSSCOrderLine) && ( ("ACTIVE".equalsIgnoreCase(maxLineDesc)) || ("RENEWAL PENDING".equalsIgnoreCase(maxLineDesc)) )){
									YFCElement orderEle = YFSSCOrderLine.getChildElement("Order");
									if(!XmlUtils.isVoid(orderEle)){
										String orderNo = orderEle.getAttribute("OrderNo");
										String strPriorityNo = orderEle.getAttribute("PriorityNumber");
										if(!XmlUtils.isVoid(orderNo)) {
											if(!XmlUtils.isVoid(strPriorityNo)){
												long priorityNo = Integer.parseInt(strPriorityNo);
												if(minPriNo == -1 || minPriNo > priorityNo){
													minPriNo = priorityNo;
													minPriOrderNo = orderNo;
												}
											}
										map.put(orderNo,orderNo);
									}
								}
							}
							}
							YFCElement scNo = prepareAgreemetNoXML(map,minPriOrderNo);
							request.setAttribute("Agreements",scNo);
						}
						}
					}
					%>
						<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@AgreementNo","xml:/OrderLine/Extn/@AgreementNo", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
							<yfc:loopOptions binding="xml:Agreements:/ServiceContractList/@ServiceContract" name="AgreementNo"
						 value="AgreementNo" selected="xml:/OrderLine/Extn/@AgreementNo"/>
						</select>
					<%}else{%>
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@AgreementNo" />
					<%}%>
				</td>
				<td class="tablecolumn">
					<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@OutOfBoxFailure","xml:/OrderLine/Extn/@OutOfBoxFailure", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue" 
						value="CodeValue" selected="xml:/OrderLine/Extn/@OutOfBoxFailure" />
					</select>
				</td>
				<td class="tablecolumn">
					<%if("Draft Created".equalsIgnoreCase(lineStatus)){%>
						<input type="text" <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/@SerialNo")%>" <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@SerialNo", "xml:/OrderLine/@SerialNo", "xml:/OrderLine/AllowedModifications")%>/>
					<%}else{%>
						<yfc:getXMLValue binding="xml:/OrderLine/@SerialNo" />
					<%}%>
				</td>
				<td class="tablecolumn">
					<input type="text" <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/Extn/@OEMSerialNo")%>" <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/Extn/@OEMSerialNo", "xml:/OrderLine/Extn/@OEMSerialNo", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/PrimaryInformation/@IsHazmat" />
				</td>
				<td class="tablecolumn" >
					<%
						if("Draft Created".equalsIgnoreCase(lineStatus)){
							
							YFCDocument input = YFCDocument.getDocumentFor("<CommonCode CodeType=\""+resolveValue("xml:OrderLine:/OrderLine/ItemDetails/Extn/@SupportStatusCode")+"\" />");
							YFCDocument template = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeType=\"\" CodeValue=\"\" /></CommonCodeList>");
					%>
						<yfc:callAPI apiName='getCommonCodeList' inputElement='<%=input.getDocumentElement()%>'
							templateElement='<%=template.getDocumentElement()%>' outputNamespace='ServiceTypes'/>
						<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ServiceType","xml:/OrderLine/Extn/@ServiceType", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
							<yfc:loopOptions binding="xml:ServiceTypes:/CommonCodeList/@CommonCode" name="CodeValue"
						 value="CodeValue" selected="xml:/OrderLine/Extn/@ServiceType"/>
						</select>
					<%}else{%>
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ServiceType" />
					<%}%>
				</td>
				<td class="tablecolumn" nowrap="true">
					<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/@ReceivingNode")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@ReceivingNode", "xml:/OrderLine/@ReceivingNode", "xml:/OrderLine/AllowedModifications")%>/>
					<img class="lookupicon" onclick="callLookup(this,'shipnode')" <%=yfsGetImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Receiving_Node", "xml:/OrderLine/@ReceivingNode", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn" nowrap="true">
					<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/@ShipNode")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@ShipNode", "xml:/OrderLine/@ShipNode", "xml:/OrderLine/AllowedModifications")%>/>
					<img class="lookupicon" onclick="callLookup(this,'TDshipnode')" <%=yfsGetImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Ship_Node", "xml:/OrderLine/@ShipNode", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@QTYOrderToday" />
				</td>
				<td class="tablecolumn">
					<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/OrderLineTranQuantity/@OrderedQty")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/OrderLineTranQuantity/@OrderedQty", "xml:/OrderLine/OrderLineTranQuantity/@OrderedQty", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn">
					<input type="text" <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@LeadTime")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@LeadTime","xml:/OrderLine/Extn/@LeadTime","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
				</td>
				<input type="hidden" id="hidKey_<%=OrderLineCounter%>" name="xml:/Order/@RequiredKey" value="" />
				<%if("Created".equalsIgnoreCase(status)){
					String confirm = resolveValue("xml:/OrderLine/Extn/@Confirmation");
					if(!isVoid(confirm)){
						String strConfirmFileName = confirm.substring(confirm.indexOf("file=")+5);
				%>
						<td class="tablecolumn" id="confirm_Td_<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>">
							<a href="<%=resolveValue("xml:/OrderLine/Extn/@Confirmation")%>"><%=strConfirmFileName%></a>
								&nbsp;<%if("Created1".equalsIgnoreCase(status)){%><a href="#" onclick="return edit('confirm_Td_<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>','confirm');">Edit</a><%}%>
						</td>
				<%
					} else if("Created".equalsIgnoreCase(status)){%>
						<input type="hidden" id="<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>_c" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@Confirmation" value="" />
						<td class="tablecolumn" id='<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>_confirm'>
							<input type="hidden" id="hidconfirm_<%=OrderLineCounter%>" name="xml:/Order/@TempConfirm" value="" />
							<input type="file" id="confirm_<%=OrderLineCounter%>" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@Confirmation" oldValue=" " value=""/><input type="button" value="upload" onclick="return validateFile('confirm_<%=OrderLineCounter%>','hidconfirm_<%=OrderLineCounter%>','confirm','hidKey_<%=OrderLineCounter%>','<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>');" />
						</td>
				<%
					}
				%>
				<%
					}else{
				%>
						<td class="tablecolumn"></td>	
				<%}%>
				<%if("Created".equalsIgnoreCase(status)){
					String fdls = resolveValue("xml:/OrderLine/Extn/@FDLS");
					if(!isVoid(fdls)){
						String strFDLSFileName = fdls.substring(fdls.indexOf("file=")+5);
				%>
						<td class="tablecolumn" id="fdls_Td_<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>">
							<a href="<%=resolveValue("xml:/OrderLine/Extn/@FDLS")%>"><%=strFDLSFileName%></a>
								&nbsp;<%if("Created1".equalsIgnoreCase(status)){%><a href="#" onclick="return edit('fdls_Td_<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>','fdls');">Edit</a><%}%>
						</td>
				<%
					} else if("Created".equalsIgnoreCase(status)){%>
						<input type="hidden" id="<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>_f" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@FDLS" value="" />
						<td class="tablecolumn" id='<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>_fdls'>
							<input type="hidden" id="hidfdls_<%=OrderLineCounter%>" name="xml:/Order/@TempFDLS" value="" />
							<input type="file" id="fdls_<%=OrderLineCounter%>" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@FDLS" oldValue=" " value=""/><input type="button" value="upload" onclick="return validateFile('fdls_<%=OrderLineCounter%>','hidfdls_<%=OrderLineCounter%>','fdls','hidKey_<%=OrderLineCounter%>','<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>');" />
						</td>
				<%
					}
				%>
				<%
					}else{
				%>
						<td class="tablecolumn"></td>	
				<%}%>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/@ReqShipDate" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/@EarliestDeliveryDate" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/Order/OrderLines/OrderLine/Extn/@ExtnShipLate" />
				</td>
				<td class="tablecolumn">
					<%if("Draft Created".equalsIgnoreCase(lineStatus)){%>
						<select OldValue="" id="handleCode<%=OrderLineCounter%>" onchange="enableDisableCode('repairCode<%=OrderLineCounter%>','expediteCode<%=OrderLineCounter%>','handleCode<%=OrderLineCounter%>');" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@HandlingNoChargeCode","xml:/OrderLine/Extn/@HandlingNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
							<yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue" 
							value="CodeValue" selected="xml:/OrderLine/Extn/@HandlingNoChargeCode"/>
						</select>
					<%}else{%>
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@HandlingNoChargeCode" />	
					<%}%>
				</td>
				<td class="tablecolumn">
					<%
					String strHandleCode = resolveValue("xml:/OrderLine/Extn/@HandlingNoChargeCode");
					if("Draft Created".equalsIgnoreCase(lineStatus)){%>
					<select id="repairCode<%=OrderLineCounter%>" <%if("N".equalsIgnoreCase(strHandleCode)){%>disabled<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@RepairNoChargeCode","xml:/OrderLine/Extn/@RepairNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" 
						value="CodeValue" selected="xml:/OrderLine/Extn/@RepairNoChargeCode"/>
					</select>
					<%}else{%>
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@RepairNoChargeCode" />
					<%}%>
				</td>
				<td class="tablecolumn">
					<%if("Draft Created".equalsIgnoreCase(lineStatus)){%>
					<select id="expediteCode<%=OrderLineCounter%>" <%if("N".equalsIgnoreCase(strHandleCode)){%>disabled<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExpeditedNoChargeCode","xml:/OrderLine/Extn/@ExpeditedNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" 
						value="CodeValue" selected="xml:/OrderLine/Extn/@ExpeditedNoChargeCode"/>
					</select>
					<%}else{%>
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ExpeditedNoChargeCode" />
					<%}%>
				</td>
				<td><yfc:getXMLValue binding="xml:/OrderLine/Extn/@AutomaticNoChargeCode" /></td>
				<td class="tablecolumn">
					<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ExtnPayment")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExtnPayment","xml:/OrderLine/Extn/@ExtnPayment", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:Payment:/PaymentTypeList/@PaymentType" name="PaymentType" 
						value="PaymentType" selected="xml:/OrderLine/Extn/@ExtnPayment"/>
					</select>
				</td>
				<td><yfc:getXMLValue binding="xml:/OrderLine/Extn/@OpenBalance" /></td>
				<td class="tablecolumn">
					<a <%=getDetailHrefOptions("L04", getParameter("orderLineKey"),"ShowReleaseNo=Y")%>><%=displayOrderStatus(getValue("OrderLine","xml:/OrderLine/@MultipleStatusesExist"),getValue("OrderLine","xml:/OrderLine/@MaxLineStatusDesc"),true)%></a>
				</td>
				<td class="tablecolumn" nowrap="true">
					<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/Extn/@SPRName")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/Extn/@SPRName", "xml:/OrderLine/Extn/@SPRName", "xml:/OrderLine/AllowedModifications")%>/>
					<img class="lookupicon" onclick="callLookup(this,'userlookup')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Review_Engg")%>/>
                </td>
			<%}
		  } else {%>
             <tr DeleteRowIndex="<%=OrderLineCounter%>">
                <td class="checkboxcolumn"> 
                    <img class="icon" onclick="setDeleteOperationForRow(this,'xml:/Order/OrderLines/OrderLine')" <%=getImageOptions(YFSUIBackendConsts.DELETE_ICON, "Remove_Row")%>/>
                </td>
                <%if (isTrue("xml:/Order/@HasDerivedParent")) {%>
                    <td class="tablecolumn">&nbsp;</td>
                <%}%>
               <td class="tablecolumn">
					<input type="hidden" OldValue="" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@Action", "xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@Action", "CREATE")%> />
                    <input type="hidden"  <%=getTextOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@DeleteRow",  "")%> />
                </td>
                <td class="tablecolumn">&nbsp;</td>
				<td class="tablecolumn">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@CustomerLinePONo","xml:/OrderLine/@CustomerLinePONo","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
				</td>
				<td class="tablecolumn">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@RMA","xml:/OrderLine/Extn/@RMA","xml:/Order/AllowedModifications","ADD_LINE","text")%> maxlength="20"/>
				</td>
				<td class="tablecolumn">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@CutomerRef","xml:/OrderLine/Extn/@CutomerRef","xml:/Order/AllowedModifications","ADD_LINE","text")%> maxlength="20"/>
				</td>
				<td class="tablecolumn"></td>
				<td class="tablecolumn">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@CustomerPONo","xml:/OrderLine/@CustomerPONo","xml:/Order/AllowedModifications","ADD_LINE","text")%> maxlength="100"/>
				</td>
				<td class="tablecolumn" nowrap="true">
					<input type="text" id='tdPartNo_<%=OrderLineCounter%>' OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ItemID","xml:/OrderLine/Item/@ItemID","xml:/Order/AllowedModifications","ADD_LINE","text")%> onblur="getItemService(this,'ItemID','ProductClass','TransactionalUOM','TDSitem','<%=extraParams%>','tdPartNo_<%=OrderLineCounter%>','tdDesc_<%=OrderLineCounter%>');"/>		
					<img class="lookupicon" id='tdPartNo_<%=OrderLineCounter%>_img' onclick="templateRowCallItemLookup_td(this,'ItemID','ProductClass','TransactionalUOM','TDSitem','<%=extraParams%>','tdPartNo_<%=OrderLineCounter%>','tdDesc_<%=OrderLineCounter%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item")%> />
                </td>
				<td class="tablecolumn" id='tdDesc_<%=OrderLineCounter%>'>&nbsp;</td>
				<td class="tablecolumn">
					<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ProductClass","xml:/OrderLine/Item/@ProductClass", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                       <yfc:loopOptions binding="xml:ProductClassList:/CommonCodeList/@CommonCode" name="CodeValue"
                       value="CodeValue" selected="xml:/OrderLine/Item/@ProductClass"/>
                   </select>
                </td>
				<td class="tablecolumn">      
				    <select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@TransactionalUOM","xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                       <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
                       value="UnitOfMeasure" selected="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM"/>
                   </select>
                </td>
				<td class="tablecolumn" nowrap="true">
					<input type="hidden" id="ibLineKey_<%=OrderLineCounter%>" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@IBLineKey","xml:/OrderLine/Extn/@IBLineKey","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
					<input type="text" class="protectedinput" id='tdSerialNo_<%=OrderLineCounter%>' OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SystemSerialNo","xml:/OrderLine/Extn/@SystemSerialNo","xml:/Order/AllowedModifications","ADD_LINE","text")%> onblur="getwarranty('tdSerialNo_<%=OrderLineCounter%>','INBorder','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemSerialNo','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemType','<%=recvNode%>');" /><img class="lookupicon" id='tdSerialNo_<%=OrderLineCounter%>_img' name="search" onclick="callLookupforserial('tdSerialNo_<%=OrderLineCounter%>','INBorder','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemSerialNo','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemType','ibLineKey_<%=OrderLineCounter%>','<%=recvNode%>');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_serial")%>/>
                </td>
				<td class="tablecolumn">
					<input type="text" class="protectedinput" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemType" value="" />
				</td>
				<td class="tablecolumn" nowrap="true">
					<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@AgreementNo","xml:/OrderLine/Extn/@AgreementNo", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            
                    </select>
				</td>
				<td class="tablecolumn">
					<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@OutOfBoxFailure","xml:/OrderLine/Extn/@OutOfBoxFailure", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                       <yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue"
                       value="CodeValue" />
                   </select>
                </td>
				<td class="tablecolumn">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@SerialNo","xml:/OrderLine/@SerialNo","xml:/Order/AllowedModifications","ADD_LINE","text")%> maxlength="40"/>
				</td>
				<td class="tablecolumn">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@OEMSerialNo","xml:/OrderLine/Extn/@OEMSerialNo","xml:/Order/AllowedModifications","ADD_LINE","text")%> maxlength="50"/>
				</td>
				<td class="tablecolumn">&nbsp;</td>
				<td class="tablecolumn">
					<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ServiceType","xml:/OrderLine/Extn/@ServiceType", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>

                    </select>
				</td>
				<td class="tablecolumn" nowrap="true">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@ReceivingNode","xml:/OrderLine/@ReceivingNode","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
					<img class="lookupicon" onclick="callLookup(this,'shipnode')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Receiving_Node")%>/>
				</td>
				<td class="tablecolumn" nowrap="true">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@ShipNode","xml:/OrderLine/@ShipNode","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
					<img class="lookupicon" onclick="callLookup(this,'TDshipnode')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Ship_Node")%>/>
				</td>
				<td class="tablecolumn" nowrap="true">&nbsp;</td>
				<td class="numerictablecolumn">
					<input type="text" OldValue="" 	<%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@OrderedQty","xml:/OrderLine/OrderLineTranQuantity/@OrderedQty","xml:/Order/AllowedModifications","ADD_LINE","text")%> style='width:40px'/>
				</td>
				<td class="tablecolumn">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@LeadTime","xml:/OrderLine/Extn/@LeadTime","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
				</td>
				<td class="tablecolumn" nowrap="true">&nbsp;</td>
				<td class="tablecolumn" nowrap="true">&nbsp;</td>
				<td class="tablecolumn" nowrap="true">&nbsp;</td>
				<td class="tablecolumn" nowrap="true">&nbsp;</td>
				<td class="tablecolumn" nowrap="true">&nbsp;</td>
				<td class="tablecolumn">
					<select OldValue="N" id="handleCode<%=OrderLineCounter%>" onchange="enableDisableCode('repairCode<%=OrderLineCounter%>','expediteCode<%=OrderLineCounter%>','handleCode<%=OrderLineCounter%>');" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@HandlingNoChargeCode","xml:/OrderLine/Extn/@HandlingNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%> >
                            <yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" selected="N"/>
                    </select>
				</td>
				<td class="tablecolumn">
					<select OldValue="" id="repairCode<%=OrderLineCounter%>" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@RepairNoChargeCode","xml:/OrderLine/Extn/@RepairNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%> disabled>
						<yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" 
						value="CodeValue" />
					</select>
				</td>
				<td class="tablecolumn">
					<select OldValue="" id="expediteCode<%=OrderLineCounter%>" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExpeditedNoChargeCode","xml:/OrderLine/Extn/@ExpeditedNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%> disabled>
						<yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" 
						value="CodeValue" />
					</select>
				</td>
				<td class="tablecolumn" nowrap="true">&nbsp;</td>
				<td class="tablecolumn">
					<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExtnPayment","xml:/OrderLine/Extn/@ExtnPayment", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:Payment:/PaymentTypeList/@PaymentType" name="PaymentType" 
						value="PaymentType" />
					</select>
				</td>
				<td class="tablecolumn">&nbsp;</td>
				<td class="tablecolumn">&nbsp;</td>
				<td class="tablecolumn">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SPRName","xml:/OrderLine/Extn/@SPRName","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
					<img class="lookupicon" onclick="callLookup(this,'userlookup')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Review_Engg")%>/>
				</td>
           </tr>
           <%}%>
        </yfc:loopXML>
    </tbody>
    <tfoot>        
		<%if (! isTrue("xml:/Order/@HasDerivedParent")) {%>
			<%if (isModificationAllowed("xml:/@AddLine","xml:/Order/AllowedModifications")) { %>
			<tr>
				<td nowrap="true" colspan="38">
					<jsp:include page="/common/editabletbl.jsp" flush="true">
                    <jsp:param name="ReloadOnAddLine" value="Y"/>
					</jsp:include>
				</td>
			</tr>
			<%} 
		}%>
    </tfoot>
</table>

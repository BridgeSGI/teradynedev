<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/orgName1.js"></script>
<script language="javascript">
	yfcDoNotPromptForChanges(true);
	
	function callLookupforCS(entity, customerSite){
		var obj = new Object();
		obj.field1 = document.all(customerSite);
		obj.lookup = true;
		obj.viewDetails = 'N';
		obj.createCustomerOrg = 'N';
		obj.createCustomerSiteOrg = 'N';
		obj.viewAudits = 'N';
		obj.addChildOrg = 'N';
		yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
		(document.all(customerSite)).focusout=callAjaxFunctionOrgChange(customerSite,'NODE');
	}
</script>
<%
    String modifyView = request.getParameter("ModifyView");
    boolean modify = "true".equals(modifyView);
	if (modifyView==null) modify = false;
	String sn = getValue("CurrentUser", "xml:CurrentUser:/User/@Node");
	boolean shipNodeUser = !isVoid(sn);
	String newReturn = resolveValue("xml:NewOrder:/Order/@OrderHeaderKey");

    String enterpriseCode = (String) request.getParameter("xml:/Order/@EnterpriseCode");
    if (isVoid(enterpriseCode)) {
        enterpriseCode = getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@PrimaryEnterpriseKey");
        request.setAttribute("xml:/Order/@EnterpriseCode", enterpriseCode);
    }
    
%>
<script language="javascript">
<%    if (!isVoid(newReturn)) {	 
        YFCDocument orderDoc = YFCDocument.createDocument("Order");
        orderDoc.getDocumentElement().setAttribute("OrderHeaderKey",resolveValue("xml:NewOrder:/Order/@OrderHeaderKey"));
        String keyString = orderDoc.getDocumentElement().getString(false);
        keyString = java.net.URLEncoder.encode(keyString);
%>
        function showPODetailPopup() {
            callPopupWithEntity('return', '<%=keyString%>');
        }

        function changeToPODetailView() {
            entityType = "return";
            showDetailFor('<%=orderDoc.getDocumentElement().getString(false)%>');
        }
<% 
        if (equals(request.getParameter(YFCUIBackendConsts.YFC_IN_POPUP), "Y")) { %>
            window.attachEvent("onload", showPODetailPopup);
        <% }
        else { %>
            window.attachEvent("onload", changeToPODetailView);
        <% }
    }
%>
</script>

<table class="view" width="100%">
    <tr>
        <td>
            <input type="hidden" name="xml:/Order/@EnteredBy" value="<%=resolveValue("xml:CurrentUser:/User/@Loginid")%>"/>
            <input type="hidden" name="xml:/Order/@DraftOrderFlag" value="Y"/>
            <input type="hidden" name="xml:/Order/@CreatedByNode" value="<%=sn%>"/>
			<input type="hidden" name="xml:/Order/@CreatedAtNode" value="<%= (shipNodeUser ? "Y" : "N")%>"/>
			<input type="hidden" name="xml:/Order/@BillToKey" value="<%=getValue("Order", "xml:/Order/@BillToKey")%>"/>
			<input type="hidden" name="xml:/Order/@ShipToKey" value="<%=getValue("Order", "xml:/Order/@ShipToKey")%>"/>
			<input type="hidden" name="xml:/Order/PriceInfo/@ReportingConversionDate" value="<%=resolveValue("xml:/Order/PriceInfo/@ReportingConversionDate")%>"/>
        </td>			
    </tr>
	
	<% 	//cr 35413
		String isResetTrue = getParameter("ResetDetailPageDocumentType");
		if(isVoid(isResetTrue) )
			isResetTrue = "N";
	%>

    <% if (modify) { %>
        <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
            <jsp:param name="ScreenType" value="detail"/>
            <jsp:param name="RefreshOnDocumentType" value="true"/>
            <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
            <jsp:param name="ApplicationCode" value="omr"/>
            <jsp:param name="ResetDocumentType" value='<%=isResetTrue%>'/>
        </jsp:include>
        <% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code)
           // Order Type is refreshed. %>
        <yfc:callAPI apiID="AP1"/>

    <% } else { %>
        <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
            <jsp:param name="ScreenType" value="detail"/>
            <jsp:param name="RefreshOnDocumentType" value="true"/>
            <jsp:param name="ShowEnterpriseCode" value="false"/>
            <jsp:param name="ApplicationCode" value="omr"/>
            <jsp:param name="ResetDocumentType" value='<%=isResetTrue%>'/>
        </jsp:include>
        <% // Now call the APIs that are dependent on the common fields (Doc Type)
           // Order Type is refreshed. 
		%>
        <yfc:callAPI apiID="AP2"/>
    <% } %>

	<!-- TD -->
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_Customer_PO_NO</yfc:i18n></td>
		<td><input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@CustomerPONo","")%>/></td>
		<td class="detaillabel"><yfc:i18n>Order_Type</yfc:i18n></td>
		<td>
            <select class="combobox" <%=getComboOptions("xml:/Order/@OrderType")%>>
                <yfc:loopOptions binding="xml:OrderTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" isLocalized="Y"/>
            </select>
        </td>
		<td class="detaillabel" ><yfc:i18n>Order_Date</yfc:i18n></td>
        <td nowrap="true">
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/@OrderDate_YFCDATE", getTodayDate())%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
        </td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>Order_#</yfc:i18n></td>
        <td><input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderNo","")%>/></td>
        <td class="detaillabel" ><yfc:i18n>Customer</yfc:i18n></td>
		<td nowrap="true" >
            <input type="text" id="customerSiteLookup" class="unprotectedinput" <%=getTextOptions("xml:/Order/@ReceivingNode")%> onblur= "callAjaxFunctionOrgChange('customerSiteLookup','NODE')"/>			
            <img class="lookupicon" onclick="callLookupforCS('TDCustSiteOrg', 'customerSiteLookup')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
		<td class="detaillabel" ><yfc:i18n>Currency</yfc:i18n></td>
		<td>
			<input type="text" id="tdCurrency" class="protectedinput" name="xml:/Order/PriceInfo/@Currency" value="" readOnly />
        </td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td class="detaillabel" ><yfc:i18n>TD_Customer_Name</yfc:i18n></td>
		<td nowrap="true">
			<input size="50" type="text" id="customerSiteLookupName" class="protectedinput"  <%=getTextOptions("xml:/Order/Extn/@CustOrgName")%> readonly />
        </td>
		<td></td>
		<td></td>
	</tr>
	<!-- TD -->
</table>
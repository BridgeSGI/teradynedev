<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/yfsjspcommon/editable_util_header.jspf" %>


<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<%  String sHiddenDraftOrderFlag = getValue("Order", "xml:/Order/@DraftOrderFlag");
	String DocumentType = getValue("Order", "xml:/Order/@DocumentType");
    String driverDate = getValue("Order", "xml:/Order/@DriverDate");
	String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("Order", "xml:/Order/@EnterpriseCode"));
	extraParams += "&" + getExtraParamsForTargetBinding("xml:/Order/@OrderHeaderKey", resolveValue("xml:/Order/@OrderHeaderKey"));
	extraParams += "&" + getExtraParamsForTargetBinding("IsStandaloneService", "Y");
	extraParams += "&" + getExtraParamsForTargetBinding("hiddenDraftOrderFlag", sHiddenDraftOrderFlag);
	extraParams += "&" + getExtraParamsForTargetBinding("DocumentType", DocumentType);
	
%>

<script language="javascript">
	// this method is used by 'Add Service Request' action on order header detail innerpanel
	function callPSItemLookup()	{
		yfcShowSearchPopupWithParams('','itemlookup',900,550,new Object(), 'psItemLookup', '<%=extraParams%>');
	}
</script>
<%
String sRequestDOM = request.getParameter("getRequestDOM");
boolean	modify = "true".equals(request.getParameter("ModifyView"));
String hdl = getValue("Order", "xml:/Order/@HasDerivedLines");
if ("Y".equals(hdl) || "true".equals(hdl))
	modify=false;

//for buyer, seller and currency only
boolean	modifyFlag = "true".equals(request.getParameter("ModifyView"));
String hasDerivedParent = getValue("Order", "xml:/Order/@HasDerivedParent");
if ("Y".equals(hasDerivedParent) || "true".equals(hasDerivedParent))
	modifyFlag=false;


//check if there is any exchange order created for the return.
boolean hasNoExchange = true;
YFCElement elem = getElement("Order");
if (elem != null) {
	YFCElement exchangeOrders = elem.getChildElement("ExchangeOrders");
	if (exchangeOrders != null) {
		YFCElement exchangeOrder = exchangeOrders.getChildElement("ExchangeOrder");
		if (exchangeOrder != null) {		
			//set HasNoExchange flag to check for exchange order.
			hasNoExchange = false;
		}
	}
}

%>

<table class="view" width="100%">
    <yfc:makeXMLInput name="orderKey">
        <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey" ></yfc:makeXMLKey>
    </yfc:makeXMLInput>
    <yfc:makeXMLInput name="returnKeyForExchange">
        <yfc:makeXMLKey binding="xml:/ReturnOrder/@ReturnOrderHeaderKeyForExchange" value="xml:/Order/@OrderHeaderKey" ></yfc:makeXMLKey>
    </yfc:makeXMLInput>
    <tr>
        <td>
            <input type="hidden" <%=getTextOptions("xml:/Order/@ModificationReasonCode","")%> />
            <input type="hidden" <%=getTextOptions("xml:/Order/@ModificationReasonText","")%>/>
			<input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>	<%-- cr 36191 --%>
            <input type="hidden" name="xml:/Order/@Override" value="N"/>
            <input type="hidden" name="TempHeaderKey" value='<%=getValue("Order", "xml:/Order/@OrderHeaderKey")%>'/>
            <input type="hidden" name="TempBuyer" value='<%=getValue("Order", "xml:/Order/@BuyerOrganizationCode")%>'/>
            <input type="hidden" name="TempSeller" value='<%=getValue("Order", "xml:/Order/@SellerOrganizationCode")%>'/>
            <input type="hidden" name="TempCurrency" value='<%=getValue("Order", "xml:/Order/PriceInfo/@Currency")%>'/>
            <input type="hidden" name="xml:/AllocateOrder/@RuleID" value='<%=getValue("Order", "xml:/Order/@AllocationRuleID")%>'/>
            <input type="hidden" name="xml:/AllocateOrder/@IgnoreAllocateDate" value='N'/>
            <input type="hidden" name="hiddenDraftOrderFlag" value='<%=getValue("Order", "xml:/Order/@DraftOrderFlag")%>'/>
            <input type="hidden" name="chkExchangeEntityKey" value='<%=getParameter("returnKeyForExchange")%>'/>
			<input type="hidden" name="xml:/Order/@EnterpriseCode" value='<%=getValue("Order", "xml:/Order/@EnterpriseCode")%>'/>
            <input type="hidden" name="WorkOrderEntityKey" value='<%=getParameter("orderKey")%>'/>
        </td>
    </tr>
    <tr>
        <td class="detaillabel"><yfc:i18n>TD_Order#</yfc:i18n></td>
        <td class="protectedtext">
		<input type="text" class="protectedtext" readonly OldValue="" <%=getTextOptions("xml:/Order/@OrderNo")%> />
		</td>
        <td class="detaillabel"><yfc:i18n>Order_Type</yfc:i18n></td>
		<td>
			<select class="combobox" id="TDOrderType" <%=getComboOptions("xml:/Order/@OrderType")%>>
				<yfc:loopOptions binding="xml:OrderTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" isLocalized="Y" selected="xml:/Order/@OrderType"/>
			</select>
		</td>
		<td class="detaillabel"><yfc:i18n>TD_Customer_ID</yfc:i18n></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:/Order/@ReceivingNode"/></td>
		<%
		String custId = resolveValue("xml:/Order/@ReceivingNode");
		String orgName = "";
		if (!isVoid(custId)) {
			YFCDocument inputDoc = YFCDocument.parse("<Organization OrganizationKey=\"" + custId + "\" />");
			YFCDocument outputTemplate = YFCDocument.parse("<Organization OrganizationCode=\"\" OrganizationName=\"\" />");
		%>
			<yfc:callAPI apiName="getOrganizationList" inputElement="<%=inputDoc.getDocumentElement()%>" templateElement="<%=outputTemplate.getDocumentElement()%>" outputNamespace="cust"/>
		<%
			YFCElement orgList = getElement("cust");
			YFCElement org = orgList.getChildElement("Organization");
			if (!isVoid(org)) {
				orgName = org.getAttribute("OrganizationName");
			}
		}
		%>
		<td class="detaillabel"><yfc:i18n>TD_Customer_Name</yfc:i18n></td>
		<td class="protectedtext"><%=orgName%></td>
	</tr>	
	<tr>
		<td class="detaillabel" ><yfc:i18n>Status</yfc:i18n></td>
        <td class="protectedtext">
            <% if (isVoid(getValue("Order", "xml:/Order/@Status"))) {%>
                [<yfc:i18n>Draft</yfc:i18n>]
            <% } else { %>
                <a <%=getDetailHrefOptions("L01", getParameter("orderKey"), "ShowReleaseNo=Y")%>><%=displayOrderStatus(getValue("Order","xml:/Order/@MultipleStatusesExist"),getValue("Order","xml:/Order/@MaxOrderStatusDesc"),true)%></a>
            <% } %>
            <% if (equals("Y", getValue("Order", "xml:/Order/@HoldFlag"))) { %>

	            <% if (isVoid(modify) || isTrue("xml:/Rules/@RuleSetValue")) {%>
					<img onmouseover="this.style.cursor='default'" class="columnicon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_is_held")%>>
				<%	}	else	{	%>
					<a <%=getDetailHrefOptions("L05", getParameter("orderKey"), "")%>><img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_is_held\nclick_to_add/remove_hold")%>></a>
				<%	}	%>

            <% } %>
            <% if (equals("Y", getValue("Order", "xml:/Order/@SaleVoided"))) { %>
                <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.SALE_VOIDED, "This_sale_is_voided")%>/>
            <% } %>
            <% if (equals("Y", getValue("Order","xml:/Order/@isHistory") )){ %>
                <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.HISTORY_ORDER, "This_is_an_archived_order")%>/>
            <% } %>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_Created_By</yfc:i18n></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:/Order/@EnteredBy" /></td>
		<%
			String createdBy = resolveValue("xml:/Order/@EnteredBy");
			if (!isVoid(createdBy)) {
			%>
				<yfc:callAPI apiID="A50"/>
			<%
			}
		%>
		<td class="detaillabel"><yfc:i18n>TD_Email</yfc:i18n></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:CreatedBy:/UserList/User/ContactPersonInfo/@EMailID" /></td>
		<td class="detaillabel"><yfc:i18n>Phone</yfc:i18n></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:CreatedBy:/UserList/User/ContactPersonInfo/@DayPhone"/></td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_Created_Date</yfc:i18n></td>
		<td class="protectedtext">
			<yfc:getXMLValue binding="xml:/Order/@OrderDate"/>
			<input type="hidden" <%=getTextOptions("xml:/Order/@OrderDate")%> />
			<input type="hidden" <%=getTextOptions("xml:/Order/@ReceivingNode")%> />
		</td>
		
		<!-- For Cost Center -->
		<td class="detaillabel"><yfc:i18n>TD_Cost_Center</yfc:i18n></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:/Order/Extn/@CostCenter" /></td>
		
		<td class="detaillabel"><yfc:i18n>TD_Attn_To</yfc:i18n></td>
		<td><input type="text" size="30" maxlength="64" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@ContactAttnTo")%>/></td>
		<td class="detaillabel"><yfc:i18n>Currency</yfc:i18n></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:/Order/PriceInfo/@Currency"/></td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_Customer_Reference</yfc:i18n></td>
		<td class="protectedtext"><input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@CustomerPONo")%>/></td>
		<td class="detaillabel"><yfc:i18n>Name</yfc:i18n></td>
		<td><input type="text" size="30" maxlength="64" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@ContactName")%> /></td>
		<td class="detaillabel"><yfc:i18n>Phone</yfc:i18n></td>
		<td><input type="text" size="30" maxlength="40" class="unprotectedinput"  <%=getTextOptions("xml:/Order/Extn/@ContactPhone")%> /></td>
		<td class="detaillabel"><yfc:i18n>TD_Email</yfc:i18n></td>
		<td><input type="text" size="30" maxlength="150" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@ContactEMailID")%> /></td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_Special_Customer_Instructions</yfc:i18n></td>
		<td><textarea class="unprotectedtextareainput" rows="4" cols="75" <%=getTextOptions("xml:/Order/Extn/@SplCustomerInstructions")%>><yfc:getXMLValue binding="xml:/Order/Extn/@SplCustomerInstructions"/></textarea></td>
		<td class="detaillabel"><yfc:i18n>TD_Billed_By_Oracle</yfc:i18n></td>
		<td><input type="checkbox" <%=getCheckBoxOptions("xml:/Order/Extn/@BilledByOracle", "xml:/Order/Extn/@BilledByOracle", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' /></td>
	</tr>	
</table>

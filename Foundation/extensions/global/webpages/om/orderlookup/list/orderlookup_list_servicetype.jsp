<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script>
function setOrderLookupValue(value)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = value;
			
		}
		window.close();
	}
</script>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td class="lookupiconheader"><br /></td>
        <td class="tablecolumnheader">
            <yfc:i18n>Order_No</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Service_Type_Code</yfc:i18n>
        </td>
        
   </tr>
</thead>
    <tbody>
        <yfc:loopXML binding="xml:/OrderList/@Order" id="Order">
		        <yfc:loopXML binding="xml:/Order/OrderLines/@OrderLine" id="OrderLine">

            <tr>
				<td class="tablecolumn">
					<img class="icon" onClick="setOrderLookupValue(this.value)"  value="<%=resolveValue("xml:/OrderLine/Extn/@ServiceType")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
				</td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Order/@OrderNo"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ServiceType"/>
                </td>
			</tr>
		</yfc:loopXML>
		</yfc:loopXML>
</tbody>
</table>
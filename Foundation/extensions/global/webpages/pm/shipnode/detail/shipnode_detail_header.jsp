<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<%
	String interfaceType = getValue("ShipNodeList","xml:/ShipNodeList/ShipNode/@InterfaceType");
%>

<table class="view" width="100%">  
<tr>
    <td class="detaillabel" ><yfc:i18n>Ship_Node</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeList/ShipNode/@ShipNode"></yfc:getXMLValue>&nbsp;</td>
    <td class="detaillabel" ><yfc:i18n>Description</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeList/ShipNode/@Description"></yfc:getXMLValue>&nbsp;</td>
    <td class="detaillabel" ><yfc:i18n>Interface</yfc:i18n></td>
    <td class="protectedtext"><%=displayShipNodeInterfaceType(interfaceType)%></td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Parent_Organization</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeList/ShipNode/OwnerOrganization/@OrganizationCode"></yfc:getXMLValue>&nbsp;</td>
    <td class="detaillabel" ><yfc:i18n>Parent_Organization_Name</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeList/ShipNode/OwnerOrganization/@OrganizationName"></yfc:getXMLValue>&nbsp;</td>
    <td class="detaillabel" ><yfc:i18n>Identified_By_Parent_As</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeList/ShipNode/@IdentifiedByParentAs"></yfc:getXMLValue>&nbsp;</td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>GLN</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeList/ShipNode/@GLN"></yfc:getXMLValue>&nbsp;</td>
</tr>
</table>


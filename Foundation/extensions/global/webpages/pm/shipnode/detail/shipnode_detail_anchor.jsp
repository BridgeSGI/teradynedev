<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<table class="anchor" cellpadding="7px" cellSpacing="0">
<tr>
    <td colspan="2" >
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I01"/>
        </jsp:include>
    </td>
</tr>
<tr>
    <td height="100%" width="50%" addressip="true" >
        <jsp:include page="/yfc/innerpanel.jsp" flush="true">
            <jsp:param name="CurrentInnerPanelID" value="I02"/>
            <jsp:param name="Path" value="xml:/ShipNodeList/ShipNode/ShipNodePersonInfo"/>
            <jsp:param name="DataXML" value="ShipNodeList"/>
            <jsp:param name="AllowedModValue" value='N'/>
        </jsp:include>
    </td>
    <td height="100%" width="50%" addressip="true" >
        <jsp:include page="/yfc/innerpanel.jsp" flush="true">
            <jsp:param name="CurrentInnerPanelID" value="I03"/>
            <jsp:param name="Path" value="xml:/ShipNodeList/ShipNode/ContactPersonInfo"/>
            <jsp:param name="DataXML" value="ShipNodeList"/>
            <jsp:param name="AllowedModValue" value='N'/>
        </jsp:include>
    </td>
</tr>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript">
function setLookupValue(value)

	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = value;					
		}
		window.close();
	}
	
</script>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td class="lookupiconheader"><br /></td>      
        <td class="tablecolumnheader">
            <yfc:i18n>Address_Line_1</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Address_Line_2</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>City</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>State</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Postal_Code</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Country</yfc:i18n>
        </td>
		<td class="tablecolumnheader">
            <yfc:i18n>Org_ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader">
            <yfc:i18n>Organization_Name</yfc:i18n>
        </td>
		<td class="tablecolumnheader">
            <yfc:i18n>Customer_Site_Status</yfc:i18n>
        </td>
   </tr>
</thead>
    <tbody>
        <yfc:loopXML binding="xml:/PersonInfoList/@PersonInfo" id="PersonInfo">
            <tr>
				<td class="tablecolumn">
					<img class="icon" onClick="setLookupValue(this.value)"  value="<%=resolveValue("xml:/PersonInfo/@PersonInfoKey")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
				</td>               
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@AddressLine1"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@AddressLine2"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@City"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@State"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@ZipCode"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@Country"/>
                </td>
				
				<yfc:loopXML binding="xml:/OrganizationList/@Organization" id="Organization">
				<%
					String PersonInfoNodeKey = resolveValue("xml:/PersonInfo/@PersonInfoKey");
					String CorporatePersonInfoKey = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@PersonInfoKey");
					
						if(PersonInfoNodeKey.equals(CorporatePersonInfoKey)){
				%>
				<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/@OrganizationCode"/>
                </td>
				<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/@OrganizationName"/>
                </td>
				<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/Extn/@CustomerSiteStatus"/>
                </td>
				<%
				}
				%>								
				</yfc:loopXML>
				</tr>
		</yfc:loopXML>
</tbody>
</table>
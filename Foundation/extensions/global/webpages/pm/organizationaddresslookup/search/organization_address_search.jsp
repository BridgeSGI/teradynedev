<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<table class="view">

<tr>
    <td class="searchlabel" >
        <yfc:i18n>Address_Line_1</yfc:i18n>
    </td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell">
        <select name="xml:/PersonInfo/@AddressLine1QryType" class="combobox">
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
            value="QueryType" selected="xml:/PersonInfo/@AddressLine1QryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/PersonInfo/@AddressLine1")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" >
        <yfc:i18n>Address_Line_2</yfc:i18n>
    </td>
</tr>
<tr>
    <td>
        <select name="xml:/PersonInfo/@AddressLine1QryType" class="combobox">
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/PersonInfo/@AddressLine1QryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/PersonInfo/@AddressLine2")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" >
        <yfc:i18n>City</yfc:i18n>
    </td>
</tr>
<tr>
    <td>
        <select name="xml:/PersonInfo/@CityQryType" class="combobox">
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/PersonInfo/@CityQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/PersonInfo/@City")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" >
        <yfc:i18n>State</yfc:i18n>
    </td>
</tr>
<tr>
    <td>
        <select name="xml:/PersonInfo/@StateQryType" class="combobox">
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/PersonInfo/@StateQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/PersonInfo/@State")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" >
        <yfc:i18n>Postal_Code</yfc:i18n>
    </td>
</tr>
<tr>
    <td>
        <select name="xml:/PersonInfo/@ZipCodeQryType" class="combobox">
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/PersonInfo/@ZipCodeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/PersonInfo/@ZipCode")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" >
        <yfc:i18n>Country</yfc:i18n>
    </td>
</tr>
<tr>
    <td>
        <select name="xml:/PersonInfo/@CountryQryType" class="combobox">
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/PersonInfo/@CountryQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/PersonInfo/@Country", getLocale().getCountry())%>/>
    </td>
</tr>
</table>

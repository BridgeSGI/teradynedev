<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<table class="table" width="100%" editable="false">
<thead>
   <tr> 
        <td class="lookupiconheader" sortable="no"><br /></td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/@ItemID")%>">
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
        
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/@UnitOfMeasure")%>">
            <yfc:i18n>UOM</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/Extn/@ICategory")%>">
            <yfc:i18n>Category</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/Extn/@SubCategory")%>">
            <yfc:i18n>Sub_Category</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/@Status")%>">
            <yfc:i18n>Product_Status</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/@GlobalItemID")%>">
            <yfc:i18n>GTIN</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/PrimaryInformation/@ShortDescription")%>">
            <yfc:i18n>Description</yfc:i18n>
        </td>
   </tr>
</thead>
<tbody>
    <yfc:loopXML name="ItemList" binding="xml:/ItemList/@Item" id="item"> 
    <tr> 
        <td class="tablecolumn">
            <img class="icon" onclick="setItemLookupValue('<%=resolveValue("xml:item:/Item/@ItemID")%>','<%=resolveValue("xml:item:/Item/PrimaryInformation/@DefaultProductClass")%>','<%=resolveValue("xml:item:/Item/@UnitOfMeasure")%>')"  value="<%=resolveValue("xml:item:/Item/@ItemID")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/@ItemID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/@UnitOfMeasure"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/Extn/@Category"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/Extn/@SubCategory"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/@Status"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/@GlobalItemID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/PrimaryInformation/@Description"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
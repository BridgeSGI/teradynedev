<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table class="view">

<%
	String isReturnService = HTMLEncode.htmlEscape(getParameter("IsReturnService"));
	if (isVoid(isReturnService)) {
		isReturnService = resolveValue("xml:/Item/PrimaryInformation/@IsReturnService");
	}
	if (isVoid(isReturnService)) {
		isReturnService = "N";
	}

	String canUseAsServiceTool = HTMLEncode.htmlEscape(getParameter("CanUseAsServiceTool"));
	if (isVoid(canUseAsServiceTool)) {
		canUseAsServiceTool = resolveValue("xml:/Item/@CanUseAsServiceTool");
	}

	String itemGroupCode = HTMLEncode.htmlEscape(getParameter("ItemGroupCode"));
	if (isVoid(itemGroupCode)) {
		itemGroupCode = resolveValue("xml:/Item/@ItemGroupCode");
	}
	if (isVoid(itemGroupCode)) {
		itemGroupCode = "PROD";
	}
	YFCElement itemGroupCodeElem = YFCDocument.createDocument("ItemGroupCode").getDocumentElement();
	request.setAttribute("ItemGroupCode", itemGroupCodeElem);
	itemGroupCodeElem.setAttribute("ItemGroupCode", itemGroupCode);

    String callingOrgCode = getValue("Item", "xml:/Item/@CallingOrganizationCode");
	if(isVoid(callingOrgCode)){
		callingOrgCode = getValue("CurrentOrganization", getSelectedOrgCodeValue("xml:/Item/@CallingOrganizationCode"));
	}
			
%>

<script language="javascript">
	window.dialogArguments.parentWindow.defaultOrganizationCode = "<%=HTMLEncode.htmlEscape(callingOrgCode)%>";
</script>

<tr>
	<td>
		<input type="hidden" name="xml:/Item/PrimaryInformation/@IsReturnService" value='<%=isReturnService%>'/>
		<input type="hidden" name="xml:/Item/@ItemGroupCode" value='<%=itemGroupCode%>'/>
		<input type="hidden" name="xml:/Item/@CanUseAsServiceTool" value='<%=canUseAsServiceTool%>'/>
	</td>
</tr>

<tr>
    <td class="searchlabel" ><yfc:i18n>Organization</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
		<input type="text" class="protectedinput" contenteditable="false" <%=getTextOptions("xml:/Item/@CallingOrganizationCode")%>/>
    </td>
</tr>

    <% // Now call the APIs that are dependent on the calling organization code %>
	<yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP3"/>
<tr>
    <td class="searchlabel" ><yfc:i18n>Catalog_ID</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Item/@MasterCatalogIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Item/@MasterCatalogIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Item/@MasterCatalogID") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Item/@ItemIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Item/@ItemIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Item/@ItemID") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Department</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Item/Extn/@DepartmentQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Item/Extn/@DepartmentQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Item/Extn/@Department") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Category</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Item/Extn/@CategoryQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Item/Extn/@CategoryQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Item/Extn/@Category") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Sub_Category</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Item/Extn/@SubCategoryQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Item/Extn/@SubCategoryQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Item/Extn/@SubCategory") %> />
    </td>
</tr>


<tr>
    <td class="searchlabel" ><yfc:i18n>Product_Status</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Item/@StatusQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Item/@StatusQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Item/@Status") %> />
    </td>
</tr>
<%	if (equals(itemGroupCode,"PROD")) { %>
<tr>
    <td class="searchlabel" ><yfc:i18n>Global_Item_Id</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Item/@GlobalItemIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Item/@GlobalItemIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Item/@GlobalItemID") %> />            
    </td>
</tr>
<% } %>
</table>
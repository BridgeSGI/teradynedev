<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script>
function setCustomerLookupValue(value)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = value;
			
		}
		window.close();
	}
</script>
<%
	String sLookupAttr = request.getParameter("LookupAttribute");
%>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td class="lookupiconheader"><br /></td>
        <td class="tablecolumnheader">
            <yfc:i18n>Org_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Organization_Name</yfc:i18n>
        </td>		
		<td class="tablecolumnheader">
            <yfc:i18n>Customer_Site_Status</yfc:i18n>
        </td>			
        
   </tr>
</thead>
    <tbody>
        <yfc:loopXML binding="xml:/OrganizationList/@Organization" id="Organization">		
            <tr>
				<td class="tablecolumn">
					<img class="icon" onClick="setCustomerLookupValue(this.value)"  value="<%=resolveValue("xml:/Organization/@" + sLookupAttr)%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
				</td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/@OrganizationCode"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/@OrganizationName"/>
                </td>																											
				<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/Extn/@CustomerSiteStatus"/>
                </td>
			</tr>
		</yfc:loopXML>
</tbody>
</table>
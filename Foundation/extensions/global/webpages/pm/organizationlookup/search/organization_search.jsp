<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%
//Get the document type that the lookup is for
String sDocType;
	sDocType = request.getParameter("DocType");

%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script>
function showCustomerLookupPopup(value, entityname, extraParams)
	{
		//BuyerOrganizationCode attribute of <Order /> is used as the customer #
		var oObj = new Object();
		oObj.field1 = document.all(value);
		var extraParams = "LookupAttribute=" +extraParams;
		
		

		yfcShowSearchPopupWithParams('TDorglookupS02','customerlookup',900,550,oObj,entityname, extraParams);
		
	}
function showOfficeRecordLookupPopup(Value, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(Value);
		//Pass TerOfficeRecords attribute to the lookup screen
		var extraParams = "LookupAttribute=" + extraParams;

		
		yfcShowSearchPopupWithParams('TDofficeS020','officelookup',900,550,oObj,entityname, extraParams);
		
	}
	
	function setByAddressSearchCriteria(){
    
    var radAddress = document.all("RadAddress");
    var billToKey = document.all("xml:/Order/SearchByAddress/@BillToKey");
    var shipToKey = document.all("xml:/Order/SearchByAddress/@ShipToKey");
    var personInfoKey = document.all("xml:/PersonInfo/@PersonInfoKey");
	var addressValue = "B";
	if(radAddress[1].checked)
		addressValue = "S";
	if(radAddress[2].checked)
		addressValue = "E";

	billToKey.value = "";
	shipToKey.value = "";
	if(personInfoKey.value != null){
		if (addressValue == "B" || addressValue == "E") {
			billToKey.value = personInfoKey.value;
		}
		if (addressValue == "S" || addressValue == "E") {
			shipToKey.value = personInfoKey.value;
		}
	}
}

function refreshSearchScreen(controlObj) {
    if (yfcHasControlChanged(controlObj)) {
        changeSearchView(getCurrentSearchViewId());
	}
}
</script>
<%
//processing for the address lookup and address keys
	
	String billToKey = resolveValue("xml:/Order/SearchByAddress/@BillToKey");
	String shipToKey = resolveValue("xml:/Order/SearchByAddress/@ShipToKey");
	String radAddress = "";

	String personInfoKey ="";
	if(!isVoid(billToKey) && !isVoid(shipToKey)){
		radAddress = "E";
		personInfoKey = billToKey; 	
	}
	else if(!isVoid(billToKey)){
		radAddress = "B";
		personInfoKey = billToKey; 	
	}
	else if(!isVoid(shipToKey)){
		radAddress = "S";
		personInfoKey = shipToKey; 	
	}

	//set the address radiobuttons
	if(isVoid(radAddress)){
		radAddress = "E";
	}
	
	YFCElement personInfoElement = (YFCElement)request.getAttribute("PersonInfo");
	if(personInfoElement == null){
		personInfoElement = YFCDocument.createDocument("PersonInfo").getDocumentElement();
		request.setAttribute("PersonInfo",personInfoElement);
	}
	personInfoElement.setAttribute("PersonInfoKey", personInfoKey);
	
	//setup date variable for searching between dates
	YFCDate oEndDate = new YFCDate(); 
	oEndDate.setEndOfDay();
	
	//Setup default Organization Role
	String DefaultRole = "Node";
	String DefaultNodeType = "Customer-Site";
	
	

%>
<table class="view">
	<tr>
		<td>
			<input type="hidden" name="DocType" value="<%=HTMLEncode.htmlEscape(sDocType)%>"/>
			<input type="hidden" name="xml:/Order/@DocumentType" value="<%=sDocType%>"/>
			<input type="hidden" <%=getTextOptions("xml:/PersonInfo/@PersonInfoKey", personInfoKey)%>/>
			<input type="hidden" name="xml:/Order/SearchByAddress/@BillToKey" value="<%=billToKey%>"/>
            <input type="hidden" name="xml:/Order/SearchByAddress/@ShipToKey" value="<%=shipToKey%>"/>
			<input type="hidden" name="xml:/Organization/CorporatePersonInfo/@PersonInfoKey" value="<%=personInfoKey%>"/>
			<input type="hidden" name="xml:/Organization/OrgRoleList/OrgRole/@RoleKey" value="<%=DefaultRole%>"/>
			<input type="hidden" name="xml:/Organization/Node/@NodeType" value="<%=DefaultNodeType%>"/>
		</td>
	</tr>
		 <% 
		if(!isVoid(personInfoKey)){
	%>
			<yfc:callAPI apiID="AP3"/>
    <%
		}
    %>
	<tr>
        <td class="searchlabel" ><yfc:i18n>Org_Role</yfc:i18n></td>
		<td>
			<%=DefaultRole%>
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" ><yfc:i18n>Node_Type</yfc:i18n></td>
		<td>
			<%=DefaultNodeType%>
        </td>
    </tr>
  
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Customer_Site_Org_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Organization/@OrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Organization/@OrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationCode")%>/>
			<img class="lookupicon" name="search" 
			onclick="showCustomerLookupPopup('xml:/Organization/@OrganizationCode', 'TDorglookup', 'OrganizationCode') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_#") %> />
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Customer_Site_Org_Name</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Organization/@OrganizationNameQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Organization/@OrganizationNameQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationName")%>/>
			<img class="lookupicon" name="search" 
			onclick="showCustomerLookupPopup('xml:/Organization/@OrganizationName', 'TDorglookup', 'OrganizationName') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_#") %> />
        </td>
    </tr>
	
	<tr>
		<td class="searchlabel">
			<fieldset>
				<legend class="searchlabel"><yfc:i18n>Address</yfc:i18n>
					<img class="lookupicon" onclick="callAddressLookup('xml:/PersonInfo/@PersonInfoKey','TERaddresslookup','');setByAddressSearchCriteria();refreshSearchScreen(document.all('xml:/PersonInfo/@PersonInfoKey'))" <%=getImageOptions(YFSUIBackendConsts.ADDRESS_DETAILS, "Search_for_Address") %> />
				</legend>
			<span class="protectedtext">
			
			<%			
			if(!isVoid(personInfoKey)){
			%>
			<% //display the address details %>
				<jsp:include page="/yfsjspcommon/address.jsp" flush="true">
					<jsp:param name="Path" value="xml:/PersonInfoList/PersonInfo"/>
					<jsp:param name="DataXML" value="PersonInfoList"/>
		        </jsp:include>
			</span>
			<% }else{ %>
			<br/>
			<%}%>
			<br/>
            <input type="radio" onclick="setByAddressSearchCriteria()" <%=getRadioOptions("RadAddress", radAddress, "B")%>>
                <yfc:i18n>Bill_To</yfc:i18n>
            </input>
            <input type="radio" onclick="setByAddressSearchCriteria()" <%=getRadioOptions("RadAddress", radAddress, "S")%>>
                <yfc:i18n>Ship_To</yfc:i18n>
            </input>
            <input type="radio" onclick="setByAddressSearchCriteria()" <%=getRadioOptions("RadAddress", radAddress, "E")%>>
                <yfc:i18n>Either</yfc:i18n>
            </input>
			</fieldset>
		</td>
	</tr>
	
		<tr>
<td class="searchlabel" ><yfc:i18n>Teradyne_Service_Office</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">        
        <input type="text" class="unprotectedinput" 
		<%=getTextOptions("xml:/Organization/Extn/@TeradyneServiceOffice") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerOfficeCode', 'TERoffice', 'TerOfficeCode') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Office_Code") %> />
    </td>
</tr>
		<tr>
<td class="searchlabel" ><yfc:i18n>Customer_Site_Type</yfc:i18n></td>
    <td class="searchcriteriacell">
            <select name="xml:/Organization/Extn/@CustomerSiteType" class="combobox">
                <yfc:loopOptions binding="xml:CustomerSiteTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/Organization/Extn/@CustomerSiteType" />
            </select>
        </td>
</tr>

<tr>
<td class="searchlabel" ><yfc:i18n>Customer_Site_Status</yfc:i18n></td>
    <td class="searchcriteriacell">
            <select name="xml:/Organization/Extn/@CustomerSiteStatus" class="combobox">
                <yfc:loopOptions binding="xml:CustomerSiteStatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/Organization/Extn/@CustomerSiteStatus" />
            </select>
        </td>
</tr>
		
	</tr>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>

<input type="hidden" name="xml:/ShipNode/@NodeType" value="LSC" />
<table class="view">
<tr>
    <td class="searchlabel" ><yfc:i18n>Participates_In_Enterprise</yfc:i18n></td>
</tr>
<tr>
   <td nowrap="true" class="searchcriteriacell">
	   <select name="xml:/ShipNode/Organization/EnterpriseOrgList/OrgEnterprise/@EnterpriseOrganizationKeyQryType" class="combobox" >
           <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc" value="QueryType" selected="xml:/ShipNode/Organization/EnterpriseOrgList/OrgEnterprise/@EnterpriseOrganizationKeyQryType"/>
       </select>
       <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNode/Organization/EnterpriseOrgList/OrgEnterprise/@EnterpriseOrganizationKey","xml:/ShipNode/Organization/EnterpriseOrgList/OrgEnterprise/@EnterpriseOrganizationKey")%>/>
      <img class="lookupicon" onclick="callLookup(this,'organization','lookupForSingleRole=ENTERPRISE')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization") %> />
   </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Node</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell">
        <select name="xml:/ShipNode/@ShipNodeQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/ShipNode/@ShipNodeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNode/@ShipNode") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>GLN</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell">
        <select name="xml:/ShipNode/@GLNQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/ShipNode/@GLNQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNode/@GLN") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Parent_Organization</yfc:i18n></td>
</tr>
<tr>
   <td nowrap="true" class="searchcriteriacell">
       <select name="xml:/ShipNode/Organization/@ParentOrganizationCodeQryType" class="combobox" >
           <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
               name="QueryTypeDesc" value="QueryType" selected="xml:/ShipNode/Organization/@ParentOrganizationCodeQryType"/>
       </select>
       <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNode/Organization/@ParentOrganizationCode", "xml:/ShipNode/Organization/@ParentOrganizationCode")%>/>
        <img class="lookupicon" onclick="callLookup(this,'organization')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization") %> />
   </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Identified_By_Parent_As</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell">
        <select name="xml:/ShipNode/@IdentifiedByParentAsQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/ShipNode/@IdentifiedByParentAsQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNode/@IdentifiedByParentAs") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Description</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell">
        <select name="xml:/ShipNode/@DescriptionQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/ShipNode/@DescriptionQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNode/@Description") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" >
        <yfc:i18n>City</yfc:i18n>
    </td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell">
        <select name="xml:/ShipNode/ShipNodePersonInfo/@CityQryType" class="combobox">
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
            value="QueryType" selected="xml:/ShipNode/ShipNodePersonInfo/@CityQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNode/ShipNodePersonInfo/@City")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" >
        <yfc:i18n>State</yfc:i18n>
    </td>
</tr>
<tr>
    <td>
        <select name="xml:/ShipNode/ShipNodePersonInfo/@StateQryType" class="combobox">
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/ShipNode/ShipNodePersonInfo/@StateQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNode/ShipNodePersonInfo/@State")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" >
        <yfc:i18n>Postal_Code</yfc:i18n>
    </td>
</tr>
<tr>
    <td>
        <select name="xml:/ShipNode/ShipNodePersonInfo/@ZipCodeQryType" class="combobox">
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/ShipNode/ShipNodePersonInfo/@ZipCodeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNode/ShipNodePersonInfo/@ZipCode")%>/>
    </td>
</tr>
</table>

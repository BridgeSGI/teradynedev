<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js">
</script>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td class="lookupiconheader"><br /></td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeList/ShipNode/@ShipNode")%>">
            <yfc:i18n>Node</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeList/ShipNode/@Description")%>">
            <yfc:i18n>Description</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeList/ShipNode/@OwnerKey")%>">
            <yfc:i18n>Owned_By</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeList/ShipNode/@InterfaceType")%>">
            <yfc:i18n>Interface</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeList/ShipNode/@ActivateFlag")%>">
            <yfc:i18n>Active</yfc:i18n>
        </td>
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/ShipNodeList/@ShipNode" id="ShipNode"> 
    
    <% String interfaceType = resolveValue("xml:/ShipNode/@InterfaceType"); %>
    <tr> 
        <td class="tablecolumn">
            <img class="icon" onClick="setLookupValue(this.value)"  value="<%=resolveValue("xml:/ShipNode/@ShipNode")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@ShipNode"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@Description"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@OwnerKey"/></td>
        <td class="tablecolumn"><%=displayShipNodeInterfaceType(interfaceType)%></td>
        <td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@ActivateFlag"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
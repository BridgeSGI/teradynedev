scRegisterAjax("checkOrgName1","/extn/ib/ajax/getCustOrgName.jsp?requestNo="+new Date(),"PreFunction","PostFunction");
scRegisterAjax("checkOrgName2","/extn/ib/ajax/getCustSellerName.jsp?requestNo="+new Date(),"PreFunction","PostFunction");
scRegisterAjax("checkOrgName3","/extn/ib/ajax/getOrgName.jsp?requestNo="+new Date(),"PreFunction","PostFunction");
var Hand="";
var enterpriseCode="";
function PreFunction(){
	var ob = new Object();
	ob.enterpriseCode = enterpriseCode;
	return ob;
}

function callAjaxFunctionOrgChange(handle,role){
	enterpriseCode = document.all("xml:/Order/@EnterpriseCode").value;
	if(document.getElementById(handle).value.length>0){
		Hand=handle;
		var srcElem = document.all(handle);
		if(role==="BUYER"){
			scCallAjax('checkOrgName1',srcElem);
		}else if(role==="SELLER"){
			scCallAjax('checkOrgName2',srcElem);
		}else if(role==="NODE"){
			scCallAjax('checkOrgName3',srcElem);
		}
	}else{
		return false;
	}
}
	
function PostFunction(ajaxResponse){
	if(!ajaxResponse.hasChildNodes()){
		document.getElementById(Hand).focus();
		document.getElementById(Hand).value="";
		alert("Invalid_Organization");
		return false;
	}else if(ajaxResponse.hasChildNodes()){
		var OrganizationName= ajaxResponse.getAttribute("orgName");
		var defaultCurrency = ajaxResponse.getAttribute("currency");
		document.getElementById('tdCurrency').value= defaultCurrency;
		document.getElementById(Hand+"Name").value=OrganizationName;
	}
}

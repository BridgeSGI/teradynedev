scRegisterAjax("getwarranties","/extn/om/order/ajax/getWarranties.jsp?requestNo="+new Date(),"preFnWarranty","postFnWarranty");

//scope in File
var sCounter = "";
var sElemId = "";
var waranty = true;

function getwarranty(elemId,entity,field1,field2,field3,recvNode,program){
	
	waranty = false;
	sElemId = elemId;
	var temp = document.getElementById(elemId);
	if((temp != null) && (temp.value == temp.oldValue)){
		return;
	}else{
		callLookupForSerialNo(elemId,entity,field1,field2,field3,recvNode,program);
	}
	
	setCounter(elemId);
	var serialNoField = document.getElementById(elemId);
	
	var select = document.all("xml:/Order/OrderLines/OrderLine_"+sCounter+"/Extn/@AgreementNo");
	if(serialNoField != null && preValidateSerialNoField(elemId)){
		//var srcElem = event.srcElement;
		scCallAjax('getwarranties',serialNoField);
	}	
}

function preValidateSerialNoField(elemId){
	var serialNoEle = document.getElementById(elemId);
	if(serialNoEle != null && serialNoEle.value != null && serialNoEle.value != ""){
		serialNoEle.oldValue = serialNoEle.value;
		return true;		
	}else{
		return false;
	}
}

function preFnWarranty(){
	var selectEle = document.all("xml:/Order/OrderLines/OrderLine_"+sCounter+"/Extn/@AgreementNo");
	removeOptions(selectEle);	
	
	var ob = new Object();
	var sOrgCode = document.all("xml:/Order/@EnterpriseCode").value;
	
	ob.orgCode = sOrgCode;
	ob.uom = sUOM;
	
	return ob;
}

function postFnWarranty(ajaxResponse){
	var i;
	if(ajaxResponse.getAttribute("Error") != null){
		alert("Invalid System Serial No");
		var elem = document.getElementById(sElemId);
		if(elem != null){
			elem.focus();
			elem.value="";
		}
	}
		
	var list = ajaxResponse.getElementsByTagName("ServiceContract");
	
	var selectEle = document.all("xml:/Order/OrderLines/OrderLine_"+sCounter+"/Extn/@AgreementNo");
	removeOptions(selectEle);
	for(i=0;i<list.length;i++){
		var ele = list.item(i);
		var agreementNo = ele.getAttribute("AgreementNo");
		//selectEle.options[selectEle.options.length] = new Option(codeValue,codeValue);
		selectEle.options[selectEle.options.length] = new Option(agreementNo,agreementNo);
	}
	selectEle.options[selectEle.options.length] = new Option(' ',' ');
}

function removeOptions(selectEle){
	var i;
	for(i=selectEle.options.length-1;i>=0;i--){
		selectEle.remove(i);
	}
}

function setCounter(elemId){
	var arrID = elemId.split("_");
	sCounter = arrID[1];
}

function callLookupForSerialNo(elemId,entity,field1,field2,field3,recvNode,program){
	/*var id = serialNoEle.id+"_img";
	var imgEle = document.getElementById(id);
	if(imgEle != null){
		imgEle.click();
	}*/
	callLookupforserial(elemId,entity,field1,field2,field3,recvNode,program);
}
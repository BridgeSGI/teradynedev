function callLookupForCust(obj,enterpriseCode)
{
//get values for Enterprise and DocType


//callLookup
callLookup(obj,'CUSorganization',"&xml:/Customer/@OrganizationCode="+enterpriseCode);

}


function callLookupForCustOrgID(obj,enterpriseCode)
{
//get values for Enterprise and DocType


//callLookup
callLookup(obj,'CUSorganization',"&xml:/Customer/@OrganizationCode="+enterpriseCode);

}

function callItemLookup1(sItemID,sProductClass,sUOM,entityname,sitemType,extraParams)
{
	
var oItemID = document.all(sItemID);
var oProductClass = document.all(sProductClass);
var oUOM = document.all(sUOM);


if(extraParams == null) {
	
showItemLookupPopup(oItemID, oProductClass, oUOM, entityname);
}
else{

extraParams=extraParams +
"&xml:/Item/PrimaryInformation/@ItemType="+sitemType;

showItemLookupPopup(oItemID, oProductClass, oUOM, entityname,extraParams);
}
}


function show() {
	document.getElementById('program').style.display = 'block';
document.getElementById('customer').style.display = 'none';

document.getElementById('customer1').value="";
document.getElementById('system').style.display = 'none';
document.getElementById('system1').style.display = 'none';
document.getElementById('system2').value="";


}
function hide() { document.getElementById('customer').style.display = 'block';
document.getElementById('program').style.display = 'none';
document.getElementById('system').style.display = 'block';
document.getElementById('system1').style.display = 'block';

document.getElementById('program').value="";

}


function validateIP(sServiceType,iDays)

	
{
 var sServiceType = document.all(sServiceType).value;
 var iDays = document.all(iDays).value;
 


if(sServiceType.length<=0)
{
alert('Service Type Offering Item ID cannot be blank');

}
if(isNaN(iDays)||iDays.length<=0)
{
alert('Service Days:Not a Number');




}

if(sServiceType.length<=0||isNaN(iDays)||iDays.length<=0)
{

return false;
}
else
	{

	document.body.style.cursor='auto';
    return true;
}
}

function validateCreate(sService,iBus,sCustNo,sProg)

	
{
	
 var sService = document.all(sService).value;
 var iBus = document.all(iBus).value;
 var sCustNo=document.all(sCustNo).value;
 var sProg=document.all(sProg).value;

if(sService.length<=0)
{
alert('Service Type Offering Item ID cannot be blank');
return false;
}
if(isNaN(iBus)||iBus.length<=0)
{
alert('Service Days:Not a Number');

return false;


}


if(sCustNo.length<=0&&sProg.length<=0)
	{
//document.getElementById('customer').style.border = "solid 1px red";
	alert('Customer Site ORG ID or Program required');

return false;
}
else
	{

	document.body.style.cursor='auto';
    return true;
}
}





	function setCustLookupValue(sOrgID,sOrgName,sOrgAdd)
	{
		
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			
		
			Obj.field1.value = sOrgID;
			if(Obj.field2 != null)
			{
				Obj.field2.value = sOrgName;
			}

			
		}
		
		window.close();
	}

	function callLookupForSBOrg(sOrgId,sOrgName,role,entBinding,docTypeBinding)
	{
	
		//get values for Enterprise and DocType
		var oEnterprise = document.all(entBinding);
		var oDocType = document.all(docTypeBinding);
		var oOrgId = document.all(sOrgId);
		var oOrgName = document.all(sOrgName);
		
		var enterprise = null;
		var docType = null;
		var  entityname="INBorganization";
				if (oEnterprise != null) {
			 enterprise = oEnterprise.value;
		}
		else {
			 enterprise = entBinding;
		}

		if (oDocType != null) {
			docType = oDocType.value;
		}
		else {
			docType = docTypeBinding;
		}
		
		//form extraParams string
		var extraParams = "role=" + role + "&forOrder=Y&enterprise=" + enterprise + "&docType=" + docType;

		//callLookup
		if(extraParams == null) {
			
			showSBLookupPopup(oOrgId, oOrgName,entityname);
		}
		else{
			

			showSBLookupPopup(oOrgId, oOrgName,entityname, extraParams);
		}
		
	}



function showSBLookupPopup(orgIDInput, orgNameInput,entityname, extraParams)
	{

		var oObj = new Object();
		oObj.field1 = orgIDInput;
		oObj.field2 = orgNameInput;
		
		if(extraParams == null) {
			yfcShowSearchPopup('','',900,550,oObj,entityname);
		}
		else{
			
			yfcShowSearchPopupWithParams('','',900,550,oObj,entityname,extraParams);
		}
	}

	function setLookupValueajax(sVal)
	{
		
		var Obj = window.dialogArguments
		if(Obj != null)
			
			Obj.field1.value = sVal;
			Obj.field1.focus();
         
		window.close();

		
	}

function IBMandatoryParam(sVal1,sVal2,sVal3,sVal4,sVal5)
{
var var1=document.all(sVal1).value;
var var2=document.all(sVal2).value;
var var3=document.all(sVal3).value;
var var4=document.all(sVal4).value;
var var5=document.all(sVal5).value;
	if(var2.length<=0||var3.length<=0||var4.length<=0||var5.length<=0)
		{
	
	alert('Mandatory Parameters Missing');
	return false;
	
	}

else
	return true;



}

function verifyLinesForIB(keyName,state)
{

	var inputElems = document.getElementsByTagName("input"),
    count = 0;
for (var i=0; i<inputElems.length; i++)
	{

    if (inputElems[i].type === "checkbox" && inputElems[i].checked === true&&(inputElems[i].value!="Y"))
		{
		
        count++;
   }
	}

if(count<=1)
	{

var chkBoxArray = document.forms["containerform"].elements;
	 	for ( var i =0; i<chkBoxArray.length; i++ ) {
		
		if (chkBoxArray[i].name == keyName) {
			if (chkBoxArray[i].checked) {
			
			
			 if((chkBoxArray[i].actdate).length>0&&(chkBoxArray[i].statuss===state))
				 {
			 enterModificationReason('YMRD001','xml:/Order/@ModificationReasonCode','xml:/Order/@ModificationReasonText','xml:/Order/@Override');
			 return true;

			 }
				
				else alert('cannot perform this action');
					return false;
			}
		}
		}
}
		else 
			
			alert('Select one record to Process');
	
		return false;
	
	}

function verifyiblinedetail(Linestatus,actdate,state)
{

var oLineStatus = document.all(Linestatus).value;
var oactDate=document.all(actdate).value;

if((oactDate.length)>0&&(oLineStatus===state))
	{

enterModificationReason('YMRD002','xml:/Order/@ModificationReasonCode','xml:/Order/@ModificationReasonText','xml:/Order/@Override');
			 return true;

}


	else alert('cannot perform this action');
					return false;
}

function lockFocus()
{
	

var seller=document.getElementById('SellerId').value;

if((seller.length)>0)
	{

document.getElementById('SellerId').focus();
document.getElementById('SellerId').blur();
}
}


	function showhideeidt()
	{
if(document.getElementById("edit").style.display=="none")
	{



document.getElementById("edit").style.display="block";
document.getElementById("show").style.display="none";
	}
	else
		{
	document.getElementById("edit").style.display="none";
document.getElementById("show").style.display="block";

	}
			
    }

	function removeExtendedDocumentTypeShipment(){
		var selectEle = document.all("xml:/Shipment/@DocumentType");
		var i;
		for(i=0;i<selectEle.length;){
			if(selectEle.options[i].value == '0017.ex' || selectEle.options[i].value == '0018.ex' ){
				selectEle.remove(i);
			}else{
				i++;
			}
		}
	}

	function removeExtendedDocumentTypeShipmentLine(){
		var selectEle = document.all("xml:/ShipmentLine/Shipment/@DocumentType");
		var i;
		for(i=0;i<selectEle.length;){
			if(selectEle.options[i].value == '0017.ex' || selectEle.options[i].value == '0018.ex' ){
				selectEle.remove(i);
			}else{
				i++;
			}
		}
	}
	
	function removeExtendedDocumentTypeForSO(){
		
		var selectEle = document.all("xml:/Order/@DocumentType");
		var i;
		for(i=0;i<selectEle.length;){
			if(selectEle.options[i].value == '0017.ex' || selectEle.options[i].value == '0018.ex' ){
				selectEle.remove(i);
			}else{
				i++;
			}
		}
	}
	
	function removeExtendedDocumentTypeForSOLine(){
		
		var selectEle = document.all("xml:/OrderLine/Order/@DocumentType");
		var i;
		for(i=0;i<selectEle.length;){
			if(selectEle.options[i].value == '0017.ex' || selectEle.options[i].value == '0018.ex' ){
				selectEle.remove(i);
			}else{
				i++;
			}
		}
	}


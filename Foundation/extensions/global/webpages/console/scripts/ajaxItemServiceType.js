scRegisterAjax("getStatusCode","/extn/om/order/ajax/getStatusCode.jsp?requestNo="+new Date(),"preFnStatusCode","postFnStatusCode");

//scope in File
var sCounter = "";
var sUOM = "";
var sElemId = "";

var descEle = null;
var service = true;

function getItemService(obj,ItemID,pc,uom,entity,extraParams,id,descId){
	service =false;
	descEle = document.getElementById(descId);
	sElemId = id;
	var temp = document.getElementById(id);
	//alert(temp + "s1");
	if((temp != null) && (temp.value == temp.oldValue)){
		return;
	}else{
		callLookupForItem(obj,ItemID,pc,uom,entity,extraParams,id,descId);
	}
	
	setCounter(id);
	setUOM(sCounter);
	var itemField = document.getElementById(id);
	
	var select = document.all("xml:/Order/OrderLines/OrderLine_"+sCounter+"/Extn/@ServiceType");
	if(itemField != null && preValidateItemField(id)){
		//var srcElem = event.srcElement;
		scCallAjax('getStatusCode',itemField);
	}	
}

function preValidateItemField(elemId){
	var itemEle = document.getElementById(elemId);
	if(itemEle != null && itemEle.value != null && itemEle.value != ""){
		itemEle.oldValue = itemEle.value;
		return true;		
	}else{
		return false;
	}
}

function preFnStatusCode(){
	var selectEle = document.all("xml:/Order/OrderLines/OrderLine_"+sCounter+"/Extn/@ServiceType");
	removeOptions(selectEle);	
	descEle.value = "";
	
	var ob = new Object();
	var sOrgCode = document.all("xml:/Order/@EnterpriseCode").value;
	
	ob.orgCode = sOrgCode;
	ob.uom = sUOM;
	return ob;
}

function postFnStatusCode(ajaxResponse){	
	var i;
	if(ajaxResponse.getAttribute("Error") != null){
		alert("Invalid Item");
		var elem = document.getElementById(sElemId);
		if(elem != null){
			elem.focus();
			elem.value="";
		}
	}
	if(descEle != null){
		descEle.innerHTML = ajaxResponse.getAttribute("ItemDesc");
	}

	var list = ajaxResponse.getElementsByTagName("CommonCode");
	
	var selectEle = document.all("xml:/Order/OrderLines/OrderLine_"+sCounter+"/Extn/@ServiceType");
	removeOptions(selectEle);
	selectEle.options[selectEle.options.length] = new Option(' ',' ');
	for(i=0;i<list.length;i++){
		var ele = list.item(i);
		var codeValue = ele.getAttribute("CodeValue");
		//selectEle.options[selectEle.options.length] = new Option(codeValue,codeValue);
		selectEle.options[selectEle.options.length] = new Option(codeValue,codeValue);
	}
}

function removeOptions(selectEle){
	var i;
	for(i=selectEle.options.length-1;i>=0;i--){
		selectEle.remove(i);
	}
}
function setUOM(counter){
	var obUOM = document.all("xml:/Order/OrderLines/OrderLine_"+sCounter+"/OrderLineTranQuantity/@TransactionalUOM");
	if(obUOM != null){
		sUOM = ""+obUOM.value;
	}
}

function setCounter(elemId){
	var arrID = elemId.split("_");
	sCounter = arrID[1];
}

function callLookupForItem(obj,ItemID,pc,uom,entity,extraParams,id,descId){
	/*var id = ItemEle.id+"_img";
	var imgEle = document.getElementById(id);
	alert(id);
	alert(imgEle);
	if(imgEle != null){
		imgEle.click();
	}*/
	templateRowCallItemLookup_td(obj,ItemID,pc,uom,entity,extraParams,id,descId);
}
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/orgName.js"></script>
<script language="Javascript" >
   IgnoreChangeNames();
   yfcDoNotPromptForChanges(true);
</script>
<%
 
   String enterpriseCode = (String) request.getParameter("xml:/Order/@EnterpriseCode");
  

   if (isVoid(enterpriseCode)) {
       enterpriseCode = getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@PrimaryEnterpriseKey");
       request.setAttribute("xml:/Order/@EnterpriseCode", enterpriseCode);
   }
   
   // Default the seller to logged in organization if it plays a role of seller
   String sellerOrgCode = (String) request.getParameter("xml:/Order/@SellerOrganizationCode");
   if (isVoid(sellerOrgCode)) {
       if(isRoleDefaultingRequired((YFCElement) request.getAttribute("CurrentOrgRoleList"))){
           sellerOrgCode = getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@OrganizationCode");
           //System.out.println("org:" + orgCode);
           //request.setAttribute("xml:/Order/@SellerOrganizationCode", orgCode);
       }
   }
   
   //prepareMasterDataElements(enterpriseCode, (YFCElement) request.getAttribute("OrganizationList"),
   //                        (YFCElement) request.getAttribute("EnterpriseParticipationList"),
   //                        (YFCElement) request.getAttribute("CurrencyList"),
   //                        (YFCElement) request.getAttribute("OrderTypeList"),getValue("CurrentOrganization", "xml:CurrentOrganization:/Organization/@IsHubOrganization"));
   
   String exchangeOrderForReturn = resolveValue("xml:/ReturnOrder/@ReturnOrderHeaderKeyForExchange");
   String orderHeaderKeyVal = resolveValue("xml:/Order/@OrderHeaderKey");	
   %>
<script language="javascript">
   <% 
      if (!isVoid(orderHeaderKeyVal)) {	 
          YFCDocument orderDoc = YFCDocument.createDocument("Order");
          orderDoc.getDocumentElement().setAttribute("OrderHeaderKey",resolveValue("xml:/Order/@OrderHeaderKey"));
      
          // If this screen is shown as a popup, then open the order detail view for the new order
          // as a popup as well (instead of refreshing the same screen).
          if (equals(request.getParameter(YFCUIBackendConsts.YFC_IN_POPUP), "Y")) {
          %>
               function showOrderDetailPopup() {
   	window.CloseOnRefresh = "Y";
             callPopupWithEntity('INBorder', '<%=orderDoc.getDocumentElement().getString(false)%>');
   	window.close();
   }
               window.attachEvent("onload", showOrderDetailPopup);
           <%
      } else {
      %>
               function showOrderDetail() {
       showDetailFor('<%=orderDoc.getDocumentElement().getString(false)%>');
   }
               window.attachEvent("onload", showOrderDetail);
           <% }
      }
      
      %>
</script>
<%
   //exchange order processing
   boolean isExchangeOrderCreation = false;
   if(!isVoid(exchangeOrderForReturn)){
   	isExchangeOrderCreation = true;
   	//call getOrderDetails api for defaulting information onto exchange order.
   %>
<yfc:callAPI apiID="AP5"/>
<%	} %>
  <% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code) %>
   <yfc:callAPI apiID="AP1"/>
   <yfc:callAPI apiID="AP2"/>
   <yfc:callAPI apiID="AP4"/>
 


<table class="view" width="100%">

<tr>
<td>  <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
      <jsp:param name="EnterpriseCodeBinding" value="xml:/Order/@EnterpriseCode"/>
      <jsp:param name="ScreenType" value="detail"/>
      <jsp:param name="EnterpriseCodeLabel" value="Enterprise"/>
      <jsp:param name="ShowDocumentType" value="false"/>
      <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
      <jsp:param name="OrganizationListForInventory" value="true"/>
      </jsp:include><input type="hidden" name="xml:/Order/@DraftOrderFlag" value="Y"/>
         <input type="hidden" name="xml:/Order/@EnteredBy" value="<%=resolveValue("xml:CurrentUser:/User/@Loginid")%>"/>
         <input type="hidden" name="xml:/Order/@DocumentType" value='0017.ex'/></td>
</tr>
  <% String enterpriseCodeFromCommon = getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode");%>	
   
 <tr>
      <td class="detaillabel">
         <yfc:i18n>Document_Type</yfc:i18n>
	  </td>
      <td>
         <font color="black">
            <yfc:i18n>Install Base</yfc:i18n>
         </font>
      </td>
	  <td class="detaillabel" >
         <yfc:i18n>Customer_Org_ID</yfc:i18n>
      </td>
	  <td nowrap="true" >
	 	
         <input type="text" OldValue="" class="unprotectedinput" id='CusOrgID'  onblur="callAjaxFunctionOrgChange('CusOrgID','BUYER')"  <%=getTextOptions("xml:/Order/@BuyerOrganizationCode")%> />
         <img class="lookupicon" onclick="callLookupForSBOrg('xml:/Order/@BuyerOrganizationCode','xml:/Order/Extn/@BuyerOrgName','BUYER','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
      </td>
	  <td class="detaillabel" align="left">
         <yfc:i18n>Customer_Site_Org_ID</yfc:i18n>
      </td>
      <td nowrap="true" >
         <input type="text" id="CustSiteId" class="unprotectedinput" onblur= "callAjaxFunctionOrgChange('CustSiteId','NODE')"  <%=getTextOptions("xml:/Order/@ReceivingNode")%>/>
         <img class="lookupicon" onclick="callLookupForSBOrg('xml:/Order/@ReceivingNode','xml:/Order/Extn/@CustOrgName','NODE','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
		 </td>
		  <td class="detaillabel" >
         <yfc:i18n>Seller_Org_ID</yfc:i18n>
      </td>
      <td nowrap="true" >
         <input type="text" id="SellerId" class="unprotectedinput" onblur= "callAjaxFunctionOrgChange('SellerId','SELLER')" <%=getTextOptions("xml:/Order/@SellerOrganizationCode", "xml:/Order/@SellerOrganizationCode", sellerOrgCode)%>/>
         <img class="lookupicon" onclick="callLookupForSBOrg('xml:/Order/@SellerOrganizationCode','xml:/Order/Extn/@SellerOrgName','SELLER','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
      </td>
	   <td  align="center" colspan="4">
         <yfc:i18n>Customer_Master_Comment</yfc:i18n>
      </td>
   </tr>
	<tr>
		<td class="detaillabel" >
         <yfc:i18n>Install_Base_#</yfc:i18n>
      </td>
      <td>
         <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderNo")%>/>
      </td>
		<td class="detaillabel" align="left">
         <yfc:i18n>Customer_Org_Name</yfc:i18n></td>
		 <td  nowrap="true"><input type="text" id ="CusOrgIDName"class="protectedinput"  <%=getTextOptions("xml:/Order/Extn/@BuyerOrgName")%> readonly/>
		 </td>
		<td class="detaillabel" align="left">
         <yfc:i18n>Customer_Site_Org_Name</yfc:i18n></td>
		 <td  nowrap="true"><input type="text" id="CustSiteIdName"class="protectedinput"  <%=getTextOptions("xml:/Order/Extn/@CustOrgName")%> readonly/>
		 </td>
		 <td class="detaillabel" align="left">
         <yfc:i18n>Seller_Org_Name</yfc:i18n></td>
		 <td  nowrap="true"><input type="text" id="SellerIdName" class="protectedinput"  <%=getTextOptions("xml:/Order/Extn/@SellerOrgName")%> readonly/>
		 </td>
		 <td rowspan="5" colspan="7" width="100%">
		 <textarea rows="5" cols="8" contenteditable='true' class="unprotectedtextareainput" style="width:100%" <%=getTextOptions("xml:/Order/Extn/@CustMstrComnt")%>></textarea>
		 </td>
	</tr>
<script language="Javascript" >
    lockFocus();
</script>
</table>
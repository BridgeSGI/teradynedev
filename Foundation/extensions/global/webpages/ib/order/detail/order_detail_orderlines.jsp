<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ include file="/extn/console/jsp/td.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/css/scripts/editabletbl.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/orgName.js"></script>
<script language="javascript">


document.body.attachEvent("onunload", processSaveRecordsForChildNode);
</script>


<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/item.js"></script>
<%@ page import="java.text.SimpleDateFormat" %>
<%
boolean bisDraft=true;
String ShipDate= getValue("Order","xml:/Order/@OrderDate");
Date d2 = new Date(ShipDate);
d2.setDate(d2.getDate()+90);
SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
String d3 = df.format(d2);
appendBundleRootParent((YFCElement)request.getAttribute("Order"));
boolean bAppendOldValue = false;
if(!isVoid(errors) || equals(sOperation,"Y") || equals(sOperation,"DELETE")) 
bAppendOldValue = true;
String modifyView = request.getParameter("ModifyView");
modifyView = modifyView == null ? "" : modifyView;
String enterpriseCodeFromCommon =getValue("Order", "xml:/Order/@EnterpriseCode");
String driverDate = getValue("Order", "xml:/Order/@DriverDate");
String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("Order", "xml:/Order/@EnterpriseCode"));
%>

<table class="table" ID="OrderLines" cellspacing="0" width="100%" yfcMaxSortingRecords="1000" >
<thead>
<tr>
<td class="checkboxheader" sortable="no">
<input type="hidden" id="userOperation" name="userOperation" value="" />
<input type="hidden" id="numRowsToAdd" name="numRowsToAdd" value="" />
<input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>

</td>
<td class="tablecolumnheader" nowrap="true" style="width:30px">&nbsp;</td>
<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Line</yfc:i18n></td>
<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemID")%>"><yfc:i18n>Item_ID</yfc:i18n></td>

<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Serial_#</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>RE-Serial_</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Description</yfc:i18n></td>



<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Item_Type</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Coverage")%>"><yfc:i18n>Coverage</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@ProdFamily")%>"><yfc:i18n>Product_Family</yfc:i18n></td>	
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>MFG_ID</yfc:i18n></td>	
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Asset_ID</yfc:i18n></td>	

<td class="tablecolumnheader" nowrap="true"  sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/@ReceivingNode")%>"><yfc:i18n>Install_Node</yfc:i18n></td>
<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/@ReqDeliveryDate")%>"><yfc:i18n>Expected_Install</yfc:i18n></td>

<td class="tablecolumnheader" nowrap="true" sortable="no"  style="width:<%=getUITableSize("xml:/OrderLine/@ReqShipDate")%>"><yfc:i18n>Actual_Install</yfc:i18n></td>
<td class="tablecolumnheader" nowrap="true" sortable="no"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@OwneOrg")%>"><yfc:i18n>Owner_Org</yfc:i18n></td>	
<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@OprOrg")%>"><yfc:i18n>Operator_Org</yfc:i18n></td>	
<td class="tablecolumnheader" nowrap="true" sortable="no"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@ServOrg")%>"><yfc:i18n>Service_Org</yfc:i18n></td>	
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>SO Line#</yfc:i18n></td>	
<td class="tablecolumnheader"	nowrap="true" sortable="no"  nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@ReqShipDate")%>"><yfc:i18n>Program</yfc:i18n></td>


<td class="tablecolumnheader"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@RCenter")%>"><yfc:i18n>Enable_At_R_Center</yfc:i18n></td>	
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@IBStatus")%>"><yfc:i18n>Mkt_Status</yfc:i18n></td>	
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@IBStatus")%>"><yfc:i18n>Processing_Status</yfc:i18n></td>	
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@IBStatus")%>"><yfc:i18n>Build_Status</yfc:i18n></td>	

<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@SerialNo")%>"><yfc:i18n>Status</yfc:i18n></td>
</tr>
</thead>
<tbody>
<yfc:loopXML name="Order" binding="xml:/Order/OrderLines/@OrderLine" id="OrderLine">
<%	
//Set variables to indicate the orderlines dependency situation. 
boolean isDependentParent = false;
boolean isDependentChild = false;
if (equals(getValue("OrderLine","xml:/OrderLine/@ParentOfDependentGroup"),"Y")) {  
isDependentParent = true;
}
if (!isVoid(getValue("OrderLine","xml:/OrderLine/@DependentOnLineKey"))) {
isDependentChild = true;
}

if(!isVoid(resolveValue("xml:OrderLine:/OrderLine/@Status"))) {  
if (equals(getValue("OrderLine","xml:/OrderLine/@ItemGroupCode"),"PROD") )
//display line in this inner panel only if item has ItemGroupCode = PROD
{
if(bAppendOldValue) {
String sOrderLineKey = resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey");
if(oMap.containsKey(sOrderLineKey))
request.setAttribute("OrigAPIOrderLine",(YFCElement)oMap.get(sOrderLineKey));
} else 
request.setAttribute("OrigAPIOrderLine",(YFCElement)pageContext.getAttribute("OrderLine"));
%>
<tr>
<yfc:makeXMLInput name="orderLineKey">
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@IsBundleParent" value="xml:/OrderLine/@IsBundleParent"/>
</yfc:makeXMLInput>
<yfc:makeXMLInput name="bundleRootParentKey">
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@BundleRootParentKey"/>
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@IsBundleParent" value="xml:/OrderLine/@IsBundleParent"/>
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OriginalLineItemClicked" value="xml:/OrderLine/@PrimeLineNo"/>
</yfc:makeXMLInput>
<td class="checkboxcolumn" >
<input type="checkbox" value='<%=getParameter("orderLineKey")%>' name="chkEntityKey" statuss='<%=getValue("OrderLine","xml:OrderLine:/OrderLine/@Status")%>' actdate='<%=getValue("OrderLine","xml:OrderLine:/OrderLine/Extn/@ActualInsDate")%>'
<% if (isDependentParent || isDependentChild) {%> inExistingDependency="true" <%}%>/>
<%/*This hidden input is required by yfc to match up each line attribute that is editable in this row against the appropriate order line # on the server side once you save.  */%>
<input type="hidden" name='OrderLineKey_<%=OrderLineCounter%>' value='<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>' />
<input type="hidden" name='OrderHeaderKey_<%=OrderLineCounter%>' value='<%=resolveValue("xml:/Order/@OrderHeaderKey")%>' />
<input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@OrderLineKey", "xml:/OrderLine/@OrderLineKey")%> />
<input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@PrimeLineNo", "xml:/OrderLine/@PrimeLineNo")%> />
<input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@SubLineNo", "xml:/OrderLine/@SubLineNo")%> />
</td>
<td class="tablecolumn" nowrap="true">
<% if (equals(getValue("OrderLine","xml:/OrderLine/@GiftFlag"),"Y") ){ %>
<img <%=getImageOptions(request.getContextPath() + "/console/icons/gift.gif", "This_is_a_Gift_Line")%>/>
<% } %>
<yfc:hasXMLNode binding="xml:/OrderLine/Instructions/Instruction">
<a <%=getDetailHrefOptions("L01", getParameter("orderLineKey"), "")%>>
<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.INSTRUCTIONS_COLUMN, "Instructions")%>></a>
</yfc:hasXMLNode>
<yfc:hasXMLNode binding="xml:/OrderLine/KitLines/KitLine">
<a <%=getDetailHrefOptions("L02", getParameter("orderLineKey"), "")%>>
<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Kit_Components")%>></a>
</yfc:hasXMLNode>

<% if (equals(getValue("OrderLine","xml:/OrderLine/@IsBundleParent"),"Y")) { %>
<a <%=getDetailHrefOptions("L02", getParameter("bundleRootParentKey"), "")%>>
<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Bundle_Components")%>></a>
<%}%>


<yfc:makeXMLInput name="orderLineKey">
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
</yfc:makeXMLInput>
<%	if(!equals("false", getParameter("CanAddServiceLines") ) )
{
if (equals(getValue("OrderLine","xml:/OrderLine/@HasServiceLines"),"Y")) { %>
<a <%=getDetailHrefOptions("L12", getParameter("orderLineKey"), "")%>><img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/providedservicecol.gif", "Line_Has_Associated_Service_Requests")%> ></a>

<%	}	else	if (equals(getValue("OrderLine","xml:/OrderLine/@CanAddServiceLines"),"Y") && ! equals(getValue("Order","xml:/Order/@isHistory"),"Y")) { %>  
<a <%=getDetailHrefOptions("L10", getParameter("orderLineKey"), "")%>><img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/addprovidedservice.gif", "Line_Has_Service_Requests_That_Can_Be_Added")%> ></a>
<%	}	%>
<%}%>
<%	if (isDependentParent) { %>    
<yfc:makeXMLInput name="dependentParentKey">
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
</yfc:makeXMLInput>
<a <%=getDetailHrefOptions("L05", getParameter("dependentParentKey"), "")%>><img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.PARENT_DEPENDENCY_COMPONENTS_COLUMN, "Dependent_Parent")%> ></a>
<% } %>
<% if (isDependentChild) { %>
<yfc:makeXMLInput name="dependentChildKey">
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@DependentOnLineKey"/>
<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
</yfc:makeXMLInput>
<a <%=getDetailHrefOptions("L06", getParameter("dependentChildKey"), "")%>>
<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.CHILD_DEPENDENCY_COMPONENTS_COLUMN, "Dependent_Child")%>></a>
<% }%>
<% if (equals(getValue("OrderLine","xml:/OrderLine/@HasChainedLines"),"Y") || (!isVoid(getValue("OrderLine","xml:/OrderLine/@ChainedFromOrderLineKey"))) || equals(getValue("OrderLine","xml:/OrderLine/@HasDerivedChild"),"Y") || (!isVoid(getValue("OrderLine","xml:/OrderLine/@DerivedFromOrderLineKey")))) { %>                        
<a <%=getDetailHrefOptions("L09", getParameter("orderLineKey"), "")%>>
<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.CHAINED_ORDERLINES_COLUMN, "Related_Lines")%>></a>
<% }%>
<% if (equals(getValue("OrderLine","xml:/OrderLine/@AwaitingDeliveryRequest"),"Y") && ! equals(getValue("Order","xml:/Order/@isHistory"),"Y")) { %>
<yfc:makeXMLInput name="orderKey">
<yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
</yfc:makeXMLInput>
<a <%=getDetailHrefOptions("L13", getParameter("orderKey"), "")%>>
<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/deliveryitem.gif", "Delivery_Request_needs_to_be_added")%>></a>
<%	}
// If the order line has a CurrentWorkOrderKey, the icon will link directly to work order details
String linkForWorkOrder = "";
String keyNameForLink = "";
String currentWorkOrderKey = getValue("OrderLine", "xml:/OrderLine/@CurrentWorkOrderKey");
if (!isVoid(currentWorkOrderKey)) {
linkForWorkOrder = "L15";
keyNameForLink = "workOrderKey";
%>
<yfc:makeXMLInput name="workOrderKey">
<yfc:makeXMLKey binding="xml:/WorkOrder/@WorkOrderKey" value="xml:/OrderLine/@CurrentWorkOrderKey"/>
</yfc:makeXMLInput>
<%
} else {
// If the order line has other work orders (without current work order key), the icon will
// link to the line-level work order list screen.
String numberOfWorkOrders = getValue("OrderLine","xml:/OrderLine/WorkOrders/@NumberOfWorkOrders");
if (!isVoid(numberOfWorkOrders)) {
int numberOfWorkOrdersInt = (new Integer(numberOfWorkOrders)).intValue();	
if (numberOfWorkOrdersInt > 0) {
linkForWorkOrder = "L14";
keyNameForLink = "orderLineKey";
}
}
}
if (!isVoid(linkForWorkOrder) && !equals(getValue("Order","xml:/Order/@isHistory"),"Y")) {
%>
<a <%=getDetailHrefOptions(linkForWorkOrder, getParameter(keyNameForLink), "")%>>
<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/workorders.gif", "View_Work_Orders")%>>
</a>
<%  } %>
		   <% 
	
	String sIBOHKey = resolveValue("xml:/OrderLine/@OrderHeaderKey");
	String sIBOLKey=resolveValue("xml:/OrderLine/@OrderLineKey");
	YFCElement inputElemSC = YFCDocument.parse("<TerSCIBMapping TerIBOHKey=\"" + sIBOHKey + "\" TerIBOLKey=\""+sIBOLKey+"\"   />").getDocumentElement();
    YFCElement templateElemSC = YFCDocument.parse("<TerSCIBMappingList ><TerSCIBMapping /></TerSCIBMappingList >").getDocumentElement();
	String isCoverage="N";
%>

<yfc:callAPI serviceName="GetIBForLine" inputElement="<%=inputElemSC%>" templateElement="<%=templateElemSC%>" outputNamespace="TerSCIBMappingList"/>
<yfc:hasXMLNode binding="xml:TerSCIBMappingList:/TerSCIBMappingList/TerSCIBMapping">

<yfc:loopXML name="TerSCIBMappingList" binding="xml:/TerSCIBMappingList/@TerSCIBMapping" id="SCOrderLine">

<% if (equals(getValue("SCOrderLine","xml:/TerSCIBMapping/YFSSCOrderLine/@MaxLineStatusDesc"),"Draft Order Created") ){ %>



<% bisDraft=false; }
else { isCoverage="Y";bisDraft=true;%>
<%}%>
</yfc:loopXML>
<%if(bisDraft){ %>
<a <%=getDetailHrefOptions("L16", getParameter("orderLineKey"), "")%>>
<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/servicetools.gif", "View_Service_Contracts")%>></a>
<%}

else {%>
	<a <%=getDetailHrefOptions("L16", getParameter("orderLineKey"), "")%>>
<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/WarningIndicatorColumn.gif", "Service_Contracts_need_attention")%>></a>
<%}%>

</yfc:hasXMLNode> 
<%
YFCDate Actdate=null;
YFCDate Expdate=null;
if (!isVoid(getValue("OrderLine","xml:/OrderLine/Extn/@ActualInsDate"))) {
 Actdate = new YFCDate(getDateValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@ActualInsDate"));


if (!isVoid(getValue("OrderLine","xml:/OrderLine/Extn/@ExpectedInsDate")))
	{
 Expdate = new YFCDate(getDateValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@ExpectedInsDate"));

long Diff= Actdate.getTime() - Expdate.getTime();

if(Diff>0)
	
{ 
String status=getValue("OrderLine","xml:/OrderLine/@Status");	
String Ent=getValue("Order","xml:/Order/@EnterpriseCode");	
YFCElement inputElemST = YFCDocument.parse("<CommonCode CallingOrganizationCode=\"" + Ent + "\" CodeShortDescription=\""+status+"\"  CodeType=\"IBStatus\" />").getDocumentElement();
    YFCElement templateElemST = YFCDocument.parse("<CommonCodeList ><CommonCode CodeValue=\"\" /></CommonCodeList >").getDocumentElement();

%>
<yfc:callAPI apiName="getCommonCodeList" inputElement="<%=inputElemST%>" templateElement="<%=templateElemST%>" outputNamespace="StatusList"/>
<%String statusNo=getValue("StatusList","xml:/CommonCodeList/CommonCode/@CodeValue");

if(statusNo.equals("1100.10")){
%>



<a <%=getDetailHrefOptions("L17", getParameter("orderLineKey"), "")%>>
<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/appointmentwaiting.gif", "Late Installation Request")%>></a>
<%}


}







}

}%>
</td>
<td class="tablecolumn" sortValue="<%=getNumericValue("xml:OrderLine:/OrderLine/@PrimeLineNo")%>">
<% if(showOrderLineNo("Order","Order")) {%>
<a <%=getDetailHrefOptions("L03", getParameter("orderLineKey"), "")%>>
<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/></a>
<%} else {%>
<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/>
<%}%>
</td>
<td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID"/></td>

<td class="tablecolumn" sortValue="<%=resolveValue("xml:OrderLine:/OrderLine/Extn/@SystemSerialNo")%>" nowrap="true"><input type="text" class="Unprotectedinput"  <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SystemSerialNo","xml:/OrderLine/Extn/@SystemSerialNo","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
</td>	

<td class="tablecolumn">
<input class="checkbox" type="checkbox" <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@IsReSerial")%>"  <%}%>
<%=yfsGetCheckBoxOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@IsReSerial","xml:/OrderLine/Extn/@IsReSerial","Y","xml:/OrderLine/AllowedModifications")%>/></td>		<td class="tablecolumn" nowrap="true"><%=getLocalizedOrderLineDescription("OrderLine")%></td>
<td class="tablecolumn" nowrap="true"><%=getLocalizedOrderLineItemTypeDescription("OrderLine")%></td>
<td class="tablecolumn"><%=isCoverage %></td>
<td class="tablecolumn" nowrap="true"><yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/Extn/@SystemProductFamily"/></td>

<td  nowrap="true"><input type="text" class="Unprotectedinput"  <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@MfgID")%>"  <%}%><%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@MfgID","xml:/OrderLine/Extn/@MfgID","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
</td>

<td  nowrap="true"><input type="text" class="Unprotectedinput"  <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@AssetID")%>"  <%}%><%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@AssetID","xml:/OrderLine/Extn/@AssetID","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
</td>
<td class="tablecolumn" nowrap="true">

<input type="text"  id='NodeLine_<%=OrderLineCounter%>'onblur= "callAjaxFunctionOrgChange('NodeLine_<%=OrderLineCounter%>','NODE')"
<%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/@ReceivingNode")%>"  <%}%><%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@ReceivingNode", "xml:/OrderLine/@ReceivingNode","xml:/Order/AllowedModifications","ADD_LINE","text")%>  />
<img class="lookupicon" onclick="callLookupForSBOrg('<%="xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@ReceivingNode"%>','','NODE','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>

</td>

<td class="tablecolumn" nowrap="true">

<input type="text"  class="dateinput" <%if(bAppendOldValue) { %> OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@ExpectedInsDate")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/Extn/@ExpectedInsDate_YFCDATE", "xml:/OrderLine/Extn/@ExpectedInsDate_YFCDATE", "xml:/OrderLine/AllowedModifications")%>/>
<img class="lookupicon" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar", "xml:/OrderLine/Extn/@ExpectedInsDate", "xml:/OrderLine/AllowedModifications")%>/>
</td>




<td class="tablecolumn" nowrap="true">
<input type="text" class="dateinput" <%if(bAppendOldValue) { %> OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@ActualInsDate")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/Extn/@ActualInsDate_YFCDATE", "xml:/OrderLine/Extn/@ActualInsDate_YFCDATE", "xml:/OrderLine/AllowedModifications")%>/>
<img class="lookupicon" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar", "xml:/OrderLine/Extn/@ActualInsDate", "xml:/OrderLine/AllowedModifications")%>/>

</td>   
<td class="tablecolumn" nowrap="true">
<input type="text" id='OwnerLine_<%=OrderLineCounter%>'onblur= "callAjaxFunctionOrgChange('OwnerLine_<%=OrderLineCounter%>','BUYER')"
<%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@TesterOwnerOrgID")%>"  <%}%><%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@TesterOwnerOrgID","xml:/OrderLine/Extn/@TesterOwnerOrgID","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
<img class="lookupicon" onclick="callLookupForSBOrg('<%="xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/Extn/@TesterOwnerOrgID"%>','','BUYER','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
</td>
<td class="tablecolumn" nowrap="true">
<input type="text" id='OperLine_<%=OrderLineCounter%>'onblur= "callAjaxFunctionOrgChange('OperLine_<%=OrderLineCounter%>','BUYER')"
<%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@TesterOperatorOrgID")%>"  <%}%><%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@TesterOperatorOrgID","xml:/OrderLine/Extn/@TesterOperatorOrgID","xml:/Order/AllowedModifications","ADD_LINE","text")%>  />
<img class="lookupicon" onclick="callLookupForSBOrg('<%="xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/Extn/@TesterOperatorOrgID"%>','','BUYER','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
</td>
<td class="tablecolumn" nowrap="true">
<input type="text"  id='SrvcLine_<%=OrderLineCounter%>'onblur= "callAjaxFunctionOrgChange('SrvcLine_<%=OrderLineCounter%>','BUYER')"
<%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@TesterServicerOrgID")%>"  <%}%><%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@TesterServicerOrgID","xml:/OrderLine/Extn/@TesterServicerOrgID","xml:/Order/AllowedModifications","ADD_LINE","text")%>  />
<img class="lookupicon" onclick="callLookupForSBOrg('<%="xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/Extn/@TesterServicerOrgID"%>','','BUYER','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
</td>

<td  nowrap="true"><input type="text" class="Unprotectedinput"  <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@SOLineNumber")%>"  <%}%><%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SOLineNumber","xml:/OrderLine/Extn/@SOLineNumber","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
</td>

<td class="tablecolumn">
<select OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@SysProgram")%>" 
<%=yfsGetComboOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SysProgram", "xml:/Order/AllowedModifications")%>>
<yfc:loopOptions binding="xml:/TDYNSrvcOvrList/@TDYNSrvcOvr" name="SystemProgram"
value="SystemProgram" selected="xml:OrigAPIOrderLine:/OrderLine/Extn/@SysProgram" isLocalized="Y"/>
</select>
</td>
<td class="tablecolumn">
<select OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@RapidSourceCenter")%>" 
<%=getComboOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@RapidSourceCenter")%>>

<yfc:loopOptions binding="xml:organizationlist:/OrganizationList/@Organization" name="OrganizationCode" 
value="OrganizationCode" selected="xml:OrigAPIOrderLine:/OrderLine/Extn/@RapidSourceCenter" isLocalized="Y"/>	

</select>
</td>

<td class="tablecolumn">
<select OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@MktStatus")%>" 
<%=getComboOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@MktStatus")%>>
<yfc:loopOptions binding="xml:ibmarketstatuslist:/CommonCodeList/@CommonCode" name="CodeShortDescription" 
value="CodeValue" selected="xml:OrigAPIOrderLine:/OrderLine/Extn/@MktStatus" isLocalized="Y"/>
</select>
</td>
<td class="tablecolumn">
<select OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@ProcessingStatus")%>" 
<%=getComboOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ProcessingStatus")%>>

<yfc:loopOptions binding="xml:processingStatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription" 
value="CodeValue" selected="xml:OrigAPIOrderLine:/OrderLine/Extn/@ProcessingStatus" isLocalized="Y"/>

</select>

</td>
<td class="tablecolumn">
<select OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@IBBuildStatus")%>" 
<%=getComboOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@IBBuildStatus")%>>

<yfc:loopOptions binding="xml:ibbuildstatus:/CommonCodeList/@CommonCode" name="CodeShortDescription" 
value="CodeValue" selected="xml:OrigAPIOrderLine:/OrderLine/Extn/@IBBuildStatus" isLocalized="Y"/>

</select>

</td>


<td class="tablecolumn">
<a <%=getDetailHrefOptions("L04", getParameter("orderLineKey"),"ShowReleaseNo=Y")%>><%=displayOrderStatus(getValue("OrderLine","xml:/OrderLine/@MultipleStatusesExist"),getValue("OrderLine","xml:/OrderLine/@MaxLineStatusDesc"))%></a>
</td>
</tr>
<%}
} else if(isVoid(resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey"))) {%>
<tr DeleteRowIndex="<%=OrderLineCounter%>">
<td class="checkboxcolumn"> 
<img class="icon" onclick="setDeleteOperationForRow(this,'xml:/Order/OrderLines/OrderLine')" <%=getImageOptions(YFSUIBackendConsts.DELETE_ICON, "Remove_Row")%>/>
</td>
<td class="tablecolumn">
<input type="hidden" OldValue="" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@Action", "xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@Action", "CREATE")%> />
<input type="hidden"  <%=getTextOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@DeleteRow",  "")%> />
</td>
<td class="tablecolumn">&nbsp;</td>
<td class="tablecolumn" nowrap="true">
<input type="text" OldValue="" id='OrderLine_<%=OrderLineCounter%>' onblur="callAjaxFunctionOnChange('<%=OrderLineCounter%>')" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ItemID","xml:/OrderLine/Item/@ItemID","xml:/Order/AllowedModifications","ADD_LINE","text")%> />		
<img class="lookupicon" onclick="templateRowCallItemLookup(this,'ItemID','ProductClass','TransactionalUOM','IBITDNitem','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item")%> />

<Input type="hidden" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ProductClass","Good", "xml:/Order/AllowedModifications","ADD_LINE","text")%>/>

<Input type="hidden" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@TransactionalUOM","EA", "xml:/Order/AllowedModifications","ADD_LINE","text")%>/>    
</td>
</td>
<td class="tablecolumn" nowrap="true">
<Input type="text" OldValue="" id='SRLNO_<%=OrderLineCounter%>' <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SystemSerialNo","xml:/OrderLine/Extn/@SystemSerialNo","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>    
</td>

<td class="tablecolumn">
<input type="checkbox" <%=getCheckBoxOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@IsReSerial", "xml:/OrderLine/Extn/@IsReSerial", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue='N' ></input>
</td>

<td class="tablecolumn"  nowrap="true">
<Input type="text" OldValue="" id='ITMDESC_<%=OrderLineCounter%>' readonly size="30" maxLength="40"<%=yfsGetTemplateRowOptions("p","","c","ADD_LINE","text")%>
/></td>

<td class="tablecolumn" nowrap="true">
<Input type="text"  OldValue="" id='ITMTYPE_<%=OrderLineCounter%>'  readonly  size="30"<%=yfsGetTemplateRowOptions("a1","","c1","ADD_LINE","text")%>/>
</td>
<td class="tablecolumn"></td>
<td class="tablecolumn"nowrap="true" ><Input type="text" id='ITMFAM_<%=OrderLineCounter%>' readonly 
<%=yfsGetTemplateRowOptions("a","","c","ADD_LINE","text")%>
/></td>


<td class="tablecolumn" nowrap="true">

<label for="mfgid" class="normal" id="mfgidlabel"><span id="startspan_<%=OrderLineCounter%>" style="color:red"></span></label>
<Input type="text" OldValue="" id='MFGID_<%=OrderLineCounter%>' <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@MfgID","xml:/OrderLine/Extn/@MfgID","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>   

</td>
<td class="tablecolumn" nowrap="true">


<Input type="text" OldValue=""  <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@AssetID","xml:/OrderLine/Extn/@AssetID","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>   

</td>


<td class="tablecolumn" nowrap="true">
<input type="text" OldValue="" id='NodeLine_<%=OrderLineCounter%>'onblur= "callAjaxFunctionOrgChange('NodeLine_<%=OrderLineCounter%>','NODE')"
<%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@ReceivingNode","xml:/Order/@ReceivingNode","xml:/Order/AllowedModifications","ADD_LINE","text")%>  />
<img class="lookupicon" onclick="callLookupForSBOrg('<%="xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@ReceivingNode"%>','','NODE','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
</td>


<td class="tablecolumn" nowrap="true">

<input type="text" OldValue="" class="dateinput" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExpectedInsDate_YFCDATE","xml:/OrderLine/Extn/@ExpectedInsDate","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
<img class="lookupicon" onclick="invokeCalendar(this)" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar")%>/>
</td>
<td class="tablecolumn" nowrap="true">
<input type="text" OldValue="" class="dateinput" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ActualInsDate_YFCDATE","xml:/OrderLine/Extn/@ActualInsDate","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
<img class="lookupicon" onclick="invokeCalendar(this)" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar")%>/>

</td> 
<td class="tablecolumn" nowrap="true">
<input type="text" OldValue="" id='OwnerLine_<%=OrderLineCounter%>'onblur= "callAjaxFunctionOrgChange('OwnerLine_<%=OrderLineCounter%>','BUYER')"<%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@TesterOwnerOrgID","xml:/Order/@BuyerOrganizationCode","xml:/Order/AllowedModifications","ADD_LINE","text")%>  />
<img class="lookupicon" onclick="callLookupForSBOrg('<%="xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@TesterOwnerOrgID"%>','','BUYER','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Recieving_Node")%>/>
</td>

<td class="tablecolumn" nowrap="true">
<input type="text" OldValue="" id='OperLine_<%=OrderLineCounter%>'onblur= "callAjaxFunctionOrgChange('OperLine_<%=OrderLineCounter%>','BUYER')"<%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@TesterOperatorOrgID","xml:/Order/@BuyerOrganizationCode","xml:/Order/AllowedModifications","ADD_LINE","text")%>  />
<img class="lookupicon" onclick="callLookupForSBOrg('<%="xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@TesterOperatorOrgID"%>','','BUYER','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Recieving_Node")%>/>
</td>
<td class="tablecolumn" nowrap="true">
<input type="text" OldValue="" id='SrvcLine_<%=OrderLineCounter%>'onblur= "callAjaxFunctionOrgChange('SrvcLine_<%=OrderLineCounter%>','BUYER')"<%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@TesterServicerOrgID","xml:/Order/@BuyerOrganizationCode","xml:/Order/AllowedModifications","ADD_LINE","text")%>  />
<img class="lookupicon" onclick="callLookupForSBOrg('<%="xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@TesterServicerOrgID"%>','','BUYER','<%=enterpriseCodeFromCommon%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Recieving_Node")%>/>
</td>

<td  nowrap="true"><input type="text" class="Unprotectedinput"  <%=getTextOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SOLineNumber")%>/>
</td>


<td class="tablecolumn">
<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SysProgram","xml:/OrderLine/Extn/@SysProgram","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
<yfc:loopOptions binding="xml:/TDYNSrvcOvrList/@TDYNSrvcOvr" name="SystemProgram"
value="SystemProgram" selected=" " isLocalized="Y"/>
</select>
</td>
<td class="tablecolumn">
<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@RapidSourceCenter","xml:/OrderLine/Extn/@RapidSourceCenter","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
<yfc:loopOptions binding="xml:organizationlist:/OrganizationList/@Organization" name="OrganizationCode" 
value="OrganizationCode" selected=" " isLocalized="Y"/>
</select>
</td>

<td class="tablecolumn">
<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@MktStatus","xml:/OrderLine/Extn/@MktStatus","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>

<yfc:loopOptions binding="xml:ibmarketstatuslist:/CommonCodeList/@CommonCode" name="CodeShortDescription" 
value="CodeValue" selected="A" isLocalized="Y"/>
</select>
</td>

<td class="tablecolumn">
<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ProcessingStatus","xml:/OrderLine/Extn/@ProcessingStatus","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
<yfc:loopOptions binding="xml:processingStatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription" 
value="CodeValue" selected="A" isLocalized="Y"/>
</select>
</td>
<td class="tablecolumn">
<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@IBBuildStatus","xml:/OrderLine/Extn/@IBBuildStatus","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
<yfc:loopOptions binding="xml:ibbuildstatus:/CommonCodeList/@CommonCode" name="CodeShortDescription" 
value="CodeValue" isLocalized="Y" selected="xml:/OrderLine/Extn/@IBBuildStatus"/>
</select>
</td>

<td class="numerictablecolumn">
<input type="hidden" OldValue="" 			<%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@OrderedQty","1","xml:/Order/AllowedModifications","ADD_LINE","text")%> style='width:40px'/>
</td>
</tr>
<%}%>	
</yfc:loopXML>
<%if(!bisDraft){ %>
<script type="text/javascript">
window.onload = function(){
    alert('One or more Service Contracts created in Draft Status');
}
</script>
<%}%>
</tbody>
<tfoot>        

<%if (isModificationAllowed("xml:/@AddLine","xml:/Order/AllowedModifications")) { %>
<tr>
<td nowrap="true" colspan="25">
<jsp:include page="/common/editabletbl.jsp" flush="true">
<jsp:param name="ReloadOnAddLine" value="Y"/>
</jsp:include>
</td>
</tr>
<%}%>

</tfoot>
</table>

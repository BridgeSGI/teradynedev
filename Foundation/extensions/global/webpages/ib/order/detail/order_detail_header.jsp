<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/yfsjspcommon/editable_util_header.jspf" %>
<%@ page import="com.yantra.yfc.util.*" %>
 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<%  

	String sRequestDOM = request.getParameter("getRequestDOM");
    String modifyView = request.getParameter("ModifyView");
    modifyView = modifyView == null ? "" : modifyView;

	String sHiddenDraftOrderFlag = getValue("Order", "xml:/Order/@DraftOrderFlag");
    String driverDate = getValue("Order", "xml:/Order/@DriverDate");
	String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("Order", "xml:/Order/@EnterpriseCode"));
	extraParams += "&" + getExtraParamsForTargetBinding("xml:/Order/@OrderHeaderKey", resolveValue("xml:/Order/@OrderHeaderKey"));
	extraParams += "&" + getExtraParamsForTargetBinding("IsStandaloneService", "Y");
	extraParams += "&" + getExtraParamsForTargetBinding("hiddenDraftOrderFlag", sHiddenDraftOrderFlag);
%>

<script language="javascript">
	// this method is used by 'Add Service Request' action on order header detail innerpanel
	function callPSItemLookup()	{
		yfcShowSearchPopupWithParams('','itemlookup',900,550,new Object(), 'psItemLookup', '<%=extraParams%>');
	}
</script>

<table class="view" width="100%">
    <yfc:makeXMLInput name="orderKey">
        <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
    </yfc:makeXMLInput>
    <tr>
        <td>
            <input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
            <input type="hidden" name="xml:/Order/@ModificationReasonCode" />
            <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
            <input type="hidden" name="xml:/Order/@Override" value="N"/>
            <input type="hidden" name="hiddenDraftOrderFlag" value='<%=sHiddenDraftOrderFlag%>'/>
            <input type="hidden" name="chkWOEntityKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>'/>
            <input type="hidden" name="chkCopyOrderEntityKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>' />
			<input type="hidden" name="chkOrderHeaderKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>' />
			<input type="hidden" name="xml:/Order/@EnterpriseCode" value='<%=getValue("Order", "xml:/Order/@EnterpriseCode")%>'/>
			<input type="hidden" name="xml:/Order/@DocumentType" value='<%=getValue("Order", "xml:/Order/@DocumentType")%>'/>
        </td>
    </tr>
    <tr>
        <td class="detaillabel" ><yfc:i18n>Enterprise</yfc:i18n></td>
        <td class="protectedtext" ><yfc:getXMLValue binding="xml:/Order/@EnterpriseCode"/></td>
        <td class="detaillabel" ><yfc:i18n>Customer_Org_ID</yfc:i18n></td>
					 <td class="protectedtext">
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/Order/@BuyerOrganizationCode" />
					</yfc:makeXMLInput>
					<a <%=getDetailHrefOptions("L03",getParameter("OrganizationKey"),"")%> >
						<yfc:getXMLValue binding="xml:/Order/@BuyerOrganizationCode"/>
					</a>
				</td>
		 <td class="detaillabel" ><yfc:i18n>Customer_Site_Org_ID</yfc:i18n></td>
		<td class="protectedtext">
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/Order/@ReceivingNode" />
					</yfc:makeXMLInput>
					<a <%=getDetailHrefOptions("L03",getParameter("OrganizationKey"),"")%> >
						<yfc:getXMLValue binding="xml:/Order/@ReceivingNode"/>
					</a>
				</td>
        <td class="detaillabel" ><yfc:i18n>Seller_Org_ID</yfc:i18n></td>
			<td class="protectedtext" nowrap="true"	>
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/Order/@SellerOrganizationCode" />
					</yfc:makeXMLInput>
					<a <%=getDetailHrefOptions("L03",getParameter("OrganizationKey"),"")%> >
						<yfc:getXMLValue binding="xml:/Order/@SellerOrganizationCode"/>
					</a>
				</td>

<td>&nbsp</td>
			
			 <td  align="left" colspan="4">
         <yfc:i18n>Customer_Master_Comment</yfc:i18n>

      </td>
	  
    </tr>
    <tr>
        <td class="detaillabel" ><yfc:i18n>Install_Base_#</yfc:i18n></td>
        <td class="protectedtext"><a <%=getDetailHrefOptions("L06", resolveValue("xml:/Order/@DocumentType"), getParameter("orderKey"),"")%> ><yfc:getXMLValue binding="xml:/Order/@OrderNo"/></a></td>

       <td class="detaillabel" align="left">
         <yfc:i18n>Customer_Org_Name</yfc:i18n></td>
		 <%if(resolveValue("xml:/Order/Extn/@BuyerOrgName").length()>0) {%>
		 <td class="protectedtext"><%=resolveValue("xml:/Order/Extn/@BuyerOrgName")%></td>
		 <%}
		 else {
		     YFCElement orgNameInput = YFCDocument.parse("<Organization OrganizationCode=\""+resolveValue("xml:/Order/@BuyerOrganizationCode")+"\" />").getDocumentElement();
			 YFCElement orgNameTemplate = YFCDocument.parse("<OrganizationList ><Organization  OrganizationName=\"\"/></OrganizationList>").getDocumentElement();
			
		 %>
		 <yfc:callAPI apiName="getOrganizationList" inputElement="<%=orgNameInput%>" templateElement="<%=orgNameTemplate%>" outputNamespace="OrgList"/>
 <td class="protectedtext"><%=resolveValue("xml:OrgList:/OrganizationList/Organization/@OrganizationName")%></td>
	<%	}%>
		 
		 
          <td class="detaillabel" align="left">
         <yfc:i18n>Customer_Site_Org_Name</yfc:i18n></td>

		<%if(resolveValue("xml:/Order/Extn/@CustOrgName").length()>0) {%>
		 <td class="protectedtext"><%=resolveValue("xml:/Order/Extn/@CustOrgName")%></td>
		 <%}
		 else {
		     YFCElement orgNameInput = YFCDocument.parse("<Organization OrganizationCode=\""+resolveValue("xml:/Order/@ReceivingNode")+"\" />").getDocumentElement();
			 YFCElement orgNameTemplate = YFCDocument.parse("<OrganizationList ><Organization  OrganizationName=\"\"/></OrganizationList>").getDocumentElement();
			
		 %>
		 <yfc:callAPI apiName="getOrganizationList" inputElement="<%=orgNameInput%>" templateElement="<%=orgNameTemplate%>" outputNamespace="OrgList"/>
 <td class="protectedtext"><%=resolveValue("xml:OrgList:/OrganizationList/Organization/@OrganizationName")%></td>
	<%	}%>


		 <td class="detaillabel" align="left">
         <yfc:i18n>Seller_Org_Name</yfc:i18n></td>
		 <%if(resolveValue("xml:/Order/Extn/@SellerOrgName").length()>0) {%>
		  <td class="protectedtext"><%=resolveValue("xml:/Order/Extn/@SellerOrgName")%></td>&nbsp
				 <%}
		 else {
		     YFCElement orgNameInput = YFCDocument.parse("<Organization OrganizationCode=\""+resolveValue("xml:/Order/@SellerOrganizationCode")+"\" />").getDocumentElement();
			 YFCElement orgNameTemplate = YFCDocument.parse("<OrganizationList ><Organization  OrganizationName=\"\"/></OrganizationList>").getDocumentElement();
			
		 %>
		 <yfc:callAPI apiName="getOrganizationList" inputElement="<%=orgNameInput%>" templateElement="<%=orgNameTemplate%>" outputNamespace="OrgList"/>
 <td class="protectedtext"><%=resolveValue("xml:OrgList:/OrganizationList/Organization/@OrganizationName")%></td>
	<%	}%>
		 
<td rowspan="5" colspan="7" width="25%">
		 <textarea rows="5" cols="8" contenteditable='true' class="unprotectedtextareainput" style="width:100%" <%=getTextOptions("xml:/Order/Extn/@CustComnt")%>><yfc:getXMLValue binding="xml:/Order/Extn/@CustMstrComnt"/></textarea>
		 </td>
    </tr>
   
	
		<td class="detaillabel" >
			<yfc:i18n>Document_Type</yfc:i18n>
		</td>
		<td class="protectedtext">
			<yfc:getXMLValueI18NDB binding="xml:DocumentParamsList:/DocumentParamsList/DocumentParams/@Description"></yfc:getXMLValueI18NDB>
		</td>
	
	<% 
		String exchangeType = "";
		if (equals(sRequestDOM,"Y")) {
			exchangeType = getValue("OrigAPIOutput", "xml:/Order/@ExchangeType");		
		}else{
			exchangeType = getValue("Order", "xml:/Order/@ExchangeType");
		}
		if(!isVoid(exchangeType)){ 
		
	//call master data for exchange type

		// Call API to get the data for the Document Type field.
			String exchangeTypeStr = "<CommonCode CodeType=\"EXCHANGE_TYPE\"/>";
			
			YFCElement exchangeTypeInput = YFCDocument.parse(exchangeTypeStr).getDocumentElement();
			YFCElement exchangeTypeTemplate = YFCDocument.parse("<CommonCodeList TotalNumberOfRecords=\"\"><CommonCode CodeType=\"\" CodeShortDescription=\"\" CodeValue=\"\" CommonCodeKey=\"\"/></CommonCodeList>").getDocumentElement();
     %>

	        <yfc:callAPI apiName="getCommonCodeList" inputElement="<%=exchangeTypeInput%>" templateElement="<%=exchangeTypeTemplate%>" outputNamespace="ExchangeTypeList"/>

		<td class="detaillabel" ><yfc:i18n>Exchange_Type</yfc:i18n></td>
		<td>
			<select  <% if (isVoid(modifyView)) {%> <%=getProtectedComboOptions()%> <%}  if (equals(sRequestDOM,"Y")) {%> OldValue="<%=resolveValue("xml:OrigAPIOutput:/Order/@ExchangeType")%>"  <%}%> <%=yfsGetComboOptions("xml:/Order/@ExchangeType", "xml:/Order/AllowedModifications")%>>
				<yfc:loopOptions binding="xml:ExchangeTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
				value="CodeValue" selected="xml:/Order/@ExchangeType" isLocalized="Y"/>
			</select>
		</td>
	<% } %>

	<% if(!isVoid(getValue("Order", "xml:/Order/@ReturnOrderHeaderKeyForExchange"))){ %>
      <yfc:makeXMLInput name="ReturnOHKeyForExchange">
			<yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/ReturnOrdersForExchange/ReturnOrderForExchange/@OrderHeaderKey" />
       </yfc:makeXMLInput>
		<td class="detaillabel" ><yfc:i18n>Created_For_Return_#</yfc:i18n></td>
		<td class="protectedtext">
			<a <%=getDetailHrefOptions("L04",getParameter("ReturnOHKeyForExchange"),"")%> >
				<yfc:getXMLValue binding="xml:/Order/ReturnOrdersForExchange/ReturnOrderForExchange/@OrderNo"/>
			</a> 
		</td>
	<% } %>
	</tr>
</table>

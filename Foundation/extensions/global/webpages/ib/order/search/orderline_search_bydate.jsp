<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script language="javascript">
	
	function preapreLookupsearch(){
		var obj = window.dialogArguments;
		if(obj != null && obj.lookup){
			var newEle1 = document.createElement("INPUT");
			newEle1.type="hidden";
			newEle1.name = "xml:/OrderLine/Order/@ReceivingNode";
			newEle1.value = obj.recvNode;
			
			var newEle2 = document.createElement("INPUT");
			newEle2.type="hidden";
			newEle2.name = "xml:/OrderLine/@ToStatus";
			newEle2.value = "1100.30";   //Active status
				
			var table = document.getElementById('searchTable');
			table.insertBefore(newEle1);
			table.insertBefore(newEle2);
		}
	}
	
	window.attachEvent('onload', preapreLookupsearch);
</script>
<%  
	YFCDate oEndDate = new YFCDate(); 
	oEndDate.setEndOfDay();
%>
<table class="view" id="searchTable">
    <tr>
        <td>
           	<input type="hidden" name="xml:OrderLine/Order/Extn/@TdShipDateQryType" value="BETWEEN"/>
            <input type="hidden" name="xml:/OrderLine/Extn/@ExpectedInsDateQryType" value="BETWEEN"/>
            <input type="hidden" name="xml:/OrderLine/Extn/@ActualInsDateQryType" value="BETWEEN"/>
            <input type="hidden" name="xml:/OrderLine/Order/@DraftOrderFlag" value="N"/>
			<input type="hidden" name='xml:/OrderLine/@ItemGroupCode' value='PROD'  />
			<input type="hidden" name="xml:/OrderLine/Order/@DocumentType" value='0017.ex'/></td>
        </td>
    </tr>

     <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="RefreshOnDocumentType" value="true"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
      <jsp:param name="ShowDocumentType" value="false"/>
        <jsp:param name="EnterpriseCodeBinding" value="xml:/OrderLine/Order/@EnterpriseCode"/>
			 <jsp:param name="AcrossEnterprisesAllowed" value="FALSE"/>
		 <jsp:param name="OrganizationListForInventory" value="true"/>
    </jsp:include>

     <tr>
        <td class="searchlabel" >
            <yfc:i18n>Document Type</yfc:i18n>
        </td>
    </tr>
<tr>
      <td>
         <font color="black">
            <yfc:i18n>Install_Base</yfc:i18n>
         </font>
      </td>
	  </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Ship_Date</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <input class="dateinput" type="text" 
			<%=getTextOptions("xml:OrderLine/Order/Extn/@FromTdShipDate_YFCDATE","xml:OrderLine/Order/Extn/@FromTdShipDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
            <input class="dateinput" type="text" <%=getTextOptions("xml:OrderLine/Order/Extn/@FromTdShipDate_YFCTIME","xml:OrderLine/Order/Extn/@FromTdShipDate_YFCTIME","")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
			<yfc:i18n>To</yfc:i18n>
			</td>
			</tr>
		<tr>
		<td nowrap="true">
            <input class="dateinput" type="text" <%=getTextOptions("xml:OrderLine/Order/Extn/@ToTdShipDate_YFCDATE","xml:OrderLine/Order/Extn/@ToTdShipDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="text" <%=getTextOptions("xml:OrderLine/Order/Extn/@ToTdShipDate_YFCTIME","xml:OrderLine/Order/Extn/@ToTdShipDate_YFCTIME",oEndDate.getString(getLocale().getTimeFormat()))%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
			</td>
		</tr>
		
			
        
    
    <tr>
	    <td class="searchlabel" >
	        <yfc:i18n>Expecetd_Install_Date</yfc:i18n>
	    </td>
	</tr>
	 <tr>
        <td nowrap="true">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/OrderLine/Extn/@FromExpectedInsDate_YFCDATE","xml:/OrderLine/Extn/@FromExpectedInsDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
            <input class="dateinput" type="text" <%=getTextOptions("xml:/OrderLine/Extn/@FromExpectedInsDate_YFCTIME","xml:/OrderLine/Extn/@FromExpectedInsDate_YFCTIME","")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
			<yfc:i18n>To</yfc:i18n>
			</td>
			</tr>
		<tr>
		<td nowrap="true">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/OrderLine/Extn/@ToExpectedInsDate_YFCDATE","xml:/OrderLine/Extn/@ToExpectedInsDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="text" <%=getTextOptions("xml:/OrderLine/Extn/@ToExpectedInsDate_YFCTIME","xml:/Orderline/Extn/@ToExpectedInsDate_YFCTIME",oEndDate.getString(getLocale().getTimeFormat()))%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
			</td>
		</tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Actual_Install_Date</yfc:i18n>
        </td>
    </tr>
     <tr>
        <td nowrap="true">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/OrderLine/Extn/@FromActualInsDate_YFCDATE","xml:/OrderLine/Extn/@FromActualInsDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
            <input class="dateinput" type="text" <%=getTextOptions("xml:/OrderLine/Extn/@FromActualInsDate_YFCTIME","xml:/OrderLine/Extn/@FromActualInsDate_YFCTIME","")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
			<yfc:i18n>To</yfc:i18n>
			</td>
			</tr>
		<tr>
		<td nowrap="true">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/OrderLine/Extn/@ToActualInsDate_YFCDATE","xml:/OrderLine/Extn/@ToActualInsDate_YFCDATE","")%>/>
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="text" <%=getTextOptions("xml:/OrderLine/Extn/@ToActualInsDate_YFCTIME","xml:/OrderLine/Extn/@ToActualInsDate_YFCTIME",oEndDate.getString(getLocale().getTimeFormat()))%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
			</td>
		</tr>
<tr>
		<td class="searchlabel" >
            <yfc:i18n>Book_year/Week</yfc:i18n>

        </td>
		
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/Extn/@BookYWQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/Extn/@BookYWQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/Extn/@BookYW")%>/>
			
        </td>
		</tr>
		<tr>
		<td class="searchlabel" >
            <yfc:i18n>Ship_year/Week</yfc:i18n>

        </td>
		
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/Extn/@ShipYWQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/Extn/@ShipYWQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/Extn/@ShipYW")%>/>
			
        </td>
		</tr>

</table>



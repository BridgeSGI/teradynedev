<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>

<script language="javascript">
	
	function preapreLookupsearch(){
	
		var obj = window.dialogArguments;
		if(obj != null && obj.lookup){
			var customerSiteIdLabel = document.getElementById("customerSiteIdLabel");
			var customerSiteIdText = document.getElementById("customerSiteIdText");
			var customerSiteNameLabel = document.getElementById("customerSiteNameLabel");
			var customerSiteNameText = document.getElementById("customerSiteNameText");
			var orgIdLabel = document.getElementById("orgIdLabel");
			var orgIdText = document.getElementById("orgIdText");
			var orgNameLabel = document.getElementById("orgNameLabel");
			var orgNameText = document.getElementById("orgNameText");
			var IBLineStatusTo = document.getElementById("IBLineStatusTo");
			var IBLineStatusFrom = document.getElementById("IBLineStatusFrom");
			var IBLineStatusLabel = document.getElementById("IBLineStatusLabel");
			
			customerSiteIdLabel.style.display='none';
			customerSiteIdText.style.display='none';
			customerSiteNameLabel.style.display='none';
			customerSiteNameText.style.display='none';
			orgIdLabel.style.display='none';
			orgIdText.style.display='none';
			orgNameLabel.style.display='none';
			orgNameText.style.display='none';
			IBLineStatusTo.style.display='none';
			IBLineStatusFrom.style.display='none';
			IBLineStatusLabel.style.display='none';
			
			var newEle1 = document.createElement("INPUT");
			newEle1.type="hidden";
			newEle1.name = "xml:/OrderLine/Order/@ReceivingNode";
			newEle1.value = obj.recvNode;
			
			var newEle2 = document.createElement("INPUT");
			newEle2.type="hidden";
			newEle2.name = "xml:/OrderLine/@ToStatus";
			newEle2.value = "1100.30";   //Active status
			
			var recvNodeEle = document.all("xml:/OrderLine/Order/@ReceivingNode");
			if(recvNodeEle != null){
				recvNodeEle.value = obj.recvNode;
			}
			/*var tr1 = document.createElement("tr");
			var td1 = document.createElement("td");
			td1.innerHTML = "Install Base Line Status";
			tr1.appendChild(td1);
						
			var tr2 = document.createElement("tr");
			var td2 = document.createElement("td");
			td2.innerHTML = "Active";
			tr2.appendChild(td2);*/
				
			var table = document.getElementById('searchTable');
			table.insertBefore(newEle1);
			table.insertBefore(newEle2);
			/*table.appendChild(tr1);
			table.appendChild(tr2);*/
		}
	}
	
	window.attachEvent('onload', preapreLookupsearch);
</script>

<%
	preparePaymentStatusList(getValue("OrderLine", "xml:/OrderLine/Order/@PaymentStatus"), (YFCElement) request.getAttribute("PaymentStatusList"));
	
	String itemType = getValue("OrderLine", "xml:/OrderLine/Extn/@ItemType");
	if (isVoid(itemType)) {
		itemType = "";
	}
%>

<table class="view" id="searchTable">
    <tr>
        <td>
            <input type="hidden" name="xml:/OrderLine/@StatusQryType" value="BETWEEN"/>
            <input type="hidden" name="xml:/OrderLine/Order/@DraftOrderFlag" value="N"/>
			<input type="hidden" name='xml:/OrderLine/@ItemGroupCode' value='PROD'  />

			<input type="hidden" name="xml:/OrderLine/Order/OrderHoldType/@Status" value=""/>
			<input type="hidden" name="xml:/OrderLine/Order/OrderHoldType/@StatusQryType" value="" />
			<input type="hidden" name="xml:/OrderLine/Order/@DocumentType" value='0017.ex'/></td>
		
			<input type="hidden" name="xml:/OrderLine/Extn/@ItemType" value='<%=itemType%>'/></td>
        </td>
    </tr>

    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="RefreshOnDocumentType" value="true"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
      <jsp:param name="ShowDocumentType" value="false"/>
        <jsp:param name="EnterpriseCodeBinding" value="xml:/OrderLine/Order/@EnterpriseCode"/>
			 <jsp:param name="AcrossEnterprisesAllowed" value="FALSE"/>
		 <jsp:param name="OrganizationListForInventory" value="true"/>
    </jsp:include>
    <% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code) %>
    <yfc:callAPI apiID="AP2"/>
  
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Document Type</yfc:i18n>
        </td>
    </tr>
	<tr>
      <td>
         <font color="black">
            <yfc:i18n>Install_Base</yfc:i18n>
         </font>
      </td>
	  </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Install_Base_ID	</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@OrderNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@OrderNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/@OrderNo")%>/>
        </td>
    </tr>
    <tr id="orgIdLabel">
        <td class="searchlabel" >
            <yfc:i18n>Customer_Org_ID</yfc:i18n>
        </td>
    </tr>
    <tr id="orgIdText">
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@BuyerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@BuyerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/@BuyerOrganizationCode")%>/>
			<% String enterpriseCode = getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode");%>
             <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@BuyerOrganizationCode','xml:/OrderLine/Order/Extn/@BuyerOrgName','BUYER','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
    <tr id="orgNameLabel">
        <td class="searchlabel" >
            <yfc:i18n>Customer_Org_Name</yfc:i18n>
        </td>
    </tr>
    <tr id="orgNameText">
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@SellerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@SellerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/Extn/@BuyerOrgName")%>/>
            <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@BuyerOrganizationCode','xml:/OrderLine/Order/Extn/@BuyerOrgName','BUYER','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
    <tr id="customerSiteIdLabel">
        <td class="searchlabel" >
            <yfc:i18n>Customer_Site_Org_ID</yfc:i18n>
        </td>
    </tr>
    <tr id="customerSiteIdText">
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@BuyerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@BuyerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/@ReceivingNode")%>/>
			
             <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@ReceivingNode','xml:/OrderLine/Order/Extn/@CustOrgName','NODE','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
    <tr id="customerSiteNameLabel">
        <td class="searchlabel" >
            <yfc:i18n>Customer_Site_Org_Name</yfc:i18n>
        </td>
    </tr>
    <tr id="customerSiteNameText">
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@SellerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@SellerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/Extn/@CustOrgName")%>/>
            <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@ReceivingNode','xml:/OrderLine/Order/Extn/@CustOrgName','NODE','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Seller_Org_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@BuyerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@BuyerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/@SellerOrganizationCode")%>/>
			
             <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@SellerOrganizationCode','xml:/OrderLine/Order/Extn/@SellerOrgName','SELLER','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Seller_Org_Name</yfc:i18n>
        </td>
    </tr>
    <tr>
         <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/Extn/@SellerOrgNameQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/Extn/@SellerOrgNameQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/Extn/@SellerOrgName")%>/>
            <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@SellerOrganizationCode','xml:/OrderLine/Order/Extn/@SellerOrgName','SELLER','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
	 <tr>
        <td class="searchlabel" >
            <yfc:i18n>Seller_Sales_order_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/Extn/@SellerSalesOrdrNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/Extn/@SellerSalesOrdrNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/Extn/@SellerSalesOrdrNo")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Product_Family</yfc:i18n>
        </td>
    </tr>
    <tr>
	<td>
        <select name="xml:/OrderLine/Item/Extn/@SystemProductFamilyQryType" class="combobox">
                 <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@SystemProductFamilyQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Item/Extn/@SystemProductFamily")%>/>
       </td>
    </tr>
    <tr id="IBLineStatusLabel">
        <td class="searchlabel" >
            <yfc:i18n>Install Base Line Status</yfc:i18n>
        </td>
    </tr>
    <tr id="IBLineStatusFrom">
        <td nowrap="true">
            <select name="xml:/OrderLine/@FromStatus" class="combobox">
                <yfc:loopOptions binding="xml:StatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription" 
		                    value="CodeValue" selected="xml:/OrderLine/@FromStatus" isLocalized="Y"/>
            </select>
            <span class="searchlabel" ><yfc:i18n>To</yfc:i18n></span>
        </td>
    </tr>
    <tr id="IBLineStatusTo">
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/@ToStatus" class="combobox">
                <yfc:loopOptions binding="xml:StatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription" 
		                    value="CodeValue" selected="xml:/OrderLine/@ToStatus" isLocalized="Y"/>
            </select>
        </td>
    </tr>
   
	

	
	
</table>
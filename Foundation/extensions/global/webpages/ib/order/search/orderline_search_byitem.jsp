<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>

<script language="javascript">
	
	function preapreLookupsearch(){
		var obj = window.dialogArguments;
		if(obj != null && obj.lookup){
		
			var customerSiteIdLabel = document.getElementById("customerSiteIdLabel");
			var customerSiteIdText = document.getElementById("customerSiteIdText");
			var customerSiteNameLabel = document.getElementById("customerSiteNameLabel");
			var customerSiteNameText = document.getElementById("customerSiteNameText");
			var orgIdLabel = document.getElementById("orgIdLabel");
			var orgIdText = document.getElementById("orgIdText");
			var orgNameLabel = document.getElementById("orgNameLabel");
			var orgNameText = document.getElementById("orgNameText");
			
			customerSiteIdLabel.style.display='none';
			customerSiteIdText.style.display='none';
			customerSiteNameLabel.style.display='none';
			customerSiteNameText.style.display='none';
			orgIdLabel.style.display='none';
			orgIdText.style.display='none';
			orgNameLabel.style.display='none';
			orgNameText.style.display='none';	
			
			var newEle1 = document.createElement("INPUT");
			newEle1.type="hidden";
			newEle1.name = "xml:/OrderLine/Order/@ReceivingNode";
			newEle1.value = obj.recvNode;
			
			var newEle2 = document.createElement("INPUT");
			newEle2.type="hidden";
			newEle2.name = "xml:/OrderLine/@ToStatus";
			newEle2.value = "1100.30";   //Active status
				
			var recvNodeEle = document.all("xml:/OrderLine/Order/@ReceivingNode");
			if(recvNodeEle != null){
				recvNodeEle.value = obj.recvNode;
			}
			
			var table = document.getElementById('searchTable');
			table.insertBefore(newEle1);
			table.insertBefore(newEle2);
		}
	}
	
	window.attachEvent('onload', preapreLookupsearch);
</script>

<%
	preparePaymentStatusList(getValue("OrderLine", "xml:/OrderLine/Order/@PaymentStatus"), (YFCElement) request.getAttribute("PaymentStatusList"));
%>

<table class="view" id="searchTable" >
    <tr>
        <td>
            <input type="hidden" name="xml:/OrderLine/@StatusQryType" value="BETWEEN"/>
            <input type="hidden" name="xml:/OrderLine/Order/@DraftOrderFlag" value="N"/>
			<input type="hidden" name='xml:/OrderLine/@ItemGroupCode' value='PROD'  />

			<input type="hidden" name="xml:/OrderLine/Order/OrderHoldType/@Status" value=""/>
			<input type="hidden" name="xml:/OrderLine/Order/OrderHoldType/@StatusQryType" value="" />
			<input type="hidden" name="xml:/OrderLine/Order/@DocumentType" value='0017.ex'/></td>
			<input type="hidden" name="xml:/OrderLine/@OrderingUOM" value='EA'/></td>
			
		
        </td>
    </tr>

    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="RefreshOnDocumentType" value="true"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
      <jsp:param name="ShowDocumentType" value="false"/>
        <jsp:param name="EnterpriseCodeBinding" value="xml:/OrderLine/Order/@EnterpriseCode"/>
			 <jsp:param name="AcrossEnterprisesAllowed" value="FALSE"/>
		 <jsp:param name="OrganizationListForInventory" value="true"/>
    </jsp:include>
    <% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code) %>
 
	 <yfc:callAPI apiID="AP3"/>
    

	
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Document Type</yfc:i18n>
        </td>
    </tr>
<tr>
      <td>
         <font color="black">
            <yfc:i18n>Install_Base</yfc:i18n>
         </font>
      </td>
	  </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Item_ID</yfc:i18n>

        </td>
		
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Item/@ItemIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Item/@ItemIDNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Item/@ItemID")%>/>
			<% String enterpriseCode = getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode");%>
			<img class="lookupicon" onclick="templateRowCallItemLookup(this,'ItemID','ProductClass','TransactionalUOM','TDNitem','<%=enterpriseCode%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item")%> />
        </td>
    
</tr>
<tr>
<td class="searchlabel" >
            <yfc:i18n>Product_Family</yfc:i18n>
        </td>
		<td class="searchlabel" >
            <yfc:i18n>Item_Type</yfc:i18n>
        </td>
		</tr>
		<tr>
        <td nowrap="true">
            <select name="xml:/OrderLine/Item/Extn/@SystemProductFamilyQryType" class="combobox">
                 <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@SystemProductFamilyQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Item/Extn/@SystemProductFamily")%>/>
       </td>
	   <td nowrap="true">
            <select name="xml:/OrderLine/Item/PrimaryInformation/@ItemType" class="combobox">
			<option value="" ></option>
                <option value="SYSTEM" >SYSTEM</option>
						 <option value="FRU">FRU</option>
            </select>
       </td>
	</tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Asset_ID</yfc:i18n>

        </td>
		
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Extn/@AssetIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@AssetIDNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Extn/@AssetID")%>/>
			
        </td>
    
</tr>
 
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Serial_No</yfc:i18n>

        </td>
		<td class="searchlabel" >
            <yfc:i18n>Coverage_Flag</yfc:i18n>

        </td>
		
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Extn/@SystemSerialNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@SystemSerialNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Extn/@SystemSerialNo")%>/>
			
        </td>
    <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Extn/@IsCoverage" class="combobox">
			<option value="" ></option>
                 <option value="Y" >Y</option>
					<option value="N">N</option>
            </select>
           
			
        </td>
</tr>
<tr>
        <td class="searchlabel" >
            <yfc:i18n>Install_Node_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/@ReceivingNodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/@ReceivingNodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/@ReceivingNode")%>/>
			
            <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/@ReceivingNode','xml:/OrderLine/Extn/@CustOrgName','NODE','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>


	<tr id="orgIdLabel">
        <td class="searchlabel" >
            <yfc:i18n>Customer_Org_ID</yfc:i18n>
        </td>
    </tr>
    <tr id="orgIdText">
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@BuyerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@BuyerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/@BuyerOrganizationCode")%>/>
			
             <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@BuyerOrganizationCode','xml:/OrderLine/Order/Extn/@BuyerOrgName','BUYER','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
	<tr id="orgNameLabel">
        <td class="searchlabel" >
            <yfc:i18n>Customer_Org_Name</yfc:i18n>
        </td>
    </tr>
    <tr id="orgNameText">
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/Extn/@BuyerOrgNameQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/Extn/@BuyerOrgNameQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/Extn/@BuyerOrgName")%>/>
            <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@BuyerOrganizationCode','xml:/OrderLine/Order/Extn/@BuyerOrgName','BUYER','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
	<tr id="customerSiteIdLabel">
        <td class="searchlabel" >
            <yfc:i18n>Customer_Site_Org_ID</yfc:i18n>
        </td>
    </tr>
    <tr id="customerSiteIdText">
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/@ReceivingNodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/@ReceivingNodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/@ReceivingNode")%>/>
			
             <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@ReceivingNode','xml:/OrderLine/Order/Extn/@CustOrgName','NODE','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
    <tr id="customerSiteNameLabel">
        <td class="searchlabel" >
            <yfc:i18n>Customer_Site_Org_Name</yfc:i18n>
        </td>
    </tr>
    <tr id="customerSiteNameText">
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/Extn/@CustOrgNameQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/Extn/@CustOrgNameQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/Extn/@CustOrgName")%>/>
            <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Order/@ReceivingNode','xml:/OrderLine/Order/Extn/@CustOrgName','NODE','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Seller_Sales_order_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Order/Extn/@SellerSalesOrdrNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Order/Extn/@SellerSalesOrdrNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Order/Extn/@SellerSalesOrdrNo")%>/>
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Tester_Owner_Org</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Extn/@TesterOwnerOrgIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@TesterOwnerOrgIDQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Extn/@TesterOwnerOrgID")%>/>
			
             <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Extn/@TesterOwnerOrgID','xml:/none','BUYER','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Tester_Operator_Org</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Extn/@TesterOperatorOrgIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@TesterOperatorOrgIDQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Extn/@TesterOperatorOrgID")%>/>
			
             <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Extn/@TesterOperatorOrgID','xml:/none','Buyer','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Tester_Servicing_Org</yfc:i18n>
        </td>
    </tr>
    <tr>
         <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/OrderLine/Extn/@TesterServicerOrgIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/OrderLine/Extn/@TesterServicerOrgIDQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/OrderLine/Extn/@TesterServicerOrgID")%>/>
			
             <img class="lookupicon" onclick="callLookupForSBOrg('xml:/OrderLine/Extn/@TesterServicerOrgID','xml:/none','Buyer','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
	<tr>
	 <td class="searchlabel" >
            <yfc:i18n>Program</yfc:i18n>
        </td>
		<td class="searchlabel" >
            <yfc:i18n>Enabled_At_Rapid_Center?</yfc:i18n>
        </td>
   </tr>
    <tr>
        <td nowrap="True">
            <select name="xml:/OrderLine/Extn/@SysProgram" class="combobox">
                <yfc:loopOptions binding="xml:/TDYNSrvcOvrList/@TDYNSrvcOvr" name="SystemProgram"
                value="SystemProgram" selected="xml:/OrderLine/Extn/@SysProgram" isLocalized="Y"/>
            </select>
                   </td>
				<td nowrap="true" >
            <select name="xml:/OrderLine/Extn/@RapidSourceCenter" class="combobox">
			 <yfc:loopOptions binding="xml:organizationlist:/OrganizationList/@Organization" name="OrganizationCode" 
		      value="OrganizationCode" selected="xml:/OrderLine/Extn/@RapidSourceCenter" isLocalized="Y"/>
            </select>
           
			
        </td>

    </tr>
	
</table>
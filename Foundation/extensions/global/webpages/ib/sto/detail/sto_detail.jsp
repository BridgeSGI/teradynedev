<%@ include file="/yfsjspcommon/yfsutil.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/sto.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>

<table class="view" width="100%">
<%
 String key=resolveValue("xml:/TDYNSrvcOvr/@StoOverDtlKey");
String extraParams="";
 if (!isVoid(key)) {%>	 
 <input type="hidden" name="xml:/TDYNSrvcOvr/@Enterprise" value='<%=getValue("TDYNSrvcOvr", "xml:/TDYNSrvcOvr/@Enterprise")%>'/>
<%  extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("TDYNSrvcOvr", "xml:/TDYNSrvcOvr/@Enterprise")); 
	
		
}
		else{ 
		 extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); 
	
		
		}%>
<tr>


    <td class="searchcriteriacell" nowrap="true">
            <yfc:i18n>Service Type Offering Item ID</yfc:i18n>
        </td>
		<td>
        <input type="text" id="servicex" class="unprotectedinput" <%=getTextOptions("xml:/TDYNSrvcOvr/@ServiceType") %>  onblur="callAjaxFunctionOnSTO('servicex')"  />
	
		
		
        <img class="lookupicon" onclick="callItemLookup1('xml:/TDYNSrvcOvr/@ServiceType','xml:/TDYNSrvcOvr/@ProductClass','xml:/TDYNSrvcOvr/@UnitOfMeasure','TDNitem','SERVICE',	'<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
    </td>


<%
  String ProgramFound = resolveValue("xml:/TDYNSrvcOvr/@SystemProgram");
  String Hidden ="";
  if(ProgramFound.length()>0){
	  
   Hidden ="none"; 
	}

	else
	{
	Hidden="block";
	}
 %>



    <td id="system1" class="searchcriteriacell" nowrap="true" style="display: <%=Hidden%>;">
            <yfc:i18n>System Item ID</yfc:i18n>
        </td>
		<td style="display: <%=Hidden%>;" id="system" >
        <input id="system2" type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDYNSrvcOvr/@SystemType") %>  onblur="callAjaxFunctionOnSTO('system2')" />
		        <img class="lookupicon" onclick="callItemLookup1('xml:/TDYNSrvcOvr/@SystemType','xml:/TDYNSrvcOvr/@ProductClass','xml:/TDYNSrvcOvr/@UnitOfMeasure','TDNitem','SYSTEM',	'<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
    </td>

<td class="detaillabel" nowrap="true">
            <yfc:i18n>Service days</yfc:i18n>
        </td>
		<td>
        <input type="number" id="srvcdays" class="unprotectedinput" <%=getTextOptions("xml:/TDYNSrvcOvr/@BusDaysToDueDate") %> />
    </td>

	</tr>
</table>

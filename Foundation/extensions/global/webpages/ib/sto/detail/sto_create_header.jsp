<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>


<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/orgName.js"></script>
<script language="Javascript" >
IgnoreChangeNames();
yfcDoNotPromptForChanges(true);

</script>

<table class="view" width="10%">

<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
<jsp:param name="EnterpriseCodeBinding" value="xml:/TDYNSrvcOvr/@EnterpriseCode"/>
<jsp:param name="ScreenType" value="detail"/>
<jsp:param name="EnterpriseCodeLabel" value="Enterprise"/>
<jsp:param name="ShowDocumentType" value="false"/>
<jsp:param name="RefreshOnEnterpriseCode" value="true"/>
<jsp:param name="OrganizationListForInventory" value="true"/>
</jsp:include> 
<input type="hidden" name="xml:/TDYNSrvcOvr/@Enterprise" value='<%=getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")%>'/>

<%
String ProgramFound = resolveValue("xml:/TDYNSrvcOvr/@SystemProgram");
String customersiteorgtFound = resolveValue("xml:/TDYNSrvcOvr/@CustomerNo");
String key=resolveValue("xml:/TDYNSrvcOvr/@StoOverDtlKey");


String nonec ="none";
String nonep ="none";
String disablec="";
String disablep="";
String checkedc="";
String checkedp="";
if(ProgramFound.length()>0){

nonep ="block"; 
//disablec="disabled";
checkedp="checked";
}

else if(customersiteorgtFound.length()>0)
{
//disablep="disabled";
nonec="block";
checkedc="checked";
}
%>
<script language="javascript">
<% 

if (!isVoid(key)) {	 
YFCDocument orderDoc = YFCDocument.createDocument("TDYNSrvcOvr");
orderDoc.getDocumentElement().setAttribute("StoOverDtlKey",resolveValue("xml:/TDYNSrvcOvr/@StoOverDtlKey"));

// If this screen is shown as a popup, then open the order detail view for the new order
// as a popup as well (instead of refreshing the same screen).
if (equals(request.getParameter(YFCUIBackendConsts.YFC_IN_POPUP), "Y")) {
%>
function showOrderDetailPopup() {
window.CloseOnRefresh = "Y";
callPopupWithEntity('IBorder', '<%=orderDoc.getDocumentElement().getString(false)%>');
window.close();
}
window.attachEvent("onload", showOrderDetailPopup);
<%
} else {
%>
function showOrderDetail() {
showDetailFor('<%=orderDoc.getDocumentElement().getString(false)%>');
}
window.attachEvent("onload", showOrderDetail);
<% }
}
    
%>
</script>

<% String enterpriseCode = getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode");%>
</table>
<table class="view" width="10%" align="center" >

<tr>
<td class="detaillabel"  nowrap="nowrap" >

<input type="radio" name=select onclick="hide();" <%=disablec%> <%=checkedc %>/>
<yfc:i18n>Customer Site Org ID</yfc:i18n></td>
<td class="detaillabel"  nowrap="nowrap" id="customer" style="display: <%=nonec%>;">
<input id="customer1" type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDYNSrvcOvr/@CustomerNo") %> onblur= "callAjaxFunctionOrgChange('customer1','NODE')"  />
<img class="lookupicon" onclick="callLookupForSBOrg('xml:/TDYNSrvcOvr/@CustomerNo','xml:/Order/Extn/@CustOrgName','NODE','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_Site_Org")%>/>
</td>

<td class="detaillabel"  nowrap="nowrap">
<input type="radio" name=select  onclick="show();" <%=disablep%> <%=checkedp %>/>
<yfc:i18n>Program</yfc:i18n> </td>
<td>
<input id="program" style="display:<%=nonep%>;" type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDYNSrvcOvr/@SystemProgram") %> />
</td>


</tr>




</table>


<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript">
function processDeleteSrvcOvrAction(keyName,msg)
{
	var inputElems = document.getElementsByTagName("input"),
    count = 0;
for (var i=0; i<inputElems.length; i++) {
    if (inputElems[i].type === "checkbox" && inputElems[i].checked === true) {
        count++;
   }
  
}
 

if(count<=1){
	if(isProgramType(keyName))
	{
       
		 var userResponse = confirm(msg);
	if(!userResponse)
		document.body.style.cursor='auto';
    return (userResponse);
		
	}
	else 
		alert('This record cannot be deleted');
	
		return false;
}
else
alert('Select one record to delete from the list');
	
		return false;
	
}




function isProgramType(chkName){
	var chkBoxArray = document.forms["containerform"].elements;
	 	for ( var i =0; i<chkBoxArray.length; i++ ) {
		
		

				if (chkBoxArray[i].name == chkName) {
			if (chkBoxArray[i].checked) {
				
				if((chkBoxArray[i].Program).length==0)
					return true;
			}
		}
	}
	return false;
}


//request.setAttribute("STOORGCODE",document.forms["containerform"]["xml:/TDYNSrvcOvrList/@OrganizationCode"].value) ;




</script>
<table class="table" editable="false" width="100%" cellspacing="0">
    <thead> 
        <tr>
            <td sortable="no" class="checkboxheader">
                <input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
                   <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader"><yfc:i18n>Service Type Item ID</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Program</yfc:i18n></td>
			 <td class="tablecolumnheader"><yfc:i18n>Customer-Site Org ID</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>System Item ID</yfc:i18n></td>
			 <td class="tablecolumnheader"><yfc:i18n>Service Days</yfc:i18n></td>
			 </tr>
    </thead>
    <tbody>
	
	<yfc:loopXML binding="xml:TYDNSRVCOVRLST:/TDYNSrvcOvrList/@TDYNSrvcOvr" id="TDYNSrvcOvr">
         <tr>
                <yfc:makeXMLInput name="StoOverDtlKey">
                    <yfc:makeXMLKey binding="xml:/TDYNSrvcOvr/@StoOverDtlKey" value="xml:/TDYNSrvcOvr/@StoOverDtlKey" />
				 </yfc:makeXMLInput>   
				 
                <td class="checkboxcolumn">                     
                    <input type="checkbox" value='<%=getParameter("StoOverDtlKey")%>' name="EntityKey" Program='<%=getValue("TDYNSrvcOvr","xml:/TDYNSrvcOvr/@SystemProgram")%>' 	/>
                </td>
                            
                <td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDYNSrvcOvr/@ServiceType"/></td>
                <td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDYNSrvcOvr/@SystemProgram"/></td>
				 <td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDYNSrvcOvr/@CustomerNo"/></td>
                <td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDYNSrvcOvr/@SystemType"/></td>
				  <td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDYNSrvcOvr/@BusDaysToDueDate"/></td>
				   <td class="tablecolumn"><yfc:getXMLValue binding="xml:/Item/@CallingOrganization"/></td>
              
              
               
                   
                </td>
            </tr>
        </yfc:loopXML>

   </tbody>
</table>
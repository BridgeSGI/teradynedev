<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table class="view">

<%
	String isReturnService = HTMLEncode.htmlEscape(getParameter("IsReturnService"));
	if (isVoid(isReturnService)) {
		isReturnService = resolveValue("xml:/Item/PrimaryInformation/@IsReturnService");
	}
	if (isVoid(isReturnService)) {
		isReturnService = "N";
	}

String itemType=getValue("Item", "xml:/Item/PrimaryInformation/@ItemType");

	if(isVoid(itemType)){
		
		itemType ="";
	}


	String canUseAsServiceTool = HTMLEncode.htmlEscape(getParameter("CanUseAsServiceTool"));
	if (isVoid(canUseAsServiceTool)) {
		canUseAsServiceTool = resolveValue("xml:/Item/@CanUseAsServiceTool");
	}

	String itemGroupCode = HTMLEncode.htmlEscape(getParameter("ItemGroupCode"));
	if (isVoid(itemGroupCode)) {
		itemGroupCode = resolveValue("xml:/Item/@ItemGroupCode");
	}
	if (isVoid(itemGroupCode)) {
		itemGroupCode = "PROD";
	}
	YFCElement itemGroupCodeElem = YFCDocument.createDocument("ItemGroupCode").getDocumentElement();
	request.setAttribute("ItemGroupCode", itemGroupCodeElem);
	itemGroupCodeElem.setAttribute("ItemGroupCode", itemGroupCode);
	   
	
	String callingOrgCode = getValue("Item", "xml:/Item/@CallingOrganizationCode");
	if(isVoid(callingOrgCode)){
		
		callingOrgCode = getValue("CurrentOrganization", getSelectedOrgCodeValue("xml:/Item/@CallingOrganizationCode"));
	}

	
%>



<script language="javascript">
	window.dialogArguments.parentWindow.defaultOrganizationCode = "<%=HTMLEncode.htmlEscape(callingOrgCode)%>";

</script>

<tr>
	<td>
		<input type="hidden" name="xml:/Item/PrimaryInformation/@IsReturnService" value='<%=isReturnService%>'/>
		<input type="hidden" name="xml:/Item/@ItemGroupCode" value='<%=itemGroupCode%>'/>
		<input type="hidden" name="xml:/Item/@CanUseAsServiceTool" value='<%=canUseAsServiceTool%>'/>
		<input type="hidden" name="xml:/Item/@UnitOfMeasure" value="EA"/>
	</td>
</tr>

<tr>
    <td class="searchlabel" ><yfc:i18n>Organizatioon</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
		<input type="text" class="protectedinput" contenteditable="false" <%=getTextOptions("xml:/Item/@CallingOrganizationCode")%>/>
    </td>
</tr>

    <% // Now call the APIs that are dependent on the calling organization code %>
	<yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP3"/>

<tr>
    <td class="searchlabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Item/@ItemIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Item/@ItemIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Item/@ItemID") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Item Type</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true">
            <select name="xml:/Item/PrimaryInformation/@ItemType" class="combobox">
			<option value="" ></option>
                <option value="SYSTEM" >SYSTEM</option>
						 <option value="FRU">FRU</option>
            </select>
       </td>
</tr>

<tr>
    <td class="searchlabel" ><yfc:i18n>Short_Description</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Item/PrimaryInformation/@ShortDescriptionQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Item/PrimaryInformation/@ShortDescriptionQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Item/PrimaryInformation/@ShortDescription") %> />
    </td>
</tr>


</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%
 String enterpriseCodeFromCommon =getValue("OrderLine", "xml:/OrderLine/Order/@EnterpriseCode");
 String DocType= getValue("OrderLine", "xml:/OrderLine/Order/@DocumentType");
%>
<%  
 String nonep="none";
%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/orgName.js"></script>
<table class="view" width="100%" >
    <tr id="edit" style="display:<%=nonep%>;">
        <td nowrap="true" >
        <yfc:i18n>Owner_Org</yfc:i18n>
		</td>
		<td nowrap="true">

         <input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@TesterOwnerOrgID")%>" id='CusOwnrID'  class="unprotectedinput" onblur="callAjaxFunctionOrgChange('CusOwnrID','BUYER')"  
		 <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@TesterOwnerOrgID","xml:/OrderLine/Extn/@TesterOwnerOrgID","xml:/OrderLine/AllowedModifications")%> />
         <img class="lookupicon" onclick="callLookupForSBOrg('xml:/Order/OrderLines/OrderLine/Extn/@TesterOwnerOrgID','','BUYER','<%=enterpriseCodeFromCommon%>','<%=DocType%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Owner_Org")%>/>
      </td>

 	<td nowrap="true" >
        <yfc:i18n>Operator_Org</yfc:i18n></td>

					<td  nowrap="true">
						<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@TesterOperatorOrgID")%>"  id='CusOprOrg' class="unprotectedinput" onblur="callAjaxFunctionOrgChange('CusOprOrg','BUYER')"  
						<%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@TesterOperatorOrgID","xml:/OrderLine/Extn/@TesterOperatorOrgID","xml:/OrderLine/AllowedModifications")%>  />
						<img class="lookupicon" onclick="callLookupForSBOrg('xml:/Order/OrderLines/OrderLine/Extn/@TesterOperatorOrgID','','BUYER','<%=enterpriseCodeFromCommon%>',
						'<%=DocType%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Operator_Org")%>/>
                                      </td>
		<td nowrap="true">
        <yfc:i18n>Servicing_Org</yfc:i18n>
    </td>
	<td nowrap="true">
						<input type="text"  OldValue="<%=resolveValue("xml:/OrderLine/Extn/@TesterServicerOrgID")%>"  id='CusSrvOrg' class="unprotectedinput" onblur="callAjaxFunctionOrgChange('CusSrvOrg','BUYER')"  <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@TesterServicerOrgID","xml:/OrderLine/Extn/@TesterServicerOrgID","xml:/OrderLine/AllowedModifications")%>  />
                        <img class="lookupicon" onclick="callLookupForSBOrg('xml:/Order/OrderLines/OrderLine/Extn/@TesterServicerOrgID','','BUYER','<%=enterpriseCodeFromCommon%>',
						'<%=DocType%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Servicer_Org")%>/>
                  <tr>
				  </tr>
					
					
					</td>

    </tr>
	<tr id="show">
	<td nowrap="true" >
        <yfc:i18n>Owner_Org</yfc:i18n>&nbsp&nbsp&nbsp;
		</td>
	<td class="protectedtext" nowrap="true">
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/OrderLine/Extn/@TesterOwnerOrgID" />
					</yfc:makeXMLInput>
					<a <%=getDetailHrefOptions("L05",getParameter("OrganizationKey"),"")%> >
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@TesterOwnerOrgID"/>
					</a>
				</td><td></td>
				<td nowrap="true" >
        <yfc:i18n>Operator_Org</yfc:i18n>&nbsp&nbsp&nbsp;</td>
			<td class="protectedtext" nowrap="true">
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/OrderLine/Extn/@TesterOperatorOrgID" />
					</yfc:makeXMLInput>
					<a <%=getDetailHrefOptions("L05",getParameter("OrganizationKey"),"")%> >
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@TesterOperatorOrgID"/>
					</a>
				</td><td></td>
			<td nowrap="true">
        <yfc:i18n>Servicing_Org</yfc:i18n>&nbsp&nbsp&nbsp;
    </td>
				<td class="protectedtext" nowrap="true">
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/OrderLine/Extn/@TesterServicerOrgID" />
					</yfc:makeXMLInput>
					<a <%=getDetailHrefOptions("L05",getParameter("OrganizationKey"),"")%> >
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@TesterServicerOrgID"/>
					</a>
				</td><td></td>
	
	</td>
	</tr>

</table>

	
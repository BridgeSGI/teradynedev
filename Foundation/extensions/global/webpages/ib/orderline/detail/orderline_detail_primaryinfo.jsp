<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/extn/console/jsp/td.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.core.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/orgName.js"></script>


<%  
    String modifyView = request.getParameter("ModifyView");
    modifyView = modifyView == null ? "" : modifyView;

    String driverDate = getValue("OrderLine", "xml:/OrderLine/Order/@DriverDate");
	String enterpriseCodeFromCommon =getValue("OrderLine", "xml:/OrderLine/Order/@EnterpriseCode");
	 String DocType= getValue("OrderLine", "xml:/OrderLine/Order/@DocumentType");
%>

<table class="view" width="100%">
<tr>
    <td>
        <input type="hidden" name="xml:/Order/@ModificationReasonCode"/>
        <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
        <input type="hidden" name="xml:/Order/@Override" value="N"/>
		<input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
        <input type="hidden" name="xml:/OrderRelease/@ModificationReasonCode"/>
        <input type="hidden" name="xml:/OrderRelease/@ModificationReasonText"/>
        <input type="hidden" name="xml:/OrderRelease/@Override" value="N"/>
        <input type="hidden" name="hiddenDraftOrderFlag" value='<%=getValue("OrderLine", "xml:/OrderLine/Order/@DraftOrderFlag")%>'/>
        <input type="hidden" <%=getTextOptions("xml:/Order/@OrderHeaderKey","xml:/OrderLine/@OrderHeaderKey")%> />
        <input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine/@OrderLineKey","xml:/OrderLine/@OrderLineKey")%> />

		<yfc:makeXMLInput name="orderKey">
			<yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/OrderLine/@OrderHeaderKey" ></yfc:makeXMLKey>
			<yfc:makeXMLKey binding="xml:/Order/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey" ></yfc:makeXMLKey>
		</yfc:makeXMLInput>
		<input type="hidden" name="chkEntityKey" value='<%=getParameter("orderKey")%>' />
    </td>
</tr>
<tr>
    <yfc:makeXMLInput name="statusKey">
        <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey" ></yfc:makeXMLKey>
    </yfc:makeXMLInput>
    <yfc:makeXMLInput name="invItemKey">
        <yfc:makeXMLKey binding="xml:/InventoryItem/@ItemID" value="xml:/OrderLine/Item/@ItemID" ></yfc:makeXMLKey>
	<yfc:makeXMLKey binding="xml:/InventoryItem/@UnitOfMeasure" value="xml:/OrderLine/Item/@UnitOfMeasure" ></yfc:makeXMLKey>
        
        <yfc:makeXMLKey binding="xml:/InventoryItem/@OrganizationCode" value="xml:/OrderLine/Order/@SellerOrganizationCode" ></yfc:makeXMLKey>
		<% if(isShipNodeUser()) { %>
			<yfc:makeXMLKey binding="xml:/InventoryItem/@ShipNode" value="xml:CurrentUser:/User/@Node"/>
		<%} else {%>
	        <yfc:makeXMLKey binding="xml:/InventoryItem/@ShipNode" value="xml:/OrderLine/@ShipNode" ></yfc:makeXMLKey>
		<%}%>
    </yfc:makeXMLInput>
    <td class="detaillabel" >
        <yfc:i18n>Install_Base_ID_#</yfc:i18n>
    </td>
    <td class="protectedtext">
		<% if(showOrderNo("OrderLine","Order")) {%>
		    <a <%=getDetailHrefOptions("L01",getParameter("orderKey"),"")%>>
				<yfc:getXMLValue binding="xml:/OrderLine/Order/@OrderNo"></yfc:getXMLValue>
			</a>&nbsp;
		<%} else {%>
			<yfc:getXMLValue binding="xml:/OrderLine/Order/@OrderNo"></yfc:getXMLValue>
		<%}%>
    </td>
    <td class="detaillabel" >
        <yfc:i18n>Line_#</yfc:i18n>
    </td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"></yfc:getXMLValue>&nbsp;</td>
    <td class="detaillabel" >
        <yfc:i18n>Line_Quantity</yfc:i18n>
    </td>
   
 
        <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/OrderLineTranQuantity/@OrderedQty"/>&nbsp;</td>

</tr>
<tr>
    <td class="detaillabel" >
        <yfc:i18n>Item_ID</yfc:i18n>
    </td>
    <td class="protectedtext">
        
		<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID"></yfc:getXMLValue>&nbsp;
	      

    </td>
<td class="detaillabel" >
        <yfc:i18n>Item_Type</yfc:i18n>
    </td>
	<td class="protectedtext"><%=getLocalizedOrderLineItemTypeDescription("OrderLine")%></td>
    <td class="detaillabel" >
        <yfc:i18n>Unit_Of_Measure</yfc:i18n>
    </td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM"></yfc:getXMLValue>&nbsp;</td>
	<td class="detaillabel" >
        <yfc:i18n>Product_Class</yfc:i18n>
    </td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ProductClass"></yfc:getXMLValue>&nbsp</td>
   
</tr>
<tr>
    <td class="detaillabel" >
        <yfc:i18n>Description</yfc:i18n>
    </td>
    
        <td class="protectedtext" colspan="3"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemDesc"></yfc:getXMLValue>&nbsp;
        </td>
   
    <td class="detaillabel" >
        <yfc:i18n>Status</yfc:i18n>
    </td>
	 <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/@Status"></yfc:getXMLValue>&nbsp;</td>
</tr>
<tr>
    <td class="detaillabel" >
        <yfc:i18n>Install_Node</yfc:i18n>
    </td>
    <% if (!isVoid(modifyView)) {%>
       <td nowrap="true" >
         <input type="text" OldValue="<%=resolveValue("xml:/OrderLine/@ReceivingNode")%>" id="CustSiteId" class="unprotectedinput" onblur= "callAjaxFunctionOrgChange('CustSiteId','NODE')"  
		 <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/@ReceivingNode", "xml:/OrderLine/@ReceivingNode", "xml:/OrderLine/AllowedModifications")%> />
         <img class="lookupicon" onclick="callLookupForSBOrg('xml:/Order/OrderLines/OrderLine/@ReceivingNode','','NODE','<%=enterpriseCodeFromCommon%>','<%=DocType%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
		 </td>



    <% } else { %>
        <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/@ReceivingNode"/>&nbsp;</td>
    <% } %>
 <td class="detaillabel" >
        <yfc:i18n>Serial_Number</yfc:i18n>
    </td>
	<% if (!isVoid(modifyView)) {%>
       <td nowrap="true"><input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@SystemSerialNo")%>"  <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@SystemSerialNo", "xml:/OrderLine/Extn/@SystemSerialNo", "xml:/OrderLine/AllowedModifications")%> /></td>


    <% } else { %>
        <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@SystemSerialNo"/>&nbsp;</td>
    <% } %>
	<td class="detaillabel" >
        <yfc:i18n>Product_Family</yfc:i18n>
    </td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/Extn/@SystemProductFamily"></yfc:getXMLValue>&nbsp;</td>
</tr>
<tr>
<td class="detaillabel" >
			<yfc:i18n>Document_Type</yfc:i18n>
		</td>
		<td class="protectedtext">
			<yfc:getXMLValueI18NDB binding="xml:DocumentParamsList:/DocumentParamsList/DocumentParams/@Description"></yfc:getXMLValueI18NDB>
		</td>
</tr>
</table>

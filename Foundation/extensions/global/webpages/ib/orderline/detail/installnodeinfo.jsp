<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<table class="view" width="100%" >
    <tr>
       <td class="detaillabel"  >
        <yfc:i18n>Install_Node</yfc:i18n>
    </td>
    
       <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/@ReceivingNode"></yfc:getXMLValue>&nbsp;</td>    
	</tr>
 <td class="detaillabel" >
        <yfc:i18n>Install_Node_Address</yfc:i18n>
    </td>

<%
String sFilename="address_" + getLocale().getCountry()+".jsp";
java.net.URL oURL=pageContext.getServletContext().getResource("/yfsjspcommon/"+sFilename);
%>

<% if (equals("Y", (String) request.getParameter("ShowAnswerSetOptions"))) {
    String answerOptionsBinding = (String) request.getParameter("AnswerSetOptionsBinding");
%>
    <input type="hidden" name="AnswerSetOptionsBinding" value='<%=HTMLEncode.htmlEscape(answerOptionsBinding)%>'/>
<% } %>

<% if (oURL != null) { %>
    <jsp:include page="<%=sFilename%>" flush="true" />
<% } else { %>
    <jsp:include page="/yfsjspcommon/address_default.jsp" flush="true" />
<% } %>
</table>

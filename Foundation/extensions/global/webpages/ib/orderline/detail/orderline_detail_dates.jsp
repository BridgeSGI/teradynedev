<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<table class="view" width="40%" >
  <td class="tablecolumn" nowrap="true">
 <yfc:i18n>Expected_Install</yfc:i18n></td><td > <yfc:getXMLValue binding="xml:/OrderLine/Extn/@ExpectedInsDate"/></td>
<td>
<%
YFCDate Actdate=null;
YFCDate Expdate=null;
if (!isVoid(getValue("OrderLine","xml:/OrderLine/Extn/@ActualInsDate"))) {
 Actdate = new YFCDate(getDateValue("xml:/OrderLine/Extn/@ActualInsDate"));


if (!isVoid(getValue("OrderLine","xml:/OrderLine/Extn/@ExpectedInsDate")))
	{
 Expdate = new YFCDate(getDateValue("xml:/OrderLine/Extn/@ExpectedInsDate"));

long Diff= Actdate.getTime() - Expdate.getTime();

if(Diff>0)
	
{ 
String status=getValue("OrderLine","xml:/OrderLine/@Status");	
String Ent=getValue("Order","xml:/Order/@EnterpriseCode");	
YFCElement inputElemST = YFCDocument.parse("<CommonCode CallingOrganizationCode=\"" + Ent + "\" CodeShortDescription=\""+status+"\"  CodeType=\"IBStatus\" />").getDocumentElement();
    YFCElement templateElemST = YFCDocument.parse("<CommonCodeList ><CommonCode CodeValue=\"\" /></CommonCodeList >").getDocumentElement();

%>
<yfc:callAPI apiName="getCommonCodeList" inputElement="<%=inputElemST%>" templateElement="<%=templateElemST%>" outputNamespace="StatusList"/>
<%String statusNo=getValue("StatusList","xml:/CommonCodeList/CommonCode/@CodeValue");

if(statusNo.equals("1100.10")){
	
%>



<a <%=getDetailHrefOptions("L17", getParameter("orderLineKey"), "")%>>
<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/appointmentwaiting.gif", "Late Installation Request")%>></a>
<%}
}
}

}%>
</td>
<tr>
 <td class="tablecolumn" nowrap="true"><yfc:i18n>Actual_Install</yfc:i18n></td>

					
				   <td class="tablecolumn" nowrap="true">
				   <input type="text" class="dateinput" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ActualInsDate")%>"  <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@ActualInsDate", "xml:/OrderLine/Extn/@ActualInsDate", "xml:/OrderLine/AllowedModifications")%>/>
							<img class="lookupicon" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar", "xml:/OrderLine/Extn/@ActualInsDate", "xml:/OrderLine/AllowedModifications")%>/>
							
                    </td>   
					</tr>
</table>

<%@ include file="/yfsjspcommon/yfsutil.jspf" %> <%@ include file="/console/jsp/modificationutils.jspf" %> <%@ include file="/console/jsp/order.jspf" %> <%@ page import="com.yantra.yfs.ui.backend.*" %> <%@ include file="/yfsjspcommon/editable_util_lines.jspf" %> <script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script> <% String isCoverage="N"; boolean bisDraft=true; %>



<table class="view" width="100%"> <tr>

<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Coverage")%>"><yfc:i18n>Coverage</yfc:i18n></td> <td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Coverage")%>"><yfc:i18n>Is_Re_Serial</yfc:i18n></td> <td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>MFG_ID</yfc:i18n></td>	<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Asset_ID</yfc:i18n></td>	<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>SO Line#</yfc:i18n></td>	<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@ReqShipDate")%>"><yfc:i18n>Program</yfc:i18n></td>



<td class="tablecolumnheader"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@RCenter")%>"><yfc:i18n>Enable_At_R_Center</yfc:i18n></td>	<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@IBStatus")%>"><yfc:i18n>Mkt_Status</yfc:i18n></td>	<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@IBStatus")%>"><yfc:i18n>Processing_Status</yfc:i18n></td>	<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@IBStatus")%>"><yfc:i18n>Build_Status</yfc:i18n></td>	

</tr> <tr> <tbody>

<yfc:hasXMLNode binding="xml:TerSCIBMappingList:/TerSCIBMappingList/TerSCIBMapping">

<yfc:loopXML name="TerSCIBMappingList" binding="xml:/TerSCIBMappingList/@TerSCIBMapping" id="SCOrderLine">

<% if (equals(getValue("SCOrderLine","xml:/TerSCIBMapping/YFSSCOrderLine/@MaxLineStatusDesc"),"Draft Order Created") ){

bisDraft=false;	%>

<% } else { isCoverage="Y";%> <%}%> </yfc:loopXML> </yfc:hasXMLNode> <%if(!bisDraft){ %> <td class="protectedtext"><%=isCoverage %>&nbsp <a <%=getDetailHrefOptions("L16", getParameter("orderLineKey"), "")%>> <img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/WarningIndicatorColumn.gif", "Service_Contracts_need_attention")%>> </td>

<%} else{ %> <td class="protectedtext"><%=isCoverage %></td> <%}%> <td class="tablecolumn"> <input class="checkbox" type="checkbox" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@IsReSerial")%>" <%=yfsGetCheckBoxOptions("xml:/Order/OrderLines/OrderLine/Extn/@IsReSerial","xml:/OrderLine/Extn/@IsReSerial","Y","xml:/OrderLine/AllowedModifications")%>/></td> <td  nowrap="true"><input type="text" class="Unprotectedinput" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@MfgID")%>"  <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@MfgID","xml:/OrderLine/Extn/@MfgID","xml:/OrderLine/AllowedModifications")%>/> </td> <td  nowrap="true"><input type="text" class="Unprotectedinput"  OldValue="<%=resolveValue("xml:/OrderLine/Extn/@AssetID")%>"  <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@AssetID","xml:/OrderLine/Extn/@AssetID","xml:/OrderLine/AllowedModifications")%>/> </td> <td  nowrap="true"><input type="text" class="Unprotectedinput"  OldValue="<%=resolveValue("xml:/OrderLine/Extn/@SOLineNumber")%>"  <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@SOLineNumber","xml:/OrderLine/Extn/@SOLineNumber","xml:/OrderLine/AllowedModifications")%>/> </td> <td class="tablecolumn"> <select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@SysProgram")%>" <%=yfsGetComboOptions("xml:/Order/OrderLines/OrderLine/Extn/@SysProgram", "xml:/OrderLine/AllowedModifications")%>> <yfc:loopOptions binding="xml:/TDYNSrvcOvrList/@TDYNSrvcOvr" name="SystemProgram" value="SystemProgram" selected="xml:/OrderLine/Extn/@SysProgram" isLocalized="Y"/> </select> </td> <td class="tablecolumn"> <select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@RapidSourceCenter")%>" <%=getComboOptions("xml:/Order/OrderLines/OrderLine/Extn/@RapidSourceCenter")%>>

<yfc:loopOptions binding="xml:organizationlist:/OrganizationList/@Organization" name="OrganizationCode" value="OrganizationCode" selected="xml:/OrderLine/Extn/@RapidSourceCenter" isLocalized="Y"/>	

</select> </td>

<td class="tablecolumn"> <select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@MktStatus")%>" <%=getComboOptions("xml:/Order/OrderLines/OrderLine/Extn/@MktStatus")%>>

<yfc:loopOptions binding="xml:ibmarketstatuslist:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/Extn/@MktStatus" isLocalized="Y"/> </select> </td> <td class="tablecolumn"> <select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ProcessingStatus")%>" <%=getComboOptions("xml:/Order/OrderLines/OrderLine/Extn/@ProcessingStatus")%>>

<yfc:loopOptions binding="xml:processingStatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/Extn/@ProcessingStatus" isLocalized="Y"/>

</select>

</td> <td class="tablecolumn"> <select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@IBBuildStatus")%>" <%=getComboOptions("xml:/Order/OrderLines/OrderLine/Extn/@IBBuildStatus")%>>

<yfc:loopOptions binding="xml:ibbuildstatus:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/Extn/@IBBuildStatus" isLocalized="Y"/>

</select> </td> </tbody> </tr> </table>
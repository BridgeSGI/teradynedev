<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="../console/scripts/om.js">
document.body.style.cursor='auto';
</script>

<script language="javascript">
	function setLookupValueForSerial(val,itemId, orderlinekey, orderheaderkey,program){
		var obj = window.dialogArguments;
		if(obj != null){
			if(obj.field1 != null) {obj.field1.value=val;}
			if(obj.field2 != null) {obj.field2.value=itemId;}
			if(obj.field3 != null) {obj.field3.value=orderlinekey;}
			if(obj.field4 != null) {obj.field4.value=orderheaderkey;}
			if(obj.field5 != null) {obj.field5.value=program}
		}
		window.close();
	}

	function displaybasedonValue(){
		var obj = window.dialogArguments;
		if(obj!=null){
			var scolKey = document.getElementById('terSCOLKey');
			var scohKey = document.getElementById('terSCOHKey');
			if (scolKey) {
				scolKey.value = obj.TerSCOLKey;
			}
			if (scohKey) {
				scohKey.value = obj.TerSCOHKey;
			}
		}
		
		if(obj!=null && obj.lookup){
			var lists = document.all('csoList');
			if(lists != null){
				if(lists.length == null){
					lists.style.display='none';
				}else{
					for(i=0;i<lists.length;i++){
						lists[i].style.display='none';
					}
				}				
			}
		}else{
			var lookups = document.all('csoLookup');
			if(lookups != null){
				if(lookups.length == null){
					lookups.style.display='none';
				}else{
					for(i=0;i<lookups.length;i++){
						lookups[i].style.display='none';
					}
				}
				
			}
		}		
	}
	window.attachEvent('onload', displaybasedonValue);
</script>

<table class="table" cellpadding="0" cellspacing="0" width="100%">
<thead> 
    <tr> 
        <td sortable="no" class="checkboxheader">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader"><yfc:i18n>Serial_#</yfc:i18n></td>            
        <td class="tablecolumnheader"><yfc:i18n>Install_Base_#</yfc:i18n></td>
        <td class="tablecolumnheader"><yfc:i18n>Item_ID</yfc:i18n></td>
        <td class="tablecolumnheader"><yfc:i18n>Item_Type</yfc:i18n></td>
        <td class="tablecolumnheader"><yfc:i18n>Product_Family</yfc:i18n></td>
        <td class="tablecolumnheader"><yfc:i18n>Coverage</yfc:i18n></td>
		 <td class="tablecolumnheader"><yfc:i18n>Owner_Org</yfc:i18n></td>
        <td class="tablecolumnheader"><yfc:i18n>Install_Node</yfc:i18n></td>
        <td class="tablecolumnheader"><yfc:i18n>Install_Node_Address</yfc:i18n></td>
        <td class="numerictablecolumnheader"><yfc:i18n>Sales_Order</yfc:i18n></td>
        <td class="tablecolumnheader"><yfc:i18n>Status</yfc:i18n></td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="OrderLineList" binding="xml:/OrderLineList/@OrderLine" id="OrderLine">
	<%
		String isCoverage="N";
		boolean bisDraft=true;
		%>

	<% if(!isVoid(resolveValue("xml:OrderLine:/OrderLine/@Status"))) {%>
        <yfc:makeXMLInput name="orderLineKey">
            <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey" />
            <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/OrderLine/@OrderHeaderKey" />
        </yfc:makeXMLInput>
        <tr> 
        <td class="checkboxcolumn" id="csoList">
            <input type="checkbox" value='<%=resolveValue("orderLineKey")%>' name="EntityKey">
			<input type="hidden" name='OrderLineKey_<%=OrderLineCounter%>' value='<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>' />
			<input type="hidden" name='OrderHeaderKey_<%=OrderLineCounter%>' value='<%=resolveValue("xml:/OrderLine/@OrderHeaderKey")%>' />
			<input type="hidden" id="terSCOLKey" name="xml:/OrderLineDetail/@TerSCOLKey" />
			<input type="hidden" id="terSCOHKey" name="xml:/OrderLineDetail/@TerSCOHKey" />

        </td>
		<td class="checkboxcolumn" id="csoLookup" style="display:block;">
			<img class="icon" onClick="setLookupValueForSerial(this.value,'<%=resolveValue("xml:/OrderLine/Item/@ItemID")%>','<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>','<%=resolveValue("xml:/OrderLine/@OrderHeaderKey")%>','<%=resolveValue("xml:/OrderLine/Extn/@SysProgram")%>');" value='<%=resolveValue("xml:/OrderLine/Extn/@SystemSerialNo")%>' <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%>/>
        </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@SystemSerialNo"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Order/@OrderNo"/> </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/PrimaryInformation/@ItemType"/></td>
       <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/Extn/@SystemProductFamily"/></td>
        <% 
	
	String sIBOHKey = resolveValue("xml:/OrderLine/@OrderHeaderKey");
	String sIBOLKey=resolveValue("xml:/OrderLine/@OrderLineKey");
	YFCElement inputElemSC = YFCDocument.parse("<TerSCIBMapping TerIBOHKey=\"" + sIBOHKey + "\" TerIBOLKey=\""+sIBOLKey+"\"   />").getDocumentElement();
    YFCElement templateElemSC = YFCDocument.parse("<TerSCIBMappingList ><TerSCIBMapping /></TerSCIBMappingList >").getDocumentElement();
	
%>

<yfc:callAPI serviceName="GetIBForLine" inputElement="<%=inputElemSC%>" templateElement="<%=templateElemSC%>" outputNamespace="TerSCIBMappingList"/>
<yfc:hasXMLNode binding="xml:TerSCIBMappingList:/TerSCIBMappingList/TerSCIBMapping">

<yfc:loopXML name="TerSCIBMappingList" binding="xml:/TerSCIBMappingList/@TerSCIBMapping" id="SCOrderLine">

<% if (equals(getValue("SCOrderLine","xml:/TerSCIBMapping/YFSSCOrderLine/@MaxLineStatusDesc"),"Draft Order Created") ){ bisDraft=false;%>
<%  }
else { isCoverage="Y";%>
<%}%>
</yfc:loopXML>

</yfc:hasXMLNode> 

<%if(!bisDraft){ %>
<td class="protectedtext"><%=isCoverage %>&nbsp
<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/WarningIndicatorColumn.gif", "Service_Contracts_need_attention")%>>
</td>

<%}
else{ %>
<td class="protectedtext"><%=isCoverage %></td>
<%}%>
		<!--<td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@TesterOwnerOrgID"/></td>!-->
		 <td class="tablecolumn">
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/OrderLine/Extn/@TesterOwnerOrgID" />
					</yfc:makeXMLInput>
					<a href="javascript:yfcShowDetailPopup('YPMD040', '', 450,300,'','', '<%=resolveValue("OrganizationKey")%>');" >
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@TesterOwnerOrgID"/>
					</a>
					</td>
					 <td class="tablecolumn">
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/OrderLine/@ReceivingNode" />
					</yfc:makeXMLInput>

					<a href="javascript:yfcShowDetailPopup('YPMD040', '', 450,300,'','', '<%=resolveValue("OrganizationKey")%>');" >
					<yfc:getXMLValue binding="xml:/OrderLine/@ReceivingNode"/>
					</a>
					</td>
					
<!--call api to get address for install base and append all to text in text area-->

		<%
String sShipNode=null;    
String sPersonID=null;
String PersInfokey=null;

sShipNode = resolveValue("xml:/OrderLine/@ReceivingNode");
String Path = "xml:PersonInfoList/PersonInfo/";
String xmlName="PersonInfoList";
   String company =null;
    String firstName =null ;
    String middleName =null ;
    String lastName =null ;
    String addressLine1 =null ;
    String addressLine2 =null ;
    String addressLine3 =null ;
    String addressLine4 =null ;
    String addressLine5 =null ;
    String addressLine6 =null ;
    String city =null ;
    String state =null ;
    String postalCode =null ;
    String country =null ;
    String dayphone =null ;
    String eveningphone =null;;
    String mobilephone =null;
    String fax =null ;
    String email =null ;
    
    String alternateEmailID =null;
    String beeper =null;
    String department =null ;
    String eveningFaxNo =null ;
    String jobTitle =null;
    String otherPhone =null ;
    String suffix =null ;
    String title =null ;
%>

		   <% 
	if(sShipNode !="")
	{
	YFCElement inputElem = YFCDocument.parse("<Organization OrganizationKey=\"" + sShipNode + "\"   />").getDocumentElement();
    YFCElement templateElem = YFCDocument.parse("<Organization ><CorporatePersonInfo PersonInfoKey=\"\" /></Organization >").getDocumentElement();
	
%>

	
       <yfc:callAPI apiName="getOrganizationHierarchy" inputElement="<%=inputElem%>" templateElement="<%=templateElem%>" outputNamespace="Organization"/>
		   <% PersInfokey = resolveValue("xml:/Organization/CorporatePersonInfo/@PersonInfoKey"); 
			

	YFCElement inputElemPrsnInfo = YFCDocument.parse("<PersonInfo  PersonInfoKey=\"" +PersInfokey + "\" />").getDocumentElement();
    YFCElement templateElemPrsnInfo = YFCDocument.parse("<PersonInfoList TotalNumberOfRecords=\"\"><PersonInfo AddressID=\"\" AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\" AddressLine6=\"\" AlternateEmailID=\"\" City=\"\" Company=\"\" Country=\"\" DayFaxNo=\"\" DayPhone=\"\" Department=\"\" EMailID=\"\" EveningFaxNo=\"\" EveningPhone=\"\" FirstName=\"\" HttpUrl=\"\" IsCommercialAddress=\"\" JobTitle=\"\" LastName=\"\" MiddleName=\"\" MobilePhone=\"\" OtherPhone=\"\"  State=\"\"  Title=\"\" ZipCode=\"\" /></PersonInfoList>").getDocumentElement();




%>

<yfc:callAPI apiName="getPersonInfoList" inputElement="<%=inputElemPrsnInfo%>" templateElement="<%=templateElemPrsnInfo%>" outputNamespace="PersonInfoList"/>
		   <%
			
     company = resolveValue(Path+"@Company");
     firstName = resolveValue(Path+"@FirstName");
     middleName = resolveValue(Path+"@MiddleName");
     lastName = resolveValue(Path+"@LastName");
     addressLine1 = resolveValue(Path+"@AddressLine1");
     addressLine2 = resolveValue(Path+"@AddressLine2");
     addressLine3 = resolveValue(Path+"@AddressLine3");
     addressLine4 = resolveValue(Path+"@AddressLine4");
     addressLine5 = resolveValue(Path+"@AddressLine5");
     addressLine6 = resolveValue(Path+"@AddressLine6");

     city = resolveValue(Path+"@City");
     state = resolveValue(Path+"@State");
     postalCode = resolveValue(Path+"@ZipCode");
     country = resolveValue(Path+"@Country");
     dayphone = resolveValue(Path+"@DayPhone");
     eveningphone = resolveValue(Path+"@EveningPhone");
     mobilephone = resolveValue(Path+"@MobilePhone");
     fax = resolveValue(Path+"@DayFaxNo");
     email = resolveValue(Path+"@EMailID");
    
     alternateEmailID = resolveValue(Path+"@AlternateEmailID");
     beeper = resolveValue(Path+"@Beeper");
     department = resolveValue(Path+"@Department");
     eveningFaxNo = resolveValue(Path+"@EveningFaxNo");
     jobTitle = resolveValue(Path+"@JobTitle");
     otherPhone = resolveValue(Path+"@OtherPhone");
     suffix = resolveValue(Path+"@Suffix");
     title = resolveValue(Path+"@Title");
    

%>




<%}%> 
<td valign="top" class="protectedtext">
            <% if(!isVoid(company)) { %> 
                <%=company%>
                <br/>
                <% } %>
            <% if(!isVoid(firstName)) { %> 
                <%=firstName%>
            <% } %>
            <% if(!isVoid(middleName)) { %> 
                <%=middleName%>
            <% } %>
            <% if(!isVoid(lastName)) { %> 
                <%=lastName%>
            <% } if ((!isVoid(firstName)) ||
                    (!isVoid(middleName)) ||
                    (!isVoid(lastName))) { %>
                <br/>
            <% } if(!isVoid(addressLine1)) { %> 
                <%=addressLine1%>
                <br/>
            <% } if(!isVoid(addressLine2)) { %> 
                <%=addressLine2%>
                <br/>
            <% } if(!isVoid(addressLine3)) { %> 
                <%=addressLine3%>
                <br/>
            <% } if(!isVoid(addressLine4)) { %> 
                <%=addressLine4%>
                <br>
            <% } if(!isVoid(addressLine5)) { %> 
                <%=addressLine5%>
                <br/>
            <% } if(!isVoid(addressLine6)) { %> 
                <%=addressLine6%>
                <br>
            <% } if(!isVoid(city)) { %> 
                <%=city%>
            <% } %>
            <% if(!isVoid(state)) { %> 
                <%=state%>
            <% } %>
            <% if(!isVoid(postalCode)) { %> 
                <%=postalCode%>&nbsp;
            <% } if ((!isVoid(city)) && 
                    (!isVoid(state)) && 
                    (!isVoid(postalCode))) { %>
                    <br/>
            <% } if(!isVoid(country)) { %> 
                <%=country%>
                <br/>   
            <%} %>
           
                <%if(!isVoid(dayphone)){%>
                    <b><yfc:i18n>Day_Phone</yfc:i18n>&nbsp;:&nbsp;</b><%=dayphone%>&nbsp;<br/>
                <%}%>
                <%if(!isVoid(mobilephone)){%>
                    <b><yfc:i18n>Mobile_Phone</yfc:i18n>&nbsp;:&nbsp;</b><%=mobilephone%>&nbsp;<br/>
                <%}%>
                <%if(!isVoid(eveningphone)){%>
                    <b><yfc:i18n>Evening_Phone</yfc:i18n>&nbsp;:&nbsp;</b><%=eveningphone%>&nbsp;
                <%}%>
            
        </td>
		
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Order/Extn/@SellerSalesOrdrNo"/></td>
        
        <td class="tablecolumn">
            <%=displayOrderStatus(getValue("OrderLine","xml:/OrderLine/@MultipleStatusesExist"),getValue("OrderLine","xml:/OrderLine/@MaxLineStatusDesc"))%>
            <% if (equals("Y", getValue("OrderLine", "xml:/OrderLine/@HoldFlag"))) { %>
                <img class="icon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_line_is_held")%>/>
            <% } %>
        </td>        
        </tr>
	<%} else {%> 
	<yfc:makeXMLInput name="orderLineKey">
            <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey" />
            <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/OrderLine/@OrderHeaderKey" />
        </yfc:makeXMLInput>
        <tr> 
          <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@SystemSerialNo"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Order/@OrderNo"/> </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/PrimaryInformation/@ItemType"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/Extn/@SystemProductFamily"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@IsCoverage"/></td>
		<td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@TesterOwnerOrgID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/@ReceivingNode"/></td>
        
        <td>
        </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Order/Extn/@SellerSalesOrdrNo"/></td>
        <td class="tablecolumn">
            <%=displayOrderStatus(getValue("OrderLine","xml:/OrderLine/@MultipleStatusesExist"),getValue("OrderLine","xml:/OrderLine/@MaxLineStatusDesc"))%>
            <% if (equals("Y", getValue("OrderLine", "xml:/OrderLine/@HoldFlag"))) { %>
                <img class="icon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_line_is_held")%>/>
            <% } %>
            <% if (equals("Y", getValue("OrderLine", "xml:/OrderLine/@isHistory") )){ %>
                <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.HISTORY_ORDER, "This_is_an_archived_order_line")%>/>
            <% } %>
        </td>        
        </tr>
	<%}%>
    </yfc:loopXML> 
</tbody>
</table>
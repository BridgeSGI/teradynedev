<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<%
	String forOrder = request.getParameter("forOrder");
	if (equals("Y",forOrder)) { %>
		<yfc:callAPI apiID='AP1'/>
	<% } else { %>
		<yfc:callAPI apiID='AP2'/>
	<%}%>

<%
    YFCElement root = (YFCElement)request.getAttribute("OrganizationList");
    int countElem = countChildElements(root);
%>
<script language="javascript">
    setRetrievedRecordCount(<%=countElem%>);
</script>

<table class="table" width="100%">
<thead>
    <tr> 
        <td class="lookupiconheader"><br /></td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/Organization/@OrganizationCode")%>">
            <yfc:i18n>Organization_Code</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/Organization/@OrganizationName")%>">
            <yfc:i18n>Organization_Name</yfc:i18n>
        </td>
        <td class="tablecolumnheader" 
            <yfc:i18n>Address</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="OrganizationList" binding="xml:/OrganizationList/@Organization" id="Organization"> 
    <tr> 
	
        <td class="tablecolumn">
            <img class="icon" onClick="setCustLookupValue('<%=resolveValue("xml:Organization:/Organization/@OrganizationCode")%>','<%=resolveValue("xml:Organization:/Organization/@OrganizationName")%>')"  value="<%=resolveValue("xml:Organization:/Organization/@OrganizationCode")%>" 
			<%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>

        <td class="tablecolumn"><yfc:getXMLValue name="Organization" binding="xml:/Organization/@OrganizationCode"/></td>
        <td class="tablecolumn"><yfc:getXMLValueI18NDB name="Organization" binding="xml:/Organization/@OrganizationName"/></td>
         <%
String sOrg=null;    
String sPersonID=null;
String PersInfokey=null;

sOrg = resolveValue("xml:/Organization/@OrganizationCode");
String Path = "xml:PersonInfoList/PersonInfo/";
String xmlName="PersonInfoList";
   String company =null;
    String firstName =null ;
    String middleName =null ;
    String lastName =null ;
    String addressLine1 =null ;
    String addressLine2 =null ;
    String addressLine3 =null ;
    String addressLine4 =null ;
    String addressLine5 =null ;
    String addressLine6 =null ;
    String city =null ;
    String state =null ;
    String postalCode =null ;
    String country =null ;
    String dayphone =null ;
    String eveningphone =null;;
    String mobilephone =null;
    String fax =null ;
    String email =null ;
    
    String alternateEmailID =null;
    String beeper =null;
    String department =null ;
    String eveningFaxNo =null ;
    String jobTitle =null;
    String otherPhone =null ;
    String suffix =null ;
    String title =null ;
%>

		   <% 
	if(sOrg !="")
	{
	YFCElement inputElem = YFCDocument.parse("<Organization OrganizationKey=\"" + sOrg + "\"   />").getDocumentElement();
    YFCElement templateElem = YFCDocument.parse("<Organization ><CorporatePersonInfo PersonInfoKey=\"\" /></Organization >").getDocumentElement();
	
%>

	
       <yfc:callAPI apiName="getOrganizationHierarchy" inputElement="<%=inputElem%>" templateElement="<%=templateElem%>" outputNamespace="Organization"/>
		   <% PersInfokey = resolveValue("xml:/Organization/CorporatePersonInfo/@PersonInfoKey"); 
			

	YFCElement inputElemPrsnInfo = YFCDocument.parse("<PersonInfo  PersonInfoKey=\"" +PersInfokey + "\" />").getDocumentElement();
    YFCElement templateElemPrsnInfo = YFCDocument.parse("<PersonInfoList TotalNumberOfRecords=\"\"><PersonInfo AddressID=\"\" AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\" AddressLine6=\"\" AlternateEmailID=\"\" City=\"\" Company=\"\" Country=\"\" DayFaxNo=\"\" DayPhone=\"\" Department=\"\" EMailID=\"\" EveningFaxNo=\"\" EveningPhone=\"\" FirstName=\"\" HttpUrl=\"\" IsCommercialAddress=\"\" JobTitle=\"\" LastName=\"\" MiddleName=\"\" MobilePhone=\"\" OtherPhone=\"\"  State=\"\"  Title=\"\" ZipCode=\"\" /></PersonInfoList>").getDocumentElement();




%>

<yfc:callAPI apiName="getPersonInfoList" inputElement="<%=inputElemPrsnInfo%>" templateElement="<%=templateElemPrsnInfo%>" outputNamespace="PersonInfoList"/>
		   <%
			
     company = resolveValue(Path+"@Company");
     firstName = resolveValue(Path+"@FirstName");
     middleName = resolveValue(Path+"@MiddleName");
     lastName = resolveValue(Path+"@LastName");
     addressLine1 = resolveValue(Path+"@AddressLine1");
     addressLine2 = resolveValue(Path+"@AddressLine2");
     addressLine3 = resolveValue(Path+"@AddressLine3");
     addressLine4 = resolveValue(Path+"@AddressLine4");
     addressLine5 = resolveValue(Path+"@AddressLine5");
     addressLine6 = resolveValue(Path+"@AddressLine6");

     city = resolveValue(Path+"@City");
     state = resolveValue(Path+"@State");
     postalCode = resolveValue(Path+"@ZipCode");
     country = resolveValue(Path+"@Country");
     dayphone = resolveValue(Path+"@DayPhone");
     eveningphone = resolveValue(Path+"@EveningPhone");
     mobilephone = resolveValue(Path+"@MobilePhone");
     fax = resolveValue(Path+"@DayFaxNo");
     email = resolveValue(Path+"@EMailID");
    
     alternateEmailID = resolveValue(Path+"@AlternateEmailID");
     beeper = resolveValue(Path+"@Beeper");
     department = resolveValue(Path+"@Department");
     eveningFaxNo = resolveValue(Path+"@EveningFaxNo");
     jobTitle = resolveValue(Path+"@JobTitle");
     otherPhone = resolveValue(Path+"@OtherPhone");
     suffix = resolveValue(Path+"@Suffix");
     title = resolveValue(Path+"@Title");
    

%>




<%}%> 
<td valign="top" class="protectedtext">
            <% if(!isVoid(company)) { %> 
                <%=company%>
                <br/>
                <% } %>
            <% if(!isVoid(firstName)) { %> 
                <%=firstName%>
            <% } %>
            <% if(!isVoid(middleName)) { %> 
                <%=middleName%>
            <% } %>
            <% if(!isVoid(lastName)) { %> 
                <%=lastName%>
            <% } if ((!isVoid(firstName)) ||
                    (!isVoid(middleName)) ||
                    (!isVoid(lastName))) { %>
                <br/>
            <% } if(!isVoid(addressLine1)) { %> 
                <%=addressLine1%>
                <br/>
            <% } if(!isVoid(addressLine2)) { %> 
                <%=addressLine2%>
                <br/>
            <% } if(!isVoid(addressLine3)) { %> 
                <%=addressLine3%>
                <br/>
            <% } if(!isVoid(addressLine4)) { %> 
                <%=addressLine4%>
                <br>
            <% } if(!isVoid(addressLine5)) { %> 
                <%=addressLine5%>
                <br/>
            <% } if(!isVoid(addressLine6)) { %> 
                <%=addressLine6%>
                <br>
            <% } if(!isVoid(city)) { %> 
                <%=city%>
            <% } %>
            <% if(!isVoid(state)) { %> 
                <%=state%>
            <% } %>
            <% if(!isVoid(postalCode)) { %> 
                <%=postalCode%>&nbsp;
            <% } if ((!isVoid(city)) && 
                    (!isVoid(state)) && 
                    (!isVoid(postalCode))) { %>
                    <br/>
            <% } if(!isVoid(country)) { %> 
                <%=country%>
                <br/>   
            <%} %>
           
                <%if(!isVoid(dayphone)){%>
                    <b><yfc:i18n>Day_Phone</yfc:i18n>&nbsp;:&nbsp;</b><%=dayphone%>&nbsp;<br/>
                <%}%>
                <%if(!isVoid(mobilephone)){%>
                    <b><yfc:i18n>Mobile_Phone</yfc:i18n>&nbsp;:&nbsp;</b><%=mobilephone%>&nbsp;<br/>
                <%}%>
                <%if(!isVoid(eveningphone)){%>
                    <b><yfc:i18n>Evening_Phone</yfc:i18n>&nbsp;:&nbsp;</b><%=eveningphone%>&nbsp;
                <%}%>
            
        </td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
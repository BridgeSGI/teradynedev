<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<table width="100%" class="view">
<tr>
    <td>
        <%	//Get neccessary params.		
			String forOrder = request.getParameter("forOrder");
			String enterprise = request.getParameter("enterprise");
			String docType = request.getParameter("docType");
			String role = request.getParameter("role");
			String roleSelected = "";
			String initialRole = "";
            String sRole = resolveValue("xml:/Organization/OrgRoleList/OrgRole/@RoleKey");
			if (!isVoid(role)) {
				initialRole = role;
			} else {
				initialRole = request.getParameter("initialRole");
			}
		%>
			<% //Set the following hidden attributes to retain values after each Search occurs. %>
			<input type="hidden" name="initialRole" value="<%=HTMLEncode.htmlEscape(initialRole)%>"/>
			<input type="hidden" name="forOrder" value="<%=HTMLEncode.htmlEscape(forOrder)%>"/>
			<input type="hidden" name="enterprise" value="<%=HTMLEncode.htmlEscape(enterprise)%>"/>
			<input type="hidden" name="docType" value="<%=HTMLEncode.htmlEscape(docType)%>"/>
			
		<%	
			// 1)Need to apply different bindings for the Role combo depending on 'forOrder' param. Different apis will be called.
			// 2)Set additional hidden attributes.

			if (equals("Y",forOrder)) {
		%>				
				<input type="hidden" name="xml:/Organization/OrderingInformation/@Role" value="<%=HTMLEncode.htmlEscape(initialRole)%>"/>
				<input type="hidden" name="xml:/Organization/OrgRoleList/OrgRole/@RoleKey" value="<%=HTMLEncode.htmlEscape(initialRole)%>"/>
				<input type="hidden" name="xml:/Organization/OrderingInformation/@EnterpriseKey" value="<%=HTMLEncode.htmlEscape(enterprise)%>"/>
				<input type="hidden" name="xml:/Organization/OrderingInformation/@DocumentType" value="<%=HTMLEncode.htmlEscape(docType)%>"/>	
				
		<%	}
			
			roleSelected = "xml:/Organization/OrgRoleList/OrgRole/@RoleKey";

			//When first coming to this screen Role combo should be defaulted to the Role param passed.
			if (!isVoid(role)) {
				roleSelected = role;
			}

            String singleRole = "";
            if (!isVoid(request.getParameter("lookupForSingleRole"))) {
                singleRole = request.getParameter("lookupForSingleRole"); 
		%>
                <input type="hidden" name="xml:/Organization/OrgRoleList/OrgRole/@RoleKey" value="<%=HTMLEncode.htmlEscape(singleRole)%>"/>
                <input type="hidden" name="lookupForSingleRole" value="<%=HTMLEncode.htmlEscape(singleRole)%>"/>
        <% }
            
            if ((equals("Y", request.getParameter("applyDataSecurity"))) || (!isVoid(resolveValue("xml:/Organization/DataAccessFilter/@UserId")))) { %>
                <input type="hidden" name="xml:/Organization/DataAccessFilter/@UserId" value="<%=resolveValue("xml:CurrentUser:/User/@Loginid")%>"/>
        <% } %>
    </td>
</tr>
<tr>
    <td class="searchlabel" >
        <yfc:i18n>Organization</yfc:i18n>
    </td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/Organization/@OrganizationCodeQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Organization/@OrganizationCodeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationCode") %> />
    </td>
</tr>
	



<tr>
    <td class="searchlabel" ><yfc:i18n>Organization_Name</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/Organization/@OrganizationNameQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Organization/@OrganizationNameQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationName") %> />
    </td>
</tr>

</table> 
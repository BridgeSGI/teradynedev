<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ include file="/extn/console/jsp/td.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<%
String isCoverage="N";
boolean bisDraft=true;
%>


<table class="table" width="100%" yfcMaxSortingRecords="1000" >
<tr>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Coverage</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@ItemID")%>"><yfc:i18n>Item_ID</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Coverage")%>"><yfc:i18n>Item_Description</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Coverage")%>"><yfc:i18n>Item_Type</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Coverage")%>"><yfc:i18n>Product_Family</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Coverage")%>"><yfc:i18n>Serial_No</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Coverage")%>"><yfc:i18n>Install_Node</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@ReqDeliveryDate")%>"><yfc:i18n>Expected_Install</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@ReqShipDate")%>"><yfc:i18n>Actual_Install</yfc:i18n></td>
<td class="tablecolumnheader"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@RCenter")%>"><yfc:i18n>Status</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@IBStatus")%>"><yfc:i18n>Mkt_Status</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@IBStatus")%>"><yfc:i18n>Processing_Status</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@IBStatus")%>"><yfc:i18n>Build_Status</yfc:i18n></td>
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@OwnerOrg")%>"><yfc:i18n>Owner_Org</yfc:i18n></td>	
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@OprOrg")%>"><yfc:i18n>Operator_Org</yfc:i18n></td>	
<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@ServOrg")%>"><yfc:i18n>Service_Org</yfc:i18n></td>

</tr>
<tr>
<tbody>
<yfc:hasXMLNode binding="xml:TerSCIBMappingListSC:/TerSCIBMappingList/TerSCIBMapping">

<yfc:loopXML name="TerSCIBMappingList" binding="xml:/TerSCIBMappingList/@TerSCIBMapping" id="SCOrderLine">

<% if (equals(getValue("SCOrderLine","xml:/TerSCIBMapping/YFSSCOrderLine/@MaxLineStatusDesc"),"Draft Order Created") ){
	
 bisDraft=false;	%>

<% }
else { isCoverage="Y";%>
<%}%>
</yfc:loopXML>
</yfc:hasXMLNode>
<%if(!bisDraft){ %>
<td class="protectedtext"><%=isCoverage %>&nbsp
<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/WarningIndicatorColumn.gif", "Service_Contracts_need_attention")%>>
</td>

<%}
else{ %>
<td class="protectedtext"><%=isCoverage %></td>
<%}%>
<td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID"/></td>
<td class="protectedtext"><%=getLocalizedOrderLineDescription("OrderLine")%></td>
<td class="protectedtext"><%=getLocalizedOrderLineItemTypeDescription("OrderLine")%></td>
<td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/Extn/@SystemProductFamily"/></td>
<td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@SystemSerialNo"/></td>
<td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/@ReceivingNode"/></td>
<td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@ExpectedInsDate"/></td>
<td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@ActualInsDate"/></td>

<td class="protectedtext" nowrap="true"><yfc:getXMLValue binding="xml:/OrderLine/@MaxLineStatusDesc"/></td>
<td class="protectedtext">


<yfc:getXMLValue binding="xml:ibmarketstatuslist:/CommonCodeList/CommonCode/@CodeShortDescription"/>

</td>
<td class="protectedtext">
<yfc:getXMLValue binding="xml:processingStatusList:/CommonCodeList/CommonCode/@CodeShortDescription"/>

</td>
<td class="protectedtext">
<yfc:getXMLValue binding="xml:ibbuildstatus:/CommonCodeList/CommonCode/@CodeShortDescription" />

</td>
<td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@TesterOwnerOrgID"/></td>
<td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@TesterOperatorOrgID"/></td>
<td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Extn/@TesterServicerOrgID"/></td>




</tbody>
</tr>
</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/extn/console/jsp/td.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.core.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/orgName.js"></script>


<%  
    String modifyView = request.getParameter("ModifyView");
    modifyView = modifyView == null ? "" : modifyView;

    String driverDate = getValue("OrderLine", "xml:/OrderLine/Order/@DriverDate");
	String enterpriseCodeFromCommon =getValue("OrderLine", "xml:/OrderLine/Order/@EnterpriseCode");
	 String DocType= getValue("OrderLine", "xml:/OrderLine/Order/@DocumentType");
%>

<table class="view" width="100%">
<tr>
    <td>
        <input type="hidden" name="xml:/Order/@ModificationReasonCode"/>
        <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
        <input type="hidden" name="xml:/Order/@Override" value="N"/>
		<input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
        <input type="hidden" name="xml:/OrderRelease/@ModificationReasonCode"/>
        <input type="hidden" name="xml:/OrderRelease/@ModificationReasonText"/>
        <input type="hidden" name="xml:/OrderRelease/@Override" value="N"/>
        <input type="hidden" name="hiddenDraftOrderFlag" value='<%=getValue("OrderLine", "xml:/OrderLine/Order/@DraftOrderFlag")%>'/>
        <input type="hidden" <%=getTextOptions("xml:/Order/@OrderHeaderKey","xml:/OrderLine/@OrderHeaderKey")%> />
        <input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine/@OrderLineKey","xml:/OrderLine/@OrderLineKey")%> />

    </td>
</tr>
<tr>
    

	<td class="detaillabel" ><yfc:i18n>Enterprise</yfc:i18n></td>
        <td class="protectedtext" ><yfc:getXMLValue binding="xml:/OrderLine/Order/@EnterpriseCode"/></td>
    <td class="detaillabel" >
        <yfc:i18n>Install_Base_ID_#</yfc:i18n>
    </td>
    <td class="protectedtext">
		<% if(showOrderNo("OrderLine","Order")) {%>
		    <a <%=getDetailHrefOptions("L01",getParameter("orderKey"),"")%>>
				<yfc:getXMLValue binding="xml:/OrderLine/Order/@OrderNo"></yfc:getXMLValue>
			</a>&nbsp;
		<%} else {%>
			<yfc:getXMLValue binding="xml:/OrderLine/Order/@OrderNo"></yfc:getXMLValue>
		<%}%>
    </td>
   <td class="detaillabel" >
        <yfc:i18n>Ship_Date</yfc:i18n>
    </td>
   
 
        <td class="protectedtext"><yfc:getXMLValue binding="xml:/OrderLine/Order/Extn/@TdShipDate"/>&nbsp;</td>




</tr>
<tr>
<td class="detaillabel" ><yfc:i18n>Customer_Org_ID</yfc:i18n></td>
					 <td class="protectedtext">
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/Order/@BuyerOrganizationCode" />
					</yfc:makeXMLInput>
					<a <%=getDetailHrefOptions("L03",getParameter("OrganizationKey"),"")%> >
						<yfc:getXMLValue binding="xml:/OrderLine/Order/@BuyerOrganizationCode"/>
					</a>
				</td>
		 <td class="detaillabel" ><yfc:i18n>Customer_Site_Org_ID</yfc:i18n></td>
		<td class="protectedtext">
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/Order/@ReceivingNode" />
					</yfc:makeXMLInput>
					<a <%=getDetailHrefOptions("L03",getParameter("OrganizationKey"),"")%> >
						<yfc:getXMLValue binding="xml:/OrderLine/Order/@ReceivingNode"/>
					</a>
				</td>
        <td class="detaillabel" ><yfc:i18n>Seller_Org_ID</yfc:i18n></td>
			<td class="protectedtext" nowrap="true"	>
					<yfc:makeXMLInput name="OrganizationKey" >
						<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:/Order/@SellerOrganizationCode" />
					</yfc:makeXMLInput>
					<a <%=getDetailHrefOptions("L03",getParameter("OrganizationKey"),"")%> >
						<yfc:getXMLValue binding="xml:/OrderLine/Order/@SellerOrganizationCode"/>
					</a>
				</td>
</tr>


</table>

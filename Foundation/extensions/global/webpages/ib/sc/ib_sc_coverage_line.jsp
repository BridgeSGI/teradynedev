<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/css/scripts/editabletbl.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>

<table class="table" ID="OrderLines" cellspacing="0" width="100%" yfcMaxSortingRecords="1000" >
	<thead>
		<tr>
		<td class="checkboxheader" sortable="no" style="width:30px">
		<input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>
		</td>
			<td class="tablecolumnheader" nowrap="true"style="width:<%=getUITableSize("xml:/OrderLine/@OrderNo")%>"><yfc:i18n>TD_SC_ContractNo</yfc:i18n></td>
			 <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>TD_SC_LineNo</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemID")%>"><yfc:i18n>TD_SI_ItemID</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>"><yfc:i18n>Status</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>"><yfc:i18n>TD_SC_EffDate</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"  id="expdate"	style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>"><yfc:i18n>TD_SC_ExpDate</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@CoveredProductFamily")%>"><yfc:i18n>TD_SC_CPF</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true"style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>TD_SCT_NRPC</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>TD_SCT_RPC</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STORepairListDiscount")%>"><yfc:i18n>TD_SCT_RLD</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOExpediteListDiscount")%>"><yfc:i18n>TD_SCT_SED</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"  style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>"><yfc:i18n>TD_SCT_AD</yfc:i18n></td>
	</tr>
	</thead>
	<tbody>
	 <yfc:loopXML name="TerSCIBMappingList" binding="xml:/TerSCIBMappingList/@TerSCIBMapping" id="TerSCIBMapping">
	
		<tr>
			<yfc:makeXMLInput name="orderLineSCKey">
				<yfc:makeXMLKey binding="xml:/TerSCIBMapping/@TerSCIBKey" value="xml:/TerSCIBMapping/@TerSCIBKey"/>
				<yfc:makeXMLKey binding="xml:/TerSCIBMapping/@TerSCOHKey" value="xml:/TerSCIBMapping/@TerSCOHKey"/>
				<yfc:makeXMLKey binding="xml:/TerSCIBMapping/@TerSCOLKey" value="xml:/TerSCIBMapping/@TerSCOLKey"/>
			</yfc:makeXMLInput>
			<td class="checkboxcolumn" >
				<input type="checkbox" value='<%=getParameter("orderLineSCKey")%>' name="chkEntityKeySC"  />
			</td>
				
			<td class="protectedtext">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Order/@OrderNo" />
			</td>
				<td class="protectedtext">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/@PrimeLineNo" />
			</td>
			<td class="protectedtext">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Item/@ItemID" />
			</td>
						<td class="protectedtext" nowrap="true">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/@MaxLineStatusDesc" />
			</td>
			<td class="protectedtext" nowrap="true" sortValue="<%=getDateValue("xml:TerSCIBMapping:/TerSCIBMapping/YFSSCOrderLine/Order/Extn/@EffectiveDate")%>">
			<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Order/Extn/@EffectiveDate" /></td>
			<td class="protectedtext" nowrap="true" sortValue="<%=getDateValue("xml:TerSCIBMapping:/TerSCIBMapping/YFSSCOrderLine/Order/Extn/@ExpirationDate")%>">
			<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Order/Extn/@ExpirationDate" /></td>
			<td class="protectedtext">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Extn/@CoveredProductFamily" />
			</td>
						<td class="protectedtext">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Extn/@NonrepairPartsCovered" />
			</td>
			<td class="protectedtext">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Extn/@RepairablePartsCovered" />
			</td>
			<td class="protectedtext">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Extn/@STORepairListDiscount" />
			</td>
			<td class="protectedtext">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Extn/@STOExpediteListDiscount" />
			</td>
			<td class="protectedtext">
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Extn/@STOAdditionalDiscount" />
			</td>
		</tr>
		 </yfc:loopXML>
	</tbody>
	<script>
    window.onload = function() 
	{
		
        document.getElementById('expdate').click();
	
    }
</script>
</table>

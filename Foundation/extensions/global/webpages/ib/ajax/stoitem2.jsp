<%@taglib prefix="yfc" uri="/WEB-INF/yfc.tld"%>
<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/inventory.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.util.*" %>


<%
//response.setContentType("text/xml");
String sItemId = request.getParameter("text");
String sUOM = request.getParameter("UOM");

String sOrgCode = request.getParameter("EnterpriseCode");

String inputString = "<Item ItemID=\""+sItemId+"\" OrganizationCode=\""+sOrgCode+"\"  UnitOfMeasure=\""+sUOM+"\"><PrimaryInformation ItemType=\"SYSTEM\" /></Item>";
String templateString = "<ItemList TotalItemList=\"\"><Item  ItemID=\"\"><PrimaryInformation ShortDescription=\"\" ItemType=\"\" /></Item></ItemList>";
//System.out.println("Input is :"+inputString);
//System.out.println("template is :"+templateString);

YFCDocument inputDoc = YFCDocument.parse(inputString);
YFCDocument templateDoc = YFCDocument.parse(templateString);
YFCElement itemDetailsElem = null;
String records="";
String result = "";

if(!isVoid(sItemId) && !isVoid(sUOM) && !isVoid(sOrgCode)){
	//System.out.println("Can call api :"+sItemId+"|"+sUOM+"|"+sOrgCode+"|"+sNode);
%>
	<yfc:callAPI apiName="getItemList" inputElement='<%=inputDoc.getDocumentElement()%>' templateElement='<%=templateDoc.getDocumentElement()%>' outputNamespace="ItemDetails"/>
	<% 

	
	records=resolveValue("xml:ItemDetails:/ItemList/@TotalItemList");
	
	itemDetailsElem = (YFCElement)request.getAttribute("ItemDetails");
	

if("1".equals(records))
	{
result = resolveValue("xml:ItemDetails:/ItemList/Item/PrimaryInformation/@ItemType");


}
else
	{
itemDetailsElem.setAttribute("ErrorDesc","Invalid Item");

}


}	

	
	out.println(itemDetailsElem.getString(false));
%>
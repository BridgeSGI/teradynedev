<%@taglib prefix="yfc" uri="/WEB-INF/yfc.tld"%>
<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/inventory.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.util.*" %>


<%
//response.setContentType("text/xml");
String sOrgId = request.getParameter("text");
//String sRole=request.getParameter("validationElement");
String sRole="SELLER";
System.out.println("Passed values are "+sOrgId+"|"+sRole);
String inputString = "<Organization OrganizationCode=\""+sOrgId+"\"><OrgRoleList><OrgRole RoleKey=\""+sRole+"\"/></OrgRoleList></Organization>";
String templateString = "<OrganizationList><Organization OrganizationCode=\"\" OrganizationKey=\"\" OrganizationName=\"\"/></OrganizationList>";
YFCDocument inputDoc = YFCDocument.parse(inputString);
YFCDocument templateDoc = YFCDocument.parse(templateString);
YFCElement orgListElem = null;
String records="";
String resultname = "";

if(!isVoid(sOrgId)){
	
	%>
	<yfc:callAPI apiName="getOrganizationList" inputElement='<%=inputDoc.getDocumentElement()%>' templateElement='<%=templateDoc.getDocumentElement()%>' outputNamespace="OrgList"/>
	<% 
	orgListElem = (YFCElement)request.getAttribute("OrgList");


if(orgListElem.hasChildNodes())
	
{


resultname = resolveValue("xml:OrgList:/OrganizationList/Organization/@OrganizationName");
orgListElem.setAttribute("orgName",resultname);


}
else
	{
orgListElem.setAttribute("ErrorDesc","Invalid Organization");

}
	}	
out.println(orgListElem.getString(false));
%>

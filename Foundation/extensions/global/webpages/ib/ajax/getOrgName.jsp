<%@taglib prefix="yfc" uri="/WEB-INF/yfc.tld"%>
<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/inventory.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.util.*" %>


<%
//response.setContentType("text/xml");
String sOrgId = request.getParameter("text");
String sEnterpriseCode = request.getParameter("enterpriseCode");
//String sRole=request.getParameter("validationElement");
String sRole="BUYER";
String sRole1="NODE";

String inputString = "<Organization OrganizationCode=\""+sOrgId+"\"><OrgRoleList><OrgRole RoleKey=\""+sRole+"\"/><OrgRole RoleKey=\""+sRole1+"\"/></OrgRoleList></Organization>";
String templateString = "<OrganizationList><Organization OrganizationCode=\"\" OrganizationKey=\"\" OrganizationName=\"\"/></OrganizationList>";
YFCDocument inputDoc = YFCDocument.parse(inputString);
YFCDocument templateDoc = YFCDocument.parse(templateString);
YFCElement orgListElem = null;
String records="";
String resultname = "";

if(!isVoid(sOrgId)){
	
	%>
	<yfc:callAPI apiName="getOrganizationList" inputElement='<%=inputDoc.getDocumentElement()%>' templateElement='<%=templateDoc.getDocumentElement()%>' outputNamespace="OrgList"/>
	<% 
	orgListElem = (YFCElement)request.getAttribute("OrgList");

	if(!orgListElem.hasChildNodes()){
		orgListElem.setAttribute("ErrorDesc","Invalid Organization");
	}else{
		resultname = resolveValue("xml:OrgList:/OrganizationList/Organization/@OrganizationName");
		orgListElem.setAttribute("orgName",resultname);
	
		if(!isVoid(sEnterpriseCode)){
			String input = "<Customer CustomerID=\""+sOrgId+"\"  OrganizationCode=\""+sEnterpriseCode+"\" />";
			String template = "<CustomerList><Customer CustomerID=\"\" ><Extn DefaultCurrency=\"\"/></Customer></CustomerList>";

			YFCDocument yfcInputDoc = YFCDocument.parse(input);
			YFCDocument yfcTemplateDoc = YFCDocument.parse(template);
	
		%>

			<yfc:callAPI apiName="getCustomerList" inputElement='<%=yfcInputDoc.getDocumentElement()%>' templateElement='<%=yfcTemplateDoc.getDocumentElement()%>' outputNamespace="CustomerList"/>

		<%
			YFCElement customerListEle = getElement("CustomerList");
			if(orgListElem.hasChildNodes()){
				resultname = resolveValue("xml:CustomerList:/CustomerList/Customer/Extn/@DefaultCurrency");
				orgListElem.setAttribute("currency",resultname);
			}	
		}	
	}
}
out.println(orgListElem.getString(false));
%>

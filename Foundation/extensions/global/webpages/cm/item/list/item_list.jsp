<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<table class="table" width="100%" editable="false">
<thead>
   <tr> 
        <td class="lookupiconheader" sortable="no"><br /></td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/@ItemID")%>">
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/PrimaryInformation/@DefaultProductClass")%>">
            <yfc:i18n>Default_PC</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/@UnitOfMeasure")%>">
            <yfc:i18n>UOM</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/PrimaryInformation/@ShortDescription")%>">
            <yfc:i18n>Short_Description</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/PrimaryInformation/@ManufacturerItem")%>">
            <yfc:i18n>Manufacturer_Item</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/PrimaryInformation/@ItemType")%>">
            <yfc:i18n>Item_Type</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/PrimaryInformation/@MasterCatalogID")%>">
            <yfc:i18n>Master_Catalog_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/@OrganizationCode")%>">
            <yfc:i18n>Catalog_Organization</yfc:i18n>
        </td>
   </tr>
</thead>
<tbody>
    <yfc:loopXML name="ItemList" binding="xml:/ItemList/@Item" id="item"> 
    <tr> 
        <td class="tablecolumn">
            <img class="icon" onclick="setItemLookupValue('<%=resolveValue("xml:item:/Item/@ItemID")%>','<%=resolveValue("xml:item:/Item/PrimaryInformation/@DefaultProductClass")%>','<%=resolveValue("xml:item:/Item/@UnitOfMeasure")%>')"  value="<%=resolveValue("xml:item:/Item/@ItemID")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/@ItemID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/PrimaryInformation/@DefaultProductClass"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/@UnitOfMeasure"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/PrimaryInformation/@ShortDescription"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/PrimaryInformation/@ManufacturerItem"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/PrimaryInformation/@ItemType"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/PrimaryInformation/@MasterCatalogID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/@OrganizationCode"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
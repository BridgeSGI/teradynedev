<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/Category/@CategoryID")%>">
            <yfc:i18n>Category_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/Category/@Description")%>">
            <yfc:i18n>Description</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Category_Path</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="CategoryList" binding="xml:/CategoryList/@Category" id="category"> 
    <tr> 
        <yfc:makeXMLInput name="categoryKey">
            <yfc:makeXMLKey binding="xml:/Category/@CategoryKey" value="xml:category:/Category/@CategoryKey" />
            <yfc:makeXMLKey binding="xml:/Category/@OrganizationCode" value="xml:category:/Category/@OrganizationCode" />
        </yfc:makeXMLInput>
        <td class="tablecolumn">
            <a href="javascript:showDetailFor('<%=getParameter("categoryKey")%>');">
                <yfc:getXMLValue name="category" binding="xml:/Category/@CategoryID"/>
            </a>
        </td>
        <td class="tablecolumn"><yfc:getXMLValueI18NDB name="category" binding="xml:/Category/@Description"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="category" binding="xml:/Category/@CategoryPath"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td sortable="no" class="lookupiconheader"><br /></td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/@ItemID")%>">
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/PrimaryInformation/@DefaultProductClass")%>">
            <yfc:i18n>Default_PC</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/@UnitOfMeasure")%>">
            <yfc:i18n>UOM</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ItemList/Item/PrimaryInformation/@Description")%>">
            <yfc:i18n>Description</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="ItemList" binding="xml:/ItemList/@Item" id="item"> 
    <tr> 
        <td class="tablecolumn">
            <img class="icon" onclick="setCatalogItemLookupValue('<%=resolveValue("xml:item:/Item/@ItemID")%>','<%=resolveValue("xml:item:/Item/PrimaryInformation/@DefaultProductClass")%>','<%=resolveValue("xml:item:/Item/@UnitOfMeasure")%>')"  value="<%=resolveValue("xml:item:/Item/@ItemID")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/@ItemID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/PrimaryInformation/@DefaultProductClass"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/@UnitOfMeasure"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="item" binding="xml:/Item/PrimaryInformation/@Description"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
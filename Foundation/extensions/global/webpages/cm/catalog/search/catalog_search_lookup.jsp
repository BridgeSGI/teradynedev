<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/cm.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>

<script language="javascript">
	window.attachEvent("onload", setCallingOrgCode);
	
	function setCallingOrgCode(){
		var orgCode = document.all("xml:/Category/@CallingOrganizationCode");
		if(window.dialogArguments.parentWindow.defaultOrganizationCode && window.dialogArguments.parentWindow.defaultOrganizationCode != null){
			orgCode.value = window.dialogArguments.parentWindow.defaultOrganizationCode;
		}
	}
</script>


<table class="view" style="behavior:url(../css/cataloglookup.htc)">
<tr>
    <td><input type="hidden" <%=getTextOptions("xml:/Category/@ParentCategoryKey") %> /></td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Organization</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
		<input type="text" class="protectedinput" contenteditable="false" <%=getTextOptions("xml:/Category/@CallingOrganizationCode")%>/>
    </td>
</tr>

<tr>
    <td class="searchlabel" ><yfc:i18n>Category_ID</yfc:i18n></td>
</tr>
<tr>
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Category/@CategoryIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Category/@CategoryIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Category/@CategoryID") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Description</yfc:i18n></td>
</tr>
<tr >
    <td nowrap="true" class="searchcriteriacell" >
        <select name="xml:/Category/@DescriptionQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Category/@DescriptionQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Category/@Description") %> />
    </td>
</tr>
</table>
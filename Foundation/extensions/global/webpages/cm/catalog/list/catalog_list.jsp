<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/CategoryList/Category/@CategoryID")%>">
            <yfc:i18n>Category_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/CategoryList/Category/@Description")%>">
            <yfc:i18n>Description</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Category_Path</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/CategoryList/Category/@OrganizationCode")%>">
            <yfc:i18n>Catalog_Organization</yfc:i18n>
        </td>
   </tr>
</thead>
<tbody>
    <yfc:loopXML name="CategoryList" binding="xml:/CategoryList/@Category" id="category"> 
    <tr> 
        <yfc:makeXMLInput name="categoryKey">
            <yfc:makeXMLKey binding="xml:/Category/@CategoryKey" value="xml:category:/Category/@CategoryKey" />
        </yfc:makeXMLInput>
        <td class="tablecolumn">

            <a href="" onClick="openCatalogDetail('<%=getParameter("categoryKey")%>', 'Category','800','500',window.dialogArguments);return false;" >

                <yfc:getXMLValue name="category" binding="xml:/Category/@CategoryID"/>
            </a>
        </td>
        <td class="tablecolumn"><yfc:getXMLValueI18NDB name="category" binding="xml:/Category/@Description"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="category" binding="xml:/Category/@CategoryPath"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="category" binding="xml:/Category/@OrganizationCode"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
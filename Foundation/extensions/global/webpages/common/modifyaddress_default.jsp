<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/modificationutils.jspf"%>

<script language=javascript src="<%=request.getContextPath()%>/console/scripts/address.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<script language="javascript">
	yfcDoNotPromptForChanges(true);
</script>	
<script language="javascript" >
	function uncheckAll(num){
		var checkBoxes = document.getElementsByName("check");
		for(i=0;i<checkBoxes.length;i++){
			if(i != (parseInt(num)-1)){
				checkBoxes[i].checked=false;
			}
		}
	}
	
	function setValue(obj,num){
		
		uncheckAll(num);
		var addr1value= document.getElementById("addressLine1_"+num).innerHTML;
		var addr2value= document.getElementById("addressLine2_"+num).innerHTML;
		var addr3value= document.getElementById("addressLine3_"+num).innerHTML;
		var addr4value= document.getElementById("addressLine4_"+num).innerHTML;
		var addr5value= document.getElementById("addressLine5_"+num).innerHTML;
		var addr6value= document.getElementById("addressLine6_"+num).innerHTML;
		var cityvalue= document.getElementById("city_"+num).innerHTML;
		var statevalue= document.getElementById("state_"+num).innerHTML;
		var zipvalue= document.getElementById("zip_"+num).innerHTML;
		var countryvalue= document.getElementById("country_"+num).innerHTML;
		var dayPhonevalue= document.getElementById("dayPhone_"+num).innerHTML;
		var evePhonevalue= document.getElementById("evePhone_"+num).innerHTML;
		var mobPhonevalue= document.getElementById("mobPhone_"+num).innerHTML;
		var dayvalue= document.getElementById("dayFax_"+num).innerHTML;
		var emailvalue= document.getElementById("email_"+num).innerHTML;
		
		/*var addr1= document.all("AddressLine1");
		var addr2= document.all("AddressLine2");
		var addr3= document.all("AddressLine3");
		var addr4= document.all("AddressLine4");
		var addr5= document.all("AddressLine5");
		var addr6= document.all("AddressLine6");
		var city= document.all("City");
		var state= document.all("State");
		var zip= document.all("ZipCode");
		var country= document.all("Country");
		var dayPhone= document.all("DayPhone");
		var evePhone= document.all("EveningPhone");
		var mobPhone= document.all("MobilePhone");
		var day= document.all("DayFaxNo");
		var email= document.all("Emailid");*/
		
		document.all("AddressLine1").value=addr1value;
		document.all("AddressLine2").value=addr2value;
		document.all("AddressLine3").value=addr3value;
		document.all("AddressLine4").value=addr4value;
		document.all("AddressLine5").value=addr5value;
		document.all("AddressLine6").value=addr6value;
		document.all("City").value=cityvalue;
		document.all("State").value=statevalue;
		document.all("ZipCode").value=zipvalue;
		document.all("Country").value=countryvalue;
		document.all("DayPhone").value=dayPhonevalue;
		document.all("EveningPhone").value= evePhonevalue;
		document.all("MobilePhone").value= mobPhonevalue;
		document.all("DayFaxNo").value=dayvalue;
		document.all("Emailid").value=emailvalue;		
	}
</script>
<table class="view">
	<thead>
        <tr>
			<td sortable="no" class="checkboxheader">&nbsp;</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Billing ID</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 1</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 2</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 3</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 4</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 5</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 6</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">City</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">State</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Country</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Zip Code</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Day Phone</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Evening Phone</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Mobile Phone</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">Day FAX No</td>
			<td sortable="no" class="tablecolumnheader" nowrap="true" style="width:30px">EMail-ID</td>
			
        </tr>
    </thead>
<yfc:loopXML binding="xml:TerCustBillingOrg:/TerCustBillingOrgList/@TerCustBillingOrg" id="TerCustBillingOrg">
	<tr id="TerCustBillingOrgCounter">
		<td><input type="checkbox" name="check" onclick="setValue(this,'<%=TerCustBillingOrgCounter%>');" /> </td>
		<td class="tablecolumn" id="billingId_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerBillingID" /></td>
		<td class="tablecolumn" id="addressLine1_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerAddressLine1" /></td>
		<td class="tablecolumn" id="addressLine2_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerAddressLine2" /></td>
		<td class="tablecolumn" id="addressLine3_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerAddressLine3" /></td>
		<td class="tablecolumn" id="addressLine4_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerAddressLine4" /></td>
		<td class="tablecolumn" id="addressLine5_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerAddressLine5" /></td>
		<td class="tablecolumn" id="addressLine6_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerAddressLine6" /></td>
		<td class="tablecolumn" id="city_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerCity" /></td>
		<td class="tablecolumn" id="state_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerState" /></td>
		<td class="tablecolumn" id="country_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerCountry" /></td>
		<td class="tablecolumn" id="zip_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerZipCode" /></td>
		<td class="tablecolumn" id="dayPhone_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerDayPhone" /></td>
		<td class="tablecolumn" id="evePhone_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerEveningPhone" /></td>
		<td class="tablecolumn" id="mobPhone_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerMobilePhone" /></td>
		<td class="tablecolumn" id="dayFax_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerDayFaxNo" /></td>
		<td class="tablecolumn" id="email_<%=TerCustBillingOrgCounter%>"><yfc:getXMLValue binding="xml:/TerCustBillingOrg/@TerEMailID" /></td>
	</tr>
</yfc:loopXML>
<tr>
    <td>
        <input type="hidden" name="xml:/Order/@ModificationReasonCode"/>
        <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
        <input type="hidden" name="xml:/Order/@Override" value="N"/>
		<input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
        <input type="hidden" name="xml:/OrderRelease/@ModificationReasonCode"/>
        <input type="hidden" name="xml:/OrderRelease/@ModificationReasonText"/>
        <input type="hidden" name="xml:/OrderRelease/@Override" value="N"/>
        <input type="hidden" name="xml:/WorkOrder/@ModificationReasonCode"/>
        <input type="hidden" name="xml:/WorkOrder/@ModificationReasonText"/>
        <input type="hidden" name="xml:/WorkOrder/@OverrideModificationRules" value="N"/>
        <input type="hidden" name="hiddenDraftOrderFlag" value="N"/>
        <input type="hidden" class="unprotectedinput" <%=getTextOptions("AddressType", "")%> />
        <input type="hidden" <%=getTextOptions("AlternateEmailID", "")%> >
        <input type="hidden" <%=getTextOptions("Beeper", "")%> >
        <input type="hidden" <%=getTextOptions("Department", "")%> >
        <input type="hidden" <%=getTextOptions("EveningFaxNo", "")%> >
        <input type="hidden" <%=getTextOptions("JobTitle", "")%> >
        <input type="hidden" <%=getTextOptions("OtherPhone", "")%> >        
        <input type="hidden" <%=getTextOptions("Suffix", "")%> >
        <input type="hidden" <%=getTextOptions("Title", "")%> >
        <input type="hidden" <%=getTextOptions("ErrorTxt", "")%> />
        <input type="hidden" <%=getTextOptions("HttpUrl", "")%> />
        <input type="hidden" <%=getTextOptions("PersonID", "")%> />
        <input type="hidden" <%=getTextOptions("PreferredShipAddress", "")%> />
        <input type="hidden" <%=getTextOptions("UseCount", "")%> />
        <input type="hidden" <%=getTextOptions("VerificationStatus", "")%> /> 
    </td>
</tr>
<tr id="AddressTypeRow">
    <td>
        <table>
            <tr>
                <td>
                    <input type="text" class="protectedinput" type="hidden" <%=getTextOptions("AddressTypeLabel", "")%>>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
	<td>
        <table class="view"  ID="ModifyAddressLeft" >
        <tr>
            <td>
                <input class="unprotectedinput" type="hidden" <%=getTextOptions("AddressLine1", "")%> >
            </td>
        </tr>
        <tr>
            <td>
                <input class="unprotectedinput" type="hidden" <%=getTextOptions("AddressLine2", "")%>>
            </td>
        </tr>
        <tr>
            <td>
                <input class="unprotectedinput" type="hidden" <%=getTextOptions("AddressLine3", "")%> >
            </td>
        </tr>
        <tr>
            <td>
                <input class="unprotectedinput" type="hidden" <%=getTextOptions("AddressLine4", "")%> >
            </td>
        </tr>
        <tr>
            <td>
                <input class="unprotectedinput" type="hidden" <%=getTextOptions("AddressLine5", "")%> >
            </td>
        </tr>
        <tr>
            <td>
                <input class="unprotectedinput" type="hidden" <%=getTextOptions("AddressLine6", "")%> >
            </td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("City", "")%> ></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("State", "")%> ></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("ZipCode", "")%> ></td>
        </tr>
        <tr>
			<td><input class="unprotectedinput" type="hidden" <%=getTextOptions("Country", "")%> ></td>
        </tr>
	    </table>
    </td>
	<td>
        <table class="view"  ID="ModifyAddressRight" >
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("FirstName", "")%>></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("MiddleName", "")%> ></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("LastName", "")%> ></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("Company", "")%>></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("DayPhone", "")%> ></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("EveningPhone", "")%> ></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("MobilePhone", "")%> ></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("DayFaxNo", "")%> ></td>
        </tr>
        <tr>
            <td><input class="unprotectedinput" type="hidden" <%=getTextOptions("Emailid", "")%> ></td>
        </tr>
    	</table>
    </td>
</tr>
</table>

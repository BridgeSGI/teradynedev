<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>


<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript"> </script>

<table class="view">

<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="ShowDocumentType" value="false"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
</jsp:include>
	<% // Now call the APIs that are dependent on the common fields (Enterprise Code) %>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP4"/>
    <yfc:callAPI apiID="AP5"/>
	<yfc:callAPI apiID="AP9"/>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Part_Number</yfc:i18n></td>
	</tr>
	
    <tr>
        <td class="searchcriteriacell" nowrap="true" >
            <select name="xml:/Item/@ItemIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Item/@ItemIDQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Item/@ItemID")%>/>
            <% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>
			<img class="lookupicon" name="search" 
			onclick="callItemLookup('xml:/Item/@ItemID','xml:/Item/PrimaryInformation/@DefaultProductClass','xml:Item/@UnitOfMeasure',
			'item','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
			
        </td>
        
    </tr>
		
		
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td sortable="no" class="checkboxheader">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
		<td class="tablecolumnheader"><yfc:i18n>Part_#</yfc:i18n></td>    
		<td class="tablecolumnheader"><yfc:i18n>Repair ABC</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Planning ABC</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Logistic ABC</yfc:i18n></td>	
		<td class="tablecolumnheader"><yfc:i18n>Admin Cost</yfc:i18n></td>
        <td class="tablecolumnheader"><yfc:i18n>Total SDS ABC Cost</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Total BPS-MPS ABC Cost</yfc:i18n></td>	
					
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/ItemList/@Item" id="Item">	
    <tr> 
		<yfc:makeXMLInput name="ItemKey">
            <yfc:makeXMLKey binding="xml:/Item/@ItemKey" value="xml:/Item/@ItemKey" />
        </yfc:makeXMLInput>
		<td class="checkboxcolumn">
		<input type="checkbox" value="<%=getParameter("ItemKey")%>" name="EntityKey"/>
		</td>
			<td class="tablecolumn">
                <a href="javascript:showDetailFor('<%=getParameter("ItemKey")%>');">
                    <yfc:getXMLValue binding="xml:/Item/@ItemID"/>
                </a>
            </td>
			<td class="numerictablecolumn"><yfc:getXMLValue binding="xml:/Item/Extn/@RepairABC"/></td>
			<td class="numerictablecolumn"><yfc:getXMLValue binding="xml:/Item/Extn/@PlanningABC"/></td>
			<td class="numerictablecolumn"><yfc:getXMLValue binding="xml:/Item/Extn/@LogisticsABC"/></td>
			<td class="numerictablecolumn"><yfc:getXMLValue binding="xml:/Item/Extn/@AdminCost"/></td>
			<td class="numerictablecolumn"><yfc:getXMLValue binding="xml:/Item/Extn/@TotalSBCABCCost"/></td>
            <td class="numerictablecolumn"><yfc:getXMLValue binding="xml:/Item/Extn/@TotalBPSMPSABCCost"/></td>			
    </tr>
    </yfc:loopXML> 
</tbody>
</table>

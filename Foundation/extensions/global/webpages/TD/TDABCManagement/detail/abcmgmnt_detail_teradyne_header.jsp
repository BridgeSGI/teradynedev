<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<table class="view" width="100%">
<tr>
	<td class="detaillabel"><yfc:i18n>Part Number</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/@ItemID"/></td>
	
	<td></td><td></td>
	
	<td class="detaillabel"><yfc:i18n>Part Description</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/PrimaryInformation/@ShortDescription"/></td>
</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Stds w/oh</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@StandardsWithOverhead"/></td>

	<td></td><td></td>
	
	<td class="detaillabel"><yfc:i18n>Total Repair_Cost</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@TotalRepairCost"/></td>

</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Repair ABC</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml/Item/Extn/@RepairABC"/></td>

	<td class="detaillabel"><yfc:i18n>Planning ABC</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@PlanningABC"/></td>
	
	<td class="detaillabel"><yfc:i18n>Logistic ABC</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@LogisticsABC"/></td>
	
	<td class="detaillabel"><yfc:i18n>Admin Cost</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@AdminCost"/></td>

</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Total BPS MPS ABC Cost</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@TotalBPSMPSABCCost"/></td>

	<td></td><td></td>
	
	<td class="detaillabel"><yfc:i18n>Total SDS ABC Cost</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@TotalSBCABCCost"/></td>

</tr>

</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script type="text/javascript">
 function sum() 
{ 
    var fn, ln; 
    fn = document.getElementById("xml:/Item/Extn/@RepairABC").value; 
    ln = document.getElementById("xml:/Item/Extn/@PlanningABC").value; 
    result =  (fn+ln); 
    document.getElementById("xml:/Item/Extn/@TotalBPSMPSABCCost").innerHTML = result; 
}
</script>

<%
	String ItemKeyVal = resolveValue("xml:/Item/@ItemKey");	
%>

<table class="view" cellspacing="10" cellpadding="10">

<tr>
	<td class="detaillabel" align="left" ><yfc:i18n>Part Number</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/@ItemID"/></td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Part Description</yfc:i18n></td>	
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/PrimaryInformation/@ShortDescription"/></td>
</tr>

<tr>
	<td class="detaillabel" align="left"><yfc:i18n>stds w/oh</yfc:i18n></td>
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@StandardsWithOverhead"/></td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Total Repair Cost</yfc:i18n></td>
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@TotalRepairCost"/></td>											
</tr>

<tr>

    <td class="detaillabel"><yfc:i18n>Repair ABC</yfc:i18n></td>		
	<td>
	<input type="text" id="ServiceType" size="10" class="unprotectedinput" <%=getTextOptions("xml:/Item/Extn/@RepairABC")%>/>
	</td>
	
	<td class="detaillabel"><yfc:i18n>Planning ABC</yfc:i18n></td>		
	<td>
	<input type="text" id="ServiceType" size="10" class="unprotectedinput" <%=getTextOptions("xml:/Item/Extn/@PlanningABC")%>/>
	</td>
	
	<td class="detaillabel"><yfc:i18n>Logistic ABC</yfc:i18n></td>		
	<td>
	<input type="text" id="ServiceType" size="10" class="unprotectedinput" <%=getTextOptions("xml:/Item/Extn/@LogisticsABC")%>/>
	</td>
	
	<td class="detaillabel"><yfc:i18n>Admin Cost</yfc:i18n></td>		
	<td>
	<input type="text" id="ServiceType" size="10" class="unprotectedinput" <%=getTextOptions("xml:/Item/Extn/@AdminCost")%>/>
	</td>
		
</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Total BPS MPS ABC Cost</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@TotalBPSMPSABCCost"/></td>

		
	<td class="detaillabel"><yfc:i18n>Total SDS ABC Cost</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/Item/Extn/@TotalSBCABCCost"/></td>

</tr>

</table>

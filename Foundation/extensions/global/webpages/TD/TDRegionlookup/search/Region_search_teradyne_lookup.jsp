<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<%
//Get the document type that the lookup is for
String sCenterNumber;
	sCenterNumber = request.getParameter("CenterNumber");
%>

<table class="view">
	
 
	<tr>
		<td>
			<input type="hidden" name="CenterNumber" value="<%=HTMLEncode.htmlEscape(sCenterNumber)%>"/>
			
		</td>
	</tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Center Number</yfc:i18n>
        </td>
    </tr>
	
    <tr>
        <td nowrap="true" class="searchcriteriacell">		
            <select class="combobox" <%=getComboOptions("xml:/TerRegionBasedABC/@TerCenterIdQryType") %> >
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/TerRegionBasedABC/@TerCenterIdQryType"/>
            </select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerRegionBasedABC/@TerCenterId")%>/>	
		</td>
            	
    </tr>
</table>

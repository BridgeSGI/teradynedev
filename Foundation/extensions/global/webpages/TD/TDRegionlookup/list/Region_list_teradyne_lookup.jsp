<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script>

function setRegionLookupValue(CenterId,CenterVal)
	{
		var Obj = window.dialogArguments
		if(Obj != null){
			Obj.field1.value = CenterId;	
            
		}
		window.close();
	}
	
</script>


<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td class="lookupiconheader" sortable="no"><br /></td>
		<td class="tablecolumnheader"><yfc:i18n>Center ID</yfc:i18n></td>    
		<td class="tablecolumnheader"><yfc:i18n>Center Name</yfc:i18n></td>
					
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/TerRegionBasedABCList/@TerRegionBasedABC" id="TerRegionBasedABC">	
    <tr> 
		
		
        <td class="tablecolumn">
			<img class="icon" onClick="setRegionLookupValue('<%=resolveValue("xml:TerRegionBasedABC:/TerRegionBasedABC/@TerCenterId")%>')"  value="<%=resolveValue("xml:TerRegionBasedABC:/TerRegionBasedABC/@TerCenterId")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerCenterId"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerCenterName"/></td>

    </tr>
    </yfc:loopXML> 
</tbody>
</table>



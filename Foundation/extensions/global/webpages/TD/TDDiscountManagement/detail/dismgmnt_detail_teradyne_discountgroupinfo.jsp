<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<table class="view" cellspacing="10" cellpadding="10">

<tr>
	<td class="detaillabel" align="left" ><yfc:i18n>Customer_#</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerDiscountGroup/@TerCustomerNo"/></td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Discount_Group</yfc:i18n></td>	
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerDiscountGroup/@TerDiscountGroupId"/></td>
</tr>

<tr>
	<td class="detaillabel" align="left"><yfc:i18n>Customer_Name</yfc:i18n></td>
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerDiscountGroup/@TerCustomerName"/></td>
														
	<td class="detaillabel" align="left"><yfc:i18n>Discount_Percentage</yfc:i18n></td>
	<td>
	<input type="text" id="DiscountPercentage" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerDiscountPercentage")%>/>
	</td>
</tr>

<tr>
	<td class="detaillabel" align="left"><yfc:i18n>Service_Type</yfc:i18n></td>
	<td>
	<input type="text" id="ServiceType" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerServiceType")%>/>
	</td>
	
	<td class="detaillabel" align="left"><yfc:i18n>System_Type</yfc:i18n></td>
	<td>
	<input type="text" id="SystemType" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerSystemType")%>/>
	</td>

</tr>

</table>

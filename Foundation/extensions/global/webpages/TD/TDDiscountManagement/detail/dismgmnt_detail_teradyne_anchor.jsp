<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>


<table cellSpacing=0 class="anchor" cellpadding="7px">
<tr>
    <td>
        <yfc:makeXMLInput name="DiscountgroupKey">
            <yfc:makeXMLKey binding="xml:/TerDiscountGroup/@TerDiscountGroupKey" value="xml:/TerDiscountGroup/@TerDiscountGroupKey"/>
        </yfc:makeXMLInput>
        <input type="hidden" value='<%=getParameter("DiscountgroupKey")%>' name="DiscountgroupEntityKey"/>
        <input type="hidden" <%=getTextOptions("xml:/TerDiscountGroup/@TerDiscountGroupKey")%>/>
    </td>
</tr>

<tr>
<td colspan="4">
	    <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I02"/>
            <jsp:param name="ModifyView" value="true"/>
		    <jsp:param name="getRequestDOM" value="Y"/>
        </jsp:include>
</td>
</tr>

</table>

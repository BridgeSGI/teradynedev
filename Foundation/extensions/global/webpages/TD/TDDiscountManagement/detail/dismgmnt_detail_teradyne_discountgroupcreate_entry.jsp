<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script>
	function showDiscountGroupCustomerNoPopup(custId,custName, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(custId);
		oObj.field2 = document.all(custName);
		var extraParams = "CustomerNo=" + extraParams;		
		yfcShowSearchPopupWithParams('TDDGLkpCusS030','Discoutgrouplookup',900,550,oObj,entityname, extraParams);
		
	}
	
	function showDiscountGroupServiceTypePopup(CodeValue, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(CodeValue);
		//Pass Common code type to the lookup screen
		var extraParams = "CodeType=" + extraParams;
		yfcShowSearchPopupWithParams('TDDGlookupS020','Discoutgrouplookup',900,550,oObj,entityname,extraParams);
		
	}
	function showDiscountGroupSystemTypePopup(SysType, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(SysType);
		var extraParams = "SystemType=" + extraParams;
		yfcShowSearchPopupWithParams('TDSysS020','Discoutgrouplookup',900,550,oObj,entityname,extraParams);
		
	}
	
	
	function validation(textvalue)
	{
		var errorLabel = document.getElementById("ErrorLabel");
		var errorMessage="";
		
		if(textvalue != "")
		{
			errorMessage="";
			errorLabel.innerHTML = errorMessage;
		}
		
		if(textvalue == "")
		{
			errorMessage = "All fields are required. System type is optional.";
			errorLabel.innerHTML = errorMessage;
		}
	}
	function inputValidation()
	{	
		var errorLabel = document.getElementById("ErrorLabel");
		var result = true;
		var errorMessage="";		
				
		var disGroupID = document.getElementById("DiscountGroup");
		var disGroupIDVal = disGroupID.value;
		if(disGroupIDVal == "")
		{ 
			var result = false;
		}
		
		var cusNumber = document.getElementById("CustomerNo");
		var cusNumberVal = cusNumber.value;
		if(cusNumberVal == "")
		{ 
			var result = false;
		}		
		
		var disPercent = document.getElementById("DiscountPercentage");
		var disPercentVal = disPercent.value;
		if(disPercentVal == "")
		{ 
			var result = false;
		}
		
		var servType = document.getElementById("ServiceType");
		var servTypeVal = servType.value;
		if(servTypeVal == "")
		{ 
			var result = false;
		}
		
		if(result == true)
		{
			errorMessage="";
			errorLabel.innerHTML = errorMessage;
		}
		
		if(result == false)
		{
			errorMessage = "All fields are required. System type is optional.";
			errorLabel.innerHTML = errorMessage;
		}
		return result;
	}
</script>

<%
	String discountGroupKeyVal = resolveValue("xml:/TerDiscountGroup/@TerDiscountGroupKey");	
%>

<script language="javascript">
<% if (!isVoid(discountGroupKeyVal)) {	 
            YFCDocument discountGroupDoc = YFCDocument.createDocument("TerDiscountGroup");
            discountGroupDoc.getDocumentElement().setAttribute("TerDiscountGroupKey",resolveValue("xml:/TerDiscountGroup/@TerDiscountGroupKey"));


            %>
                function showDiscountGroupDetail() {
					showDetailFor('<%=discountGroupDoc.getDocumentElement().getString(false)%>');
				}
                window.attachEvent("onload", showDiscountGroupDetail);
            <% }      
    
%>
</script>

<table class="view" cellspacing="10" cellpadding="10">

<tr>
	<td class="detaillabel"><yfc:i18n>Customer_#</yfc:i18n><%=discountGroupKeyVal%></td>
	<td class="searchcriteriacell" nowrap="true">
		<input type="text" id="CustomerNo" size="30" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerCustomerNo")%>/>
		<img class="lookupicon" name="search" 
		onclick="showDiscountGroupCustomerNoPopup('xml:/TerDiscountGroup/@TerCustomerNo','xml:/TerDiscountGroup/@TerCustomerName','TDDisGrpLupCustomer', 'CustomerNo') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_Number") %> />
	</td>
	
	
	
	<td class="detaillabel"><yfc:i18n>Discount_Group</yfc:i18n></td>
	<td>
		<input type="text" id="DiscountGroup" size="30" onblur="validation(this.value)" class="unprotectedinput"  <%=getTextOptions("xml:/TerDiscountGroup/@TerDiscountGroupId")%>/>
	</td>

</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Customer_Name</yfc:i18n></td>
	<td nowrap="true"><input type="text" class="protectedinput" size="40" <%=getTextOptions("xml:/TerDiscountGroup/@TerCustomerName")%> readonly/>
	
	<td class="detaillabel"><yfc:i18n>Discount_Percentage</yfc:i18n></td>
	<td>
		<input type="text" id="DiscountPercentage" size="30" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerDiscountPercentage")%>/>
	</td>
</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Service_Type</yfc:i18n></td>
	<td class="searchcriteriacell" nowrap="true">
		<input type="text" id="ServiceType" size="30" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerServiceType")%>/>
		<img class="lookupicon" name="search" 
		onclick="showDiscountGroupServiceTypePopup('xml:/TerDiscountGroup/@TerServiceType','TDDiscoutgrouplookup','ServiceType') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Service_Type") %> />	
	</td>
	
	<td class="detaillabel"><yfc:i18n>System_Type</yfc:i18n></td>
	<td nowrap="true">
		<input type="text" id="SystemType" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerSystemType")%>/>
	<img class="lookupicon" name="search" 
		onclick="showDiscountGroupSystemTypePopup('xml:/TerDiscountGroup/@TerSystemType','TDDiscountSystemlookup', 'SystemType') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_System_type") %> />
	
	</td>
	
</tr>

<tr>
 	<td colspan="4">	
 		<div id="ErrorLabel" style="color:#ff0000"></div>
 	</td>
</tr>
	
</table>


<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>


<%@page import="com.yantra.yfc.log.YFCLogCategory"%>

<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td sortable="no" class="checkboxheader">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
		<td class="tablecolumnheader"><yfc:i18n>Discount_Group</yfc:i18n></td>    
		<td class="tablecolumnheader"><yfc:i18n>Customer_No</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Customer_Name</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Discount_Percentage</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Service_Type</yfc:i18n></td>	
		<td class="tablecolumnheader"><yfc:i18n>System_Type</yfc:i18n></td>	
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/TerDiscountGroupList/@TerDiscountGroup" id="TerDiscountGroup">	
    <tr> 
		<yfc:makeXMLInput name="TerDiscountGroupKey">
            <yfc:makeXMLKey binding="xml:/TerDiscountGroup/@TerDiscountGroupKey" value="xml:/TerDiscountGroup/@TerDiscountGroupKey" />
        </yfc:makeXMLInput>
		<td class="checkboxcolumn">
		<input type="checkbox" value="<%=getParameter("TerDiscountGroupKey")%>" name="EntityKey"/>
		</td>
			<td class="tablecolumn">
                <a href="javascript:showDetailFor('<%=getParameter("TerDiscountGroupKey")%>');">
                    <yfc:getXMLValue binding="xml:/TerDiscountGroup/@TerDiscountGroupId"/>
                </a>
            </td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerDiscountGroup/@TerCustomerNo"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerDiscountGroup/@TerCustomerName"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerDiscountGroup/@TerDiscountPercentage"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerDiscountGroup/@TerServiceType"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerDiscountGroup/@TerSystemType"/></td>			
    </tr>
    </yfc:loopXML> 
</tbody>
</table>

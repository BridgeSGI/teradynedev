<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<%@page import="com.yantra.yfc.log.YFCLogCategory"%>

<script language="javascript">


function showDiscountGroupCustomerNoPopup(custId,custName, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(custId);
		oObj.field2 = document.all(custName);
		var extraParams = "CustomerNo=" + extraParams;		
		yfcShowSearchPopupWithParams('TDDGLkpCusS030','TDDisGrpLupCustomer',900,550,oObj,entityname, extraParams);
		
	}
	
function showDiscountGroupServiceTypePopup(CodeValue, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(CodeValue);
		//Pass Common code type to the lookup screen
		var extraParams = "CodeType=" + extraParams;
		yfcShowSearchPopupWithParams('TDDGlookupS020','Discoutgrouplookup',900,550,oObj,entityname,extraParams);
		
	}
	
	function showDiscountGroupSystemTypePopup(SysType, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(SysType);
		//Pass Common code type to the lookup screen
		var extraParams = "SysType=" + extraParams;
		yfcShowSearchPopupWithParams('TDSysS020','TDDiscountSystemlookup',900,550,oObj,entityname,extraParams);
		
	}
</script>

<table class="view">
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Discount_Group</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
		<select class="combobox"  <%=getComboOptions("xml:/TerDiscountGroup/@TerDiscountGroupIdQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerDiscountGroup/@TerDiscountGroupIdQryType"/>
		</select>        
    </td>
	<td class="searchcriteriacell">
		<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerDiscountGroupId")%>/>
	</td>
	</tr>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Customer_#</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
        <select class="combobox"  <%=getComboOptions("xml:/TerDiscountGroup/@TerCustomerNoQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerDiscountGroup/@TerCustomerNoQryType"/>
		</select>			
    </td>
	<td class="searchcriteriacell">
		<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerCustomerNo")%>/>
		<img class="lookupicon" name="search" 
		onclick="showDiscountGroupCustomerNoPopup('xml:/TerDiscountGroup/@TerCustomerNo','xml:/TerDiscountGroup/@TerCustomerName','TDDisGrpLupCustomer','CustomerNo') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_Number") %> />
	</td>
	</tr>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Customer_Name</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
		<select class="combobox"  <%=getComboOptions("xml:/TerDiscountGroup/@TerCustomerNameQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerDiscountGroup/@TerCustomerNameQryType"/>
		</select>
	</td>
	<td class="searchcriteriacell">
		<input type="text" size="30" maxlength="20" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerCustomerName")%>/>
	</td>
	</tr>
	
	<tr>
	<td class="searchlabel" ><yfc:i18n>Service_Type</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
		<select class="combobox"  <%=getComboOptions("xml:/TerDiscountGroup/@TerServiceTypeQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerDiscountGroup/@TerServiceTypeQryType"/>
		</select>
	</td>
	<td class="searchcriteriacell">	
		<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerServiceType")%>/>
		<img class="lookupicon" name="search" 
		onclick="showDiscountGroupServiceTypePopup('xml:/TerDiscountGroup/@TerServiceType','TDDiscoutgrouplookup','SystemType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Service_Type") %> />
	</td>
	</tr>
	
	<tr>
	<td class="searchlabel" ><yfc:i18n>System_Type</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
		<select class="combobox"  <%=getComboOptions("xml:/TerDiscountGroup/@TerSystemTypeQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerDiscountGroup/@TerSystemTypeQryType"/>
		</select>
	</td>
	<td class="searchcriteriacell">
		<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TerDiscountGroup/@TerSystemType")%>/>
	<img class="lookupicon" name="search" 
		onclick="showDiscountGroupSystemTypePopup('xml:/TerDiscountGroup/@TerSystemType', 'TDDiscountSystemlookup', 'SysType') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_System_Type") %> />
	</td>
	</tr>

</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script>
function setPlannerNameLookupValue(PlannerName,PlannerVal)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = PlannerName;			
		}
		window.close();
	}
</script>

<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td class="lookupiconheader" sortable="no"><br /></td>
		<td class="tablecolumnheader"><yfc:i18n>Planner Code</yfc:i18n></td>   
		<td class="tablecolumnheader"><yfc:i18n>Planner Name</yfc:i18n></td>						
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/TerPlannerCodeList/@TerPlannerCode" id="TerPlannerCode"> 
    <tr> 
        <td class="tablecolumn">
			<img class="icon" onClick="setPlannerNameLookupValue('<%=resolveValue("xml:TerPlannerCode:/TerPlannerCode/@TerPlannerName")%>')"  value="<%=resolveValue("xml:xml:TerPlannerCode:/TerPlannerCode/@TerPlannerName")%>"
			<%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:TerPlannerCode:/TerPlannerCode/@TerPlannerCode"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:TerPlannerCode:/TerPlannerCode/@TerPlannerName"/></td>

    </tr>
    </yfc:loopXML> 
</tbody>
</table>



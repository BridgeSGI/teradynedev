<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script>
function setBusinessUnitLookupValue(BusinessUnit,PlannerVal)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = BusinessUnit;			
		}
		window.close();
	}
</script>

<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td class="lookupiconheader" sortable="no"><br /></td>
		<td class="tablecolumnheader"><yfc:i18n>Planner Code</yfc:i18n></td>   
		<td class="tablecolumnheader"><yfc:i18n>Business Unit</yfc:i18n></td>						
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/TerPlannerCodeList/@TerPlannerCode" id="TerPlannerCode"> 
    <tr> 
        <td class="tablecolumn">
			<img class="icon" onClick="setBusinessUnitLookupValue('<%=resolveValue("xml:TerPlannerCode:/TerPlannerCode/@TerBusinessUnit")%>')"  value="<%=resolveValue("xml:xml:TerPlannerCode:/TerPlannerCode/@TerBusinessUnit")%>"
			<%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:TerPlannerCode:/TerPlannerCode/@TerPlannerCode"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:TerPlannerCode:/TerPlannerCode/@TerBusinessUnit"/></td>

    </tr>
    </yfc:loopXML> 
</tbody>
</table>



<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript">

function clearInput()
{
	var entityKey = document.getElementsByName("EntityKey")[0]
	entityKey.value = "";
	entityKey.checked = true;
	
	alert(entityKey);
	return (true);
}
</script>

<table class="table" editable="false" width="100%" cellspacing="0">
    <thead>
	<tr>
            <td sortable="no" class="checkboxheader">
                <input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
                <input type="hidden" name="xml:/TerOfficeRecords/@TerModificationReasonCode"/>
                <input type="hidden" name="xml:/TerOfficeRecords/@TerModificationReasonText"/>
                <input type="hidden" name="ResetDetailPageDocumentType" value="Y"/>	<%-- cr 35413 --%>
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader"><yfc:i18n>Office Code</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Office Name</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Office Type</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Status</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Country</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Price Zone</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>GSO Revenue Region</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Worldwide</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service Region</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service Sub-Region</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service Location</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Clarify Work Group</yfc:i18n></td>
    </tr>
	</thead>

	<tbody>
	<yfc:loopXML binding="xml:/TerOfficeRecordsList/@TerOfficeRecords" id="TerOfficeRecords">
	<tr>
			<yfc:makeXMLInput name="OfficeKey">
                    <yfc:makeXMLKey binding="xml:/TerOfficeRecords/@TerOfficeKey" value="xml:/TerOfficeRecords/@TerOfficeKey" />
            </yfc:makeXMLInput>
			<td class="checkboxcolumn">                     
                    <input type="checkbox" value='<%=getParameter("OfficeKey")%>' name="EntityKey"/>
            </td>
			<td class="tablecolumn">
				<a href="javascript:showDetailForViewGroupId('TDoffice','TDofficeD020','<%=getParameter("OfficeKey")%>');"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeCode"/></a>
			</td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeName"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeType"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeStatus"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeCountry"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerPriceZone"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerGsoRevenueRegion"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerWorldwideRegion"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerServiceRegion"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerServiceOffice"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerServiceLocation"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerClarifyWorkgroup"/></td>
	</tr>		
	</yfc:loopXML>
	</tbody>
</table>
	
	
	
	
	
	
	
	
	
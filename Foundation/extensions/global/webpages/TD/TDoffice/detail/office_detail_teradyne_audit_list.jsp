<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="java.net.URLDecoder" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>

<table class="view" width="33%">
<!--tr>
    <td class="detaillabel" nowrap="true" ><yfc:i18n>Office Record</yfc:i18n></td>
    <td class="protectedtext">  <yfc:getXMLValue name="TDOffice" binding="xml:/TDOffice/@Createts"/></td>
</tr-->
<tr>
    <td colspan="2">
        <input class="button" type="button" value="<%=getI18N("Expand_All")%>" onclick="expandAll('TR','yfsOfficeAudit_','<%=replaceI18N("Click To Collapse Audit Info")%>','<%=YFSUIBackendConsts.FOLDER_COLLAPSE%>' )"/>
        <input class="button" type="button" value="<%=getI18N("Collapse_All")%>" onclick="collapseAll('TR','yfsOfficeAudit_','<%=replaceI18N("Click To Expand Audit Info")%>','<%=YFSUIBackendConsts.FOLDER_EXPAND%>' )"/>
    </td>
</tr>
</table>
<table class="table" width="100%">
<thead>
    <tr> 
        <td class="tablecolumnheader">
            <yfc:i18n>Audit #</yfc:i18n>
        </td> 
		<td class="tablecolumnheader">
			<yfc:i18n>Date</yfc:i18n>
		</td>
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Modification Reason Code</yfc:i18n>
        </td> 
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Modification Reason Text</yfc:i18n>
        </td> 
    </tr>
</thead> 
<tbody>
<% int OfficeAuditCounter=0; %>
    <yfc:loopXML name="OfficeAuditList" binding="xml:OfficeAuditList:/AuditList/@Audit" id="Audit" > 
        <% 
		OfficeAuditCounter++;
		String divToDisplay = "yfsOfficeAudit_" + OfficeAuditCounter;
		String auditXML = resolveValue("xml:Audit:/Audit/@AuditXml");
		String timeStamp="";
		String previousUser="";
		String user="";
		
		//get rid of the html characters in the string and replace them with their counterparts. 
		auditXML = auditXML.replace("&#34;","\"");
		auditXML = auditXML.replace("&#60;","<");
		auditXML = auditXML.replace("&#62;",">");
		auditXML = auditXML.replace("&#10;"," ");
	
		YFCElement auditXMLElem = YFCDocument.parse(auditXML).getDocumentElement();
		YFCElement attributes = auditXMLElem.getChildElement("Attributes");
		Iterator itr = attributes.getChildren();//getLoopingElementList(auditXMLElem.getChildElement("Attribute"));
		
		while(itr.hasNext())
		{
			YFCElement attributeEle = (YFCElement) itr.next();
			String attributeName = attributeEle.getAttribute("Name");
			if(attributeName.equals("Createts") || attributeName.equals("Modifyts"))
			{
				timeStamp = attributeEle.getAttribute("NewValue");
				//format the time stamp
				timeStamp = timeStamp.substring(4,6) + "/" + timeStamp.substring(6,8) + "/" + timeStamp.substring(0,4) + " " + timeStamp.substring(8,10) + ":" + timeStamp.substring(10,12) + ":" + timeStamp.substring(12,14);

			}
			if(attributeName.equals("Createuserid") || attributeName.equals("Modifyuserid"))
			{
				user = attributeEle.getAttribute("NewValue");
				if(user.length() < 1)
				{
					user = previousUser;
				}
				if(!user.equals(previousUser))
				{
					previousUser = user;
				}
			}

			
		}
		%>
        <tr>

            <td class="tablecolumn">
                <%
                    String sUser = resolveValue("xml:/OrderAudit/@CreateUserName");
                    if(isVoid(sUser) )
                        sUser = resolveValue("xml:/OrderAudit/@Createuserid");
                %>
                <%=OfficeAuditCounter%>
				<img id="<%=divToDisplay+"_img"%>" onclick="expandCollapseDetails('<%=divToDisplay%>','<%=replaceI18N("Click_To_Expand_Audit_Info")%>','<%=replaceI18N("Click_To_Collapse_Audit_Info")%>','<%=YFSUIBackendConsts.FOLDER_COLLAPSE%>','<%=YFSUIBackendConsts.FOLDER_EXPAND%>')" style="cursor:hand" <%=getImageOptions(YFSUIBackendConsts.FOLDER,"Click_To_Expand_Audit_Info")%> />

            </td>
			<td class="tablecolumn" sortValue="<%=timeStamp%>">
				<%=timeStamp%>
            </td>
			<!--Reference3 = Modification Reason Code -->
            <td class="tablecolumn">
                <yfc:getXMLValue binding="xml:/Audit/@Reference3"/>
            </td>
			<!--Reference4 = Modification Reason Text -->
           <td class="tablecolumn">
                <yfc:getXMLValue binding="xml:/Audit/@Reference4"/>

            </td>
        </tr>
	<tr id="<%=divToDisplay%>" style="display:none" BypassRowColoring="true">
            <td class="tablecolumn" colspan="6">
                <jsp:include page="/extn/TD/TDoffice/detail/office_detail_teradyne_audit_list_expand.jsp" flush="true">
					<jsp:param name="AuditXML" value="<%=auditXML%>"/>
		        </jsp:include>
                &nbsp;
            </td>
        </tr>
    </yfc:loopXML> 
</tbody>
</table>

<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<%
	String officeHeaderKeyVal = resolveValue("xml:/TerOfficeRecords/@TerOfficeKey");	
%>
<script language="javascript">
    window.attachEvent("onload", showOfficeDetail);
    <% if (!isVoid(officeHeaderKeyVal)) {	 
            YFCDocument officeDoc = YFCDocument.createDocument("TerOfficeRecords");
            officeDoc.getDocumentElement().setAttribute("TerOfficeKey",resolveValue("xml:/TerOfficeRecords/@TerOfficeKey"));
    %>
      
	function showOfficeDetail() {
				showDetailForViewGroupId('TDoffice','TDofficeD020','<%=getParameter("officeEntityKey")%>');
				//showDetailForView('<%=officeDoc.getDocumentElement().getString(false)%>');
	}
    <%}%>
	
	function inputValidation(){
		var inputElem = document.getElementById("TerOfficeCode");
		var inputElemVal = inputElem.value;		
		var errorLabel = document.getElementById("ErrorLabel");
		var errorMessage="";
		var result = true;

		if(inputElemVal.length < 1){
			result = false;
		}		

		inputElem = document.getElementById("TerOfficeType");
		inputElemVal = inputElem.options[inputElem.selectedIndex].text;
		
		if(inputElemVal.length < 1){
			result = false;
		}
			
		if(result == false){
			errorMessage = "All fields are required. <br/><br/>" + errorMessage;
			errorLabel.innerHTML = errorMessage;
		}
		return (result);
	}
</script>


<table class="view" width="100%">
	<tr>
		<input type="hidden" name="xml:/TerOfficeRecords/@TerModificationReasonCode" value="New Office Record"/>
        <input type="hidden" name="xml:/TerOfficeRecords/@TerModificationReasonText" value="Office Record Created"/>
        <input type="hidden" name="xml:/TerOfficeRecords/@TerOrganizationCode" value="<%=resolveValue("xml:CurrentUser:/User/@OrganizationKey")%>"/>
	</tr>
	<tr>
 		<td colspan="4">
 			<div id="ErrorLabel" style="color:#ff0000"></div>
 		</td>
 	</tr>
    <tr>
	
        <td class="detaillabel" ><yfc:i18n>Office Code</yfc:i18n></td>
		<td>
            <input type="text" id="TerOfficeCode" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOfficeCode")%>/>
        </td>
		
		<td class="detaillabel"><yfc:i18n>Office Type</yfc:i18n></td>
        <td>
			<select name="xml:/TerOfficeRecords/@TerOfficeType" id="TerOfficeType" class="combobox" >
				<yfc:loopOptions binding="xml:OfficeTypes:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/CommonCode/@CodeShortDescription" isLocalized="Y"/>
			</select>			
	    </td>
	</tr>
</table>

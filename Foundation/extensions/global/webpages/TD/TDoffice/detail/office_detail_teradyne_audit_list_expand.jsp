<!--@Author-->

<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%
/* This is the inner table for the office detail audit list */ 
	String auditXML = request.getParameter("AuditXML");
	YFCElement auditXMLElem = YFCDocument.parse(auditXML).getDocumentElement();
	YFCElement attributes = auditXMLElem.getChildElement("Attributes");

%>


<table class="table" >
<tbody>
    <tr>
        <td width="10%" >
            &nbsp;
        </td>
        <td width="80%" class="bordertablecolumn">
            <table class="table" SuppressRowColoring="true">
            <thead>
                <td nowrap="true" class="tablecolumnheader">
                    <yfc:i18n>Detail #</yfc:i18n>
                </td> 
                <td class="tablecolumnheader" >
                    <yfc:i18n>Change</yfc:i18n>
                </td>
                <td class="tablecolumnheader" >
                    <yfc:i18n>Old Value</yfc:i18n>
                </td>
                <td class="tablecolumnheader" >
                    <yfc:i18n>New Value</yfc:i18n>
                </td> 
            </thead>
            <tbody>
			
			<%
			int OfficeAuditLevelCounter = 0;
			Iterator itr = attributes.getChildren();
			while(itr.hasNext())
			{

				YFCElement attributeEle = (YFCElement) itr.next();
				String attributeName = attributeEle.getAttribute("Name");
				//will not show information regarding the following attributes:
				//Createts, Modifyts, Createuserid, Modifyuserid, Lockid, Modifyprogid, Createprogid, and OfficeKey
				if(!(attributeName.equals("Createts") || attributeName.equals("Modifyts") || attributeName.equals("Createuserid") || attributeName.equals("Modifyuserid") || attributeName.equals("Lockid") || attributeName.equals("Modifyprogid") || attributeName.equals("Createprogid") || attributeName.equals("OfficeKey")))
				{
					OfficeAuditLevelCounter++;
					%>
					<tr>
						<td class="tablecolumn">
							<%=OfficeAuditLevelCounter%>
						</td>
						<td class="tablecolumn">
							<%=attributeName%>
						</td>
						<td class="tablecolumn">
							<%=attributeEle.getAttribute("OldValue")%>
						</td>                   
						<td class="tablecolumn">
							<%=attributeEle.getAttribute("NewValue")%>
						</td>                       
                    
					</tr>
					<%
				}

				
			}
		%>
              
            </tbody>
            </table>
        </td>
        <td width="10%" >
            &nbsp;
        </td>
    </tr>
</tbody>
</table>


<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@page import="com.yantra.yfs.ui.backend.*" %>

<%
String sLookupAttr = request.getParameter("LookupAttribute");
String officeTypeValue = request.getParameter("OfficeTypeValue");
String officeTypeValueFinal=officeTypeValue;
if(null==officeTypeValue){
officeTypeValueFinal=resolveValue("xml:/TerOfficeRecords/@TerOfficeType");
}
%>

<table class="view">
	<tr>
		<td>
			<input type="hidden" name="LookupAttribute" value="<%=HTMLEncode.htmlEscape(sLookupAttr)%>"/>
			<input type="hidden" name="xml:/TerOfficeRecords/@TerOfficeType" value="<%=officeTypeValueFinal%>"/>
		</td>
	</tr>

	<tr>
		<td class="searchlabel" ><yfc:i18n>Office Status</yfc:i18n></td>
		<td class="searchcriteriacell">
            <select name="xml:/TerOfficeRecords/@TerOfficeStatus" class="combobox">
                <yfc:loopOptions binding="xml:OfficeStatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/TerOfficeRecords/@TerOfficeStatus" />
            </select>
        </td>
	</tr>
	<tr>
		<td class="searchlabel" ><yfc:i18n>Office Name</yfc:i18n></td>
		<td class="searchcriteriacell">
			<select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerOfficeNameQryType") %> >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerOfficeNameQryType"/>
			</select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOfficeName")%>/>
        </td>
	</tr>
	<tr>
		<td class="searchlabel" ><yfc:i18n>Office Code</yfc:i18n></td>
		<td class="searchcriteriacell">
			<select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerOfficeCodeQryType") %> >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerOfficeCodeQryType"/>
			</select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOfficeCode")%>/>
        </td>

    </tr>

	<tr >
	<td class="searchlabel" ><yfc:i18n>Worldwide Region</yfc:i18n></td>
		<td class="searchcriteriacell" nowrap="true">
			<select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerWorldwideRegionQryType") %> >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerWorldwideRegionQryType"/>
			</select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerWorldwideRegion") %> />
		</td>
	</tr>

	<tr >
	<td class="searchlabel" ><yfc:i18n>Service Region</yfc:i18n></td>
		<td class="searchcriteriacell" nowrap="true">
			<select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerServiceRegionQryType") %> >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerServiceRegionQryType"/>
			</select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerServiceRegion") %> />
		</td>
	</tr>

	<tr >
	<td class="searchlabel" ><yfc:i18n>Service Sub-Region</yfc:i18n></td>
		<td class="searchcriteriacell" nowrap="true">
			<select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerServiceOfficeQryType") %> >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerServiceOfficeQryType"/>
			</select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerServiceOffice") %> />
		</td>
	</tr>

	<tr >
	<td class="searchlabel" ><yfc:i18n>Service Location</yfc:i18n></td>
		<td class="searchcriteriacell" nowrap="true">
			<select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerServiceLocationQryType") %> >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerServiceLocationQryType"/>
			</select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerServiceLocation") %> />
		</td>
	</tr> 
</table>

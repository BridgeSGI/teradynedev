<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script>

function showOfficeRecordCommonCodeLookupPopup(CodeValue, entityname, extraParamCodeType, extraParamSearchLabel){
		var oObj = new Object();
		oObj.field1 = document.all(CodeValue);
		var extraParams = "&CodeType=" + extraParamCodeType + "&ActualSearchLabel="+extraParamSearchLabel;
		yfcShowSearchPopupWithParams('TDorlookupS020',' ',850,550,oObj,entityname, extraParams);
	}
	
function showOfficeRecordLookupPopup(Value, entityname, extraParamsLookUpAttribute, extraParamOfficeTypeValue){
		var oObj = new Object();
		oObj.field1 = document.all(Value);
		var extraParams = "&LookupAttribute=" + extraParamsLookUpAttribute + "&OfficeTypeValue="+extraParamOfficeTypeValue;		
		yfcShowSearchPopupWithParams('TDofficeS020',' ',900,550,oObj,entityname, extraParams);
		
	}
</script>

<table width="100%" class="view">
<!-- <Dropdown lists fed by Common Codes>-->
<tr>
<td class="searchlabel" ><yfc:i18n>Office Type</yfc:i18n></td>
<td class="searchcriteriacell">
            <select name="xml:/TerOfficeRecords/@TerOfficeType" class="combobox">
                <yfc:loopOptions binding="xml:OfficeTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/TerOfficeRecords/@TerOfficeType" />
            </select>
        </td>
		
<td class="searchlabel" ><yfc:i18n>Office Status</yfc:i18n></td>
<td class="searchcriteriacell">
            <select name="xml:/TerOfficeRecords/@TerOfficeStatus" class="combobox">
                <yfc:loopOptions binding="xml:OfficeStatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/TerOfficeRecords/@TerOfficeStatus" />
            </select>
        </td>

</tr>
<!-- </Dropdown lists fed by Common Codes>-->
<tr>
<td class="searchlabel" ><yfc:i18n>Office Code</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerOfficeCodeQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerOfficeCodeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOfficeCode") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerOfficeCode', 'TDoffice', 'TerOfficeCode','') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Office Code") %> />
    </td>
</tr>

<tr >
<td class="searchlabel" ><yfc:i18n>Office Name</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerOfficeNameQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerOfficeNameQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOfficeName") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerOfficeName', 'TDoffice', 'TerOfficeName','') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Office Name") %> />		
    </td>
</tr>

<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Office Country</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerOfficeCountryQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerOfficeCountryQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOfficeCountry") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerOfficeCountry', 'TDofficelookup', 'Office_Country', 'Office Country') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Office Country") %> />
    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> -->
<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Price Zone</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerPriceZoneQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerPriceZoneQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerPriceZone") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerPriceZone', 'TDofficelookup', 'Price_Zone','Price Zone') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Price Zone") %> />
    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> -->
<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>GSO Revenue Region</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerGsoRevenueRegionQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerGsoRevenueRegionQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerGsoRevenueRegion") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerGsoRevenueRegion', 'TDofficelookup', 'GSO_Revenue_Region','GSO Revenue Region') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for GSO Revenue Region") %> />

    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> --> 
<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Ops Coordinator</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerOpsCoordinatorQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerOpsCoordinatorQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOpsCoordinator") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerOpsCoordinator', 'TDofficelookup', 'Regional_Ops_Coord','Regional Ops Coordinator') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Regional Ops Coordinator") %> />

    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> --> 

<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Regional Manager</yfc:i18n></td>
	<td class="searchcriteriacell" nowrap="true">
			<select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerRegionalManagerQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerRegionalManagerQryType"/>
			 </select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerRegionalManager")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerRegionalManager', 'TDofficelookup', 'Regional_Manager','Regional Manager') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Regional Manager") %> />		
	</td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> --> 

<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Clarify Workgroup</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerClarifyWorkgroupQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerClarifyWorkgroupQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerClarifyWorkgroup") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerClarifyWorkgroup', 'TDofficelookup', 'Clarify_Workgroup','Clarify Workgroup') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Clarify Workgroup") %> />

    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> -->


<tr >
<td class="searchlabel" ><yfc:i18n>Service Region</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerServiceRegionQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerServiceRegionQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerServiceRegion") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerServiceRegion', 'TDoffice', 'TerServiceRegion', 'C') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Service Region") %> />		
    </td>
</tr>

<tr >
<td class="searchlabel" ><yfc:i18n>Service Sub-Region</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerServiceOfficeQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerServiceOfficeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerServiceOffice") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerServiceOffice', 'TDoffice', 'TerServiceOffice', 'R') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Service Sub-Region") %> />
    </td>
</tr>

<tr >
<td class="searchlabel" ><yfc:i18n>Service Location</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TerOfficeRecords/@TerServiceLocationQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerOfficeRecords/@TerServiceLocationQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerServiceLocation") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerServiceLocation', 'TDoffice', 'TerServiceLocation', 'L') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Service Location") %> />		
    </td>
</tr> 

</table>

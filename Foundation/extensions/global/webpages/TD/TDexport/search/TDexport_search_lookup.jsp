<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript">
</script>

<table class="view">
	<tr>
		<td><input type="hidden" name="xml:/CommonCode/@CodeType" value="Valid_ECCN"/></td>
	</tr>
	<tr>

        <td class="searchlabel" >
            <yfc:i18n>ECCN</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
		
            <select name="xml:/CommonCode/@CodeValueQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/CommonCode/@CodeValueQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/CommonCode/@CodeValue")%>/>
        </td>
    </tr>

</table>

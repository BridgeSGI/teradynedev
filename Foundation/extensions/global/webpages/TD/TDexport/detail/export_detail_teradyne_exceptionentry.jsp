<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script language="Javascript" >
	IgnoreChangeNames();
	yfcDoNotPromptForChanges(true);
	
	function updateCodeValue()
	{
		var dropdown1 = document.getElementById("CodeValue1");
		var strCodeVal1 = dropdown1.options[dropdown1.selectedIndex].text;
		var dropdown2 = document.getElementById("CodeValue2");
		var strCodeVal2 = dropdown2.options[dropdown2.selectedIndex].text;
		
		var inputElem = document.getElementById("CodeValueInput");
		inputElem.value = strCodeVal1 + "-" + strCodeVal2;
	}
	
	function inputValidation()
	{
		var inputElem = document.getElementById("CodeType");
		var inputElemVal = inputElem.options[inputElem.selectedIndex].text;
		
		var errorLabel = document.getElementById("ErrorLabel");
		var errorMessage="";
		var result = true;

		//check CodeType dropdown box or the ECCN selected
		if(inputElemVal.length < 1)
		{
			//errorMessage = "Please select a valid ECCN. <br/>";
			result = false;
		}
		
		
		//check CodeValue1 dropdown box or the Ship From value selected
		inputElem = document.getElementById("CodeValue1");
		inputElemVal = inputElem.options[inputElem.selectedIndex].text;
		
		if(inputElemVal.length < 1)
		{
			//errorMessage += "Please provide a Ship From Country Code <br/>";
			result = false;
		}
		
		//check CodeValue2 dropdown box or the Ship To value selected
		inputElem = document.getElementById("CodeValue2");
		inputElemVal = inputElem.options[inputElem.selectedIndex].text;
		
		if(inputElemVal.length < 1)
		{
			//errorMessage += "Please provide a Ship To Country Code <br/>";
			result = false;
		}
		
		//check CodeShortDescription dropdown box or the License Exception value selected
		inputElem = document.getElementById("CodeShortDescription");
		inputElemVal = inputElem.options[inputElem.selectedIndex].text;
		
		if(inputElemVal.length < 1)
		{
			//errorMessage += "Please select a License Exception.";
			result = false;
		}
		
		if(result == false)
		{
			errorMessage = "All fields are required. <br/><br/>" + errorMessage;
			errorLabel.innerHTML = errorMessage;
		}
		return (result);
	}
</script>

<%
//Get the document type that the lookup is for
String sCodeValue1;
String sCodeValue2;

sCodeValue1 = request.getParameter("CodeValue1");
sCodeValue2 = request.getParameter("CodeValue2");

%>

<table class="view" width="100%">
    <tr>
        <td colspan="8">
            <input type="hidden" name="xml:/CommonCode/@CodeValue" id="CodeValueInput" value=""/>
		</td>
    </tr>
	<tr>
 		<td colspan="8">
 			<div id="ErrorLabel" style="color:#ff0000"></div>
 		</td>
 	</tr>
    <tr>
        <td class="detaillabel" ><yfc:i18n>ECCN</yfc:i18n></td>
		<td>
			<select name="xml:/CommonCode/@CodeType" id="CodeType" class="combobox" >
				<yfc:loopOptions binding="xml:ValidECCNList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/CommonCode/@CodeType" isLocalized="Y"/>
			</select>			
        </td>
		
		<td class="detaillabel"><yfc:i18n>Ship_From</yfc:i18n></td>
        <td>			
			<select name="CodeValue1" id="CodeValue1" class="combobox" onchange="updateCodeValue()">
				<yfc:loopOptions binding="xml:CommonCountryCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="<%=sCodeValue1%>" isLocalized="Y"/>
			</select>			
	        
        </td>
		<td class="detaillabel"><yfc:i18n>Ship_To</yfc:i18n></td>
        <td>			
			<select name="CodeValue2" id="CodeValue2" class="combobox" onchange="updateCodeValue()">
				<yfc:loopOptions binding="xml:CommonCountryCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="<%=sCodeValue2%>" isLocalized="Y"/>
			</select>			
	        
        </td>		
		
		
		<td class="detaillabel"><yfc:i18n>License_Exception</yfc:i18n></td>
        <td>
			<select name="xml:/CommonCode/@CodeShortDescription" id="CodeShortDescription" class="combobox" >
				<yfc:loopOptions binding="xml:LiscenseExceptionCode:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/CommonCode/@CodeShortDescription" isLocalized="Y"/>
			</select>			
	    </td>
	</tr>
</table>

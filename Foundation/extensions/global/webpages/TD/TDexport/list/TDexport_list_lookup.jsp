<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script>
function setECCNValueLookupValue(value)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = value;
			
		}
		window.close();
	}
</script>

<table class="table" editable="false" width="100%" cellspacing="0">
    <thead> 
        <tr>
            <td class="lookupiconheader" sortable="no"><br /></td>
			<td class="tablecolumnheader"><yfc:i18n>ECCN</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Short_Description</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML binding="xml:/CommonCodeList/@CommonCode" id="CommonCode">
            <tr>              
				<td class="tablecolumn">
					<img class="icon" onClick="setECCNValueLookupValue(this.value)"  value="<%=resolveValue("xml:/CommonCode/@CodeValue")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
				</td>

				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/CommonCode/@CodeValue"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/CommonCode/@CodeShortDescription"/></td>
            </tr>
        </yfc:loopXML>
   </tbody>
</table>
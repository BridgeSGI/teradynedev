<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<table class="table" editable="false" width="100%" cellspacing="0">
    <thead> 
        <tr>
            <td sortable="no" class="checkboxheader">
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader"><yfc:i18n>Ship_To_Country</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Ship_From_Country</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>ECCN</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>License_Exception_Code</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML binding="xml:/CommonCodeList/@CommonCode" id="CommonCode">
            <tr>
                <yfc:makeXMLInput name="commonCodeKey">
                    <yfc:makeXMLKey binding="xml:/CommonCode/@CommonCodeKey" value="xml:/CommonCode/@CommonCodeKey" />
                </yfc:makeXMLInput>                
                <td class="checkboxcolumn">                     
                    <input type="checkbox" value='<%=getParameter("commonCodeKey")%>' name="EntityKey"/>
                </td>
				<%
				String codeValue = resolveValue("xml:/CommonCode/@CodeValue");
				String[] countryCodes = codeValue.split("-");
				%>
				<td class="tablecolumn"><%=countryCodes[0]%></td>
				<td class="tablecolumn"><%=countryCodes[1]%></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/CommonCode/@CodeType"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/CommonCode/@CodeShortDescription"/></td>
            </tr>
        </yfc:loopXML>
   </tbody>
</table>
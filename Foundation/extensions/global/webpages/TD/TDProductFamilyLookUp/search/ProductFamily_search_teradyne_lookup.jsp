<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<%
//Get the document type that the lookup is for
String orgID;
	orgID = request.getParameter("ProductFamily");
%>

<table class="view">
	<tr>
		<td>
			<input type="hidden" name="ProductFamily" value="<%=HTMLEncode.htmlEscape(orgID)%>"/>
		</td>
	</tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n><%=orgID%></yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">		
            <select class="combobox" <%=getComboOptions("xml:/TerPlannerCode/@TerProductFamilyQryType") %> >
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/TerPlannerCode/@TerProductFamilyQryType"/>
            </select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerProductFamily")%>/>	
		</td>
            	
    </tr>

</table>

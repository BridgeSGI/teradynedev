<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<%
//Get the document type that the lookup is for
String sSysType;
	sSysType = request.getParameter("SysType");
%>

<table class="view">
	
 
	<tr>
		<td>
			<input type="hidden" name="SysType" value="<%=HTMLEncode.htmlEscape(sSysType)%>"/>
			<input type="hidden" name="xml:/ItemList/Item/@ItemID" value="<%=sSysType%>"/>
		</td>
	</tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>System Type</yfc:i18n>
        </td>
    </tr>
	
    <tr>
        <td nowrap="true" class="searchcriteriacell">		
            <select class="combobox" <%=getComboOptions("xml:/ItemList/Item/@ItemIDQryType") %> >
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/ItemList/Item/@ItemIDQryType"/>
            </select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ItemList/Item/@ItemID")%>/>	
		</td>
            	
    </tr>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script>

function setSystemTypeLookupValue(SysType,SysVal)
	{
		var Obj = window.dialogArguments
		if(Obj != null){
			Obj.field1.value = SysType;	
        }
		window.close();
	}
	
</script>


<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td class="lookupiconheader" sortable="no"><br /></td>
		<td class="tablecolumnheader"><yfc:i18n>System_Type</yfc:i18n></td>    
		<td class="tablecolumnheader"><yfc:i18n>Description</yfc:i18n></td>
					
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/ItemList/@Item" id="Item">	
    <tr> 
		
		
        <td class="tablecolumn">
			<img class="icon" onClick="setSystemTypeLookupValue('<%=resolveValue("xml:Item:/Item/@ItemID")%>')"  value="<%=resolveValue("xml:Item:/Item/@ItemID")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/Item/@ItemID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/Item/PrimaryInformation/@ShortDescription"/></td>

    </tr>
    </yfc:loopXML> 
</tbody>
</table>



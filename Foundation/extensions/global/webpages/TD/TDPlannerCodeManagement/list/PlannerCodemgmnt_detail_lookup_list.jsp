<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>


<%@page import="com.yantra.yfc.log.YFCLogCategory"%>

<div align="center">
<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td sortable="no" class="checkboxheader">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
		<td class="tablecolumnheader"><yfc:i18n>Planner Code</yfc:i18n></td>    
		<td class="tablecolumnheader"><yfc:i18n>Planner Name</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Email Address</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Planner Code Name</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Product Family</yfc:i18n></td>
        <td class="tablecolumnheader"><yfc:i18n>Platform</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Business Unit</yfc:i18n></td>		
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/TerPlannerCodeList/@TerPlannerCode" id="TerPlannerCode">	
    <tr> 
		<yfc:makeXMLInput name="TerPlannerCodeKey">
            <yfc:makeXMLKey binding="xml:/TerPlannerCode/@TerPlannerCodeKey" value="xml:/TerPlannerCode/@TerPlannerCodeKey" />
        </yfc:makeXMLInput>
		<td class="checkboxcolumn">
		<input type="checkbox" value="<%=getParameter("TerPlannerCodeKey")%>" name="EntityKey"/>
		</td>
			<td class="tablecolumn">
                <a href="javascript:showDetailFor('<%=getParameter("TerPlannerCodeKey")%>');">
                    <yfc:getXMLValue binding="xml:/TerPlannerCode/@TerPlannerCode"/>
                </a>
            </td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerPlannerName"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerEmailAddress"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerPlannerCodeName"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerProductFamily"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerPlatform"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerBusinessUnit"/></td>
	   </tr>
    </yfc:loopXML> 
</tbody>
</table>
</div>


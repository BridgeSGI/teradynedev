<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<%@page import="com.yantra.yfc.log.YFCLogCategory"%>



<script language="javascript">

function showPlannerCodePopup(PlannerCode,PlannerCodeName, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(PlannerCode);
		oObj.field2 = document.all(PlannerCodeName);
		var extraParams = "PlannerNo=" + extraParams;		
		yfcShowSearchPopupWithParams('TDPlannerS020','TDPlannerLkUp',900,550,oObj,entityname, extraParams);
		
	}

	
	function showPlannerNamePopup(PlannerName, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(PlannerName);
		var extraParams = "PlannerName=" + extraParams;		
		yfcShowSearchPopupWithParams('TDPlanNameS020','PlannerNameLkUp',900,550,oObj,entityname, extraParams);
		
	}
	
	function showProductFamilyPopup(ProductFamily, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(ProductFamily);
		var extraParams = "ProductFamily=" + extraParams;		
		yfcShowSearchPopupWithParams('TDPdFamS020','ProductFamilyLkUp',900,550,oObj,entityname, extraParams);
		
	}
	
	function showBusinessUnitPopup(BusinessUnit, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(BusinessUnit);
		var extraParams = "BusinessUnit=" + extraParams;		
		yfcShowSearchPopupWithParams('BusUnitS020','BusinessUnitLkUp',900,550,oObj,entityname, extraParams);
		
	}
</script>

<table class="view">

	<tr>
		<td class="searchlabel" ><yfc:i18n>Planner Code</yfc:i18n></td>
	</tr>
	
	<tr>
	<td class="searchcriteriacell">
        <select class="combobox"  <%=getComboOptions("xml:/TerPlannerCode/@TerPlannerCodeQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerPlannerCode/@TerPlannerCodeQryType"/>
		</select>			
    </td>
	<td class="searchcriteriacell">
    <input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerPlannerCode")%>/>
	<img class="lookupicon" name="search" onclick="showPlannerCodePopup('xml:/TerPlannerCode/@TerPlannerCode','xml:/TerPlannerCode/@TerPlannerCodeName','TDPlannerLkUp','PlannerNo') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Planner_Code") %> />
		
	</td>
	</tr>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Planner Name</yfc:i18n></td>
	</tr>
	
	<tr>
	<td class="searchcriteriacell">
        <select class="combobox"  <%=getComboOptions("xml:/TerPlannerCode/@TerPlannerNameQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerPlannerCode/@TerPlannerNameQryType"/>
		</select>			
    </td>
	<td class="searchcriteriacell">
    <input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerPlannerName")%>/>
	<img class="lookupicon" name="search" onclick="showPlannerNamePopup('xml:/TerPlannerCode/@TerPlannerName','PlannerNameLkUp','PlannerName') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Planner_Name") %> />
		
	</td>
	</tr>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Planner Code Name</yfc:i18n></td>
	</tr>
	
	<tr>
	<td class="searchcriteriacell">
        <select class="combobox"  <%=getComboOptions("xml:/TerPlannerCode/@TerPlannerCodeNameQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerPlannerCode/@TerPlannerCodeNameQryType"/>
		</select>			
    </td>
	<td class="searchcriteriacell">
    <input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerPlannerCodeName")%>/>
		
	</td>
	</tr>
		
		
	<tr>
		<td class="searchlabel" ><yfc:i18n>Product Family</yfc:i18n></td>
	</tr>
	
	<tr>
	<td class="searchcriteriacell">
        <select class="combobox"  <%=getComboOptions("xml:/TerPlannerCode/@TerProductFamilyQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerPlannerCode/@TerProductFamilyQryType"/>
		</select>			
    </td>
	<td class="searchcriteriacell">
    <input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerProductFamily")%>/>
	<img class="lookupicon" name="search" onclick="showProductFamilyPopup('xml:/TerPlannerCode/@TerProductFamily','ProductFamilyLkUp','ProductFamily') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Product_Family") %> />
	</td>
	</tr>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Business Unit</yfc:i18n></td>
	</tr>
	
	<tr>
	<td class="searchcriteriacell">
        <select class="combobox"  <%=getComboOptions("xml:/TerPlannerCode/@TerBusinessUnitQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerPlannerCode/@TerBusinessUnitQryType"/>
		</select>			
    </td>
	<td class="searchcriteriacell">
    <input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerBusinessUnit")%>/>
	<img class="lookupicon" name="search" onclick="showBusinessUnitPopup('xml:/TerPlannerCode/@TerBusinessUnit','BusinessUnitLkUp','BusinessUnit') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Business_Unit") %> />
		
	</td>
	</tr>
	
</table>

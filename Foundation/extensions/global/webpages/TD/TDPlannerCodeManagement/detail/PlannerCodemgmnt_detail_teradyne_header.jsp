<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<table class="view" width="100%">
<tr>
	<td class="detaillabel"><yfc:i18n>Planner Code</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerPlannerCode"/></td>
	
	<td></td><td></td>
	
	<td class="detaillabel"><yfc:i18n>Planner Name</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerPlannerName"/></td>
</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Planner Code Name</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerPlannerCodeName"/></td>

	<td></td><td></td>
	
	<td class="detaillabel"><yfc:i18n>Email Address</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerEmailAddress"/></td>

</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Product Family</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerProductFamily"/></td>

	<td></td><td></td>
	
	<td class="detaillabel"><yfc:i18n>Platform</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerPlatform"/></td>
	
	<td></td><td></td>
	<td class="detaillabel"><yfc:i18n>Business Unit</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerBusinessUnit"/></td>

</tr>
</table>

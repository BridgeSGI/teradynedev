<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script>
	
	function validation(textvalue)
	{
		var errorLabel = document.getElementById("ErrorLabel");
		var errorMessage="";
		
		if(textvalue != "")
		{
			errorMessage="";
			errorLabel.innerHTML = errorMessage;
		}
		
		if(textvalue == "")
		{
			errorMessage = "All fields are required.";
			errorLabel.innerHTML = errorMessage;
		}
	}
	function inputValidation()
	{	
	    var disPercent = document.getElementById("TerPlannerCode");
		var disPercentVal = disPercent.value;
		if(disPercentVal == "")
		{ 
			var result = false;
		}
		
		var servType = document.getElementById("TerPlannerName");
		var servTypeVal = servType.value;
		if(servTypeVal == "")
		{ 
			var result = false;
		}
		
		var disPercent = document.getElementById("TerPlannerCodeName");
		var disPercentVal = disPercent.value;
		if(disPercentVal == "")
		{ 
			var result = false;
		}
		
		var servType = document.getElementById("TerEmailAddress");
		var servTypeVal = servType.value;
		if(servTypeVal == "")
		{ 
			var result = false;
		}
		
		var servType = document.getElementById("TerProductFamily");
		var servTypeVal = servType.value;
		if(servTypeVal == "")
		{ 
			var result = false;
		}
		
		var disPercent = document.getElementById("TerPlatform");
		var disPercentVal = disPercent.value;
		if(disPercentVal == "")
		{ 
			var result = false;
		}
		
		var servType = document.getElementById("TerBusinessUnit");
		var servTypeVal = servType.value;
		if(servTypeVal == "")
		{ 
			var result = false;
		}
		
		if(result == true)
		{
			errorMessage="";
			errorLabel.innerHTML = errorMessage;
		}
		
		if(result == false)
		{
			errorMessage = "All fields are required.";
			errorLabel.innerHTML = errorMessage;
		}
		return result;
	}
</script>

<%
	String PlannerCodeKeyVal = resolveValue("xml:/TerPlannerCode/@TerPlannerCodeKey");	
%>

<table class="view" cellspacing="10" cellpadding="10">

<tr>
	
	<td class="detaillabel" align="left"><yfc:i18n>Planner Code</yfc:i18n></td>
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerPlannerCode/@TerPlannerCode"/></td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Planner Name</yfc:i18n></td>
	<td>
	<input type="text" id="ServiceType" size="40" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerPlannerName")%>/>
	</td>
	
</tr>

<tr>

	<td class="detaillabel" align="left"><yfc:i18n>Planner Code Name</yfc:i18n></td>
	<td>
	<input type="text" id="ServiceType" size="25" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerPlannerCodeName")%>/>
	</td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Email Address</yfc:i18n></td>
	<td>
	<input type="text" id="ServiceType" size="40" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerEmailAddress")%>/>
	</td>
	
</tr>


<tr>
    <td class="detaillabel" align="left"><yfc:i18n>Product Family</yfc:i18n></td>
	<td>
	<input type="text" id="ServiceType" size="30" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerProductFamily")%>/>
	</td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Platform</yfc:i18n></td>
	<td>
	<input type="text" id="ServiceType" size="30" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerPlatform")%>/>
	</td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Business Unit</yfc:i18n></td>
	<td>
	<input type="text" id="SystemType" size="30" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerPlannerCode/@TerBusinessUnit")%>/>
	</td>

</tr>

<tr>
 	<td colspan="4">	
 		<div id="ErrorLabel" style="color:#ff0000"></div>
 	</td>
</tr>

</table>

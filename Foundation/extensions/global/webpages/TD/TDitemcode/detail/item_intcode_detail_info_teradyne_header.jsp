<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<table class="view" width="100%">
	<tr>
	
		<td class="detaillabel" ><yfc:i18n>Ship_To_Country</yfc:i18n></td>

        <td>			
			<select name="xml:/TerIntItemCodes/@TerShipToCountryCode" id="ShipToCountry" class="combobox">
				<yfc:loopOptions binding="xml:CommonCountryCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:TerIntItemCodes/@TerShipToCountryCode" isLocalized="Y"/>
			</select>			
        </td>
		<td class="detaillabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
		<td>
			<input type="text" id="TerItemID" class="unprotectedinput" <%=getTextOptions("xml:/TerIntItemCodes/@TerItemID")%>/>
            <img class="lookupicon" name="search" 
			onclick="callItemLookup('xml:/TerIntItemCodes/@TerItemID',' ',' ',
			'item',' ')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
		</td>
		
		<td class="detaillabel" ><yfc:i18n>ECCN</yfc:i18n></td>
		<td>
			<select name="xml:/TerIntItemCodes/@TerECCNCode" class="combobox" >
				<yfc:loopOptions binding="xml:ECCNList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerIntItemCodes/@TerECCNCode" isLocalized="Y"/>
			</select>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>HTC</yfc:i18n></td>
		<td>
			<input type="text" id="HTCCode" class="unprotectedinput" <%=getTextOptions("xml:/TerIntItemCodes/@TerHTSCode")%>/>

		</td>
		<td class="detaillabel" ><yfc:i18n>HTC_Description</yfc:i18n></td>
		<td>
			<input type="text" id="HTCDescription" size="20" maxlength="40" class="unprotectedinput" <%=getTextOptions("xml:/TerIntItemCodes/@TerHTSDescription")%>/>

		</td>
	</tr>


</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript">
</script>

<table class="view">
	<tr>
		
	</tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Ship_To_Country</yfc:i18n>
        </td>
    </tr>
    <tr>
		<td>
			<select name="xml:/TerIntItemCodes/@TerShipToCountryCode" class="combobox">
				<yfc:loopOptions binding="xml:CommonCountryCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerIntItemCodes/@TerShipToCountryCode" isLocalized="Y"/>
			</select>			
		</td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
		
            <select name="xml:/TerIntItemCodes/@TerItemIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/TerIntItemCodes/@TerItemIDQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerIntItemCodes/@TerItemID")%>/>
			<img class="lookupicon" name="search" 
				onclick="callItemLookup('xml:/TerIntItemCodes/@TerItemID',' ',' ',
				'item',' ')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
        </td>
    </tr>
	<tr>

        <td class="searchlabel" >
            <yfc:i18n>ECCN</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
		
			<select name="xml:/TerIntItemCodes/@TerECCNCode" class="combobox" >
				<yfc:loopOptions binding="xml:ECCNList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerIntItemCodes/@TerECCNCode" isLocalized="Y"/>
			</select>	
        </td>
    </tr>
    
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>HTS</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
		
            <select name="xml:/TerIntItemCodes/@TerHTSCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/TerIntItemCodes/@TerHTSCodeQryType"/>
            </select>
            <input type="text" size="10" maxlength="20" class="unprotectedinput" <%=getTextOptions("xml:/TerIntItemCodes/@TerHTSCode")%>/>
        </td>
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>HTS_Description</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
		
            <select name="xml:/TerIntItemCodes/@TerHTSDescriptionQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/TerIntItemCodes/@TerHTSDescriptionQryType"/>
            </select>
            <input type="text" size="20" maxlength="40" class="unprotectedinput" <%=getTextOptions("xml:/TerIntItemCodes/@TerHTSDescription")%>/>
        </td>
    </tr>
</table>

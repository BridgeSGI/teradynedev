<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script>
function setCustomerLookupValue(PlannerCode,PlannerCodeName,PlannerCodeVal)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = PlannerCode;			
			Obj.field2.value = PlannerCodeName;
		}
		window.close();
	}
</script>

<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td class="lookupiconheader" sortable="no"><br /></td>
		<td class="tablecolumnheader"><yfc:i18n>Planner Code</yfc:i18n></td>   
		<td class="tablecolumnheader"><yfc:i18n>Planner Code Name</yfc:i18n></td>						
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/TerPlannerCodeList/@TerPlannerCode" id="TerPlannerCode"> 
    <tr> 
        <td class="tablecolumn">
			<img class="icon" onClick="setCustomerLookupValue('<%=resolveValue("xml:TerPlannerCode:/TerPlannerCode/@TerPlannerCode")%>','<%=resolveValue("xml:TerPlannerCode:/TerPlannerCode/@TerPlannerCodeName")%>')"  value="<%=resolveValue("xml:xml:TerPlannerCode:/TerPlannerCode/@TerPlannerCode")%>"
			<%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:TerPlannerCode:/TerPlannerCode/@TerPlannerCode"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:TerPlannerCode:/TerPlannerCode/@TerPlannerCodeName"/></td>

    </tr>
    </yfc:loopXML> 
</tbody>
</table>



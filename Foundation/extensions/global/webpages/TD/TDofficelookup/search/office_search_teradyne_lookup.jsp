<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<%
//Get the Code type that the lookup is for
String sCodeType;
sCodeType = request.getParameter("CodeType");
String actualSearchLabel = request.getParameter("ActualSearchLabel");
String callingOrgCode = resolveValue("xml:CurrentUser:/User/@OrganizationKey");	
%>

<input type="hidden" name="xml:/CommonCode/@CallingOrganizationCode" value="<%=callingOrgCode%>"/>

<table class="view">
	<tr>
		<td>
			<input type="hidden" name="CodeType" value="<%=HTMLEncode.htmlEscape(sCodeType)%>"/>
			<input type="hidden" name="xml:/CommonCode/@CodeType" value="<%=sCodeType%>"/>
			<input type="hidden" name="xml:/TempSearchLabel/@ActualSearchLabel" value="<%=actualSearchLabel%>"/>
		</td>
	</tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n><%=actualSearchLabel%></yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
		
            <select name="xml:/CommonCode/@CodeValueQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/CommonCode/@CodeValueQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/CommonCode/@CodeValue")%>/>
        </td>
    </tr>
   
	<tr>
        <td class="searchlabel">
            <yfc:i18n>Selecting All May Be Slow</yfc:i18n>
        </td>
    </tr>
</table>
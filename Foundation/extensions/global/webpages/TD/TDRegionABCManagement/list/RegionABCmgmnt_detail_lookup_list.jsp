<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>


<%@page import="com.yantra.yfc.log.YFCLogCategory"%>

<div align="center">
<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td sortable="no" class="checkboxheader">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
		<td class="tablecolumnheader"><yfc:i18n>Item #</yfc:i18n></td>    
		<td class="tablecolumnheader"><yfc:i18n>Center #</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Center Name</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Express Cost</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Non Express Cost</yfc:i18n></td>	
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/TerRegionBasedABCList/@TerRegionBasedABC" id="TerRegionBasedABC">	
    <tr> 
		<yfc:makeXMLInput name="TerRegionBasedABCKey">
            <yfc:makeXMLKey binding="xml:/TerRegionBasedABC/@TerRegionBasedABCKey" value="xml:/TerRegionBasedABC/@TerRegionBasedABCKey" />
        </yfc:makeXMLInput>
		<td class="checkboxcolumn">
		<input type="checkbox" value="<%=getParameter("TerRegionBasedABCKey")%>" name="EntityKey"/>
		</td>
			<td class="tablecolumn">
                <a href="javascript:showDetailFor('<%=getParameter("TerRegionBasedABCKey")%>');">
                    <yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerItemId"/>
                </a>
            </td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerCenterId"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerCenterName"/></td>
			<td class="numerictablecolumn"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerExpressCost"/></td>
			<td class="numerictablecolumn"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerNonExpressCost"/></td>
	   </tr>
    </yfc:loopXML> 
</tbody>
</table>
</div>


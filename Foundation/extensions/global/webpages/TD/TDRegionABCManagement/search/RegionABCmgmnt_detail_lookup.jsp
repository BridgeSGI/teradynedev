<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@page import="com.yantra.yfc.log.YFCLogCategory"%>


<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript">

function showRegionCenterIdPopup(CenterId, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(CenterId);
		var extraParams = "CenterNumber=" + extraParams;		
		yfcShowSearchPopupWithParams('RegionLkUpS020','RegionLkUp',900,550,oObj,entityname, extraParams);
		
	}

</script>

<table class="view">

<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="ShowDocumentType" value="false"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
</jsp:include>
	<% // Now call the APIs that are dependent on the common fields (Enterprise Code) %>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP4"/>
    <yfc:callAPI apiID="AP5"/>
	<yfc:callAPI apiID="AP9"/>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Part_Number</yfc:i18n></td>
	</tr>
	
	
	<tr>
	<td class="searchcriteriacell">
        <select class="combobox"  <%=getComboOptions("xml:/TerRegionBasedABC/@TerItemIdQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerRegionBasedABC/@TerItemIdQryType"/>
		</select>			
    </td>
	<td class="searchcriteriacell">
		<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerRegionBasedABC/@TerItemId")%>/>
		<% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); %>
			<img class="lookupicon" name="search" 
			onclick="callItemLookup('xml:/TerRegionBasedABC/@TerItemId','xml:/Item/PrimaryInformation/@DefaultProductClass','xml:Item/@UnitOfMeasure',
			'item','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
		
	</td>
	
	</tr>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Center Number</yfc:i18n></td>
	</tr>
	
	<tr>
	<td class="searchcriteriacell">
        <select class="combobox"  <%=getComboOptions("xml:/TerRegionBasedABC/@TerCenterIdQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TerRegionBasedABC/@TerCenterIdQryType"/>
		</select>			
    </td>
	<td class="searchcriteriacell">
	<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerRegionBasedABC/@TerCenterId")%>/>
	<img class="lookupicon" name="search" onclick="showRegionCenterIdPopup('xml:/TerRegionBasedABC/@TerCenterId','RegionLkUp','CenterNumber')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Center_ID") %> />
		
	</td>
	</tr>
		
</table>

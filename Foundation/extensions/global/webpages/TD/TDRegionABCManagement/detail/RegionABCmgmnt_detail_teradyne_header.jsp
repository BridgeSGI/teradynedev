<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<table class="view" width="100%">
<tr>
	<td class="detaillabel"><yfc:i18n>Part Number</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerItemId"/></td>
	
	<td></td><td></td>
	
	<td class="detaillabel"><yfc:i18n>Part Description</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerItemDesc"/></td>
</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Center #</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerCenterId"/></td>

	<td></td><td></td>
	
	<td class="detaillabel"><yfc:i18n>Center Name</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerCenterName"/></td>

</tr>

<tr>
	<td class="detaillabel"><yfc:i18n>Express Cost</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerExpressCost"/></td>

	<td></td><td></td>
	
	<td class="detaillabel"><yfc:i18n>Non-Express Cost</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerNonExpressCost"/></td>

</tr>
</table>

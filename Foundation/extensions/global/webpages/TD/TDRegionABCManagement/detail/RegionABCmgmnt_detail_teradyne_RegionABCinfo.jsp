<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script>
	
	function validation(textvalue)
	{
		var errorLabel = document.getElementById("ErrorLabel");
		var errorMessage="";
		
		if(textvalue != "")
		{
			errorMessage="";
			errorLabel.innerHTML = errorMessage;
		}
		
		if(textvalue == "")
		{
			errorMessage = "All fields are required.";
			errorLabel.innerHTML = errorMessage;
		}
	}
	function inputValidation()
	{	
		var disPercent = document.getElementById("TerNonExpressCost");
		var disPercentVal = disPercent.value;
		if(disPercentVal == "")
		{ 
			var result = false;
		}
		
		var servType = document.getElementById("TerExpressCost");
		var servTypeVal = servType.value;
		if(servTypeVal == "")
		{ 
			var result = false;
		}
		
		if(result == true)
		{
			errorMessage="";
			errorLabel.innerHTML = errorMessage;
		}
		
		if(result == false)
		{
			errorMessage = "All fields are required.";
			errorLabel.innerHTML = errorMessage;
		}
		return result;
	}
</script>

<%
	String discountGroupKeyVal = resolveValue("xml:/TerRegionBasedABC/@TerRegionBasedABCKey");	
%>

<table class="view" cellspacing="10" cellpadding="10">

<tr>
	<td class="detaillabel" align="left" ><yfc:i18n>Part Number</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerItemId"/></td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Part Description</yfc:i18n></td>	
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerItemDesc"/></td>
</tr>

<tr>
	<td class="detaillabel" align="left" ><yfc:i18n>Center #</yfc:i18n></td>		
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerCenterId"/></td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Center Name</yfc:i18n></td>	
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/TerRegionBasedABC/@TerCenterName"/></td>
</tr>

<tr>
	<td class="detaillabel" align="left"><yfc:i18n>Express Cost</yfc:i18n></td>
	<td>
	<input type="text" id="ServiceType" size="20" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerRegionBasedABC/@TerExpressCost")%>/>
	</td>
	
	<td class="detaillabel" align="left"><yfc:i18n>Non-Express Cost</yfc:i18n></td>
	<td>
	<input type="text" id="SystemType" size="20" onblur="validation(this.value)" class="unprotectedinput" <%=getTextOptions("xml:/TerRegionBasedABC/@TerNonExpressCost")%>/>
	</td>

</tr>

<tr>
 	<td colspan="4">	
 		<div id="ErrorLabel" style="color:#ff0000"></div>
 	</td>
</tr>

</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script>
function setCustomerLookupValue(custId,custName,custVal)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = custId;			
			Obj.field2.value = custName;
		}
		window.close();
	}
</script>

<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td class="lookupiconheader" sortable="no"><br /></td>
		<td class="tablecolumnheader"><yfc:i18n>Customer_#</yfc:i18n></td>   
		<td class="tablecolumnheader"><yfc:i18n>Customer_Name</yfc:i18n></td>						
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/OrganizationList/@Organization" id="Organization"> 
    <tr> 
        <td class="tablecolumn">
			<img class="icon" onClick="setCustomerLookupValue('<%=resolveValue("xml:Organization:/Organization/@OrganizationCode")%>','<%=resolveValue("xml:Organization:/Organization/@OrganizationName")%>')"  value="<%=resolveValue("xml:Organization:/Organization/@OrganizationCode")%>"
			<%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/Organization/@OrganizationCode"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/Organization/@OrganizationName"/></td>

    </tr>
    </yfc:loopXML> 
</tbody>
</table>



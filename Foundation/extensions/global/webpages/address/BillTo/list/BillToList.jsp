<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script>
	function setLookupValueForBill(billingId,billingIdName,address1,address2,address3,address4,address5,address6,city,state,
		country,zipcode,dayPhone,evePhone,mobPhone,beeper,otherPhone,dayFax,eveFax,emailID,altEmailId){
		var obj = window.dialogArguments;
		if(obj != null){
			obj.field1.value=billingId;
			obj.field2.value=billingIdName;
			obj.field3.value=address1;
			obj.field4.value=address2;
			obj.field5.value=address3;
			obj.field6.value=address4;
			obj.field7.value=address5;
			obj.field8.value=address6;
			obj.field9.value=city;
			obj.field10.value=state;
			obj.field11.value=country;
			obj.field12.value=zipcode;
			obj.field13.value=dayPhone;
			obj.field14.value=evePhone;
			obj.field15.value=mobPhone;
			obj.field16.value=beeper;
			obj.field17.value=otherPhone;
			obj.field18.value=dayFax;
			obj.field19.value=eveFax;
			obj.field20.value=emailID;
			obj.field21.value=altEmailId;
		}
		window.close();
	}
</script>

<table class="table" editable="false" width="100%" cellspacing="0">
	<thead>
		<tr>
			<td class="lookupiconheader"><br /></td>
			<td class="tablecolumnheader">Bill. ID</td>
			<td class="tablecolumnheader"><yfc:i18n>TD_Bill_To_Name</yfc:i18n></td>
			<td class="tablecolumnheader">AddressLine1</td>
			<td class="tablecolumnheader">AddressLine2</td>
			<td class="tablecolumnheader">AddressLine3</td>
			<td class="tablecolumnheader">City</td>
			<td class="tablecolumnheader">State</td>
			<td class="tablecolumnheader">Country</td>
		</tr>
	</thead>
	<tbody>
		<yfc:loopXML binding="xml:/TerCustBillingOrgList/@TerCustBillingOrg" id="ter">
			<tr>
				<td class="tablecolumn">
				<img class="icon" onClick="setLookupValueForBill('<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerBillingID")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerBillingName")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerAddressLine1")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerAddressLine2")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerAddressLine3")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerAddressLine4")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerAddressLine5")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerAddressLine6")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerCity")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerState")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerCountry")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerZipCode")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerDayPhone")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerEveningPhone")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerMobilePhone")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerBeeper")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerOtherPhone")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerDayFaxNo")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerEveningFaxNo")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerEMailID")%>','<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerAlternateEmailID")%>');" value="<%=resolveValue("xml:ter:/TerCustBillingOrg/@TerBillingID")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
				</td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:ter:/TerCustBillingOrg/@TerBillingID"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:ter:/TerCustBillingOrg/@TerBillingName"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:ter:/TerCustBillingOrg/@TerAddressLine1"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:ter:/TerCustBillingOrg/@TerAddressLine2"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:ter:/TerCustBillingOrg/@TerAddressLine3"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:ter:/TerCustBillingOrg/@TerCity"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:ter:/TerCustBillingOrg/@TerState"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:ter:/TerCustBillingOrg/@TerCountry"/></td>
			</tr>
		</yfc:loopXML>		
	</tbody>
</table>

<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/css/scripts/editabletbl.js"></script>
<script src="../extn/scripts/billtorow.js" ></script>

<script language="javascript">
	
	function uncheckAllFlag(){
		var rec = document.getElementById('record').value;
		for(i=1;i<=rec;i++){
		  var flagEle = document.getElementById("flag"+i);
		  if(flagEle != null){
			flagEle.checked = false;
			flagEle.value="N";
		  }
		}
		return;
	}	
	
	function checkCurrentBoxOnly(obj){
		uncheckAllFlag();
		var flagsEle = document.all(obj.id);
		flagsEle.checked=true;
		flagsEle.value="Y";
	}
	
	function callLookupforBill(billingId,billingIdName,entityKey,address1,address2,address3,address4,address5,address6,city,state,country,zipcode,dayPhone,evePhone,mobPhone,beeper,otherPhone,dayFax,eveFax,emailID,altEmailId){
		var ob = new Object();
		ob.field1=document.getElementById(billingId);
		ob.field2=document.getElementById(billingIdName);
		ob.field3=document.getElementById(address1);
		ob.field4=document.getElementById(address2);
		ob.field5=document.getElementById(address3);
		ob.field6=document.getElementById(address4);
		ob.field7=document.getElementById(address5);
		ob.field8=document.getElementById(address6);
		ob.field9=document.getElementById(city);
		ob.field10=document.getElementById(state);
		ob.field11=document.getElementById(country);
		ob.field12=document.getElementById(zipcode);
		ob.field13=document.getElementById(dayPhone);
		ob.field14=document.getElementById(evePhone);
		ob.field15=document.getElementById(mobPhone);
		ob.field16=document.getElementById(beeper);
		ob.field17=document.getElementById(otherPhone);
		ob.field18=document.getElementById(dayFax);
		ob.field19=document.getElementById(eveFax);
		ob.field20=document.getElementById(emailID);
		ob.field21=document.getElementById(altEmailId);
		
		yfcShowSearchPopupWithParams('','lookup',900,550,ob,entityKey,'');
	}
	
	function defaultFlagCheck(){
		var rec = document.getElementById('record').value;
		var count=0;
		for(i=1;i<=rec;i++){
		  var flagEle = document.getElementById("flag"+i);
		  if(flagEle != null && flagEle.checked){
			count++;
		  }
		}
		if(count == 0){
			alert("select default flag");
			return false;
		}
		return true;
	}
	
</script>
<% 
	int i;
	int noOfBlankRecords = 10;
	int currentRecord = 1;
	String orgKey = (String) session.getAttribute("orgCode");
	YFCDocument inputDoc = YFCDocument.getDocumentFor("<TerCustBillingOrg TerOrganizationCodeKey=\""+orgKey+"\" />");
%>
<%
		String xpath = resolveValue("xml:/Attribute/@Xpath");
		
//System.out.println("To out-put All the request-attributes received from request - ");
Enumeration enAttr = request.getAttributeNames(); 
while(enAttr.hasMoreElements()){
 String attributeName = (String)enAttr.nextElement();
 //System.out.println("Attribute Name - "+attributeName+", Value - "+(request.getAttribute(attributeName)).toString());
}

//System.out.println("To out-put All the request parameters received from request - ");

Enumeration enParams = request.getParameterNames(); 
while(enParams.hasMoreElements()){
 String paramName = (String)enParams.nextElement();
 //System.out.println("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
}

//System.out.println("Remote User Name - "+request.getRemoteUser());
//System.out.println("Remote Address - "+request.getRemoteAddr());
//System.out.println("Remote Host Name - "+request.getRemoteHost());

	%>
	

<yfc:callAPI serviceName="getTerCustBillingOrgList" inputElement='<%=inputDoc.getDocumentElement()%>'/>

<table class="table" ID="BillingAddresses" cellspacing="0" width="100%" yfcMaxSortingRecords="1000" >
    <thead>
        <tr>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Billing ID</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Billing ID Name</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 1</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 2</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 3</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 4</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 5</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Address Line 6</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">City</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">State</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Country</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Zip Code</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Day Phone</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Evening Phone</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Mobile Phone</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Beeper</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Other Phone</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Day FAX No</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Evening FAX Phone</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">EMail-ID</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Alternate Email-ID</td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">Default Flag</td>
        </tr>
    </thead>
	<input type="hidden" name="xml:/Organization/@OrganizationKey" value="<%=orgKey%>" />
	<input type="hidden" name="xml:/Organization/@Operation" value="Manage" />
	
    <tbody id="billToBody">
		<yfc:loopXML binding="xml:/TerCustBillingOrgList/@TerCustBillingOrg" id="TerCustBillingOrg">
			<%if(!isVoid(resolveValue("xml:TerCustBillingOrg:/TerCustBillingOrg/@TerCustBillingOrgKey"))) {
				currentRecord++;
			%>
				<tr>	
					<input type="hidden" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerShipToOrg",orgKey)%>/>
					<input type="hidden" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerCustBillingOrgKey","xml:/TerCustBillingOrg/@TerCustBillingOrgKey")%>/>
					<td class="unprotectedinput"><input type="text" id="TerBillingId<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerBillingID","xml:/TerCustBillingOrg/@TerBillingID")%> /><img id="img<%=TerCustBillingOrgCounter%>" class="lookupicon" name="search" onclick="callLookupforBill('TerBillingId<%=TerCustBillingOrgCounter%>','TerBillingIDName<%=TerCustBillingOrgCounter%>','TDBillTo','TerAddressLine1<%=TerCustBillingOrgCounter%>','TerAddressLine2<%=TerCustBillingOrgCounter%>','TerAddressLine3<%=TerCustBillingOrgCounter%>','TerAddressLine4<%=TerCustBillingOrgCounter%>','TerAddressLine5<%=TerCustBillingOrgCounter%>','TerAddressLine6<%=TerCustBillingOrgCounter%>','TerCity<%=TerCustBillingOrgCounter%>','TerState<%=TerCustBillingOrgCounter%>','TerCountry<%=TerCustBillingOrgCounter%>','TerZipCode<%=TerCustBillingOrgCounter%>','TerDayPhone<%=TerCustBillingOrgCounter%>','TerEveningPhone<%=TerCustBillingOrgCounter%>','TerMobilePhone<%=TerCustBillingOrgCounter%>','TerBeeper<%=TerCustBillingOrgCounter%>','TerOtherPhone<%=TerCustBillingOrgCounter%>','TerDayFaxNo<%=TerCustBillingOrgCounter%>','TerEveningFaxNo<%=TerCustBillingOrgCounter%>','TerEMailID<%=TerCustBillingOrgCounter%>','TerAlternateEmailID<%=TerCustBillingOrgCounter%>');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for BillToID") %> /></td>
					<td class="unprotectedinput"><input type="text" id="TerBillingIDName<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerBillingName","xml:/TerCustBillingOrg/@TerBillingName")%> /></td>
					<td class="unprotectedinput"><input type="text" id="TerAddressLine1<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerAddressLine1","xml:/TerCustBillingOrg/@TerAddressLine1")%> /></td>
					<td class="unprotectedinput"><input type="text" id="TerAddressLine2<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerAddressLine2","xml:/TerCustBillingOrg/@TerAddressLine2")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerAddressLine3<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerAddressLine3","xml:/TerCustBillingOrg/@TerAddressLine3")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerAddressLine4<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerAddressLine4","xml:/TerCustBillingOrg/@TerAddressLine4")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerAddressLine5<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerAddressLine5","xml:/TerCustBillingOrg/@TerAddressLine5")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerAddressLine6<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerAddressLine6","xml:/TerCustBillingOrg/@TerAddressLine6")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerCity<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerCity","xml:/TerCustBillingOrg/@TerCity")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerState<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerState","xml:/TerCustBillingOrg/@TerState")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerCountry<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerCountry","xml:/TerCustBillingOrg/@TerCountry")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerZipCode<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerZipCode","xml:/TerCustBillingOrg/@TerZipCode")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerDayPhone<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerDayPhone","xml:/TerCustBillingOrg/@TerDayPhone")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerEveningPhone<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerEveningPhone","xml:/TerCustBillingOrg/@TerEveningPhone")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerMobilePhone<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerMobilePhone","xml:/TerCustBillingOrg/@TerMobilePhone")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerBeeper<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerBeeper","xml:/TerCustBillingOrg/@TerBeeper")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerOtherPhone<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerOtherPhone","xml:/TerCustBillingOrg/@TerOtherPhone")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerDayFaxNo<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerDayFaxNo","xml:/TerCustBillingOrg/@TerDayFaxNo")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerEveningFaxNo<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerEveningFaxNo","xml:/TerCustBillingOrg/@TerEveningFaxNo")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerEMailID<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerEMailID","xml:/TerCustBillingOrg/@TerEMailID")%> /></td>
					<td class="tablecolumn"><input type="text" id="TerAlternateEmailID<%=TerCustBillingOrgCounter%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerAlternateEmailID","xml:/TerCustBillingOrg/@TerAlternateEmailID")%> /></td>
					<td class="tablecolumn">
					<input class="checkbox" type="checkbox" id="flag<%=TerCustBillingOrgCounter%>" OldValue="<%=resolveValue("xml:/TerCustBillingOrg/@TerDefaultFLag")%>" <%=getCheckBoxOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+TerCustBillingOrgCounter+"/@TerDefaultFLag","xml:/TerCustBillingOrg/@TerDefaultFLag","Y")%> onclick="checkCurrentBoxOnly(this);"/></td>
				</tr>
			<%}%>
		</yfc:loopXML>
		
		<%for(i=currentRecord; i<=noOfBlankRecords; i++){%>
			<tr>
				<input type="hidden" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerShipToOrg",orgKey)%>/>
				<td class="tablecolumn"><input type="text" id="TerBillingId<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerBillingID","")%> /><img id="img<%=i%>" class="lookupicon" name="search" onclick="callLookupforBill('TerBillingId<%=i%>','TerBillingIDName<%=i%>','TDBillTo','TerAddressLine1<%=i%>','TerAddressLine2<%=i%>','TerAddressLine3<%=i%>','TerAddressLine4<%=i%>','TerAddressLine5<%=i%>','TerAddressLine6<%=i%>','TerCity<%=i%>','TerState<%=i%>','TerCountry<%=i%>','TerZipCode<%=i%>','TerDayPhone<%=i%>','TerEveningPhone<%=i%>','TerMobilePhone<%=i%>','TerBeeper<%=i%>','TerOtherPhone<%=i%>','TerDayFaxNo<%=i%>','TerEveningFaxNo<%=i%>','TerEMailID<%=i%>','TerAlternateEmailID<%=i%>');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for BillToID") %> /></td>
				<td class="tablecolumn"><input type="text" id="TerBillingIDName<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerBillingName","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerAddressLine1<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerAddressLine1","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerAddressLine2<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerAddressLine2","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerAddressLine3<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerAddressLine3","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerAddressLine4<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerAddressLine4","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerAddressLine5<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerAddressLine5","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerAddressLine6<%=i%>" name="<%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerAddressLine6","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerCity<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerCity","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerState<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerState","")%>  /></td>
				<td class="tablecolumn"><input type="text" id="TerCountry<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerCountry","")%>  /></td>
				<td class="tablecolumn"><input type="text" id="TerZipCode<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerZipCode","")%>  /></td>
				<td class="tablecolumn"><input type="text" id="TerDayPhone<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerDayPhone","")%>  /></td>
				<td class="tablecolumn"><input type="text" id="TerEveningPhone<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerEveningPhone","")%>  /></td>
				<td class="tablecolumn"><input type="text" id="TerMobilePhone<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerMobilePhone","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerBeeper<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerBeeper","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerOtherPhone<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerOtherPhone","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerDayFaxNo<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerDayFaxNo","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerEveningFaxNo<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerEveningFaxNo","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerEMailID<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerEMailID","")%> /></td>
				<td class="tablecolumn"><input type="text" id="TerAlternateEmailID<%=i%>" <%=getTextOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerAlternateEmailID","")%> /></td>
				<td><input class="checkbox" type="checkbox" id="flag<%=i%>" <%=getCheckBoxOptions("xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerDefaultFLag","N","Y")%> onclick="checkCurrentBoxOnly(this);"/></td>
			</tr>
		<%}%>	
		<input type="hidden" id="record" value="<%=i%>" />		
    </tbody>
	<tfoot>        
		<tr>
        	<td nowrap="true" colspan="22">
        		<table class="tablefooter" width="100%" cellspacing="0" cellpadding="0" >
					<tr>
						<td align="left">
							<table class="tablefooter" cellspacing="0" style="border:0px">
							<tr>
								<td width="18px">
									<input type="text" name="numCopyAdd" class="tablefooterinput" value="1" size="1" maxlength="2" minValue="1" maxValue="99" minValueString="1" maxValueString="99" contentEditable="true"/>
								</td>
								<td>
									<IMG class=icon src='../console/icons/add.gif' alt='<%=getI18N("Add_Row")%>' onclick="addRow('numCopyAdd','billToBody','record');" style='width:12px;height:12px'/>
								</td>
							</tr>
							</table>
						</td>
						<td align="right"></td>
					</tr>
				</table>
        	</td>
        </tr>
    </tfoot>
</table>

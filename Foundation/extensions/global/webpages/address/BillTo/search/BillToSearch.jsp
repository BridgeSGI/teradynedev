<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>

<table class="view">
	<tr>
		<td><yfc:i18n>TD_Billing_ID</yfc:i18n></td>
	</tr>
	<tr>
		<td>
			<select name="xml:/TerCustBillingOrg/@TerBillingIDQryType" class="combobox" >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/TerCustBillingOrg/@TerBillingIDQryType"/>
			</select>
			<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerCustBillingOrg/@TerBillingID")%>/>
		</td>
	</tr>
	<tr>
		<td><yfc:i18n>TD_Bill_To_Name</yfc:i18n></td>
	</tr>
	<tr>
		<td>
			<select name="xml:/TerCustBillingOrg/@TerBillingNameQryType" class="combobox" >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/TerCustBillingOrg/@TerBillingNameQryType"/>
			</select>
			<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TerCustBillingOrg/@TerBillingName")%>/>
		</td>
	</tr>
	<tr>
		<td>
			<fieldset>
				<legend><yfc:i18n>Bill_To_Address</yfc:i18n></legend>
				<table class="view" height="100%" width="100%">
					<tr>
						<td class="searchlabel">
							<yfc:i18n>Address_Line_1</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td nowrap="true" class="searchcriteriacell">
							<select name="xml:/TerCustBillingOrg/@TerAddressLine1QryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
								value="QueryType" selected="xml:/TerCustBillingOrg/@TerAddressLine1QryType"/>
							</select>
							<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerCustBillingOrg/@TerAddressLine1")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel">
							<yfc:i18n>Address_Line_2</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/TerCustBillingOrg/@TerAddressLine2QryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/TerCustBillingOrg/@TerAddressLine2QryType"/>
							</select>
							<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerCustBillingOrg/@TerAddressLine2")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>City</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/TerCustBillingOrg/@TerCityQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/TerCustBillingOrg/@TerCityQryType"/>
							</select>
							<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerCustBillingOrg/@TerCity")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>State</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/TerCustBillingOrg/@TerStateQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/TerCustBillingOrg/@TerStateQryType"/>
							</select>
							<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerCustBillingOrg/@TerState")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>Postal_Code</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/TerCustBillingOrg/@TerZipCodeQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/TerCustBillingOrg/@TerZipCodeQryType"/>
							</select>
							<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerCustBillingOrg/@TerZipCode")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>Country</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/TerCustBillingOrg/@TerCountryQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/TerCustBillingOrg/@TerCountryQryType"/>
							</select>
							<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/TerCustBillingOrg/@TerCountry")%>/>
						</td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>
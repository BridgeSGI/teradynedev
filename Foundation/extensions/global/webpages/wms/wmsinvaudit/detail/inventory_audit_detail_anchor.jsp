<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<table class="anchor" cellpadding="7px"  cellSpacing="0" >
<% 
String ItemID =  resolveValue("xml:/LocationInventoryAudit/InventoryItem/@ItemID");
if (isVoid(ItemID)) { %>
<tr>
    <td colspan="3" valign="top">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I02"/>
        </jsp:include>
    </td>
</tr>
<tr>
    <td width="40%" height="100%" valign="top">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I08"/>
        </jsp:include>
    </td>
    <td width="45%" height="100%" valign="top">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I06"/>
        </jsp:include>
    </td>
</tr>
<tr>
    <td colspan="3" valign="top">
		<jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I09"/>
        </jsp:include>
    </td>
</tr>
<%}else{%>
<yfc:callAPI apiID='AP1'/>
<yfc:callAPI apiID='AP3'/>
<tr>
    <td colspan="3" valign="top">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I01"/>
        </jsp:include>
    </td>
</tr>
<tr>
    <td width="40%" height="100%" valign="top">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I05"/>
        </jsp:include>
    </td>
    <td width="45%" height="100%" valign="top">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I06"/>
        </jsp:include>
    </td>
</tr>
<tr>
	<td width="40%" height="100%" valign="top">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I03"/>
        </jsp:include>
    </td>
    <td width="40%" height="100%" valign="top">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I04"/>
        </jsp:include>
    </td>
</tr>
<tr>
	<td width="40%" height="100%" valign="top">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I10"/>
        </jsp:include>
    </td>
</tr>

<%}%>
<tr>
   <yfc:hasXMLNode binding="xml:LocationInventoryAudit/TagDetail">	
   <yfc:hasXMLNode binding="xml:ItemDetails:/Item/InventoryTagAttributes">
<% YFCElement tagElem =(  YFCElement) request.getAttribute("Item");
   if(tagElem!=null){
	   request.setAttribute("Item", tagElem);
   }
%>
		<td colspan="3">
				<jsp:include page="/yfc/innerpanel.jsp" flush="true" >
		            <jsp:param name="CurrentInnerPanelID" value="I07"/>
	                <jsp:param name="Modifiable" value='false'/>
	                <jsp:param name="LabelTDClass" value='detaillabel'/>
	                <jsp:param name="TagContainer" value='LocationInventoryAudit'/>
	                <jsp:param name="TagElement" value='TagDetail'/>
	            </jsp:include>
		</td>
    </yfc:hasXMLNode>
    </yfc:hasXMLNode>
</tr>
</table>
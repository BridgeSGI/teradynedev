<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<table class="view" width="100%">
<tr>
    <td class="detaillabel" ><yfc:i18n>Enterprise</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@EnterpriseCode" name="LocationInventoryAudit" /></td>
    <td class="detaillabel" ><yfc:i18n>Pallet_ID</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@PalletId" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Case_ID</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@CaseId" name="LocationInventoryAudit" /></td>
<tr>
</tr>
	<td class="detaillabel" ><yfc:i18n>Audit_Type</yfc:i18n></td>
  	<td class="protectedtext">
			<%if (equals("+",resolveValue("xml:/LocationInventoryAudit/@AuditOperation"))){%>
		    <yfc:i18n>In</yfc:i18n>
			<%}else{%>
			<yfc:i18n>Out</yfc:i18n>
			<%}%>
    </td>
</tr>
</table>
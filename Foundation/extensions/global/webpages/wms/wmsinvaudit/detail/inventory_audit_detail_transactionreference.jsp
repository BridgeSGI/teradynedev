<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<table class="view" width="100%">
<tr>
	<td class="detaillabel" ><yfc:i18n>Order_#</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@OrderNo" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Release_#</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@ReleaseNo" name="LocationInventoryAudit"/> </td>
	<td class="detaillabel" ><yfc:i18n>Order_Line_#</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@PrimeLineNo" name="LocationInventoryAudit" /></td>
</tr>
<tr>	
	<td class="detaillabel" ><yfc:i18n>Shipment_#</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@ShipmentNo" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Container_#</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@ContainerNo" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Batch_#</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@BatchNo" name="LocationInventoryAudit" /></td>
</tr>
<tr>	
	<td class="detaillabel" ><yfc:i18n>Program_ID</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@Modifyprogid" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Adjustment_Reason_Code</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@ReasonCode" name="LocationInventoryAudit" /></td>
    <td class="detaillabel" ><yfc:i18n>Reason_Text</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@ReasonText" name="LocationInventoryAudit" /></td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Work_Order_#</yfc:i18n></td>
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/WorkOrder/@WorkOrderNo" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Wave_#</yfc:i18n></td>
	<td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@WaveNo" name="LocationInventoryAudit" /></td>
</tr>
</table>
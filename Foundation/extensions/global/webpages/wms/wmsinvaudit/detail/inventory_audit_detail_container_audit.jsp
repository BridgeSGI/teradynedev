<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<table class="view" width="100%">
<tr>
    <yfc:makeXMLInput name="locationKey">
            <yfc:makeXMLKey binding="xml:/Location/@Node" value="xml:/LocationInventoryAudit/@Node" />
            <yfc:makeXMLKey binding="xml:/Location/@LocationId" value="xml:/LocationInventoryAudit/@LocationId" />
    </yfc:makeXMLInput>
	<td class="detaillabel" ><yfc:i18n>Location</yfc:i18n></td>
    <td class="protectedtext">
	 <a <%=getDetailHrefOptions("L01", getParameter("locationKey"),"")%> >
		<yfc:getXMLValue binding="xml:/LocationInventoryAudit/@LocationId" name="LocationInventoryAudit" />
	 </a>
	</td>
	<td class="detaillabel" ><yfc:i18n>Date</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@Createts" name="LocationInventoryAudit" /></td>
   
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>User_ID</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@Modifyuserid" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Task_Type</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@TaskType" name="LocationInventoryAudit" /></td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Parent_Pallet_ID</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@OuterMostPalletId" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Parent_Case_ID</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@OuterMostCaseId" name="LocationInventoryAudit" /></td>
</tr>
<tr>
	<td class="detaillabel" ><yfc:i18n>Adjustment_Type</yfc:i18n></td>
    <td class="protectedtext"><yfc:i18n> 
		<yfc:getXMLValue  binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@AdjustmentType"/> </yfc:i18n>
	</td>
</tr>
</table> 
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/modificationutils.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<%if(equals("Y",request.getParameter("DivRequired"))){%>
<div style="height:250px;overflow:auto">
<%}%>
<table class="table" editable="false" width="100%" cellspacing="0">
<%
	boolean bylpn = false;
	String serachcriteria = (String) request.getAttribute("SearchCriteriaIs");
	if(serachcriteria!=null)
	{
	if (equals("ByLPN",serachcriteria)) {
		bylpn = true;
	}
	}
%>
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>	
        <td class="tablecolumnheader">
            <yfc:i18n>Activity_Date</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/CountRequest/@CountRequestNo")%>">
            <yfc:i18n>Count_Request_#</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/@LocationId")%>">
            <yfc:i18n>Location</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/@EnterpriseCode")%>">
            <yfc:i18n>Enterprise</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="">
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="">
            <yfc:i18n>Item_Description</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@ProductClass")%>">
            <yfc:i18n>Product_Class</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@UnitOfMeasure")%>">
            <yfc:i18n>Unit_Of_Measure</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/@InventoryStatus")%>">
            <yfc:i18n>Inventory_Status</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/@Segment")%>">
            <yfc:i18n>Segment</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/@SegmentType")%>">
            <yfc:i18n>Segment_Type</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/@Modifyuserid")%>">
            <yfc:i18n>User_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:LocationInventoryAudit:/LocationInventoryAudit/@Quantity")%>">
            <yfc:i18n>Quantity</yfc:i18n>
        </td>		
    </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:LocationInventoryAudit:/LocationInventoryAudits/@LocationInventoryAudit" id="LocationInventoryAudit"> 
<%   if (bylpn) {
		if((!isVoid(resolveValue("xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@ItemID"))) && (!isVoid(resolveValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@PalletId")) || !isVoid(resolveValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@CaseId")))){  %>
			<tr> 
				<yfc:makeXMLInput name="locnInventoryAuditKey">
					<yfc:makeXMLKey binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@LocnInventoryAuditKey" value="xml:LocationInventoryAudit:/LocationInventoryAudit/@LocnInventoryAuditKey" />
				  </yfc:makeXMLInput>
				<td class="checkboxcolumn">
					<input type="checkbox"  name="LocEntityKey" value='<%=getParameter("locnInventoryAuditKey")%>' />
				</td>
				<td class="tablecolumn" sortValue="<%=getDateValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@Createts")%>">
				<a <%=getDetailHrefOptions("L01", getParameter("locnInventoryAuditKey"),"")%> >
				  <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@Createts"/>
				 </a>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/CountRequest/@CountRequestNo"/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@LocationId"/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@EnterpriseCode"/>
				</td>
				
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@ItemID"/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" 
					binding="xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/Item/PrimaryInformation/@Description" />
				</td>

				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@ProductClass"/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@UnitOfMeasure"/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@InventoryStatus"/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@Segment"/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@SegmentType"/>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@Modifyuserid"/>
				</td>
				<td class="numerictablecolumn" sortValue="
					<%if (equals("+",resolveValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@AuditOperation"))){%>
					+<%}else{%>
					-<%}%>
					<%=getNumericValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@Quantity")%>">
					<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@Quantity"/>
					<%if (equals("+",resolveValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@AuditOperation"))){%><yfc:i18n>+</yfc:i18n>
					<%}else{%><yfc:i18n>-</yfc:i18n>
					<%}%>

				</td>
			</tr>
		<% }
		} else {
		if(!isVoid(resolveValue("xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@ItemID"))){%>  

    <tr> 
        <yfc:makeXMLInput name="locnInventoryAuditKey">
            <yfc:makeXMLKey binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@LocnInventoryAuditKey" value="xml:LocationInventoryAudit:/LocationInventoryAudit/@LocnInventoryAuditKey" />
          </yfc:makeXMLInput>
        <td class="checkboxcolumn">
			<input type="checkbox"  name="LocEntityKey" value='<%=getParameter("locnInventoryAuditKey")%>' />
		</td>
        <td class="tablecolumn" sortValue="<%=getDateValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@Createts")%>">
		<a <%=getDetailHrefOptions("L01", getParameter("locnInventoryAuditKey"),"")%> >
		  <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@Createts"/>
		 </a>
        </td>
		<td class="tablecolumn">
            <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/CountRequest/@CountRequestNo"/>
        </td>
		<td class="tablecolumn">
            <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@LocationId"/>
        </td>
		<td class="tablecolumn">
            <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@EnterpriseCode"/>
        </td>
        
        <td class="tablecolumn">
            <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@ItemID"/>
        </td>
        <td class="tablecolumn">
	        <yfc:getXMLValue name="LocationInventoryAudit" 
			binding="xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/Item/PrimaryInformation/@Description" />
        </td>

        <td class="tablecolumn">
            <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@ProductClass"/>
        </td>
        <td class="tablecolumn">
            <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/InventoryItem/@UnitOfMeasure"/>
        </td>
		<td class="tablecolumn">
            <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@InventoryStatus"/>
        </td>
		<td class="tablecolumn">
            <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@Segment"/>
        </td>
		<td class="tablecolumn">
            <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@SegmentType"/>
        </td>
		<td class="tablecolumn">
			<yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@Modifyuserid"/>
		</td>
		<td class="numerictablecolumn" sortValue="
			<%if (equals("+",resolveValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@AuditOperation"))){%>
		    +<%}else{%>
			-<%}%>
			<%=getNumericValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@Quantity")%>">
		    <yfc:getXMLValue name="LocationInventoryAudit" binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@Quantity"/>
			<%if (equals("+",resolveValue("xml:LocationInventoryAudit:/LocationInventoryAudit/@AuditOperation"))){%><yfc:i18n>+</yfc:i18n>
			<%}else{%><yfc:i18n>-</yfc:i18n>
			<%}%>

        </td>
    </tr>
		<%} 
		}%>
    </yfc:loopXML> 
</tbody>
</table>
<%if(equals("Y",request.getParameter("DivRequired"))){%>
</div>
<%}%>

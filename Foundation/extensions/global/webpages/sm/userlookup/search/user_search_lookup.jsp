<%@include file="/yfsjspcommon/yfsutil.jspf"%>

<%
	String sOrganizationKey = getParameter("OrganizationKey");
	if (isVoid(sOrganizationKey)) {
		sOrganizationKey = resolveValue("xml:/User/@OrganizationKey");
	}
	sOrganizationKey = sOrganizationKey == null ? "" : sOrganizationKey;

	YFCDocument inputDoc = YFCDocument.parse("<UserGroup UsergroupId=\"TER_REVIEW_ENGG\"/>");
	YFCDocument outputTemplate = YFCDocument.parse("<UserGroup UsergroupKey=\"\"/>");
%>
	<yfc:callAPI apiName="getUserGroupList" inputElement="<%=inputDoc.getDocumentElement()%>" templateElement="<%=outputTemplate.getDocumentElement()%>" outputNamespace="usergrp"/>
<%
	String userGroupKey = "";
	YFCElement userGroupList = getElement("usergrp");
	YFCElement userGroup = userGroupList.getChildElement("UserGroup");
	if (!isVoid(userGroup)) {
		userGroupKey = userGroup.getAttribute("UsergroupKey");
	}
%>

<table class="view">
	<tr>
		<td>
			<input type="hidden" name="xml:/User/@OrganizationKey" value='<%=sOrganizationKey%>'/>
			<input type="hidden" name="xml:/User/UserGroupLists/UserGroupList/@UsergroupKey" value='<%=userGroupKey%>'/>
		</td>
	</tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>User_ID</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/User/@LoginidQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc" value="QueryType" selected="xml:/User/@LoginidQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/User/@Loginid")%>/>
        </td>
    </tr>

	<tr>
        <td class="searchlabel" >
            <yfc:i18n>User_Name</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/User/@UsernameQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/User/@UsernameQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/User/@Username")%>/>
        </td>
    </tr>
	
	<tr>
        <td class="searchcriteriacell">
            <input type="checkbox" <%=getCheckBoxOptions("xml:/User/@OnlyLoggedInUsers", "xml:/User/@OnlyLoggedInUsers","Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' ><yfc:i18n>Only_Logged_In_Users</yfc:i18n>
        </td>
    </tr>
</table>
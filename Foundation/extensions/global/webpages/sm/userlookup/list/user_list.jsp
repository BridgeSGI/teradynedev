<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td class="lookupiconheader"><br /></td>
        <td class="tablecolumnheader" nowrap="true">
            <yfc:i18n>Organization</yfc:i18n>
        </td>
        <td class="tablecolumnheader" nowrap="true">
            <yfc:i18n>User_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" nowrap="true">
            <yfc:i18n>User_Name</yfc:i18n>
        </td>
        <td class="tablecolumnheader" nowrap="true">
            <yfc:i18n>Login_Status</yfc:i18n>
        </td>
   </tr>
</thead>
<tbody>
    <yfc:loopXML name="UserList" binding="xml:/UserList/@User" id="user"> 
    
    <% String loginStatus = getValue("user","xml:/User/@LoginStatus"); %>

    <tr> 
        <td class="tablecolumn">
            <img class="icon" onClick="setLookupValue(this.value)"  value="<%=resolveValue("xml:user:/User/@Username")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue name="user" binding="xml:/User/@OrganizationKey"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="user" binding="xml:/User/@Loginid"/></td>
        <td class="tablecolumn"><yfc:getXMLValueI18NDB name="user" binding="xml:/User/@Username"/></td>
		<td class="tablecolumn">
			<% if (equals(loginStatus, "Y")) { %>
				<img class="icon" <%=getImageOptions(YFSUIBackendConsts.USER_LOGGED_IN_ICON, "Logged_In")%>/>
			<% } else { %>
				&nbsp;
			<% } %>
		</td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
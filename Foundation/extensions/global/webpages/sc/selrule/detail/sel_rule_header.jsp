<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript">
<%
	
	String srKey = resolveValue("xml:TerSCTSelectionRule/@TerSRKey");
	if (!isVoid(srKey)) {
		YFCDocument srDoc = YFCDocument.createDocument("TerSCTSelectionRule");
		YFCElement srEle = srDoc.getDocumentElement();
		srEle.setAttribute("TerSRKey",srKey);
%>
		function showDetail() {
			showDetailFor('<%=srDoc.getDocumentElement().getString(false)%>');
		}
		window.attachEvent("onload", showDetail);
	<%
	}
%>
</script>

<table class="view" width="100%"  >
	<yfc:callAPI apiID="AP1" />
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SR_ID</yfc:i18n></td>
        <td>
			<input type="text" size="15" maxLength="30" class="unprotectedinput" id="srID" <%=getTextOptions("xml:/TerSCTSelectionRule/@TerSRID")%>/>
			<input type="hidden" class="unprotectedinput" value="Y" <%=getTextOptions("xml:/TerSCTSelectionRule/@TerSRDraft")%>/>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_SR_Name</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" size="40" maxLength="50" class="unprotectedinput"  <%=getTextOptions("xml:/TerSCTSelectionRule/@TerSRName")%>/>
        </td>
	</tr>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf"%>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/css/scripts/editabletbl.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>

<input type="hidden" class="unprotectedinput" <%=getTextOptions("xml:/TerSCTSelectionRule/@TerSRKey")%> <%=resolveValue("xml:/TerSCTSelectionRule/@TerSRKey")%>/>
<input type="hidden" name="xml:/TerSelectionRuleLine/@TerSRLReasonCode" />
<input type="hidden" name="xml:/TerSelectionRuleLine/@TerSRLReasonText" />

<%
request.setAttribute("RootNodeName","TerSCTSelectionRule");
request.setAttribute("ChildLoopXMLName","TerSelectionRuleLine");
request.setAttribute("ChildLoopXMLKeyName","TerSRLKey");
request.setAttribute("ChildLoopParentXMLName","TerSelectionRuleLineList");
	
%>
<%
     //System.out.println("Comes");
	 String ssOperation = request.getParameter("userOperation");
     YFCElement eerrors = (YFCElement)request.getAttribute("YFC_EXCEPTIONXML");
	 YFCElement rroot = null;
	 YFCElement oNewOrder = null;
	 YFCElement eOrigAPIElem = null;
     
	 YFCElement ddetailAPIRoot = (YFCElement) request.getAttribute("TerSCTSelectionRule"); //Detail API Output
	 YFCElement allowModsElem = ddetailAPIRoot.getChildElement("AllowedModifications", true);
	 YFCElement allowModElem = allowModsElem.getChildElement("AllowedModification", true);
	 allowModElem.setAttribute("ModificationType","OTHERS");
	 allowModsElem.appendChild(allowModElem);
	 ddetailAPIRoot.appendChild(allowModsElem);
	 request.setAttribute("TerSCTSelectionRule",ddetailAPIRoot);
	 eOrigAPIElem = (YFCElement)ddetailAPIRoot.cloneNode(true);
	 request.setAttribute("OrigAPIOutput",eOrigAPIElem);
	 String allowedBinding = "xml:/TerSCTSelectionRule/AllowedModifications";
	 //System.out.println("String allowedBinding:"+allowedBinding+"::"+ddetailAPIRoot.toString());
	 HashMap appointmentOrderAttrs  = new HashMap();
	 appointmentOrderAttrs.put("TerSelectionRuleLine","TerSRLKey");
	
	 if(!isVoid(eerrors) || equals(ssOperation,"Y") || equals(ssOperation,"DELETE")){
		rroot = getRequestDOM(); //Data that Posted from the Detail View
		request.setAttribute("getRequestDOMOutput",rroot);
		if(rroot != null)
			oNewOrder = rroot.getChildElement("TerSCTSelectionRule");
		if(oNewOrder !=null)		
		{
			//System.out.println("Does it come here");
			mergeRequestAndAPIOutput(ddetailAPIRoot,oNewOrder,appointmentOrderAttrs);
		}
	}
	YFCElement selectionRuleLineList = ddetailAPIRoot.getChildElement("TerSelectionRuleLineList");
	int childNodes = countChildElements(selectionRuleLineList); 
%>
<input type="hidden" class="unprotectedinput" id="childNodes" value="<%=childNodes%>" />
<table class="table" border="0" cellspacing="0" ID="SCTemplates" width="100%" yfcMaxSortingRecords="1000">
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
			<input type="hidden" id="userOperation" name="userOperation" value="" />
            <input type="hidden" id="numRowsToAdd" name="numRowsToAdd" value="" />
            <input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader" nowrap="true" colspan="1">
        <yfc:i18n>TD_SCT_ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true" colspan="1">
            <yfc:i18n>TD_SCT_Name</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
	<yfc:loopXML name="TerSCTSelectionRule" binding="xml:/TerSCTSelectionRule/TerSelectionRuleLineList/@TerSelectionRuleLine" id="TerSelectionRuleLine"  keyName="TerSRLineKey" > 
	<% String prefixBinding = "xml:/TerSCTSelectionRule/TerSelectionRuleLineList/@TerSelectionRuleLine_"+TerSelectionRuleLineCounter; %>
    <tr> 
		<yfc:makeXMLInput name="srLineKey">
            <yfc:makeXMLKey binding="xml:/TerSelectionRuleLine/@TerSRLineKey" value="xml:/TerSelectionRuleLine/@TerSRLineKey" />
			<yfc:makeXMLKey binding="xml:/TerSelectionRuleLine/@TerSRKey" value="xml:/TerSelectionRuleLine/@TerSRKey" />
		</yfc:makeXMLInput> 
        <td class="checkboxcolumn">
            <input type="checkbox" value='<%=getParameter("srLineKey")%>' name="chkEntityKey"/>
		</td>
        <td class="tablecolumn"><yfc:getXMLValue name="TerSelectionRuleLine" binding="xml:/TerSelectionRuleLine/TERSerContTemp/@TerSCTID"/></a></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TerSelectionRuleLine" binding="xml:/TerSelectionRuleLine/TERSerContTemp/@TerSCTName"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
<tfoot>
    <tr style='display:none' TemplateRow="true">
        <%String tempBinding = "xml:/TerSCTSelectionRule/TerSelectionRuleLineList/TerSelectionRuleLine_";%>
        <td><input type="hidden" <%=yfsGetTemplateRowOptions(tempBinding+"/@TerSCTKey",allowedBinding,"OTHERS","text")%>/></td>
        
		<td class="tablecolumn" nowrap="true">
			<input type="text" readonly <%=yfsGetTemplateRowOptions(tempBinding+"/TERSerContTemplate/@TerSCTID",allowedBinding,"OTHERS","text")%> />
			<img class="lookupicon" onclick="tempRowSCTLookup(this, 'TerSCTKey','TerSCTID','TerSCTName','','servicecontractstemplat','xml:/TerSCTVw/@CallForLookup=Y')" 
			<%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_servicecontractstemplat") %> />
		</td>
		<td class="tablecolumn" nowrap="true">
			<input size="40" type="text" readonly <%=yfsGetTemplateRowOptions(tempBinding+"/TERSerContTemplate/@TerSCTName",allowedBinding,"OTHERS","text")%>/>		
		</td>
	</tr>
	<%if (isModificationAllowedWithModType("OTHERS",allowedBinding)) { %> 
    <tr>
    	<td nowrap="true" colspan="3">
    		<jsp:include page="/common/editabletbl.jsp" flush="true" />
    	</td>
	</tr>
	<%}%>
</tfoot>
</table>

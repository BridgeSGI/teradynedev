<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>

<% String draftFlag = resolveValue("xml:/TerSCTSelectionRule/@TerSRDraft");%>
<input type="hidden" id="hiddenDraftFlag" value='<%=draftFlag%>' />
<input type="hidden" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerSRKey")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerSRKey")%>/>
<%if(draftFlag.equals("Y")){%>
<input type="hidden" class="unprotectedinput" value="N" <%=getTextOptions("xml:/TerSCTSelectionRule/@TerSRDraft")%> />
<%}%>
<input type="hidden" name="xml:/TerSCTSelectionRule/@TerSRReasonCode" />
<input type="hidden" name="xml:/TerSCTSelectionRule/@TerSRReasonText" />

<yfc:callAPI apiID="AP1" />
<yfc:callAPI apiID="AP2" />
<yfc:callAPI apiID="AP3" />
<table class="view" width="100%">
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SR_Owner_Org</yfc:i18n>:</td>
		<td>
			<select name="xml:/TerSCTSelectionRule/@TerOwnerOrgIDQType" class="combobox">
                <option value="FLIKE" >starts with</option>
            </select>
			<input type="text" size="15" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerOwnerOrgID")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerOwnerOrgID")%>/>
			<% String buyerExtraParams = "role=BUYER&forOrder=Y&enterprise=CSO&docType=0018.ex"; %>
			<img class="lookupicon" onclick="callSCOrgLookup('xml:/TerSCTSelectionRule/@TerOwnerOrgID','','TDORGL','<%=buyerExtraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TDORGL")%> />
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SR_IB_Node</yfc:i18n>:</td>
		<td>
			<select name="xml:/TerSCTSelectionRule/@TerInstallNodeQType" class="combobox">
                <option value="FLIKE" >starts with</option>
            </select>
			<input type="text" size="15" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerInstallNode")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerInstallNode")%> />
			<img class="lookupicon" onclick="callSCOrgLookup('xml:/TerSCTSelectionRule/@TerInstallNode','','TDORGL','<%=buyerExtraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TDORGL")%> />
        </td>
	</tr>
</table>
<table class="view" width="100%">
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_IB_ITEM_ID</yfc:i18n>:</td>
		<td>
			<select name="xml:/TerSCTSelectionRule/@TerItemIDQType" class="combobox">
                <option value="FLIKE" >starts with</option>
            </select>
			<input type="text" size="15"  class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerItemID")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerItemID")%>/>
			<%String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode","CSO"); %>
			<img class="lookupicon" onclick="callItemLookup('xml:/TerSCTSelectionRule/@TerItemID',' ',' ','IBITDNitem','<%=extraParams%>')" 
			<%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_Build_Status</yfc:i18n></td>
        <td>
			<select name="xml:/TerSCTSelectionRule/@TerBuildStatus" class="combobox"  >
				<yfc:loopOptions binding="xml:BuildStatus:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue"  selected="xml:/TerSCTSelectionRule/@TerBuildStatus" targetBinding="xml:/TerSCTSelectionRule/@TerBuildStatus"/>
			</select>
        </td>
	</tr>
</table>
<table class="view" width="100%">
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_RESP_MFG_ID</yfc:i18n>:</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerRespProductMfgID")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerRespProductMfgID")%>/><img class="lookupicon" onclick="			callPGLookup('xml:/TerSCTSelectionRule/@TerRespProductMfgID','xml:/TerSCTSelectionRule/@TerRespProdMfgDivCode','TDPG','')" 
			<%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TDPG") %> />
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_RESP_MFG_DIV</yfc:i18n></td>
        <td>
			<input type="text" size="15" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerRespProdMfgDivCode")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerRespProdMfgDivCode")%> />
        </td>
	</tr>
</table>
<table class="view" width="100%">
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_Country_Code</yfc:i18n></td>
        <td>
			<select name="xml:/TerSCTSelectionRule/@TerCountryCode" class="combobox"  >
				<yfc:loopOptions binding="xml:CountryCode:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue"  selected="xml:/TerSCTSelectionRule/@TerCountryCode" targetBinding="xml:/TerSCTSelectionRule/@TerCountryCode"/>
			</select>
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_Book_YW</yfc:i18n>:</td>
		<td>
			<input type="text" class="numericunprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerFromBookYw")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerFromBookYw")%>/>
			<span>&nbsp;&nbsp;To&nbsp;&nbsp;</span>
			<input type="text" class="numericunprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerToBookYw")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerToBookYw")%>/>
        </td>
	</tr>
</table>
<table class="view" width="100%">
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_Ship_Date</yfc:i18n>:</td>
		<td>
			<input class="dateinput" type="text" <%=getTextOptions("xml:/TerSCTSelectionRule/@TerFromShipDate_YFCDATE","xml:/TerSCTSelectionRule/@TerFromShipDate_YFCDATE", "")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<span>&nbsp;&nbsp;To&nbsp;&nbsp;</span>
			<input type="text" class="dateinput" <%=getTextOptions("xml:/TerSCTSelectionRule/@TerToShipDate_YFCDATE","xml:/TerSCTSelectionRule/@TerToShipDate_YFCDATE","")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
        </td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<table class="view" width="100%">
	<tr>
		<td align="left">
			<span>Then create Service Contracts using the following Service Contract Template ID's below</span>
		</td>
	</tr>
</table>
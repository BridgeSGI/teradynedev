<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<table width="100%" class="view">
    <% // Now call the APIs that are dependent on the common fields. %>
    <yfc:callAPI apiID="AP1"/>
    <yfc:callAPI apiID="AP2"/>
	<yfc:callAPI apiID="AP3"/>
	
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SR_ID</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
		
        <select name="xml:/TerSRVw/@TerSRIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSRVw/@TerSRIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSRVw/@TerSRID")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SR_Name</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerSRVw/@TerSRNameQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSRVw/@TerSRNameQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSRVw/@TerSRName")%> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SCT_ID</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerSRVw/@TerSCTIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSRVw/@TerSCTIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput"  <%=getTextOptions("xml:/TerSRVw/@TerSCTID")%>/><img class="lookupicon" onclick="callSCTLookup('','xml:/TerSRVw/@TerSCTID','','','servicecontractstemplat','xml:/TerSCTVw/@CallForLookup=Y')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_servicecontractstemplat") %> />
	</td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SR_Owner_Org</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerSRVw/@TerOwnerOrgIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSRVw/@TerOwnerOrgIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSRVw/@TerOwnerOrgID")%>/>
		<% String buyerExtraParams = "role=BUYER&forOrder=Y&enterprise=CSO&docType=0018.ex"; %>
		<img class="lookupicon" onclick="callSCOrgLookup('xml:/TerSRVw/@TerOwnerOrgID','','TDORGL','<%=buyerExtraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TDORGL")%> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SR_IB_Node</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerSRVw/@TerInstallNodeQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSRVw/@TerInstallNodeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSRVw/@TerInstallNode")%>/>
		<img class="lookupicon" onclick="callSCOrgLookup('xml:/TerSRVw/@TerInstallNode','','TDORGL','<%=buyerExtraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TDORGL")%> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_IB_ITEM_ID</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerSRVw/@TerItemIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSRVw/@TerItemIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSRVw/@TerItemID")%>/>
		<%String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode","CSO"); %>
		<img class="lookupicon" onclick="callItemLookup('xml:/TerSRVw/@TerItemID',' ',' ','IBITDNitem','<%=extraParams%>')" 
		<%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
    </td>
</tr>
<tr>
	<td class="searchlabel" ><yfc:i18n>TD_Build_Status</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerSCTSelectionRule/@TerBuildStatus" class="combobox"  >
			<yfc:loopOptions binding="xml:BuildStatus:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue"  selected="xml:/TerSCTSelectionRule/@TerBuildStatus" targetBinding="xml:/TerSCTSelectionRule/@TerBuildStatus"/>
		</select>
    </td>
</tr>
<tr>
	<td class="searchlabel" ><yfc:i18n>TD_RESP_MFG_ID</yfc:i18n>:</td>
</tr>
<tr>
	<td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerSRVw/@TerRespProductMfgIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSRVw/@TerRespProductMfgIDQryType"/>
        </select>
		<input type="text" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerRespProductMfgID")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerRespProductMfgID")%>/><img class="lookupicon" onclick="callPGLookup('xml:/TerSCTSelectionRule/@TerRespProductMfgID','xml:/TerSCTSelectionRule/@TerRespProdMfgDivCode','TDPG','')" 
		<%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TDPG") %> />
    </td>
</tr>
<tr>
	<td class="searchlabel" ><yfc:i18n>TD_RESP_MFG_DIV</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<input type="text" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerRespProdMfgDivCode")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerRespProdMfgDivCode")%> />
    </td>
</tr>
<tr>
	<td class="searchlabel" ><yfc:i18n>TD_Country_Code</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerSCTSelectionRule/@TerCountryCode" class="combobox"  >
			<yfc:loopOptions binding="xml:CountryCode:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue"  selected="xml:/TerSCTSelectionRule/@TerCountryCode" targetBinding="xml:/TerSCTSelectionRule/@TerCountryCode"/>
		</select>
    </td>
</tr>
<tr>
	<td class="searchlabel" ><yfc:i18n>TD_Book_YW</yfc:i18n>:</td>
</tr>
<tr>
	<td>
		<input type="text" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerFromBookYw")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerFromBookYw")%>/>
		<span>&nbsp;&nbsp;To&nbsp;&nbsp;</span>
		<input type="text" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerToBookYw")%> <%=getTextOptions("xml:/TerSCTSelectionRule/@TerToBookYw")%>/>
    </td>
</tr>
<tr>
	<td class="searchlabel" ><yfc:i18n>TD_Ship_Date</yfc:i18n>:</td>
</tr>
<tr>
	<td>
		<input class="dateinput" type="text" <%=getTextOptions("xml:/TerSCTSelectionRule/@TerFromShipDate_YFCDATE","xml:/TerSCTSelectionRule/@TerFromShipDate_YFCDATE", "")%>/>
		<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
		<span>&nbsp;&nbsp;To&nbsp;&nbsp;</span>
		<input type="text" class="dateinput" <%=getTextOptions("xml:/TerSCTSelectionRule/@TerToShipDate_YFCDATE","xml:/TerSCTSelectionRule/@TerToShipDate_YFCDATE","")%>/>
		<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
    </td>
</tr>
</table>

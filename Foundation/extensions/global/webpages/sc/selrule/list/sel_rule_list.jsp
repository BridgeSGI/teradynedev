<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>

<%
YFCElement root = (YFCElement)request.getAttribute("TerSCTSelectionRuleList");
int countElem = countChildElements(root); %>
<script language="javascript">setRetrievedRecordCount(<%=countElem%>);</script>
<table class="table" border="0" cellspacing="0" width="100%">
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader" nowrap="true" 
		style="width:<%=getUITableSize("xml:/TerSCTSelectionRuleList/TerSCTSelectionRule/@TerSRID")%>">
        <yfc:i18n>TD_SR_ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  style="width:<%=getUITableSize("xml:/TerSCTSelectionRuleList/TerSCTSelectionRule/@TerSRName")%>">
        <yfc:i18n>TD_SR_Name</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  style="width:<%=getUITableSize("xml:/TerSCTSelectionRuleList/TerSCTSelectionRule/@TerBuildStatus")%>">
            <yfc:i18n>TD_Build_Status</yfc:i18n>
        </td>
        <td class="tablecolumnheader" 
		style="width:<%= getUITableSize("xml:/TerSCTSelectionRuleList/TerSCTSelectionRule/@TerRespProdMfgDivCode")%>">
        <yfc:i18n>TD_RESP_MFG_DIV</yfc:i18n>
        </td>
		<td class="tablecolumnheader" 
		style="width:<%= getUITableSize("xml:/TerSCTSelectionRuleList/TerSCTSelectionRule/@TerRespProductMfgID")%>" >
        <yfc:i18n>TD_RESP_MFG_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" nowrap="true"  
		style="width:<%= getUITableSize("xml:/TerSCTSelectionRuleList/TerSCTSelectionRule/@TerItemID")%>">
            <yfc:i18n>TD_IB_ITEM_ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  
		style="width:<%= getUITableSize("xml:/TerSCTSelectionRuleList/TerSCTSelectionRule/@TerCountryCode")%>">
            <yfc:i18n>TD_Country_Code</yfc:i18n>
        </td>
	</tr>
</thead>
<tbody>
    <yfc:loopXML name="TerSCTSelectionRuleList" binding="xml:/TerSCTSelectionRuleList/@TerSCTSelectionRule" id="TerSCTSelectionRule"  keyName="TerSRKey" > 
    <tr> 
		<yfc:makeXMLInput name="terSRKey">
            <yfc:makeXMLKey binding="xml:/TerSCTSelectionRule/@TerSRKey" value="xml:/TerSCTSelectionRule/@TerSRKey" />
        </yfc:makeXMLInput> 
        <td class="checkboxcolumn">
            <input type="checkbox" value='<%=getParameter("terSRKey")%>' name="EntityKey"/>
		</td>
        <td class="tablecolumn"><a onclick="javascript:showDetailFor('<%=getParameter("terSRKey")%>');
		return false;" href=""><yfc:getXMLValue name="TerSCTSelectionRule" binding="xml:/TerSCTSelectionRule/@TerSRID"/></a></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TerSCTSelectionRule" binding="xml:/TerSCTSelectionRule/@TerSRName"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TerSCTSelectionRule" binding="xml:/TerSCTSelectionRule/@TerBuildStatus"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="TerSCTSelectionRule" binding="xml:/TerSCTSelectionRule/@TerRespProdMfgDivCode"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="TerSCTSelectionRule" binding="xml:/TerSCTSelectionRule/@TerRespProductMfgID"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TerSCTSelectionRule" binding="xml:/TerSCTSelectionRule/@TerItemID"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TerSCTSelectionRule" binding="xml:/TerSCTSelectionRule/@TerCountryCode"/></td>
	</tr>
    </yfc:loopXML> 
</tbody>
</table>

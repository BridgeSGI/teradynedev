<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table width="100%" class="view">
	<!--
    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="EnterpriseCodeBinding" value="xml:/InventoryItem/@OrganizationCode"/>
        <jsp:param name="EnterpriseCodeLabel" value="Organization"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
        <jsp:param name="ShowDocumentType" value="false"/>
        <jsp:param name="OrganizationListForInventory" value="true"/>
    </jsp:include> -->
    <% // Now call the APIs that are dependent on the common fields. %>
    <yfc:callAPI apiID="AP1"/>
    <yfc:callAPI apiID="AP2"/>
	
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SCT_ID</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
		
        <select name="xml:/TerSCTVw/@TerSCTIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSerContTemplate/@TerSCTIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" id="serviceTemplateID" <%=getTextOptions("xml:/TerSCTVw/@TerSCTID")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SCT_Name</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		
        <select name="xml:/TerSCTVw/@TerSCTNameQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSCTVw/@TerSCTNameQryType"/>
        </select>
        <input type="text" size="40" maxLength="50" class="unprotectedinput" id="serviceTemplateID" <%=getTextOptions("xml:/TerSCTVw/@TerSCTName")%>/>
	</td>
</tr>
<tr>
    <td  class="searchlabel" ><yfc:i18n>TD_SC_Types</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
      	<select id="serviceType" name="xml:/TerSCTVw/@TerSerContType" class="combobox" >
			<yfc:loopOptions binding="xml:/CommonCodeList/@CommonCode" 
					name="CodeValue" value="CodeValue" selected="xml:/TerSCTVw/@TerSerContType"/>
		</select>
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SCT_Priority</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/TerSCTVw/@TerSCTPriorityQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/NumericQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSCTVw/@TerSCTPriorityQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSCTVw/@TerSCTPriority") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SCT_DURATION_DAYS</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/TerSCTVw/@TerSCTDurationDaysQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/NumericQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSCTVw/@TerSCTDurationDaysQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSCTVw/@TerSCTDurationDays") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SCT_Status</yfc:i18n></td>
</tr>
<tr >
	<% boolean isLookupCall = resolveValue("xml:/TerSCTVw/@CallForLookup").equals("Y")? true: false;
	if(isLookupCall){ %>
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/TerSCTVw/@TerSCTStatus" class="combobox" >
            <option value="A" >Active</option>
		</select>
    </td>
	<%}else{%>
	<td class="searchcriteriacell" nowrap="true">
        <select name="xml:/TerSCTVw/@TerSCTStatus" class="combobox" >
            <option value="A" >Active</option>
			<option value="O" >Obsolete</option>
        </select>
    </td>
	<%}%>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_SI_ItemID</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/TerSCTVw/@ItemIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerSCTVw/@ItemIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSCTVw/@ItemID")%> />
		<% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", "CSO"); 
		String extraParams1 = getExtraParamsForTargetBinding("xml:/Item/PrimaryInformation/@ItemType", "SERVICE"); 
		extraParams = extraParams+"&"+extraParams1;  %>
        <img class="lookupicon" onclick="callItemLookup('xml:/TerSCTVw/@ItemID','xml:/TerSCTVw/@ItemKey','xml:/TerSCTVw/@UnitOfMeasure','Seritem','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> /> 
    </td>
	<input type="hidden" class="unprotectedinput"  <%=getTextOptions("xml:/TerSCTVw/@ItemKey")%>  />
</tr>
<tr>
	<td>
		<input type="hidden" <%=getTextOptions("xml:/TerSCTVw/@CallForLookup")%> <%=resolveValue("xml:/TerSCTVw/@CallForLookup")%> />
	</td>
</tr>
</table>

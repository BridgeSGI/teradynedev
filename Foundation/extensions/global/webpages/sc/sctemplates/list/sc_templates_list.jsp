<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>

<%
YFCElement root = (YFCElement)request.getAttribute("TERSerContTemplateVwList");
int countElem = countChildElements(root); %>
<script language="javascript">setRetrievedRecordCount(<%=countElem%>);</script>
<table class="table" border="0" cellspacing="0" width="100%">
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader" nowrap="true" >
        <yfc:i18n>TD_SCT_ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true" >
            <yfc:i18n>TD_SC_Types</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true" >
            <yfc:i18n>Template Name</yfc:i18n>
        </td>
        <td class="tablecolumnheader" nowrap="true">
        <yfc:i18n>Template Priority</yfc:i18n>
        </td>
		<td class="tablecolumnheader" nowrap="true">
        <yfc:i18n>Template Duration Days</yfc:i18n>
        </td>
        <td class="tablecolumnheader" nowrap="true" >
            <yfc:i18n>Template Status</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true" >
            <yfc:i18n>Install Base Association required?</yfc:i18n>
        </td>
	</tr>
</thead>

<tbody>
<% 
String callForLookup = resolveValue("xml:/TERSerContTemplateVwList/@CallForLookup");
boolean isCallFromLookup = callForLookup.equals("Y")?true:false;
%>
    <yfc:loopXML name="TERSerContTemplateVwList" binding="xml:/TERSerContTemplateVwList/@TERSerContTemplateVw" id="TERSerContTemplateVw"  keyName="TerSCTKey" > 
    <tr>
		<%if(isCallFromLookup){%>
		<td class="tablecolumn">
			<img class="icon" onclick="setSCTLookupValue('<%=resolveValue("xml:TERSerContTemplateVw:/TERSerContTemplateVw/@TerSCTKey")%>','<%=resolveValue("xml:TERSerContTemplateVw:/TERSerContTemplateVw/@TerSCTID")%>','<%=resolveValue("xml:TERSerContTemplateVw:/TERSerContTemplateVw/@TerSCTName")%>','<%=resolveValue("xml:TERSerContTemplateVw:/TERSerContTemplateVw/@TerSerContType")%>')"  
			value="<%=resolveValue("xml:TERSerContTemplateVw:/TERSerContTemplateVw/@TerSCTKey")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
		</td>
		<%}else{%>
		<yfc:makeXMLInput name="terSCTKey">
            <yfc:makeXMLKey binding="xml:/TerSerContTemplate/@TerSCTKey" value="xml:/TERSerContTemplateVw/@TerSCTKey" />
        </yfc:makeXMLInput> 
		<td class="checkboxcolumn">
            <input type="checkbox" value='<%=getParameter("terSCTKey")%>' name="EntityKey"/>
			<input type="hidden" name='TerSCTKey_<%=TERSerContTemplateVwCounter%>' value='<%=resolveValue("xml:/TERSerContTemplateVw/@TerSCTKey")%>' />
		</td>
		<%}%>
        <td class="tablecolumn"><%if(!isCallFromLookup){%><a onclick="javascript:showDetailFor('<%=getParameter("terSCTKey")%>');
		return false;" href=""><%}%><yfc:getXMLValue name="TERSerContTemplateVw" binding="xml:/TERSerContTemplateVw/@TerSCTID"/></a></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TERSerContTemplateVw" binding="xml:/TERSerContTemplateVw/@TerSerContType"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TERSerContTemplateVw" binding="xml:/TERSerContTemplateVw/@TerSCTName"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="TERSerContTemplateVw" binding="xml:/TERSerContTemplateVw/@TerSCTPriority"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="TERSerContTemplateVw" binding="xml:/TERSerContTemplateVw/@TerSCTDurationDays"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TERSerContTemplateVw" binding="xml:/TERSerContTemplateVw/@TerSCTStatus"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TERSerContTemplateVw" binding="xml:/TERSerContTemplateVw/@TerIBAssoReqd"/></td>
	</tr>
    </yfc:loopXML> 
</tbody>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<input type="hidden" name="xml:/TerServiceItemList/@TerSIReasonCode" />
<input type="hidden" name="xml:/TerServiceItemList/@TerSIReasonText" />
<table class="table" width="100%">
<thead>
    <tr>
		<td class="checkboxheader" sortable="no">
			<input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>
		</td>
		<td class="tablecolumnheader" nowrap="true" style="width:<%= getUITableSize("xml:/TerServiceItemList/TerServiceItem/YFSItem/@ItemID")%>">
            <yfc:i18n>Service Item ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Service Item Description</yfc:i18n>
        </td>
		<td class="tablecolumnheader" nowrap="true" style="width:<%= getUITableSize("xml:/TerServiceItemList/TerServiceItem/YFSItem/@UnitOfMeasure")%>">
            <yfc:i18n>Selling UOM</yfc:i18n>
        </td>
        <td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/TerServiceItemList/TerServiceItem/@TerSIExpediteDiscount")%>">
            <yfc:i18n>Expedite Discount</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/TerServiceItemList/TerServiceItem/@TerSIExpediteDiscount")%>">
            <yfc:i18n>Repair List Discount</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/TerServiceItemList/TerServiceItem/@TerSIExpediteDiscount")%>">
            <yfc:i18n>Additional Discount</yfc:i18n>
        </td>
        <td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/TerServiceItemList/TerServiceItem/@TerRepPartsCovered")%>">
            <yfc:i18n>Repairable Parts Covered</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/TerServiceItemList/TerServiceItem/@TerNonRepPartsCovered")%>">
            <yfc:i18n>Non Repairable Parts Covered</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
	<yfc:loopXML name="TerServiceItemList" binding="xml:/TerServiceItemList/@TerServiceItem" id="TerServiceItem"> 
	<% String prefixBinding = "xml:/TerServiceItemList/TerServiceItem_"+TerServiceItemCounter; %>
	<tr> 
		<yfc:makeXMLInput name="terSIKey">
			<yfc:makeXMLKey binding="xml:/TerServiceItem/@TerSIKey" value="xml:/TerServiceItem/@TerSIKey" />
		</yfc:makeXMLInput>
		
		<td class="checkboxcolumn"> 
			<input type="checkbox" value='<%=getParameter("terSIKey")%>' name="chkEntityKey"/>
			<input type="hidden" <%=getTextOptions(prefixBinding+ "/@TerSIKey", "xml:/TerServiceItem/@TerSIKey")%> />
		</td>
		<td class="tablecolumn"><yfc:getXMLValue name="TerServiceItem" binding="xml:/TerServiceItem/YFSItem/@ItemID"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="TerServiceItem" binding="xml:/TerServiceItem/YFSItem/PrimaryInformation/@ShortDescription"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TerServiceItem" binding="xml:/TerServiceItem/YFSItem/@UnitOfMeasure"/></td>
		<td class="tablecolumn">
		<select class="combobox" <%=getComboOptions(prefixBinding+"/@TerSIExpediteDiscount")%> >
			<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerServiceItem/@TerSIExpediteDiscount" targetBinding="<%=prefixBinding+"/@TerSIExpediteDiscount"%>"/>
		</select>
		</td>
		<td class="tablecolumn">
		<select <%=getComboOptions(prefixBinding+"/@TerSIRepairListDiscount")%> class="combobox"  >
			<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerServiceItem/@TerSIRepairListDiscount" targetBinding="<%=prefixBinding+"/@TerSIRepairListDiscount"%>"/>
		</select>
		</td>
		<td class="tablecolumn">
		<input type="text" class="numericunprotectedinput" <%=resolveValue("xml:/TerServiceItem/@TerSIAdditionalDiscount")%> 
		<%=getTextOptions(prefixBinding+"/@TerSIAdditionalDiscount", "xml:/TerServiceItem/@TerSIAdditionalDiscount")%> />
		</td>
		<td class="tablecolumn">
			<select  class="combobox" <%=getComboOptions(prefixBinding+"/@TerRepPartsCovered")%> >
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerServiceItem/@TerRepPartsCovered" targetBinding="<%=prefixBinding+"/@TerRepPartsCovered"%>" />
			</select>
		</td>
		<td class="tablecolumn">
			<select class="combobox"  <%=getComboOptions(prefixBinding+"/@TerNonRepPartsCovered")%> >
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerServiceItem/@TerNonRepPartsCovered" targetBinding="<%=prefixBinding+"/@TerNonRepPartsCovered"%>" />
			</select>
		</td>
		
    </tr>
    </yfc:loopXML>
</tbody>
</table>

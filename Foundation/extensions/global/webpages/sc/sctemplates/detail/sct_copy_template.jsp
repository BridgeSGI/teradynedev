<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script language="javascript" >
	IgnoreChangeNames();
	yfcDoNotPromptForChanges(true);
</script>

<%
	String copySCTKey = resolveValue("xml:/TerSerContTemplate/@TerSCTKey");
	String sctID = resolveValue("xml:/TerSerContTemplate/@TerSCTID");
	String newSCTKey = resolveValue("xml:NewTemplate:/TerSerContTemplate/@TerSCTKey");
	System.out.println("newSCTKey::"+newSCTKey);
%>

<script language="javascript">
    <%	if(!isVoid(copySCTKey)){
			if(!isVoid(newSCTKey)){
			YFCDocument tempRoot = YFCDocument.createDocument("TerSerContTemplate");
			tempRoot.getDocumentElement().setAttribute("TerSCTKey",resolveValue("xml:NewTemplate:/TerSerContTemplate/@TerSCTKey"));
    %>
				function showOrderDetailPopup() {
					window.CloseOnRefresh = "Y";
		            callPopupWithEntity('servicecontractstemplat', '<%=tempRoot.getDocumentElement().getString(false)%>');
					window.close();
				}

				function changeToOrderDetailView() {
					showDetailFor('<%=tempRoot.getDocumentElement().getString(false)%>');
				}

	<% 
			if (equals(request.getParameter(YFCUIBackendConsts.YFC_IN_POPUP), "Y")) { 
	%>
				window.attachEvent("onload", showOrderDetailPopup);
	<%		} else {
	%>
				window.attachEvent("onload", changeToOrderDetailView);		
	<%		} 
		}
		}
	%>
</script>
<yfc:callAPI apiID="AP1"/>
<table class="view" width="100%">
    <tr>
        <td>
           <input type="hidden" name="xml:/TerSerContTemplate/@OldTerSCTKey" value="<%=copySCTKey%>"/>
		</td>
    </tr>
	<tr>
        <td class="detaillabel" ><yfc:i18n>Copy_From_Template#</yfc:i18n></td>
        <td><input type="text" class="protectedinput" readonly value=<%=sctID%> /></td>
    <tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_ID</yfc:i18n></td>
        <td>
			<input type="text" size="15" maxLength="30" class="unprotectedinput" <%=resolveValue("xml:/TerSerContTemplate/@TerSCTID")%> <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTID")%>/>
			<input type="hidden" class="unprotectedinput" <%=resolveValue("xml:/TerSerContTemplate/@TerSCTDraft")%> <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTDraft")%> />
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_Name</yfc:i18n></td>
        <td>
			<input type="text" size="40" maxLength="50" class="unprotectedinput"  <%=resolveValue("xml:/TerSerContTemplate/@TerSCTName")%> <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTName")%>/>
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Types</yfc:i18n></td>
		<td>
			<select id="serviceType" name="xml:/TerSerContTemplate/@TerSerContType" class="combobox" >
				<yfc:loopOptions binding="xml:/CommonCodeList/@CommonCode" 
					name="CodeShortDescription" value="CodeValue" selected="xml:/TerSerContTemplate/@TerSerContType"/>
			</select>
        </td>
	</tr>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf"%>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/td.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/css/scripts/editabletbl.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>


<input type="hidden" class="unprotectedinput" <%=resolveValue("xml:/TerSerContTemplate/@TerSCTKey")%> <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTKey")%>/>


<%
request.setAttribute("RootNodeName","TerSerContTemplate");
request.setAttribute("ChildLoopXMLName","TerServiceItem");
request.setAttribute("ChildLoopXMLKeyName","TerSIKey");
request.setAttribute("ChildLoopXMLSecondaryKeyName","TerSCTKey");
request.setAttribute("ChildLoopParentXMLName","TerServiceItemList");
	
%>

<%
     
	 String ssOperation = request.getParameter("userOperation");
     YFCElement eerrors = (YFCElement)request.getAttribute("YFC_EXCEPTIONXML");
	 YFCElement rroot = null;
	 YFCElement oNewOrder = null;
	 YFCElement eOrigAPIElem = null;
     
	 YFCElement ddetailAPIRoot = (YFCElement) request.getAttribute("TerSerContTemplate"); //Detail API Output
	 YFCElement allowModsElem = ddetailAPIRoot.getChildElement("AllowedModifications", true);
	 YFCElement allowModElem = allowModsElem.getChildElement("AllowedModification", true);
	 allowModElem.setAttribute("ModificationType","OTHERS");
	 allowModsElem.appendChild(allowModElem);
	 ddetailAPIRoot.appendChild(allowModsElem);
	 request.setAttribute("TerSerContTemplate",ddetailAPIRoot);
	 eOrigAPIElem = (YFCElement)ddetailAPIRoot.cloneNode(true);
	 request.setAttribute("OrigAPIOutput",eOrigAPIElem);
	 String allowedBinding = "xml:/TerSerContTemplate/AllowedModifications";
	 //System.out.println("String allowedBinding:"+allowedBinding+"::"+ddetailAPIRoot.toString());
	 HashMap appointmentOrderAttrs  = new HashMap();
	 appointmentOrderAttrs.put("TerServiceItem", "TerSIKey");
	
	 if(!isVoid(eerrors) || equals(ssOperation,"Y") || equals(ssOperation,"DELETE")){
		rroot = getRequestDOM(); //Data that Posted from the Detail View
		request.setAttribute("getRequestDOMOutput",rroot);
		if(rroot != null)
			oNewOrder = rroot.getChildElement("TerSerContTemplate");
		if(oNewOrder !=null)		
		{
			//System.out.println("Does it come here");
			mergeRequestAndAPIOutput(ddetailAPIRoot,oNewOrder,appointmentOrderAttrs);
		}
	}
	YFCElement terServiceItemList = ddetailAPIRoot.getChildElement("TerServiceItemList");
	int childNodes = countChildElements(terServiceItemList); 
%>
<input type="hidden" class="unprotectedinput" id="childNodes" value="<%=childNodes%>" />
<table class="table" width="100%" ID="ServiceItems" cellspacing="0" yfcMaxSortingRecords="1000" >
<yfc:callAPI apiID="AP3" />
<yfc:callAPI apiID="AP4" />
<yfc:callAPI apiID="AP1" />
<thead>
    <tr> 
		<td class="checkboxheader" sortable="no" >
            <input type="hidden" id="userOperation" name="userOperation" value="" />
            <input type="hidden" id="numRowsToAdd" name="numRowsToAdd" value="" />
            <input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>
        </td>
		<td class="tablecolumnheader">&nbsp;</td>
        <td class="tablecolumnheader" nowrap="true" >
            <yfc:i18n>Service Item ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader" nowrap="true" >
            <yfc:i18n>Short Description</yfc:i18n>
        </td>
		<td class="tablecolumnheader" nowrap="true" >
            <yfc:i18n>Selling UOM</yfc:i18n>
        </td>
		<td class="tablecolumnheader" nowrap="true" width="10%">
            <yfc:i18n>Service Expedite Discount</yfc:i18n>
        </td>
		<td class="tablecolumnheader" nowrap="true" width="10%">
            <yfc:i18n>Repair List Discount</yfc:i18n>
        </td>
		<td class="tablecolumnheader" nowrap="true" width="10%">
            <yfc:i18n>Additional Discount</yfc:i18n>
        </td>
		<td class="tablecolumnheader" nowrap="true" width="10%" >
            <yfc:i18n>Repairable Parts Covered</yfc:i18n>
        </td>
		<td class="tablecolumnheader" nowrap="true" width="10%" >
            <yfc:i18n>Non Repairable Parts Covered</yfc:i18n>
        </td>
   </tr>
</thead>
<tbody>
	<yfc:loopXML name="TerSerContTemplate"
			binding="xml:/TerSerContTemplate/TerServiceItemList/@TerServiceItem"
			id="TerServiceItem">
	<% String prefixBinding = "xml:/TerSerContTemplate/TerServiceItemList/TerServiceItem_"+TerServiceItemCounter; %>
	<tr>
		<yfc:makeXMLInput name="terSIKey">
			<yfc:makeXMLKey binding="xml:/TerServiceItem/@TerSIKey" value="xml:/TerServiceItem/@TerSIKey" />
			<yfc:makeXMLKey binding="xml:/TerServiceItem/@TerSCTKey" value="xml:/TerServiceItem/@TerSCTKey" />
			<yfc:makeXMLKey binding="xml:/TerServiceItem/@KitCode" value="xml:/TerServiceItem/YFSItem/PrimaryInformation/@KitCode" />
			<yfc:makeXMLKey binding="xml:/TerServiceItem/@TerSIItemKey" value="xml:/TerServiceItem/@TerSIItemKey" />
		</yfc:makeXMLInput>
		
		<yfc:makeXMLInput name="bundleParentKey">
			<yfc:makeXMLKey binding="xml:/TerServiceItem/@TerSIItemKey" value="xml:/TerServiceItem/@TerSIItemKey" />
			<yfc:makeXMLKey binding="xml:/TerServiceItem/@TerSCTKey" value="xml:/TerServiceItem/@TerSCTKey" />
		</yfc:makeXMLInput>
        
		<td class="checkboxcolumn"> 
			<input type="checkbox" value='<%=getParameter("terSIKey")%>' name="chkEntityKey"/>
			<input type="hidden" <%=getTextOptions(prefixBinding+ "/@TerSIKey", "xml:/TerServiceItem/@TerSIKey")%> />
		</td>
		<td class="tablecolumn"><%if (equals(getValue("TerServiceItem","xml:/TerServiceItem/YFSItem/PrimaryInformation/@KitCode"),"BUNDLE")){%>
			<a <%=getDetailHrefOptions("L01", getParameter("bundleParentKey"), "")%> >
				<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Kit_Components")%>>
			</a><%}%>
		</td>
        <td class="tablecolumn" nowrap="true"><yfc:getXMLValue binding="xml:/TerServiceItem/YFSItem/@ItemID" /></td>
		<td class="tablecolumn" nowrap="true"><yfc:getXMLValue binding="xml:/TerServiceItem/YFSItem/PrimaryInformation/@ShortDescription" /></td>
		<td class="tablecolumn" nowrap="true"><yfc:getXMLValue binding="xml:/TerServiceItem/YFSItem/@UnitOfMeasure" /></td>
		<%if(!equals(getValue("TerServiceItem","xml:/TerServiceItem/YFSItem/PrimaryInformation/@KitCode"),"BUNDLE")){%>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" <%=yfsGetTemplateRowOptions(prefixBinding+"/@TerSIExpediteDiscount","xml:/TerServiceItem/@TerSIExpediteDiscount", allowedBinding,"OTHERS","combo")%> >
				<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue"  selected="xml:/TerServiceItem/@TerSIExpediteDiscount" targetBinding="<%=prefixBinding+"/@TerSIExpediteDiscount"%>"/>
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" <%=yfsGetTemplateRowOptions(prefixBinding+"/@TerSIRepairListDiscount","xml:/TerServiceItem/@TerSIRepairListDiscount", allowedBinding,"OTHERS","combo")%> >
				<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerServiceItem/@TerSIRepairListDiscount" targetBinding="<%=prefixBinding+"/@TerSIRepairListDiscount"%>" />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<input type="text"  <%=yfsGetTemplateRowOptions(prefixBinding+"/@TerSIAdditionalDiscount","xml:/TerServiceItem/@TerSIAdditionalDiscount",allowedBinding,"OTHERS","text")%> 
			/>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select  <%=yfsGetTemplateRowOptions(prefixBinding+"/@TerRepPartsCovered","xml:/TerServiceItem/@TerRepPartsCovered", allowedBinding,"OTHERS","combo")%> class="combobox"  >
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerServiceItem/@TerRepPartsCovered" targetBinding="<%=prefixBinding+"/@TerRepPartsCovered"%>" />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select   <%=yfsGetTemplateRowOptions(prefixBinding+"/@TerNonRepPartsCovered","xml:/TerServiceItem/@TerNonRepPartsCovered", allowedBinding,"OTHERS","combo")%> >
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerServiceItem/@TerNonRepPartsCovered" targetBinding="<%=prefixBinding+"/@TerNonRepPartsCovered"%>" />
			</select>
		</td>
		<%} else{%>
		   <td class="tablecolumn" nowrap="true">&nbsp;</td>
		   <td class="tablecolumn" nowrap="true">&nbsp;</td>
		   <td class="tablecolumn" nowrap="true">&nbsp;</td>
		   <td class="tablecolumn" nowrap="true">&nbsp;</td>
		   <td class="tablecolumn" nowrap="true">&nbsp;</td>
		<%}%>
	</tr>	
    </yfc:loopXML>
</tbody>
<tfoot>
    <tr style='display:none' TemplateRow="true">
        <%String tempBinding = "xml:/TerSerContTemplate/TerServiceItemList/TerServiceItem_";%>
		<td>
			<input type="hidden" <%=yfsGetTemplateRowOptions(tempBinding+"/@TerSIItemKey",allowedBinding, "OTHERS", "text")%> />
		</td>
        <td><input type="hidden" <%=yfsGetTemplateRowOptions(tempBinding+"/@KitCode",allowedBinding, "OTHERS", "text")%>/></td>
		
		<td class="tablecolumn" nowrap="true">
			<input type="text" <%=yfsGetTemplateRowOptions(tempBinding+"/YFSItem/@ItemID",allowedBinding,"OTHERS","text")%> />
			<img class="lookupicon" onclick="tempRowServiceItemLookup(this, 'ItemID','TerSIItemKey','ShortDescription','KitCode','UnitOfMeasure','Seritem','xml:/Item/@CallingOrganizationCode=CSO&xml:/Item/PrimaryInformation/@ItemType=SERVICE')" 
			<%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
		</td>
		<td class="tablecolumn" nowrap="true">
			<input type="text" class="protectedinput" readonly  <%=yfsGetTemplateRowOptions(tempBinding+"/YFSItem/PrimaryInformation/@ShortDescription", allowedBinding,"OTHERS","text")%> />		
		</td>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" <%=yfsGetTemplateRowOptions(tempBinding+"/YFSItem/@UnitOfMeasure", allowedBinding,"OTHERS","combo")%> >
				<yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure" value="UnitOfMeasure" />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" <%=yfsGetTemplateRowOptions(tempBinding+"/@TerSIExpediteDiscount", allowedBinding,"OTHERS","combo")%> >
				<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" <%=yfsGetTemplateRowOptions(tempBinding+"/@TerSIRepairListDiscount", allowedBinding,"OTHERS","combo")%> >
				<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<input type="text" <%=yfsGetTemplateRowOptions(tempBinding+"/@TerSIAdditionalDiscount", allowedBinding,"OTHERS","text")%> />		
		</td>
		<td class="tablecolumn" nowrap="true">
			<select <%=yfsGetTemplateRowOptions(tempBinding+"/@TerRepPartsCovered", allowedBinding,"OTHERS","combo")%> class="combobox"  >
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" <%=yfsGetTemplateRowOptions(tempBinding+"/@TerNonRepPartsCovered", allowedBinding,"OTHERS","combo")%> >
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" />
			</select>
		</td>
    </tr>
    <%if (isModificationAllowedWithModType("OTHERS",allowedBinding)) { %> 
    <tr>
    	<td nowrap="true" colspan="10">
    		<jsp:include page="/common/editabletbl.jsp" flush="true" />
    	</td>
	
    </tr>
	<%}%>
</tfoot>    
</table>

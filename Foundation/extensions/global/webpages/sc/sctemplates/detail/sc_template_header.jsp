<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript">
<%
	String sctKey = resolveValue("xml:TerSerContTemplate/@TerSCTKey");
	//String sctName = resolveValue("xml:TerSerContTemplate/@TerSCTName");
	if (!isVoid(sctKey)) {
		//session.setAttribute("sctName",sctName);
		YFCDocument sctDoc = YFCDocument.createDocument("TerSerContTemplate");
		YFCElement sctEle = sctDoc.getDocumentElement();
		sctEle.setAttribute("TerSCTKey",sctKey);
%>
		function showDetail() {
			showDetailFor('<%=sctDoc.getDocumentElement().getString(false)%>');
		}
		window.attachEvent("onload", showDetail);
	<%
	}
%>
</script>

<table class="view" width="100%"  >
	<yfc:callAPI apiID="AP1" />
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_ID</yfc:i18n></td>
        <td>
			<input type="text" size="15" maxLength="30" class="unprotectedinput" id="serviceTemplateID" <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTID")%>/>
			<input type="hidden" class="unprotectedinput" value="Y" <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTDraft")%>/>
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_Name</yfc:i18n></td>
        <td>
			<input type="text" size="40" maxLength="50" class="unprotectedinput" <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTName")%>/>
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Types</yfc:i18n></td>
		<td>
			<select id="serviceType" name="xml:/TerSerContTemplate/@TerSerContType" class="combobox" >
				<yfc:loopOptions binding="xml:/CommonCodeList/@CommonCode" 
					name="CodeShortDescription" value="CodeValue" selected="xml:/TerSerContTemplate/@TerSerContType"/>
			</select>
        </td>
	</tr>
</table>

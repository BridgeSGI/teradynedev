<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="bundlecomponents.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>

<%	if(equals("Y",resolveValue("xml:/OrderLineDetail/@IsBundleParent"))) {
	
		YFCElement OrderInput = YFCDocument.parse("<Order/>").getDocumentElement();		
		OrderInput.setAttribute("OrderHeaderKey",resolveValue("xml:/OrderLineDetail/@OrderHeaderKey"));
		YFCElement orderTemplate = YFCDocument.parse("<Order OrderHeaderKey=\"\" DraftOrderFlag=\"\" ><OrderLines><OrderLine PrimeLineNo=\"\" OrderedQty=\"\" OrderLineKey=\"\" KitCode=\"\" MultipleStatusesExist=\"\" MaxLineStatusDesc=\"\" ><Item ItemID=\"\" UnitOfMeasure=\"\"></Item><Extn CoveredProductFamily=\"\" CoveredInstallBaseStatus=\"\" NonrepairPartsCovered=\"\"  RepairablePartsCovered=\"\" STORepairListDiscount=\"\" STOExpediteListDiscount=\"\" STOAdditionalDiscount=\"\"  ServiceCapQty=\"\" ServiceCapUOM=\"\" Percent_Cap_Used=\"\" ServiceStartDateTime=\"\" /><BundleParentLine OrderLineKey=\"\" /><LineOverallTotals LineTotal=\"\" /><AllowedModifications /><LinePriceInfo DiscountPercentage=\"\" UnitPrice=\"\" IsLinePriceForInformationOnly=\"\" /></OrderLine></OrderLines></Order>").getDocumentElement();
		
		%>		
			 <yfc:callAPI apiName="getOrderDetails" inputElement="<%=OrderInput%>"  templateElement="<%=orderTemplate%>" outputNamespace="Order" /> 
		<% 
		YFCElement OrderDoc = (YFCElement)request.getAttribute("Order");
		//System.out.println("orderTemplate :" + OrderDoc);
		rearrangeBundleComponents(OrderDoc);
		request.setAttribute("Order",OrderDoc);
		} 
		String draftFlag = resolveValue("xml:Order:/Order/@DraftOrderFlag");
		
%>
<yfc:callAPI apiID="AP1" />
<yfc:callAPI apiID="AP2" />
<yfc:callAPI apiID="AP3" />
<yfc:callAPI apiID="AP4" />
<table class="table" width="100%">
<thead>
<tr>
	<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Line</yfc:i18n></td>
    <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemID")%>"><yfc:i18n>TD_SI_ItemID</yfc:i18n></td>
	<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemShortDesc")%>"><yfc:i18n>Description</yfc:i18n></td>
	<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/@OrderedQty")%>"><yfc:i18n>TD_SC_Qty</yfc:i18n></td>
	<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Item/@TransactionalUOM")%>"><yfc:i18n>TD_SC_UOM</yfc:i18n></td>
	<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/LinePriceInfo/@UnitPrice")%>"><yfc:i18n>TD_SC_SUP</yfc:i18n></td>
    <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/LinePriceInfo/@DiscountPercentage")%>" ><yfc:i18n>TD_SC_SLD</yfc:i18n></td>
    <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/LineOverallTotals/@LineTotal")%>"><yfc:i18n>TD_SC_SLA</yfc:i18n></td>
	<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@CoveredProductFamily")%>"><yfc:i18n>TD_SC_CPF</yfc:i18n></td>
    <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@CoveredInstallBaseStatus")%>"><yfc:i18n>TD_SC_IBS</yfc:i18n></td>
    <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@NonrepairPartsCovered")%>"><yfc:i18n>TD_SCT_NRPC</yfc:i18n></td>
	<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@RepairablePartsCovered")%>"><yfc:i18n>TD_SCT_RPC</yfc:i18n></td>
	<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STORepairListDiscount")%>"><yfc:i18n>TD_SCT_RLD</yfc:i18n></td>
	<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOExpediteListDiscount")%>"><yfc:i18n>TD_SCT_SED</yfc:i18n></td>
	<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>"><yfc:i18n>TD_SCT_AD</yfc:i18n></td>
    <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@ServiceCapQty")%>"><yfc:i18n>TD_SC_SCQ</yfc:i18n></td>
    <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@ServiceCapUOM")%>"><yfc:i18n>TD_SC_SCU</yfc:i18n></td>
	<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Percent_Cap_Used")%>"><yfc:i18n>TD_SC_SCP</yfc:i18n></td>
	<td class="tablecolumnheader" nowrap="true"><yfc:i18n>TD_SC_SSD</yfc:i18n></td>
	<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@MaxLineStatusDesc")%>"><yfc:i18n>TD_SC_Line_Status</yfc:i18n></td></tr>
</thead>
<tbody>
	<input type="hidden" name="xml:/Order/@OrderHeaderKey" value="<%=resolveValue("xml:/Order/@OrderHeaderKey")%>"  />
	<input type="hidden" name="xml:/Order/@ModificationReasonCode"/>
	<input type="hidden" name="xml:/Order/@ModificationReasonText"/>
	<input type="hidden" name="xml:/Order/@Override" value="N"/>
	<input type="hidden" name="hiddenDraftOrderFlag" value='<%=draftFlag%>' />
			
    <yfc:loopXML name="Order" binding="xml:Order:/Order/OrderLines/@OrderLine" id="OrderLine">
	<%String prefixBinding = "xml:/Order/OrderLines/OrderLine_"+OrderLineCounter;%>
		<yfc:makeXMLInput name="orderLineKey">
            <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
            <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/OrderLine/@OrderHeaderKey"/>
        </yfc:makeXMLInput>
    <tr>
		<td class="tablecolumn" sortValue="<%=getNumericValue("xml:/OrderLine/@PrimeLineNo")%>">
			<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/>
			<input type="hidden" <%=getTextOptions(prefixBinding+ "/@OrderLineKey", "xml:/OrderLine/@OrderLineKey")%> />
			<%String oldLinePriceFlag = resolveValue(prefixBinding+"/LinePriceInfo/@IsLinePriceForInformationOnly");
			if(!equals(getValue("OrderLine","xml:/OrderLine/LinePriceInfo/@IsLinePriceForInformationOnly"),"N")){%>
				<input type="hidden" <%=getTextOptions(prefixBinding+ "/LinePriceInfo/@IsLinePriceForInformationOnly", "N")%> />
			<%}%>
		</td>
        <td nowrap="true" class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID" /></td>
		<td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemShortDesc"/></td>
		<td class="numerictablecolumn">
		    <input type="text"  OldValue="<%=resolveValue("xml:/OrderLine/@OrderedQty")%>" <%=yfsGetTextOptions(prefixBinding+ "/@OrderedQty", "xml:/OrderLine/@OrderedQty", "xml:/OrderLine/AllowedModifications")%> style='width:40px' title='<%=getI18N("Open_Qty")%>: <%=getValue("OrderLine", "xml:/OrderLine/@OpenQty")%>'/>
		</td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/@UnitOfMeasure"/></td>
					
		<td class="tablecolumn">
			<input type="text"  OldValue="<%=resolveValue("xml:/OrderLine/LinePriceInfo/@UnitPrice")%>"  <%=yfsGetTextOptions(prefixBinding+ "/LinePriceInfo/@UnitPrice", "xml:/OrderLine/LinePriceInfo/@UnitPrice", "xml:/OrderLine/AllowedModifications")%> />
		</td>
		<td class="tablecolumn">
			<input type="text"  OldValue="<%=resolveValue("xml:/OrderLine/LinePriceInfo/@DiscountPercentage")%>"  
			<%=yfsGetTextOptions(prefixBinding+ "/LinePriceInfo/@DiscountPercentage", "xml:/OrderLine/LinePriceInfo/@DiscountPercentage", "xml:/OrderLine/AllowedModifications")%> />
		</td>
		<td class="tablecolumn"><yfc:getXMLValue binding="xml:/CurrencyList/Currency/@PrefixSymbol"/>&nbsp;
			<yfc:getXMLValue binding="xml:/OrderLine/LineOverallTotals/@LineTotal"/>&nbsp;
			<yfc:getXMLValue binding="xml:/CurrencyList/Currency/@PostfixSymbol"/>
		</td>
		<td class="tablecolumn" nowrap="true">
			<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@CoveredProductFamily")%>"  <%=yfsGetTextOptions(prefixBinding+ "/Extn/@CoveredProductFamily", "xml:/OrderLine/Extn/@CoveredProductFamily", "xml:/OrderLine/AllowedModifications")%> />
		</td>
		<td class="tablecolumn" nowrap="true">
			<select <%=yfsGetComboOptions(prefixBinding+"/Extn/@CoveredInstallBaseStatus","xml:/OrderLine/Extn/@CoveredInstallBaseStatus", "xml:/OrderLine/AllowedModifications")%> class="combobox"  >
				<yfc:loopOptions binding="xml:IBStatus:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/Extn/@CoveredInstallBaseStatus"  />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@NonrepairPartsCovered")%>" <%=yfsGetComboOptions(prefixBinding+"/Extn/@NonrepairPartsCovered","xml:/OrderLine/Extn/@NonrepairPartsCovered", "xml:/OrderLine/AllowedModifications")%> class="combobox"  >
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@NonrepairPartsCovered"  />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@RepairablePartsCovered")%>" <%=yfsGetComboOptions(prefixBinding+"/Extn/@RepairablePartsCovered","xml:/OrderLine/Extn/@RepairablePartsCovered", "xml:/OrderLine/AllowedModifications")%> 
			class="combobox"  >
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@RepairablePartsCovered"  />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" <%=yfsGetComboOptions(prefixBinding+"/Extn/@STOExpediteListDiscount","xml:/OrderLine/Extn/@STOExpediteListDiscount", "xml:/OrderLine/AllowedModifications")%> >
				<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue"  selected="xml:/OrderLine/Extn/@STOExpediteListDiscount" />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" <%=yfsGetComboOptions(prefixBinding+"/Extn/@STORepairListDiscount","xml:/OrderLine/Extn/@STORepairListDiscount", "xml:/OrderLine/AllowedModifications")%> >
				<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/Extn/@STORepairListDiscount"  />
			</select>
		</td>
		<td class="tablecolumn" nowrap="true">
			<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>" <%=yfsGetTextOptions(prefixBinding+"/Extn/@STOAdditionalDiscount","xml:/OrderLine/Extn/@STOAdditionalDiscount","xml:/OrderLine/AllowedModifications")%> />
		</td>
		<td class="tablecolumn" nowrap="true">
			<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ServiceCapQty")%>" <%=yfsGetTextOptions(prefixBinding+"/Extn/@ServiceCapQty","xml:/OrderLine/Extn/@ServiceCapQty","xml:/OrderLine/AllowedModifications")%> />
		</td>
		<td class="tablecolumn">
			<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ServiceCapUOM"/>
		</td>
		<td class="tablecolumn" nowrap="true">
			<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@Percent_Cap_Used")%>"   <%=yfsGetTextOptions(prefixBinding+"/Extn/@Percent_Cap_Used","xml:/OrderLine/Extn/@Percent_Cap_Used","xml:/OrderLine/AllowedModifications")%> />
		</td>
		<td class="tablecolumn" nowrap="true">
			<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ServiceStartDateTime_YFCDATE")%>" <%=yfsGetTextOptions(prefixBinding+ "/Extn/@ServiceStartDateTime_YFCDATE", "xml:/OrderLine/Extn/@ServiceStartDateTime_YFCDATE", "xml:/OrderLine/AllowedModifications")%>/>
			<img class="lookupicon" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar", "xml:/OrderLine/Extn/@ServiceStartDateTime", "xml:/OrderLine/AllowedModifications")%>/>
			<input type="text"  OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ServiceStartDateTime_YFCTIME")%>" <%=yfsGetTextOptions(prefixBinding+ "/Extn/@ServiceStartDateTime_YFCTIME", "xml:/OrderLine/Extn/@ServiceStartDateTime_YFCTIME", "xml:/OrderLine/AllowedModifications")%>/>
			<img class="lookupicon" onclick="invokeTimeLookup(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "TimeLookup", "xml:/OrderLine/Extn/@ServiceStartDateTime", "xml:/OrderLine/AllowedModifications")%>/>
		</td>
		<td class="tablecolumn">
			<%=displayOrderStatus(getValue("OrderLine","xml:/OrderLine/@MultipleStatusesExist"),getValue("OrderLine","xml:/OrderLine/@MaxLineStatusDesc"),true)%>
		</td>
    </tr>
</yfc:loopXML>
</tbody>
</table>

<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/css/scripts/editabletbl.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>

<script language="javascript">
    document.body.attachEvent("onunload", processSaveRecordsForChildNode);
</script>

<%
	appendBundleRootParent((YFCElement)request.getAttribute("Order"));
	boolean bAppendOldValue = false;
	if(!isVoid(errors) || equals(sOperation,"Y") || equals(sOperation,"DELETE")) 
		bAppendOldValue = true;
	//String modifyView = request.getParameter("ModifyView");
    //modifyView = modifyView == null ? "" : modifyView;
    //System.out.println("Order Output::"+request.getAttribute("Order").toString());
    String driverDate = getValue("Order", "xml:/Order/@DriverDate");
	String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("Order", "xml:/Order/@EnterpriseCode"));
%>

<table class="table" ID="OrderLines" cellspacing="0" width="100%" yfcMaxSortingRecords="1000" >
    <thead>
        <tr>
            <td class="checkboxheader" sortable="no">
                <input type="hidden" id="userOperation" name="userOperation" value="" />
                <input type="hidden" id="numRowsToAdd" name="numRowsToAdd" value="" />
				<input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader" nowrap="true" style="width:30px">&nbsp;</td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Line</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemID")%>"><yfc:i18n>TD_SI_ItemID</yfc:i18n></td>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemDesc")%>"><yfc:i18n>Description</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/@OrderedQty")%>"><yfc:i18n>TD_SC_Qty</yfc:i18n></td>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM")%>"><yfc:i18n>TD_SC_UOM</yfc:i18n></td>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/LinePriceInfo/@UnitPrice")%>"><yfc:i18n>TD_SC_SUP</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/LinePriceInfo/@DiscountPercentage")%>" ><yfc:i18n>TD_SC_SLD</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/LineOverallTotals/@ExtendedPrice")%>"><yfc:i18n>TD_SC_SLA</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@CoveredProductFamily")%>"><yfc:i18n>TD_SC_CPF</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@CoveredInstallBaseStatus")%>"><yfc:i18n>TD_SC_IBS</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@NonrepairPartsCovered")%>"><yfc:i18n>TD_SCT_NRPC</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@RepairablePartsCovered")%>"><yfc:i18n>TD_SCT_RPC</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STORepairListDiscount")%>"><yfc:i18n>TD_SCT_RLD</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOExpediteListDiscount")%>"><yfc:i18n>TD_SCT_SED</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>"><yfc:i18n>TD_SCT_AD</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@ServiceCapQty")%>"><yfc:i18n>TD_SC_SCQ</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@ServiceCapUOM")%>"><yfc:i18n>TD_SC_SCU</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Percent_Cap_Used")%>"><yfc:i18n>TD_SC_SCP</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"><yfc:i18n>TD_SC_SSD</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@MaxLineStatusDesc")%>"><yfc:i18n>TD_SC_Line_Status</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
	
        <yfc:loopXML name="Order" binding="xml:/Order/OrderLines/@OrderLine" id="OrderLine">
        	<%	
			//Set variables to indicate the orderlines dependency situation.
			String prefixBinding="xml:/Order/OrderLines/OrderLine_"+OrderLineCounter;
			String orderModAllowed="xml:/Order/AllowedModifications";
			String ordLineModAllowed="xml:/OrderLine/AllowedModifications";
			
			if(!isVoid(resolveValue("xml:OrderLine:/OrderLine/@Status"))) {  
				if (equals(getValue("OrderLine","xml:/OrderLine/@ItemGroupCode"),"PROD") &&  isVoid(resolveValue("xml:OrderLine:/OrderLine/BundleParentLine/@OrderLineKey")))
					//display line in this inner panel only if item has ItemGroupCode = PROD and is not a bundle component
				{
					if(bAppendOldValue) {
						String sOrderLineKey = resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey");
						if(oMap.containsKey(sOrderLineKey))
							request.setAttribute("OrigAPIOrderLine",(YFCElement)oMap.get(sOrderLineKey));
				} else 
						request.setAttribute("OrigAPIOrderLine",(YFCElement)pageContext.getAttribute("OrderLine"));
			%>
				<tr>
					<yfc:makeXMLInput name="orderLineKey">
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@IsBundleParent" value="xml:/OrderLine/@IsBundleParent"/>
					</yfc:makeXMLInput>
					<yfc:makeXMLInput name="bundleRootParentKey">
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@BundleRootParentKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@IsBundleParent" value="xml:/OrderLine/@IsBundleParent"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OriginalLineItemClicked" value="xml:/OrderLine/@PrimeLineNo"/>
					</yfc:makeXMLInput>
					<td class="checkboxcolumn" >
						<input type="checkbox" value='<%=getParameter("orderLineKey")%>' name="chkEntityKey" />
						<%/*This hidden input is required by yfc to match up each line attribute that is editable in this row against the appropriate order line # on the server side once you save.  */%>
						<input type="hidden" name='OrderLineKey_<%=OrderLineCounter%>' value='<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>' />
						<input type="hidden" name='OrderHeaderKey_<%=OrderLineCounter%>' value='<%=resolveValue("xml:/Order/@OrderHeaderKey")%>' />
                        <input type="hidden" <%=getTextOptions(prefixBinding+ "/@OrderLineKey", "xml:/OrderLine/@OrderLineKey")%> />
						<input type="hidden" <%=getTextOptions(prefixBinding+ "/@PrimeLineNo", "xml:/OrderLine/@PrimeLineNo")%> />
						<input type="hidden" <%=getTextOptions(prefixBinding+ "/@SubLineNo", "xml:/OrderLine/@SubLineNo")%> />
					</td>
					<td class="tablecolumn" nowrap="true">
						<% if (equals(getValue("OrderLine","xml:/OrderLine/@IsBundleParent"),"Y")) { %>
							<a <%=getDetailHrefOptions("L02", getParameter("bundleRootParentKey"), "")%>>
								<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Bundle_Components")%> />	
							</a>
						<%}%>
					</td>
					<td class="tablecolumn" sortValue="<%=getNumericValue("xml:OrderLine:/OrderLine/@PrimeLineNo")%>">
						<% if(showOrderLineNo("Order","Order")) {%>
							<a <%=getDetailHrefOptions("L01", getParameter("orderLineKey"), "")%>>
								<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/></a>
						<%} else {%>
							<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/>
						<%}%>
					</td>
					<td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID"/></td>
					<td class="tablecolumn"><yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemShortDesc"/></td>
					<td class="numerictablecolumn">
					    <input type="text"  <%if(bAppendOldValue) { %> OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/OrderLineTranQuantity/@OrderedQty")%>"  <%}%> <%=yfsGetTextOptions(prefixBinding+ "/OrderLineTranQuantity/@OrderedQty", "xml:/OrderLine/OrderLineTranQuantity/@OrderedQty", "xml:/OrderLine/AllowedModifications")%> style='width:40px' title='<%=getI18N("Open_Qty")%>: <%=getValue("OrderLine", "xml:/OrderLine/OrderLineTranQuantity/@OpenQty")%>'/>
					</td>
                    <td class="tablecolumn">
                        <yfc:getXMLValue binding="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM"/>
                    </td>
					
					<td class="tablecolumn">
						<input type="text"  <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/LinePriceInfo/@UnitPrice")%>"  <%}%> <%=yfsGetTextOptions(prefixBinding+ "/LinePriceInfo/@UnitPrice", "xml:/OrderLine/LinePriceInfo/@UnitPrice", "xml:/OrderLine/AllowedModifications")%> />
					</td>
					<td class="tablecolumn">
						<input type="text"  <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/LinePriceInfo/@DiscountPercentage")%>"  <%}%> <%=yfsGetTextOptions(prefixBinding+ "/LinePriceInfo/@DiscountPercentage", "xml:/OrderLine/LinePriceInfo/@DiscountPercentage", "xml:/OrderLine/AllowedModifications")%> />
					</td>
					<td class="tablecolumn"><yfc:getXMLValue binding="xml:/CurrencyList/Currency/@PrefixSymbol"/>&nbsp;
						<yfc:getXMLValue binding="xml:/OrderLine/LineOverallTotals/@LineTotal"/>&nbsp;
						<yfc:getXMLValue binding="xml:/CurrencyList/Currency/@PostfixSymbol"/>
					</td>
					<td class="tablecolumn" nowrap="true">
						<input type="text"  <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@CoveredProductFamily")%>"  <%}%> <%=yfsGetTextOptions(prefixBinding+ "/Extn/@CoveredProductFamily", "xml:/OrderLine/Extn/@CoveredProductFamily", "xml:/OrderLine/AllowedModifications")%> />
					</td>
					<td class="tablecolumn" nowrap="true">
						<select <%=yfsGetComboOptions(prefixBinding+"/Extn/@CoveredInstallBaseStatus","xml:/OrderLine/Extn/@CoveredInstallBaseStatus", "xml:/OrderLine/AllowedModifications")%> class="combobox"  >
							<yfc:loopOptions binding="xml:IBStatus:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/Extn/@CoveredInstallBaseStatus"  />
						</select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@NonrepairPartsCovered")%>"<%}%> <%=yfsGetComboOptions(prefixBinding+"/Extn/@NonrepairPartsCovered","xml:/OrderLine/Extn/@NonrepairPartsCovered", "xml:/OrderLine/AllowedModifications")%> class="combobox"  >
							<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@NonrepairPartsCovered"  />
						</select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@RepairablePartsCovered")%>"<%}%> <%=yfsGetComboOptions(prefixBinding+"/Extn/@RepairablePartsCovered","xml:/OrderLine/Extn/@RepairablePartsCovered", "xml:/OrderLine/AllowedModifications")%> class="combobox"  >
							<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@RepairablePartsCovered"  />
						</select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<select class="combobox" <%=yfsGetComboOptions(prefixBinding+"/Extn/@STOExpediteListDiscount","xml:/OrderLine/Extn/@STOExpediteListDiscount", "xml:/OrderLine/AllowedModifications")%> >
							<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue"  selected="xml:/OrderLine/Extn/@STOExpediteListDiscount" />
						</select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<select class="combobox" <%=yfsGetComboOptions(prefixBinding+"/Extn/@STORepairListDiscount","xml:/OrderLine/Extn/@STORepairListDiscount", "xml:/OrderLine/AllowedModifications")%> >
							<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/Extn/@STORepairListDiscount"  />
						</select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<input type="text" <%if(bAppendOldValue){%>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@STOAdditionalDiscount")%>"  <%}%> <%=yfsGetTextOptions(prefixBinding+"/Extn/@STOAdditionalDiscount","xml:/OrderLine/Extn/@STOAdditionalDiscount","xml:/OrderLine/AllowedModifications")%> />
					</td>
					
					<td class="tablecolumn" nowrap="true">
						<input type="text" <%if(bAppendOldValue){%>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@ServiceCapQty")%>"  <%}%> <%=yfsGetTextOptions(prefixBinding+"/Extn/@ServiceCapQty","xml:/OrderLine/Extn/@ServiceCapQty","xml:/OrderLine/AllowedModifications")%> />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ServiceCapUOM"/>
		            </td>
					<td class="tablecolumn" nowrap="true">
						<input type="text" <%if(bAppendOldValue){%>OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@Percent_Cap_Used")%>"  <%}%> <%=yfsGetTextOptions(prefixBinding+"/Extn/@Percent_Cap_Used","xml:/OrderLine/Extn/@Percent_Cap_Used","xml:/OrderLine/AllowedModifications")%> />
					</td>
					<td class="tablecolumn" nowrap="true">
						<input type="text"  <%if(bAppendOldValue) { %> OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@ServiceStartDateTime_YFCDATE")%>"  <%}%> <%=yfsGetTextOptions(prefixBinding+ "/Extn/@ServiceStartDateTime_YFCDATE", "xml:/OrderLine/Extn/@ServiceStartDateTime_YFCDATE", "xml:/OrderLine/AllowedModifications")%>/>
						<img class="lookupicon" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar", "xml:/OrderLine/Extn/@ServiceStartDateTime", "xml:/OrderLine/AllowedModifications")%>/>
						<input type="text"  <%if(bAppendOldValue) { %> OldValue="<%=resolveValue("xml:OrigAPIOrderLine:/OrderLine/Extn/@ServiceStartDateTime_YFCTIME")%>"  <%}%> <%=yfsGetTextOptions(prefixBinding+ "/Extn/@ServiceStartDateTime_YFCTIME", "xml:/OrderLine/Extn/@ServiceStartDateTime_YFCTIME", "xml:/OrderLine/AllowedModifications")%>/>
						<img class="lookupicon" onclick="invokeTimeLookup(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "TimeLookup", "xml:/OrderLine/Extn/@ServiceStartDateTime", "xml:/OrderLine/AllowedModifications")%>/>
						
					</td>
					<td class="tablecolumn">
						<a <%=getDetailHrefOptions("L04", getParameter("orderLineKey"),"ShowReleaseNo=Y")%>><%=displayOrderStatus(getValue("OrderLine","xml:/OrderLine/@MultipleStatusesExist"),getValue("OrderLine","xml:/OrderLine/@MaxLineStatusDesc"),true)%></a>
					</td>
				</tr>
                <%}
            } else if(isVoid(resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey"))) {%>
					<tr DeleteRowIndex="<%=OrderLineCounter%>">
						<td class="checkboxcolumn"> 
							<img class="icon" onclick="setDeleteOperationForRow(this,'xml:/Order/OrderLines/OrderLine')" <%=getImageOptions(YFSUIBackendConsts.DELETE_ICON, "Remove_Row")%>/>
					</td>
                    <td class="tablecolumn">
						<input type="hidden" <%=getTextOptions(prefixBinding+"/@Action", prefixBinding+"/@Action", "CREATE")%> />
						<input type="hidden"  <%=getTextOptions(prefixBinding+"/@DeleteRow",  "")%> />
						<input type="hidden"  <%=yfsGetTemplateRowOptions(prefixBinding+"/Item/@KitCode",prefixBinding+"/Item/@KitCode", "ADD_LINE","text")%> />
                    </td>
                    <td class="tablecolumn">&nbsp;</td>
                    <td class="tablecolumn" nowrap="true">
						<input type="text" <%=yfsGetTemplateRowOptions(prefixBinding+"/Item/@ItemID","xml:/OrderLine/Item/@ItemID",orderModAllowed,"ADD_LINE","text")%> />		
						<img class="lookupicon" onclick="tempRowServiceItemLookup(this, 'ItemID','','','KitCode','TransactionalUOM','Seritem','xml:/Item/@CallingOrganizationCode=CSO&xml:/Item/PrimaryInformation/@ItemType=SERVICE')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
					</td>
                    <td class="tablecolumn">&nbsp;
		            </td>
					<td class="tablecolumn" nowrap="true">
						<input type="text" <%=yfsGetTemplateRowOptions(prefixBinding+"/@OrderedQty","xml:/OrderLine/@OrderedQty",orderModAllowed,"ADD_LINE","text")%> />		
					</td>
                    <td class="tablecolumn">
		                <select <%=yfsGetTemplateRowOptions(prefixBinding+"/OrderLineTranQuantity/@TransactionalUOM","xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM",orderModAllowed,"ADD_LINE","combo")%>>
                           <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
		                    value="UnitOfMeasure" selected="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM"/>
                        </select>
                    </td>
                    <td class="tablecolumn" nowrap="true">
						<input type="text" <%=yfsGetTemplateRowOptions(prefixBinding+"/LinePriceInfo/@UnitPrice","xml:/OrderLine/LinePriceInfo/@UnitPrice",orderModAllowed,"ADD_LINE","text")%> />		
					</td>
                    <td class="tablecolumn" nowrap="true">
						<input type="text" <%=yfsGetTemplateRowOptions(prefixBinding+"/LinePriceInfo/@DiscountPercentage","xml:/OrderLine/LinePriceInfo/@DiscountPercentage",orderModAllowed,"ADD_LINE","text")%> />		
					</td>
                    <td class="tablecolumn">&nbsp;</td>
                    <td class="tablecolumn" nowrap="true">
						<input type="text" <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@CoveredProductFamily","xml:/OrderLine/Extn/@CoveredProductFamily",orderModAllowed,"ADD_LINE","text")%> />
                    </td>
					<td class="tablecolumn" nowrap="true">
						<select class="combobox" <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@CoveredInstallBaseStatus","xml:/OrderLine/Extn/@CoveredInstallBaseStatus", orderModAllowed,"ADD_LINE","combo")%> >
							<yfc:loopOptions binding="xml:IBStatus:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/Extn/@CoveredInstallBaseStatus" 
							targetBinding="<%=prefixBinding+"/Extn/@CoveredInstallBaseStatus"%>" />
						</select>
                        
                    </td>
					<td class="tablecolumn" nowrap="true">
						<select class="combobox" <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@NonrepairPartsCovered","xml:/OrderLine/Extn/@NonrepairPartsCovered", orderModAllowed,"ADD_LINE","combo")%> >
							<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@NonrepairPartsCovered" 
							targetBinding="<%=prefixBinding+"/Extn/@NonrepairPartsCovered"%>" />
						</select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<select class="combobox" <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@RepairablePartsCovered",
						"xml:/OrderLine/Extn/@RepairablePartsCovered", orderModAllowed,"ADD_LINE","combo")%>  >
							<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@RepairablePartsCovered" 
							targetBinding="<%=prefixBinding+"/Extn/@RepairablePartsCovered"%>" />
						</select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<select class="combobox" <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@STOExpediteListDiscount","xml:/OrderLine/Extn/@STOExpediteListDiscount", orderModAllowed,"ADD_LINE","combo")%> >
							<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue"  selected="xml:/OrderLine/Extn/@STOExpediteListDiscount" 
							targetBinding="<%=prefixBinding+"/Extn/@STOExpediteListDiscount"%>"/>
						</select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<select class="combobox" <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@STORepairListDiscount","xml:/OrderLine/Extn/@STORepairListDiscount", orderModAllowed,"ADD_LINE","combo")%> >
							<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/Extn/@STORepairListDiscount" 
							targetBinding="<%=prefixBinding+"/Extn/@STORepairListDiscount"%>" />
						</select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<input type="text"  <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@STOAdditionalDiscount","xml:/OrderLine/Extn/@STOAdditionalDiscount",orderModAllowed,"ADD_LINE","text")%> />
					</td>
					<td class="tablecolumn" nowrap="true">
						<input type="text"  <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@ServiceCapQty",
						"xml:/OrderLine/Extn/@ServiceCapQty",orderModAllowed,"ADD_LINE","text")%> />
					</td>
					<td class="tablecolumn">
		                <select <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@ServiceCapUOM",
						"xml:/OrderLine/Extn/@ServiceCapUOM",orderModAllowed,"ADD_LINE","combo")%>>
                           <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
		                    value="UnitOfMeasure" selected="xml:/OrderLine/Extn/@ServiceCapUOM"/>
                        </select>
                    </td>
					<td class="tablecolumn" nowrap="true">&nbsp;</td>
					<td class="tablecolumn" nowrap="true">
                       	<input class="dateinput" type="text"  <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@ServiceStartDateTime_YFCDATE","xml:/OrderLine/Extn/@ServiceStartDateTime_YFCDATE",orderModAllowed,"ADD_LINE","text")%>/>
						<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
                         <input class="dateinput" type="text"  <%=yfsGetTemplateRowOptions(prefixBinding+"/Extn/@ServiceStartDateTime_YFCTIME","xml:/OrderLine/Extn/@ServiceStartDateTime_YFCTIME",orderModAllowed,"ADD_LINE","text")%>/>
						 <img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
                    </td>             
                    <td class="tablecolumn">&nbsp;</td>
                </tr>
            <%}%>
        </yfc:loopXML>
	</tbody>
    <tfoot>        
		<% if(isModificationAllowed("xml:/@AddLine","xml:/Order/AllowedModifications")) { %>
        <tr>
        	<td nowrap="true" colspan="22">
        		<jsp:include page="/common/editabletbl.jsp" flush="true">
					<jsp:param name="ReloadOnAddLine" value="Y"/>
        		</jsp:include>
        	</td>
        </tr>
        <%}%>
    </tfoot>
</table>

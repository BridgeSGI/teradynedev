<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>

<table class="table" width="100%"  >
	<thead>
		<tr>
			<td class="tablecolumnheader"  width="5%"><yfc:i18n>Line</yfc:i18n></td>
            <td class="tablecolumnheader" width="15%" nowrap="true"><yfc:i18n>STO/STOG Item ID</yfc:i18n></td>
			<td class="tablecolumnheader" width="30%" sortable="no"  ><yfc:i18n>Description</yfc:i18n></td>
			<td class="tablecolumnheader" width="10%" sortable="no"><yfc:i18n>TD_SC_CPF</yfc:i18n></td>
            <td class="tablecolumnheader" width="10%" sortable="no" nowrap="true"><yfc:i18n>TD_SC_IBS</yfc:i18n></td>
            <td class="tablecolumnheader" sortable="no" width="5%"><yfc:i18n>TD_SCT_NRPC</yfc:i18n></td>
			<td class="tablecolumnheader" sortable="no" width="5%"><yfc:i18n>TD_SCT_RPC</yfc:i18n></td>
			<td class="tablecolumnheader" sortable="no" width="5%"><yfc:i18n>TD_SCT_RLD</yfc:i18n></td>
			<td class="tablecolumnheader" sortable="no" width="5%"><yfc:i18n>TD_SCT_SED</yfc:i18n></td>
			<td class="tablecolumnheader" width="10%" sortable="no"><yfc:i18n>TD_SCT_AD</yfc:i18n></td>
	</tr>
	</thead>
	<tbody>
		<tr>
			<yfc:makeXMLInput name="orderLineKey">
				<yfc:makeXMLKey binding="xml:/TerSCIBMapping/@TerSCOLKey" value="xml:/OrderLineDetail/@OrderLineKey" />
				<yfc:makeXMLKey binding="xml:/TerSCIBMapping/@TerSCOHKey" value="xml:/OrderLineDetail/@OrderHeaderKey" />
			</yfc:makeXMLInput>
			<input type="hidden" name="xml:/TerSCIBMapping/@TerSCOLKey" value="<%=resolveValue("xml:/OrderLineDetail/@OrderLineKey")%>"/>
			<input type="hidden" name="xml:/TerSCIBMapping/@TerSCOHKey" value="<%=resolveValue("xml:/OrderLineDetail/@OrderHeaderKey")%>"/>
			<td class="tablecolumn">
				<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID" />
			</td>
			<td class="tablecolumn" >
				<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemDesc" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@CoveredProductFamily" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:hasXMLNode binding="xml:/OrderLine/Extn/@CoveredInstallBaseStatus" >
					<yfc:getXMLValue binding="xml:IBStatus:/CommonCodeList/CommonCode/@CodeShortDescription" />
				</yfc:hasXMLNode>
			</td>
			<td class="tablecolumn" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@NonrepairPartsCovered" />
			</td>
			<td class="tablecolumn"  >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@RepairablePartsCovered" />
			</td>
			<td class="tablecolumn"  >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@STORepairListDiscount" />
			</td>
			<td class="tablecolumn" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@STOExpediteListDiscount" />
			</td>
			<td class="tablecolumn" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@STOAdditionalDiscount" />
			</td>
		</tr>
	</tbody>
</table>

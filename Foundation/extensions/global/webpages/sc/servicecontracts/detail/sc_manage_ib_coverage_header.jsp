<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>

<%if(resolveValue("xml:/OrderLine/Order/@BuyerOrganizationCode")!=""){%>
<yfc:callAPI apiID="AP2" />
<%}
if(resolveValue("xml:/OrderLine/Order/@SellerOrganizationCode")!=""){ %>
<yfc:callAPI apiID="AP1" />
<%}%>
<table class="view" width="100%"  >
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Types</yfc:i18n></td>
        <td class="protectedtext">
			<yfc:getXMLValue binding="xml:/OrderLine/Order/@OrderType" />
		</td>
		<td class="detaillabel"><yfc:i18n>Seller</yfc:i18n></td>
        <td class="protectedtext">
			<yfc:getXMLValue binding="xml:/OrderLine/Order/@SellerOrganizationCode" />
		</td>
		<td class="detaillabel"><yfc:i18n>Seller_Name</yfc:i18n></td>
		<td class="protectedtext">
			<yfc:hasXMLNode binding="xml:/OrderLine/Order/@SellerOrganizationCode" >
				<yfc:getXMLValue binding="xml:SellerOrg:/OrganizationList/Organization/@OrganizationName" />
			</yfc:hasXMLNode>
		</td>
		<td class="detaillabel" ><yfc:i18n>Line Status</yfc:i18n></td>
        <td class="protectedtext">
            <% if (isVoid(getValue("OrderLine", "xml:/OrderLine/@Status"))) {%>
                [<yfc:i18n>Draft</yfc:i18n>]
            <% } else { %>
                <%=displayOrderStatus(getValue("OrderLine","xml:/OrderLine/@MultipleStatusesExist"),getValue("OrderLine","xml:/OrderLine/@MaxLineStatusDesc"),true)%>
            <% } %>
        </td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_ContractNo</yfc:i18n></td>
        <td class="protectedtext">
			<yfc:getXMLValue binding="xml:/OrderLine/Order/@OrderNo" />
        </td>
		<td class="detaillabel"><yfc:i18n>Buyer</yfc:i18n></td>
        <td class="protectedtext">
			<yfc:getXMLValue binding="xml:/OrderLine/Order/@BuyerOrganizationCode" />
		</td>
		<td class="detaillabel"><yfc:i18n>Buyer_Name</yfc:i18n></td>
		<td class="protectedtext">
			<yfc:hasXMLNode binding="xml:/OrderLine/Order/@BuyerOrganizationCode" >
				<yfc:getXMLValue binding="xml:BuyerOrg:/OrganizationList/Organization/@OrganizationName" />
			</yfc:hasXMLNode>
		</td>
	</tr>
	
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<yfc:callAPI apiID="AP1" />
<table class="table" width="100%"  >
	<thead>
		<tr>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@OrderedQty")%>"><yfc:i18n>Selling Unit Qty</yfc:i18n></td>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM")%>"><yfc:i18n>Selling UOM</yfc:i18n></td>
			
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/LinePriceInfo/@UnitPrice")%>"><yfc:i18n>TD_SC_SUP</yfc:i18n></td>
            <td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/LinePriceInfo/@DiscountPercentage")%>" ><yfc:i18n>TD_SC_SLD</yfc:i18n></td>
            <td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/LineOverallTotals/@ExtendedPrice")%>"><yfc:i18n>TD_SC_SLA</yfc:i18n></td>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@ServiceCapQty")%>"><yfc:i18n>TD_SC_SCQ</yfc:i18n></td>
            <td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@ServiceCapUOM")%>"><yfc:i18n>TD_SC_SCU</yfc:i18n></td>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@Percent_Cap_Used")%>"><yfc:i18n>TD_SC_SCP</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_SC_SSD</yfc:i18n></td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="tablecolumn" nowrap="true" >
				<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/@OrderedQty")%>" <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/@OrderedQty", "xml:/OrderLine/@OrderedQty", "xml:/OrderLine/AllowedModifications")%> title='<%=getI18N("Open_Qty")%>: 
				<%=getValue("OrderLine", "xml:/OrderLine/OrderLineTranQuantity/@OpenQty")%>'/>
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/LinePriceInfo/@UnitPrice")%>" <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/LinePriceInfo/@UnitPrice","xml:/OrderLine/LinePriceInfo/@UnitPrice","xml:/OrderLine/AllowedModifications")%> />		
			</td>
			<td class="tablecolumn" nowrap="true" >
				<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/LinePriceInfo/@DiscountPercentage")%>" <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/LinePriceInfo/@DiscountPercentage","xml:/OrderLine/LinePriceInfo/@DiscountPercentage","xml:/OrderLine/AllowedModifications")%> />		
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/LineOverallTotals/@ExtendedPrice" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ServiceCapQty")%>" <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@ServiceCapQty","xml:/OrderLine/Extn/@ServiceCapQty","xml:/OrderLine/AllowedModifications")%> />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ServiceCapUOM")%>" <%=yfsGetComboOptions("xml:/Order/OrderLines/OrderLine/Extn/@ServiceCapUOM","xml:/OrderLine/Extn/@ServiceCapUOM","xml:/OrderLine/AllowedModifications")%> >
                    <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
		            value="UnitOfMeasure" selected="xml:/OrderLine/Extn/@ServiceCapUOM"/>
                </select>
			</td>
			<td class="tablecolumn" nowrap="true" >
				<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@Percent_Cap_Used")%>" <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@Percent_Cap_Used","xml:/OrderLine/Extn/@Percent_Cap_Used","xml:/OrderLine/AllowedModifications")%> />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<input class="dateinput" type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ServiceStartDateTime_YFCDATE")%>" <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@ServiceStartDateTime_YFCDATE","xml:/OrderLine/Extn/@ServiceStartDateTime_YFCDATE","xml:/OrderLine/AllowedModifications")%>/>
				<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar","xml:/OrderLine/Extn/@ServiceStartDateTime", "xml:/OrderLine/AllowedModifications") %> />
                <input class="dateinput" type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ServiceStartDateTime_YFCTIME")%>" <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@ServiceStartDateTime_YFCTIME","xml:/OrderLine/Extn/@ServiceStartDateTime_YFCTIME","xml:/OrderLine/AllowedModifications")%>/>
				<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup","xml:/OrderLine/Extn/@ServiceStartDateTime", "xml:/OrderLine/AllowedModifications") %>/>
            </td>
		</tr>
	</tbody>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<table class="anchor" cellpadding="7px"  cellSpacing="0">
<% YFCElement orderElem = (YFCElement) request.getAttribute("Order");
 boolean isHistory = YFCCommon.equals(orderElem.getAttribute("isHistory"),"Y");
 String isNotHistoryVal = isHistory ? "N" : "Y";
 orderElem.setAttribute("isNotHistory", isNotHistoryVal);
 
 boolean activeFlag = YFCCommon.equals(orderElem.getAttribute("Status"),"1100.200");
 String isConfirmed =  YFCCommon.equals(orderElem.getAttribute("DraftOrderFlag"),"N") ? "Y" : "N" ;
 String isActive = activeFlag ? "Y" : "N";
 String isNotActive = !activeFlag ? "Y" : "N";
 orderElem.setAttribute("IsActive", isActive);
 orderElem.setAttribute("IsNotActive", isNotActive);
 orderElem.setAttribute("IsConfirmed", isConfirmed);
%>
<tr>
	<td>
		<yfc:makeXMLInput name="orderHeaderKey">
            <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
        </yfc:makeXMLInput>
        <input type="hidden" value='<%=getParameter("orderHeaderKey")%>' name="OrderEntityKey"/>
	</td>
</tr>
<tr>
    <td colspan="5">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I01"/>
        </jsp:include>
    </td>
</tr>
<tr>
   <td height="100%" width="15%" addressip="true">
         <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I07"/>
            <jsp:param name="Path" value="xml:/Order/PersonInfoShipTo"/>
            <jsp:param name="DataXML" value="Order"/>
            <jsp:param name="AllowedModValue" value='<%=getModificationAllowedValueWithPermission("ShipToAddress", "xml:/Order/AllowedModifications")%>'/>
        </jsp:include>
    </td>
    <td height="100%" width="15%" addressip="true">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I06"/>
            <jsp:param name="Path" value="xml:/Order/PersonInfoBillTo"/>
            <jsp:param name="DataXML" value="Order"/>
            <jsp:param name="AllowedModValue" value='<%=getModificationAllowedValueWithPermission("BillToAddress", "xml:/Order/AllowedModifications")%>'/>
        </jsp:include>
    </td>
	<td height="100%" width="15%" >
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I04"/>
        </jsp:include>
    </td>
	<td height="100%" width="15%">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I03"/>
        </jsp:include>
    </td>
	<td height="100%" width="30%">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I02"/>
        </jsp:include>
    </td>
</tr>
<tr>
    <td colspan="5">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I05"/>
        </jsp:include>
    </td>
</tr>
</table>
	
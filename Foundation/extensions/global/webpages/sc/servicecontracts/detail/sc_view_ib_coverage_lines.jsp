<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<table class="table" width="100%"  >
	<thead>
		<tr>
			<td class="checkboxheader" sortable="no" style="width:30px">
				<input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>
			</td>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSSCOrderLine/@PrimeLineNo")%>"><yfc:i18n>SC Line#</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSSCOrderLine/Item/@ItemID")%>">
			<yfc:i18n>STO/STOG Item ID</yfc:i18n>
			</td>
			<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSOrderLine/Extn/@SystemSerialNo")%>"><yfc:i18n>Serial #</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSOrderLine/@Status")%>">
				<yfc:i18n>IB Status</yfc:i18n>
			</td>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSOrderLine/ItemDetails/PrimaryInformation/@ItemType")%>">
				<yfc:i18n>Item_Type</yfc:i18n>
			</td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSOrderLine/ItemDetails/Extn/@SystemProductFamily")%>">
				<yfc:i18n>Product Family</yfc:i18n>
			</td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSOrderLine/ItemDetails/@ItemID")%>">
				<yfc:i18n>Item_ID</yfc:i18n>
			</td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" >
				<yfc:i18n>Description</yfc:i18n>
			</td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSOrderLine/@ReceivingNode")%>">
				<yfc:i18n>Install Node</yfc:i18n>
			</td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSOrderLine/Extn/@TesterOwnerOrgID")%>">
				<yfc:i18n>Tester Owner Org</yfc:i18n>
			</td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSOrderLine/Order/Extn/@TdShipDate")%>">
				<yfc:i18n>Ship Date</yfc:i18n>
			</td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/TerSCIBMappingList/TerSCIBMapping/YFSOrderLine/Extn/@ActualInsDate")%>">
				<yfc:i18n>Actual Install Date</yfc:i18n>
			</td>
	</tr>
	</thead>
	<tbody>
		<yfc:loopXML name="TerSCIBMappingList" binding="xml:/TerSCIBMappingList/@TerSCIBMapping" id="TerSCIBMapping">
		<tr>
			<yfc:makeXMLInput name="orderLineKey">
				<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/TerSCIBMapping/@TerIBOLKey"/>
				<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/TerSCIBMapping/@TerIBOHKey"/>
			</yfc:makeXMLInput>
			<td class="checkboxcolumn" >
				<input type="checkbox" value='<%=getParameter("orderLineKey")%>' name="chkEntityKey"  />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/@PrimeLineNo" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSSCOrderLine/Item/@ItemID" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<a <%=getDetailHrefOptions("L01", getParameter("orderLineKey"), "")%> >
					<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSOrderLine/Extn/@SystemSerialNo" />
				</a>
			</td>
			<td class="tablecolumn" nowrap="true" >
				<%=displayOrderStatus(getValue("TerSCIBMapping","xml:/TerSCIBMapping/YFSOrderLine/@MultipleStatusesExist"),getValue("TerSCIBMapping","xml:/TerSCIBMapping/YFSOrderLine/@MaxLineStatusDesc"),true)%>
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSOrderLine/ItemDetails/PrimaryInformation/@ItemType" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSOrderLine/ItemDetails/Extn/@SystemProductFamily" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSOrderLine/ItemDetails/@ItemID" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSOrderLine/ItemDetails/PrimaryInformation/@Description" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSOrderLine/@ReceivingNode" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSOrderLine/Extn/@TesterOwnerOrgID" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSOrderLine/Order/Extn/@TdShipDate" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/TerSCIBMapping/YFSOrderLine/Extn/@ActualInsDate" />
			</td>
		</tr>
		</yfc:loopXML>
	</tbody>
</table>

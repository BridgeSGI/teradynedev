<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>

<table class="view" width="100%">
	<tr>
		<td>
			<yfc:i18n>TD_SC_EffDate</yfc:i18n>
		</td>
        <td nowrap="true">
		<%String status = resolveValue("xml:/Order/@Status");
		if(YFCCommon.equals(status,"1000") || YFCCommon.equals(status,"1000.100") || YFCCommon.equals(status,"1100.100")){ %>
			<input class="dateinput" type="text" <%=yfsGetTextOptions("xml:/Order/Extn/@EffectiveDate_YFCDATE","xml:/Order/AllowedModifications")%> <%=resolveValue("xml:/Order/Extn/@EffectiveDate_YFCDATE")%> />
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar", "xml:/Order/Extn/@EffectiveDate_YFCDATE", "xml:/Order/AllowedModifications")%> />
			<input class="dateinput" type="text" <%=yfsGetTextOptions("xml:/Order/Extn/@EffectiveDate_YFCTIME", "xml:/Order/AllowedModifications")%> <%=resolveValue("xml:/Order/Extn/@EffectiveDate_YFCTIME")%> />
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup", "xml:/Order/Extn/@EffectiveDate_YFCTIME", "xml:/Order/AllowedModifications") %>/>
		<% }else{%>
			<input class="dateinput" readonly type="text" <%=yfsGetTextOptions("xml:/Order/Extn/@EffectiveDate_YFCDATE","xml:/Order/AllowedModifications")%> <%=resolveValue("xml:/Order/Extn/@EffectiveDate_YFCDATE")%> />
			&nbsp;
			<input class="dateinput" readonly type="text" <%=yfsGetTextOptions("xml:/Order/Extn/@EffectiveDate_YFCTIME", "xml:/Order/AllowedModifications")%> <%=resolveValue("xml:/Order/Extn/@EffectiveDate_YFCTIME")%> />
		<%}%>
		</td>
	</tr>
	<tr>
		<td>
			<yfc:i18n>TD_SC_ExpDate</yfc:i18n>
		</td>
        <td nowrap="true">
			<input class="dateinput" type="text" 
			<%=yfsGetTextOptions("xml:/Order/Extn/@ExpirationDate_YFCDATE","xml:/Order/AllowedModifications")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar","xml:/Order/Extn/@EffectiveDate_YFCDATE","xml:/Order/AllowedModifications") %> />
			<input class="dateinput" type="text" 
			<%=yfsGetTextOptions("xml:/Order/Extn/@ExpirationDate_YFCTIME","xml:/Order/AllowedModifications")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup", "xml:/Order/Extn/@ExpirationDate_YFCTIME","xml:/Order/AllowedModifications") %>/>
		</td>
	</tr>
	<tr>
        <td>
            <yfc:i18n>Reason_Code</yfc:i18n>
        </td>
        <td>
            <select name="xml:/Order/@ModificationReasonCode" class="combobox">
                <yfc:loopOptions binding="xml:ReasonCodeList:/CommonCodeList/@CommonCode" 
                    name="CodeShortDescription" value="CodeValue" isLocalized="Y" selected="xml:/Order/@ModificationReasonCode"/>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <yfc:i18n>Reason_Text</yfc:i18n>
        </td>
        <td>
            <textarea class="unprotectedtextareainput" rows="3" cols="50" <%=getTextAreaOptions("xml:/Order/@ModificationReasonText")%>></textarea>
			<input type="hidden" name="xml:/Order/@Override" value="N"/>
        </td>
    </tr>
</table>
	
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<%if(resolveValue("xml:/Order/@BuyerOrganizationCode")!=""){%>
<yfc:callAPI apiID="AP3" />
<%}
if(resolveValue("xml:/Order/@SellerOrganizationCode")!=""){ %>
<yfc:callAPI apiID="AP2" />
<%}%>
<% String sHiddenDraftOrderFlag = getValue("Order", "xml:/Order/@DraftOrderFlag"); %>
<table class="view" width="100%"  >
	<yfc:makeXMLInput name="confirmOrderKey">
        <yfc:makeXMLKey binding="xml:/ConfirmDraftOrder/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
		<yfc:makeXMLKey binding="xml:/ConfirmDraftOrder/@EffectiveDate" value="xml:/Order/Extn/@EffectiveDate"/>
		<yfc:makeXMLKey binding="xml:/ConfirmDraftOrder/@ExpirationDate" value="xml:/Order/Extn/@ExpirationDate"/>
    </yfc:makeXMLInput>
	<yfc:makeXMLInput name="orderKey">
        <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
	</yfc:makeXMLInput>
	<tr>
		<td>
			<input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
			<input type="hidden" name="ConfirmOrderKey" value='<%=HTMLEncode.htmlEscape(getParameter("confirmOrderKey"))%>' />
			<input type="hidden" name="chkOrderEntityKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>' />
			<input type="hidden" name="hiddenDraftOrderFlag" value='<%=sHiddenDraftOrderFlag%>'/>
			<input type="hidden" name="xml:/Order/@EffectiveDate" value="<%=resolveValue("xml:/Order/Extn/@EffectiveDate")%>" />
			<input type="hidden" name="xml:/Order/@ModificationReasonCode" />
            <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
            <input type="hidden" name="xml:/Order/@Override" value="N"/>
        </td>
	</tr>
	<tr>
		<td class="detaillabel" >
			<yfc:i18n>Document_Type</yfc:i18n>
		</td>
		<td class="protectedtext">
			<yfc:getXMLValueI18NDB binding="xml:DocumentParamsList:/DocumentParamsList/DocumentParams/@Description"></yfc:getXMLValueI18NDB>
		</td>
        <td class="detaillabel" ><yfc:i18n>Enterprise</yfc:i18n></td>
        <td class="protectedtext"><yfc:getXMLValue binding="xml:/Order/@EnterpriseCode" /></td>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Status</yfc:i18n></td>
        <td class="protectedtext">
            <% if (isVoid(getValue("Order", "xml:/Order/@Status"))) {%>
                [<yfc:i18n>Draft</yfc:i18n>]
            <% } else { %>
                <%=displayOrderStatus(getValue("Order","xml:/Order/@MultipleStatusesExist"),getValue("Order","xml:/Order/@MaxOrderStatusDesc"),true)%>
            <% } %>
            <% if (equals("Y", getValue("Order", "xml:/Order/@HoldFlag"))) { %>

	            <% if (isTrue("xml:/Rules/@RuleSetValue")) {%>
					<img onmouseover="this.style.cursor='default'" class="columnicon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_is_held")%>>
				<%	}	else	{	%>
					<a <%=getDetailHrefOptions("L01", getParameter("orderKey"), "")%>><img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_is_held\nclick_to_add/remove_hold")%>></a>
				<%	}	%>

            <% } %>
            <% if (equals("Y", getValue("Order", "xml:/Order/@SaleVoided"))) { %>
                <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.SALE_VOIDED, "This_sale_is_voided")%>/>
            <% } %>
            <% if (equals("Y", getValue("Order","xml:/Order/@isHistory") )){ %>
                <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.HISTORY_ORDER, "This_is_an_archived_order")%>/>
            <% } %>
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SC_TemplateID</yfc:i18n></td>
        <td class="protectedtext">
			<yfc:getXMLValue binding="xml:/Order/Extn/@SCTemplateID" />
        </td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Types</yfc:i18n></td>
        <td class="protectedtext">
			<yfc:getXMLValue binding="xml:/Order/@OrderType" />
		</td>
		<td class="detaillabel"><yfc:i18n>Seller</yfc:i18n></td>
        <td class="protectedtext">
			<yfc:getXMLValue binding="xml:/Order/@SellerOrganizationCode" />
		</td>
		<td class="detaillabel"><yfc:i18n>TD_Seller_Name</yfc:i18n></td>
		<td class="protectedtext">
			<yfc:hasXMLNode binding="xml:/Order/@SellerOrganizationCode" >
				<yfc:getXMLValue binding="xml:SellerOrg:/OrganizationList/Organization/@OrganizationName" />
			</yfc:hasXMLNode>
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SC_Coordinator</yfc:i18n></td>
		<td nowrap="true">
			<input type="text" maxLength="40" size="30" class="unprotectedinput" <%=yfsGetTextOptions("xml:/Order/Extn/@ContractCoordinator","xml:/Order/AllowedModifications")%> />
		</td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_ContractNo</yfc:i18n></td>
        <td class="protectedtext">
			<yfc:getXMLValue binding="xml:/Order/@OrderNo" />
        </td>
		<td class="detaillabel"><yfc:i18n>Buyer</yfc:i18n></td>
        <td class="protectedtext">
			<yfc:getXMLValue binding="xml:/Order/@BuyerOrganizationCode" />
		</td>
		<td class="detaillabel"><yfc:i18n>TD_Buyer_Name</yfc:i18n></td>
		<td class="protectedtext">
			<yfc:hasXMLNode binding="xml:/Order/@BuyerOrganizationCode" >
				<yfc:getXMLValue binding="xml:BuyerOrg:/OrganizationList/Organization/@OrganizationName" />
			</yfc:hasXMLNode>
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SC_Manager</yfc:i18n></td>
		<td nowrap="true">
			<input type="text" maxLength="40" size="30" class="unprotectedinput" <%=yfsGetTextOptions("xml:/Order/Extn/@ContractManager","xml:/Order/AllowedModifications")%> />
		</td>
	</tr>
	<% boolean isDraft = YFCCommon.equals(resolveValue("xml:/Order/@DraftOrderFlag"),"Y"); %>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Priority</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput"  <%=yfsGetTextOptions("xml:/Order/@PriorityNumber","xml:/Order/AllowedModifications")%>/>
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SC_EffDate</yfc:i18n></td>
        <td nowrap="true">
			<%if(isDraft){%>
			<input class="dateinput" type="text" <%=yfsGetTextOptions("xml:/Order/Extn/@EffectiveDate_YFCDATE","xml:/Order/AllowedModifications")%> <%=resolveValue("xml:/Order/Extn/@EffectiveDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar", "xml:/Order/Extn/@EffectiveDate_YFCDATE", "xml:/Order/AllowedModifications")%> />
			<input class="dateinput" type="text" <%=yfsGetTextOptions("xml:/Order/Extn/@EffectiveDate_YFCTIME", "xml:/Order/AllowedModifications")%> <%=resolveValue("xml:/Order/Extn/@EffectiveDate_YFCTIME")%> />
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup", "xml:/Order/Extn/@EffectiveDate_YFCTIME", "xml:/Order/AllowedModifications") %>/>
			<%} else {%>
			<yfc:getXMLValue binding="xml:/Order/Extn/@EffectiveDate" />
			<%}%>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_SC_ExpDate</yfc:i18n></td>
        <td nowrap="true">
			<%if(isDraft){%>
			<input class="dateinput" type="text" 
			<%=yfsGetTextOptions("xml:/Order/Extn/@ExpirationDate_YFCDATE","xml:/Order/AllowedModifications")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar","xml:/Order/Extn/@EffectiveDate_YFCDATE","xml:/Order/AllowedModifications") %> />
			<input class="dateinput" type="text" 
			<%=yfsGetTextOptions("xml:/Order/Extn/@ExpirationDate_YFCTIME","xml:/Order/AllowedModifications")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup", "xml:/Order/Extn/@ExpirationDate_YFCTIME","xml:/Order/AllowedModifications") %>/>
			<%} else {%>
			<yfc:getXMLValue binding="xml:/Order/Extn/@ExpirationDate" />
			<%}%>
        </td>
	</tr>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<%
	String sRequestDOM = request.getParameter("getRequestDOM");
    preparePaymentStatusList(getValue("Order", "xml:/Order/@PaymentStatus"), (YFCElement) request.getAttribute("PaymentStatusList"));
	System.out.print("Order::"+request.getAttribute("Order"));
%>
<yfc:callAPI apiID="AP3" />
<yfc:callAPI apiID="AP4" />
<table class="view" width="100%"  >
	<tr>
		<td class="detaillabel" ><yfc:i18n>Payment_Status</yfc:i18n></td>
        <td>
			<select <% if (equals(sRequestDOM,"Y")) { %>OldValue="<%=resolveValue("xml:Order:/Order/@PaymentStatus")%>" <%}%> 
			<%=yfsGetComboOptions("xml:/Order/@PaymentStatus", "xml:/Order/AllowedModifications")%> >
                <yfc:loopOptions binding="xml:/PaymentStatusList/@PaymentStatus" name="DisplayDescription"
                value="CodeType" selected="xml:/Order/@PaymentStatus"/>
            </select>
        </td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>Payment_Terms</yfc:i18n></td>
        <td>
			<select <% if (equals(sRequestDOM,"Y")) { %>OldValue="<%=resolveValue("xml:Order:/Order/@TermsCode")%>" <%}%> 
			<%=yfsGetComboOptions("xml:/Order/@TermsCode", "xml:/Order/AllowedModifications")%> >
				<yfc:loopOptions binding="xml:PaymentTerms:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/Order/@TermsCode"/>
			</select>
        </td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_Payment_Frequency</yfc:i18n></td>
        <td>
			<select <% if (equals(sRequestDOM,"Y")) { %>OldValue="<%=resolveValue("xml:Order:/Order/Extn/@PaymentFrequency")%>" <%}%>
			<%=yfsGetComboOptions("xml:/Order/Extn/@PaymentFrequency", "xml:/Order/AllowedModifications")%> >
				<yfc:loopOptions binding="xml:PaymentFrequency:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/Order/Extn/@PaymentFrequency"/>
			</select>
        </td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>Payment_Type</yfc:i18n></td>
        <td nowrap="true">
			<select <% if (equals(sRequestDOM,"Y")) { %>OldValue="<%=resolveValue("xml:Order:/Order/PaymentMethods/PaymentMethod/@PaymentType")%>"
			<%}%><%=yfsGetComboOptions("xml:/Order/PaymentMethods/PaymentMethod/@PaymentType","xml:/Order/AllowedModifications")%> >
		        <yfc:loopOptions binding="xml:/PaymentTypeList/@PaymentType" name="PaymentTypeDescription" value="PaymentType" selected="xml:/Order/PaymentMethods/PaymentMethod/@PaymentType" />
	        </select>
        </td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_Payment_Instrument#</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" class="unprotectedinput" <% if (equals(sRequestDOM,"Y")) { %>OldValue="<%=resolveValue("xml:Order:/Order/PaymentMethods/PaymentMethod/@CustomerPONo")%>"<%}%>
			<%=yfsGetTextOptions("xml:/Order/PaymentMethods/PaymentMethod/@CustomerPONo","xml:/Order/AllowedModifications")%>/>
		</td>
	</tr>
</table>

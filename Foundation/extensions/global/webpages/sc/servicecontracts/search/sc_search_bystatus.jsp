<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@include file="/extn/jspf/servicecontract.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript">

function setOrderStateFlags(value1, value2){
	var oOrderComplete = document.all("xml:/Order/@OrderComplete");
	if (oOrderComplete != null)
		oOrderComplete.value = value1;
	var oReadFromHistory = document.all("xml:/Order/@ReadFromHistory");
	if (oReadFromHistory != null)
		oReadFromHistory.value = value2;
}
</script>

<%
	// Values for radiobinding
	// open : OrderComplete="N" ReadFromHistory="N" radiobinding="open"
	// recent : OrderComplete=" " ReadFromHistory="N" radiobinding="recent"
	// history : OrderComplete=" " ReadFromHistory="Y" radiobinding="history"
	// all : OrderComplete=" " ReadFromHistory="B" radiobinding="all"

	// default is open : OrderComplete="N" ReadFromHistory="N" radiobinding="open"

	String bReadFromHistory = resolveValue("xml:/Order/@ReadFromHistory");
	String sOrderComplete = resolveValue("xml:/Order/@OrderComplete");

	String radiobinding = "open";
	if(isVoid(sOrderComplete) && equals(bReadFromHistory, "N")){
		radiobinding = "recent";
	}
	else if(isVoid(sOrderComplete) && equals(bReadFromHistory, "Y")){
		radiobinding = "history";
	}
	else if(isVoid(sOrderComplete) && equals(bReadFromHistory, "B")){
		radiobinding = "all";
	}
	else{
		radiobinding = "open";
		YFCElement orderElem = (YFCElement)request.getAttribute("Order");
		if(orderElem == null){
			orderElem = YFCDocument.createDocument("Order").getDocumentElement();
		}
		orderElem.setAttribute("OrderComplete", "N");
		orderElem.setAttribute("ReadFromHistory", "N");
		bReadFromHistory = "N";
		sOrderComplete = "N";

	}
	YFCDate oEndDate = new YFCDate(); 
	oEndDate.setEndOfDay();
%>

<table class="view">
    <tr>
        <td>
            <input type="hidden" name="xml:/Order/@StatusQryType" value="BETWEEN"/>
			<input type="hidden" name="xml:/Order/Extn/@EffectiveDateQryType" value="BETWEEN"/>
			<input type="hidden" name="xml:/Order/Extn/@ExpirationDateQryType" value="BETWEEN"/>
            <input type="hidden" name="xml:/Order/@DocumentType" value="0018.ex"/>
			<input type="hidden" name="xml:/Order/@EnterpriseCode" value="CSO"/>
			<input type="hidden" name="xml:/Order/OrderHoldType/@Status" value=""/>
			<input type="hidden" name="xml:/Order/OrderHoldType/@StatusQryType" value="" />
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Document_Type</yfc:i18n>
        </td>
	</tr>
	<tr>
		<td class="searchcriteriacell">
			<input type="textbox" class="protectedinput" readonly value="Service Contracts" />
		</td>
	</tr>
	<tr>
		<td class="searchlabel" >
            <yfc:i18n>Enterprise_Code</yfc:i18n>
        </td>
	</tr>
	<tr>
		<td class="searchcriteriacell">
			<input type="textbox" class="protectedinput" readonly value="CSO" />
		</td>
	</tr>
	<% // Now call the APIs that are dependent on the common fields (Doc Type & Enterprise Code) %>
    <yfc:callAPI apiID="AP2"/>
	<yfc:callAPI apiID="AP4"/>
    <yfc:callAPI apiID="AP5"/>
	<yfc:callAPI apiID="AP7"/>
    <%
		if(!isTrue("xml:/Rules/@RuleSetValue") )	{
    %>
			<yfc:callAPI apiID="AP6"/>
    
	<%
			YFCElement listElement = (YFCElement)request.getAttribute("HoldTypeList");

			YFCDocument document = listElement.getOwnerDocument();
			YFCElement newElement = document.createElement("HoldType");

			newElement.setAttribute("HoldType", " ");
			newElement.setAttribute("HoldTypeDescription", getI18N("All_Held_Orders"));

			YFCElement eFirst = listElement.getFirstChildElement();
			if(eFirst != null)	{
				listElement.insertBefore(newElement, eFirst);
			}	else	{
				listElement.appendChild(newElement);
			}
			request.setAttribute("defaultHoldType", newElement);
		}

        //Remove Statuses 'Draft Order Created' and 'Held' from the Status Search Combobox.
		prepareSCStatusList((YFCElement) request.getAttribute("StatusList"));
    %>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_Types</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@OrderType" class="combobox" >
				<yfc:loopOptions binding="xml:SCTypes:/CommonCodeList/@CommonCode" 
					name="CodeShortDescription" value="CodeValue" selected="xml:/Order/@OrderType" />
			</select>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_ContractNo</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@OrderNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@OrderNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderNo")%>/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Buyer</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@BuyerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@BuyerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@BuyerOrganizationCode")%>/>
			<% String enterpriseCode = getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode");%>
            <img class="lookupicon" name="search" onclick="callLookupForOrder(this,'BUYER','<%=enterpriseCode%>','0018.ex')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Seller</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@SellerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@SellerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@SellerOrganizationCode")%>/>
            <img class="lookupicon" name="search" onclick="callLookupForOrder(this,'SELLER','<%=enterpriseCode%>','0018.ex')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization") %> />
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_BookYrWk</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/Extn/@BookYWQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/NumericQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/Extn/@BookYWQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@BookYW")%>/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_DivisionName</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/Extn/@DivisionNameQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/Extn/@DivisionNameQryType"/>
            </select>
            <input type="text" maxLength="30" size="20" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@DivisionName")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_Salesman</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/Extn/@SalesmanQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/Extn/@SalesmanQryType"/>
            </select>
            <input type="text" maxLength="45" size="20" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@Salesman")%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_EffDate</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@FromEffectiveDate_YFCDATE")%> />
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="hidden" <%=getTextOptions("xml:/Order/Extn/@FromEffectiveDate_YFCTIME","xml:/Order/Extn/@FromEffectiveDate_YFCTIME","")%>/>
			&nbsp;<yfc:i18n>To</yfc:i18n>&nbsp;
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@ToEffectiveDate_YFCDATE")%> />
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="hidden" <%=getTextOptions("xml:/Order/Extn/@ToEffectiveDate_YFCTIME","xml:/Order/Extn/@ToEffectiveDate_YFCTIME",oEndDate.getString(getLocale().getTimeFormat()))%>/>
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_ExpDate</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@FromExpirationDate_YFCDATE")%> />
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="hidden" <%=getTextOptions("xml:/Order/Extn/@FromExpirationDate_YFCTIME","xml:/Order/Extn/@FromExpirationDate_YFCTIME","")%>/>
			&nbsp;<yfc:i18n>To</yfc:i18n>&nbsp;
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@ToExpirationDate_YFCDATE")%> />
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="hidden" <%=getTextOptions("xml:/Order/Extn/@ToExpirationDate_YFCTIME","xml:/Order/Extn/@ToExpirationDate_YFCTIME",oEndDate.getString(getLocale().getTimeFormat()))%>/>
		</td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_Line_Status</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <select name="xml:/Order/@FromStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
                value="Status" selected="xml:/Order/@FromStatus" isLocalized="Y"/>
            </select>
            <span class="searchlabel" ><yfc:i18n>To</yfc:i18n></span>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@ToStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
                value="Status" selected="xml:/Order/@ToStatus" isLocalized="Y"/>
            </select>
        </td>
    </tr>
    <%	if(isTrue("xml:/Rules/@RuleSetValue") )	{	%>
		<tr>
			<td class="searchcriteriacell">
				<input type="checkbox" onclick="manageHoldOpts(this)" <%=getCheckBoxOptions("xml:/Order/@HoldFlag", "xml:/Order/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' ><yfc:i18n>Held_Orders</yfc:i18n></input>
			</td>
		</tr>    
		<tr>
			<td class="searchlabel" >
				<yfc:i18n>Hold_Reason_Code</yfc:i18n>
			</td>
		</tr>
		<tr>
			<td class="searchcriteriacell">
				<select name="xml:/Order/@HoldReasonCode" class="combobox">
					<yfc:loopOptions binding="xml:HoldReasonCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue" selected="xml:/Order/@HoldReasonCode" isLocalized="Y"/>
				</select>
			</td>
		</tr>
	<%	}	else	{	%>
		<tr>
			<td class="searchcriteriacell">
				<input type="checkbox" <%=getCheckBoxOptions("xml:/Order/@HoldFlag", "xml:/Order/@HoldFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' onclick="manageHoldOpts(this)" ><yfc:i18n>Held Contracts With Hold Type</yfc:i18n></input>
			</td>
		</tr>
		<tr>
	        <td class="searchcriteriacell">
				<select resetName="<%=getI18N("All_Held_Orders")%>" onchange="resetObjName(this, 'xml:/Order/OrderHoldType/@HoldType')" name="xml:/Order/OrderHoldType/@HoldType" class="combobox" <%if(isTrue("xml:/Order/@HoldFlag") ) {%> ENABLED <%} else {%> disabled="true" <%}%> >
					<yfc:loopOptions binding="xml:/HoldTypeList/@HoldType" name="HoldTypeDescription" value="HoldType" suppressBlank="Y" selected="xml:/Order/OrderHoldType/@HoldType" isLocalized="Y"/>
				</select>

			</td>
		</tr>
	<%	}	%>

	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_Status</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell">
			<input type="radio" onclick="setOrderStateFlags('N','N')" <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "open")%>><yfc:i18n>Open</yfc:i18n> 
			<input type="radio" onclick="setOrderStateFlags(' ','N')"  <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "recent")%>><yfc:i18n>Recent</yfc:i18n><!-- The use of 'NO' is done intentionally, getOrderList API returns history orders only if ReadFromHistory =='Y'  -->
			<input type="radio" onclick="setOrderStateFlags(' ','Y')" <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "history")%>><yfc:i18n>History</yfc:i18n>
            <input type="radio" onclick="setOrderStateFlags(' ','B')" <%=getRadioOptions("xml:/Order/@Radiobinding", radiobinding, "all")%>><yfc:i18n>All</yfc:i18n>
            <input type="hidden" name="xml:/Order/@OrderComplete" value="<%=sOrderComplete%>"/>
            <input type="hidden" name="xml:/Order/@ReadFromHistory" value="<%=bReadFromHistory%>"/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel">
            <yfc:i18n>Selecting 'All' contracts may take a few minutes</yfc:i18n>
        </td>
    </tr>
</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<%
YFCElement root = (YFCElement)request.getAttribute("TerProductGroupList");
int countElem = countChildElements(root); %>

<script language="javascript">setRetrievedRecordCount(<%=countElem%>);</script>
<table class="table" border="0" cellspacing="0" width="100%">
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader" nowrap="true" >
		<yfc:i18n>TD_RESP_ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true" >
        <yfc:i18n>TD_RESP_Name</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true" >
            <yfc:i18n>TD_DIV_CODE</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="TerProductGroupList" binding="xml:/TerProductGroupList/@TerProductGroup" id="TerProductGroup"  keyName="TerProductGroupKey" > 
    <tr> 
		<td class="tablecolumn">
            <img class="icon" onclick="setPGLookupValue('<%=resolveValue("xml:TerProductGroup:/TerProductGroup/@TerResponsibleID")%>',
			'<%=resolveValue("xml:TerProductGroup:/TerProductGroup/@TerDivisionCode")%>')"  
			value="<%=resolveValue("xml:TerProductGroup:/TerProductGroup/@TerProductGroupKey")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
		</td>
        <td class="tablecolumn"><yfc:getXMLValue name="TerProductGroup" binding="xml:/TerProductGroup/@TerResponsibleID"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TerProductGroup" binding="xml:/TerProductGroup/@TerResponsibleName"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="TerProductGroup" binding="xml:/TerProductGroup/@TerDivisionCode"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>

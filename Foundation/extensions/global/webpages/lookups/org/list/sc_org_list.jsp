<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<%
    YFCElement root = (YFCElement)request.getAttribute("OrganizationList");
    int countElem = countChildElements(root);
%>
<script language="javascript">
    setRetrievedRecordCount(<%=countElem%>);
</script>

<table class="table" width="100%">
<thead>
    <tr> 
        <td class="lookupiconheader"><br /></td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/Organization/@OrganizationCode")%>">
            <yfc:i18n>Organization_Code</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/Organization/@OrganizationName")%>">
            <yfc:i18n>Organization_Name</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/Organization/CorporatePersonInfo/@City")%>">
            <yfc:i18n>City</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="OrganizationList" binding="xml:/OrganizationList/@Organization" id="Organization"> 
    <tr> 
        <td class="tablecolumn">
            <img class="icon" onClick="setSCOrgLookupValue('<%=resolveValue("xml:Organization:/Organization/@OrganizationCode")%>','<%=resolveValue("xml:Organization:/Organization/@OrganizationName")%>')"  value="<%=resolveValue("xml:/Organization/@OrganizationCode")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue name="Organization" binding="xml:/Organization/@OrganizationCode"/></td>
        <td class="tablecolumn"><yfc:getXMLValueI18NDB name="Organization" binding="xml:/Organization/@OrganizationName"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="Organization" binding="xml:/Organization/CorporatePersonInfo/@City"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
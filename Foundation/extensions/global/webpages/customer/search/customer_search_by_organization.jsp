<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript">
function setRole(v) {
	if(v) {
		document.getElementById('IsEnterprise').value='';
		document.getElementById('IsBuyer').value='';
		document.getElementById(v).value='Y';
	}
}

function setRoleComboBox() {
	var obj = window.dialogArguments;
	if (obj) {
		var ent = obj.setEnt;
		if (ent) {
			if (ent == "Y") {
				ent = "IsEnterprise";
				var options=document.getElementById('rolecombo').options;
				for (var i=0, n=options.length; i < n ; i++) {
					if (options[i].value==ent) {
						document.getElementById("rolecombo").selectedIndex = i;
						setRole(ent);
						break;
					}
				}
			}
		}
	}
}

function setExtraParams() {
	var obj = window.dialogArguments;
	if (obj) {
		if (obj.viewDetails) {
			createExtraParam('ViewDetails', obj.viewDetails);
		}
		if (obj.createCustomerOrg) {
			createExtraParam('CreateCustomerOrg', obj.createCustomerOrg);
		}
		if (obj.createCustomerSiteOrg) {
			createExtraParam('CreateCustomerSiteOrg', obj.createCustomerSiteOrg);
		}
		if (obj.viewAudits) {
			createExtraParam('ViewAudits', obj.viewAudits);
		}
		if (obj.addParticipantEnt) {
			createExtraParam('AddParticipantEnt', obj.addParticipantEnt);
		}
		if (obj.orgCode) {
			createExtraParam('CurrOrgCode', obj.orgCode);
		}
		if (obj.addChildOrg) {
			createExtraParam('AddChildOrg', obj.addChildOrg);
		}
	}
}

function createExtraParam(name, value) {
	var newElement = document.createElement("INPUT");
	newElement.type="hidden";
	newElement.name = "xml:/Organization/ExtraParams/@" + name;
	newElement.value = value;
	document.getElementById('searchtable').insertBefore(newElement);
}

window.attachEvent('onload', setRoleComboBox);
window.attachEvent('onload', setExtraParams);
</script>

<input type="hidden" id="IsEnterprise" name="xml:/Organization/@IsEnterprise" />
<input type="hidden" id="IsBuyer" name="xml:/Organization/@IsBuyer" />

<input type="hidden" name="xml:/Organization/@SearchCustomer" value="Y" />

<%
YFCDocument docXMLForOrgRoles = YFCDocument.parse("<OrganizationRolesForSearch><Role name=\"Enterprise\" value=\"IsEnterprise\"/><Role name=\"Buyer\" value=\"IsBuyer\"/></OrganizationRolesForSearch>");
YFCElement root = docXMLForOrgRoles.getDocumentElement();
YFCElement queryTypeList = getElement("QueryTypeList");
queryTypeList.importNode(root);
%>

<table class="view" width="100%" id="searchtable">
	<tr>
		<td class="searchlabel">
			<yfc:i18n>TD_Org_Role</yfc:i18n>
		</td>
	</tr>
		<td class="searchlabel">
			<select id="rolecombo" class="combobox" name="xml:/Role" onchange="setRole(this.value)">
				<yfc:loopOptions binding="xml:/QueryTypeList/OrganizationRolesForSearch/@Role" 
					name="name" value="value" selected="xml:/Role"/>
			</select>
		</td>
	</tr>
	<tr>
		<td class="searchlabel"><yfc:i18n>TD_Org_ID</yfc:i18n></td>
	</tr>
	<tr>
		<td>
			<select name="xml:/Organization/@OrganizationCodeQryType" class="combobox" >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/Organization/@OrganizationCodeQryType"/>
			</select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationCode")%>/>
		</td>
	</tr>
	<tr>
		<td class="searchlabel"><yfc:i18n>TD_Org_Name</yfc:i18n></td>
	</tr>
	<tr>
		<td>
			<select name="xml:/Organization/@OrganizationNameQryType" class="combobox" >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/Organization/@OrganizationNameQryType"/>
			</select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationName")%>/>
		</td>
	</tr>
	<tr>
		<td>
			<fieldset>
				<legend><yfc:i18n>TD_Org_Address</yfc:i18n></legend>
				<table class="view" height="100%" width="100%">
					<tr>
						<td class="searchcriteriacell" align="center">
							<input type="radio" <%=getRadioOptions("xml:/Organization/Temp/@AddressType", "xml:/Organization/Temp/@AddressType", "B" )%>><yfc:i18n>Bill_To</yfc:i18n></input>
							<input type="radio" <%=getRadioOptions("xml:/Organization/Temp/@AddressType", "xml:/Organization/Temp/@AddressType", "S" )%>><yfc:i18n>Ship_To</yfc:i18n></input>
						</td>
					</tr>
					<tr>
						<td class="searchlabel">
							<yfc:i18n>Address_Line_1</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td nowrap="true" class="searchcriteriacell">
							<select name="xml:/Organization/Temp/Address/@AddressLine1QryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
								value="QueryType" selected="xml:/Organization/Temp/Address/@AddressLine1QryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@AddressLine1")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel">
							<yfc:i18n>Address_Line_2</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@AddressLine2QryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@AddressLine2QryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@AddressLine2")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>City</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@CityQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@CityQryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@City")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>State</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@StateQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@StateQryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@State")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>Postal_Code</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@ZipCodeQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@ZipCodeQryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@ZipCode")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>Country</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@CountryQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@CountryQryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@Country")%>/>
						</td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>

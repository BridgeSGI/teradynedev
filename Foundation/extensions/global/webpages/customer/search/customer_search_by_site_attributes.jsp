<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript">

function showOfficeRecordLookupPopup(Value, entityname, extraParamsLookUpAttribute, extraParamOfficeTypeValue){
	var oObj = new Object();
	oObj.field1 = document.all(Value);
	oObj.returnOfficeNameFlag = true;
	var extraParams = "&LookupAttribute=" + extraParamsLookUpAttribute + "&OfficeTypeValue=" + extraParamOfficeTypeValue;
	yfcShowSearchPopupWithParams('TDofficeS030',' ',900,550,oObj,entityname, extraParams);
}

function settingDefault(){
	var obj = window.dialogArguments;
	if(obj != null && obj.lookup){
		var newEle1 = document.createElement("INPUT");
		newEle1.type="hidden";
		newEle1.name = "xml:/Organization/@IsNode";
		newEle1.value = "Y";
		var newEle2 = document.createElement("INPUT");
		newEle2.type="hidden";
		newEle2.name = "xml:/Organization/@IsBuyer";
		newEle2.value = "Y";
		var newEle2 = document.createElement("INPUT");
		newEle2.type="hidden";
		newEle2.name = "xml:/Organization/Extn/@CustomerSiteStatus";
		newEle2.value = "A";
		
		var parent = document.getElementById("searchtable");
		parent.insertBefore(newEle1);
		parent.insertBefore(newEle2);
		
		var siteStatus = document.getElementById('siteStatus');
		siteStatus.disabled = "disabled";
	}
}

function setExtraParams() {
	var obj = window.dialogArguments;
	if (obj) {
		if (obj.viewDetails) {
			createExtraParam('ViewDetails', obj.viewDetails);
		}
		if (obj.createCustomerOrg) {
			createExtraParam('CreateCustomerOrg', obj.createCustomerOrg);
		}
		if (obj.createCustomerSiteOrg) {
			createExtraParam('CreateCustomerSiteOrg', obj.createCustomerSiteOrg);
		}
		if (obj.viewAudits) {
			createExtraParam('ViewAudits', obj.viewAudits);
		}
		if (obj.addChildOrg) {
			createExtraParam('AddChildOrg', obj.addChildOrg);
		}
		if (obj.orgCode) {
			createExtraParam('CurrOrgCode', obj.orgCode);
		}
	}
}

function createExtraParam(name, value) {
	var newElement = document.createElement("INPUT");
	newElement.type="hidden";
	newElement.name = "xml:/Organization/ExtraParams/@" + name;
	newElement.value = value;
	document.getElementById('searchtable').insertBefore(newElement);
}

window.attachEvent('onload', settingDefault);
window.attachEvent('onload', setExtraParams);
</script>

<input type="hidden" name="xml:/Organization/@SearchCustomer" value="N" />

<table class="view" id="searchtable">
	<input type="hidden" value="Y" <%=getTextOptions("xml:/Organization/@IsNode")%>/>
	<tr>
		<td style="width:30%" class="searchlabel">
			<yfc:i18n>TD_Org_Role</yfc:i18n>
		</td>
	</tr>
	<tr>
		<td style="color:black">
			Node
		</td>
	</tr>
	<tr>
		<td class="searchlabel">
			<yfc:i18n>Node_Type</yfc:i18n>
		</td>
	</tr>
	<tr>
		<td style="color:black">
			Customer-Site
		</td>
	</tr>
	<tr>
		<td class="searchlabel"><yfc:i18n>TD_Org_ID</yfc:i18n></td>
	</tr>
	<tr>
		<td>
			<select name="xml:/Organization/@OrganizationCodeQryType" class="combobox" >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/Organization/@OrganizationCodeQryType"/>
			</select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationCode")%>/>
		</td>
	</tr>
	<tr>
		<td class="searchlabel"><yfc:i18n>TD_Org_Name</yfc:i18n></td>
	</tr>
	<tr>
		<td>
			<select name="xml:/Organization/@OrganizationNameQryType" class="combobox" >
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
					name="QueryTypeDesc" value="QueryType" selected="xml:/Organization/@OrganizationNameQryType"/>
			</select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationName")%>/>
		</td>
	</tr>
	<tr>
		<td>
			<fieldset>
				<legend><yfc:i18n>TD_Org_Address</yfc:i18n></legend>
				<table class="view" height="100%" width="100%">
					<tr>
						<td class="searchcriteriacell" align="center">
							<input type="radio" <%=getRadioOptions("xml:/Organization/Temp/@AddressType", "xml:/Organization/Temp/@AddressType", "B" )%>><yfc:i18n>Bill_To</yfc:i18n></input>
							<input type="radio" <%=getRadioOptions("xml:/Organization/Temp/@AddressType", "xml:/Organization/Temp/@AddressType", "S" )%>><yfc:i18n>Ship_To</yfc:i18n></input>
						</td>
					</tr>
					<tr>
						<td class="searchlabel">
							<yfc:i18n>Address_Line_1</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td nowrap="true" class="searchcriteriacell">
							<select name="xml:/Organization/Temp/Address/@AddressLine1QryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
								value="QueryType" selected="xml:/Organization/Temp/Address/@AddressLine1QryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@AddressLine1")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel">
							<yfc:i18n>Address_Line_2</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@AddressLine2QryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@AddressLine2QryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@AddressLine2")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>City</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@CityQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@CityQryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@City")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>State</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@StateQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@StateQryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@State")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>Postal_Code</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@ZipCodeQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@ZipCodeQryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@ZipCode")%>/>
						</td>
					</tr>
					<tr>
						<td class="searchlabel" >
							<yfc:i18n>Country</yfc:i18n>
						</td>
					</tr>
					<tr>
						<td>
							<select name="xml:/Organization/Temp/Address/@CountryQryType" class="combobox">
								<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
									value="QueryType" selected="xml:/Organization/Temp/Address/@CountryQryType"/>
							</select>
							<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Temp/Address/@Country")%>/>
						</td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
	<tr>
		<td class="searchlabel">
			<yfc:i18n>TD_Service_Location</yfc:i18n>
		</td>
	</tr>
	<tr>
		<td>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Organization/Extn/@TeradyneServiceOffice")%>/>
			<img class="lookupicon" onclick="showOfficeRecordLookupPopup('xml:/Organization/Extn/@TeradyneServiceOffice', 'TDoffice', 'TerServiceLocation', 'L')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_For_Service_Office")%>/>
		</td>
	</tr>
	<tr>
		<td class="searchlabel">
			<yfc:i18n>TD_Customer_Site_Type</yfc:i18n>
		</td>
	</tr>
	<tr>
		<td>
			<select <%=getComboOptions("xml:/Organization/Extn/@CustomerSiteType")%> class="combobox" >
				<yfc:loopOptions binding="xml:CUSTOMER_SITE_TYPES:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue" selected="xml:/Organization/Extn/@CustomerSiteType"/>
			</select>
		</td>
	</tr>
	<tr>
		<td class="searchlabel">
			<yfc:i18n>TD_Customer_Site_Status</yfc:i18n>
		</td>
	</tr>
	<tr>
		<td>
			<select id="siteStatus" name="xml:/Organization/Extn/@CustomerSiteStatus" class="combobox" >
				<yfc:loopOptions binding="xml:ORGANIZATION_STATUS:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue" selected="xml:/Organization/Extn/@CustomerSiteStatus"/>
			</select>
		</td>
	</tr>
	<tr>
		<td class="searchlabel">
			<yfc:i18n>TD_Credit_Hold</yfc:i18n>
		</td>
	</tr>
	<tr>
		<td>
			<select name="xml:/Organization/@IsCreditHold" class="combobox">
				<yfc:loopOptions binding="xml:CREDIT_HOLD:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue" selected="xml:/Organization/@IsCreditHold"/>
			</select>
		</td>
	</tr>
	<tr>
		<td class="searchlabel">
			<yfc:i18n>TD_Service_Hold</yfc:i18n>
		</td>
	</tr>
	<tr>
		<td>
			<select name="xml:/Organization/@IsServiceHold" class="combobox">
				<yfc:loopOptions binding="xml:SERVICE_HOLD:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue" selected="xml:/Organization/@IsServiceHold"/>
			</select>
		</td>
	</tr>
</table>
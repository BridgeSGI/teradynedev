<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<%@ page import="com.yantra.yfc.core.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>

<%
YFCElement organization = getElement("Organization");
YFCElement subOrganization = organization.getChildElement("SubOrganization");
if (subOrganization != null) {
	 YFCIterable<YFCElement> allSubOrganizations = subOrganization.getChildren("Organization");
	 if (allSubOrganizations != null) {
		 for (YFCElement subOrg : allSubOrganizations) {
			YFCElement orgRoleList = subOrg.getChildElement("OrgRoleList");
			if (orgRoleList != null) {
				YFCIterable<YFCElement> allOrgRoles = orgRoleList.getChildren("OrgRole");
				if (allOrgRoles != null) {
					StringBuilder abbrRoles = new StringBuilder();
					for (YFCElement orgRole : allOrgRoles) {
						String role = orgRole.getAttribute("RoleKey");
						if (role != null && role.trim().length()>0) {
							abbrRoles.append(role.charAt(0)).append(",");
						}
					}
					abbrRoles.deleteCharAt(abbrRoles.length() - 1);
					orgRoleList.setAttribute("Roles", abbrRoles.toString());
				}
			}
		}
	}
}
%>

<script language="javascript">
function uncheckSelectedRows() {
	doCheckAll(document.getElementById('chk'));
	return true;
}

function callCreateCustomerOrg() {
	var orgCode = "<%=resolveValue("xml:/Organization/@OrganizationCode")%>";
	yfcShowDetailPopupWithParams('TDCSOHeader','','1400','700','', '','%3CParentOrganization+CurrOrgCode%3D%22' + orgCode + '%22%2F%3E');
}

function loadChildOrgs() {
	var obj = new Object();
	obj.createCustomerOrg = "N";
	obj.createCustomerSiteOrg = "N";
	obj.viewDetails = "N";
	obj.viewAudits = "N";
	obj.addChildOrg = "Y";
	obj.orgCode = "<%=resolveValue("xml:/Organization/@OrganizationCode")%>";
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,'TDCustSiteOrg','');
}

function loadChildCustOrgs() {
	var obj = new Object();
	obj.createCustomerOrg = "N";
	obj.createCustomerSiteOrg = "N";
	obj.viewDetails = "N";
	obj.viewAudits = "N";
	obj.addChildOrg = "Y";
	obj.orgCode = "<%=resolveValue("xml:/Organization/@OrganizationCode")%>";
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,'TDCustOrg','');
}

function confirmRemove() {
	var reply = confirm("This operation will remove the selected child organization(s). Are you sure to proceed?");
	if (reply == true) {
		return true;
	} else {
		return false;
	}
}

</script>

<table class="table" editable="false" width="100%" cellspacing="0">
	<thead>
		<tr>
            <td sortable="no" class="checkboxheader">
                <input id='chk' type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
			<td class="tablecolumnheader">Org. ID</td>
			<td class="tablecolumnheader">Org. Name</td>
			<td class="tablecolumnheader">Roles</td>
		</tr>
	</thead>
	<tbody>
		<yfc:loopXML binding="xml:/Organization/SubOrganization/@Organization" id="child">
			<tr>
				<yfc:makeXMLInput name="OrganizationKey">
					<yfc:makeXMLKey binding="xml:/Organization/@OrganizationCode" value="xml:child:/Organization/@OrganizationCode" />
					<yfc:makeXMLKey binding="xml:/Organization/@CurrOrgCode" value="xml:/OrganizationList/@CurrOrgCode" />
                </yfc:makeXMLInput>
				<td class="checkboxcolumn" id="childList" style="display:block;">
					<input type="checkbox" value='<%=getParameter("OrganizationKey")%>' name="chkEntityKey"/>
				</td>
				<td class="tablecolumn"><yfc:getXMLValue name="child" binding="xml:/Organization/@OrganizationCode"/></td>
				<td class="tablecolumn"><yfc:getXMLValue name="child" binding="xml:/Organization/@OrganizationName"/></td>
				<td class="tablecolumn"><yfc:getXMLValue name="child" binding="xml:/Organization/OrgRoleList/@Roles"/></td>
			</tr>
		</yfc:loopXML>
	</tbody>
</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfc.ui.backend.util.HTMLEncode" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>

<%
    String xmlName = getParameter("DataXML");
	if (isVoid(xmlName)) {
        xmlName = "EntityDetail";
    }
    
    String outputPath = getParameter("OutputPath");
    if (isVoid(outputPath)) {
        outputPath = getParameter("Path");
    }
	outputPath = HTMLEncode.htmlEscape(outputPath);
	String allowedModValue = getParameter("AllowedModValue");
    if (isVoid(allowedModValue)) {
        allowedModValue = "false";
    }

    String addressTypePath = getParameter("AddressTypePath");
    if (!isVoid(addressTypePath)) {
        String outputAddressTypePath = getParameter("OutputAddressTypePath");
        if (isVoid(outputAddressTypePath)) {
            outputAddressTypePath = getParameter("AddressTypePath");
        }
        String addressType = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@AddressType");
        %>
        <input type="hidden" yName="<%=outputAddressTypePath%>@AddressType" class="protectedtext" contentEditable=false value="<%=addressType%>">
        <%
    }
    String displayPhoneNo = getParameter("DisplayPhoneNo");     
    String company = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@Company");
    String firstName = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@FirstName");
    String middleName = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@MiddleName");
    String lastName = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@LastName");
    String addressLine1 = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@AddressLine1");
	String addressLine2 = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@AddressLine2");
    String addressLine3 = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@AddressLine3");
    String addressLine4 = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@AddressLine4");
    String addressLine5 = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@AddressLine5");
    String addressLine6 = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@AddressLine6");

    String city = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@City");
    String state = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@State");
    String postalCode = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@ZipCode");
    String country = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@Country");
    String dayphone = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@DayPhone");
    String eveningphone = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@EveningPhone");
    String mobilephone = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@MobilePhone");
    String fax = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@DayFaxNo");
    String email = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@EMailID");
    
    String alternateEmailID = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@AlternateEmailID");
    String beeper = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@Beeper");
    String department = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@Department");
    String eveningFaxNo = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@EveningFaxNo");
    String jobTitle = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@JobTitle");
    String otherPhone = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@OtherPhone");
    String suffix = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@Suffix");
    String title = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@Title");

    String errortxt = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@ErrorTxt");
    String httpurl = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@HttpUrl");
    String personid = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@PersonID");
    String prefershipaddr = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@PreferredShipAddress");
    String usecount = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@UseCount");
    String verificationstatus = resolveValue("xml:/Output/OrganizationList/Organization/CorporatePersonInfo/@VerificationStatus");
	String style = getParameter("style");
	if(!isVoid(style) )	{
		style = "style='"+style+"'";
	}
	
	//Changes for populating the pop-up address box
	Map<String, String> m = new HashMap<String, String>();
	m.put("AddressLine1", addressLine1);
	m.put("AddressLine2", addressLine2);
	m.put("AddressLine3", addressLine3);
	m.put("AddressLine4", addressLine4);
	m.put("AddressLine5", addressLine5);
	m.put("AddressLine6", addressLine6);
	m.put("City", city);
	m.put("State", state);
	m.put("PostalCode", postalCode);
	m.put("Country", country);
	m.put("GivenName", firstName);
	m.put("MiddleName", middleName);
	m.put("Surname", lastName);
	m.put("Company", company);
	m.put("DayTimePhone", dayphone);
	m.put("EveningPhone", eveningphone);
	m.put("MobilePhone", mobilephone);
	m.put("Fax", fax);
	m.put("Email", email);
	//m.put("AddressType", addressType);
	m.put("AlternateEmailId", alternateEmailID);
	m.put("Beeper", beeper);
	m.put("Department", department);
	m.put("EveningFaxNo", eveningFaxNo);
	m.put("JobTitle", jobTitle);
	m.put("OtherPhone", otherPhone);
	m.put("Suffix", suffix);
	m.put("Title", title);
	m.put("ErrorTxt", errortxt);
	m.put("HttpUrl", httpurl);
	m.put("PersonID", personid);
	m.put("PreferredShipAddress", prefershipaddr);
	m.put("UseCount", usecount);
	m.put("VerificationStatus", verificationstatus);
		
	session.setAttribute("AddressMap", m);
%>
    <input type="hidden" yName="xml:/AddressTemp/@AllowedModValue" value="<%=HTMLEncode.htmlEscape(allowedModValue)%>"/>
    <input type="hidden" yName="<%=outputPath%>@Company" class="protectedtext" contentEditable=false value="<%=company%>">
    <input type="hidden" yName="<%=outputPath%>@FirstName" class="protectedtext" contentEditable=false value="<%=firstName%>">
    <input type="hidden" yName="<%=outputPath%>@MiddleName" class="protectedtext" contentEditable=false value="<%=middleName%>">
    <input type="hidden" yName="<%=outputPath%>@LastName" class="protectedtext" contentEditable=false value="<%=lastName%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine1" class="protectedtext" contentEditable=false value="<%=addressLine1%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine2" class="protectedtext" contentEditable=false value="<%=addressLine2%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine3" class="protectedtext" contentEditable=false value="<%=addressLine3%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine4" class="protectedtext" contentEditable=false value="<%=addressLine4%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine5" class="protectedtext" contentEditable=false value="<%=addressLine5%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine6" class="protectedtext" contentEditable=false value="<%=addressLine6%>">
    <input type="hidden" yName="<%=outputPath%>@City" class="protectedtext" contentEditable=false value="<%=city%>">
    <input type="hidden" yName="<%=outputPath%>@State" class="protectedtext" contentEditable=false value="<%=state%>">
    <input type="hidden" yName="<%=outputPath%>@ZipCode" class="protectedtext" contentEditable=false value="<%=postalCode%>">
    <input type="hidden" yName="<%=outputPath%>@Country" class="protectedtext" contentEditable=false value="<%=country%>">
    <input type="hidden" yName="<%=outputPath%>@DayPhone" class="protectedtext" contentEditable=false value="<%=dayphone%>">
    <input type="hidden" yName="<%=outputPath%>@EveningPhone" class="protectedtext" contentEditable=false value="<%=eveningphone%>">
    <input type="hidden" yName="<%=outputPath%>@MobilePhone" class="protectedtext" contentEditable=false value="<%=mobilephone%>">
    <input type="hidden" yName="<%=outputPath%>@DayFaxNo" class="protectedtext" contentEditable=false value="<%=fax%>">
    <input type="hidden" yName="<%=outputPath%>@EMailID" class="protectedtext" contentEditable=false value="<%=email%>">
    <input type="hidden" yName="<%=outputPath%>@AlternateEmailID" class="protectedtext" contentEditable=false value="<%=alternateEmailID%>">
    <input type="hidden" yName="<%=outputPath%>@Beeper" class="protectedtext" contentEditable=false value="<%=beeper%>">
    <input type="hidden" yName="<%=outputPath%>@Department" class="protectedtext" contentEditable=false value="<%=department%>">
    <input type="hidden" yName="<%=outputPath%>@EveningFaxNo" class="protectedtext" contentEditable=false value="<%=eveningFaxNo%>">
    <input type="hidden" yName="<%=outputPath%>@JobTitle" class="protectedtext" contentEditable=false value="<%=jobTitle%>">
    <input type="hidden" yName="<%=outputPath%>@OtherPhone" class="protectedtext" contentEditable=false value="<%=otherPhone%>">
    <input type="hidden" yName="<%=outputPath%>@Suffix" class="protectedtext" contentEditable=false value="<%=suffix%>">
    <input type="hidden" yName="<%=outputPath%>@Title" class="protectedtext" contentEditable=false value="<%=title%>">
    <input type="hidden" yName="<%=outputPath%>@ErrorTxt" class="protectedtext" contentEditable=false value="<%=errortxt%>">
    <input type="hidden" yName="<%=outputPath%>@HttpUrl" class="protectedtext" contentEditable=false value="<%=httpurl%>">
    <input type="hidden" yName="<%=outputPath%>@PersonID" class="protectedtext" contentEditable=false value="<%=personid%>">
    <input type="hidden" yName="<%=outputPath%>@PreferredShipAddress" class="protectedtext" contentEditable=false value="<%=prefershipaddr%>">
    <input type="hidden" yName="<%=outputPath%>@UseCount" class="protectedtext" contentEditable=false value="<%=usecount%>">
    <input type="hidden" yName="<%=outputPath%>@VerificationStatus" class="protectedtext" contentEditable=false value="<%=verificationstatus%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine1" class="protectedtext" contentEditable=false value="<%=addressLine1%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine2" class="protectedtext" contentEditable=false value="<%=addressLine2%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine3" class="protectedtext" contentEditable=false value="<%=addressLine3%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine4" class="protectedtext" contentEditable=false value="<%=addressLine4%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine5" class="protectedtext" contentEditable=false value="<%=addressLine5%>">
    <input type="hidden" yName="<%=outputPath%>@AddressLine6" class="protectedtext" contentEditable=false value="<%=addressLine6%>">
    <input type="hidden" yName="<%=outputPath%>@City" class="protectedtext" contentEditable=false value="<%=city%>">
    <input type="hidden" yName="<%=outputPath%>@State" class="protectedtext" contentEditable=false value="<%=state%>">
    <input type="hidden" yName="<%=outputPath%>@ZipCode" class="protectedtext" contentEditable=false value="<%=postalCode%>">
    <input type="hidden" yName="<%=outputPath%>@Country" class="protectedtext" contentEditable=false value="<%=country%>">
    <input type="hidden" yName="<%=outputPath%>@DayPhone" class="protectedtext" contentEditable=false value="<%=dayphone%>">
    <input type="hidden" yName="<%=outputPath%>@EveningPhone" class="protectedtext" contentEditable=false value="<%=eveningphone%>">
    <input type="hidden" yName="<%=outputPath%>@MobilePhone" class="protectedtext" contentEditable=false value="<%=mobilephone%>">
    <input type="hidden" yName="<%=outputPath%>@DayFaxNo" class="protectedtext" contentEditable=false value="<%=fax%>">
    <input type="hidden" yName="<%=outputPath%>@EMailID" class="protectedtext" contentEditable=false value="<%=email%>">
    <input type="hidden" yName="<%=outputPath%>@AlternateEmailID" class="protectedtext" contentEditable=false value="<%=alternateEmailID%>">
    <input type="hidden" yName="<%=outputPath%>@Beeper" class="protectedtext" contentEditable=false value="<%=beeper%>">
    <input type="hidden" yName="<%=outputPath%>@Department" class="protectedtext" contentEditable=false value="<%=department%>">
    <input type="hidden" yName="<%=outputPath%>@EveningFaxNo" class="protectedtext" contentEditable=false value="<%=eveningFaxNo%>">
    <input type="hidden" yName="<%=outputPath%>@JobTitle" class="protectedtext" contentEditable=false value="<%=jobTitle%>">
    <input type="hidden" yName="<%=outputPath%>@OtherPhone" class="protectedtext" contentEditable=false value="<%=otherPhone%>">
    <input type="hidden" yName="<%=outputPath%>@Suffix" class="protectedtext" contentEditable=false value="<%=suffix%>">
    <input type="hidden" yName="<%=outputPath%>@Title" class="protectedtext" contentEditable=false value="<%=title%>">
    <input type="hidden" yName="<%=outputPath%>@ErrorTxt" class="protectedtext" contentEditable=false value="<%=errortxt%>">
    <input type="hidden" yName="<%=outputPath%>@HttpUrl" class="protectedtext" contentEditable=false value="<%=httpurl%>">
    <input type="hidden" yName="<%=outputPath%>@PersonID" class="protectedtext" contentEditable=false value="<%=personid%>">
    <input type="hidden" yName="<%=outputPath%>@PreferredShipAddress" class="protectedtext" contentEditable=false value="<%=prefershipaddr%>">
    <input type="hidden" yName="<%=outputPath%>@UseCount" class="protectedtext" contentEditable=false value="<%=usecount%>">
    <input type="hidden" yName="<%=outputPath%>@VerificationStatus" class="protectedtext" contentEditable=false value="<%=verificationstatus%>">

<table height="100%" class="view" cellSpacing=0 cellPadding=0 <%=style%> >
    <tr>
        <td valign="top" class="protectedtext">
            <% if(!isVoid(company)) { %> 
                <%=company%>
                <br/>
                <% } %>
            <% if(!isVoid(firstName)) { %> 
                <%=firstName%>
            <% } %>
            <% if(!isVoid(middleName)) { %> 
                <%=middleName%>
            <% } %>
            <% if(!isVoid(lastName)) { %> 
                <%=lastName%>
            <% } if ((!isVoid(firstName)) ||
                    (!isVoid(middleName)) ||
                    (!isVoid(lastName))) { %>
                <br/>
            <% } if(!isVoid(addressLine1)) { %> 
                <%=addressLine1%>
                <br/>
            <% } if(!isVoid(addressLine2)) { %> 
                <%=addressLine2%>
                <br/>
            <% } if(!isVoid(addressLine3)) { %> 
                <%=addressLine3%>
                <br/>
            <% } if(!isVoid(addressLine4)) { %> 
                <%=addressLine4%>
                <br>
            <% } if(!isVoid(addressLine5)) { %> 
                <%=addressLine5%>
                <br/>
            <% } if(!isVoid(addressLine6)) { %> 
                <%=addressLine6%>
                <br>
            <% } if(!isVoid(city)) { %> 
                <%=city%>
            <% } %>
            <% if(!isVoid(state)) { %> 
                <%=state%>
            <% } %>
            <% if(!isVoid(postalCode)) { %> 
                <%=postalCode%>&nbsp;
            <% } if ((!isVoid(city)) && 
                    (!isVoid(state)) && 
                    (!isVoid(postalCode))) { %>
                    <br/>
            <% } if(!isVoid(country)) { %> 
                <%=country%>
                <br/>   
            <%} %>
            <%if(equals("Y",displayPhoneNo)){%>
                <%if(!isVoid(dayphone)){%>
                    <b><yfc:i18n>Day_Phone</yfc:i18n>&nbsp;:&nbsp;</b><%=dayphone%>&nbsp;<br/>
                <%}%>
                <%if(!isVoid(mobilephone)){%>
                    <b><yfc:i18n>Mobile_Phone</yfc:i18n>&nbsp;:&nbsp;</b><%=mobilephone%>&nbsp;<br/>
                <%}%>
                <%if(!isVoid(eveningphone)){%>
                    <b><yfc:i18n>Evening_Phone</yfc:i18n>&nbsp;:&nbsp;</b><%=eveningphone%>&nbsp;
                <%}%>
            <%}%>
        </td>
    </tr>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/commonutil.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>

<script type="text/javascript">	
function f1(){//This function is to select SHQRegion in accordance with the SHQLocal
var comboVal=document.getElementById('SHQlocal').value;
		<% YFCElement codeList = (YFCElement)request.getAttribute("SHQ");
		if (codeList != null) {
			for(YFCElement commonCode :codeList.getElementsByTagName("CommonCode")) {
				String codeValue=commonCode.getAttribute("CodeValue"); %>
				if(comboVal=='<%=codeValue%>') {
					var CodeShortDescription = '<%=commonCode.getAttribute("CodeShortDescription") %>';
					document.getElementById('SHQregion').value=CodeShortDescription;
				}
			<%}
		}%>
}
</script>

<%
	String orgCode1 = resolveValue("xml:/Output/OrganizationList/Organization/@OrganizationCode");
	session.setAttribute("orgCode",orgCode1);
%>

<input type="hidden" name="xml:/Organization/@TerReasonCode" />
<input type="hidden" name="xml:/Organization/@TerReasonText" />

<table class="view" width="100%">
	<tr>
		<td > &emsp;<yfc:i18n>TD_Customer_Site_Org_ID</yfc:i18n>&emsp;&emsp;&emsp;
        <input type="text" class="unprotectedinput" readonly OldValue="" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@OrganizationCode")%>" <%=getTextOptions("xml:/Organization/@OrganizationCode")%>/>
        </td>
		<% String organizationCode = resolveValue("xml:/Output/OrganizationList/Organization/@OrganizationCode");
			session.setAttribute("OrganizationCode",organizationCode);
		%>
		
		<td><yfc:i18n>TD_Parent_Org_ID</yfc:i18n>&emsp;&emsp;&emsp;
		<input  type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@ParentOrganizationCode")%>" <%=getTextOptions("xml:/Organization/@ParentOrganizationCode")%>/>
					<img class="lookupicon" onclick="callOrgLookupForCustomerMaster('xml:/Organization/@ParentOrganizationCode', 'xml:/Organization/@ParentOrganizationName','organization','')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
		</td>
		<%// Call API to get ParentOrganizationName 
		String pOrgCode = resolveValue("xml:/Output/OrganizationList/Organization/@ParentOrganizationCode");
		String parentOrgName = "";
		if(!isVoid(pOrgCode)) {
			YFCDocument inputDoc1 = YFCDocument.parse("<Organization OrganizationKey=\""+pOrgCode+ "\"/>");
			YFCDocument templateDoc1 = YFCDocument.parse("<Organization OrganizationName=\"\"/>");
			%>
			<yfc:callAPI apiName="getOrganizationHierarchy" inputElement="<%=inputDoc1.getDocumentElement()%>" templateElement="<%=templateDoc1.getDocumentElement()%>" outputNamespace="ParentOrg"/>
			<%
			YFCElement parentOrg = getElement("ParentOrg");
			parentOrgName = parentOrg.getAttribute("OrganizationName");
			parentOrgName = parentOrgName == null ? "" : parentOrgName;
		}
		%>
		<td >
			<yfc:i18n>TD_Locale</yfc:i18n>&emsp;
				<select  class="combobox"<%=getComboOptions("xml:/Organization/@LocaleCode")%>>
					<yfc:loopOptions binding="xml:/LocaleList/@Locale" name="LocaleDescription"
					value="Localecode"   selected="xml:/Output/OrganizationList/Organization/@LocaleCode" isLocalized="Y"/>
				</select>
		</td>
		
		<%// Call API to get the data for the Default Currency field. 
		String localecode = resolveValue("xml:/Organization/@LocaleCode");
		YFCDocument inputDoc2 = YFCDocument.parse("<Locale Localecode=\""+localecode+ "\"/>");
		YFCDocument templateDoc2 = YFCDocument.parse("<LocaleList  TotalNumberOfRecords=\"\" ><Locale Currency=\"\" LocaleDescription=\"\" Localecode=\"\" LocaleKey=\"\"/></LocaleList>");
		%>
		<yfc:callAPI apiName="getLocaleList" inputElement='<%=inputDoc2.getDocumentElement()%>'  templateElement="<%=templateDoc2.getDocumentElement()%>" outputNamespace='LocaleList'/>
		
		<td><yfc:i18n>TD_Comment</yfc:i18n></td>
	</tr>	
	<tr>
		<td>&emsp;<yfc:i18n>TD_Customer_Site_Org_Name</yfc:i18n>&emsp;
		<input type="text"  size="25" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@OrganizationName")%>" <%=getTextOptions("xml:/Organization/@OrganizationName")%>/>
		</td>
		
		<td><yfc:i18n>TD_Parent_Org_Name</yfc:i18n>&emsp;
		<input type="text" size="30" class="unprotectedinput" readonly value="<%=parentOrgName%>"  <%=getTextOptions("xml:/Organization/@ParentOrganizationName")%>/>
		</td>
		<td><yfc:i18n>TD_SHQ_Local</yfc:i18n>&emsp;
		<select  class="combobox" id="SHQlocal" onchange="f1()" <%=getComboOptions("xml:/Organization/Extn/@SHQLocal")%> >
					<yfc:loopOptions binding="xml:SHQ:/CommonCodeList/@CommonCode" name="CodeValue"
					value="CodeValue"   selected="xml:/Output/OrganizationList/Organization/Extn/@SHQLocal" />
		</select>
		</td>
		<td >
			<textarea class="unprotectedtextareainput" rows="3" cols="50%"<%=getTextAreaOptions("xml:/Organization/Extn/@Comments")%>><yfc:getXMLValue binding="xml:/Output/OrganizationList/Organization/Extn/@Comments"/></textarea>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><yfc:i18n>TD_SHQ_Region</yfc:i18n>&emsp;
		<input type="text" size="20" class="unprotectedinput" id="SHQregion" readonly value="<%=resolveValue("xml:/Output/OrganizationList/Organization/Extn/@SHQRegion")%>" <%=getTextOptions("xml:/Organization/Extn/@SHQRegion")%>/>
		</td>
	</tr>
	
</table>	
	
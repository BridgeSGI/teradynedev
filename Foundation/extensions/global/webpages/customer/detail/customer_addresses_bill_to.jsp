<%@include file="/yfsjspcommon/yfsutil.jspf"%>

<script language="javascript">
function callLookupforBill(){
	var ob = new Object();
	ob.field1=document.getElementById('billingId');
	ob.field2=document.getElementById('al1');
	ob.field3=document.getElementById('al2');
	ob.field4=document.getElementById('al3');
	ob.field5=document.getElementById('al4');
	ob.field6=document.getElementById('al5');
	ob.field7=document.getElementById('al6');
	ob.field8=document.getElementById('city');
	ob.field9=document.getElementById('st');
	ob.field10=document.getElementById('coun');
	ob.field11=document.getElementById('pc');
	ob.field12=document.getElementById('dtp');
	ob.field13=document.getElementById('ep');
	ob.field14=document.getElementById('mp');
	ob.field15=document.getElementById('beeper');
	ob.field16=document.getElementById('otherPhone');
	ob.field17=document.getElementById('fax');
	ob.field18=document.getElementById('eveFax');
	ob.field19=document.getElementById('email');
	ob.field20=document.getElementById('altEmailId');
	yfcShowSearchPopupWithParams('','lookup',900,550,ob,'TDBillTo','');
}
</script>


<%
	String Path = HTMLEncode.htmlEscape((String)request.getParameter("Path"));
%>

<script language="Javascript" >
	IgnoreChangeNames();
</script>

<input type="hidden" id="altEmailId"/>
<input type="hidden" id="eveFax"/>
<input type="hidden" id="otherPhone"/>
<input type="hidden" id="beeper"/>
<input type="hidden" id="billingId"/>

<table class="view" height="100%">
<tr>
	<td valign="top" height="100%" >
        <table class="view"  ID="ModifyAddressLeft" >
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_1</yfc:i18n></td>
            <td>
                <input type="text" id="al1" OldValue="<%=resolveValue(Path+"/@AddressLine1")%>" class="unprotectedinput" <%=getTextOptions(Path+"/@AddressLine1")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_2</yfc:i18n></td>
            <td>
                <input type="text" id="al2" class="unprotectedinput" <%=getTextOptions(Path+"/@AddressLine2")%>>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_3</yfc:i18n></td>
            <td>
                <input type="text" id="al3" class="unprotectedinput" <%=getTextOptions(Path+"/@AddressLine3")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_4</yfc:i18n></td>
            <td>
                <input type="text" id="al4" class="unprotectedinput" <%=getTextOptions(Path+"/@AddressLine4")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_5</yfc:i18n></td>
            <td>
                <input type="text" id="al5" class="unprotectedinput" <%=getTextOptions(Path+"/@AddressLine5")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_6</yfc:i18n></td>
            <td>
                <input type="text" id="al6" class="unprotectedinput" <%=getTextOptions(Path+"/@AddressLine6")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>City</yfc:i18n></td>
            <td><input type="text" id="city" class="unprotectedinput" <%=getTextOptions(Path+"/@City")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>State</yfc:i18n></td>
            <td><input type="text" id="st" class="unprotectedinput" <%=getTextOptions(Path+"/@State")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Postal_Code</yfc:i18n></td>
            <td><input type="text" id="pc" class="unprotectedinput" <%=getTextOptions(Path+"/@ZipCode")%> ></td>
        </tr>
		<%
		String countryCode = resolveValue(Path+"/@Country");
		%>
        <tr>
			<td class="detaillabel" ><yfc:i18n>Country</yfc:i18n></td>
		    <td>
				<select id="coun" class="combobox" <%=getComboOptions(Path+"/@Country")%>>
					<yfc:loopOptions binding="xml:CommonCountryCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" isLocalized="Y" selected="<%=countryCode%>"/>
				</select>			
	        </td>
        </tr>

		</table>
    </td>
	<td valign="top" height="100%" >
        <table class="view"  ID="ModifyAddressRight" >
        <tr>
            <td class="detaillabel" ><yfc:i18n>First_Name</yfc:i18n></td>
            <td><input id="fn" class="unprotectedinput" <%=getTextOptions(Path+"/@FirstName")%>></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Middle_Name</yfc:i18n></td>
            <td><input id="mn" class="unprotectedinput" <%=getTextOptions(Path+"/@MiddleName")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Last_Name</yfc:i18n></td>
            <td><input id="ln" class="unprotectedinput" <%=getTextOptions(Path+"/@LastName")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Company</yfc:i18n></td>
            <td><input id="comp" class="unprotectedinput" <%=getTextOptions(Path+"/@Company")%>></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Day_Time_Phone</yfc:i18n></td>
            <td><input id="dtp" class="unprotectedinput" <%=getTextOptions(Path+"/@DayPhone")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Evening_Phone</yfc:i18n></td>
            <td><input id="ep" class="unprotectedinput" <%=getTextOptions(Path+"/@EveningPhone")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Mobile_Phone</yfc:i18n></td>
            <td><input id="mp" class="unprotectedinput" <%=getTextOptions(Path+"/@MobilePhone")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Fax</yfc:i18n></td>
            <td><input id="fax" class="unprotectedinput" <%=getTextOptions(Path+"/@DayFaxNo")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>E_Mail</yfc:i18n></td>
            <td><input id="email" class="unprotectedinput" <%=getTextOptions(Path+"/@EMailID")%> ></td>
        </tr>
    	</table>
    </td>
</tr>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="Javascript" > 
IgnoreChangeNames(); 
</script>
<%
// Call API to get the data for the Document Type field.
YFCElement documentTypeInput = YFCDocument.parse("<CommonCode CodeType=\"COUNTRY\" />").getDocumentElement();

YFCElement documentTypeTemplate = YFCDocument.parse("<CommonCode CodeName=\"\" CodeShortDescription=\"\" CodeType=\"\" CodeValue=\"\" CommonCodeKey=\"\" />").getDocumentElement();
%>

<yfc:callAPI apiName="getCommonCodeList" inputElement="<%=documentTypeInput%>" templateElement="<%=documentTypeTemplate%>" outputNamespace="CommonCountryCodeList"/>

<%String organizationCode = (String)session.getAttribute("OrganizationCode");%>

<%
Map<String, String> addrMap = (Map<String, String>) session.getAttribute("AddressMap");
%>

<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
		<td>
			<input type="hidden" name="xml:/Organization/@OrganizationCode" value="<%=organizationCode%>"/>
			<input type="hidden" class="unprotectedinput" value="<%=addrMap.get("AddressType")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@AddressType")%> />
			<input type="hidden" value="<%=addrMap.get("AlternateEmailId")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@AlternateEmailID")%> >
			<input type="hidden" value="<%=addrMap.get("Beeper")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@Beeper")%> >
			<input type="hidden" value="<%=addrMap.get("Department")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@Department")%> >
			<input type="hidden" value="<%=addrMap.get("EveningFaxNo")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@EveningFaxNo")%> >
			<input type="hidden" value="<%=addrMap.get("JobTitle")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@JobTitle")%> >
			<input type="hidden" value="<%=addrMap.get("OtherPhone")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@OtherPhone")%> >        
			<input type="hidden" value="<%=addrMap.get("Suffix")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@Suffix")%> >
			<input type="hidden" value="<%=addrMap.get("Title")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@Title")%> >
			<input type="hidden" value="<%=addrMap.get("ErrorTxt")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@ErrorTxt")%> />
			<input type="hidden" value="<%=addrMap.get("HttpUrl")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@HttpUrl")%> />
			<input type="hidden" value="<%=addrMap.get("PersonID")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@PersonID")%> />
			<input type="hidden" value="<%=addrMap.get("PreferredShipAddress")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@PreferredShipAddress")%> />
			<input type="hidden" value="<%=addrMap.get("UseCount")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@UseCount")%> />
			<input type="hidden" value="<%=addrMap.get("VerificationStatus")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@VerificationStatus")%> /> 
		</td>
    </tr>
	<tr id="AddressTypeRow">
    <td valign="top" height="100%" >
        <table>
            <tr>
                <td class="detaillabel" ><yfc:i18n>Address_Type</yfc:i18n></td>
                <td>
                    <input type="text" class="protectedinput" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@AddressTypeLabel")%>>
                </td>
            </tr>
        </table>
    </td>
	</tr>
	<tr>
	<td valign="top" height="100%" >
        <table class="view"  ID="ModifyAddressLeft" >
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_1</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" value="<%=addrMap.get("AddressLine1")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@AddressLine1")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_2</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" value="<%=addrMap.get("AddressLine2")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@AddressLine2")%>>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_3</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" value="<%=addrMap.get("AddressLine3")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@AddressLine3")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_4</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" value="<%=addrMap.get("AddressLine4")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@AddressLine4")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_5</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" value="<%=addrMap.get("AddressLine5")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@AddressLine5")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Address_Line_6</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" value="<%=addrMap.get("AddressLine6")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@AddressLine6")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>City</yfc:i18n></td>
            <td><input type="text" class="unprotectedinput" value="<%=addrMap.get("City")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@City")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>State</yfc:i18n></td>
            <td><input type="text" class="unprotectedinput" value="<%=addrMap.get("State")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@State")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Postal_Code</yfc:i18n></td>
            <td><input type="text" class="unprotectedinput" value="<%=addrMap.get("PostalCode")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@ZipCode")%> ></td>
        </tr>
        <tr>
			<td class="detaillabel" ><yfc:i18n>Country</yfc:i18n></td>
		    <td>
				<select id="CountryCodeObj" <%=getComboOptions("xml:/Organization/CorporatePersonInfo/@Country")%>>
					<yfc:loopOptions binding="xml:CommonCountryCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue" isLocalized="Y" selected="<%=addrMap.get("Country")%>"/>
				</select>			
	        </td>
        </tr>
	    </table>
    </td>
	<td valign="top" height="100%" >
        <table class="view"  ID="ModifyAddressRight" >
        <tr>
            <td class="detaillabel" ><yfc:i18n>First_Name</yfc:i18n></td>
            <td><input class="unprotectedinput" value="<%=addrMap.get("GivenName")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@FirstName")%>></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Middle_Name</yfc:i18n></td>
            <td><input class="unprotectedinput" value="<%=addrMap.get("MiddleName")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@MiddleName")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Last_Name</yfc:i18n></td>
            <td><input class="unprotectedinput" value="<%=addrMap.get("Surname")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@LastName")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Company</yfc:i18n></td>
            <td><input class="unprotectedinput" value="<%=addrMap.get("Company")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@Company")%>></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Day_Time_Phone</yfc:i18n></td>
            <td><input class="unprotectedinput" value="<%=addrMap.get("DayTimePhone")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@DayPhone")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Evening_Phone</yfc:i18n></td>
            <td><input class="unprotectedinput" value="<%=addrMap.get("EveningPhone")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@EveningPhone")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Mobile_Phone</yfc:i18n></td>
            <td><input class="unprotectedinput" value="<%=addrMap.get("MobilePhone")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@MobilePhone")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Fax</yfc:i18n></td>
            <td><input class="unprotectedinput" value="<%=addrMap.get("Fax")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@DayFaxNo")%> ></td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>E_Mail</yfc:i18n></td>
            <td><input class="unprotectedinput" value="<%=addrMap.get("Email")%>" <%=getTextOptions("xml:/Organization/CorporatePersonInfo/@EmailID")%> ></td>
        </tr>
    	</table>
    </td>
	</tr>
</table>

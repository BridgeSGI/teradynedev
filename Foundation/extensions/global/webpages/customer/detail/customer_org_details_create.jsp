<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/commonutil.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>

<input type="hidden" class="unprotectedinput" <%=resolveValue("xml:/Organization/@OrganizationKey")%> <%=getTextOptions("xml:/Organization/@OrganizationKey")%>/>
<input type="hidden" value="Modify" <%=getTextOptions("xml:/Organization/@Operation")%> />
<input type="hidden" value="N" <%=getTextOptions("xml:/Organization/@CustomerCreate")%> />

<input type="hidden" name="xml:/Organization/@TerReasonCode" />
<input type="hidden" name="xml:/Organization/@TerReasonText" />

<table class="view" width="100%">
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_Customer_Org_ID</yfc:i18n>:</td>
		<td>
			<input type="text" size="25" class="unprotectedinput" readonly <%=resolveValue("xml:/Organization/@OrganizationCode")%> <%=getTextOptions("xml:/Organization/@OrganizationCode")%>/>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_Locale</yfc:i18n></td>
        <td nowrap="true">
			<%
			String selectedLocale = resolveValue("xml:/Organization/@LocaleCode");
			%>
			<select class="combobox" name="xml:/Organization/@LocaleCode" tabindex="1">
                <yfc:loopOptions binding="xml:/LocaleList/@Locale" name="LocaleDescription" value="Localecode" isLocalized="N" targetBinding="xml:/Organization/@LocaleCode" selected="<%=selectedLocale%>"/>
            </select>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_Parent_Org_ID</yfc:i18n></td>
		<td nowrap="true">
			<%
			String parentOrgCode = resolveValue("xml:/Organization/@ParentOrganizationCode");
			String parentOrgName = "";
			if (!isVoid(parentOrgCode)) {
				YFCDocument inputDocument = YFCDocument.createDocument("Organization");
				YFCElement org = inputDocument.getDocumentElement();
				org.setAttribute("OrganizationKey", parentOrgCode);
				YFCDocument outputDocument = YFCDocument.parse("<Organization OrganizationCode=\"\" OrganizationName=\"\" />");
				%>
				<yfc:callAPI apiName="getOrganizationHierarchy" inputElement="<%=inputDocument.getDocumentElement()%>" templateElement="<%=outputDocument.getDocumentElement()%>" outputNamespace="ParentOrg"/>
				<%
				YFCElement parentOrg = getElement("ParentOrg");
				parentOrgName = parentOrg.getAttribute("OrganizationName");
				parentOrgName = parentOrgName == null ? "" : parentOrgName;
			}
			%>
			<input type="text" size="25" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@ParentOrganizationCode") %> tabindex="2"/>
			<img class="lookupicon" onclick="callOrgLookupForCustomerMaster('xml:/Organization/@ParentOrganizationCode','xml:/Organization/@ParentOrganizationName','organization','xml:/Organization/EnterpriseKey=CSO')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_Comment</yfc:i18n></td>
		<td rowspan="2">
			<textarea class="unprotectedtextareainput" rows="4" cols="45" <%=getTextAreaOptions("xml:/Organization/Extn/@Comments")%> tabindex="4"><yfc:getXMLValue binding="xml:/Organization/Extn/@Comments"/></textarea>
        </td>
    </tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_Customer_Org_Name</yfc:i18n></td>
        <td>
			<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationName")%> tabindex="3"/>
        </td>
		<td>&nbsp;</td>
        <td>&nbsp;</td>
		<td class="detaillabel"><yfc:i18n>TD_Parent_Org_Name</yfc:i18n></td>
        <td>
			<input type="text" size="30" class="protectedinput" readonly value="<%=parentOrgName%>" <%=getTextOptions("xml:/Organization/@ParentOrganizationName") %>/>
        </td>
	</tr>
	<tr></tr>
	<tr></tr>
</table>

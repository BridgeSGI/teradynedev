<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/commonutil.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript">
<%
	String organizationKey = resolveValue("xml:/Organization/@OrganizationKey");
	String pOrgCode = resolveValue("xml:/ParentOrganization/@CurrOrgCode");	
	if (!isVoid(organizationKey)) {
		YFCDocument orgDoc = YFCDocument.createDocument("Organization");
		YFCElement organization = orgDoc.getDocumentElement();
		organization.setAttribute("OrganizationKey",organizationKey);
		%>
		function showOrderDetail() {
		    showDetailFor('<%=orgDoc.getDocumentElement().getString(false)%>');
		}
		window.attachEvent("onload", showOrderDetail);
	<%
	}
%>
</script>
<script type="text/javascript">	
function f1(){//This function is to select SHQRegion in accordance with the SHQLocal
var comboVal=document.getElementById('SHQlocal').value;
		<% YFCElement codeList = (YFCElement)request.getAttribute("SHQ");
		if (codeList != null) {
			for(YFCElement commonCode :codeList.getElementsByTagName("CommonCode")) {
				String codeValue=commonCode.getAttribute("CodeValue"); %>
				if(comboVal=='<%=codeValue%>') {
					var CodeShortDescription = '<%=commonCode.getAttribute("CodeShortDescription") %>';
					document.getElementById('SHQregion').value=CodeShortDescription;
				}
			<%}
		}%>
}
</script>

			<input type="hidden" name="xml:/Organization/@IsNode" value="Y"/>
			<input type="hidden" name="xml:/Organization/@InventoryPublished" value="Y"/>
			<input type="hidden" name="xml:/Organization/@PrimaryEnterpriseKey" value="CSO"/>
			<input type="hidden" name="xml:/Organization/OrgRoleList/OrgRole/@RoleKey" value="BUYER"/>
		
<table class="view">
   <tr>
		<td class="detaillabel">
			<yfc:i18n>TD_Customer_Site_Org_ID</yfc:i18n>
		</td>
		<td>
			<input type="text" size="20" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationCode","")%> tabindex="1" />
		</td>			
		<td class="detaillabel">
			<yfc:i18n>TD_Parent_Org_ID</yfc:i18n>
		</td>
		<td nowrap="true">
			<input type="text" id='parentOrg' OldValue="<%=resolveValue("xml:/Organization/@ParentOrganizationCode")%>" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@ParentOrganizationCode",pOrgCode)%> tabindex="3" />
			<img class="lookupicon" onclick="callOrgLookupForCustomerMaster('xml:/Organization/@ParentOrganizationCode', 'xml:/Organization/@ParentOrganizationName','organization','')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
		</td>
		<td class="detaillabel">
			<yfc:i18n>TD_Locale</yfc:i18n>
		</td>
		<td>
			<select class="combobox"<%=getComboOptions("xml:/Organization/@LocaleCode")%> tabindex="4">
			<yfc:loopOptions binding="xml:/LocaleList/@Locale" name="LocaleDescription"	value="Localecode"   selected="xml:/Organization/@LocaleCode" isLocalized="Y"/>
			</select>
		</td>
		<td class="detaillabel">
			<yfc:i18n>TD_Comment</yfc:i18n>
		</td>
		<td rowspan="3">
			<textarea class="unprotectedtextareainput" rows="5" cols="36" <%=getTextAreaOptions("xml:/Organization/Extn/@Comments")%> tabindex="6"></textarea>
		</td>
	</tr>
	<tr>
		<td class="detaillabel">
			<yfc:i18n>TD_Customer_Site_Org_Name</yfc:i18n>
		</td>
		<td>
			<input type="text" size="25" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationName")%> tabindex="2" />
		</td>
		<td class="detaillabel">
			<yfc:i18n>TD_Parent_Org_Name</yfc:i18n>
		</td>
		<td>
			<input type="text" size="25" class="unprotectedinput" readonly <%=getTextOptions("xml:/Organization/@ParentOrganizationName")%>/>
		</td>
		<td class="detaillabel">
			<yfc:i18n>TD_SHQ_Local</yfc:i18n>
		</td>
		<td>
			<select class="combobox" id="SHQlocal" onchange="f1()" <%=getComboOptions("xml:/Organization/Extn/@SHQLocal")%> tabindex="5">
				<yfc:loopOptions binding="xml:SHQ:/CommonCodeList/@CommonCode" name="CodeValue"
				value="CodeValue"   selected="xml:/Organization/Extn/@SHQLocal" isLocalized="Y"/>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td class="detaillabel">
			<yfc:i18n>TD_SHQ_Region</yfc:i18n>
		</td>
		<td>
			<input size="25" type="text" class="unprotectedinput" id="SHQregion" readonly <%=getTextOptions("xml:/Organization/Extn/@SHQRegion")%>/>
		</td>	
	</tr>
</table>

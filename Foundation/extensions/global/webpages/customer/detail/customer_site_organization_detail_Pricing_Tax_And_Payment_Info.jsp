<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<%@ page import="com.bridge.sterling.utils.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/commonutil.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>

<script>
function showOfficeRecordCommonCodeLookupPopup(CodeValue, entityname, extraParamCodeType, extraParamActualSearchLabel){
	var oObj = new Object();
	oObj.field1 = document.all(CodeValue);
	var extraParams = "&CodeType=" + extraParamCodeType + "&ActualSearchLabel="+extraParamActualSearchLabel;
	yfcShowSearchPopupWithParams('TDorlookupS020',' ',850,550,oObj,entityname, extraParams);		
}

function showOfficeRecordLookupPopup(V1, V2, entityname, extraParamsLookUpAttribute, extraParamOfficeTypeValue){
	var oObj = new Object();
	oObj.field1 = document.all(V1);
	oObj.field2 = document.all(V2);
	oObj.returnOfficeNameFlag = true;
	var extraParams = "&LookupAttribute=" + extraParamsLookUpAttribute + "&OfficeTypeValue=" + extraParamOfficeTypeValue;
	yfcShowSearchPopupWithParams('TDofficeS030',' ',900,550,oObj,entityname, extraParams);
}
</script>

<input type="hidden" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/Extn/@TeradyneServiceOffice")%>" <%=getTextOptions("xml:/Organization/Extn/@TeradyneServiceOffice")%>/>

<table class="view" width="100%" border="0" >
	<tr>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Default_Currency</yfc:i18n>
		</td>
		<%
		String defCurrency = resolveValue("xml:/Output/CustomerList/Customer/Extn/@DefaultCurrency");
		if (isVoid(defCurrency)) {
			String orgLocale = resolveValue("xml:/Output/OrganizationList/Organization/@LocaleCode");
			YFCElement localeList = getElement("LocaleList");
			defCurrency = XPathUtil.getXpathAttribute(localeList, "/LocaleList/Locale[@Localecode='" + orgLocale +
			"']/@Currency");
		}
		%>
		<td>
			<select class="combobox" OldValue="" <%=getComboOptions("xml:/Organization/ForCustomer/@DefaultCurrency")%> >
				<yfc:loopOptions binding="xml:Currency:/CurrencyList/@Currency" name="CurrencyDescription" value="CurrencyKey" selected="<%=defCurrency%>"/>
			</select>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>TD_Is_Tax_Exempt</yfc:i18n>
		</td>
		<td>
			<input type="checkbox" <%=getCheckBoxOptions("xml:/Organization/@TaxExemptFlag", "xml:/Output/OrganizationList/Organization/@TaxExemptFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' '>
			</input>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>TD_Expemtion_Certificate</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@TaxExemptionCertificate")%>" <%=getTextOptions("xml:/Organization/@TaxExemptionCertificate")%>/>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>TD_Price_Zone</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/CustomerList/Customer/Extn/@PriceZone")%>" <%=getTextOptions("xml:/Organization/ForCustomer/@PriceZone")%>/>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Tax_Payer_ID</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@TaxpayerId")%>" <%=getTextOptions("xml:/Organization/@TaxpayerId")%>/>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Tax_Jurisdiction</yfc:i18n>&nbsp;
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@TaxJurisdiction")%>" <%=getTextOptions("xml:/Organization/@TaxJurisdiction")%>/>
		</td>	
	</tr>
	<tr>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>TD_Internal_Customer_Site</yfc:i18n>
		</td>
		<td>
			<input type="checkbox" <%=getCheckBoxOptions("xml:/Organization/ForCustomer/@InternalCustomerFlag", "xml:/Output/CustomerList/Customer/Extn/@InternalCustomerFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' >
			</input>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Issuing_Authority</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@IssuingAuthority")%>" <%=getTextOptions("xml:/Organization/@IssuingAuthority")%>/>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>TD_Service_Location</yfc:i18n>
		</td>
		<td>
			<%
				String terServiceOfficeName = "";
				String tdServiceOfficeCode = resolveValue("xml:/Output/OrganizationList/Organization/Extn/@TeradyneServiceOffice");
				if (!isVoid(tdServiceOfficeCode)) {
					YFCDocument inputDoc = YFCDocument.parse("<TerOfficeRecords TerOfficeCode=\"" + tdServiceOfficeCode + "\"/>");
					YFCDocument templateDoc = YFCDocument.parse("<TerOfficeRecords TerOfficeCode=\"\" TerOfficeName=\"\"/>");
					%>
					<yfc:callAPI serviceName='getTerOfficeRecordsList' inputElement='<%=inputDoc.getDocumentElement()%>' templateElement='<%=templateDoc.getDocumentElement()%>' outputNamespace='ServiceOffice'/>
					<%
					YFCElement terOfficeRecordsList = getElement("ServiceOffice");
					YFCElement terOfficeRecords = terOfficeRecordsList.getChildElement("TerOfficeRecords");
					if (!isVoid(terOfficeRecords)) {
						terServiceOfficeName = terOfficeRecords.getAttribute("TerOfficeName");
					}
				}
			%>
			<input type="text" size="10" class="unprotectedinput" name="TerServiceOfficeName" value="<%=terServiceOfficeName%>" />
			<img class="lookupicon" onclick="showOfficeRecordLookupPopup('xml:/Organization/Extn/@TeradyneServiceOffice', 'TerServiceOfficeName','TDoffice', 'TerServiceLocation', 'L')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_For_Service_Office")%>/>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" nowrap="true" colspan="2" style="text-align:left">
			<yfc:i18n>TD_Customer_Site_Type</yfc:i18n>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Authority_Type</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@AuthorityType")%>" <%=getTextOptions("xml:/Organization/@AuthorityType")%>/>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>TD_Revenue_Office</yfc:i18n>
		</td>
		<td>
			<input type="text" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/Extn/@TeradyneRevenueRegion")%>" <%=getTextOptions("xml:/Organization/Extn/@TeradyneRevenueRegion")%>/>
			<img class="lookupicon" onclick="showOfficeRecordCommonCodeLookupPopup('xml:/Organization/Extn/@TeradyneRevenueRegion', 'TDofficelookup', 'GSO_Revenue_Region', 'GSO Revenue Region')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_For_Revenue_Office")%>/>
		</td>
	</tr>
	<tr>
		<td colspan="4">
			<select class="combobox" <%=getComboOptions("xml:/Organization/Extn/@CustomerSiteType")%> >
				<yfc:loopOptions binding="xml:CUSTOMER_SITE_TYPES:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/Output/OrganizationList/Organization/Extn/@CustomerSiteType"/>
			</select>
		</td>
	</tr>
</table>
	
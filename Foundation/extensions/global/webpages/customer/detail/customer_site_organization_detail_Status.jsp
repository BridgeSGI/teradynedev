<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript">
<%
String status = resolveValue("xml:/Output/OrganizationList/Organization/Extn/@CustomerSiteStatus");
if (isVoid(status) || status.equals("A")) {
%>
	function setDefaultStatusAsActive() {
		document.getElementsByName("xml:/Organization/Extn/@CustomerSiteStatus")[0].value='A';
	}
	window.attachEvent('onload', setDefaultStatusAsActive);
<%
}
%>
</script>

<script language="javascript">
function validatePaymentTerms() {
	var paymentTerm = document.getElementById('pt');
	if (paymentTerm && paymentTerm.value) {
		enterSCReason('CustModReason','xml:/Organization/@TerReasonCode','xml:/Organization/@TerReasonText');
		return true;
	} else {
		alert('Payment Terms is mandatory');
		return false;
	}
}
</script>

<table class="view" border=0>
	<tr>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Status</yfc:i18n>
		</td>
		<td>
			<select class="combobox" <%=getComboOptions("xml:/Organization/Extn/@CustomerSiteStatus")%>>
					<yfc:loopOptions binding="xml:ORGANIZATION_STATUS:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue"   selected="xml:/Output/OrganizationList/Organization/Extn/@CustomerSiteStatus" />
			</select>		
		</td>
		<td nowrap="true" class="detaillabel" nowrap="true">
			<yfc:i18n>Payment_Terms</yfc:i18n>&nbsp;<span style="color:red">*</span>
		</td>
		<td>
			<select id="pt" class="combobox" <%=getComboOptions("xml:/Organization/ForCustomer/@PaymentTerms")%>>
					<yfc:loopOptions binding="xml:PAYMENT_TERM:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue"  selected="xml:/Output/CustomerList/Customer/Extn/@PaymentTerms"/>
			</select>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>TD_Credit_Hold</yfc:i18n>
		</td>
		<td>
			<select class="combobox" <%=getComboOptions("xml:/Organization/ForCustomer/@IsCreditHold")%>>
					<yfc:loopOptions binding="xml:CREDIT_HOLD:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue"  selected="xml:/Output/CustomerList/Customer/Extn/@IsCreditHold"/>
			</select>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>TD_Service_Hold</yfc:i18n>
		</td>
		<td>
			<select class="combobox" <%=getComboOptions("xml:/Organization/ForCustomer/@IsServiceHold")%> >
					<yfc:loopOptions binding="xml:SERVICE_HOLD:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue"   selected="xml:/Output/CustomerList/Customer/Extn/@IsServiceHold"/>
			</select>
		</td>
	</tr>
</table>	
	
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/commonutil.js"></script>

<script language="javascript">
<%
	String organizationKey = resolveValue("xml:/Organization/@OrganizationKey");
	String parentOrganizationName = resolveValue("xml:/Organization/@ParentOrganizationName");
	if (!isVoid(organizationKey)) {
		YFCDocument orgDoc = YFCDocument.createDocument("Organization");
		YFCElement organization = orgDoc.getDocumentElement();
		organization.setAttribute("OrganizationKey",organizationKey);
	%>
		function showDetail() {
			showDetailFor('<%=orgDoc.getDocumentElement().getString(false)%>');
		}
		window.attachEvent("onload", showDetail);
	<%
	}
%>
</script>

<input type="hidden" value="Y" <%=getTextOptions("xml:/Organization/@CustomerCreate")%> />

<table class="view" width="100%">
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_Customer_Org_ID</yfc:i18n></td>
        <td>
			<input type="text" size="25" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationCode","")%> tabindex="1"/>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_Locale</yfc:i18n></td>
        <td nowrap="true">
			<select class="combobox" name="xml:/Organization/@LocaleCode" tabindex="3">
                <yfc:loopOptions binding="xml:/LocaleList/@Locale" name="LocaleDescription" value="Localecode" isLocalized="N" targetBinding="xml:/Organization/@LocaleCode"/>
            </select>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_Parent_Org_ID</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" size="25" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@ParentOrganizationCode") %> tabindex="4"/>
			<img class="lookupicon" onclick="callOrgLookupForCustomerMaster('xml:/Organization/@ParentOrganizationCode','xml:/Organization/@ParentOrganizationName','organization','xml:/Organization/EnterpriseKey=CSO')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
		<td class="detaillabel"><yfc:i18n>TD_Comment</yfc:i18n></td>
		<td rowspan="2">
			<textarea class="unprotectedtextareainput" rows="4" cols="45" <%=getTextOptions("xml:/Organization/Extn/@Comments")%> tabindex="5"></textarea>
		</td>
    </tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_Customer_Org_Name</yfc:i18n></td>
        <td>
			<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/Organization/@OrganizationName")%> tabindex="2"/>
        </td>
		<td>&nbsp;</td>
        <td>&nbsp;</td>
		<td class="detaillabel" ><yfc:i18n>TD_Parent_Org_Name</yfc:i18n></td>
        <td>
			<input type="text" size="30" class="unprotectedinput" readonly <%=getTextOptions("xml:/Organization/@ParentOrganizationName") %>/>
        </td>
	</tr>
	<tr></tr>
	<tr></tr>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<table class="view" width="100%" border="0" >
	<tr>
		<td nowrap="true" class="detaillabel">
			<yfc:i18n>TD_IB_Part_Direct_Ship_Entitled</yfc:i18n>
		</td>
		<td>
			<input type="checkbox" <%=getCheckBoxOptions("xml:/Organization/ForCustomer/@IsInboundDirectShipAllowed", "xml:/Output/CustomerList/Customer/Extn/@IsInboundDirectShipAllowed", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' '>
			</input>
		</td>
	</tr>
	<tr>
		<td nowrap="true" class="detaillabel">
			<yfc:i18n>TD_OB_Part_Direct_Ship_Entitled</yfc:i18n>
		</td>
		<td>
			<input type="checkbox" <%=getCheckBoxOptions("xml:/Organization/ForCustomer/@IsOutboundDirectShipAllowed", "xml:/Output/CustomerList/Customer/Extn/@IsOutboundDirectShipAllowed", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' '>
			</input>
		</td>
	</tr>
	<tr>
		<td nowrap="true" class="detaillabel">
			<yfc:i18n>TD_Per_Trans_Exp_Entitled</yfc:i18n>
		</td>
		<td>
			<input type="checkbox" <%=getCheckBoxOptions("xml:/Organization/ForCustomer/@IsTransExceptionAllowed", "xml:/Output/CustomerList/Customer/Extn/@IsTransExceptionAllowed", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' '>
			</input>
		</td>
	</tr>
</table>	
	
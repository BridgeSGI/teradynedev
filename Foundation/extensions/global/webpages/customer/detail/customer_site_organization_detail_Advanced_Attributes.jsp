<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<%
YFCElement output = getElement("Output");
YFCElement orgList = output.getChildElement("OrganizationList");
YFCElement org = orgList.getChildElement("Organization");
String currentOrgCode = resolveValue("xml:/Output/OrganizationList/Organization/@OrganizationCode");
%>

<table class="view" width="100%" border="0">
	<tr>
		<td>
			<input type="radio" disabled="true" <%=getRadioOptions("xml:/Output/OrganizationList/Organization/@InventoryPublished", "xml:/Output/OrganizationList/Organization/@InventoryPublished", "N")%>><yfc:i18n>TD_Inv_Info_Not_Available</yfc:i18n></input>
		</td>
		<%
		boolean isCatalogOrg = false;
		if (org.hasAttribute("IsCatalogOrg")) {
			org.removeAttribute("IsCatalogOrg");
		}
		String catalogOrgCode = resolveValue("xml:/Output/OrganizationList/Organization/@CatalogOrganizationCode");
		if (catalogOrgCode != null) {
			if (currentOrgCode.equals(catalogOrgCode)) {
				isCatalogOrg = true;
				org.setAttribute("IsCatalogOrg","Y");
			} else {
				org.setAttribute("IsCatalogOrg","N");
			}
		} else {
			org.setAttribute("IsCatalogOrg","N");
		}
		%>
		<td>
			<input type="radio" disabled="true" <%=getRadioOptions("xml:/Output/OrganizationList/Organization/@IsCatalogOrg", "xml:/Output/OrganizationList/Organization/@IsCatalogOrg", "Y")%>><yfc:i18n>TD_Org_Defines_Own_Catalog</yfc:i18n></input>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" disabled="true" <%=getRadioOptions("xml:/Output/OrganizationList/Organization/@InventoryPublished", "xml:/Output/OrganizationList/Organization/@InventoryPublished", "Y")%>><yfc:i18n>TD_Inv_Info_Available</yfc:i18n></input>
		</td>
		<%
		String selectedCatalogOrg = "";
		if (!isCatalogOrg) {
			selectedCatalogOrg = resolveValue("xml:/Output/OrganizationList/Organization/@CatalogOrganizationCode");
		}
		%>
		<td>
			<input type="radio" disabled="true" <%=getRadioOptions("xml:/Output/OrganizationList/Organization/@IsCatalogOrg", "xml:/Output/OrganizationList/Organization/@IsCatalogOrg", "N")%>><yfc:i18n>TD_Catalog_Defined_By</yfc:i18n></input>
			<select class="combobox" disabled="true">
				<yfc:loopOptions binding="xml:OrganizationList:/OrganizationList/@Organization" name="OrganizationName" value="OrganizationKey" selected="<%=selectedCatalogOrg%>"/>
			</select>
		</td>
	</tr>
	
	<%
	if (org.hasAttribute("IsInvOrg")) {
		org.removeAttribute("IsInvOrg");
	}
	boolean flag = false;
	String inInvPub = org.getAttribute("InventoryPublished");
	if (inInvPub.equals("Y")) {
		flag = true;
	}
	String inventoryOrgCode = resolveValue("xml:/Output/OrganizationList/Organization/@InventoryOrganizationCode");
	if (inventoryOrgCode != null) {
		if (currentOrgCode.equals(inventoryOrgCode)) {
			org.setAttribute("IsInvOrg","Y");
		} else {
			org.setAttribute("IsInvOrg","N");
		}
	} else {
		org.setAttribute("IsInvOrg","N");
	}
	%>
	<tr>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" disabled="true" <%=flag?getRadioOptions("xml:/Output/OrganizationList/Organization/@IsInvOrg", "xml:/Output/OrganizationList/Organization/@IsInvOrg", "Y"):""%>/>
			<yfc:i18n>TD_Is_Inv_Org</yfc:i18n>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<yfc:i18n>TD_Get_Ext_Inv_RT</yfc:i18n>
			<input type="checkbox" disabled="true" <%=getCheckBoxOptions("xml:/Output/OrganizationList/Organization/@InventoryKeptExternally", "xml:/Output/OrganizationList/Organization/@InventoryKeptExternally", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' >
			</input>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" disabled="true" <%=flag?getRadioOptions("xml:/Output/OrganizationList/Organization/@IsInvOrg", "xml:/Output/OrganizationList/Organization/@IsInvOrg", "N"):""%>><yfc:i18n>TD_Inv_Org</yfc:i18n></input>
			<%
			String selectedInvOrg = "";
			String inInvOrg = resolveValue("xml:/Output/OrganizationList/Organization/@IsInvOrg");
			if (flag && inInvOrg.equals("N")) {
				selectedInvOrg = resolveValue("xml:/Output/OrganizationList/Organization/@InventoryOrganizationCode");
			}
			%>
			<select class="combobox" disabled="true">
				<yfc:loopOptions binding="xml:OrganizationList:/OrganizationList/@Organization" name="OrganizationName" value="OrganizationKey" selected="<%=selectedInvOrg%>" />
			</select>
		</td>
	</tr>
</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>

<script language="javascript">
function loadChildOrgs() {
	var obj = new Object();
	obj.createCustomerOrg = "N";
	obj.createCustomerSiteOrg = "N";
	obj.viewDetails = "N";
	obj.viewAudits = "N";
	obj.addChildOrg = "Y";
	obj.orgCode = "<%=resolveValue("xml:/Output/OrganizationList/Organization/@OrganizationCode")%>";
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,'TDCustSiteOrg','');
}

function confirmRemove() {
	var reply = confirm("This operation will remove the selected child organization(s). Are you sure to proceed?");
	if (reply == true) {
		return true;
	} else {
		return false;
	}
}

</script>

<table class="table" editable="false" width="100%" cellspacing="0">
	<thead>
		<tr>
            <td sortable="no" class="checkboxheader">
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
			<td class="tablecolumnheader">Org. ID</td>
			<td class="tablecolumnheader">Org. Name</td>
			<td class="tablecolumnheader">Roles</td>
		</tr>
	</thead>
	<tbody>
		<yfc:loopXML binding="xml:/Output/OrganizationList/Organization/SubOrganization/@Organization" id="child">
			<tr>
				<yfc:makeXMLInput name="OrganizationKey">
					<yfc:makeXMLKey binding="xml:/Organization/@OrganizationCode" value="xml:child:/Organization/@OrganizationCode" />
					<yfc:makeXMLKey binding="xml:/Organization/@CurrOrgCode" value="xml:/OrganizationList/@CurrOrgCode" />
                </yfc:makeXMLInput>
				<td class="checkboxcolumn" id="childList" style="display:block;">
					<input type="checkbox" value='<%=getParameter("OrganizationKey")%>' name="chkEntityKey" onclick=""/>
				</td>
				<td class="tablecolumn"><yfc:getXMLValue name="child" binding="xml:/Organization/@OrganizationCode"/></td>
				<td class="tablecolumn"><yfc:getXMLValue name="child" binding="xml:/Organization/@OrganizationName"/></td>
				<td class="tablecolumn"><yfc:getXMLValue name="child" binding="xml:/Organization/OrgRoleList/@Roles"/></td>
			</tr>
		</yfc:loopXML>		
	</tbody>
</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>

<table class="table" width="100%">
<thead>
    <tr> 
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Audit_Type</yfc:i18n>
        </td>
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Identifier</yfc:i18n>
        </td>
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Name</yfc:i18n>
        </td>
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Old_Value</yfc:i18n>
        </td>
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>New_Value</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
	<tr>
		<td class="tablecolumn">
		<%=resolveValue("xml:/AuditDetail/@AuditType")%>
		</td>
		<td class="tablecolumn">
			<yfc:loopXML binding="xml:/AuditDetail/IDs/@ID" id="ID" > 
				<yfc:getXMLValue name="ID" binding="xml:/ID/@Name"/>:&nbsp;<yfc:getXMLValue name="ID" binding="xml:/ID/@Value"/> <br/> 
			</yfc:loopXML>
		</td>
		<td/>
		<td/>
		<td/>
	</tr>
	<yfc:loopXML name="AuditDetail" binding="xml:/AuditDetail/Attributes/@Attribute" id="Attribute" > 
	<%  String sAttrName = resolveValue("xml:Attribute:/Attribute/@Name"); %>
	<% if (!equals(sAttrName, "Modifyts") && !equals(sAttrName, "Createts") && !equals(sAttrName, "Modifyprogid") && !equals(sAttrName, "Createprogid") && !equals(sAttrName, "Modifyuserid") && !equals(sAttrName, "Createuserid") && !equals(sAttrName, "TerOrgReasonCode") && !equals(sAttrName, "TerOrgReasonText") && !equals(sAttrName, "TerCustReasonCode") && !equals(sAttrName, "TerCustReasonText") && !equals(sAttrName, "Lockid")) { 
	%>
	<%	boolean bSkip = false; %>
		<yfc:loopXML binding="xml:/AuditDetail/IDs/@ID" id="ID" > 
		<%  String sID = resolveValue("xml:ID:/ID/@Name"); 
			if (equals(sID, sAttrName)) { 
				bSkip = true;
			}
		%>
		</yfc:loopXML>
	<% if (!bSkip) { %>
	<tr>
		<td/>
		<td/>
		<td class="tablecolumn"><yfc:getXMLValue name="Attribute" binding="xml:/Attribute/@Name" /></td>   
		<td class="tablecolumn"><yfc:getXMLValue name="Attribute" binding="xml:/Attribute/@OldValue" /></td> 
    	<td class="tablecolumn"><yfc:getXMLValue name="Attribute" binding="xml:/Attribute/@NewValue"/></td>
	</tr>
	<%}}%>
	</yfc:loopXML>
</tbody>
</table>

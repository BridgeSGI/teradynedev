<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" >

	function setLookupValueForCS(orgKey){
		var keys = orgKey.split('-');
		var obj = window.dialogArguments;
		if(obj != null){
			obj.field1.value=keys[0];
			obj.field2.value=keys[1];
		}
		window.close();
	}
	
	function displaybasedonValue(){
		var obj = window.dialogArguments;
		if(obj!=null && obj.lookup){
			var lists = document.all('csoList');
			if(lists != null){
				if(lists.length == null){
				    lists.style.display='none';
				}else{
					for(i=0;i<lists.length;i++){
						lists[i].style.display='none';
					}
				}
			}
		}else{
			var lookups = document.all('csoLookup');
			if(lookups != null){
				if(lookups.length == null){
				    lookups.style.display='none';
				}else{
					for(i=0;i<lookups.length;i++){
						lookups[i].style.display='none';
					}
				}
				
			}
		}		
	}
	
	function uncheckSelectedRows() {
		doCheckAll(document.getElementById('chk'));
		return true;
	}
	
	window.attachEvent('onload', displaybasedonValue);
</script>

<table class="table" editable="false" width="100%" cellspacing="0">
    <thead> 
        <tr>
            <td sortable="no" class="checkboxheader">
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader"><yfc:i18n>TD_Org_ID</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>TD_Org_Name</yfc:i18n></td>
            <td sortable="no" class="tablecolumnheader"><yfc:i18n>TD_Ship_To_Addr</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Roles</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Parent_Organization</yfc:i18n></td>
        </tr>
    </thead>
	<tbody>
		<yfc:loopXML binding="xml:/OrganizationList/@Organization" id="Organization">
			<tr>
				<yfc:makeXMLInput name="OrganizationKey">
					<yfc:makeXMLKey binding="xml:/Organization/@OrganizationKey" value="xml:Organization:/Organization/@OrganizationCode" />
                    <yfc:makeXMLKey binding="xml:/Organization/@OrganizationCode" value="xml:Organization:/Organization/@OrganizationCode" />
					<yfc:makeXMLKey binding="xml:/Organization/@CurrOrgCode" value="xml:/OrganizationList/@CurrOrgCode" />
                </yfc:makeXMLInput>
				<td class="checkboxcolumn" id="csoList" style="display:block;">
					<input type="checkbox" value='<%=getParameter("OrganizationKey")%>' name="EntityKey"/>
				</td>
				<td class="checkboxcolumn" id="csoLookup" style="display:block;">
					<img class="icon" onClick="setLookupValueForCS(this.value);" value='<%=resolveValue("xml:Organization:/Organization/@OrganizationCode")%>' <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%>/>
				</td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:Organization:/Organization/@OrganizationCode"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:Organization:/Organization/@OrganizationName"/></td>
				<td class="protectedtext" valign="top">
				<%
				String company = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@Company");
				String firstName = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@FirstName");
				String middleName = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@MiddleName");
				String lastName = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@LastName");
				
				String addressLine1 = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@AddressLine1");
				String addressLine2 = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@AddressLine2");
				String addressLine3 = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@AddressLine3");
				String addressLine4 = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@AddressLine4");
				String addressLine5 = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@AddressLine5");
				String addressLine6 = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@AddressLine6");

				String city = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@City");
				String state = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@State");
				String postalCode = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@ZipCode");
				String country = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@Country");
				
				String dayphone = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@DayPhone");
				String eveningphone = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@EveningPhone");
				String mobilephone = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@MobilePhone");
				String fax = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@DayFaxNo");
				String email = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@EMailID");
				
				String alternateEmailID = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@AlternateEmailID");
				String beeper = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@Beeper");
				String department = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@Department");
				String eveningFaxNo = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@EveningFaxNo");
				String jobTitle = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@JobTitle");
				String otherPhone = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@OtherPhone");
				String suffix = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@Suffix");
				String title = resolveValue("xml:Organization:/Organization/CorporatePersonInfo/@Title");
				if (!isVoid(company)) {%>
					<%=company%>
					<br/>
				<%
				}
				if (!isVoid(firstName)) {%>
					<%=firstName%>&nbsp;
				<%
				}
				if (!isVoid(middleName)) {%>
					<%=middleName%>&nbsp;
				<%
				}
				if (!isVoid(lastName)) {%>
					<%=lastName%>
				<%
				}
				if (!isVoid(firstName) || !isVoid(middleName) || !isVoid(lastName)) {
				%>
					<br/>
				<%
				}
				if (!isVoid(addressLine1)) {%>
					<%=addressLine1%>
					<br/>
				<%
				}
				if (!isVoid(addressLine2)) {%>
					<%=addressLine2%>
					<br/>
				<%
				}
				if (!isVoid(addressLine3)) {%>
					<%=addressLine3%>
					<br/>
				<%
				}
				if (!isVoid(addressLine4)) {%>
					<%=addressLine4%>
					<br/>
				<%
				}
				if (!isVoid(addressLine5)) {%>
					<%=addressLine5%>
					<br/>
				<%
				}
				if (!isVoid(addressLine6)) {%>
					<%=addressLine6%>
					<br/>
				<%
				}
				if (!isVoid(city)) {%>
					<%=city%> &nbsp;
				<%
				}
				if (!isVoid(state)) {%>
					<%=state%>
				<%
				}
				%>
				<br/>
				<%
				if (!isVoid(country)) {%>
					<%=country%>
				<%
				} if (!isVoid(postalCode)) {
				%>
					&nbsp;<%=postalCode%>
				<%
				}
				%>
				<br/>
				<%
				if (!isVoid(dayphone)) {%>
					<b>Day Phone: </b><%=dayphone%>
					<br/>
				<%
				}
				if (!isVoid(mobilephone)) {%>
					<b>Mobile Phone: </b><%=mobilephone%>
					<br/>
				<%
				}
				if (!isVoid(fax)) {%>
					<b>Fax: </b><%=fax%>
					<br/>
				<%
				}
				if (!isVoid(email)) {%>
					<b>Email: </b><%=email%>
				<%
				}
				%>
				</td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:Organization:/Organization/@RoleList"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:Organization:/Organization/@ParentOrganizationCode"/></td>
			</tr>
		</yfc:loopXML>	
	</tbody>
</table>

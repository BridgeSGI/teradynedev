<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/dm.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>

<%  String destNode ="";%>

<table class="table" editable="false" width="100%" cellspacing="0">
    <thead> 
        <tr>
            <td sortable="no" class="checkboxheader">
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
                <input type="hidden" name="xml:/DeliveryPlan/@DeliveryPlanKey" />
            </td>
            <td class="tablecolumnheader"><yfc:i18n>Shipment_#</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Status</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Customer Name</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Ship Date</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Signed For By</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>WayBill #</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Receiver Name</yfc:i18n></td>			
            <td class="tablecolumnheader"><yfc:i18n>Carrier Service</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>POD Status</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>POD Date Time</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Total Transit Time (Hours)</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML binding="xml:/Shipments/@Shipment" id="Shipment">
            <tr>
                <yfc:makeXMLInput name="shipmentKey">
					<yfc:makeXMLKey binding="xml:/Shipment/@ShipNode" value="xml:/Shipment/@ShipNode"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@ReceivingNode" value="xml:/Shipment/@ReceivingNode"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@DocumentType" value="xml:/Shipment/@DocumentType"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@ShipmentKey" value="xml:/Shipment/@ShipmentKey"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@ShipmentNo" 
					value="xml:/Shipment/@ShipmentNo"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@OrderNo" 
					value="xml:/Shipment/@OrderNo"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@EnterpriseCode" value="xml:/Shipment/@EnterpriseCode"> 
					</yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@BuyerOrganizationCode" value="xml:/Shipment/@BuyerOrganizationCode"> 
					</yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@SellerOrganizationCode" value="xml:/Shipment/@SellerOrganizationCode">
					</yfc:makeXMLKey>
                </yfc:makeXMLInput>
					
                <td class="checkboxcolumn"> 
                      <input type="checkbox" value='<%=getParameter("shipmentKey")%>' name="EntityKey" yfcMultiSelectCounter='<%=ShipmentCounter%>' yfcMultiSelectValue1='<%=getValue("Shipment", "xml:/Shipment/@ShipmentKey")%>' />
                </td>
                <td class="tablecolumn"><a href="javascript:showDetailForOnAdvancedList('shipment', 'TDNYOMD710', '<%=getParameter("shipmentKey")%>');">
                    <yfc:getXMLValue binding="xml:/Shipment/@ShipmentNo"/></a>
                </td>
                <td class="tablecolumn">
                    <yfc:getXMLValueI18NDB binding="xml:/Shipment/Status/@Description"/>
					<% 
					 if (equals("Y", getValue("Shipment", "xml:/Shipment/@HoldFlag"))) { %>
                        <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_shipment_is_held")%>/>
                    <% } %>
				</td>
				<!-- customer name -->
				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/Shipment/Extn/@TdynCustName"/>
				</td>

				<td class="tablecolumn"  sortValue="<%=getDateValue("xml:/Shipment/@ActualShipmentDate")%>">
							<yfc:getXMLValue binding="xml:/Shipment/@ActualShipmentDate"/>
				</td>

				<td class="tablecolumn">
					<select  <%=getComboOptions("xml:/Shipment/Extn/@ExtnPodUpdatedBy", "xml:/Shipment/Extn/@ExtnPodUpdatedBy")%>>
						<yfc:loopOptions binding="xml:PodUpdatedByCodeList:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/Shipment/Extn/@ExtnPodUpdatedBy"/>
					</select>
				</td>

				<td class="tablecolumn">
							<input type="text" <%=getTextOptions("xml:/Shipment/@TrackingNo", "xml:/Shipment/@TrackingNo")%>/>
				</td>
				<td class="tablecolumn">
							<input type="text" <%=getTextOptions("xml:/Shipment/Extn/@PODPersonName", "xml:/Shipment/Extn/@PODPersonName")%>/>
				</td>


			<td class="protectedtext">
				<%if(equals(resolveValue("xml:/Shipment/@HasOneManifestedContainer"),"Y")){ %> <yfc:getXMLValueI18NDB binding="xml:/Shipment/ScacAndService/@ScacAndServiceDesc"/>
				<%}else{%>
				<select   <%=getComboOptions("xml:/Shipment/ScacAndService/@ScacAndServiceKey", "xml:/Shipment/ScacAndService/@ScacAndServiceKey")%>>
					<yfc:loopOptions binding="xml:/ScacAndServiceList/@ScacAndService" name="ScacAndServiceDesc"
					 value="ScacAndServiceKey" selected="xml:/Shipment/ScacAndService/@ScacAndServiceKey" isLocalized="Y"/>
				</select>
				<%}%>
			</td>

				<td class="tablecolumn">
					<select  <%=getComboOptions("xml:/Shipment/Extn/@ExtnPODStatus", "xml:/Shipment/Extn/@ExtnPODStatus")%>>
						<yfc:loopOptions binding="xml:PODStatusCodeList:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/Shipment/Extn/@ExtnPODStatus"/>
					</select>
				</td>

			<td nowrap="true" class="tablecolumn" sortValue="<%=getDateValue("xml:/Shipment/@ActualDeliveryDate")%>">
				<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@ActualDeliveryDate_YFCDATE", "xml:/Shipment/@ActualDeliveryDate_YFCDATE")%>/>
				<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
				<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@ActualDeliveryDate_YFCTIME", "xml:/Shipment/@ActualDeliveryDate_YFCTIME")%>/>
				<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %> />
			</td>

				<!-- POD Date Time � Ship Date -->
				<!-- ActualDeliveryDate - ActualShipDate -->

				<td class="tablecolumn">
						<!-- <yfc:getXMLValue binding="xml:/Shipment/@ActualDeliveryDate"/> - 
						<yfc:getXMLValue binding="xml:/Shipment/@ActualShipDate"/> -->
					<%	String aShipDate =  resolveValue("xml:/Shipment/@ActualShipmentDate");
						String aDelDate =  resolveValue("xml:/Shipment/@ActualDeliveryDate");
						if(!YFCCommon.isVoid(aShipDate) && !YFCCommon.isVoid(aDelDate)){
							YFCDate date1 = new YFCDate(getDateValue("xml:/Shipment/@ActualDeliveryDate"));
							YFCDate date2 = new YFCDate(getDateValue("xml:/Shipment/@ActualShipmentDate"));
							long diff = date1.getTime() - date2.getTime();
							long diffHours = diff / (60 * 60 * 1000);
							%>
							<%=diffHours%>
					<%}else {%>
					NA
					<%}%>
				</td>

			</tr>
        </yfc:loopXML> 
   </tbody>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/dm.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script>
window.attachEvent('onload', removeExtendedDocumentTypeShipment);
</script>
<table class="view">
 <tr>
        <td>
            <input type="hidden" name="xml:/Shipment/@StatusQryType" value="BETWEEN"/>
			<input type="hidden" name="xml:/Shipment/ShipmentHoldTypes/ShipmentHoldType/@Status" value=""/>
			<input type="hidden" name="xml:/Shipment/ShipmentHoldTypes/ShipmentHoldType/@StatusQryType" value="" />
			<input type="hidden" name="xml:/Shipment/@ActualShipmentDateQryType" value="BETWEEN"/>
        </td>
    </tr>
<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
  <jsp:param name="ShowDocumentType" value="true"/>
  <jsp:param name="RefreshOnDocumentType" value="true"/>
  <jsp:param name="DocumentTypeBinding" value="xml:/Shipment/@DocumentType"/>
  <jsp:param name="EnterpriseCodeBinding" value="xml:/Shipment/@EnterpriseCode"/>
  <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
  <jsp:param name="ScreenType" value="search"/>
 </jsp:include>

    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP3"/>
    <yfc:callAPI apiID="AP4"/>   
    <%
		String docType = resolveValue("xml:CommonFields:/CommonFields/@DocumentType");
		
    %>

  <tr> 
        <td>
            <input type="hidden" name="xml:/Shipment/@StatusQryType" value="BETWEEN"/>
        </td>
    </tr>    
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Shipment_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ShipmentNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@ShipmentNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@ShipmentNo")%>/>
        </td>
    </tr>
	    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Status</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true">
            <select name="xml:/Shipment/@FromStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description" value="Status" selected="xml:/Shipment/@FromStatus" isLocalized="Y"/>
            </select>
            <span class="searchlabel" ><yfc:i18n>To</yfc:i18n></span>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Shipment/@ToStatus" class="combobox">
                <yfc:loopOptions binding="xml:/StatusList/@Status" name="Description" value="Status" selected="xml:/Shipment/@ToStatus" isLocalized="Y"/>
            </select>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Customer ID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">            
			<select name="xml:/Shipment/@BuyerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Shipment/@BuyerOrganizationCodeQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Shipment/@BuyerOrganizationCode")%>/>
        </td>
    </tr>

	<tr> 
        <td nowrap="true">
			<span class="searchlabel" ><yfc:i18n>Customer Name</yfc:i18n></span>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/Extn/@TdynCustName")%> />
		</td>
    </tr>    

	<tr>
		<td nowrap="true">
			<yfc:i18n>Ship Date</yfc:i18n>
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@FromActualShipmentDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<yfc:i18n>To</yfc:i18n>
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@ToActualShipmentDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
		</td>
	</tr>
	<tr>
		<td nowrap="true">
			<yfc:i18n>Due Date</yfc:i18n>
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@FromRequestedShipmentDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<yfc:i18n>To</yfc:i18n>
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Shipment/@ToRequestedShipmentDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
		</td>
	</tr>

	<tr>
		<td>
			<yfc:i18n>POD Status</yfc:i18n>
			<select name="xml:/Shipment/Extn/@ExtnPODStatus" class="combobox">
				<yfc:loopOptions binding="xml:PODStatusCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
				value="CodeValue" selected="xml:/Shipment/Extn/@ExtnPODStatus" />
			</select>
		</td>
	</tr>

	<tr>
		<td>
			<yfc:i18n>Carrier</yfc:i18n>
			<select name="xml:/Shipment/ScacAndService/@ScacAndServiceKey" class="combobox">
				<yfc:loopOptions binding="xml:ScacAndServiceList:/ScacAndServiceList/@ScacAndService" name="ScacAndServiceDesc" value="ScacAndServiceKey" selected="xml:/Shipment/ScacAndService/@ScacAndServiceKey" isLocalized="Y"/>
		</td>
	</tr>

    <tr>
        <td nowrap="true" >
            <span class="searchlabel" ><yfc:i18n>AWB</yfc:i18n></span>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/@TrackingNo")%>/>
        </td>
    </tr>

</table>
<input type="hidden" class="unprotectedinput" <%=getTextOptions("xml:/Shipment/ShipmentLines/ShipmentLine/@WaveNo")%>/>
<input type="hidden" name="xml:/Shipment/@OrderAvailableOnSystem" value=" "/>
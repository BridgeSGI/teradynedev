<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/dm.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>

<%  String destNode ="";
	YFCDate nowDate = new YFCDate();	
%>

<table class="table" editable="false" width="100%" cellspacing="0">
<yfc:callAPI apiID="AP1"/>
    <thead> 
        <tr>
            <td sortable="no" class="checkboxheader">
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
                <input type="hidden" name="xml:/DeliveryPlan/@DeliveryPlanKey" />
            </td>
			<td class="tablecolumnheader"><yfc:i18n>From Node</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Shipment_#</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Wave No</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Customer ID</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Customer Name</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Inco Terms</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Planner Name</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Order Type</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Control No</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Part Number</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Allocated Qty</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Due Date</yfc:i18n></td>
			<!-- <td class="tablecolumnheader"><yfc:i18n>Planner Name</yfc:i18n></td> -->
			<td class="tablecolumnheader"><yfc:i18n>Remarks</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Run date & time</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Bucketing Issue</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>No of Days Open</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Date Order is Captured</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Shipment Status</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML binding="xml:/ShipmentLines/@ShipmentLine" id="ShipmentLine">
            <tr>
                <yfc:makeXMLInput name="shipmentLineKey">
					<yfc:makeXMLKey binding="xml:/ShipmentLine/Shipment/@ShipNode" value="xml:/ShipmentLine/Shipment/@ShipNode"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/ShipmentLine/Shipment/@ReceivingNode" value="xml:/ShipmentLine/Shipment/@ReceivingNode"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/ShipmentLine/Shipment/@DocumentType" value="xml:/ShipmentLine/Shipment/@DocumentType"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/ShipmentLine/@ShipmentLineKey" value="xml:/ShipmentLine/@ShipmentLineKey"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/ShipmentLine/Shipment/@ShipmentKey" value="xml:/ShipmentLine/Shipment/@ShipmentKey"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/ShipmentLine/Shipment/@ShipmentNo" value="xml:/ShipmentLine/Shipment/@ShipmentNo"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/ShipmentLine/Shipment/@OrderNo" value="xml:/ShipmentLine/Shipment/@OrderNo"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/ShipmentLine/Shipment/@EnterpriseCode" value="xml:/ShipmentLine/Shipment/@EnterpriseCode"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/ShipmentLine/Shipment/@BuyerOrganizationCode" value="xml:/ShipmentLine/Shipment/@BuyerOrganizationCode"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/ShipmentLine/Shipment/@SellerOrganizationCode" value="xml:/ShipmentLine/Shipment/@SellerOrganizationCode"> </yfc:makeXMLKey>
                </yfc:makeXMLInput>
				<yfc:makeXMLInput name="shipmentKey">
					<yfc:makeXMLKey binding="xml:/Shipment/@ShipNode" value="xml:/ShipmentLine/Shipment/@ShipNode"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@DocumentType" value="xml:/ShipmentLine/Shipment/@DocumentType"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@ShipmentKey" value="xml:/ShipmentLine/Shipment/@ShipmentKey"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/@ShipmentNo" value="xml:/ShipmentLine/Shipment/@ShipmentNo"> </yfc:makeXMLKey>
					<yfc:makeXMLKey binding="xml:/Shipment/ShipmentLines/ShipmentLine/@ShipmentLineKey" value="xml:/ShipmentLine/@ShipmentLineKey"> </yfc:makeXMLKey>
				</yfc:makeXMLInput>

                <td class="checkboxcolumn"> 
                      <input type="checkbox" value='<%=getParameter("shipmentLineKey")%>' name="EntityKey" yfcMultiSelectCounter='<%=ShipmentLineCounter%>' yfcMultiSelectValue1='<%=getValue("ShipmentLine", "xml:/ShipmentLine/@ShipmentLineKey")%>' shipmentKey='<%=getParameter("shipmentKey")%>'/>
                </td>
				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/Shipment/@ShipNode"/>
				</td>
                <td class="tablecolumn"><a href="javascript:showDetailForOnAdvancedList('shipment', 'TDNYOMD710', '<%=getParameter("shipmentKey")%>');">
                    <yfc:getXMLValue binding="xml:/ShipmentLine/Shipment/@ShipmentNo"/></a>
                </td>
				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/@WaveNo"/>
				</td>
				<!-- customer ID -->
				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/Shipment/@BuyerOrganizationCode"/>
				</td>
				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/Shipment/Extn/@TdynCustName"/>
				</td>
				<!-- Inco Terms -->
				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/Shipment/@FreightTerms"/>
				</td>
				<td></td>
				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/Shipment/@OrderType"/>
				</td>

				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/Order/OrderLine/Extn/@ControlNo"/>
				</td>
				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/@ItemID"/>
				</td>
				<td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/@Quantity"/>
				</td>
				<td class="tablecolumn" sortValue="<%=getDateValue("xml:/ShipmentLine/Shipment/@RequestedShipmentDate")%>">
							<yfc:getXMLValue binding="xml:/ShipmentLine/Shipment/@RequestedShipmentDate"/>
				</td>

				<!-- Planner Code -->
				<!-- todo, call getItemList to get plannerName ? -->
				<!-- <td class="tablecolumn">
							<yfc:getXMLValue binding="xml:/ShipmentLine/@PlannerName"/>
				</td> -->
				<td class="tablecolumn">
							<input type="text" <%=getTextOptions("xml:/ShipmentLine/Extn/@ExtnApniRemarks", "xml:/ShipmentLine/Extn/@ExtnApniRemarks")%>/>
				</td>
				<td class="tablecolumn">
					<%= nowDate.getString(getLocale().getDateHourMinuteFormat())%>
				</td>
				<td class="tablecolumn">
					<!-- <yfc:getXMLValue binding="xml:/ShipmentLine/Extn/@ExtnBucketingIssue"/> -->
					<select  <%=getComboOptions("xml:/ShipmentLine/Extn/@ExtnBucketingIssue", "xml:/ShipmentLine/Extn/@ExtnBucketingIssue")%>>
						<yfc:loopOptions binding="xml:BucketingIssueCodeList:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/ShipmentLine/Extn/@ExtnBucketingIssue"/>
					</select>
				</td>
				<!-- no of days open -->
				<td class="tablecolumn">
				<% String sDate = resolveValue("xml:/ShipmentLine/Extn/@ExtnDatePickedupForAPNI");
				if(!YFCCommon.isVoid(sDate)){
				YFCDate datePickedupForAPNI = new YFCDate(getDateValue("xml:/ShipmentLine/Extn/@ExtnDatePickedupForAPNI"));
					if(!YFCCommon.isVoid(datePickedupForAPNI)){%>
						<%=datePickedupForAPNI.diffDays(nowDate)%>
					<%}
				}%>
				</td>


				<!-- Date Order is Captured -->
				<td class="tablecolumn" nowrap="true">
			<input class="dateinput" type="text" <%=getTextOptions("xml:/ShipmentLine/Extn/@ExtnDatePickedupForAPNI_YFCDATE", "xml:/ShipmentLine/Extn/@ExtnDatePickedupForAPNI_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="text" <%=getTextOptions("xml:/ShipmentLine/Extn/@ExtnDatePickedupForAPNI_YFCTIME", "xml:/ShipmentLine/Extn/@ExtnDatePickedupForAPNI_YFCTIME")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %> />
				</td>
                <td class="tablecolumn">
                    <yfc:getXMLValueI18NDB binding="xml:/ShipmentLine/Shipment/Status/@Description"/>
					<% 
					 if (equals("Y", getValue("ShipmentLine", "xml:/ShipmentLine/Shipment/@HoldFlag"))) { %>
                        <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_shipment_is_held")%>/>
                    <% } %>
				</td>

            </tr>
        </yfc:loopXML> 
   </tbody>
</table>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/dm.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script>
window.attachEvent('onload', removeExtendedDocumentTypeShipmentLine);
</script>
<table class="view">
 <tr>
        <td>
			<input type="hidden" name="xml:/ShipmentLine/Shipment/@RequestedShipmentDateQryType" value="BETWEEN"/>
        </td>
    </tr>
<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
  <jsp:param name="ShowDocumentType" value="true"/>
  <jsp:param name="RefreshOnDocumentType" value="true"/>
  <jsp:param name="DocumentTypeBinding" value="xml:/ShipmentLine/Shipment/@DocumentType"/>
  <jsp:param name="EnterpriseCodeBinding" value="xml:/ShipmentLine/Shipment/@EnterpriseCode"/>
  <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
  <jsp:param name="ScreenType" value="search"/>
 </jsp:include>
   
    <%
		String docType = resolveValue("xml:CommonFields:/CommonFields/@DocumentType");
    %>  

	<!-- Shipment # -->
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Shipment_#</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/ShipmentLine/Shipment/@ShipmentNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/ShipmentLine/Shipment/@ShipmentNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipmentLine/Shipment/@ShipmentNo")%>/>
        </td>
    </tr>

	<!-- Customer ID -->
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Customer ID</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">            
			<select name="xml:/ShipmentLine/Shipment/@BuyerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/ShipmentLine/Shipment/@BuyerOrganizationCodeQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/ShipmentLine/Shipment/@BuyerOrganizationCode")%>/>
        </td>
    </tr>

	<tr>
	</tr>

	<!-- Customer Name -->
	<tr> 
        <td nowrap="true">
			<span class="searchlabel" ><yfc:i18n>Customer Name</yfc:i18n></span>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipmentLine/Shipment/Extn/@TdynCustName")%> />
		</td>
    </tr>    

	<tr>
	</tr>

	<!-- Order Type -->
    <tr>
        <td nowrap="true" >
            <span class="searchlabel" ><yfc:i18n>Order Type</yfc:i18n></span>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipmentLine/Shipment/@OrderType")%>/>
        </td>
    </tr>

	<tr>
	</tr>

	<tr>
	<tr>
		<td nowrap="true">
			<yfc:i18n>Due Date</yfc:i18n>
			</td>
		</tr>

		<tr>
		<td nowrap="true">
			<input class="dateinput" type="text" <%=getTextOptions("xml:/ShipmentLine/Shipment/@FromRequestedShipmentDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="text" <%=getTextOptions("xml:/ShipmentLine/Shipment/@FromRequestedShipmentDate_YFCTIME")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" 
			<%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %> />
			<yfc:i18n>To</yfc:i18n>
		</td>
		</tr>
		<tr>
		<td>
			<input class="dateinput" type="text" <%=getTextOptions("xml:/ShipmentLine/Shipment/@ToRequestedShipmentDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="text" <%=getTextOptions("xml:/ShipmentLine/Shipment/@ToRequestedShipmentDate_YFCTIME")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" 
			<%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %> />
		</td>
		</tr>
	</tr>

	<tr>
	</tr>

<!-- Include Quantity Back Ordered From Node -->
	<tr>
		<td class="searchcriteriacell">
			<input type="checkbox" yfcCheckedValue=" " yfcUnCheckedValue="0" <%=getCheckBoxOptions("xml:/ShipmentLine/@ShortageQty", "xml:/ShipmentLine/@ShortageQty", " ")%>><yfc:i18n>Include Quantity Back Ordered From Node</yfc:i18n></input>
		</td>
	</tr>


</table>
<input type="hidden" name="xml:/ShipmentLine/Shipment/@OrderAvailableOnSystem" value=" "/>
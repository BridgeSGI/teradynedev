
var myObject = new Object();
myObject = dialogArguments;
var parentWindow = myObject.currentWindow;
var parentReasonCodeInput = myObject.reasonCodeInput;
var parentReasonTextInput = myObject.reasonTextInput;

function setOKClickedAttribute() {
	//alert("ReasonCodePopup");
	parentWindow.document.documentElement.setAttribute("OKClicked", "true");
    var reasonCodeInput = document.all("xml:/ModificationReason/@ReasonCode");
	parentReasonCodeInput.value = reasonCodeInput.value;
	var reasonTextInput = document.all("xml:/ModificationReason/@ReasonText");
    parentReasonTextInput.value = reasonTextInput.value;
	//alert(parentReasonCodeInput.value);
    window.close();
}
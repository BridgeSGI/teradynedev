	function callOrgLookupForCustomerMaster(orgID,orgName,entityname,extraParams)
	{
		var oOrgID = document.all(orgID);
		var oOrgName = document.all(orgName);
		if(extraParams == null) {
			showOrgLookupPopupForCustomerMaster(oOrgID, oOrgName, entityname);
		}
		else{
			showOrgLookupPopupForCustomerMaster(oOrgID, oOrgName, entityname, extraParams);
		}
	}
	
	function showOrgLookupPopupForCustomerMaster(orgID,orgName,entityname,extraParams)
	{
		var oObj = new Object();
		oObj.field1 = orgID;
		oObj.field2 = orgName;
		if(extraParams == null) {
			yfcShowSearchPopup('','',900,550,oObj,entityname);
		}
		else{
			yfcShowSearchPopupWithParams('','',900,550,oObj,entityname,extraParams);
		}
	}
	
	function setOrgLookupValue(orgID,orgName)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = orgID;
			if(Obj.field2 != null)
			{
				Obj.field2.value = orgName;
			}
		}
		window.close();
	}
	
	/*function showCustOrgDetailFor(key,newCustomer) {
		document.body.style.cursor='wait';
		var str = contextPath+"/console/"+entityType+".detail?EntityKey="+encodeURIComponent(key);
		str = str + "&isNewCustomerOrg=" + newCustomer;
		if (yfcIsPopup == true)
			str = str + "&Popup=Y";
			
		yfcDebug("changing to url: " + str);		
		
		window.location.href = sc.csrf.getUrlWithAdditionalParams(str);
	}*/
	
	function checkLocale(custSiteOrgID,custSiteOrgName,localecode)
	{
	
	var vcustSiteOrgID=document.all(custSiteOrgID).value;
	var vcustSiteOrgName=document.all(custSiteOrgName).value;
	var vlocalecode=document.all(localecode).value;
		
		if(vcustSiteOrgID.length<=0){
		alert('Customer-Site Org. ID is Mandatory');
		return false;
		}
		
		if(vcustSiteOrgName.length<=0){
		alert('Customer-Site Org. Name is Mandatory');
		return false;
		}
		
		if(vlocalecode.length<=0){
		alert('Please select a Locale');
		return false;
		}
	
	return true;
	}
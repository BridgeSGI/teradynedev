
function enterSCReason(reasonViewID, reasonCodeBinding, reasonTextBinding) {

    var myObject = new Object();
    myObject.currentWindow = window;
    myObject.reasonCodeInput = document.all(reasonCodeBinding);
    myObject.reasonTextInput = document.all(reasonTextBinding);

	var draftInput = document.all("hiddenDraftFlag");
    if (draftInput != null) {
        if ("Y" == draftInput.value) {
            return (true);
        }
    }

    yfcShowDetailPopup(reasonViewID, "", "550", "255", myObject);
	var ok = window.document.documentElement.getAttribute("OKClicked");
	//alert("MyObbj"+myObject.reasonCodeInput.value);
	if(ok == "true"){
		window.document.documentElement.setAttribute("OKClicked", "false");
		return (true);
	}else{
		window.document.documentElement.setAttribute("OKClicked", "false");
        return (false);
	}
	/*if (ok.equals("true")) {
		alert("OKClicked is true");
        window.document.documentElement.setAttribute("OKClicked", "false");
		alert("OKClicked is true");
		return (true);
    }
    else {
        window.document.documentElement.setAttribute("OKClicked", "false");
        return (false);
    }*/
}


function processSaveRecordsForSCTemplate(){
	//yfcSpecialChangeNames("SCTemplate", true);
	//alert("unloader");
	enterSCReason('TDSCTR','xml:/TerSerContTemplate/@TerSCTReasonCode','xml:/TerSerContTemplate/@TerSCTReasonText');
	return true;
}

function processSaveRecordsForSCTemplates(){
	yfcSpecialChangeNames("SCTemplates", false);
	return true;
}

function processSaveRecordsForServiceItems(){
	yfcSpecialChangeNames("ServiceItems", false);
	return true;
}

function validateSRCreate(sSRID){
	var srID = document.getElementById(sSRID).value;
	if(srID.length<=0){
		alert('Selection Rule ID cannot be blank.');
		return false;
	}
	else{
		return true;
	}
}

function deleteLastChild(name){
	var childNodes = window.document.getElementById("childNodes").value;
	//alert("childNodes"+childNodes);
	if(name=='SelectionRule'){
		if(childNodes == 1){
			alert("You cannot delete the last template in a selection rule. Please add another before deleting this one.");
			return false;
		}/*else{
			
			var response = enterSCReason('SCTSRR','xml:/TerSCTSelectionRule/@TerSRReasonCode','xml:/TerSCTSelectionRule/@TerSRReasonText');
			return response;			
			
		}*/
	}else{
		if(childNodes == 1){
			alert("You cannot delete the last service item on the template. Please add another before deleting this one.");
			return false;
		}/*else{
			
			var response = enterSCReason('TDSCTR','xml:/TerSerContTemplate/@TerSCTReasonCode','xml:/TerSerContTemplate/@TerSCTReasonText');
			return response;
		}*/
	}
	return true;
	

}
function confirmDelete(name){
	if(name=='SelectionRule'){
		var response = confirm("You are about to delete a selection rule. Do you want to proceed?");
	}else{
		var response = confirm("You are about to delete a template. Do you want to proceed?");
	}
	if(response == true){
		return true;
	}else{
		return false;
	}
}

function validateSCTemplateSave(){
	var draftTemplate = window.document.getElementById("hiddenDraftFlag").value;
	if(draftTemplate == "N"){
		var response = enterSCReason('TDSCTR','xml:/TerSerContTemplate/@TerSCTReasonCode','xml:/TerSerContTemplate/@TerSCTReasonText');
		return response;
	} else{
		var serviceItemsObj = window.document.getElementById("ServiceItems");
		var trNodes = serviceItemsObj.getElementsByTagName("TR");
		if(trNodes.length > 5) //5 is the number of rows present irrespective of any addition
		{	
			//var response = enterSCReason('TDSCTR','xml:/TerSerContTemplate/@TerSCTReasonCode','xml:/TerSerContTemplate/@TerSCTReasonText');
			return true;
		}
		else{
			alert("Atleast one service item should be present for a successful Save");
			return false;
		}
	}

}
function validateSelectionRuleSave(){
	
	var fromBook = document.all("xml:/TerSCTSelectionRule/@TerFromBookYw").value;
	var toBook = document.all("xml:/TerSCTSelectionRule/@TerToBookYw").value;
	var fromShip = document.all("xml:/TerSCTSelectionRule/@TerFromShipDate_YFCDATE").value;
	var toShip = document.all("xml:/TerSCTSelectionRule/@TerToShipDate_YFCDATE").value;
	//var fromIns = document.all("xml:/TerSCTSelectionRule/@TerExpFromInstallDate_YFCDATE").value;
	//var toIns = document.all("xml:/TerSCTSelectionRule/@TerExpToInstallDate_YFCDATE").value;
	
	var draftTemplate = window.document.getElementById("hiddenDraftFlag").value;
	if(draftTemplate == "N"){
		if((fromBook.length>0 &&toBook.length>0) && (fromBook > toBook)){
			alert("From Book Year/Wk value cannot be greater than To Book Year/Wk");
			return false;
		}
		if((fromShip.length>0 &&toShip.length>0) && (fromShip > toShip)){
			alert("From Ship Date cannot be greater than To Ship Date");
			return false;
		}
		/*if((fromIns.length>0 &&toIns.length>0) && (fromIns > toIns)){
			alert("From Install Date cannot be greater than To Install Date");
			return false;
		}*/
		var response = enterSCReason('SCTSRR','xml:/TerSCTSelectionRule/@TerSRReasonCode','xml:/TerSCTSelectionRule/@TerSRReasonText');
		return response;
	}else{
		var val1 = document.all("xml:/TerSCTSelectionRule/@TerOwnerOrgID").value;
		var val2 = document.all("xml:/TerSCTSelectionRule/@TerInstallNode").value;
		var val3 = document.all("xml:/TerSCTSelectionRule/@TerItemID").value;
		var val4 = document.all("xml:/TerSCTSelectionRule/@TerBuildStatus").value;
		var val5 = document.all("xml:/TerSCTSelectionRule/@TerRespProductMfgID").value;
		var val6 = document.all("xml:/TerSCTSelectionRule/@TerRespProdMfgDivCode").value;
		var val7 = document.all("xml:/TerSCTSelectionRule/@TerCountryCode").value;
		var scTemplateObj = window.document.getElementById("SCTemplates");
		var trNodes = scTemplateObj.getElementsByTagName("TR");
		if((val1.length>0||val2.length>0||val3.length>0||val4.length>0||val5.length>0||val6.length>0||
			val7.length>0||(fromBook.length>0 &&toBook.length>0)||(fromShip.length>0 &&toShip.length>0))&& trNodes.length > 5){
			if((fromBook.length>0 &&toBook.length>0) && (fromBook > toBook)){
				alert("From Book Year/Wk value cannot be greater than To Book Year/Wk");
				return false;
			}
			if((fromShip.length>0 &&toShip.length>0) && (fromShip > toShip)){
				alert("From Ship Date cannot be greater than To Ship Date");
				return false;
			}
			/*if((fromIns.length>0 &&toIns.length>0) && (fromIns > toIns)){
				alert("From Install Date cannot be greater than To Install Date");
				return false;
			}*/
			return true;
		}
		else{
			alert("Atleast one condition and one template should be present for a successful Save");
			return false;
		}
	}
	
}

function validateCreate(sTemplateID,sServiceType){
	var templateID = document.getElementById(sTemplateID).value;
	var serviceType=document.getElementById(sServiceType).value;
	if(templateID.length<=0){
		alert('Service Contract Template ID cannot be blank.');
		return false;
	}

	if(serviceType.length<=0){
		alert('Service Type cannot be blank.');
		return false;
	}
	else{
		//document.body.style.cursor='auto';
		return true;
	}
}

function setSerItemLookupValue(sItemID,sItemKey,sShortDesc,sKitCode,sUOM){
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = sItemID;
			if(Obj.field2 != null)
			{
				Obj.field2.value = sItemKey;
			}
			if(Obj.field3 != null)
			{
				Obj.field3.value = sShortDesc;
			}
			if(Obj.field4 != null)
			{
				Obj.field4.value = sKitCode;
			}
			if(Obj.field5 != null){
				Obj.field5.value = sUOM;
			}
		}
		
		window.close();
}


function showSerItemLookupPopup(itemIDInput, itemKeyInput, shortDescInput, kitCodeInput, uomInput, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = itemIDInput;
		oObj.field2 = itemKeyInput;
		oObj.field3 = shortDescInput;
		oObj.field4 =kitCodeInput;
		oObj.field5=uomInput;
		
		if(extraParams == null) {
			yfcShowSearchPopup('','itemlookup',900,550,oObj,entityname);
		}
		else{
			yfcShowSearchPopupWithParams('','itemlookup',900,550,oObj,entityname,extraParams);
		}
	}

function tempRowServiceItemLookup(imgObj, sItemIDAttr, sItemKey, sShortDesc, sKitCode, sUOM, entityname, extraParams){
		var itemIDInput = null;
		var itemKeyInput = null;
		var shortDescInput = null;
		var kitCodeInput = null;
		var uomInput = null;
		
		// get the parent TR of the image object
		var parentTR = getParentTag(imgObj, "TR");

		// loop through all input objects in this TR and find the ones matching
		// the attribute names.
		var trInputs = parentTR.getElementsByTagName("input");
		for (var i=0;i<trInputs.length;i++)	 {
			var trInput = trInputs.item(i);
			var inputName = trInput.name;
			
			var attributeName = getAttributeNameFromBinding(inputName);
			
			if (attributeName == sItemIDAttr) {
				itemIDInput = trInput;
			}
			if (attributeName == sItemKey) {
				itemKeyInput = trInput;
			}
			if (attributeName == sShortDesc) {
				shortDescInput = trInput;
			}
			if (attributeName == sKitCode) {
				kitCodeInput = trInput;
			}
		}	
		var trSelects = parentTR.getElementsByTagName("select");
		for (var i=0;i<trSelects.length;i++)	 {
			var trSelect = trSelects.item(i);
			var inputName = trSelect.name;
			var attributeName = getAttributeNameFromBinding(inputName);
			if (attributeName == sUOM) {
				uomInput = trSelect;
			}
		}
		
		if(extraParams == null) {
			showSerItemLookupPopup(itemIDInput, itemKeyInput, shortDescInput, kitCodeInput, uomInput, entityname, null);
		}
		else{
			showSerItemLookupPopup(itemIDInput, itemKeyInput, shortDescInput, kitCodeInput, uomInput,entityname, extraParams);
		}
}

function callSerItemLookup(sItemID, sItemKey, sShortDesc, sKitCode, sUOM, entityname, extraParams)
	{
		var oItemID = document.all(sItemID);
		var oItemKey = document.all(sItemKey);
		var oShortDesc = document.all(sShortDesc);
		var oKitCode = document.all(sKitCode);
		var oUOM = document.all(sUOM);
		
		if(extraParams == null) {
			showSerItemLookupPopup(oItemID, oItemKey, oShortDesc, oKitCode, oUOM, entityname, null);
		}
		else{
			showSerItemLookupPopup(oItemID, oItemKey, oShortDesc, oKitCode, oUOM,  entityname, extraParams);
		}
	}

function showSCTemplateLookupPopup(sctKeyInput, sctIDInput, sctNameInput, scTypeInput, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = sctKeyInput;
		oObj.field2 = sctIDInput;
		oObj.field3 = sctNameInput;
		oObj.field4 = scTypeInput;
		if(extraParams == null) {
			yfcShowSearchPopup('','sctlookup',900,550,oObj,entityname);
		}
		else{
			yfcShowSearchPopupWithParams('','sctlookup',900,550,oObj,entityname,extraParams);
		}
}

function tempRowSCTLookup(imgObj, sSCTKey, sSCTID, sSCTName, sSCType, entityname, extraParams){
		var sctKeyInput = null;
		var sctIDInput = null;
		var sctNameInput = null;
		var scTypeInput = null;
		// get the parent TR of the image object
		var parentTR = getParentTag(imgObj, "TR");

		// loop through all input objects in this TR and find the ones matching
		// the attribute names.
		var trInputs = parentTR.getElementsByTagName("input");
		for (var i=0;i<trInputs.length;i++)	 {
			var trInput = trInputs.item(i);
			var inputName = trInput.name;
			
			var attributeName = getAttributeNameFromBinding(inputName);
			
			if (attributeName == sSCTKey) {
				sctKeyInput = trInput;
			}
			if (attributeName == sSCTID) {
				sctIDInput = trInput;
			}
			if (attributeName == sSCTName) {
				sctNameInput = trInput;
			}
			if (attributeName == sSCType) {
				scTypeInput = trInput;
			}
		}
		if(extraParams == null) {
			showSCTemplateLookupPopup(sctKeyInput, sctIDInput, sctNameInput, scTypeInput, entityname)
			
		}
		else{
			showSCTemplateLookupPopup(sctKeyInput, sctIDInput, sctNameInput, scTypeInput, entityname, extraParams)
		}
}

function callSCTLookup(sSCTKey,sSCTID,sSCTName,sSCType,entityname,extraParams)
	{
		var oSCTKey = document.all(sSCTKey);
		var oSCTID = document.all(sSCTID);
		var oSCTName = document.all(sSCTName);
		var oSCType = document.all(sSCType);
		if(extraParams == null) {
			showSCTemplateLookupPopup(oSCTKey, oSCTID, oSCTName, oSCType, entityname);
		}
		else{
			showSCTemplateLookupPopup(oSCTKey, oSCTID, oSCTName, oSCType, entityname, extraParams);
		}
	}
	
function setSCTLookupValue(sSCTKey,sSCTID,sSCTName,sSCType){
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			if(Obj.field1 != null){
				Obj.field1.value = sSCTKey;
			}
			Obj.field2.value = sSCTID;
			if(Obj.field3 != null)
			{
				Obj.field3.value = sSCTName;
			}
			if(Obj.field4 != null)
			{
				Obj.field4.value = sSCType;
			}
		}
		window.close();
}

function showPGLookupPopup(sRespIDInput, sDivCodeInput, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = sRespIDInput;
		oObj.field2 = sDivCodeInput;
		if(extraParams == null) {
			yfcShowSearchPopup('','pglookup',900,550,oObj,entityname);
		}
		else{
			yfcShowSearchPopupWithParams('','pglookup',900,550,oObj,entityname,extraParams);
		}
}

function callPGLookup(sRespID,sDivCode,entityname,extraParams)
	{
		var oRespID = document.all(sRespID);
		var oDivCode = document.all(sDivCode);
		
		if(extraParams == null) {
			showPGLookupPopup(oRespID, oDivCode, entityname);
		}
		else{
			showPGLookupPopup(oRespID, oDivCode, entityname, extraParams);
		}
	}
	
function setPGLookupValue(sRespID,sDivCode,sSCTName){
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = sRespID;
			Obj.field2.value = sDivCode;
		}
		window.close();
}

function showTRItemLookupPopup(itemIDInput, pcInput, uomInput, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = itemIDInput;
		oObj.field2=pcInput;
		oObj.field3=uomInput;
		if(extraParams == null) {
			yfcShowSearchPopup('','itemlookup',900,550,oObj,entityname);
		}
		else{
			yfcShowSearchPopupWithParams('','itemlookup',900,550,oObj,entityname,extraParams);
		}
	}


function callTRItemLookup(sItemID, sPC, sUOM, entityname, extraParams)
	{
		var oItemID = document.all(sItemID);
		var oUOM = document.all(sUOM);
		var oPC = document.all(sPC);
		if(extraParams == null) {
			showTRItemLookupPopup(oItemID, oPC, oUOM, entityname, null);
		}
		else{
			showTRItemLookupPopup(oItemID, oPC, oUOM, entityname, extraParams);
		}
	}

function setTRItemLookupValue(sItemID,sPC,sUOM){
		
		var Obj = window.dialogArguments
		
		if(Obj != null)
		{	
			Obj.field1.value = sItemID;
			
			if(Obj.field2 != null)
			{
				Obj.field2.value = sPC;
			}
			if(Obj.field3 != null)
			{
				Obj.field3.value = sUOM;
			}
		}
		window.close();
}

function callSCOrgLookup(sOrgID,sOrgName,entityname,extraParams)
	{
		
		var oObj = new Object();
		oObj.field1 = document.all(sOrgID);
		oObj.field2 = document.all(sOrgName);
		
		if(extraParams == null) {
			yfcShowSearchPopup('','tdorgllookup',900,550,oObj,entityname);
		}
		else{
			yfcShowSearchPopupWithParams('','tdorgllookup',900,550,oObj,entityname,extraParams);
		}
	}
	
function setSCOrgLookupValue(sOrgID,sOrgName){
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = sOrgID;
			if(Obj.field2 != null){
				Obj.field2.value = sOrgName;
			}
		}
		window.close();
}

function enterSCMRWithOverridePermission(modReasonViewID, modReasonCodeBinding, modReasonTextBinding, overrideFlagBinding) {
    
    var hasOverridePermissions = document.all("userHasOverridePermissions");
    var hiddenOverride = document.all(overrideFlagBinding);
       
    if (hasOverridePermissions.getAttribute("value") == "true") {
        hiddenOverride.value = "Y"; 
    }

    if (enterSCReason(modReasonViewID, modReasonCodeBinding, modReasonTextBinding)) {
        return (true);
    }
    hiddenOverride.value = "N"; 
    return (false);
}

function validateSCCreateHeader(creationFrom){
	var val1 = document.all("xml:/Order/@SellerOrganizationCode").value;
	var val2 = document.all("xml:/Order/@BuyerOrganizationCode").value;
	var val4 = document.all("xml:/Order/Extn/@EffectiveDate_YFCDATE").value;
	if(creationFrom=="Template"){
		var val6 = document.all("xml:/Order/Extn/@SCTemplateID").value;
		if(val1.length<=0 || val2.length<=0 || val6.length<=0 || val4.length<=0){
			alert('All fields except name fields are mandatory!');
			return false;
		}
	}
	else if(creationFrom=="Order"){
		var val3 = document.all("xml:/Order/@OrderType").value;
		if(val1.length<=0 || val2.length <= 0 || val3.length <= 0 || val4.length <= 0){
			alert('All fields except name fields are mandatory!');
			return false;
		}
	} 
	return true;
}

function validateSCDraftSave(){
	var val1 = document.all("hiddenDraftOrderFlag");
	if(val1 != null && val1.value == "Y"){
		var response = checkExpirationDate();
		return response;
		
	}
	return (enterModificationReason('SCMR','xml:/Order/@ModificationReasonCode','xml:/Order/@ModificationReasonText','xml:/Order/@Override'));
}

function checkExpirationDate(){
	var val1 = document.all("xml:/Order/Extn/@ExpirationDate_YFCDATE").value;
	var val3 = document.all("xml:/Order/Extn/@ExpirationDate_YFCTIME").value;
	
	var val2 = document.all("xml:/Order/Extn/@EffectiveDate_YFCDATE").value;
	var val4 = document.all("xml:/Order/Extn/@EffectiveDate_YFCTIME").value;
	
	if(val1.length <= 0){
		alert("Expiration Date cannot be blank");
		return false;
	}
	
	dat1 = Date.parse(val1+" "+val3);
	dat2 = Date.parse(val2+" "+val4);
	
	if(dat1 < dat2){ // || ((val2 == val1) && (val3 < val4))){
		alert("Expiration Date cannot be less than Effective Date");
		return false;
	}
	return true;
}

function openSearchForIBFromSC(){
		var obj = new Object();
		obj.TerSCOHKey = document.all("xml:/TerSCIBMapping/@TerSCOHKey").value;
		obj.TerSCOLKey = document.all("xml:/TerSCIBMapping/@TerSCOLKey").value;
		yfcShowSearchPopupWithParams('','lookup',900,550,obj,'INBorder','');
}


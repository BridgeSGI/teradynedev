<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<table class="table">
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryAudit/@Modifyts")%>">
            <yfc:i18n>Activity_Date</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryAudit/@ItemID")%>">
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryAudit/@ProductClass")%>">
            <yfc:i18n>PC</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryAudit/@UnitOfMeasure")%>">
            <yfc:i18n>UOM</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryAudit/@TransactionType")%>">
            <yfc:i18n>Transaction_Type</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryAudit/@ShipNode")%>">
            <yfc:i18n>Ship_Node</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryAudit/@Quantity")%>">
            <yfc:i18n>Quantity</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="InventoryAudits" binding="xml:/InventoryAudits/@InventoryAudit" id="InventoryAudit"> 
    <tr> 
        <yfc:makeXMLInput name="inventoryAuditKey">
            <yfc:makeXMLKey binding="xml:/InventoryAudit/@InventoryAuditKey" value="xml:/InventoryAudit/@InventoryAuditKey" />
            <yfc:makeXMLKey binding="xml:/InventoryAudit/@OrganizationCode" value="xml:/InventoryAudit/@InventoryOrganizationCode" />
        </yfc:makeXMLInput>
        <td class="checkboxcolumn">
            <input type="checkbox" value='<%=getParameter("inventoryAuditKey")%>' name="EntityKey"/>
        </td>
        <td class="tablecolumn" sortValue="<%=getDateValue("xml:/InventoryAudit/@Modifyts")%>">
            <%if ( "Y".equals(request.getParameter(YFCUIBackendConsts.YFC_IN_POPUP)) ) {%>
            <a href="" onClick="showPopupDetailFor('<%=getParameter("inventoryAuditKey")%>', '','900','550',window.dialogArguments);return false;" >
                <yfc:getXMLValue name="InventoryAudit" binding="xml:/InventoryAudit/@Modifyts"/>
            </a>
            <%} else {%>
                <a href="javascript:showDetailFor('<%=getParameter("inventoryAuditKey")%>');">
                    <yfc:getXMLValue name="InventoryAudit" binding="xml:/InventoryAudit/@Modifyts"/>
                </a>
            <%}%>
        </td>
        <td class="tablecolumn">
            <yfc:getXMLValue name="InventoryAudit" binding="xml:/InventoryAudit/@ItemID"/>
        </td>
        <td class="tablecolumn">
            <yfc:getXMLValue name="InventoryAudit" binding="xml:/InventoryAudit/@ProductClass"/>
        </td>
        <td class="tablecolumn">
            <yfc:getXMLValue name="InventoryAudit" binding="xml:/InventoryAudit/@UnitOfMeasure"/>
        </td>
        <td class="tablecolumn">
            <yfc:getXMLValue name="InventoryAudit" binding="xml:/InventoryAudit/@TransactionType"/>
        </td>
        <td class="tablecolumn">
            <yfc:getXMLValue name="InventoryAudit" binding="xml:/InventoryAudit/@ShipNode"/>
        </td>
		<td class="numerictablecolumn" sortValue="<%=getNumericValue("xml:InventoryAudit:/InventoryAudit/@Quantity")%>">
            <yfc:getXMLValue name="InventoryAudit" binding="xml:/InventoryAudit/@Quantity"/>
        </td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
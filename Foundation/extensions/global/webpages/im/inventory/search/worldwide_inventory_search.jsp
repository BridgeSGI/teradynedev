<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@page import="com.yantra.yfs.ui.backend.*"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<table width="100%" class="view">

    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="EnterpriseCodeBinding" value="xml:/InventoryItem/@OrganizationCode"/>
        <jsp:param name="EnterpriseCodeLabel" value="Organization"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
        <jsp:param name="ShowDocumentType" value="false"/>
        <jsp:param name="OrganizationListForInventory" value="true"/>
    </jsp:include>
    <% // Now call the APIs that are dependent on the common fields (Organization Code) %>
    <yfc:callAPI apiID="AP2"/>
	<yfc:callAPI apiID="AP3"/>
	<yfc:callAPI apiID="AP4"/>
<tr>
    <td class="searchlabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
</tr>
<tr>
     <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/InventoryItem/@ItemIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/InventoryItem/@ItemIDQryType"/>
        </select>
		<% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode"));%>
        <input type="text" class="unprotectedinput" name="xml:/InventoryItem/@ItemID" <%=getTextOptions("xml:/InventoryItem/@ItemID")%> />
		<img class="lookupicon" 
		onclick="callTRItemLookup('xml:/InventoryItem/@ItemID','','xml:/InventoryItem/@UnitOfMeasure','TRitem','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TRitem") %> />
    </td>
</tr>
<tr>
    <td  class="searchlabel" ><yfc:i18n>Unit_Of_Measure</yfc:i18n></td>
</tr>
<tr>
	<td class="searchcriteriacell" nowrap="true">
        <select name="xml:/InventoryItem/@UnitOfMeasure" class="combobox" >
            <yfc:loopOptions binding="xml:UnitOfMeasure:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure" value="UnitOfMeasure" selected="<%=getSearchCriteriaValueWithDefaulting("xml:/InventoryItem/@UnitOfMeasure","EA")%>" />
        </select>
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Breakout A1's</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/InventoryItem/@BreakoutA1" class="combobox" >
            <yfc:loopOptions binding="xml:BreakoutA1:/CommonCodeList/@CommonCode" 
                name="CodeValue" value="CodeValue" selected="xml:/InventoryItem/@BreakoutA1"/>
        </select>
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Distribution Rule ID</yfc:i18n></td>
</tr>
<tr>
	<td>
        <select class="combobox" name="xml:/InventoryItem/@DistributionRuleId" >
            <yfc:loopOptions binding="xml:DistributionRuleList:/DistributionRuleList/@DistributionRule" 
              name="Description" value="DistributionRuleId" selected="xml:/InventoryItem/@DistributionRuleId"/>
        </select>
    </td>
</tr>
</table>

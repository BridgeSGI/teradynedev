<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/im/inventory/detail/inventory_detail_shipnodelist_include.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<table width="100%" class="table" editable="false">
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader" sortable="no"><br/></td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/@ShipNode")%>">
            <yfc:i18n>Ship_Node</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/@ExtnPlan")%>">
            <yfc:i18n>Plan Level</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/@ExtnPlatinumPlan")%>">
            <yfc:i18n>Platinum Plan Level</yfc:i18n>
        </td>
        <td class="tablecolumnheader"  style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/@TotalSupply")%>">
            <yfc:i18n>Supply</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/@TotalDemand")%>">
            <yfc:i18n>Demand</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/@Tracked")%>">
            <yfc:i18n>Tracked</yfc:i18n>
        </td>
        <td class="tablecolumnheader" sortable="no">
            <table width="100%" class="table">
                <tr>
                    <td width="5%"><yfc:i18n>Supply</yfc:i18n></td>
                    <td valign="middle">
                        <table bgcolor=<%=supplyColor%> border="1" style="border-color:Black;width:30px">
                        <tr>
                            <td></td>
                        </tr>
                        </table>
                    </td>
                    <td width="5%"><yfc:i18n>Demand&nbsp;</yfc:i18n>&nbsp;</td>
                    <td valign="middle">
                        <table bgcolor=<%=demandColor%> border="1" style="border-color:Black;width:30px">
                        <tr>
                            <td></td>
                        </tr>
                        </table>
                    </td>
                    <td width="75%"></td>
                </tr>
            </table>
        </td>
    </tr>
</thead>	
<tbody>
    <yfc:loopXML name="ShipNodeInventory" binding="xml:/ShipNodeInventory/Item/ShipNodes/@ShipNode" id="shipnode"> 
	
    <tr> 
        <yfc:makeXMLInput name="nodeKey">
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ItemID" value="xml:/ShipNodeInventory/Item/@ItemID" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@UnitOfMeasure" value="xml:/ShipNodeInventory/Item/@UnitOfMeasure" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ProductClass" value="xml:/ShipNodeInventory/Item/@ProductClass" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@OrganizationCode" value="xml:/ShipNodeInventory/Item/@OrganizationCode" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@EndDate" value="xml:/ShipNodeInventory/Item/@EndDate" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ShipNode" value="xml:shipnode:/ShipNode/@ShipNode" />
        </yfc:makeXMLInput>
        <td class="checkboxcolumn">
            <input type="checkbox" name="newEntityKey" value="<%=getParameter("nodeKey")%>"/>
        </td>
        <td valign="top">
            <% if(equals(resolveValue("xml:shipnode:/ShipNode/@ExternalNode"), "Y")) {%>
                <img <%=getImageOptions(YFSUIBackendConsts.EXTERNAL_NODE, "External_Node")%>/>
            <% } else { %>
                <img <%=getImageOptions(YFSUIBackendConsts.OWN_NODE, "Internal_Node")%>/>
            <% } %>
        </td>
        <td class="tablecolumn" width="9%">
            <a <%=getDetailHrefOptions("L01",getParameter("nodeKey"),"")%>>
                <yfc:getXMLValue name="shipnode" binding="xml:/ShipNode/@ShipNode"/>
            </a>
        </td>
		<td class="tablecolumn" width="5%">  
			<yfc:getXMLValue name="shipnode" binding="xml:/ShipNode/@ExtnPlanNo" />
        </td>
        <td class="tablecolumn" width="5%">
            <yfc:getXMLValue name="shipnode" binding="xml:/ShipNode/@ExtnPlatinumPlan" />
        </td>
        <td class="numerictablecolumn" width="18%" sortValue="<%=getNumericValue("xml:shipnode:/ShipNode/@TotalSupply")%>">
            <yfc:getXMLValue name="shipnode" binding="xml:/ShipNode/@TotalSupply" />
            <% if ( (getDoubleFromLocalizedString(getLocale(),resolveValue("xml:shipnode:/ShipNode/@TotalSupply"))) > 0) {%>
                <img onclick="showATPDiv('s'+'<%=shipnodeCounter%>','<%=getI18N("Click_To_Expand")%>','<%=getI18N("Click_To_Collapse")%>','<%=YFSUIBackendConsts.FOLDER_COLLAPSE%>','<%=YFSUIBackendConsts.FOLDER_EXPAND%>')" <%=getImageOptions(YFSUIBackendConsts.FOLDER,"Click_to_Expand")%> />
            <%}%>
            <% String sSupplyID = "s"+shipnodeCounter.intValue() ;%>
            <div id=<%=sSupplyID%> style="display:none;padding-top:5px">
                <table width="100%" class="simpletable" cellspacing="0"  cellpadding="0">
            	<thead>
                    <tr>
                        <td class="tablecolumnheadernosort" style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/Supplies/InventorySupply/@SupplyType")%>">
                            <yfc:i18n>Supply_Type</yfc:i18n>
                        </td>
                        <td class="tablecolumnheadernosort" style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/Supplies/InventorySupply/@Quantity")%>">
                            <yfc:i18n>Quantity</yfc:i18n>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <yfc:loopXML name="shipnode" binding="xml:/ShipNode/Supplies/@InventorySupplyType" id="supply" > 
                    <tr>
                        <yfc:makeXMLInput name="supplyTypeKey">
                            <yfc:makeXMLKey binding="xml:/InventorySupply/@ItemID" value="xml:/ShipNodeInventory/Item/@ItemID"/>
                            <yfc:makeXMLKey binding="xml:/InventorySupply/@UnitOfMeasure" value="xml:/ShipNodeInventory/Item/@UnitOfMeasure"/>
                            <yfc:makeXMLKey binding="xml:/InventorySupply/@ProductClass" value="xml:/ShipNodeInventory/Item/@ProductClass"/>
                            <yfc:makeXMLKey binding="xml:/InventorySupply/@OrganizationCode" value="xml:/ShipNodeInventory/Item/@OrganizationCode"/>
                            <yfc:makeXMLKey binding="xml:/InventorySupply/@ShipNode" value="xml:shipnode:/ShipNode/@ShipNode"/>
                            <yfc:makeXMLKey binding="xml:/InventorySupply/@SupplyType" value="xml:supply:/InventorySupplyType/@SupplyType"/>
                            <yfc:makeXMLKey binding="xml:/InventorySupply/@EndDate" value="xml:/ShipNodeInventory/Item/@ShipDate"/>
                            <yfc:makeXMLKey binding="xml:/InventorySupply/@DistributionRuleId" value="xml:/ShipNodeInventory/Item/@DistributionRuleId"/>
                            <yfc:makeXMLKey binding="xml:/InventorySupply/@ConsiderAllNodes" value="xml:/ShipNodeInventory/Item/@ConsiderAllNodes"/>
							<yfc:makeXMLKey binding="xml:/InventorySupply/@ConsiderAllSegments" value="xml:/ShipNodeInventory/Item/@ConsiderAllSegments"/>
							<yfc:makeXMLKey binding="xml:/InventorySupply/@SegmentType" value="xml:/ShipNodeInventory/Item/@SegmentType"/>
							<yfc:makeXMLKey binding="xml:/InventorySupply/@Segment" value="xml:/ShipNodeInventory/Item/@Segment"/>
                        </yfc:makeXMLInput>
                        <td class="tablecolumn">
							<%=getComboText("xml:SupplyTypeList:/InventorySupplyTypeList/@InventorySupplyType","Description","SupplyType",resolveValue("xml:supply:/InventorySupplyType/@SupplyType"),true)%>
                        </td>
                        <td class="numerictablecolumn">
                            <a <%=getDetailHrefOptions("L02",getParameter("supplyTypeKey"),"")%>>
                                <yfc:getXMLValue name="supply" binding="xml:/InventorySupplyType/@Quantity" />
                            </a>
                        </td>
                    </tr>
                    </yfc:loopXML>
                </tbody>
                </table>
            </div>
        </td>
        <td class="numerictablecolumn" width="18%" sortValue="<%=getNumericValue("xml:shipnode:/ShipNode/@TotalDemand")%>">
            <yfc:getXMLValue name="shipnode" binding="xml:/ShipNode/@TotalDemand" />
            <% if ((getDoubleFromLocalizedString(getLocale(),resolveValue("xml:shipnode:/ShipNode/@TotalDemand"))) > 0) {%>
                <img onclick="showATPDiv('d'+'<%=shipnodeCounter%>','<%=getI18N("Click_To_Expand")%>','<%=getI18N("Click_To_Collapse")%>','<%=YFSUIBackendConsts.FOLDER_COLLAPSE%>','<%=YFSUIBackendConsts.FOLDER_EXPAND%>')" <%=getImageOptions(YFSUIBackendConsts.FOLDER,"Click_to_Expand")%> />
            <%}%>
            <% String sDemandID = "d"+shipnodeCounter.intValue() ;%>
            <div id=<%=sDemandID%> style="display:none;padding-top:5px">
                <table width="100%"  class="simpletable" cellspacing="0"  cellpadding="0">
                <thead>
                    <tr>
                        <td class="tablecolumnheadernosort" style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/Demands/InventoryDemand/@DemandType")%>">
                            <yfc:i18n>Demand_Type</yfc:i18n>
                        </td>
                        <td class="tablecolumnheadernosort" style="width:<%= getUITableSize("xml:/ShipNodeInventory/Item/ShipNodes/ShipNode/Demands/InventoryDemand/@Quantity")%>">
                            <yfc:i18n>Quantity</yfc:i18n>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <yfc:loopXML name="shipnode" binding="xml:/ShipNode/Demands/@InventoryDemandType" id="demand"> 
                    <tr>
                        <td class="tablecolumn">
							<%=getComboText("xml:DemandTypeList:/InventoryDemandTypeList/@InventoryDemandType","Description","DemandType",resolveValue("xml:demand:/InventoryDemandType/@DemandType"),true)%>
                        </td>
                        <td class="numerictablecolumn">
                            <a href="" onclick="showDemandList('<%=resolveValue("xml:/ShipNodeInventory/Item/@ItemID")%>', '<%=resolveValue("xml:/ShipNodeInventory/Item/@UnitOfMeasure")%>', '<%=resolveValue("xml:/ShipNodeInventory/Item/@ProductClass")%>', '<%=resolveValue("xml:/ShipNodeInventory/Item/@OrganizationCode")%>', '<%=resolveValue("xml:demand:/InventoryDemandType/@DemandType")%>', '<%=resolveValue("xml:shipnode:/ShipNode/@ShipNode")%>','',
							'<%=resolveValue("xml:/ShipNodeInventory/Item/@ShipDate")%>',
							'<%=resolveValue("xml:/ShipNodeInventory/Item/@DistributionRuleId")%>',
							'<%=resolveValue("xml:/ShipNodeInventory/Item/@ConsiderAllNodes")%>','BETWEEN'
							);return false;">
                               <yfc:getXMLValue name="demand" binding="xml:/InventoryDemandType/@Quantity" />
                            </a>
                        </td>
                    </tr>
                    </yfc:loopXML>
                </tbody>
                </table>
            </div>
        </td>
        <td class="tablecolumn" width="5%">
            <%=displayFlagAttribute(getValue("shipnode","xml:/ShipNode/@Tracked"))%>
        </td>
        <td width="50%" valign="top">
            <table  cellspacing="0" cellpadding="0"  style="border-color:Black;borderCollapse:collapse" border="1">
            <tr>
                <td bgcolor="<%=supplyColor%>" style=<%=getPixels(iMax,resolveValue("xml:shipnode:/ShipNode/@TotalSupply"))%>></td>
            </tr>
            </table>
            <table  cellspacing="0" cellpadding="0"  style="border-color:Black;borderCollapse:collapse" border="1">
            <tr>
                <td bgcolor="<%=demandColor%>" style=<%=getPixels(iMax,resolveValue("xml:shipnode:/ShipNode/@TotalDemand"))%>></td>
            </tr>
            </table>
        </td>
    </tr>
    </yfc:loopXML>
</tbody>	
</table>
<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>

<%
    String acrossAllRules = resolveValue("xml:/ShipNodeInventory/Item/@ConsiderAllNodes");
	String acrossAllSegments = resolveValue("xml:/ShipNodeInventory/Item/@ConsiderAllSegments");
	String segmentType = resolveValue("xml:/ShipNodeInventory/Item/@SegmentType");
	String segment = resolveValue("xml:/ShipNodeInventory/Item/@Segment");
	if (equals(acrossAllSegments,"N")) {
		if ((isVoid(segmentType)) && (isVoid(segment))) {
			acrossAllSegments = " ";
		}
	}
%>

<table width="100%" border="0" cellpadding="0" cellSpacing="7px">
    <tr>
        <td width="50%" height="100%">
            <table class="view" width="100%">
				<tr>
					<td class="detaillabel" ><yfc:i18n>Organization_Code</yfc:i18n></td>
					<td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeInventory/Item/@OrganizationCode"></yfc:getXMLValue></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
                <tr>
                    <td class="detaillabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
                    <td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeInventory/Item/@ItemID"></yfc:getXMLValue></td>
                    <td class="detaillabel" ><yfc:i18n>Product_Class</yfc:i18n></td>
                    <td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeInventory/Item/@ProductClass"></yfc:getXMLValue></td>
                </tr>
                <tr>
                    <td class="detaillabel" ><yfc:i18n>Unit_Of_Measure</yfc:i18n></td>
                    <td class="protectedtext"><yfc:getXMLValue binding="xml:/ShipNodeInventory/Item/@UnitOfMeasure"></yfc:getXMLValue></td>
                    <td class="detaillabel" ><yfc:i18n>Tracked_Everywhere</yfc:i18n></td>
                    <td class="protectedtext"><%=displayFlagAttribute(getValue("ShipNodeInventory","xml:/ShipNodeInventory/Item/@TrackedEverywhereFlag"))%></td>
                </tr>
                <tr>
                    <td class="detaillabel"><yfc:i18n>Description</yfc:i18n></td>
                    <td class="protectedtext" colspan="3"><yfc:getXMLValue binding="xml:/ShipNodeInventory/Item/PrimaryInformation/@ShortDescription"></yfc:getXMLValue></td>
                </tr>
            </table>
        </td>
        <td width="50%" height="100%" valign="top" style="border:1px ridge black">
            <table class="view" width="100%">
				<% if (!isShipNodeUser()) { %>
                <tr>
					 <td>
                        <input type="radio" onclick="setDistributionState('xml:/ShipNodeInventory/Item/@DistributionRuleId', '')" <%=getRadioOptions("xml:/ShipNodeInventory/Item/@ConsiderAllNodes", "xml:/ShipNodeInventory/Item/@ConsiderAllNodes", "Y")%>><yfc:i18n>Consider_All_Nodes</yfc:i18n>
                    </td>
					 <td>
                        <input type="radio" onclick="setDistributionState('xml:/ShipNodeInventory/Item/@DistributionRuleId', 'true')" <%=getRadioOptions("xml:/ShipNodeInventory/Item/@ConsiderAllNodes", "xml:/ShipNodeInventory/Item/@ConsiderAllNodes", "N")%>><yfc:i18n>Distribution_Group</yfc:i18n>
                    </td>
                    <td nowrap="true">
                        <select class="combobox" name="xml:/ShipNodeInventory/Item/@DistributionRuleId" <%if (equals(acrossAllRules, "Y")) { %> disabled="true" <% } %>>
                            <yfc:loopOptions binding="xml:/DistributionRuleList/@DistributionRule" 
                                name="Description" value="DistributionRuleId" selected="<%=getSearchCriteriaValueWithDefaulting("xml:/ShipNodeInventory/Item/@DistributionRuleId","Centers")%>" />
                        </select>
                    </td>
                </tr>
				<%}%>
				<tr>
					<td colspan="3">
						<fieldset>
							<legend><yfc:i18n>Segmentation</yfc:i18n></legend> 
								<table class="view" width="100%">
									<tr>
										<td>
											<input type="radio" onclick="setSegmentState('xml:/ShipNodeInventory/Item/@SegmentType','xml:/ShipNodeInventory/Item/@Segment','true')" <%=getRadioOptions("xml:/ShipNodeInventory/Item/@ConsiderAllSegments", acrossAllSegments, "Y")%>><yfc:i18n>All_Inventory</yfc:i18n>
										</td>
										<td>
											<input type="radio" onclick="setSegmentState('xml:/ShipNodeInventory/Item/@SegmentType','xml:/ShipNodeInventory/Item/@Segment','true')" <%=getRadioOptions("xml:/ShipNodeInventory/Item/@ConsiderAllSegments", acrossAllSegments, " ")%>><yfc:i18n>Unsegmented_Inventory</yfc:i18n>
										</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td nowrap="true">
											<input type="radio" onclick="setSegmentState('xml:/ShipNodeInventory/Item/@SegmentType','xml:/ShipNodeInventory/Item/@Segment', '')" <%=getRadioOptions("xml:/ShipNodeInventory/Item/@ConsiderAllSegments", acrossAllSegments, "N")%>><yfc:i18n>Consider_Segment_Type</yfc:i18n>
										</td>
										<td>
											<select <%if (!equals(acrossAllSegments,"N")) { %> disabled="true" <% } %> <%=getComboOptions("xml:/ShipNodeInventory/Item/@SegmentType")%> class="combobox" >
												<yfc:loopOptions binding="xml:SegmentTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/ShipNodeInventory/Item/@SegmentType" isLocalized="Y"/>
											</select>
										</td>
										<td class="numericprotectedtext">
											<yfc:i18n>Segment</yfc:i18n>
											<input <%if (!equals(acrossAllSegments,"N")) { %> disabled="true" <% } %> type="text" class="unprotectedinput" <%=getTextOptions("xml:/ShipNodeInventory/Item/@Segment") %> />
										</td>											
									</tr>
								</table>
						</fieldset>
					</td>
				</tr>
                <tr>
                    <td>&nbsp;&nbsp;<yfc:i18n>Horizon_End_Date</yfc:i18n></td>
                    <td nowrap="true">
                        <input type="text" class="dateinput" onkeydown="return checkKeyPress(event)" <%=getTextOptions("xml:/ShipNodeInventory/Item/@ShipDate","xml:/ShipNodeInventory/Item/@ShipDate","")%> />
                        <img class="lookupicon" onclick="invokeCalendar(this);return false;"  <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON,"View_Calendar")%> />        
                        <input type="button" class="button" value="<%=getI18N("GO")%>"  onclick="if(validateControlValues())yfcChangeDetailView(getCurrentViewId())"/>
                    </td>
					<td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

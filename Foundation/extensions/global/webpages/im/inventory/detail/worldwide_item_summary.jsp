<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<table class="view" width="100%" style="align:left">
	<tr>
        <td class="detaillabel">
            <yfc:i18n>CPS Owned at Customer Site</yfc:i18n>
        </td>
        <td class="protectedtext"><yfc:getXMLValue name="Item" binding="xml:/Item/@CPSOwned"/></td>
        <td class="detaillabel">
            <yfc:i18n>Customer owned at CPS repair</yfc:i18n>
        </td>
        <td class="protectedtext"><yfc:getXMLValue name="Item" binding="xml:/Item/@CustomerOwned"/></td>
    </tr>
    <tr>
        <td class="detaillabel">
            <yfc:i18n>CPS Owned at Taiwan Site</yfc:i18n> 
        </td>
		<td class="protectedtext"><yfc:getXMLValue name="Item" binding="xml:/Item/@CPSOwnedAtTaiwan"/></td>
        <td class="detaillabel">
            <yfc:i18n>Taiwan owned at CPS repair</yfc:i18n> 
        </td>
        <td class="protectedtext"><yfc:getXMLValue name="Item" binding="xml:/Item/@CustomerOwnedAtTaiwan"/></td>
    </tr>
</table>
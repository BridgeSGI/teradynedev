<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<table class="view" width="100%" >
	<tr>
		<td class="detaillabel"><yfc:i18n>Item ID</yfc:i18n></td>
        <td class="protectedtext"><yfc:getXMLValue name="Item" binding="xml:/Item/@ItemID"/></td>
		<td class="detaillabel"><yfc:i18n>Description</yfc:i18n></td>
        <td class="protectedtext"><yfc:getXMLValue name="Item" binding="xml:/Item/@ShortDescription" /></td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>Total PUP</yfc:i18n></td>
		<td class="protectedtext"><yfc:getXMLValue name="Item" binding="xml:/Item/@PUP"/></td>
        <td class="detaillabel"><yfc:i18n>Distribution Rule ID</yfc:i18n></td>
        <td class="protectedtext"><yfc:getXMLValue name="Item" binding="xml:/Item/@DistributionRuleId"/></td>
    </tr>
</table>
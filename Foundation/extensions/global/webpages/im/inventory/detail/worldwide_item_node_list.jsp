<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<table class="table" border="0" cellspacing="0" width="100%">
<thead>
	 <tr>
		<td class="tablecolumnheader" >
		</td>
		<td class="tablecolumnheader" colspan="3" style="text-align:center">
			<yfc:i18n>On Hand</yfc:i18n>
		</td>
		<td class="tablecolumnheader" colspan="3" style="text-align:center" >
			<yfc:i18n>On Order</yfc:i18n>
		</td>
		<td class="tablecolumnheader" >
		</td>
		<td class="tablecolumnheader" colspan="4" style="text-align:center" >
			<yfc:i18n>In Transit</yfc:i18n>
		</td>
		<td class="tablecolumnheader" >
		</td>
		<td class="tablecolumnheader" >
		</td>
	</tr>
     <tr> 
        <td class="tablecolumnheader" nowrap="true" >
            <yfc:i18n>Node</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  >
			<yfc:i18n>Plan</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true"  >
			<yfc:i18n>Good</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true"  > 
			<yfc:i18n>Bad</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true"  >
			<yfc:i18n>ALC</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true" >
			<yfc:i18n>BO</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true" >
			<yfc:i18n>PO</yfc:i18n>
		</td>
		<td class="tablecolumnheader" >
            <yfc:i18n>Open WO</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  >
			<yfc:i18n>In Good</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true" >
			<yfc:i18n>Out Good</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true" >
			<yfc:i18n>In Bad</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true" >
			<yfc:i18n>Out Bad</yfc:i18n>
		</td>
		<td class="tablecolumnheader" nowrap="true" >
            <yfc:i18n>Usage (270 Days)</yfc:i18n>
        </td>
        <td class="tablecolumnheader"  nowrap="true"  >
            <yfc:i18n>%EMS</yfc:i18n>
        </td>
		
    </tr>
</thead>
<tbody>
	<%YFCElement root = (YFCElement)request.getAttribute("Item");
	//System.out.println(root.toString());%>
    <yfc:loopXML name="Item" binding="xml:/Item/ShipNodes/@ShipNode" id="ShipNode" keyName="ShipNode" > 
    <tr> 
        <td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@ShipNode"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@PlanNo"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@GoodOnhandQty"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@BadOnhandQty"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@Allocated" /></td>
        <td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@Backordered" /></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@POQty" /></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@OpenWO" /></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@GoodInTransit" /></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@GoodOutTransit" /></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@BadInTransit" /></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@BadOutTransit" /></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@Usage" /></td>
		<td class="tablecolumn"><yfc:getXMLValue name="ShipNode" binding="xml:/ShipNode/@EMSPercentage" /></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
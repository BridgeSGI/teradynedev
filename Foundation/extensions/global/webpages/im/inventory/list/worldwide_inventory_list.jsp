<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<table class="table" border="0" cellspacing="0" width="100%">
<thead>
    <tr> 
        <td class="tablecolumnheader" nowrap="true" >
            <yfc:i18n>Node</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  >
			<yfc:i18n>Plan</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true"  >
			<yfc:i18n>Good</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true"  > 
			<yfc:i18n>Bad</yfc:i18n>
		</td>
		<td class="tablecolumnheader"  nowrap="true"  >
            <yfc:i18n>On Order</yfc:i18n>
			<!--
			<tr>
				<td class="tablecolumnheader"  nowrap="true"  
				>
				<yfc:i18n>ALC</yfc:i18n>
				</td>
				<td class="tablecolumnheader"  nowrap="true"  
				>
				<yfc:i18n>BO</yfc:i18n>
				</td>
				<td class="tablecolumnheader"  nowrap="true"  
				>
				<yfc:i18n>PO</yfc:i18n>
				</td>
			</tr>-->
        </td>
		<td class="tablecolumnheader" >
            <yfc:i18n>Open WO</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  >
            <yfc:i18n>In Transit</yfc:i18n>
			<!--
			<tr>
				<td class="tablecolumnheader"  nowrap="true"  
				>
				<yfc:i18n>In Good</yfc:i18n>
				</td>
				<td class="tablecolumnheader"  nowrap="true"  
				>
				<yfc:i18n>Out Good</yfc:i18n>
				</td>
				<td class="tablecolumnheader"  nowrap="true"  
				>
				<yfc:i18n>In Bad</yfc:i18n>
				</td>
				<td class="tablecolumnheader"  nowrap="true"  
				>
				<yfc:i18n>Out Bad</yfc:i18n>
				</td>
			</tr>-->
        </td>
		<td class="tablecolumnheader" nowrap="true" >
            <yfc:i18n>Usage (270 Days)</yfc:i18n>
        </td>
        <td class="tablecolumnheader"  nowrap="true"  >
            <yfc:i18n>%EMS</yfc:i18n>
        </td>
		
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="SerialList" binding="xml:/SerialList/@Serial" id="Serial"  keyName="GlobalSerialKey" > 
    <tr> 
        <yfc:makeXMLInput name="inventoryItemKey">
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ItemID" value="xml:/Serial/InventoryItem/@ItemID" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@UnitOfMeasure" value="xml:/Serial/InventoryItem/@UnitOfMeasure" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ProductClass" value="xml:/Serial/InventoryItem/@ProductClass" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@OrganizationCode" value="xml:/Serial/@OrganizationCode" />
			<% if(isShipNodeUser()) { %>
				<yfc:makeXMLKey binding="xml:/InventoryItem/@ShipNode" value="xml:CurrentUser:/User/@Node" />
			<%}%>
        </yfc:makeXMLInput>
        <td class="checkboxcolumn">
            <input type="checkbox" value='<%=getParameter("inventoryItemKey")%>' name="EntityKey"/>
			<input type="hidden" name='ItemID_<%=SerialCounter%>' value='<%=resolveValue("xml:/Serial/InventoryItem/@ItemID")%>' />
			<input type="hidden" name='UOM_<%=SerialCounter%>' value='<%=resolveValue("xml:/Serial/InventoryItem/@UnitOfMeasure")%>' />
			<input type="hidden" name='PC_<%=SerialCounter%>' value='<%=resolveValue("xml:/Serial/InventoryItem/@ProductClass")%>' />
			<input type="hidden" name='OrgCode_<%=SerialCounter%>' value='<%=resolveValue("xml:/Serial/@OrganizationCode")%>' />
        </td>
        <td class="tablecolumn">
            <a onclick="javascript:showDetailFor('<%=getParameter("inventoryItemKey") %>');return false;" href=""><yfc:getXMLValue name="Serial" binding="xml:/Serial/InventoryItem/@ItemID"/></a>
        </td>
		<td class="tablecolumn"><yfc:getXMLValue name="Serial" binding="xml:/Serial/@SerialNo"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="Serial" binding="xml:/Serial/InventoryItem/@ProductClass"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="Serial" binding="xml:/Serial/InventoryItem/PrimaryInformation/@Description"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="Serial" binding="xml:/Serial/@ShipNode"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="Serial" binding="xml:/Serial/@CountryOfOrigin"/></td>
        
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
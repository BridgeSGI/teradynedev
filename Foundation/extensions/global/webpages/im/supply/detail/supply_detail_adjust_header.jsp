<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>


<table width="100%" class="view">

<tr>
    <td class="detaillabel" >
        <yfc:i18n>Item_ID</yfc:i18n>
    </td>
    <td class="protectedtext">
        <%=resolveValue("xml:/InventoryItem/@ItemID")%>
        <input type="hidden" class="protectedinput" <%=getTextOptions("xml:/Items/Item/@ItemID","xml:/InventoryItem/@ItemID")%> />
    </td>
    <td class="detaillabel" >
        <yfc:i18n>Product_Class</yfc:i18n>
    </td>
    <td class="protectedtext">
        <%=resolveValue("xml:/InventoryItem/@ProductClass")%>
        <input type="hidden" class="protectedinput" <%=getTextOptions("xml:/Items/Item/@ProductClass","xml:/InventoryItem/@ProductClass")%> />
    </td>
    <td class="detaillabel" >
        <yfc:i18n>Unit_Of_Measure</yfc:i18n>
    </td>
    <td class="protectedtext">
        <%=resolveValue("xml:/InventoryItem/@UnitOfMeasure")%>
        <input type="hidden" class="protectedinput" <%=getTextOptions("xml:/Items/Item/@UnitOfMeasure","xml:/InventoryItem/@UnitOfMeasure")%> />
    </td>
</tr>
<tr>
    <td>
        <input type="hidden" class="protectedinput" <%=getTextOptions("xml:/Items/Item/@OrganizationCode","xml:/InventoryItem/@OrganizationCode")%> />
    </td>
    <td>
        <input type="hidden" class="protectedinput" <%=getTextOptions("xml:/Items/Item/@ETA","xml:/Item/Supplies/InventorySupply/@ETA")%> />
    </td>
    <td>
        <input type="hidden" class="protectedinput" <%=getTextOptions("xml:/Items/Item/@AdjustmentType","ADJUSTMENT")%> />
    </td>
</tr>
</table>
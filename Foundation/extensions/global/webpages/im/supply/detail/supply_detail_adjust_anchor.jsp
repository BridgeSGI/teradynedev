<%@include file="/yfsjspcommon/yfsutil.jspf"%>

<table class="anchor" cellpadding="7px"  cellSpacing="0" >
	<tr>
	    <td colspan="3" valign="top">
	        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
	            <jsp:param name="CurrentInnerPanelID" value="I01"/>
	        </jsp:include>
	    </td>
	</tr>
	<tr>
	    <td colspan="3" valign="top">
	        <jsp:include page="/yfc/innerpanel.jsp" flush="true">
	        	<jsp:param name="CurrentInnerPanelID" value="I03"/>
	    	</jsp:include>
	    </td>
	</tr>
	<tr>
	    <td width="33%" height="100%" valign="top">
	        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
	            <jsp:param name="CurrentInnerPanelID" value="I02"/>
	        </jsp:include>
	    </td>
	    <td width="33%" height="100%" valign="top">
	        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
	            <jsp:param name="CurrentInnerPanelID" value="I04"/>
	            <jsp:param name="BindingNode" value="xml:/Items/Item"/>
	        </jsp:include>
	    </td>
	    <td width="34%" valign="top">
	        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
	            <jsp:param name="CurrentInnerPanelID" value="I05"/>
	            <jsp:param name="BindingNode" value="xml:/Items/Item"/>
	        </jsp:include>
	    </td>
	</tr>
</table>
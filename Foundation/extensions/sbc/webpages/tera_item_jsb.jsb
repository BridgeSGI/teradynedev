<?xml version="1.0" encoding="UTF-8"?>
<!--
@author Sourabh Goyal, Bridge Solutions Group
-->

<project name="TeradyneSBCItemJSBTargets" author="Goyal,Sourabh">
	
	<!--Start: Teradyne Item related New JSB targets -->
	
	<target name="teraNewScreenTarget" file="/extn/item/teranewscreen/teranewscreenextn_all.js" allowDynamicLoad="true" debug="true" shorthand="false" shorthand-list="" 			
			depends="sbcitemrelatedtasks;itemprimaryinfo">
		<include name="/extn/item/teranewscreen/teranewscreenextn_config.js"/>
		<include name="/extn/item/teranewscreen/teranewscreenextn.js"/>   
	</target>
	
	<target name="teraNewScreenExtensionTarget" file="/extn/item/teranewscreenextension/teranewscreenextension_all.js" allowDynamicLoad="true" debug="true" shorthand="false" 
			shorthand-list="" loadAfter="teraNewScreenTarget">
		<include name="/extn/item/teranewscreenextension/teranewscreenextension_overlays.js"/>
		<include name="/extn/item/teranewscreenextension/teranewscreenextension.js"/>   
	</target>
	
	<target name="teraSystemAttributesNewScreenTarget" file="/extn/item/terasystemattributesnewscreen/terasystemattributesscreen_all.js" allowDynamicLoad="true" debug="true" 
			shorthand="false" shorthand-list="" depends="sbcitemrelatedtasks;itemprimaryinfo">
		<include name="/extn/item/terasystemattributesnewscreen/terasystemattributesscreen_config.js"/>
		<include name="/extn/item/terasystemattributesnewscreen/terasystemattributesscreen.js"/>   
	</target>
	
	<target name="teraSystemAttributesNewScreenExtensionTarget" file="/extn/item/terasystemattributesnewscreenextension/terasystemattributesscreenextn_all.js" 
			allowDynamicLoad="true" debug="true" shorthand="false" shorthand-list="" loadAfter="teraSystemAttributesNewScreenTarget">
		<include name="/extn/item/terasystemattributesnewscreenextension/terasystemattributesscreenextn_overlays.js"/>
		<include name="/extn/item/terasystemattributesnewscreenextension/terasystemattributesscreenextn.js"/>   
	</target>
	
	<target name="teraitemclassificationextensiontarget" loadAfter="seaitemclassifications" file="/extn/item/itemclassificationextension/teraitemclassificationextension_all.js"  
			allowDynamicLoad="true" debug="true">
        <include name="/extn/item/itemclassificationextension/teraitemclassificationextension_overlays.js"/>
		<include name="/extn/item/itemclassificationextension/teraitemclassificationextension.js"/>
	</target>
	
	<target name="teraItemAuditScreenTarget" file="/extn/item/teraitemauditscreen/teraitemauditscreen_all.js" allowDynamicLoad="true" debug="true" shorthand="false" 
			shorthand-list="" depends="sbcitemrelatedtasks;itemprimaryinfo;sbcItemSearchPopup;sbcServiceSearchPopup">
		<include name="/extn/item/teraitemauditscreen/teraitemauditscreen_config.js"/>
		<include name="/extn/item/teraitemauditscreen/teraitemauditscreen.js"/>
		<include name="/extn/item/teraitemauditscreen/addteraitemdetailspopup_config.js"/>   
		<include name="/extn/item/teraitemauditscreen/addteraitemdetailspopup.js"/>   
	</target>
	
	<target name="teraItemAuditScreenExtensionTarget" file="/extn/item/teraitemauditscreenextension/teraitemauditscreenextension_all.js" allowDynamicLoad="true" debug="true" 
			shorthand="false" shorthand-list="" loadAfter="teraItemAuditScreenTarget">
		<include name="/extn/item/teraitemauditscreenextension/teraitemauditscreenextension_overlays.js"/>
		<include name="/extn/item/teraitemauditscreenextension/teraitemauditscreenextension.js"/>   
		<include name="/extn/item/teraitemauditscreenextension/addteraitemdetailspopupextension_overlays.js"/>   
		<include name="/extn/item/teraitemauditscreenextension/addteraitemdetailspopupextension.js"/>   
	</target>

	<target name="teraitemprimaryinfoextensiontarget" loadAfter="seaitemdetails" file="/extn/item/itemprimaryinfoextension/teraitemprimaryinfoextension_all.js"  
			allowDynamicLoad="true" debug="true">
        <include name="/extn/item/itemprimaryinfoextension/teraitemprimaryinfoextension_overlays.js"/>
		<include name="/extn/item/itemprimaryinfoextension/teraitemprimaryinfoextension.js"/>
	</target>	
	
	<target name="teraitemdetailsextensiontarget" loadAfter="seaitemdetails" file="/extn/item/itemdetailsextension/teraitemdetailsextension_all.js" allowDynamicLoad="true" 	
			debug="true">
        <include name="/extn/item/itemdetailsextension/teraitemdetailsextension_overlays.js"/>
		<include name="/extn/item/itemdetailsextension/teraitemdetailsextension.js"/>
	</target>
	
	<!--End: Teradyne Item related New JSB targets -->

	<!--Start: Teradyne Price related New JSB targets -->
	
	<target name="tera_price_summaryresults_target" loadAfter="seaPriceListSummary" file="/extn/pricing/terapricelistsummaryresultsextn/terapricelistsummaryresultsextn_all.js" 
			allowDynamicLoad="true" debug="true">
        <include name="/extn/pricing/terapricelistsummaryresultsextn/terapricelistsummaryresultsextn_overlays.js"/>
        <include name="/extn/pricing/terapricelistsummaryresultsextn/terapricelistsummaryresultsextn.js"/>
	</target>
	
	<target name="teraZonePricelistSearch" file="/extn/pricing/terapricezonesearchscreen/terazonepricelistsearch_all.js" allowDynamicLoad="true" debug="true" shorthand="false" 
			shorthand-list="" depends="seaSearch;sbcOpenTestPricing;seaCopyPriceList">
		<include name="/extn/pricing/terapricezonesearchscreen/terazonebasicpricelistsearch_config.js"/>
		<include name="/extn/pricing/terapricezonesearchscreen/terazonebasicpricelistsearch.js"/>   
		<include name="/extn/pricing/terapricezonesearchscreen/terazonepricelistsearchresults_config.js"/>   
		<include name="/extn/pricing/terapricezonesearchscreen/terazonepricelistsearchresults.js"/>  
		<include name="/extn/pricing/terapricezonesearchscreen/terazoneadvancedpricelistsearch.js"/>
		<include name="/extn/pricing/terapricezonesearchscreen/terazonepricelistsearch.js"/>
		<include name="/sbc/common/relatedtask/actions/opencreatepricelist.js"/>
 	
	</target>
	
	<target name="teraZonePricelistSearchExtn" loadAfter="teraZonePricelistSearch" file="/extn/pricing/terapricezonesearchscreenExtn/terazonebasicpricelistsearchExtn_all.js" 
			allowDynamicLoad="true" debug="true">
        <include name="/extn/pricing/terapricezonesearchscreenExtn/terazonebasicpricelistsearchExtn_overlays.js"/>
        <include name="/extn/pricing/terapricezonesearchscreenExtn/terazonebasicpricelistsearchExtn.js"/>
	</target>
	
	<target name="teraZonePricelistSearchResultExtn" loadAfter="teraZonePricelistSearch" file="/extn/pricing/terapricezonesearchscreenExtn/terazonepricelistsearchresultsExtn_all.js" 
			allowDynamicLoad="true" debug="true">
        <include name="/extn/pricing/terapricezonesearchscreenExtn/terazonepricelistsearchresultsExtn_overlays.js"/>
        <include name="/extn/pricing/terapricezonesearchscreenExtn/terazonepricelistsearchresultsExtn.js"/>
	</target>
	
	<target name="teraZonesbcsearchExtn" loadAfter="teraZonePricelistSearch" file="/extn/pricing/terapricezonesearchscreenExtn/terasbcsearchExtn_all.js" 
			allowDynamicLoad="true" debug="true">
        <include name="/extn/pricing/terapricezonesearchscreenExtn/terasbcsearchExtn_overlays.js"/>
        <include name="/extn/pricing/terapricezonesearchscreenExtn/terasbcsearchExtn.js"/>
	</target>
	
	<!--End: Teradyne Price related New JSB targets -->

	<!--Start: Teradyne Existing OOB JSB targets overridded-->

	<target name="sbcitemrelatedtasks" file="/scripts/sbcitemrelatedtasks.js" allowDynamicLoad="true" debug="true" depends="">
		<include name="/sbc/common/relatedtask/actions/openmanageclassifications.js"/>
		<include name="/sbc/common/relatedtask/actions/openmanagevariations.js"/>
		<include name="/sbc/common/relatedtask/actions/openmanageassociations.js"/>
		<include name="/sbc/common/relatedtask/actions/openmanageattributes.js"/>
		<include name="/sbc/common/relatedtask/actions/openiteminstructions.js"/>
		<include name="/sbc/common/relatedtask/actions/openmanageserviceoptions.js" /> 
		<include name="/sbc/common/relatedtask/actions/openmanageserviceskills.js" /> 
		<include name="/sbc/common/relatedtask/actions/openmanageserviceactivities.js" /> 
		<include name="/sbc/common/relatedtask/actions/openserviceprimaryinfo.js" />
		<include name="/sbc/common/relatedtask/actions/openitemdetailsmanageprovidedservice.js" />
		<include name="/sbc/common/relatedtask/actions/openitemdetailsmanagedeliveryservice.js" />
		<include name="/sbc/common/relatedtask/actions/openitemdetailsmanagereturnpickupservice.js" />
		<include name="/sbc/common/relatedtask/actions/openitemdetailsmanageotherservice.js" />
		<include name="/sbc/common/relatedtask/actions/openaliasesandkeywords.js" />
		<include name="/sbc/common/relatedtask/actions/openextendedfields.js" />
		<include name="/sbc/common/relatedtask/actions/openfulfillment.js" /> 
		<include name="/sbc/common/relatedtask/actions/openmanageuom.js" />
		<include name="/sbc/common/relatedtask/actions/openreturnfulfillment.js" />
		<include name="/sbc/common/relatedtask/actions/openbundlecomponents.js" />
		<include name="/sbc/common/relatedtask/actions/opennodeitems.js" />
		<include name="/sbc/common/relatedtask/actions/openentitlementsearch.js" />
		<include name="/sbc/common/relatedtask/actions/opensellingentitlement.js" />
		<include name="/sbc/common/relatedtask/actions/opencustomerverification.js" />
		<include name="/sbc/common/relatedtask/actions/opensellingentitlementsearch.js" />
		<include name="/sbc/common/relatedtask/actions/itemassetstask.js" />
		<include name="/sbc/common/relatedtask/actions/openinventory.js" />
		<include name="/sbc/common/relatedtask/actions/openmarketingandsalestask.js" />
		<include name="/sbc/common/relatedtask/actions/openiteml10n.js"/>
		<include name="/sbc/itemadmin/item/manage/basic/itemdetailsl10n.js"/>
		<include name="/sbc/common/relatedtask/actions/openpreviewitemdetails.js"/>
		<include name="/sbc/common/relatedtask/actions/openbuyingentitlement.js" />
		<include name="/sbc/common/relatedtask/actions/openservicesearch.js" />
		<include name="/sbc/common/relatedtask/actions/opencreateservice.js" />
		<include name="/sbc/common/relatedtask/actions/openitemsearch.js" /> 
		<include name="/sbc/common/relatedtask/actions/opencreateitem.js"/>
		<include name="/extn/sbc/common/relatedtask/actions/openteraitemattributeaction.js"/>
		<include name="/extn/sbc/common/relatedtask/actions/openterasystemitemattributeaction.js"/>
		<include name="/extn/sbc/common/relatedtask/actions/teraitemauditaction.js"/>
	</target>
	
	<target name="seaOpenPriceListSearch" file="/scripts/seaopenPricelistSearch.js" allowDynamicLoad="true" debug="true" shorthand="false" shorthand-list="" 
		depends="">
			<include name="/sbc/common/relatedtask/actions/managepricelist.js"/>
			<include name="/extn/sbc/common/relatedtask/actions/terapricelistbyzoneaction.js"/>
	</target>
	
	<target name="seaSearch" file="/scripts/seaSearch.js" allowDynamicLoad="true" debug="true" shorthand="false" shorthand-list="" 
			depends="sbcpaging;cacheManager">
		<include name="/sbc/common/search/sbcsearch_config.js"/>
		<include name="/sbc/common/search/sbcsearch.js"/>
		<include name="/sbc/common/advancedsearch/advancedsearch_config.js"/>
		<include name="/sbc/common/advancedsearch/advancedsearch.js"/>
		<include name="/sbc/common/advancedsearch/conditionpanel_config.js"/>
		<include name="/sbc/common/advancedsearch/conditionpanel.js"/>
		<include name="/sbc/common/advancedsearch/advancedsearchcontrols/sbcadvcombocontrol.js"/>
		<include name="/sbc/common/advancedsearch/advancedsearchcontrols/sbcadvtextcontrol.js"/>
		<include name="/sbc/common/advancedsearch/advancedsearchcontrols/sbcadvnumbercontrol.js"/>
		<include name="/sbc/common/advancedsearch/advancedsearchcontrols/sbcadvradiocontrol.js"/>
		<include name="/sbc/common/advancedsearch/advancedsearchcontrols/sbcadvtriggercontrol.js"/>
		<include name="/sbc/common/advancedsearch/advancedsearchcontrols/sbcadvdatecontrol.js"/>
		<include name="/sbc/common/advancedsearch/advancedsearchcontrols/extendedcontrols/sbcadvattrlookup.js"/>
		<include name="/sbc/common/savedsearch/findsavedsearch/findsavedsearch_config.js"/>
		<include name="/sbc/common/savedsearch/findsavedsearch/findsavedsearch.js"/>
		<include name="/sbc/common/savedsearch/createsavedsearch/createsavedsearch_config.js"/>
		<include name="/sbc/common/savedsearch/createsavedsearch/createsavedsearch.js"/>
		<include name="/sbc/common/savedsearch/savedsearchutils.js"/>		
		<include name="/extn/pricing/terapricezonesearchscreen/terasbcsearch_config.js"/>
		<include name="/extn/pricing/terapricezonesearchscreen/terasbcsearch.js"/>
		
	</target>
	<!--End: Teradyne Existing OOB JSB targets overridded-->

</project>

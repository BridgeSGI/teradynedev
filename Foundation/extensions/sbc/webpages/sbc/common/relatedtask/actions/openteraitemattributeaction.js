/*
@author Sourabh Goyal, Bridge Solutions Group
*/
 
Ext.namespace("sc.sbc.common.relatedtask.actions");

sc.sbc.common.relatedtask.actions.managesourahbextendedfields = new Ext.Action( {
	handler : function(screen) {
		itemUtils.openItemRelatedTask("loadteraitemattributestrutsaction");
	}
});

sc.sbc.common.relatedtask.RelatedTaskActionMgr.registerAction("extnTeraItemAttributeAction", sc.sbc.common.relatedtask.actions.managesourahbextendedfields);

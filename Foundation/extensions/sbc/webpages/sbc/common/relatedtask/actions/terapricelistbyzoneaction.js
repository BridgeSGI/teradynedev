/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace("sc.sbc.common.relatedtask.actions");

sc.sbc.common.relatedtask.actions.teraPricelistByZoneActionObj = new Ext.Action({
	handler : function(screen){
			sc.sbc.helper.AppHelper.loadEntity("terapricelistbyzonesearch", sc.sbc.App.PricingStrutsNS);
		}
	}
);

sc.sbc.common.relatedtask.RelatedTaskActionMgr.registerAction("teraPricelistZoneSearchAction", sc.sbc.common.relatedtask.actions.teraPricelistByZoneActionObj);

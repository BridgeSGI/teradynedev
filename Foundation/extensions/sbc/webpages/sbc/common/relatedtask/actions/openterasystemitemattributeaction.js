/*
@author Sourabh Goyal, Bridge Solutions Group
*/
 
Ext.namespace("sc.sbc.common.relatedtask.actions");

sc.sbc.common.relatedtask.actions.manageterasystemattributes = new Ext.Action( {
	handler : function(screen) {
		itemUtils.openItemRelatedTask("loadterasystemitemattributestrutsaction");
	}
});

sc.sbc.common.relatedtask.RelatedTaskActionMgr.registerAction("extnTeraItemSystemAttributeAction", sc.sbc.common.relatedtask.actions.manageterasystemattributes);

/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace("sc.sbc.common.relatedtask.actions");

sc.sbc.common.relatedtask.actions.teraitemauditaction = new Ext.Action( {
	handler : function(screen) {
		itemUtils.openItemRelatedTask("teraitemauditaction");
	}
});

sc.sbc.common.relatedtask.RelatedTaskActionMgr.registerAction("extnTeraItemAuditAction", sc.sbc.common.relatedtask.actions.teraitemauditaction);

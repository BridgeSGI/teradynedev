/*
 * @author Sourabh Goyal, Bridge Solutions Group
 */

Ext.namespace('sc.item.itemprimaryinfoextension');

sc.item.itemprimaryinfoextension.itemprimaryinfoExtension = function() {
    sc.item.itemprimaryinfoextension.itemprimaryinfoExtension.superclass.constructor.call(this);
}
Ext.extend(sc.item.itemprimaryinfoextension.itemprimaryinfoExtension, sc.plat.ui.Extension, {
    className: 'sc.item.itemprimaryinfoextension.itemprimaryinfoExtension',
    getConfigOverlays: sc.item.itemprimaryinfoextension.itemprimaryinfoExtensionConfigOverlays,
    namespaces: {
        target: [],
        source: []
    },
    namespacesDesc: {
        targetDesc: [],
        sourceDesc: []
    }
});
sc.plat.ScreenExtnMgr.add('sc.itemadmin.item.manage.basic.itemprimaryinfo', sc.item.itemprimaryinfoextension.itemprimaryinfoExtension);
/*
 * @author Sourabh Goyal, Bridge Solutions Group
 */

Ext.namespace('sc.item.itemprimaryinfoextension');

sc.item.itemprimaryinfoextension.itemprimaryinfoExtensionConfigOverlays = function() {
    return [{
        op: "insert",
        ownerId: "primaryinfoPanel",
        property: "items",
        refId: "panel74",
        pos: "before",
        config: {
            xtype: "label",
            sciId: "extn_lastupdatedtext",
            cls: "sc-plat-noneditable-text",
            width: 250,
            hidden: true,
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetailsOutputForPrimaryInfo:Item.Modifyts"
            }
        }
    },
    {
        op: "insert",
        ownerId: "panel71",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "label",
            sciId: "extn_blanklabel",
            width: 150
        }
    },
    {
        op: "insert",
        ownerId: "panel71",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "panel",
            sciId: "extn_terapanel",
            layout: "table",
            border: false,
            layoutConfig: {
                defid: "tableLayoutConfig",
                columns: 4
            }
        }
    },
    {
        op: "insert",
        ownerId: "panel73",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "label",
            sciId: "extn_ecflabel",
            text: "ECF:",
            cls: "sc-plat-label"
        }
    },
    {
        op: "insert",
        ownerId: "panel73",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "label",
            sciId: "extn_ecftext",
            cls: "sc-plat-noneditable-text",
            width: 75,
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetailsOutputForPrimaryInfo:Item.Extn.EngineeringControlFlag"
            }
        }
    },
    {
        op: "move",
        remove: {
            op: "remove",
            ownerId: "panel73",
            property: "items",
            sciId: "deleteButton"
        },
        insert: {
            op: "insert",
            ownerId: "panel73",
            property: "items",
            refId: null,
            pos: "in"
        }
    },
    {
        op: "change",
        sciId: "primaryinfoScreen",
        config: {
            xtype: "screen"
        }
    },
    {
        op: "insert",
        ownerId: "extn_terapanel",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "label",
            sciId: "extn_lastupdatelabel",
            text: "Last Updated:",
            cls: "sc-plat-label"
        }
    },
    {
        op: "insert",
        ownerId: "extn_terapanel",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "xdatetime",
            sciId: "extn_LastUpdateDate",
            dateFormat: "d-M-Y",
            disabled: true,
            editable: false,
            readOnly: true,
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetailsOutputForPrimaryInfo:Item.Modifyts"
            },
            timeConfig: {
                defid: "timeConfig",
                hidden: true
            }
        }
    },
    {
        op: "insert",
        ownerId: "extn_terapanel",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "label",
            sciId: "extn_ByLabel",
            text: "By:",
            cls: "sc-plat-label"
        }
    },
    {
        op: "insert",
        ownerId: "extn_terapanel",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "label",
            sciId: "extn_ByText",
            cls: "sc-plat-noneditable-text",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetailsOutputForPrimaryInfo:Item.Modifyuserid"
            }
        }
    },
    {
        op: "change",
        sciId: "lblItemcharacteristicValue",
        config: {
            xtype: "label",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetailsOutputForPrimaryInfo:Item.PrimaryInformation.ItemChar"
            }
        }
    }];
}
/*
@author Sourabh Goyal, Bridge Solutions Group
*/
 
Ext.namespace('sc.item.teraitemauditscreen');

sc.item.teraitemauditscreen.TeraAuditScreenClass = function(config) {
	var header = '<div class="x-grid3-hd-checker"> </div>';
	var singleSelect = true;
	Ext.apply(config, {singleSelect : singleSelect});
	header="";

		this.getItemSelectionModel = function(){
	    	return new Ext.grid.CheckboxSelectionModel({
	    		listeners : {
					"selectionchange" : this.itemSelectionChangeHandler,
					scope : this    						
				},
				singleSelect:singleSelect,
				header:header
	    	});
		}
    	
    	this.getCategorySelectionModel = function(){
	    	return new Ext.grid.CheckboxSelectionModel({
	    		listeners : {
					"selectionchange" : this.categorySelectionChangeHandler,
					scope : this    						
				}
	    	});
    	}	
	
	sc.item.teraitemauditscreen.TeraAuditScreenClass.superclass.constructor.call(this, config);
	var itemGrid = this.find('sciId','seaItemAssociations')[0];
	var categoryGrid = this.find('sciId','seaCategoryAssociations')[0];

	itemGrid.on("cellClick", function(grid, rowIndex, columnIndex, e) {},this);
    categoryGrid.on("cellClick", function(grid, rowIndex, columnIndex, e) {},this);    

}

Ext.extend(sc.item.teraitemauditscreen.TeraAuditScreenClass,
		sc.plat.ui.ExtensibleScreen, {
			className : 'sc.item.teraitemauditscreen.TeraAuditScreenClass',
			getUIConfig : sc.item.teraitemauditscreen.TeraAuditScreenClassUIConfig,
			associationType : "",			
			checkBox : new Ext.form.Checkbox(),
			seaItemCheckBoxColumn : new Ext.grid.CheckboxSelectionModel(),
			seaCategoryCheckBoxColumn : new Ext.grid.CheckboxSelectionModel(),
			associatedQuantity : new Ext.form.NumberField({
						emptyText : "0"
					}),
			priorityColumn : new Ext.form.NumberField({
						emptyText : "0",
						allowDecimals : false,
						allowNegative : false
					}),
			numberOfRows :0,

			showLink : function(value) {
				return "<a href='#'>" + value + "</a>"
			},
			namespaces : {
				source : [],
				target : []
			},
			seaAssociationLabelText : function() {
				return this.associationType;
			},
			
			idColumnHeader : function() {
				return this.b_ItemID;
			},
			seaItemsPaneltitle : function() {
				return this.b_Items;
			},
			
			seaCustomPrimaryInfoPanelxtype : function(){
				return "seaItemPrimaryInfo";
			},
			/** Populate and initialize audit screen data including primary info screen*/
			populateAuditPrimaryInfoData : function() {
				var headerPanel = this.find("sciId", "seaMainPanel")[0];
				itemUtils.setPanelTitle(headerPanel, "Teradyne Item Audits", this);
				var primaryinfo = this.find("sciId", "seaCustomPrimaryInfoPanel")[0];
                primaryinfo.initialize();
			},

			/**
			 * To View Details of Selected Audit record/checkbox
			 */
			addAssociationItems : function() {				
					var grid = this.find('sciId', "seaItemAssociations")[0];
					var sel = grid.getSelectionModel().selections.items;
					var selectedAuditElem = sel[0].json;
					console.log("selectedAuditElem::::::"+Ext.encode(selectedAuditElem));
					this.teraAuditDetailPopup = new sc.item.teraitemauditscreen.TeraAuditDetailPopupClass();

					spopupTitle = "Teradyne Item Audit Details";
					this.addAssociationWindow = sc.sbc.util.WindowUtils.getWindow({
						screen : this.teraAuditDetailPopup,
						windowConfig : {
						closeAction : "close",
						maximizable : false,
						title: spopupTitle
						},					
						buttons : sc.sbc.util.WindowUtils.CLOSE,
						scope : this,
						parent: this
					});
					this.addAssociationWindow.show();					
					this.teraAuditDetailPopup.setData(selectedAuditElem);
			},

				resHandlerForItemAuditList : function(res, options) {
					this.clean();
				},

				itemSelectionChangeHandler : function(selModel, rowIndex, record){
					var grid = sc.sbc.util.CoreUtils.getItemBySciId(this,"seaItemAssociations");
					var sel = selModel.selections.items;
					if(selModel.hasSelection() && sel.length ==1)
							sc.sbc.util.CoreUtils.enableTBarButton(grid, "seaAddItemButton");
					else
							sc.sbc.util.CoreUtils.disableTBarButton(grid, "seaAddItemButton");
				},
				
				categorySelectionChangeHandler : function(selModel, rowIndex, record){
				},
				
				processTaskCategory : function(category, categoryId){
						return itemUtils.processTaskCategoryForItems(category, this.getCurrentTaskId());
				},
				getCurrentTaskId : function(){
						return "TERA_SBCITM0003";
				}
				
		});
Ext.reg('teraItemAuditScreenRegX',sc.item.teraitemauditscreen.TeraAuditScreenClass);

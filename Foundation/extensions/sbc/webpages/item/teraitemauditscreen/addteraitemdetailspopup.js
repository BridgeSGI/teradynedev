/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.item.teraitemauditscreen');

sc.item.teraitemauditscreen.TeraAuditDetailPopupClass = function(config) {
	config = config || {};
	Ext.applyIf(config, {height: 200, width: 600});
	this.qtyFromFieldEditor=new sc.plat.ui.BigNumberField({allowNegative :false});
	this.qtyToEditor=new sc.plat.ui.BigNumberField({allowNegative :false});
	this.absAdjustmentEditor=new sc.plat.ui.BigNumberField(),
	this.percAdjustmentEditor=new sc.plat.ui.BigNumberField(),
	this.listPriceEditor=new sc.plat.ui.BigNumberField(),

	sc.item.teraitemauditscreen.TeraAuditDetailPopupClass.superclass.constructor.call(this, config);
}
Ext.extend(sc.item.teraitemauditscreen.TeraAuditDetailPopupClass, sc.plat.ui.ExtensibleScreen, {
    className: 'sc.item.teraitemauditscreen.TeraAuditDetailPopupClass',
    getUIConfig: sc.item.teraitemauditscreen.TeraAuditDetailPopupClassUIConfig,
	namespaces: {
				source: ["getAdditionalAuditData"],
                target:[]
            },
	namespacesDesc: {
                sourceDesc: ["Selected Item Audit Data"],
                targetDesc: []
            },
	
	setData : function(selectedAuditElem){
		var auditElem = selectedAuditElem;
		this.setModel(auditElem, "getAdditionalAuditData", {clearOldVals : true});	
	},	
    
	addMoreCriteriaLink :"<img src='"+Ext.BLANK_IMAGE_URL+"' class='sbc-icon-add pricing-link-margin-top'/>"+sc.plat.bundle["b_addTier"],
	disableBtn : function(disable) {
	},
	handleCheckBoxSelected:function(){
	},
	addRow:function(){
	},
	handleAfterEdit:function(e){
	},
	returnRemoveButton:function(){
	},
	handleDeletedRecords:function(record){
	},
	handeCellClick:function(grid, rowIndex, columnIndex, e ){
	}
});
Ext.reg('teraItemAuditDetailPopupRegX', sc.item.teraitemauditscreen.TeraAuditDetailPopupClass);

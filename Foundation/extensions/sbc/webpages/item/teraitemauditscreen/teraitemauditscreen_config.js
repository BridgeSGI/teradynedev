/*
@author Sourabh Goyal, Bridge Solutions Group
*/


Ext.namespace('sc.item.teraitemauditscreen');

sc.item.teraitemauditscreen.TeraAuditScreenClassUIConfig = function() {
    return {
        xtype: "screen",
        sciId: "teraItemAuditScreenSCIID",
        layout: "anchor",
        autoScroll: true,
        items: [{
            xtype: "panel",
            sciId: "seaMainPanel",
            layout: "anchor",
            items: [{
                defid: "customct",
                sciId: "seaCustomPrimaryInfoPanel",
                xtype: this.seaCustomPrimaryInfoPanelxtype(),
                text: "custom_component"
            },
            {
                xtype: "panel",
                sciId: "seaHolderPanel",
                layout: "anchor",
                header: false,
                items: [{
                    xtype: "label",
                    sciId: "seaAssociationLabel",
                    cls: "sc-plat-noneditable-text padding-left-5px"
                },
                {
                    xtype: "label",
                    sciId: "seaAssociationDesc",
                    cls: "sc-plat-label-left padding-left-5px"
                },
                {
                    xtype: "tabpanel",
                    sciId: "seaCategoryOrgCode",
                    items: [{
                        xtype: "panel",
                        sciId: "seaItemsPanel",
                        title: this.seaItemsPaneltitle(),
                        layout: "anchor",
                        header: false,
                        items: [{
                            xtype: "grid",
                            sciId: "seaItemAssociations",
                            clicksToEdit: 1,
                            tbar: [{
                                xtype: "button",
                                sciId: "seaAddItemButton",
                                text: this.b_Add,
                                handler: this.addAssociationItems,
                                scope: this,
                                minWidth: 60
                            }],
                            header: false,
                            columns: [this.seaItemCheckBoxColumn, {
                                defid: "grid-column",
                                sciId: "seaItemNameColumn",
                                header: this.idColumnHeader(),
                                sortable: true,
                                dataIndex: "Item.ItemID",
                                renderer: this.showLink,
                                width: 150,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Item.ItemID",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Item.ItemID"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaItemDescColumn",
                                header: this.ShortDescription,
                                sortable: true,
                                dataIndex: "Item.PrimaryInformation.ShortDescription",
                                id: "descColumn",
                                width: 175,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Item.PrimaryInformation.ShortDescription",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Item.PrimaryInformation.ShortDescription"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaItemQuantityColumn",
                                header: this.b_AssociatedQuantity_Header,
                                sortable: true,
                                dataIndex: "AssociatedQuantity",
                                width: 80,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "AssociatedQuantity",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "AssociatedQuantity"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaItemPriorityColumn",
                                header: this.b_Priority_Header,
                                sortable: true,
                                dataIndex: "Priority",
                                width: 80,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Priority",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Priority"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaItemStartDateColumn",
                                header: this.EffectiveStartDate,
                                sortable: true,
                                dataIndex: "EffectiveFrom",
                                width: 125,
                                scope: this,
                                css: "sc-plat-datefield-text",
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "EffectiveFrom",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "EffectiveFrom"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaItemEndDateColumn",
                                header: this.EffectiveEndDate,
                                sortable: true,
                                dataIndex: "EffectiveTo",
                                width: 125,
                                scope: this,
                                css: "sc-plat-datefield-text",
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "EffectiveTo",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "EffectiveTo"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaAssocKey",
                                header: "Association Key",
                                sortable: true,
                                dataIndex: "AssociationKey",
                                hidden: true,
                                hideable: false,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "AssociationKey",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "AssociationKey"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaOrgCode",
                                header: "Org Code",
                                sortable: true,
                                dataIndex: "Item.OrganizationCode",
                                hidden: true,
                                hideable: false,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Item.OrganizationCode",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Item.OrganizationCode"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaUOM",
                                header: "UOM",
                                sortable: false,
                                dataIndex: "Item.UnitOfMeasure",
                                hidden: true,
                                hideable: false,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Item.UnitOfMeasure",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Item.UnitOfMeasure"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaAssociationType",
                                header: "Association Type",
                                sortable: false,
                                dataIndex: "AssociationType",
                                hidden: true,
                                hideable: false,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "AssociationType",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "AssociationType"
                                    }
                                }
                            }],
                            selModel: this.getItemSelectionModel(),
                            autoHeight: true,
                            autoScroll: true,
                            sctype: "InnerGridPanel",
                            border: true,
                            stripeRows: true,
                            bindingData: {
                                defid: "object",
                                sourceBinding: "ItemAssociationsModel:AssociationList.Association",
                                targetBinding: ["ItemAssociationsTargetModel:ItemList.AssociationList.Association"]
                            }
                        }],
                        autoHeight: true,
                        autoWidth: true,
                        border: false,
                        layoutConfig: {
                            defid: "layoutConfig",
                            columns: 1
                        }
                    },
                    {
                        xtype: "panel",
                        sciId: "seaCategoryPanel",
                        title: this.b_Category,
                        layout: "anchor",
                        items: [{
                            xtype: "grid",
                            sciId: "seaCategoryAssociations",
                            title: "Category Associations",
                            clicksToEdit: 1,
                            tbar: [{
                                xtype: "button",
                                sciId: "seaAddCategoryButton",
                                text: this.b_Add,
                                handler: this.addAssociationCategories,
                                scope: this,
                                minWidth: 60
                            },
                            "     ", {
                                xtype: "button",
                                sciId: "seaModifyCategoryButton",
                                text: this.b_Modify,
                                handler: this.modifyAssociationCategories,
                                scope: this,
                                disabled: true,
                                minWidth: 60
                            },
                            "     ", {
                                xtype: "button",
                                sciId: "seaRemoveCategoryButton",
                                text: this.b_Remove,
                                handler: this.removeCategoryAction,
                                scope: this,
                                disabled: true,
                                minWidth: 60
                            }],
                            columns: [this.seaCategoryCheckBoxColumn, {
                                defid: "grid-column",
                                sciId: "seaCategoryNameColumn",
                                header: this.b_Associations_categoryid,
                                sortable: true,
                                dataIndex: "Category.CategoryID",
                                width: 150,
                                renderer: this.showLink,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Category.CategoryID",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Category.CategoryID"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaCategoryPathColumn",
                                header: this.b_Associations_categorypath,
                                sortable: true,
                                dataIndex: "Category.CategoryPath",
                                hidden: false,
                                width: 150,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Category.CategoryPath",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Category.CategoryPath"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaCategoryDescColumn",
                                header: this.ShortDescription,
                                sortable: true,
                                dataIndex: "Category.ShortDescription",
                                width: 180,
                                id: "catDescColumn",
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Category.ShortDescription",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Category.ShortDescription"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaCategoryStartDateColumn",
                                header: this.EffectiveStartDate,
                                sortable: true,
                                dataIndex: "EffectiveFrom",
                                scope: this,
                                width: 125,
                                css: "sc-plat-datefield-text",
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "EffectiveFrom",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "EffectiveFrom"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaCategoryEndDateColumn",
                                header: this.EffectiveEndDate,
                                sortable: true,
                                dataIndex: "EffectiveTo",
                                scope: this,
                                width: 125,
                                css: "sc-plat-datefield-text",
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "EffectiveTo",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "EffectiveTo"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaCategoryOrgCodeColumn",
                                header: "Org  Code",
                                sortable: true,
                                dataIndex: "Category.OrganizationCode",
                                hidden: true,
                                hideable: false,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Category.OrganizationCode",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Category.OrganizationCode"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaCategoryKeyColumn",
                                header: "Category Key",
                                sortable: true,
                                hidden: true,
                                hideable: false,
                                dataIndex: "Category.CategoryKey",
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "Category.CategoryKey",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "Category.CategoryKey"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaAssocType",
                                header: "Association Type",
                                sortable: true,
                                hidden: true,
                                hideable: false,
                                dataIndex: "AssociationType",
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "AssociationType",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "AssociationType"
                                    }
                                }
                            },
                            {
                                defid: "grid-column",
                                sciId: "seaCatAssocKey",
                                header: "Association Key",
                                sortable: true,
                                dataIndex: "AssociationKey",
                                hidden: true,
                                hideable: false,
                                bindingData: {
                                    defid: "object",
                                    tAttrBinding: "AssociationKey",
                                    sFieldConfig: {
                                        defid: "object",
                                        mapping: "AssociationKey"
                                    }
                                }
                            }],
                            selModel: this.getCategorySelectionModel(),
                            autoHeight: true,
                            autoScroll: true,
                            header: false,
                            sctype: "InnerGridPanel",
                            border: false,
                            stripeRows: true,
                            bindingData: {
                                defid: "object",
                                sourceBinding: "CategoryAssociationsModel:AssociationList.Association",
                                targetBinding: ["CategoryAssociationsTargetModel:CategoryList.AssociationList.Association"]
                            }
                        }],
                        autoHeight: true,
                        autoWidth: true,
                        border: false,
                        header: false,
                        layoutConfig: {
                            defid: "layoutConfig",
                            columns: 1
                        }
                    }],
                    autoHeight: true,
                    plain: true,
                    border: false,
                    activeTab: 0,
                    deferredRender: false,
                    monitorResize: true,
                    sctype: "TabPanel"
                }],
                border: false,
                layoutConfig: {
                    defid: "layoutConfig",
                    columns: 1
                }
            }],
            sctype: "ScreenMainPanel",
            header: true,
            layoutConfig: {
                defid: "layoutConfig",
                columns: 1
            }
        }],
        border: false,
        sctype: "ScreenBasePanel"
    };
}

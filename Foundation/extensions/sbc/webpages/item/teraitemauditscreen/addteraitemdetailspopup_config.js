/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.item.teraitemauditscreen');

sc.item.teraitemauditscreen.TeraAuditDetailPopupClassUIConfig = function() {
    return {
        xtype: "screen",
        sciId: "teraAuditDetailPopupSCIID",
        header: false,
        layout: "anchor",
        autoScroll: true,
        items: [{
            xtype: "panel",
            sciId: "seaPanelMain",
            layout: "anchor",
            items: [{
                xtype: "panel",
                sciId: "seaPaneListPrice",
                layout: "table",
                items: [{
                    xtype: "panel",
                    sciId: "seaPanelDateRanges",
                    layout: "table",
                    items: [{
                        xtype: "label",
                        sciId: "seaLabelItemPrices",
                        text: this.b_itempricesfrom,
                        cls: "sc-plat-noneditable-text"
                    },
                    {
                        xtype: "label",
                        sciId: "seaLabelDateFrom",
                        cls: "sc-plat-noneditable-text",
                        bindingData: {
                            defid: "object",
                            sourceBinding: "quantityTiersElem:PriceListLine.FormattedDates"
                        }
                    }],
                    border: false,
                    layoutConfig: {
                        defid: "tableLayoutConfig",
                        columns: 4
                    }
                },
                {
                    xtype: "panel",
                    sciId: "panel3",
                    layout: "table",
                    items: [{
                        xtype: "label",
                        sciId: "seaLblListPrice",
                        text: this.b_listpricelabel,
                        cls: "sc-plat-label sc-mandatory"
                    },
                    {
                        xtype: "label",
                        sciId: "sealabelsymbolprefix",
                        text: "",
                        cls: "sc-plat-noneditable-text",
                        bindingData: {
                            defid: "object",
                            sourceBinding: "currencyElemModel:currencyElem.PrefixSymbol"
                        }
                    },
                    {
                        xtype: "bignumberfield",
                        sciId: "seaTextListPrice",
                        allowBlank: false,
                        cls: "sc-plat-noneditable-text",
                        blankText: sc.sbc.ui.messages.getRequiredFieldText("b_listprice"),
                        bindingData: {
                            defid: "object",
                            sourceBinding: "quantityTiersElem:PriceListLine.ListPrice",
                            targetBinding: ["getTargetModelForQtyTiers:PricelistLineList.PricelistLine.ListPrice"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "sealabelsymbolpostfix",
                        cls: "sc-plat-noneditable-text",
                        bindingData: {
                            defid: "object",
                            sourceBinding: "currencyElemModel:currencyElem.PostfixSymbol"
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "sealabelpercadjustment",
                        text: this.b_percentageadjustment,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "bignumberfield",
                        sciId: "sbctextfieldadjustment",
                        cls: "sc-plat-noneditable-text",
                        bindingData: {
                            defid: "object",
                            sourceBinding: "quantityTiersElem:PriceListLine.PercentAdjustment",
                            targetBinding: ["getTargetModelForQtyTiers:PricelistLineList.PricelistLine.PercentAdjustment"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "sealabeladjustment",
                        text: this.b_absadjustmentlabel,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "label",
                        sciId: "sealabeladjustmentprefix",
                        cls: "sc-plat-noneditable-text",
                        bindingData: {
                            defid: "object",
                            sourceBinding: "currencyElemModel:currencyElem.PrefixSymbol"
                        }
                    },
                    {
                        xtype: "bignumberfield",
                        sciId: "sbctextfieldabsadjustment",
                        cls: "sc-plat-noneditable-text",
                        bindingData: {
                            defid: "object",
                            sourceBinding: "quantityTiersElem:PriceListLine.AbsoluteAdjustment",
                            targetBinding: ["getTargetModelForQtyTiers:PricelistLineList.PricelistLine.AbsoluteAdjustment"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "sealabeladjustmentpostfix",
                        cls: "sc-plat-noneditable-text",
                        bindingData: {
                            defid: "object",
                            sourceBinding: "currencyElemModel:currencyElem.PostfixSymbol"
                        }
                    },
                    {
                        xtype: "checkbox",
                        sciId: "seaCheckboxCondition",
                        boxLabel: this.b_qtybasedprices,
                        handler: this.handleCheckBoxSelected,
                        id: "checkBoxTiers",
                        scope: this,
                        cellCls: "sc-plat-checkboxlabel"
                    }],
                    border: false,
                    layoutConfig: {
                        defid: "tableLayoutConfig",
                        columns: 10
                    }
                }],
                border: false,
                layoutConfig: {
                    defid: "tableLayoutConfig",
                    columns: 1
                }
            },
            {
                xtype: "panel",
                sciId: "seaFieldSetCondition",
                layout: "anchor",
                items: [{
                    xtype: "grid",
                    sciId: "seaGridQtyRanges",
                    columns: [{
                        defid: "grid-column",
                        sciId: "seaGridClmDelete",
                        sortable: true,
                        renderer: this.returnRemoveButton,
                        width: 30,
                        dataIndex: "Delete",
                        hideable: false
                    },
                    {
                        defid: "grid-column",
                        sciId: "seaGridClmQtyFrom",
                        sortable: true,
                        header: this.b_greaterthanorequalto,
                        dataIndex: "FromQuantity",
                        editor: this.qtyFromFieldEditor,
                        width: 150,
                        css: sc.plat.CSS.getMergedCssText(".sc-numeric-column"),
                        bindingData: {
                            defid: "object",
                            tAttrBinding: "FromQuantity",
                            sFieldConfig: {
                                defid: "object",
                                mapping: "FromQuantity",
                                type: "string",
                                sortType: "asBigNumber"
                            }
                        }
                    },
                    {
                        defid: "grid-column",
                        sciId: "seaGridQtyTo",
                        sortable: true,
                        header: this.b_lessthan,
                        dataIndex: "ToQuantity",
                        editor: this.qtyToEditor,
                        width: 150,
                        css: sc.plat.CSS.getMergedCssText(".sc-numeric-column"),
                        bindingData: {
                            defid: "object",
                            tAttrBinding: "ToQuantity",
                            sFieldConfig: {
                                defid: "object",
                                mapping: "ToQuantity",
                                type: "string",
                                sortType: "asBigNumber"
                            }
                        }
                    },
                    {
                        defid: "grid-column",
                        sciId: "seaGridClmPrice",
                        sortable: true,
                        header: this.b_listprice,
                        dataIndex: "ListPrice",
                        editor: this.listPriceEditor,
                        id: "sbcautoexpandcolumn",
                        css: sc.plat.CSS.getMergedCssText(".sc-numeric-column"),
                        width: 120,
                        bindingData: {
                            defid: "object",
                            tAttrBinding: "ListPrice",
                            sFieldConfig: {
                                defid: "object",
                                mapping: "ListPrice",
                                type: "string",
                                sortType: "asBigNumber"
                            }
                        }
                    }],
                    id: "gridRanges",
                    hidden: false,
                    autoWidth: true,
                    autoScroll: true,
                    header: false,
                    stripeRows: true,
                    height: 200,
                    border: false,
                    sctype: "InnerGridPanel",
                    bindingData: {
                        defid: "object",
                        sourceBinding: "quantityTiersElem:PriceListLine.PricelistLineQuantityTierList.PricelistLineQuantityTier",
                        targetBinding: ["getTargetModelForQtyTiers:PricelistLineList.PricelistLine.PricelistLineQuantityTierList.PricelistLineQuantityTier"]
                    }
                },
                {
                    xtype: "sclink",
                    sciId: "seaLinkAddAnotherTier",
                    hidden: false,
                    id: "linkAddAnotherTier",
                    handler: this.addRow,
                    scope: this,
                    html: this.addMoreCriteriaLink,
                    cls: "padding-left-5px"
                }],
                border: false,
                anchor: "100%",
                header: true,
                title: this.b_TPQuantity,
                sctype: "ScreenInnerPanel",
                autoHeight: true,
                autoScroll: true,
                layoutConfig: {
                    defid: "layoutConfig",
                    columns: 4
                }
            }],
            header: true,
            sctype: "ScreenMainPanel",
            layoutConfig: {
                defid: "layoutConfig",
                columns: 1
            }
        }],
        border: false,
        sctype: "ScreenBasePanel",
        title: this.b_AdjustPrices
    };
}

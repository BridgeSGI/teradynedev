<%
/*
@author Sourabh Goyal, Bridge Solutions Group
*/
%>

<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIUserPreferences"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="com.sterlingcommerce.sbc.core.utils.SBCJSONUtils"%> 
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Document"%>
<%

SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);

Object retoutput = uiContext.getAttribute("getTeraItemAuditOutput");
String teraItemAuditjson = "";
if(null!=retoutput){
Element getTeraItemAuditEle=(Element)retoutput;
teraItemAuditjson = SBCJSONUtils.getJSONFromXML(request, response, getTeraItemAuditEle);
}
%>

function loadItemAudit(){		
	var itemAuditVar = new sc.item.teraitemauditscreen.TeraAuditScreenClass();
	var screenTitle = "Teradyne Item Audits";
	containerUtil.drawScreen(itemAuditVar, {title : screenTitle});
	
	itemAuditVar.postInit = function() {
		itemAuditVar.populateAuditPrimaryInfoData();
		itemAuditVar.extension.setModel(<%=teraItemAuditjson%>,"getTeraItemAuditOutputNS");
	}
	itemAuditVar.postInit();
};
sc.plat.JSLibManager.loadLibrary("teraItemAuditScreenTarget", loadItemAudit);

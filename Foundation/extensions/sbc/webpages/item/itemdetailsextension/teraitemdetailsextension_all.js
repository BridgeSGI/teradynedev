/*
 * 
 */

/*
 * 
 */


Ext.namespace('sc.item.itemdetailsextension');sc.item.itemdetailsextension.itemdetailsExtensionConfigOverlays=function(){return[{op:"change",sciId:"itemdetailsScreen",config:{xtype:"screen"}},{op:"change",sciId:"ShortDescriptionLabel",config:{xtype:"label"}},{op:"change",sciId:"localshortdescriptionLink",config:{xtype:"sclink"}},{op:"change",sciId:"shortDescriptionText",config:{xtype:"textfield"}},{op:"change",sciId:"LongDescriptionLabel",config:{xtype:"label"}},{op:"change",sciId:"locallongdescriptionLink",config:{xtype:"sclink"}},{op:"change",sciId:"longDescriptionText",config:{xtype:"textarea"}}];}

Ext.namespace('sc.item.itemdetailsextension');sc.item.itemdetailsextension.itemdetailsExtension=function(){sc.item.itemdetailsextension.itemdetailsExtension.superclass.constructor.call(this);}
Ext.extend(sc.item.itemdetailsextension.itemdetailsExtension,sc.plat.ui.Extension,{className:'sc.item.itemdetailsextension.itemdetailsExtension',getConfigOverlays:sc.item.itemdetailsextension.itemdetailsExtensionConfigOverlays,namespaces:{target:[],source:[]},namespacesDesc:{targetDesc:[],sourceDesc:[]}});sc.plat.ScreenExtnMgr.add('sc.sbc.itemadmin.item.manage.basic.itemdetails',sc.item.itemdetailsextension.itemdetailsExtension);

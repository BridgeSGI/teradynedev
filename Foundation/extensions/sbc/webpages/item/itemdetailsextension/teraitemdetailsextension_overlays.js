/*
 * @author Sourabh Goyal, Bridge Solutions Group
 */
Ext.namespace('sc.item.itemdetailsextension');

sc.item.itemdetailsextension.itemdetailsExtensionConfigOverlays = function() {
    return [{
        op: "change",
        sciId: "itemdetailsScreen",
        config: {
            xtype: "screen"
        }
    },
    {
        op: "change",
        sciId: "ShortDescriptionLabel",
        config: {
            xtype: "label"
        }
    },
    {
        op: "change",
        sciId: "localshortdescriptionLink",
        config: {
            xtype: "sclink"
        }
    },
    {
        op: "change",
        sciId: "shortDescriptionText",
        config: {
            xtype: "textfield"
        }
    },
    {
        op: "change",
        sciId: "LongDescriptionLabel",
        config: {
            xtype: "label"
        }
    },
    {
        op: "change",
        sciId: "locallongdescriptionLink",
        config: {
            xtype: "sclink"
        }
    },
    {
        op: "change",
        sciId: "longDescriptionText",
        config: {
            xtype: "textarea"
        }
    }];
}
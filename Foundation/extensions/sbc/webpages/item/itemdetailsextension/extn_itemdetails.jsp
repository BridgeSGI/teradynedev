<%
/**
 * @author Sourabh Goyal, Bridge Solutions Group
 */
%>


<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIUtils"%><%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="com.sterlingcommerce.sbc.core.utils.SBCJSONUtils"%> 
<%@page import="com.sterlingcommerce.framework.utils.SCXmlUtils"%>
<%@page import="com.sterlingcommerce.sbc.itemadmin.util.itemutil"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="java.io.*"%>
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.dom.*"%>
<%@page import="javax.xml.transform.stream.*"%>
<%
SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);
Object retItemDetails = uiContext.getAttribute("itemDetailsOutput");
Element eItemDetails = (Element)retItemDetails;
String jsonItemDetails= SBCJSONUtils.getJSONFromXML(request, response, eItemDetails);

Object retCommonCode = uiContext.getAttribute("commonCodeOutput");
Element eCommonCode = (Element)retCommonCode;
String jsonCommonCode= SBCJSONUtils.getJSONFromXML(request, response, eCommonCode);  

Object retCountryCommonCode = uiContext.getAttribute("countryCommonCodeInput");
Element eCountryCommonCode = (Element)retCountryCommonCode;
String jsonCountryCommonCode= SBCJSONUtils.getJSONFromXML(request, response, eCountryCommonCode);

Element eModelItemDetails = (Element)uiContext.getAttribute("modelItemDetailsOutput");
String modelItemDetails = "''"; 
if(!SCUIUtils.isVoid(eModelItemDetails)){
	modelItemDetails = SBCJSONUtils.getJSONFromXML(request, response, eModelItemDetails);
}

Element eItemContext = (Element)request.getAttribute("ItemContext");
String itemContext = "''";
if(!SCUIUtils.isVoid(eItemContext)){
	itemContext = SBCJSONUtils.getJSONFromXML(request, response, eItemContext);
}

String isFromRelatedTask = request.getParameter("IsFromRelatedTask");
if(SCUIUtils.isVoid(isFromRelatedTask)){
	isFromRelatedTask = "N";	
}
%>

function loadItemDetails(){
	
	var getitemDetailsOutput = '<%=jsonItemDetails%>';
	getitemDetailsOutput = Ext.decode(getitemDetailsOutput);
    var extnRepairCode = getitemDetailsOutput.Item.Extn.RepairCode;

	var itemdetails = new sc.sbc.itemadmin.item.manage.basic.itemdetails();
	var isCreateFlow = itemManager.isBackButtonNotRequired();
	if(isCreateFlow){
		containerUtil.drawScreen(itemdetails, {title: itemdetails.b_PrimaryInformation,
											buttons : [{btype:sc.sbc.helper.ScreenHelper.SAVE, text:itemdetails.b_WindowSave, handler:itemdetails.btnSave}]});
		if(<%=itemContext%>){
			itemManager.setCurrentItemContext(<%=itemContext%>,true,"MANAGE");
		}
	}else if('<%=isFromRelatedTask%>' === "Y"){
		containerUtil.drawScreen(itemdetails, {title: itemdetails.b_PrimaryInformation,
											back : true,
											cacheRequired:false,
											buttons : [{btype:sc.sbc.helper.ScreenHelper.SAVE, text:itemdetails.b_WindowSave, handler:itemdetails.btnSave}]});
	}else {
		containerUtil.drawScreen(itemdetails, {title: itemdetails.b_PrimaryInformation,
											back : true,
											buttons : [{btype:sc.sbc.helper.ScreenHelper.SAVE, text:itemdetails.b_WindowSave, handler:itemdetails.btnSave}]});
		
	}
	itemManager.setItemStatusList(<%=jsonCommonCode %>);
	itemdetails.postInit = function() {
			itemdetails.setModel(<%=jsonCountryCommonCode%>, "getCommonCodeListCountryOutput");
			itemdetails.setModel(<%=jsonCommonCode%>, "getCommonCodeListItemStatusOutput");
			
			var countryOfOriginCombo = this.find("sciId", "combo2")[0];
			if(!("C"==extnRepairCode || "N"==extnRepairCode)){
			countryOfOriginCombo.displayField="";
			countryOfOriginCombo.disable();
			}
			
			var primaryinfo = this.find("sciId", "custItemPrimaryInfo")[0];
			primaryinfo.initialize();
			itemdetails.initialize(<%=jsonItemDetails%>, <%=modelItemDetails %>);
			itemdetails.doLayout();
	}
	itemdetails.postInit();
};
sc.plat.JSLibManager.loadLibrary("seaitemdetails", loadItemDetails);

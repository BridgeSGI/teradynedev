/*
 * @author Sourabh Goyal, Bridge Solutions Group
 */

Ext.namespace('sc.item.itemdetailsextension');

sc.item.itemdetailsextension.itemdetailsExtension = function() {
    sc.item.itemdetailsextension.itemdetailsExtension.superclass.constructor.call(this);
}
Ext.extend(sc.item.itemdetailsextension.itemdetailsExtension, sc.plat.ui.Extension, {
    className: 'sc.item.itemdetailsextension.itemdetailsExtension',
    getConfigOverlays: sc.item.itemdetailsextension.itemdetailsExtensionConfigOverlays,
    namespaces: {
        target: [],
        source: []
    },
    namespacesDesc: {
        targetDesc: [],
        sourceDesc: []
    }
});
sc.plat.ScreenExtnMgr.add('sc.sbc.itemadmin.item.manage.basic.itemdetails', sc.item.itemdetailsextension.itemdetailsExtension);
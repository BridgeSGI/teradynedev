/*
 * @author Sourabh Goyal, Bridge Solutions Group
 */

Ext.namespace('sc.item.terasystemattributesnewscreenextension');

sc.item.terasystemattributesnewscreenextension.ClassTeraSystemAttributesScreenExtension = function() {
    sc.item.terasystemattributesnewscreenextension.ClassTeraSystemAttributesScreenExtension.superclass.constructor.call(this);
}
Ext.extend(sc.item.terasystemattributesnewscreenextension.ClassTeraSystemAttributesScreenExtension, sc.plat.ui.Extension, {
    className: 'sc.item.terasystemattributesnewscreenextension.ClassTeraSystemAttributesScreenExtension',
    getConfigOverlays: sc.item.terasystemattributesnewscreenextension.ClassTeraSystemAttributesScreenExtensionConfigOverlays,
    namespaces: {
        target: [],
        source: ["getCommonCodeListForPlatformNS","getCommonCodeListForPTExceptionFlagNS","getCommonCodeListForLifecycleCategoryNS"]
    },
    namespacesDesc: {
        targetDesc: [],
        sourceDesc: ["Teradyne Platform Code List","Teradyne PT Exception Flag Code List","Teradyne Lifecycle Category Code List"]
    }
});
sc.plat.ScreenExtnMgr.add('sc.item.terasystemattributesnewscreen.ClassTeraSystemAttributesScreen', sc.item.terasystemattributesnewscreenextension.ClassTeraSystemAttributesScreenExtension);
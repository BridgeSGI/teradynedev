<%
/**
 *@author Sourabh Goyal, Bridge Solutions Group
 */
%>

<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="com.sterlingcommerce.sbc.core.utils.SBCJSONUtils"%> 
<%@page import="com.sterlingcommerce.framework.utils.SCXmlUtils"%>
<%@page import="org.w3c.dom.*"%>
<%@page import="java.io.*"%>
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.dom.*"%>
<%@page import="javax.xml.transform.stream.*"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIUserPreferences"%>
<%@page import="com.yantra.yfc.dom.*"%>
<%@page import="com.yantra.interop.*"%>

<%

SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);

Object ret = uiContext.getAttribute("getitemdetails");
Element e = (Element)ret;
String json = SBCJSONUtils.getJSONFromXML(request, response, e);

Object ret123 = uiContext.getAttribute("getBoxIDComboBoxValuesServiceOutput");
Element eBox = (Element)ret123;
String jsonGetBoxIDComboBoxValues = SBCJSONUtils.getJSONFromXML(request, response, eBox);

/*Start: This Below code between Start-end is not used as of now so better commenting*/
/*
Object ret123 = uiContext.getAttribute("teraGetItemListOutput");
Element teragetItemDetails = (Element)ret123;
Document getItemListDoc = teragetItemDetails.getOwnerDocument();
	NodeList itemNodeList=getItemListDoc.getElementsByTagName("Item");
	for(int i=0;i<itemNodeList.getLength();i++){
		Element itemElement=(Element)itemNodeList.item(i);
		NodeList itemAttriNodeList=itemElement.getElementsByTagName("AdditionalAttribute");
		for(int j=0;j<itemAttriNodeList.getLength();j++){
		Element itemAttrElement=(Element)itemAttriNodeList.item(j);
		String itemAddAttrValue=itemAttrElement.getAttribute("Value");
		itemElement.setAttribute("AdditionalAttributeValueTemp",itemAddAttrValue);
	}
}
String jsonGetItemDetails = SBCJSONUtils.getJSONFromXML(request, response, getItemListDoc);
*/
/*End: This above code between Start-end is not used as of now so better commenting*/

Object ret1 = uiContext.getAttribute("getclassificationpurposelist");
Element e1=(Element)ret1;
String json1 = SBCJSONUtils.getJSONFromXML(request, response, e1);

Object ret2 = uiContext.getAttribute("getcommoncodelistforposting");
Element e2=(Element)ret2;
String json2 = SBCJSONUtils.getJSONFromXML(request, response, e2);

Object ret3 = uiContext.getAttribute("getcommoncodelistforvelocitycode");
Element e3=(Element)ret3;
String json3 = SBCJSONUtils.getJSONFromXML(request, response, e3);

Object ret4 = uiContext.getAttribute("getcommoncodelistforteraattriharmonizedcodeoutput");
Element e4=(Element)ret4;
String json4 = SBCJSONUtils.getJSONFromXML(request, response, e4);

Object ret5 = uiContext.getAttribute("getcommoncodelistforteraattrieccncodeoutput");
Element e5=(Element)ret5;
String json5 = SBCJSONUtils.getJSONFromXML(request, response, e5);

Object ret6 = uiContext.getAttribute("getcommoncodelistforteraattriitemtypecodeoutput");
Element e6=(Element)ret6;
String json6 = SBCJSONUtils.getJSONFromXML(request, response, e6);

Object ret7 = uiContext.getAttribute("getcommoncodelistforteraattriimportagenciescodeoutput");
Element e7=(Element)ret7;
String json7 = SBCJSONUtils.getJSONFromXML(request, response, e7);

Object ret8 = uiContext.getAttribute("getcommoncodelistforproductgroupoutput");
Element e8=(Element)ret8;
String json8 = SBCJSONUtils.getJSONFromXML(request, response, e8);

Object ret9 = uiContext.getAttribute("getcommoncodelistforflagcodeoutput");
Element e9=(Element)ret9;
String json9 = SBCJSONUtils.getJSONFromXML(request, response, e9);

Object ret10 = uiContext.getAttribute("getcommoncodelistforsupportstatusoutput");
Element e10=(Element)ret10;
//Code for Concatenation of Common Code Vale and Desc for Support Status Code 
Document getSupportStatusListDoc = e10.getOwnerDocument();
	NodeList supportStatusCCNodeList=getSupportStatusListDoc.getElementsByTagName("CommonCode");
	for(int i=0;i<supportStatusCCNodeList.getLength();i++){
		Element coomonCodeElement=(Element)supportStatusCCNodeList.item(i);
		String commonCodeValue=coomonCodeElement.getAttribute("CodeValue");
		String commonCodeShortDescription=coomonCodeElement.getAttribute("CodeShortDescription");
		String commonCodeValueAndShortDesc="";
		if("".equals(commonCodeShortDescription)){
		commonCodeValueAndShortDesc=commonCodeValue;
		} else{
				commonCodeValueAndShortDesc=commonCodeValue.concat("-").concat(commonCodeShortDescription);
		}
		coomonCodeElement.setAttribute("CommonCodeValuePlusDescValueTemp",commonCodeValueAndShortDesc);
}
String json10 = SBCJSONUtils.getJSONFromXML(request, response, e10);

Object ret10Desc = uiContext.getAttribute("getVariousCommonCodeDescriptionServiceOutput");
Element e10Desc=(Element)ret10Desc;
String json10Desc = SBCJSONUtils.getJSONFromXML(request, response, e10Desc);

Object ret11 = uiContext.getAttribute("getPlannerCodeComboBoxValuesServiceOutput");
Element e11=(Element)ret11;
String json11 = SBCJSONUtils.getJSONFromXML(request, response, e11);

Object ret12 = uiContext.getAttribute("getcommoncodelistforcomponentcodeoutput");
Element e12=(Element)ret12;
String json12 = SBCJSONUtils.getJSONFromXML(request, response, e12);

Object ret13 = uiContext.getAttribute("getcommoncodelistforrepairmodeloutput");
Element e13=(Element)ret13;
String json13 = SBCJSONUtils.getJSONFromXML(request, response, e13);

Object ret14 = uiContext.getAttribute("getcommoncodelistforinvlocationoutput");
Element e14=(Element)ret14;
String json14 = SBCJSONUtils.getJSONFromXML(request, response, e14);

Object ret15 = uiContext.getAttribute("getcommoncodelistforwarrantycodeoutput");
Element e15=(Element)ret15;
String json15 = SBCJSONUtils.getJSONFromXML(request, response, e15);

Object ret19 = uiContext.getAttribute("getcommoncodelistforflagcodeoutput");
Element e19=(Element)ret19;
String json19 = SBCJSONUtils.getJSONFromXML(request, response, e19);

Object ret20 = uiContext.getAttribute("getcommoncodelistforflagcodeoutput");
Element e20=(Element)ret20;
String json20 = SBCJSONUtils.getJSONFromXML(request, response, e20);

Object ret21divcodeonly = uiContext.getAttribute("DivCodeOnlyServiceOutput");
Element e21divcodeonly=(Element)ret21divcodeonly;
String json21divcodeonly = SBCJSONUtils.getJSONFromXML(request, response, e21divcodeonly);

Object ret25 = uiContext.getAttribute("getcommoncodelistforrepcodeoutput");
Element e25=(Element)ret25;
//Code for Concatenation of Common Code Vale and Desc for Repair Code 
Document getRepairListDoc = e25.getOwnerDocument();
	NodeList repairCCNodeList=getRepairListDoc.getElementsByTagName("CommonCode");
	for(int i=0;i<repairCCNodeList.getLength();i++){
		Element coomonCodeElement=(Element)repairCCNodeList.item(i);
		String commonCodeValue=coomonCodeElement.getAttribute("CodeValue");
		String commonCodeShortDescription=coomonCodeElement.getAttribute("CodeShortDescription");
		String commonCodeValueAndShortDesc="";
		if("".equals(commonCodeShortDescription)){
		commonCodeValueAndShortDesc=commonCodeValue;
		} else{
				commonCodeValueAndShortDesc=commonCodeValue.concat("-").concat(commonCodeShortDescription);
		}
		coomonCodeElement.setAttribute("CommonCodeValuePlusDescValueTemp",commonCodeValueAndShortDesc);
}
String json25 = SBCJSONUtils.getJSONFromXML(request, response, e25);

Object ret26 = uiContext.getAttribute("getcommoncodelistfordistributioncodeoutput");
Element e26=(Element)ret26;
//Code for Concatenation of Common Code Vale and Desc for Distribution Code 
Document getDistributionListDoc = e26.getOwnerDocument();
	NodeList distributionCCNodeList=getDistributionListDoc.getElementsByTagName("CommonCode");
	for(int i=0;i<distributionCCNodeList.getLength();i++){
		Element coomonCodeElement=(Element)distributionCCNodeList.item(i);
		String commonCodeValue=coomonCodeElement.getAttribute("CodeValue");
		String commonCodeShortDescription=coomonCodeElement.getAttribute("CodeShortDescription");
		String commonCodeValueAndShortDesc="";
		if("".equals(commonCodeShortDescription)){
		commonCodeValueAndShortDesc=commonCodeValue;
		} else{
				commonCodeValueAndShortDesc=commonCodeValue.concat("-").concat(commonCodeShortDescription);
		}		
		coomonCodeElement.setAttribute("CommonCodeValuePlusDescValueTemp",commonCodeValueAndShortDesc);
}
String json26 = SBCJSONUtils.getJSONFromXML(request, response, e26);

Object ret27 = uiContext.getAttribute("getcommoncodelistforrepcomplexityoutput");
Element e27=(Element)ret27;
String json27 = SBCJSONUtils.getJSONFromXML(request, response, e27);

Object ret28 = uiContext.getAttribute("getcommoncodelistforrepproductgroupoutput");
Element e28=(Element)ret28;
String json28 = SBCJSONUtils.getJSONFromXML(request, response, e28);

Object ret29 = uiContext.getAttribute("getcommoncodelistforflagcodeoutput");
Element e29=(Element)ret29;
String json29 = SBCJSONUtils.getJSONFromXML(request, response, e29);

Object ret30 = uiContext.getAttribute("getUserListSPRAttnToOutput");
Element e30=(Element)ret30;
String json30 = SBCJSONUtils.getJSONFromXML(request, response, e30);

Object ret31 = uiContext.getAttribute("getcommoncodelistforplatformoutput");
Element e31=(Element)ret31;
String json31 = SBCJSONUtils.getJSONFromXML(request, response, e31);

Object ret32 = uiContext.getAttribute("getcommoncodelistforflagcodeoutput");
Element e32=(Element)ret32;
String json32 = SBCJSONUtils.getJSONFromXML(request, response, e32);

Object ret33 = uiContext.getAttribute("getcommoncodelistforlifecyclecategoryoutput");
Element e33=(Element)ret33;
String json33 = SBCJSONUtils.getJSONFromXML(request, response, e33);

String itemdetailsNS = uiContext.getRequest().getParameter("itemdetailsNS");

%>


function loaditemAttributesTeradyneScreenObj(){

var getItemDetailsOutput = '<%=json%>';
var getBoxIDComboBoxValuesServiceOutputVar = '<%=jsonGetBoxIDComboBoxValues%>';

var getClassificationPurposeListOutput = '<%=json1%>';
var getCommonCodeListForPostingOutput = '<%=json2%>';
var getCommonCodeListForVelocityCodeOutput = '<%=json3%>';

var getCommonCodeListForHarmonizedCodeOutput = '<%=json4%>';
var getCommonCodeListForECCNCodeOutput = '<%=json5%>';
var getCommonCodeListForItemTypeCodeOutput = '<%=json6%>';
var getCommonCodeListForImportAgenciesCodeOutput = '<%=json7%>';

var getCommonCodeListForProductGroupOutput = '<%=json8%>';
var getCommonCodeListForOEMSNOutput = '<%=json9%>';
var getCommonCodeListForSupportStatusOutput = '<%=json10%>';
var getVariousCommonCodeDescriptionOutputVar = '<%=json10Desc%>';
var getCommonCodeListForPlannerCodeOutput = '<%=json11%>';
var getCommonCodeListForComponentCodeOutput = '<%=json12%>';
var getCommonCodeListForRepairModelOutput = '<%=json13%>';
var getCommonCodeListForInvLocationOutput = '<%=json14%>';
var getCommonCodeListForWarrantyCodeOutput = '<%=json15%>';
var getCommonCodeListForIsShippedAloneOutput = '<%=json19%>';
var getCommonCodeListForIsCriticalPartOutput = '<%=json20%>';
var terDivCodeOnlyServiceOutput = '<%=json21divcodeonly%>';
var getCommonCodeListForRepCodeOutput = '<%=json25%>';
var getCommonCodeListForDistributionCodeOutput = '<%=json26%>';
var getCommonCodeListForRepComplexityOutput = '<%=json27%>';
var getCommonCodeListForRepProductGroupOutput = '<%=json28%>';
var getCommonCodeListForSpecReviewOutput = '<%=json29%>';
var getUserListForSPRAttnToOutput = '<%=json30%>';
var getCommonCodeListForPlatformOutput = '<%=json31%>';
var getCommonCodeListForPTExceptionFlagOutput = '<%=json32%>';
var getCommonCodeListForLifeCycleCategoryOutput = '<%=json33%>';

var itemdetailsNS = '<%=itemdetailsNS%>';

		getClassificationPurposeListOutput = Ext.decode(getClassificationPurposeListOutput);
        var classificationPurposeList = getClassificationPurposeListOutput.ClassificationPurposeList.ClassificationPurpose;
       
		if(classificationPurposeList){
			var len = classificationPurposeList.length;
	        if(len>0){
				var attributeMap = {};
	        }
	 
			for (var i = 0; i < len; i++) {
	         var classificationPurposeObj = classificationPurposeList[i];
	         var attrName = classificationPurposeObj.AttributeName;
	          attributeMap[attrName] = classificationPurposeObj;
			}
		}
		getItemDetailsOutput =Ext.decode(getItemDetailsOutput);
		getBoxIDComboBoxValuesServiceOutputVar = Ext.decode(getBoxIDComboBoxValuesServiceOutputVar);
		getCommonCodeListForPostingOutput = Ext.decode(getCommonCodeListForPostingOutput);
		getCommonCodeListForVelocityCodeOutput = Ext.decode(getCommonCodeListForVelocityCodeOutput);		
		
		getCommonCodeListForHarmonizedCodeOutput = Ext.decode(getCommonCodeListForHarmonizedCodeOutput);
		getCommonCodeListForECCNCodeOutput = Ext.decode(getCommonCodeListForECCNCodeOutput);
		getCommonCodeListForItemTypeCodeOutput = Ext.decode(getCommonCodeListForItemTypeCodeOutput);
		getCommonCodeListForImportAgenciesCodeOutput = Ext.decode(getCommonCodeListForImportAgenciesCodeOutput);

		getCommonCodeListForProductGroupOutput = Ext.decode(getCommonCodeListForProductGroupOutput);
		
		getCommonCodeListForOEMSNOutput = Ext.decode(getCommonCodeListForOEMSNOutput);
		getCommonCodeListForSupportStatusOutput = Ext.decode(getCommonCodeListForSupportStatusOutput);
		getVariousCommonCodeDescriptionOutputVar = Ext.decode(getVariousCommonCodeDescriptionOutputVar);
		
		getCommonCodeListForPlannerCodeOutput = Ext.decode(getCommonCodeListForPlannerCodeOutput);
		getCommonCodeListForComponentCodeOutput = Ext.decode(getCommonCodeListForComponentCodeOutput);
		getCommonCodeListForRepairModelOutput = Ext.decode(getCommonCodeListForRepairModelOutput);
		getCommonCodeListForInvLocationOutput = Ext.decode(getCommonCodeListForInvLocationOutput);
		getCommonCodeListForWarrantyCodeOutput = Ext.decode(getCommonCodeListForWarrantyCodeOutput);
		getCommonCodeListForIsShippedAloneOutput = Ext.decode(getCommonCodeListForIsShippedAloneOutput);
		getCommonCodeListForIsCriticalPartOutput = Ext.decode(getCommonCodeListForIsCriticalPartOutput);
		terDivCodeOnlyServiceOutput = Ext.decode(terDivCodeOnlyServiceOutput);
		
		getCommonCodeListForRepCodeOutput = Ext.decode(getCommonCodeListForRepCodeOutput);
		getCommonCodeListForDistributionCodeOutput = Ext.decode(getCommonCodeListForDistributionCodeOutput);
		getCommonCodeListForRepComplexityOutput = Ext.decode(getCommonCodeListForRepComplexityOutput);
		getCommonCodeListForRepProductGroupOutput = Ext.decode(getCommonCodeListForRepProductGroupOutput);
		getCommonCodeListForSpecReviewOutput = Ext.decode(getCommonCodeListForSpecReviewOutput);
		getUserListForSPRAttnToOutput = Ext.decode(getUserListForSPRAttnToOutput);
		getCommonCodeListForPlatformOutput = Ext.decode(getCommonCodeListForPlatformOutput);
		getCommonCodeListForPTExceptionFlagOutput = Ext.decode(getCommonCodeListForPTExceptionFlagOutput);
		getCommonCodeListForLifeCycleCategoryOutput = Ext.decode(getCommonCodeListForLifeCycleCategoryOutput);

		var itemAttributesTeradyneScreenObj = new  sc.item.teranewscreen.ManageTeraClassificationsScreen({attrMap:attributeMap});
		var noBackLink = itemManager.isBackButtonNotRequired();
		if(noBackLink){
			containerUtil.drawScreen(itemAttributesTeradyneScreenObj, {title : "Teradyne Part Attributes", 
															buttons : {btype:sc.sbc.helper.ScreenHelper.SAVE,
																	handler:itemAttributesTeradyneScreenObj.saveAction}});
		}else{
        	containerUtil.drawScreen(itemAttributesTeradyneScreenObj, {title : "Teradyne Part Attributes", 
															back:true, 
															cacheRequired:false,
															buttons : {btype:sc.sbc.helper.ScreenHelper.SAVE,
																	handler:itemAttributesTeradyneScreenObj.saveAction}});
		}
		itemAttributesTeradyneScreenObj.postInit = function() {		
		itemAttributesTeradyneScreenObj.populateData(getItemDetailsOutput,getClassificationPurposeListOutput,
                                       		getCommonCodeListForPostingOutput,
											getCommonCodeListForVelocityCodeOutput,itemdetailsNS);
		
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForHarmonizedCodeOutput,"getCommonCodeListForHarmonizedCodeNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForECCNCodeOutput,"getCommonCodeListForECCNCodeNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForItemTypeCodeOutput,"getCommonCodeListForItemTypeCodeNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForImportAgenciesCodeOutput,"getCommonCodeListForImportAgenciesCodeNS");

		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForProductGroupOutput,"getCommonCodeListForProductGroupNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getBoxIDComboBoxValuesServiceOutputVar,"getBoxIDComboBoxValuesServiceOutputNS");
		
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForOEMSNOutput,"getCommonCodeListForOEMSNNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForSupportStatusOutput,"getCommonCodeListForSupportStatusNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getVariousCommonCodeDescriptionOutputVar,"getVariousCommonCodeDescriptionOutputNS");
		
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForPlannerCodeOutput,"getCommonCodeListForPlannerCodeNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForComponentCodeOutput,"getCommonCodeListForComponentCodeNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForRepairModelOutput,"getCommonCodeListForRepairModelNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForInvLocationOutput,"getCommonCodeListForInvLocationNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForWarrantyCodeOutput,"getCommonCodeListForWarrantyCodeNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForIsShippedAloneOutput,"getCommonCodeListForIsShippedAloneNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForIsCriticalPartOutput,"getCommonCodeListForIsCriticalPartNS");
		itemAttributesTeradyneScreenObj.extension.setModel(terDivCodeOnlyServiceOutput,"DivCodeOnlyServiceOutputNS");
		
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForRepCodeOutput,"getCommonCodeListForRepCodeNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForDistributionCodeOutput,"getCommonCodeListForDistributionCodeNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForRepComplexityOutput,"getCommonCodeListForRepComplexityNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForRepProductGroupOutput,"getCommonCodeListForRepProductNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForSpecReviewOutput,"getCommonCodeListForSpecReviewNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getUserListForSPRAttnToOutput,"getUserListForSPRAttnToNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForPlatformOutput,"getCommonCodeListForPlatformNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForPTExceptionFlagOutput,"getCommonCodeListForPTExceptionFlagNS");
		itemAttributesTeradyneScreenObj.extension.setModel(getCommonCodeListForLifeCycleCategoryOutput,"getCommonCodeListForLifecycleCategoryNS");
		
		
	}
	itemAttributesTeradyneScreenObj.postInit();
};

sc.plat.JSLibManager.loadLibrary("teraNewScreenTarget", loaditemAttributesTeradyneScreenObj);

/*
 @author Sourabh Goyal, Bridge Solutions Group
*/
 
Ext.namespace('sc.item.teranewscreen');

sc.item.teranewscreen.ManageTeraClassificationsScreen = function(config) {
	Ext.apply(this,config);
	sc.item.teranewscreen.ManageTeraClassificationsScreen.superclass.constructor.call(this, config);
}
	Ext.extend(
				sc.item.teranewscreen.ManageTeraClassificationsScreen,
				sc.plat.ui.ExtensibleScreen, {
					className : 'sc.item.teranewscreen.ManageTeraClassificationsScreen',
					getUIConfig : sc.item.teranewscreen.ManageTeraClassificationsScreenUIConfig,
					sItemID : "", //  ItemID
					sUnitOfMeasure : "", // Item UOM
					sOrgCode : "",// Organization Code
//					itemPrimaryInfo	: {Item:{ItemID: "TV0005" , UnitOfMeasure: "EACH",OrganizationCode : "DEFAULT" }},
					itempopupWin : null, //item popup window
					
					namespaces : {
						source : ["getItemDetails","getItemDetailsOutput",
								"getCommonCodeListForVelocityCode",
								"getCommonCodeListForPosting"],
						target : ["getManageItem"]
					},
					
					populateData : function(getItemDetailsOutput,
							getClassificationPurposeListOutput,
							getCommonCodeListForPostingOutput,
							getCommonCodeListForVelocityCodeOutput,itemdetailsNS) {
								
//						this.itemPrimaryInfo = itemdetailsNS;--To do
								
//						this.sItemID = this.itemPrimaryInfo.Item.ItemID;
//						this.sUnitOfMeasure =this.itemPrimaryInfo.Item.UnitOfMeasure;
//						this.sOrgCode = this.itemPrimaryInfo.Item.OrganizationCode;
						
						var headerPanel = this.find("sciId", "seaMainPanel")[0];
						itemUtils.setPanelTitle(headerPanel, "Teradyne Part Attributes", this);
						var primaryinfo = this.find("sciId", "seaCustomPrimaryInfoPanel")[0];
                 		primaryinfo.initialize();
						
						this.setModel(getItemDetailsOutput, "getItemDetails", {
									clearOldVals : true
								});
						this.setModel(getItemDetailsOutput, "getItemDetailsOutput", {
									clearOldVals : true
								});								
						this.setModel(getCommonCodeListForVelocityCodeOutput,
								"getCommonCodeListForVelocityCode", {
									clearOldVals : true
								});

						this.setModel(getCommonCodeListForPostingOutput,
								"getCommonCodeListForPosting", {
									clearOldVals : true
								});
					},

					hideTrigger : function(value) {
						if(this.attrMap){
							var map = this.attrMap;
						  
							if (map[value]) {
								return false;
							} else {
								return true;
							}
						}else{
							return false;
						}
						 
					},
					
					
					readOnly : function(value) {
						if(this.attrMap){
							var map = this.attrMap;
						  
							if (map[value]) {
								return true;
							} else {
								return false;
							}
						}else{
							return true;
						}
						 
					},

					saveAction : function() {						
						var manageItemModel = this.getTargetModel("getManageItem")[0];
						
						/*Extract Support Status Code value from Support Status DropDown that contains value-desc pair and store it in YFS_ITEM */
						var tempTerSupportStatusValueOutput=manageItemModel.ItemList.Item.Extn.TempSupportStatusValue;
						var terSupportStatusValue=tempTerSupportStatusValueOutput.substring(0,tempTerSupportStatusValueOutput.indexOf("-"));
	
						if((""==terSupportStatusValue)){
						manageItemModel.ItemList.Item.Extn.SupportStatusCode=tempTerSupportStatusValueOutput;
						}else{
						manageItemModel.ItemList.Item.Extn.SupportStatusCode=terSupportStatusValue;
						}

						/*Extract Planner Code value from Planner_Code DropDown that contains value-desc pair and store it in YFS_ITEM */
						var tempTerPlannerCodeValueOutput=manageItemModel.ItemList.Item.Extn.TempPlannerCodeValue;
						var terPlannerCodeValue1=tempTerPlannerCodeValueOutput.substring(0,tempTerPlannerCodeValueOutput.indexOf("|"));
						var terPlannerCodeValue2=tempTerPlannerCodeValueOutput.substring(0,tempTerPlannerCodeValueOutput.indexOf("-"));						
						
						if((""==terPlannerCodeValue1) && (""==terPlannerCodeValue2)){
						manageItemModel.ItemList.Item.Extn.PlannerCode=tempTerPlannerCodeValueOutput;
						}else if(!(""==terPlannerCodeValue1)){
						manageItemModel.ItemList.Item.Extn.PlannerCode=terPlannerCodeValue1;
						}else if(!(""==terPlannerCodeValue2)){
						manageItemModel.ItemList.Item.Extn.PlannerCode=terPlannerCodeValue2;
						}

						/*Extract Box ID Code value from Box ID DropDown that contains value-desc pair and store it in YFS_ITEM */
						var tempTerBoxIDValueOutput=manageItemModel.ItemList.Item.Extn.TempBoxIDValue;
						var terBoxIDValue=tempTerBoxIDValueOutput.substring(0,tempTerBoxIDValueOutput.indexOf("-"));

						if((""==terBoxIDValue)){
						manageItemModel.ItemList.Item.Extn.BoxID=tempTerBoxIDValueOutput;
						}else{
						manageItemModel.ItemList.Item.Extn.BoxID=terBoxIDValue;
						}

						/*Extract Repair Code value from Repair Code DropDown that contains value-desc pair and store it in YFS_ITEM */
						var tempTerRepairCodeValueOutput=manageItemModel.ItemList.Item.Extn.TempRepairCodeValue;
						var terRepairCodeValue=tempTerRepairCodeValueOutput.substring(0,tempTerRepairCodeValueOutput.indexOf("-"));

						if((""==terRepairCodeValue)){
						manageItemModel.ItemList.Item.Extn.RepairCode=tempTerRepairCodeValueOutput;
						}else{
						manageItemModel.ItemList.Item.Extn.RepairCode=terRepairCodeValue;
						}

						/*Extract Distribution Code value from Distribution Code DropDown that contains value-desc pair and store it in YFS_ITEM */
						var tempTerDistributionCodeValueOutput=manageItemModel.ItemList.Item.Extn.TempDistributionCodeValue;
						var terDistributionCodeValue=tempTerDistributionCodeValueOutput.substring(0,tempTerDistributionCodeValueOutput.indexOf("-"));

						if((""==terDistributionCodeValue)){
						manageItemModel.ItemList.Item.Extn.DistributionCode=tempTerDistributionCodeValueOutput;
						}else{
						manageItemModel.ItemList.Item.Extn.DistributionCode=terDistributionCodeValue;
						}
						
						/*Set TimeSensitive=Y if DefaultExpirationDays/Calibration Days is more than zero*/
						var terCalibrationDays=manageItemModel.ItemList.Item.InventoryParameters.DefaultExpirationDays;
						if(terCalibrationDays>0){
						manageItemModel.ItemList.Item.InventoryParameters.TimeSensitive="Y";
						}else{
						manageItemModel.ItemList.Item.InventoryParameters.TimeSensitive="N";
						}
						
						var paramsObj = {};
						paramsObj["manageiteminput"] = Ext.encode(manageItemModel);							
								    itemUtils.callItemStrutsAction("manageitemclassifications_save", {
								    	params : paramsObj,
								    	success :this.resHandlerForManageItem,
								    	successMsg : sc.sbc.ui.messages.getSaveSuccessfullText("Item"),
								    	scope :this
							});
					},
					
				/**
				 * Handler when the APi call to save data is successful
				 */
				resHandlerForManageItem : function(res, options) {
					this.clean();
				},
					
				/**
				 * This method is called to set the item model for other screens
				 */	
				openItemRelatedTask : function(strutsAction){
					var obj = {};
					obj["itemdetailsNS"] = Ext.encode(this.itemPrimaryInfo);
					seaAjaxUtils.request({
						actionNS : sc.sbc.App.ItemStrutsNS,
						action : strutsAction,
						success : this.relatedTaskActionSuccess,
						params : obj,
						scope: this
					});
				},
				
				/**
				 * Handler when the related task action is successful
				 */
				relatedTaskActionSuccess : function(res, options){
					eval(res.responseText);
				},
				
				loadClassificationLookup : function(e){
					if(this.scope.attrMap){
							var map = this.scope.attrMap;	
							if (map[this.fieldName]) {
								var classificationPurposeElem = map[this.fieldName];
								var categoryDomainElem = classificationPurposeElem.CategoryDomain;
								var sCategoryDomainKey = categoryDomainElem.CategoryDomainKey;
								sc.sbc.util.LoadEntityUtils.loadClassificationTreeLookup(sCategoryDomainKey, this.scope.populateItemField, this.scope,this.sciId);
							}
							
						}
				},
				
				 populateItemField : function(category,sciid){
				 	var sCategoryID = category[0].CategoryID;
				 	
				 	var triggerField = this.find("sciId", sciid)[0];
				 	triggerField.setValue(sCategoryID);
				 	
//				 	this.setDirty(true);
//    				var sAttributeName = this.getAttributeName(this.parentScreen);
				 },
				
				processTaskCategory : function(category, categoryId){
						return itemUtils.processTaskCategoryForItems(category, this.getCurrentTaskId());
				},
				getCurrentTaskId : function(){
						return "TERA_SBCITM0001";
				}
		});
Ext.reg('sbcManageTeraClassificationsScreen',sc.item.teranewscreen.ManageTeraClassificationsScreen);

/*
 * @author Sourabh Goyal, Bridge Solutions Group
 */

Ext.namespace('sc.item.teranewscreen');

sc.item.teranewscreen.ManageTeraClassificationsScreenUIConfig = function() {
    return {
        xtype: "screen",
        sciId: "teraNewScreenExtnSCIID",
        header: false,
        layout: "anchor",
        autoScroll: true,
        items: [{
            xtype: "panel",
            sciId: "seaMainPanel",
            layout: "anchor",
            items: [{
                defid: "customct",
                sciId: "seaCustomPrimaryInfoPanel",
                xtype: "seaItemPrimaryInfo",
                text: "custom_component"
            },
            {
                xtype: "panel",
                sciId: "seaStandardPanel",
                layout: "anchor",
                items: [{
                    xtype: "panel",
                    sciId: "seaStandardClassifications",
                    layout: "table",
                    title: this.b_Standard_Classifications,
                    items: [{
                        xtype: "label",
                        sciId: "seaHarmonizedCodeLabel",
                        text: this.b_HarmonizedCode_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaHarmonizedCodeTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('HarmonizedCode'),
                        readOnly: this.readOnly('HarmonizedCode'),
                        ctCls: "sc-plat-triggerfield-text",
                        onTriggerClick: this.loadClassificationLookup,
                        fieldName: "HarmonizedCode",
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.HarmonizedCode",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.HarmonizedCode"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaScheduleCodeLabel",
                        text: this.b_ScheduleCode_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaScheduleCodeTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('Schedule_B_Code'),
                        readOnly: this.readOnly('Schedule_B_Code'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "Schedule_B_Code",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.Schedule_B_Code",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.Schedule_B_Code"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaCommodityCodeLabel",
                        text: this.b_CommodityCode_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaCommodityCodeTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('CommodityCode'),
                        readOnly: this.readOnly('CommodityCode'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "CommodityCode",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.CommodityCode",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.CommodityCode"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaNMFCCodeLabel",
                        text: this.b_NMFCCode_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaNMFCCodeTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('NMFCCode'),
                        readOnly: this.readOnly('NMFCCode'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "NMFCCode",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.NMFCCode",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.NMFCCode"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaECCNNumberLabel",
                        text: this.b_ECCNNumber_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaECCNNumberTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('ECCNNo'),
                        readOnly: this.readOnly('ECCNNo'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "ECCNNo",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.ECCNNo",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.ECCNNo"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaNMFCClassLabel",
                        text: this.b_NMFCClass_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaNMFCClassTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('NMFCClass'),
                        readOnly: this.readOnly('NMFCClass'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "NMFCClass",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.NMFCClass",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.NMFCClass"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaUNSPSCLabel",
                        text: this.b_UNSPSC_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaUNSPSCTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('UNSPSC'),
                        readOnly: this.readOnly('UNSPSC'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "UNSPSC",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.UNSPSC",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.UNSPSC"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaNAICSCodeLabel",
                        text: this.b_NAICSCode_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaNAICSCodeTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('NAICSCode'),
                        readOnly: this.readOnly('NAICSCode'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "NAICSCode",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.NAICSCode",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.NAICSCode"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaTaxLabel",
                        text: this.b_TaxProductClass_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaTaxTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('TaxProductCode'),
                        readOnly: this.readOnly('TaxProductCode'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "TaxProductCode",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.TaxProductCode",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.TaxProductCode"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaHazardousLabel",
                        text: this.b_HazardousMaterialClass_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaHazardousTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('HazmatClass'),
                        readOnly: this.readOnly('HazmatClass'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "HazmatClass",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.HazmatClass",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.HazmatClass"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaModelLabel",
                        text: this.b_Model_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaModelTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('Model'),
                        readOnly: this.readOnly('Model'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "Model",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.Model",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.Model"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "label32"
                    },
                    {
                        xtype: "checkbox",
                        sciId: "seaHazardousCheckbox",
                        boxLabel: this.b_IsHazardousItem_lbl,
                        ctCls: "sc-plat-checkboxlabel",
                        colspan: 2,
                        bindingData: {
                            defid: "object",
                            checkedValue: "Y",
                            unCheckedValue: "N",
                            sourceBinding: "getItemDetails:Item.PrimaryInformation.IsHazmat",
                            targetBinding: ["getManageItem:ItemList.Item.PrimaryInformation.IsHazmat"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaCostClassificationLabel",
                        text: this.b_CostPostingClassification_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "combo",
                        sciId: "seaCostClassificationCombo",
                        displayField: "CodeShortDescription",
                        valueField: "CodeValue",
                        mode: "local",
                        triggerAction: "all",
                        editable: false,
                        cls: "sbc-standard-combo",
                        resizable: true,
                        ctCls: "sc-plat-combo-selection",
                        listClass: "sc-plat-combo-list-item",
                        bindingData: {
                            defid: "object",
                            optionsBinding: "getCommonCodeListForPosting:CommonCodeList.CommonCode",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.PostingClassification",
                            targetBinding: "getManageItem:ItemList.Item.ClassificationCodes.PostingClassification"
                        },
                        store: new Ext.data.JsonStore({
                            defid: "jsonstore",
                            fields: ["CodeShortDescription", "CodeValue"],
                            sortInfo: {
                                defid: "object",
                                field: "CodeShortDescription",
                                direction: "ASC"
                            }
                        })
                    }],
                    autoHeight: true,
                    border: false,
                    header: false,
                    autoWidth: true,
                    defaults: {
                        defid: "defaults",
                        cls: "x-panel-item",
                        columnWidth: 0.25
                    },
                    layoutConfig: {
                        defid: "tableLayoutConfig",
                        columns: 4
                    }
                }],
                border: false,
                header: true,
                title: this.b_Standard_Classifications,
                collapsible: false,
                sctype: "GroupPanel",
                layoutConfig: {
                    defid: "layoutConfig",
                    columns: 4
                }
            },
            {
                xtype: "panel",
                sciId: "seaOrganiztionPanel",
                title: this.b_Organization_Classifications,
                layout: "anchor",
                items: [{
                    xtype: "panel",
                    sciId: "seaOrganizationClassifications",
                    layout: "table",
                    title: "Organization Classifications",
                    items: [{
                        xtype: "label",
                        sciId: "seaProductTypeLabel",
                        text: this.b_ProductLine_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaProductTypeTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('ProductLine'),
                        readOnly: this.readOnly('ProductLine'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "ProductLine",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.PrimaryInformation.ProductLine",
                            targetBinding: ["getManageItem:ItemList.Item.PrimaryInformation.ProductLine"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "label33"
                    },
                    {
                        xtype: "label",
                        sciId: "seaItemTypeLabel",
                        text: this.b_ItemType_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaItemTypeTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('ItemType'),
                        readOnly: this.readOnly('ItemType'),
                        ctCls: "sc-plat-triggerfield-text",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        fieldName: "ItemType",
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.PrimaryInformation.ItemType",
                            targetBinding: ["getManageItem:ItemList.Item.PrimaryInformation.ItemType"]
                        }
                    }],
                    autoHeight: true,
                    autoWidth: true,
                    header: false,
                    border: false,
                    defaults: {
                        defid: "defaults",
                        cls: "x-panel-item"
                    },
                    layoutConfig: {
                        defid: "tableLayoutConfig",
                        columns: 5
                    }
                }],
                collapsible: false,
                sctype: "GroupPanel",
                layoutConfig: {
                    defid: "layoutConfig",
                    columns: 4
                }
            },
            {
                xtype: "panel",
                sciId: "seaOperationalPanel",
                title: this.b_Operational_Classifications,
                layout: "anchor",
                items: [{
                    xtype: "panel",
                    sciId: "seaOperationalClassifications",
                    layout: "table",
                    title: "Operational Classifications",
                    autoHeight: true,
                    autoWidth: true,
                    items: [{
                        xtype: "label",
                        sciId: "seaStorageTypeLabel",
                        text: this.b_StorageType_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaStorageTypeTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('StorageType'),
                        readOnly: this.readOnly('StorageType'),
                        ctCls: "sc-plat-triggerfield-text",
                        fieldName: "StorageType",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.StorageType",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.StorageType"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaPickingTypeLabel",
                        text: this.b_PickingType_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "trigger",
                        sciId: "seaPickingTypeTrigger",
                        triggerClass: "x-form-search-trigger",
                        hideTrigger: this.hideTrigger('PickingType'),
                        ctCls: "sc-plat-triggerfield-text",
                        readOnly: this.readOnly('PickingType'),
                        fieldName: "PickingType",
                        onTriggerClick: this.loadClassificationLookup,
                        scope: this,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.PickingType",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.PickingType"]
                        }
                    },
                    {
                        xtype: "label",
                        sciId: "seaVelocityCodeLabel",
                        text: this.b_VelocityCode_lbl,
                        cls: "sc-plat-label"
                    },
                    {
                        xtype: "combo",
                        sciId: "seaVelocityCodeCombo",
                        displayField: "CodeShortDescription",
                        valueField: "CodeValue",
                        mode: "local",
                        triggerAction: "all",
                        editable: false,
                        ctCls: "sc-plat-combo-selection",
                        listClass: "sc-plat-combo-list-item",
                        cls: "sbc-standard-combo",
                        resizable: true,
                        bindingData: {
                            defid: "object",
                            optionsBinding: "getCommonCodeListForVelocityCode:CommonCodeList.CommonCode",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.VelocityCode",
                            targetBinding: "getManageItem:ItemList.Item.ClassificationCodes.VelocityCode"
                        },
                        store: new Ext.data.JsonStore({
                            defid: "jsonstore",
                            fields: ["CodeShortDescription", "CodeValue"],
                            sortInfo: {
                                defid: "object",
                                field: "CodeShortDescription",
                                direction: "ASC"
                            }
                        })
                    },
                    {
                        xtype: "label",
                        sciId: "label31"
                    },
                    {
                        xtype: "checkbox",
                        sciId: "seaOperationalCheckBox",
                        boxLabel: this.b_OperationConfigurationComplete_Y,
                        ctCls: "sc-plat-checkboxlabel",
                        colspan: 2,
                        bindingData: {
                            defid: "object",
                            sourceBinding: "getItemDetails:Item.ClassificationCodes.OperationalConfigurationComplete",
                            targetBinding: ["getManageItem:ItemList.Item.ClassificationCodes.OperationalConfigurationComplete"],
                            checkedValue: "Y",
                            unCheckedValue: "N"
                        }
                    }],
                    header: false,
                    border: false,
                    defaults: {
                        defid: "defaults",
                        cls: "x-panel-item"
                    },
                    layoutConfig: {
                        defid: "tableLayoutConfig",
                        columns: 4
                    }
                }],
                collapsible: false,
                sctype: "GroupPanel",
                layoutConfig: {
                    defid: "layoutConfig",
                    columns: 4
                }
            }],
            sctype: "ScreenMainPanel",
            header: true,
            layoutConfig: {
                defid: "layoutConfig",
                columns: 4
            }
        }],
        border: false,
        sctype: "ScreenBasePanel"
    };
}

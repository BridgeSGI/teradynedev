<%
/**
 * @author Sourabh Goyal, Bridge Solutions Group
 */
%>

<%@page import="com.sterlingcommerce.sbc.core.utils.SBCJSONUtils"%>
<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="org.w3c.dom.Element"%>

<%
response.setContentType("text/javascript");
SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);
Object obj = uiContext.getAttribute("manageRespNameOnlyServiceOutput");
String json = SBCJSONUtils.getJSONFromXML(request, response,(Element)obj);
%>
{"output":<%=json%>}
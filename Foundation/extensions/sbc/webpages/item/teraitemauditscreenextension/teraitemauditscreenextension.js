/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.item.teraitemauditscreenextension');

sc.item.teraitemauditscreenextension.TeraAuditScreenClassExtension = function() {
    sc.item.teraitemauditscreenextension.TeraAuditScreenClassExtension.superclass.constructor.call(this);
}
Ext.extend(sc.item.teraitemauditscreenextension.TeraAuditScreenClassExtension, sc.plat.ui.Extension, {
    className: 'sc.item.teraitemauditscreenextension.TeraAuditScreenClassExtension',
    getConfigOverlays: sc.item.teraitemauditscreenextension.TeraAuditScreenClassExtensionConfigOverlays,
    namespaces: {
        target: ["ItemAuditTargetModel"],
        source: ["getTeraItemAuditOutputNS"]
    },
    namespacesDesc: {
        targetDesc: ["Target Model for Audit List Screen"],
        sourceDesc: ["getAuditList Output NS for Audit List of an item"]
    },
	
	returnFormattedAuditDate:function(startDate){
		var formattedStartDate=startDate.dateFormat("m/d/Y H:i:s");
		return formattedStartDate;
	}
});
sc.plat.ScreenExtnMgr.add('sc.item.teraitemauditscreen.TeraAuditScreenClass', sc.item.teraitemauditscreenextension.TeraAuditScreenClassExtension);
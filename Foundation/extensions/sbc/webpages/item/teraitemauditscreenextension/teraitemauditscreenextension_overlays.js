/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.item.teraitemauditscreenextension');

sc.item.teraitemauditscreenextension.TeraAuditScreenClassExtensionConfigOverlays = function() {
    return [{
        op: "move",
        remove: {
            op: "remove",
            ownerId: "seaItemAssociations",
            property: "columns",
            sciId: "seaItemStartDateColumn"
        },
        insert: {
            op: "insert",
            ownerId: "seaItemAssociations",
            property: "columns",
            refId: "seaItemNameColumn",
            pos: "before"
        }
    },
    {
        op: "change",
        sciId: "teraItemAuditScreenSCIID",
        config: {
            xtype: "screen"
        }
    },
    {
        op: "change",
        sciId: "seaCategoryPanel",
        config: {
            xtype: "panel",
            hidden: true,
            title: "",
            disabled: true
        }
    },
    {
        op: "change",
        sciId: "seaCategoryAssociations",
        config: {
            xtype: "grid",
            hidden: true,
            hideParent: true,
            disabled: true
        }
    },
    {
        op: "change",
        sciId: "seaItemsPanel",
        config: {
            xtype: "panel",
            title: "Item Audits"
        }
    },
    {
        op: "move",
        remove: {
            op: "remove",
            ownerId: "seaCategoryOrgCode",
            property: "items",
            sciId: "seaCategoryPanel"
        },
        insert: {
            op: "insert",
            ownerId: "seaHolderPanel",
            property: "items",
            refId: null,
            pos: "in"
        }
    },
    {
        op: "change",
        sciId: "seaItemDescColumn",
        config: {
            defid: "grid-column",
            hidden: true,
            hideable: true
        }
    },
    {
        op: "change",
        sciId: "seaItemQuantityColumn",
        config: {
            defid: "grid-column",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaItemPriorityColumn",
        config: {
            defid: "grid-column",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaItemEndDateColumn",
        config: {
            defid: "grid-column",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaItemStartDateColumn",
        config: {
            defid: "grid-column",
            header: "Audit Date",
            dataIndex: "Modifyts",
            width: 275,
            css: "sbc-cellValue-AlignLeft",
            renderer: this.returnFormattedAuditDate,
            bindingData: {
                defid: "object",
                tAttrBinding: "ModifytsTB",
                sFieldConfig: {
                    defid: "object",
                    mapping: "Modifyts"
                }
            }
        }
    },
    {
        op: "change",
        sciId: "seaItemNameColumn",
        config: {
            defid: "grid-column",
            header: "Modified By",
            dataIndex: "Modifyuserid",
            width: 277,
            css: "sbc-cellValue-AlignLeft",
            bindingData: {
                defid: "object",
                tAttrBinding: "ModifyuseridTB",
                sFieldConfig: {
                    defid: "object",
                    mapping: "Modifyuserid"
                }
            }
        }
    },
    {
        op: "change",
        sciId: "seaItemAssociations",
        config: {
            xtype: "grid",
            colspan: 2,
            height: 230,
            width: 600,
            autoHeight: false,
            bindingData: {
                defid: "object",
                sourceBinding: "getTeraItemAuditOutputNS:AuditDetailListOutput.Audit",
                targetBinding: ["ItemAuditTargetModel:AuditList.Audit"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaAddItemButton",
        config: {
            xtype: "button",
            text: "View Details",
            disabled: true,
            tooltip: "View Details of Selected Audit"
        }
    },
    {
        op: "change",
        sciId: "sbcModifyItemButton",
        config: {
            xtype: "button",
            hidden: true,
            disabled: false,
            width: 200
        }
    },
    {
        op: "change",
        sciId: "seaRemoveItemButton",
        config: {
            xtype: "button",
            hidden: true,
            disabled: false
        }
    }];
}
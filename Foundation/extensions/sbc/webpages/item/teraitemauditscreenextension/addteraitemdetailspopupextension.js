/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.item.teraitemauditscreenextension');

sc.item.teraitemauditscreenextension.TeraAuditDetailPopupClassExtension = function() {
    sc.item.teraitemauditscreenextension.TeraAuditDetailPopupClassExtension.superclass.constructor.call(this);
}
Ext.extend(sc.item.teraitemauditscreenextension.TeraAuditDetailPopupClassExtension, sc.plat.ui.Extension, {
    className: 'sc.item.teraitemauditscreenextension.TeraAuditDetailPopupClassExtension',
    getConfigOverlays: sc.item.teraitemauditscreenextension.TeraAuditDetailPopupClassExtensionConfigOverlays,
    namespaces: {
        target: [],
        source: []
    },
    namespacesDesc: {
        targetDesc: [],
        sourceDesc: []
    }
});
sc.plat.ScreenExtnMgr.add('sc.item.teraitemauditscreen.TeraAuditDetailPopupClass', sc.item.teraitemauditscreenextension.TeraAuditDetailPopupClassExtension);
/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.item.teraitemauditscreenextension');

sc.item.teraitemauditscreenextension.TeraAuditDetailPopupClassExtensionConfigOverlays = function() {
    return [{
        op: "move",
        remove: {
            op: "remove",
            ownerId: "seaPanelMain",
            property: "items",
            sciId: "seaPaneListPrice"
        },
        insert: {
            op: "insert",
            ownerId: "seaPanelMain",
            property: "items",
            refId: null,
            pos: "in"
        }
    },
    {
        op: "move",
        remove: {
            op: "remove",
            ownerId: "seaFieldSetCondition",
            property: "items",
            sciId: "seaGridQtyRanges"
        },
        insert: {
            op: "insert",
            ownerId: "teraAuditDetailPopupSCIID",
            property: "items",
            refId: null,
            pos: "in"
        }
    },
    {
        op: "move",
        remove: {
            op: "remove",
            ownerId: "teraAuditDetailPopupSCIID",
            property: "items",
            sciId: "seaPanelMain"
        },
        insert: {
            op: "insert",
            ownerId: "teraAuditDetailPopupSCIID",
            property: "items",
            refId: null,
            pos: "in"
        }
    },
    {
        op: "change",
        sciId: "teraAuditDetailPopupSCIID",
        config: {
            xtype: "screen"
        }
    },
    {
        op: "change",
        sciId: "panel3",
        config: {
            xtype: "panel",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaLabelItemPrices",
        config: {
            xtype: "label",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaPanelDateRanges",
        config: {
            xtype: "panel",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaPanelMain",
        config: {
            xtype: "panel",
            header: true,
            title: "Audit Details",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaFieldSetCondition",
        config: {
            xtype: "panel",
            title: "Audit Details",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaLinkAddAnotherTier",
        config: {
            xtype: "sclink",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaPaneListPrice",
        config: {
            xtype: "panel",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaGridQtyRanges",
        config: {
            xtype: "editorgrid",
            header: true,
            title: "Audit Details",
            bindingData: {
                defid: "object",
                sourceBinding: "getAdditionalAuditData:Attributes.Attribute"
            }
        }
    },
    {
        op: "change",
        sciId: "seaGridClmQtyFrom",
        config: {
            defid: "grid-column",
            header: "Name",
            dataIndex: "Name",
            editable: false,
            bindingData: {
                defid: "object",
                sFieldConfig: {
                    defid: "object",
                    mapping: "Name",
                    type: "string"
                }
            }
        }
    },
    {
        op: "change",
        sciId: "seaGridQtyTo",
        config: {
            defid: "grid-column",
            header: "Old Value",
            dataIndex: "OldValue",
            editable: false,
            bindingData: {
                defid: "object",
                sFieldConfig: {
                    defid: "object",
                    mapping: "OldValue",
                    type: "string"
                }
            }
        }
    },
    {
        op: "change",
        sciId: "seaGridClmPrice",
        config: {
            defid: "grid-column",
            header: "New Value",
            dataIndex: "NewValue",
            editable: false,
            bindingData: {
                defid: "object",
                sFieldConfig: {
                    defid: "object",
                    mapping: "NewValue",
                    type: "string"
                }
            }
        }
    },
    {
        op: "change",
        sciId: "seaGridClmDelete",
        config: {
            defid: "grid-column",
            hideable: true,
            hidden: true
        }
    }];
}
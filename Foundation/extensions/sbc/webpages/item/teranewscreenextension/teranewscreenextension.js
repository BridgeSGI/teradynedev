/*
	@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.item.teranewscreenextension');

sc.item.teranewscreenextension.ManageTeraClassificationsScreenExtension = function() {
    sc.item.teranewscreenextension.ManageTeraClassificationsScreenExtension.superclass.constructor.call(this);
}
Ext.extend(sc.item.teranewscreenextension.ManageTeraClassificationsScreenExtension, sc.plat.ui.Extension, {
    className: 'sc.item.teranewscreenextension.ManageTeraClassificationsScreenExtension',
    getConfigOverlays: sc.item.teranewscreenextension.ManageTeraClassificationsScreenExtensionConfigOverlays,
    namespaces: {
        target: [],
        source: ["getCommonCodeListForHarmonizedCodeNS","getCommonCodeListForECCNCodeNS","getCommonCodeListForItemTypeCodeNS","getCommonCodeListForImportAgenciesCodeNS",
				 "getCommonCodeListForProductGroupNS","getCommonCodeListForOEMSNNS","getCommonCodeListForSupportStatusNS","getCommonCodeListForPlannerCodeNS",
				 "getCommonCodeListForComponentCodeNS","getCommonCodeListForRepairModelNS","getCommonCodeListForInvLocationNS","getCommonCodeListForWarrantyCodeNS","getCommonCodeListForNoAgreeFlagNS","getCommonCodeListForIsShippedAloneNS","getCommonCodeListForIsCriticalPartNS","TERProductGroupServiceNS","getCommonCodeListForRepCodeNS","getCommonCodeListForDistributionCodeNS","getCommonCodeListForRepComplexityNS","getCommonCodeListForRepProductNS","getCommonCodeListForSpecReviewNS","getUserListForSPRAttnToNS","getCommonCodeListForPlatformNS","getCommonCodeListForPTExceptionFlagNS","getCommonCodeListForLifecycleCategoryNS","getBoxIDComboBoxValuesServiceOutputNS","DivCodeOnlyServiceOutputNS","MfgRespIDOnlyServiceOutputNS","MfgRespNameOnlyServiceOutputNS","RepairRespIDOnlyServiceOutputNS","RepairRespNameOnlyServiceOutputNS","getVariousCommonCodeDescriptionOutputNS"]
    },
    namespacesDesc: {
        targetDesc: [],
        sourceDesc: ["Teradyne Harmonized Code List","Teradyne ECCN Code List","Teradyne Item Type Code List","Teradyne Import Agencies Code List","Teradyne Product Group Code 	List","Teradyne OEM SN Required Code List","Teradyne Support Status Code List","Teradyne Planner Code Code List","Teradyne Component Code Code List","Teradyne Repair Model Code List","Teradyne Default Inv Location Code List","Teradyne Warranty Code Code List","Teradyne No Agreement Flag Code List","Teradyne Is Shipped Alone Code List","Teradyne Is Critical Part Code List","TERProductGroupService Mfg/Div Codes","Teradyne Repair Code Code List","Teradyne Distribution Code Code List","Teradyne Rep Complexity Code List","Teradyne Rep Product Code List","Teradyne Spec Review Code List","Teradyne SPR Attn To Users List","Teradyne Platform Code List","Teradyne PT Exception Flag Code List","Teradyne Lifecycle Category Code List","Get Box ID Combo Box Values Output","Div Code Output NS","Mfg Resp ID Output NS","Mfg Resp Name Output NS","Repair Resp ID Output NS","Repair Resp Name Output NS","Various Common Code Description based on Common Code value stored in YFS_ITEM Output NS"]
    },
	
	onMfgDivCodeSelection : function(combo, record, index){
		var manageItemModel = this.getTargetModel("getManageItem")[0];
		var mfgDivCodeSelected=combo.getValue();
		manageItemModel.ItemList.Item.Extn.DivCodeComboSelectedValue=mfgDivCodeSelected;
		var paramsObj = {};
		paramsObj["manageRespIdOnlyServiceInput"] = Ext.encode(manageItemModel);							
		seaAjaxUtils.request({
				action : "manageDivCodeSelection",
				actionNS : sc.sbc.App.ItemStrutsNS,
				params : paramsObj,
				success :this.resHandlerForMfgRespIDServiceCall,
				scope : this
		});
    },

	/**
	 * Handler when the Service call to TERProductGroupListGetRespIDOnlyClass is successful
	 */
	resHandlerForMfgRespIDServiceCall : function(res, options) {
		var results=Ext.decode(res.responseText);
		this.setModel(results.output, "MfgRespIDOnlyServiceOutputNS", {clearOldVals :true});		
	},
	
	onMfgRespIDSelection : function(combo, record, index){
		var manageItemModel = this.getTargetModel("getManageItem")[0];
		var mfgRespIDSelected=combo.getValue();
		manageItemModel.ItemList.Item.Extn.RespIDComboSelectedValue=mfgRespIDSelected;
		var paramsObj = {};
		paramsObj["manageRespNameOnlyServiceInput"] = Ext.encode(manageItemModel);							
		seaAjaxUtils.request({
				action : "manageRespIDSelection",
				actionNS : sc.sbc.App.ItemStrutsNS,
				params : paramsObj,
				success :this.resHandlerForMfgRespNameServiceCall,
				scope : this
		});
    },

	/**
	 * Handler when the Service call to TERProductGroupListGetRespNameOnlyClass is successful
	 */
	resHandlerForMfgRespNameServiceCall : function(res, options) {
		var results=Ext.decode(res.responseText);
		this.setModel(results.output, "MfgRespNameOnlyServiceOutputNS", {clearOldVals :true});		
	},
	
	onRepairDivCodeSelection : function(combo, record, index){
		var manageItemModel = this.getTargetModel("getManageItem")[0];
		var repairDivCodeSelected=combo.getValue();
		manageItemModel.ItemList.Item.Extn.DivCodeComboSelectedValue=repairDivCodeSelected;
		var paramsObj = {};
		paramsObj["manageRespIdOnlyServiceInput"] = Ext.encode(manageItemModel);							
		seaAjaxUtils.request({
				action : "manageDivCodeSelection",
				actionNS : sc.sbc.App.ItemStrutsNS,
				params : paramsObj,
				success :this.resHandlerForRepairRespIDServiceCall,
				scope : this
		});
    },

	/**
	 * Handler when the Service call to TERProductGroupListGetRespIDOnlyClass is successful
	 */
	resHandlerForRepairRespIDServiceCall : function(res, options) {
		var results=Ext.decode(res.responseText);
		this.setModel(results.output, "RepairRespIDOnlyServiceOutputNS", {clearOldVals :true});		
	},
	
	onRepairRespIDSelection : function(combo, record, index){
		var manageItemModel = this.getTargetModel("getManageItem")[0];
		var repairRespIDSelected=combo.getValue();
		manageItemModel.ItemList.Item.Extn.RespIDComboSelectedValue=repairRespIDSelected;
		var paramsObj = {};
		paramsObj["manageRespNameOnlyServiceInput"] = Ext.encode(manageItemModel);							
		seaAjaxUtils.request({
				action : "manageRespIDSelection",
				actionNS : sc.sbc.App.ItemStrutsNS,
				params : paramsObj,
				success :this.resHandlerForRepairRespNameServiceCall,
				scope : this
		});
    },

	/**
	 * Handler when the Service call to TERProductGroupListGetRespNameOnlyClass is successful
	 */
	resHandlerForRepairRespNameServiceCall : function(res, options) {
		var results=Ext.decode(res.responseText);
		this.setModel(results.output, "RepairRespNameOnlyServiceOutputNS", {clearOldVals :true});		
	}

});
sc.plat.ScreenExtnMgr.add('sc.item.teranewscreen.ManageTeraClassificationsScreen', sc.item.teranewscreenextension.ManageTeraClassificationsScreenExtension);
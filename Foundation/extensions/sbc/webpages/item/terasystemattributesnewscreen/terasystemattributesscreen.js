/*
 * @author Sourabh Goyal, Bridge Solutions Group
 */
 
Ext.namespace('sc.item.terasystemattributesnewscreen');

sc.item.terasystemattributesnewscreen.ClassTeraSystemAttributesScreen = function(config) {
	Ext.apply(this,config);
	sc.item.terasystemattributesnewscreen.ClassTeraSystemAttributesScreen.superclass.constructor.call(this, config);
}
Ext.extend(
				sc.item.terasystemattributesnewscreen.ClassTeraSystemAttributesScreen,
				sc.plat.ui.ExtensibleScreen, {
					className : 'sc.item.terasystemattributesnewscreen.ClassTeraSystemAttributesScreen',
					getUIConfig : sc.item.terasystemattributesnewscreen.ClassTeraSystemAttributesScreenUIConfig,
					sItemID : "", //  ItemID
					sUnitOfMeasure : "", // Item UOM
					sOrgCode : "",// Organization Code
//					itemPrimaryInfo	: {Item:{ItemID: "TV0005" , UnitOfMeasure: "EACH",OrganizationCode : "DEFAULT" }},
					itempopupWin : null, //item popup window
					
					namespaces : {
						source : ["getItemDetails","getItemDetailsOutput",
								"getCommonCodeListForVelocityCode",
								"getCommonCodeListForPosting"],
						target : ["getManageItem"]
					},
					
					populateData : function(getItemDetailsOutput,
							getClassificationPurposeListOutput,
							getCommonCodeListForPostingOutput,
							getCommonCodeListForVelocityCodeOutput,itemdetailsNS) {
								
//						this.itemPrimaryInfo = itemdetailsNS;--To do
								
//						this.sItemID = this.itemPrimaryInfo.Item.ItemID;
//						this.sUnitOfMeasure =this.itemPrimaryInfo.Item.UnitOfMeasure;
//						this.sOrgCode = this.itemPrimaryInfo.Item.OrganizationCode;
						
						var headerPanel = this.find("sciId", "seaMainPanel")[0];
						itemUtils.setPanelTitle(headerPanel, "Teradyne System Type Attributes", this);
						var primaryinfo = this.find("sciId", "seaCustomPrimaryInfoPanel")[0];
                 		primaryinfo.initialize();
						
						this.setModel(getItemDetailsOutput, "getItemDetails", {
									clearOldVals : true
								});
						this.setModel(getItemDetailsOutput, "getItemDetailsOutput", {
									clearOldVals : true
								});								
						this.setModel(getCommonCodeListForVelocityCodeOutput,
								"getCommonCodeListForVelocityCode", {
									clearOldVals : true
								});

						this.setModel(getCommonCodeListForPostingOutput,
								"getCommonCodeListForPosting", {
									clearOldVals : true
								});
					},

					hideTrigger : function(value) {
						if(this.attrMap){
							var map = this.attrMap;
						  
							if (map[value]) {
								return false;
							} else {
								return true;
							}
						}else{
							return false;
						}
						 
					},
					
					
					readOnly : function(value) {
						if(this.attrMap){
							var map = this.attrMap;
						  
							if (map[value]) {
								return true;
							} else {
								return false;
							}
						}else{
							return true;
						}
						 
					},

					saveAction : function() {
						
						var manageItemModel = this.getTargetModel("getManageItem")[0];
						
						var paramsObj = {};
						paramsObj["manageiteminput"] = Ext.encode(manageItemModel);							
								    itemUtils.callItemStrutsAction("manageitemclassifications_save", {
								    	params : paramsObj,
								    	success :this.resHandlerForManageItem,
								    	successMsg : sc.sbc.ui.messages.getSaveSuccessfullText("Item"),
								    	scope :this
							});
								
//						manageItemModel.ItemList.Item.ItemID = this.sItemID;
//						manageItemModel.ItemList.Item.UnitOfMeasure = this.sUnitOfMeasure;
//						manageItemModel.ItemList.Item.OrganizationCode = this.sOrgCode;
								
//						seaAjaxUtils.request({
//							action : "manageitemclassifications_save",
//							actionNS : sc.sbc.App.ItemStrutsNS,
//							inputNS : "manageiteminput",
//							inputObj : manageItemModel,
//							success : this.resHandlerForManageItem,
//							scope : this
//						});				
						

					},
					
				/**
				 * Handler when the APi call to save data is successful
				 */
				resHandlerForManageItem : function(res, options) {
					this.clean();
				},
					
				/**
				 * This method is called to set the item model for other screens
				 */	
				openItemRelatedTask : function(strutsAction){
					var obj = {};
					obj["itemdetailsNS"] = Ext.encode(this.itemPrimaryInfo);
					seaAjaxUtils.request({
						actionNS : sc.sbc.App.ItemStrutsNS,
						action : strutsAction,
						success : this.relatedTaskActionSuccess,
						params : obj,
						scope: this
					});
				},
				
				/**
				 * Handler when the realted task action is successful
				 */
				relatedTaskActionSuccess : function(res, options){
					eval(res.responseText);
				},
				
				loadClassificationLookup : function(e){
					if(this.scope.attrMap){
							var map = this.scope.attrMap;	
							if (map[this.fieldName]) {
								var classificationPurposeElem = map[this.fieldName];
								var categoryDomainElem = classificationPurposeElem.CategoryDomain;
								var sCategoryDomainKey = categoryDomainElem.CategoryDomainKey;
								sc.sbc.util.LoadEntityUtils.loadClassificationTreeLookup(sCategoryDomainKey, this.scope.populateItemField, this.scope,this.sciId);
							}
							
						}
				},
				
				 populateItemField : function(category,sciid){
				 	var sCategoryID = category[0].CategoryID;
				 	
				 	var triggerField = this.find("sciId", sciid)[0];
				 	triggerField.setValue(sCategoryID);
				 	
//				 	this.setDirty(true);
//    				var sAttributeName = this.getAttributeName(this.parentScreen);
				 },
				
				processTaskCategory : function(category, categoryId){
						return itemUtils.processTaskCategoryForItems(category, this.getCurrentTaskId());
				},
				getCurrentTaskId : function(){
						return "TERA_SBCITM0002";
				}
		});
Ext.reg('teraSystemTypeAttributesRegex',sc.item.terasystemattributesnewscreen.ClassTeraSystemAttributesScreen);

<%
/*
 * @author Sourabh Goyal, Bridge Solutions Group
 */
%>

<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="com.sterlingcommerce.sbc.core.utils.SBCJSONUtils"%> 
<%@page import="com.sterlingcommerce.framework.utils.SCXmlUtils"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="java.io.*"%>
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.dom.*"%>
<%@page import="javax.xml.transform.stream.*"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIUserPreferences"%>

<%

SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);

Object ret = uiContext.getAttribute("getitemdetails");
Element e = (Element)ret;
String json = SBCJSONUtils.getJSONFromXML(request, response, e);

Object ret1 = uiContext.getAttribute("getclassificationpurposelist");
Element e1=(Element)ret1;
String json1 = SBCJSONUtils.getJSONFromXML(request, response, e1);

Object ret2 = uiContext.getAttribute("getcommoncodelistforposting");
Element e2=(Element)ret2;
String json2 = SBCJSONUtils.getJSONFromXML(request, response, e2);

Object ret3 = uiContext.getAttribute("getcommoncodelistforvelocitycode");
Element e3=(Element)ret3;
String json3 = SBCJSONUtils.getJSONFromXML(request, response, e3);

Object ret31 = uiContext.getAttribute("getcommoncodelistforplatformoutput");
Element e31=(Element)ret31;
String json31 = SBCJSONUtils.getJSONFromXML(request, response, e31);

Object ret32 = uiContext.getAttribute("getcommoncodelistforflagcodeoutput");
Element e32=(Element)ret32;
String json32 = SBCJSONUtils.getJSONFromXML(request, response, e32);

Object ret33 = uiContext.getAttribute("getcommoncodelistforlifecyclecategoryoutput");
Element e33=(Element)ret33;
String json33 = SBCJSONUtils.getJSONFromXML(request, response, e33);

String itemdetailsNS = uiContext.getRequest().getParameter("itemdetailsNS");

%>

function loadItemSystemAttributes(){

var getItemDetailsOutput = '<%=json%>';
var getClassificationPurposeListOutput = '<%=json1%>';
var getCommonCodeListForPostingOutput = '<%=json2%>';
var getCommonCodeListForVelocityCodeOutput = '<%=json3%>';
var getCommonCodeListForPlatformOutput = '<%=json31%>';
var getCommonCodeListForPTExceptionFlagOutput = '<%=json32%>';
var getCommonCodeListForLifeCycleCategoryOutput = '<%=json33%>';
var itemdetailsNS = '<%=itemdetailsNS%>';


		getClassificationPurposeListOutput = Ext.decode(getClassificationPurposeListOutput);
        var classificationPurposeList = getClassificationPurposeListOutput.ClassificationPurposeList.ClassificationPurpose;
       
		if(classificationPurposeList){
			var len = classificationPurposeList.length;
	        if(len>0){
				var attributeMap = {};
	        }
	 
			for (var i = 0; i < len; i++) {
	         var classificationPurposeObj = classificationPurposeList[i];
	         var attrName = classificationPurposeObj.AttributeName;
	          attributeMap[attrName] = classificationPurposeObj;
			}
		}
		getItemDetailsOutput =Ext.decode(getItemDetailsOutput);
		getCommonCodeListForPostingOutput = Ext.decode(getCommonCodeListForPostingOutput);
		getCommonCodeListForVelocityCodeOutput = Ext.decode(getCommonCodeListForVelocityCodeOutput);		
		getCommonCodeListForPlatformOutput = Ext.decode(getCommonCodeListForPlatformOutput);
		getCommonCodeListForPTExceptionFlagOutput = Ext.decode(getCommonCodeListForPTExceptionFlagOutput);
		getCommonCodeListForLifeCycleCategoryOutput = Ext.decode(getCommonCodeListForLifeCycleCategoryOutput);
		
		var itemSystemAttributesObj = new  sc.item.terasystemattributesnewscreen.ClassTeraSystemAttributesScreen({attrMap:attributeMap});
		var noBackLink = itemManager.isBackButtonNotRequired();
		if(noBackLink){
			containerUtil.drawScreen(itemSystemAttributesObj, {title : "Teradyne System Type Attributes", 
															buttons : {btype:sc.sbc.helper.ScreenHelper.SAVE,
																	handler:itemSystemAttributesObj.saveAction}});
		}else{
        	containerUtil.drawScreen(itemSystemAttributesObj, {title : "Teradyne System Type Attributes", 
															back:true, 
															cacheRequired:false,
															buttons : {btype:sc.sbc.helper.ScreenHelper.SAVE,
																	handler:itemSystemAttributesObj.saveAction}});
		}
		itemSystemAttributesObj.postInit = function() {		
		itemSystemAttributesObj.populateData(getItemDetailsOutput,getClassificationPurposeListOutput,
                                       		getCommonCodeListForPostingOutput,
											getCommonCodeListForVelocityCodeOutput,itemdetailsNS);
		itemSystemAttributesObj.extension.setModel(getCommonCodeListForPlatformOutput,"getCommonCodeListForPlatformNS");
		itemSystemAttributesObj.extension.setModel(getCommonCodeListForPTExceptionFlagOutput,"getCommonCodeListForPTExceptionFlagNS");
		itemSystemAttributesObj.extension.setModel(getCommonCodeListForLifeCycleCategoryOutput,"getCommonCodeListForLifecycleCategoryNS");
	}
	itemSystemAttributesObj.postInit();		
};

sc.plat.JSLibManager.loadLibrary("teraSystemAttributesNewScreenTarget", loadItemSystemAttributes);

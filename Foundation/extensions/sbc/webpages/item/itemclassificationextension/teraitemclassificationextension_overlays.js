Ext.namespace('sc.item.itemclassificationextension');

sc.item.itemclassificationextension.ManageClassificationsScreenExtensionConfigOverlays = function() {
    return [{
        op: "insert",
        ownerId: "seaStandardClassifications",
        property: "items",
        refId: "seaScheduleCodeLabel",
        pos: "before",
        config: {
            xtype: "combo",
            sciId: "extn_harmonizedcombo",
            displayField: "CommonCodeValuePlusDescValueTemp",
            valueField: "CodeValue",
            mode: "local",
            triggerAction: "all",
            editable: true,
            cls: "sbc-standard-combo",
            resizable: true,
            ctCls: "sc-plat-combo-selection",
            listClass: "sc-plat-combo-list-item",
            width: 250,
            bindingData: {
                defid: "object",
                optionsBinding: "getCommonCodeListForHarmonizedCodeNS:CommonCodeList.CommonCode",
                sourceBinding: "getVariousCommonCodeDescriptionHarmCodeOutputNS:CommonCodeAndTableDescXml.HarmonizedCodeValueDesc",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.HarmonizedCode"]
            },
            store: new Ext.data.JsonStore({
                defid: "jsonstore",
                fields: ["CommonCodeValuePlusDescValueTemp", "CodeValue"],
                sortInfo: {
                    defid: "object",
                    direction: "ASC",
                    field: "CommonCodeValuePlusDescValueTemp"
                }
            })
        }
    },
    {
        op: "insert",
        ownerId: "seaStandardClassifications",
        property: "items",
        refId: "seaECCNNumberLabel",
        pos: "before",
        config: {
            xtype: "label",
            sciId: "extn_ccatslabel",
            text: "CCATS:",
            cls: "sc-plat-label"
        }
    },
    {
        op: "insert",
        ownerId: "seaStandardClassifications",
        property: "items",
        refId: "seaECCNNumberLabel",
        pos: "before",
        config: {
            xtype: "trigger",
            sciId: "extn_ccatstriggerfield",
            triggerClass: "x-form-search-trigger",
            hideTrigger: true,
            readOnly: false,
            ctCls: "sc-plat-triggerfield-text",
            fieldName: "CCATS",
            onTriggerClick: this.loadClassificationLookup,
            scope: this,
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.Extn.CCATS",
                targetBinding: ["getManageItemTera:ItemList.Item.Extn.CCATS"]
            }
        }
    },
    {
        op: "insert",
        ownerId: "seaStandardClassifications",
        property: "items",
        refId: "seaECCNNumberLabel",
        pos: "before",
        config: {
            xtype: "textfield",
            sciId: "extn_textfield40",
            hidden: true
        }
    },
    {
        op: "insert",
        ownerId: "seaStandardClassifications",
        property: "items",
        refId: "seaECCNNumberLabel",
        pos: "before",
        config: {
            xtype: "textfield",
            sciId: "extn_textfield41",
            hidden: true
        }
    },
    {
        op: "insert",
        ownerId: "seaStandardClassifications",
        property: "items",
        refId: "seaNMFCClassLabel",
        pos: "before",
        config: {
            xtype: "combo",
            sciId: "extn_eccncombo",
            displayField: "CodeShortDescription",
            valueField: "CodeValue",
            mode: "local",
            triggerAction: "all",
            editable: true,
            cls: "sbc-standard-combo",
            resizable: true,
            ctCls: "sc-plat-combo-selection",
            listClass: "sc-plat-combo-list-item",
            bindingData: {
                defid: "object",
                optionsBinding: "getCommonCodeListForECCNCodeNS:CommonCodeList.CommonCode",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.ECCNNo",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.ECCNNo"]
            },
            store: new Ext.data.JsonStore({
                defid: "jsonstore",
                fields: ["CodeShortDescription", "CodeValue"],
                sortInfo: {
                    defid: "object",
                    direction: "ASC",
                    field: "CodeShortDescription"
                }
            })
        }
    },
    {
        op: "insert",
        ownerId: "seaStandardClassifications",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "label",
            sciId: "extn_label42",
            text: "Import Agencies:",
            cls: "sc-plat-label"
        }
    },
    {
        op: "insert",
        ownerId: "seaStandardClassifications",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "combo",
            sciId: "extn_importagenciescombo",
            displayField: "CodeShortDescription",
            valueField: "CodeValue",
            mode: "local",
            triggerAction: "all",
            cls: "sbc-standard-combo",
            resizable: true,
            ctCls: "sc-plat-combo-selection",
            listClass: "sc-plat-combo-list-item",
            editable: false,
            bindingData: {
                defid: "object",
                optionsBinding: "getCommonCodeListForImportAgenciesCodeNS:CommonCodeList.CommonCode",
                sourceBinding: "getItemDetails:Item.Extn.ImportAgency",
                targetBinding: ["getManageItemTera:ItemList.Item.Extn.ImportAgency"]
            },
            store: new Ext.data.JsonStore({
                defid: "jsonstore",
                fields: ["CodeShortDescription", "CodeValue"],
                sortInfo: {
                    defid: "object",
                    direction: "ASC",
                    field: "CodeShortDescription"
                }
            })
        }
    },
    {
        op: "move",
        remove: {
            op: "remove",
            ownerId: "seaStandardPanel",
            property: "items",
            sciId: "seaStandardClassifications"
        },
        insert: {
            op: "insert",
            ownerId: "seaStandardPanel",
            property: "items",
            refId: null,
            pos: "in"
        }
    },
    {
        op: "insert",
        ownerId: "seaOrganizationClassifications",
        property: "items",
        refId: null,
        pos: "in",
        config: {
            xtype: "combo",
            sciId: "extn_itemtypecombo",
            displayField: "CodeShortDescription",
            valueField: "CodeValue",
            mode: "local",
            triggerAction: "all",
            editable: false,
            cls: "sbc-standard-combo",
            ctCls: "sc-plat-combo-selection",
            listClass: "sc-plat-combo-list-item",
            resizable: true,
            allowBlank: false,
            bindingData: {
                defid: "object",
                optionsBinding: "getCommonCodeListForItemTypeCodeNS:CommonCodeList.CommonCode",
                sourceBinding: "getItemDetails:Item.PrimaryInformation.ItemType",
                targetBinding: ["getManageItemTera:ItemList.Item.PrimaryInformation.ItemType"]
            },
            store: new Ext.data.JsonStore({
                defid: "jsonstore",
                fields: ["CodeShortDescription", "CodeValue"],
                sortInfo: {
                    defid: "object",
                    direction: "ASC",
                    field: "CodeShortDescription"
                }
            })
        }
    },
    {
        op: "move",
        remove: {
            op: "remove",
            ownerId: "seaStandardClassifications",
            property: "items",
            sciId: "seaHarmonizedCodeTrigger"
        },
        insert: {
            op: "insert",
            ownerId: "seaMainPanel",
            property: "items",
            refId: "seaOrganiztionPanel",
            pos: "before"
        }
    },
    {
        op: "move",
        remove: {
            op: "remove",
            ownerId: "seaStandardClassifications",
            property: "items",
            sciId: "seaECCNNumberTrigger"
        },
        insert: {
            op: "insert",
            ownerId: "seaMainPanel",
            property: "items",
            refId: "seaOrganiztionPanel",
            pos: "before"
        }
    },
    {
        op: "move",
        remove: {
            op: "remove",
            ownerId: "seaOrganizationClassifications",
            property: "items",
            sciId: "seaItemTypeTrigger"
        },
        insert: {
            op: "insert",
            ownerId: "seaMainPanel",
            property: "items",
            refId: "seaOperationalPanel",
            pos: "before"
        }
    },
    {
        op: "change",
        sciId: "sbcManageClassifications",
        config: {
            xtype: "screen"
        }
    },
    {
        op: "change",
        sciId: "seaHarmonizedCodeTrigger",
        config: {
            xtype: "trigger",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaECCNNumberTrigger",
        config: {
            xtype: "trigger",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaItemTypeTrigger",
        config: {
            xtype: "trigger",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaScheduleCodeTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.Schedule_B_Code",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.Schedule_B_Code"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaCommodityCodeTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.CommodityCode",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.CommodityCode"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaNMFCCodeTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.NMFCCode",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.NMFCCode"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaNMFCClassTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.NMFCClass",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.NMFCClass"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaUNSPSCTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.UNSPSC",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.UNSPSC"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaNAICSCodeTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.NAICSCode",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.NAICSCode"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaTaxTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.TaxProductCode",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.TaxProductCode"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaHazardousTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.HazmatClass",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.HazmatClass"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaModelTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.Model",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.Model"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaHazardousCheckbox",
        config: {
            xtype: "checkbox",
            bindingData: {
                defid: "object",
                checkedValue: "Y",
                unCheckedValue: "N",
                sourceBinding: "getItemDetails:Item.PrimaryInformation.IsHazmat",
                targetBinding: ["getManageItemTera:ItemList.Item.PrimaryInformation.IsHazmat"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaCostClassificationCombo",
        config: {
            xtype: "combo",
            bindingData: {
                defid: "object",
                optionsBinding: "getCommonCodeListForPosting:CommonCodeList.CommonCode",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.PostingClassification",
                targetBinding: "getManageItemTera:ItemList.Item.ClassificationCodes.PostingClassification"
            }
        }
    },
    {
        op: "change",
        sciId: "seaProductTypeTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.PrimaryInformation.ProductLine",
                targetBinding: ["getManageItemTera:ItemList.Item.PrimaryInformation.ProductLine"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaStorageTypeTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.StorageType",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.StorageType"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaPickingTypeTrigger",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.PickingType",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.PickingType"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaVelocityCodeCombo",
        config: {
            xtype: "combo",
            bindingData: {
                defid: "object",
                optionsBinding: "getCommonCodeListForVelocityCode:CommonCodeList.CommonCode",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.VelocityCode",
                targetBinding: "getManageItemTera:ItemList.Item.ClassificationCodes.VelocityCode"
            }
        }
    },
    {
        op: "change",
        sciId: "seaOperationalCheckBox",
        config: {
            xtype: "checkbox",
            bindingData: {
                defid: "object",
                sourceBinding: "getItemDetails:Item.ClassificationCodes.OperationalConfigurationComplete",
                targetBinding: ["getManageItemTera:ItemList.Item.ClassificationCodes.OperationalConfigurationComplete"],
                checkedValue: "Y",
                unCheckedValue: "N"
            }
        }
    },
    {
        op: "change",
        sciId: "seaItemTypeLabel",
        config: {
            xtype: "label",
            cls: "sc-plat-label sc-mandatory"
        }
    }];
}
/*
@author Sourabh Goyal, Bridge Solutions Group
*/
Ext.namespace('sc.item.itemclassificationextension');

sc.item.itemclassificationextension.ManageClassificationsScreenExtension = function() {
    sc.item.itemclassificationextension.ManageClassificationsScreenExtension.superclass.constructor.call(this);
}
Ext.extend(sc.item.itemclassificationextension.ManageClassificationsScreenExtension, sc.plat.ui.Extension, {
    className: 'sc.item.itemclassificationextension.ManageClassificationsScreenExtension',
    getConfigOverlays: sc.item.itemclassificationextension.ManageClassificationsScreenExtensionConfigOverlays,
    namespaces: {
        target: ["getManageItemTera"],
        source: ["getCommonCodeListForHarmonizedCodeNS","getCommonCodeListForECCNCodeNS","getCommonCodeListForItemTypeCodeNS","getCommonCodeListForImportAgenciesCodeNS",	
				 "getVariousCommonCodeDescriptionHarmCodeOutputNS"]
    },
    namespacesDesc: {
        targetDesc: ["ManageItem API called for SAVE_TERA for values under this NS:::: Harmonized,ECCN,ItemType"],
        sourceDesc: ["Teradyne Harmonized Code List","Teradyne ECCN Code List","Teradyne Item Type Code List","Teradyne Import Agencies Code List","Teradyne Harmonized Code Desc"]
    },
	
					saveActionTera : function() {
						var manageItemModel = this.getTargetModel("getManageItemTera")[0];
						var paramsObj = {};
						paramsObj["manageiteminput"] = Ext.encode(manageItemModel);							
						itemUtils.callItemStrutsAction("manageitemclassifications_save", {
								    	params : paramsObj,
								    	success :this.resHandlerForManageItem,
								    	successMsg : sc.sbc.ui.messages.getSaveSuccessfullText("Item"),
								    	scope :this
							});
					}
});
sc.plat.ScreenExtnMgr.add('sc.sbc.itemadmin.item.manage.specifications.ManageClassificationsScreen', sc.item.itemclassificationextension.ManageClassificationsScreenExtension);
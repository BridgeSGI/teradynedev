<%
/**
 * @author Sourabh Goyal, Bridge Solutions Group
 */
%>

<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="com.sterlingcommerce.sbc.core.utils.SBCJSONUtils"%> 
<%@page import="com.sterlingcommerce.framework.utils.SCXmlUtils"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="java.io.*"%>
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.dom.*"%>
<%@page import="javax.xml.transform.stream.*"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIUserPreferences"%>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@page import="com.yantra.yfs.ui.backend.*" %>

<%

SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);

Object ret = uiContext.getAttribute("getitemdetails");
Element e = (Element)ret;
String json = SBCJSONUtils.getJSONFromXML(request, response, e);

Object ret1 = uiContext.getAttribute("getclassificationpurposelist");
Element e1=(Element)ret1;
String json1 = SBCJSONUtils.getJSONFromXML(request, response, e1);

Object ret2 = uiContext.getAttribute("getcommoncodelistforposting");
Element e2=(Element)ret2;
String json2 = SBCJSONUtils.getJSONFromXML(request, response, e2);

Object ret3 = uiContext.getAttribute("getcommoncodelistforvelocitycode");
Element e3=(Element)ret3;
String json3 = SBCJSONUtils.getJSONFromXML(request, response, e3);

Object ret4 = uiContext.getAttribute("getcommoncodelistforharmonizedcodeoutput");
Element e4=(Element)ret4;
//Code for Concatenation of Common Code Vale and Desc for Harmonized Code 
Document getHarmonListDoc = e4.getOwnerDocument();
	NodeList harmonCCNodeList=getHarmonListDoc.getElementsByTagName("CommonCode");
	for(int i=0;i<harmonCCNodeList.getLength();i++){
		Element coomonCodeElement=(Element)harmonCCNodeList.item(i);
		String commonCodeValue=coomonCodeElement.getAttribute("CodeValue");
		String commonCodeShortDescription=coomonCodeElement.getAttribute("CodeShortDescription");
		String commonCodeValueAndShortDesc="";
		if("".equals(commonCodeShortDescription)){
		commonCodeValueAndShortDesc=commonCodeValue;
		} else{
				commonCodeValueAndShortDesc=commonCodeValue.concat("-").concat(commonCodeShortDescription);
		}
		coomonCodeElement.setAttribute("CommonCodeValuePlusDescValueTemp",commonCodeValueAndShortDesc);
}
String json4 = SBCJSONUtils.getJSONFromXML(request, response, e4);

Object ret4Desc = uiContext.getAttribute("getVariousCommonCodeDescriptionServiceOutputHarmCode");
Element e4Desc=(Element)ret4Desc;
String json4Desc = SBCJSONUtils.getJSONFromXML(request, response, e4Desc);

Object ret5 = uiContext.getAttribute("getcommoncodelistforeccncodeoutput");
Element e5=(Element)ret5;
String json5 = SBCJSONUtils.getJSONFromXML(request, response, e5);

Object ret6 = uiContext.getAttribute("getcommoncodelistforitemtypecodeoutput");
Element e6=(Element)ret6;
String json6 = SBCJSONUtils.getJSONFromXML(request, response, e6);

Object ret7 = uiContext.getAttribute("getcommoncodelistforimportagenciescodeoutput");
Element e7=(Element)ret7;
String json7 = SBCJSONUtils.getJSONFromXML(request, response, e7);

String itemdetailsNS = uiContext.getRequest().getParameter("itemdetailsNS");

%>

function loadItemClassifications(){

var getItemDetailsOutput = '<%=json%>';
var getClassificationPurposeListOutput = '<%=json1%>';
var getCommonCodeListForPostingOutput = '<%=json2%>';
var getCommonCodeListForVelocityCodeOutput = '<%=json3%>';

var getCommonCodeListForHarmonizedCodeOutput = '<%=json4%>';
var getVariousCommonCodeDescriptionOutputHarmCodeVar = '<%=json4Desc%>';

var getCommonCodeListForECCNCodeOutput = '<%=json5%>';
var getCommonCodeListForItemTypeCodeOutput = '<%=json6%>';
var getCommonCodeListForImportAgenciesCodeOutput = '<%=json7%>';

var itemdetailsNS = '<%=itemdetailsNS%>';


		getClassificationPurposeListOutput = Ext.decode(getClassificationPurposeListOutput);
        var classificationPurposeList = getClassificationPurposeListOutput.ClassificationPurposeList.ClassificationPurpose;
       
		if(classificationPurposeList){
			var len = classificationPurposeList.length;
	        if(len>0){
				var attributeMap = {};
	        }
	 
			for (var i = 0; i < len; i++) {
	         var classificationPurposeObj = classificationPurposeList[i];
	         var attrName = classificationPurposeObj.AttributeName;
	          attributeMap[attrName] = classificationPurposeObj;
			}
		}
		getItemDetailsOutput =Ext.decode(getItemDetailsOutput);
		getCommonCodeListForPostingOutput = Ext.decode(getCommonCodeListForPostingOutput);
		getCommonCodeListForVelocityCodeOutput = Ext.decode(getCommonCodeListForVelocityCodeOutput);
		
		getCommonCodeListForHarmonizedCodeOutput = Ext.decode(getCommonCodeListForHarmonizedCodeOutput);
		getVariousCommonCodeDescriptionOutputHarmCodeVar = Ext.decode(getVariousCommonCodeDescriptionOutputHarmCodeVar);

		getCommonCodeListForECCNCodeOutput = Ext.decode(getCommonCodeListForECCNCodeOutput);
		getCommonCodeListForItemTypeCodeOutput = Ext.decode(getCommonCodeListForItemTypeCodeOutput);
		getCommonCodeListForImportAgenciesCodeOutput = Ext.decode(getCommonCodeListForImportAgenciesCodeOutput);

		var itemClassifications = new  sc.sbc.itemadmin.item.manage.specifications.ManageClassificationsScreen({attrMap:attributeMap});
		
		var noBackLink = itemManager.isBackButtonNotRequired();
		if(noBackLink){
			containerUtil.drawScreen(itemClassifications, {title : itemClassifications.b_ItemClassifications, 
															buttons : {btype:sc.sbc.helper.ScreenHelper.SAVE,
																	handler:itemClassifications.extension.saveActionTera}});
		}else{
        	containerUtil.drawScreen(itemClassifications, {title : itemClassifications.b_ItemClassifications, 
															back:true, 
															cacheRequired:false,
															buttons : {btype:sc.sbc.helper.ScreenHelper.SAVE,
																	handler:itemClassifications.extension.saveActionTera}});
		}
		itemClassifications.postInit = function() {		
		itemClassifications.populateData(getItemDetailsOutput,
										 getClassificationPurposeListOutput,
										 getCommonCodeListForPostingOutput,
										 getCommonCodeListForVelocityCodeOutput,
										 itemdetailsNS);
		itemClassifications.extension.setModel(getCommonCodeListForHarmonizedCodeOutput,"getCommonCodeListForHarmonizedCodeNS");
		itemClassifications.extension.setModel(getVariousCommonCodeDescriptionOutputHarmCodeVar,"getVariousCommonCodeDescriptionHarmCodeOutputNS");

		itemClassifications.extension.setModel(getCommonCodeListForECCNCodeOutput,"getCommonCodeListForECCNCodeNS");
		itemClassifications.extension.setModel(getCommonCodeListForItemTypeCodeOutput,"getCommonCodeListForItemTypeCodeNS");
		itemClassifications.extension.setModel(getCommonCodeListForImportAgenciesCodeOutput,"getCommonCodeListForImportAgenciesCodeNS");
	}
	itemClassifications.postInit();
};

sc.plat.JSLibManager.loadLibrary("seaitemclassifications", loadItemClassifications);

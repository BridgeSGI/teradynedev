/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreenExtn');

sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneBasicPriceListSearchExtensionConfigOverlays = function() {
    return [{
        op: "change",
        sciId: "terazoneseabasicpricelistsearch",
        config: {
            xtype: "screen"
        }
    },
    {
        op: "change",
        sciId: "seaTxtListName",
        config: {
            xtype: "textfield",
            hidden: true,
            bindingData: {
                defid: "object",
                targetBinding: ["basicPriceListSearch:PricelistLine.PricelistHeader.PricelistName"]
            }
        }
    },
    {
        op: "change",
        sciId: "seaLblListName",
        config: {
            xtype: "label",
            hidden: true,
            bindingData: {
                defid: "object",
                targetBinding: ["basicPriceListSearch:PricelistLine.PricelistHeader.PricelistName"]
            },
            text: ""
        }
    },
    {
        op: "change",
        sciId: "seaTriggerSearchItem",
        config: {
            xtype: "trigger",
            bindingData: {
                defid: "object",
                targetBinding: ["basicPriceListSearch:PricelistLine.Item.ItemID"],
                sourceBinding: "SavedSearch:SearchWrapper.PricelistHeader.PricelistLineList.PricelistLine[0].ItemID"
            }
        }
    },
    {
        op: "change",
        sciId: "radio4",
        config: {
            xtype: "radio",
            bindingData: {
                defid: "object",
                targetBinding: ["basicPriceListSearch:PricelistLine.PricelistHeader.PricingStatus"]
            }
        }
    },
    {
        op: "change",
        sciId: "radio5",
        config: {
            xtype: "radio",
            bindingData: {
                defid: "object",
                targetBinding: ["basicPriceListSearch:PricelistLine.PricelistHeader.PricingStatus"],
                selectedValue: "ACTIVE",
                sourceBinding: "SavedSearch:SearchWrapper.PricelistHeader.PricingStatus"
            }
        }
    },
    {
        op: "change",
        sciId: "radio3",
        config: {
            xtype: "radio",
            bindingData: {
                defid: "object",
                selectedValue: "INACTIVE",
                targetBinding: ["basicPriceListSearch:PricelistLine.PricelistHeader.PricingStatus"],
                sourceBinding: "SavedSearch:SearchWrapper.PricelistHeader.PricingStatus"
            }
        }
    },
    {
        op: "change",
        sciId: "seaCheckboxDate",
        config: {
            xtype: "checkbox",
            bindingData: {
                defid: "object",
                targetBinding: ["basicPriceListSearch:PricelistLine.PricelistHeader.CheckboxDateSelected"],
                checkedValue: "true",
                unCheckedValue: "false",
                sourceBinding: "SavedSearch:SearchWrapper.PricelistHeader.CheckboxDateSelected"
            }
        }
    }];
}
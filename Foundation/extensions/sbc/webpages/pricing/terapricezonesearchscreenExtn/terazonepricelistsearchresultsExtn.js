/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreenExtn');

sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneSearchResultsExtension = function() {
    sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneSearchResultsExtension.superclass.constructor.call(this);
}
Ext.extend(sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneSearchResultsExtension, sc.plat.ui.Extension, {
    className: 'sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneSearchResultsExtension',
    getConfigOverlays: sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneSearchResultsExtensionConfigOverlays,
    namespaces: {
        target: [],
        source: []
    },
    namespacesDesc: {
        targetDesc: [],
        sourceDesc: []
    }/*,
	getRepairORListPriceWithCurrency:function(val,metadata,record){
		return sc.sbc.util.RendererUtils.currencyRendererFromValue(val);
	},
	getExpeditePriceWithCurrency:function(val,metadata,record){
		return sc.sbc.util.RendererUtils.currencyRendererFromValue(val);
	},
	getHandlingChargeWithCurrency:function(val,metadata,record){
		return sc.sbc.util.RendererUtils.currencyRendererFromValue(val);
	}*/
});
sc.plat.ScreenExtnMgr.add('sc.pricing.terapricezonesearchscreen.ClassTeraZoneSearchResults', sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneSearchResultsExtension);
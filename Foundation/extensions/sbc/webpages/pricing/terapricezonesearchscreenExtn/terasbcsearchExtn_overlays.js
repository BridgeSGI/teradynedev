/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreenExtn');

sc.pricing.terapricezonesearchscreenExtn.terasbcsearchExtensionConfigOverlays = function() {
    return [{
        op: "change",
        sciId: "teraseasearchSCIID",
        config: {
            xtype: "screen"
        }
    },
    {
        op: "change",
        sciId: "seapnlholder",
        config: {
            xtype: "panel",
            title: "Teradyne: Price List Search By Zone"
        }
    }];
}
/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreenExtn');

sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneBasicPriceListSearchExtension = function() {
    sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneBasicPriceListSearchExtension.superclass.constructor.call(this);
}
Ext.extend(sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneBasicPriceListSearchExtension, sc.plat.ui.Extension, {
    className: 'sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneBasicPriceListSearchExtension',
    getConfigOverlays: sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneBasicPriceListSearchExtensionConfigOverlays,
    namespaces: {
        target: [],
        source: []
    },
    namespacesDesc: {
        targetDesc: [],
        sourceDesc: []
    }
});
sc.plat.ScreenExtnMgr.add('sc.pricing.terapricezonesearchscreen.ClassTeraZoneBasicPriceListSearch', sc.pricing.terapricezonesearchscreenExtn.ClassTeraZoneBasicPriceListSearchExtension);
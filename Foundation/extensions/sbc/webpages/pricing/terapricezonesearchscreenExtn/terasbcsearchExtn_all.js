/*
 * 
 */

/*
 * 
 */


Ext.namespace('sc.pricing.terapricezonesearchscreenExtn');sc.pricing.terapricezonesearchscreenExtn.terasbcsearchExtensionConfigOverlays=function(){return[{op:"change",sciId:"teraseasearchSCIID",config:{xtype:"screen"}},{op:"change",sciId:"seapnlholder",config:{xtype:"panel",title:"Teradyne: Price List Search By Zone"}}];}

Ext.namespace('sc.pricing.terapricezonesearchscreenExtn');sc.pricing.terapricezonesearchscreenExtn.terasbcsearchExtension=function(){sc.pricing.terapricezonesearchscreenExtn.terasbcsearchExtension.superclass.constructor.call(this);}
Ext.extend(sc.pricing.terapricezonesearchscreenExtn.terasbcsearchExtension,sc.plat.ui.Extension,{className:'sc.pricing.terapricezonesearchscreenExtn.terasbcsearchExtension',getConfigOverlays:sc.pricing.terapricezonesearchscreenExtn.terasbcsearchExtensionConfigOverlays,namespaces:{target:[],source:[]},namespacesDesc:{targetDesc:[],sourceDesc:[]}});sc.plat.ScreenExtnMgr.add('sc.pricing.terapricezonesearchscreen.terasbcsearch',sc.pricing.terapricezonesearchscreenExtn.terasbcsearchExtension);

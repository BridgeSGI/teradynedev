Ext.namespace('sc.pricing.terapricelistsummaryresultsextn');

sc.pricing.terapricelistsummaryresultsextn.pricelistsummaryresultsExtensionConfigOverlays = function() {
    return [{
        op: "insert",
        ownerId: "seaEditorGridItems",
        property: "columns",
        refId: "seaGridClmQtyTiers",
        pos: "before",
        config: {
            defid: "grid-column",
            sciId: "extn_ExpeditingPrice",
            header: "Expedite Price",
            sortable: true,
            width: 70,
            hideable: false,
            css: "text-align: right;",
            renderer: this.getExpeditePriceWithCurrency,
            bindingData: {
                defid: "object",
                tAttrBinding: "Extn.ExpiditingPrice",
                sFieldConfig: {
                    defid: "object",
                    mapping: "Extn.ExpiditingPrice"
                }
            }
        }
    },
    {
        op: "insert",
        ownerId: "seaEditorGridItems",
        property: "columns",
        refId: "seaGridClmQtyTiers",
        pos: "before",
        config: {
            defid: "grid-column",
            sciId: "extn_HandlingCharge",
            header: "Handling Charge",
            sortable: true,
            width: 70,
            hideable: false,
            css: "text-align: right;",
            renderer: this.getHandlingChargeWithCurrency,
            bindingData: {
                defid: "object",
                tAttrBinding: "Extn.HandlingCharge",
                sFieldConfig: {
                    defid: "object",
                    mapping: "Extn.HandlingCharge"
                }
            }
        }
    },
    {
        op: "change",
        sciId: "screen",
        config: {
            xtype: "screen"
        }
    },
    {
        op: "change",
        sciId: "seaGridClmQtyTiers",
        config: {
            defid: "grid-column",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaGridClmCalculatedPrice",
        config: {
            defid: "grid-column",
            bindingData: {
                defid: "object",
                sFieldConfig: {
                    defid: "object",
                    mapping: "CalculatedPrice"
                }
            }
        }
    },
    {
        op: "change",
        sciId: "seaGridClmEffectiveDate",
        config: {
            defid: "grid-column",
            hidden: true
        }
    },
    {
        op: "change",
        sciId: "seaGridClmListPrice",
        config: {
            defid: "grid-column",
            header: "Repair Price"
        }
    }];
}
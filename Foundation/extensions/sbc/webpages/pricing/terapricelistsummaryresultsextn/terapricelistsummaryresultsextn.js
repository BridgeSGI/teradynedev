/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricelistsummaryresultsextn');

sc.pricing.terapricelistsummaryresultsextn.pricelistsummaryresultsExtension = function() {
    sc.pricing.terapricelistsummaryresultsextn.pricelistsummaryresultsExtension.superclass.constructor.call(this);
}
Ext.extend(sc.pricing.terapricelistsummaryresultsextn.pricelistsummaryresultsExtension, sc.plat.ui.Extension, {
    className: 'sc.pricing.terapricelistsummaryresultsextn.pricelistsummaryresultsExtension',
    getConfigOverlays: sc.pricing.terapricelistsummaryresultsextn.pricelistsummaryresultsExtensionConfigOverlays,
    namespaces: {
        target: [],
        source: []
    },
    namespacesDesc: {
        targetDesc: [],
        sourceDesc: []
    },
	getExpeditePriceWithCurrency:function(val,metadata,record){
		return sc.sbc.util.RendererUtils.currencyRendererFromValue(val);
	},
	getHandlingChargeWithCurrency:function(val,metadata,record){
		return sc.sbc.util.RendererUtils.currencyRendererFromValue(val);
	}
});
sc.plat.ScreenExtnMgr.add('sc.sbc.pricing.pricelist.pricelistsummaryresults', sc.pricing.terapricelistsummaryresultsextn.pricelistsummaryresultsExtension);
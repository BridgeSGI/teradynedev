/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreen.search');

	sc.pricing.terapricezonesearchscreen.search.advancedpricelistsearch = function(config) {
	sc.pricing.terapricezonesearchscreen.search.advancedpricelistsearch.superclass.constructor.call(this, config);
	this.screenContext = this.scOwnerScr.getScreenContext();
}
Ext.extend(sc.pricing.terapricezonesearchscreen.search.advancedpricelistsearch, sc.sbc.common.AdvancedSearch, {
	initialize : function() {
		this.initAdvancedSearch();
	},
	getScreenTargetModel : function(config) {
		var advancedSearchInput = this.getAdvancedSearchTargetModel(config);
		if (advancedSearchInput) {
			return advancedSearchInput;
		} else {
			return;
		}
	}, 
	paintSavedSearch : function(searchData) {
		var AdvancedSearchData = searchData.SearchWrapper.AdvancedSearch;
		this.paintScreenForSavedSearch(AdvancedSearchData);
	}
});
Ext.reg('seaadvancedpricelistsearch', sc.pricing.terapricezonesearchscreen.search.advancedpricelistsearch);

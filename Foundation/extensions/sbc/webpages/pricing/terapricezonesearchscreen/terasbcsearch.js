/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreen');

sc.pricing.terapricezonesearchscreen.terasbcsearch = function(config) {
    sc.pricing.terapricezonesearchscreen.terasbcsearch.superclass.constructor.call(this, config);
}
Ext.extend(sc.pricing.terapricezonesearchscreen.terasbcsearch, sc.plat.ui.ExtensibleScreen, {
    className: 'sc.pricing.terapricezonesearchscreen.terasbcsearch',
    getUIConfig: sc.pricing.terapricezonesearchscreen.terasbcsearchUIConfig,
	namespaces : {
		source : ["sortingOrder"]
	},
    menuButton : null,
    seapnlholder : null,
	initialize : function() {
		this.getSortingOrder();
		//Call Inner Panel containing just the search criteria components.
		var activeScreen = this.getSearchCriteriaPnlHolder().getLayout().activeItem.items.itemAt(0);
		if(activeScreen.initialize instanceof Function) {
			activeScreen.initialize(arguments);
			activeScreen.initialized = true;
		}
		if(this.getResultsScreen().getResultGrid instanceof Function) {
			var resultGrid = this.getResultsScreen().getResultGrid();
			if(resultGrid instanceof Ext.grid.GridPanel) {
				resultGrid.getStore().on("load", this.onGridLoad);
			}
		}
	},
	onGridLoad : function(store, records, options) {
		if (records.length === 0) {
			sc.plat.RequestUtils.handleErrors({
						em : sc.plat.bundle["b_NoResultMessage"],
						type : 2
					});
		} else {
			var errorPanel = sc.sbc.util.CoreUtils.getErrorPanel();
			if(errorPanel)
				errorPanel.close();
		}
	},
	getTopBar : function() {
		var switchBtn = this.getSwitchButton();
		var savedSearchBtn = this.getSavedSearchButton();
		var topToolbar = "";
		if (savedSearchBtn || switchBtn) {
			topToolbar = new Ext.Toolbar({
						items : [{
                xtype: "tbfill",
                sciId: "tbfill"
            }, switchBtn, (savedSearchBtn ? " " : ""),
								(savedSearchBtn ? " " : ""), savedSearchBtn]
					});
		}
		return topToolbar;
	},
	getActiveItem : function() {
		if (this.basicSearch.getXType.apply(this))
			return 0;
		else if (this.advancedSearch.getXType.apply(this))
			return 1;
		else 
			this.showError();
	},
    getSwitchButton : function() {
				if (this.advancedSearch.getXType() && this.basicSearch.getXType.apply(this)) {
					return new Ext.Toolbar.Button({
								sciId : "seabtntogglesearch",
								text : sc.plat.bundle["b_CommonAdvancedSearch"],
								handler : this.toggleSearch,
								scope : this
							});
				}
				else
					return "";
	},
	getSavedSearchButton : function() {
		if (this.isSavedSearchRequired() === false) {
			return "";
		} else {
			return new Ext.Toolbar.Button({
						sciId : "btnSavedSearch",
						text : this.b_SavedSearches,
						handler : this.handleSavedSearchMenu,
						scope : this,
						iconCls: "sbc-savedsearch-menu",
						iconAlign: "right"  
					});
		}
	},
	getBasicSearchXType : function() {
		if(this.basicSearch.getXType.apply(this))
			return this.basicSearch.getXType.apply(this);
		else if(this.advancedSearch.getXType.apply(this))
			return "hidden";
		else 
			this.showError();
	},
	getAdvancedSearchXType : function() {
		if(this.advancedSearch.getXType.apply(this))
			return this.advancedSearch.getXType.apply(this);
		else if(this.basicSearch.getXType.apply(this))
			return "hidden";
		else 
			this.showError();
	},
    getHolderTitle : function() {
    	if(this.basicSearch.getXType.apply(this))
    		return this.basicSearch.getTitle.apply(this);
    	else if(this.advancedSearch.getXType.apply(this))
    		return this.advancedSearch.getTitle.apply(this);
    	else
    		return "";
    },
	getScreenContext : Ext.emptyFn,
    getResultsScreenXtype : Ext.emptyFn,
    isSavedSearchRequired : Ext.emptyFn,
    basicSearch : {
		getTitle : Ext.emptyFn,
		getViewID : Ext.emptyFn,
		getXType : Ext.emptyFn
	},
	advancedSearch : {
		getTitle : Ext.emptyFn,
		getViewID : Ext.emptyFn,
		getXType : Ext.emptyFn
	},

	getSortingOrder : function() {
		var input = {Parameters : {}};
    	input.Parameters.ScreenContext = this.getScreenContext();
		sc.sbc.AjaxUtils.request({
					action : "getSortingOrder",
					actionNS : "/sbc/pricing",
					success : this.handleGetSortOrder,
					scope : this,
					inputNS : "getSortingOrder",
					inputObj : input,
					hideMask : true
				});
	},
    handleSearch : function() {
		var searchInput = this.getScreenTargetModel();
		if(searchInput) {
			var pnlSearchResults = this.find("sciId", "seapnlsearchresults")[0];
			pnlSearchResults.setVisible(true);
			pnlSearchResults.doLayout(); 
			var resultsScreen = this.find("sciId", "seacstmresults")[0];
			resultsScreen.on('onsearch', this.collapseHeader, this);
			resultsScreen.doLayout(); 
			this.clearSortingInfo(resultsScreen);
			resultsScreen.performSearch(searchInput);
		}
    },
    
    clearSortingInfo:function(resultsScreen){
    	if(sc.sbc.util.CoreUtils.screenHasMethod(resultsScreen, "getResultGrid")){
	    	var resultsGrid=resultsScreen.getResultGrid();
	    	if(resultsGrid){
	    		var gridStore=resultsGrid.getStore();
	    		if(gridStore){
	    			if(gridStore.sortInfo){
	    				delete gridStore.sortInfo;
	    			}
	    		}
	    	}
    	}
    },
    collapseHeader : function(canCollapse) {
    	if(canCollapse) {
    		var pnlHolder = this.getPnlHolder();
			pnlHolder.collapse(true);
    	}
    },
    handleSavedSearchMenu : function() {
		if (!this.menuButton)
			this.menuButton = sc.sbc.util.CoreUtils.getTopToolBarItemBySciId(this.getPnlHolder(), "btnSavedSearch");

		var options = {
					menuButton : this.menuButton,
					owner : this,
					savedSearchDetailsHandler : this.handleGetSavedSearchDetails,
					savedSearchDataProvider : this.getDataToSaveSearch
				};
		seaSavedSearchUtils.paintSavedSearchMenu(this.getScreenContext(), options);
	},
	handleGetSavedSearchDetails : function(savedSearchObject) {
		seaSavedSearchUtils.setSelectedSavedSearchKey(savedSearchObject.SavedSearch.SavedSearchKey,this.getScreenContext());
		var searchData = savedSearchObject.SavedSearch.SearchData;
		//Call Inner Panel containing just the search criteria components.
		var isBasicSearch = this.getSearchCriteriaPnlHolder().getLayout().activeItem.pnlBasicSearch;
		var dataForSearch = null;
		if (searchData.SearchWrapper.SearchInfo.SearchViewID == this.basicSearch.getViewID.apply(this)) {
			dataForSearch = sc.sbc.common.SearchUtils.getDataForSearch(searchData, true);
			if (!isBasicSearch)
				this.toggleSearch(false);
		} else {
			dataForSearch = sc.sbc.common.SearchUtils.getDataForSearch(searchData, false);
			if (isBasicSearch)
				this.toggleSearch(false);
		}
		//Call Inner Panel containing just the search criteria components.
		var activeScreen = this.getSearchCriteriaPnlHolder().getLayout().activeItem.items.itemAt(0);
		if(activeScreen.paintSavedSearch instanceof Function)
			activeScreen.paintSavedSearch(searchData);
		if (searchData.SearchWrapper.SearchInfo.AutoExec === "true") {
			var pnlSearchResults = this.find("sciId", "seapnlsearchresults")[0];
			pnlSearchResults.setVisible(true);
			this.doLayout();
			var resultsScreen = this.find("sciId", "seacstmresults")[0];
			resultsScreen.on('onsearch', this.collapseHeader, this);
			sc.sbc.util.JsonUtils.clearBlankAttributes(dataForSearch);
			this.getFormattedOrderBy(dataForSearch);
			
			//this.handleSearch();
			resultsScreen.performSearch(dataForSearch);
		}
		this.paintOrderByAttribute(dataForSearch);
	},
	handleSavedSearchChange : function() {
		seaSavedSearchUtils.setSavedSearchChanged();
	},
	getFormattedOrderBy : function(dataForSearch) {
		var rootNode = sc.sbc.util.JsonUtils.getRootNode(dataForSearch, "SearchInfo");
		if (dataForSearch[rootNode].hasOwnProperty("OrderBy") && dataForSearch[rootNode]["OrderBy"].hasOwnProperty("Attribute")) {
			var attribute = dataForSearch[rootNode]["OrderBy"]["Attribute"];
			//delete attribute["Display"];
			if (!(attribute instanceof Array))
				dataForSearch[rootNode]["OrderBy"]["Attribute"] = [attribute];
			return dataForSearch[rootNode]["OrderBy"]["Attribute"][0];
		} else {
			return false;
		}
	},
	paintOrderByAttribute : function(dataForSearch) {
		var orderByAttribute = "";
		var rootNode = sc.sbc.util.JsonUtils.getRootNode(dataForSearch, "SearchInfo");
		var formattedOrderBy = this.getFormattedOrderBy(dataForSearch);
		if (formattedOrderBy !== false) {
			orderByAttribute = formattedOrderBy.Display;
		}
		var sortByCombo = sc.sbc.util.CoreUtils.getBottomToolBarItemBySciId(this.getPnlHolder(), "cmbAttrib");
		sortByCombo.setValue(orderByAttribute);
	},
	getDataToSaveSearch : function() {
		var searchInput = this.getScreenTargetModel({forSavedSearch : true});
		if(!searchInput)
			return;
		//Call Inner Panel containing just the search criteria components.
		var seapnlholder = this.getSearchCriteriaPnlHolder();
		var createSearchInput = {SearchWrapper : {}};
		Ext.apply(createSearchInput.SearchWrapper, searchInput);
		createSearchInput.SearchWrapper.SearchInfo = {
			SearchViewID : (seapnlholder.getLayout().activeItem.pnlBasicSearch ? this.basicSearch.getViewID.apply(this) : this.advancedSearch.getViewID.apply(this)),
			AutoExec : "true"
		};
		var dataToSaveSearch = {};
		dataToSaveSearch.createSearchInput = createSearchInput;
		return dataToSaveSearch;
	},
	getScreenTargetModel : function(config) {
		//changed
		var activeScreen = this.getSearchCriteriaPnlHolder().getLayout().activeItem.items.itemAt(0);
		if(activeScreen.getScreenTargetModel instanceof Function) {
			var targetModel = activeScreen.getScreenTargetModel(config);
			/* Commented as part of Teradyne Dev			
			if(targetModel) {
				var sortByCombo = sc.sbc.util.CoreUtils.getBottomToolBarItemBySciId(this.getPnlHolder(), "cmbAttrib");
				var sortData = {};
				if(sortByCombo.selectedIndex != -1)
					sortData = sortByCombo.store.data.itemAt(sortByCombo.selectedIndex).json;
//				delete sortData["Display"];
				config = config || {};
				//Call Inner Panel containing just the search criteria components.
				config.isBasicSearch = this.getSearchCriteriaPnlHolder().getLayout().activeItem.pnlBasicSearch;
				//sc.sbc.common.SearchUtils.appendOrderBy(targetModel, sortData, config);
				if(!(config && config.forSavedSearch))
					sc.sbc.util.JsonUtils.clearBlankAttributes(targetModel);
			}*/
			return targetModel;
		} else
			return;
	},
    toggleSearch : function(toBeInitialized) {
    	toBeInitialized = (toBeInitialized === false ? false : true);
		var seapnlholder = this.getPnlHolder();
		//Call Inner Panel containing just the search criteria components.
		var searchCriteriaPnlHolder = this.getSearchCriteriaPnlHolder();
		var isBasicSearch = searchCriteriaPnlHolder.getLayout().activeItem.pnlBasicSearch;
    	if (isBasicSearch) {
    		//Call Inner Panel containing just the search criteria components.
    		searchCriteriaPnlHolder.getLayout().setActiveItem(1);
			sc.sbc.util.CoreUtils.getTopToolBarItemBySciId(seapnlholder, "seabtntogglesearch").setText(sc.plat.bundle["b_CommonBasicSearch"]);
			seapnlholder.setTitle(this.advancedSearch.getTitle.apply(this));
		} else {
			//Call Inner Panel containing just the search criteria components.
			searchCriteriaPnlHolder.getLayout().setActiveItem(0);
			sc.sbc.util.CoreUtils.getTopToolBarItemBySciId(seapnlholder, "seabtntogglesearch").setText(sc.plat.bundle["b_CommonAdvancedSearch"]);
			seapnlholder.setTitle(this.basicSearch.getTitle.apply(this));
		}
		if(toBeInitialized) {
			//Call Inner Panel containing just the search criteria components.
			var activeScreen = this.getSearchCriteriaPnlHolder().getLayout().activeItem.items.itemAt(0);
				if(activeScreen.initialize instanceof Function) {
					if(!this.getSearchCriteriaPnlHolder().getLayout().activeItem.items.itemAt(0).initialized)
					{
						this.getSearchCriteriaPnlHolder().getLayout().activeItem.items.itemAt(0).initialize();
						this.getSearchCriteriaPnlHolder().getLayout().activeItem.items.itemAt(0).initialized = true;
					}
				}
		}
    },
    hasError : function(ns) {
    	//Call Inner Panel containing just the search criteria components.
    	return this.getSearchCriteriaPnlHolder().getLayout().activeItem.items.itemAt(0).hasError(ns);
    },
    getPnlHolder : function() {
    	if(!this.seapnlholder)
    		this.seapnlholder = this.find("sciId", "seapnlholder")[0];
    	return this.seapnlholder;
    },
    getSearchCriteriaPnlHolder : function(){
    	//This function returns the panel that is inner panel to the panel returned by <getPnlHolder function>.
    	//Return the Inner Panel containing just the search criteria components.
    	if(!this.searchCriteriaPnlHolder)
    		this.searchCriteriaPnlHolder = this.find("sciId", "searchCriteriaHolderPanel")[0];
    	return this.searchCriteriaPnlHolder;
    },
    getResultsScreen : function() {
    	return this.find("sciId", "seacstmresults")[0];
    },
    showError : function() {
		if (!this.threwError) {
			console.error("Atleast basic search or advanced search should be implemented.");
			this.threwError = true;
		}
	},
	handleGetSortOrder : function(res, options) {
		var sortData = Ext.decode(res.responseText).output;
		if(sortData.OrderByList) {
			var sortByCombo = sc.sbc.util.CoreUtils.getBottomToolBarItemBySciId(this.getPnlHolder(), "cmbAttrib");
			sortByCombo.store.loadData(sortData.OrderByList.OrderBy);
			sortByCombo.setValue(sortData.OrderByList.OrderBy[0].Display);
			sortByCombo.clean();
			sortByCombo.selectedIndex = sc.sbc.util.JsonUtils.getIndexOf(sortByCombo.store, sortByCombo.valueField, sortData.OrderByList.OrderBy[0].Display);
		}
	}
});

sc.sbc.common.SearchUtils = function() {
	var qryTypeString = "QryType";
	function removeBlankAttributes(json) {
		for (var h in json) {
			if (json.hasOwnProperty(h)) {
				if (json[h] instanceof Object)
					removeBlankAttributes(json[h]);
				if (Ext.encode(json[h]) === "{}")
					delete json[h];
				else {
					if (!json[h]) {
						delete json[h];
						if (json.hasOwnProperty(h + qryTypeString))
							delete json[h + qryTypeString];
						else if(json.hasOwnProperty("To" + h + qryTypeString))
							delete json["To" + h + qryTypeString];
						else if(json.hasOwnProperty("From" + h + qryTypeString))
							delete json["From" + h + qryTypeString];
					}
				}
			}
		}
	}
	return {
		getDataForSearch : function(searchData, forBasicSearch) {
			var dataForSearch = {};
			if (forBasicSearch) {
				var rootNode = sc.sbc.util.JsonUtils.getRootNode(searchData.SearchWrapper, "SearchInfo");
				dataForSearch[rootNode] = searchData.SearchWrapper[rootNode];
				return dataForSearch;
			} else {
				var advancedSearchData = searchData.SearchWrapper.AdvancedSearch;
				advancedSearchData = (advancedSearchData instanceof Array) ? advancedSearchData : [advancedSearchData];
				var rootNode = sc.sbc.util.JsonUtils.getRootNode(advancedSearchData[0], "SearchParameter");
				dataForSearch[rootNode] = {};
				for (var i = 0; i < advancedSearchData.length; i++) {
					var condition = advancedSearchData[i];
					var searchParameter = condition.SearchParameter;
					var xpath = searchParameter.XPath.split('.');
					var innerElem = advancedSearchData[i];
					for (var j = 0; j < xpath.length; j++) {
						innerElem = innerElem[xpath[j]];
					}
					innerElem[searchParameter.SearchField + qryTypeString] = searchParameter.QueryType;
					Ext.applyIf(dataForSearch[rootNode], condition[rootNode]);
				}
				return dataForSearch;
			}
		},
		appendOrderBy : function(obj, sortData, config) {
			var orderBy = {OrderBy : {Attribute : [sortData]}};
			if(config && config.forSavedSearch) {
				if(config.isBasicSearch) {
					var rootNode = sc.sbc.util.JsonUtils.getRootNode(obj);
					Ext.applyIf(obj[rootNode], orderBy);
				} else {
					var advancedSearchData = obj.AdvancedSearch;
					var rootNode = sc.sbc.util.JsonUtils.getRootNode(advancedSearchData[0], "SearchParameter");
					Ext.applyIf(advancedSearchData[0][rootNode], orderBy);
				}
			} else {
				var rootNode = sc.sbc.util.JsonUtils.getRootNode(obj);
				Ext.applyIf(obj[rootNode], orderBy);
			}
		}
	}
}();

Ext.reg('teraseacommonsearch', sc.pricing.terapricezonesearchscreen.terasbcsearch);

/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreen.search');

sc.pricing.terapricezonesearchscreen.search.terazonepricelistsearch = function(config) {
	  config = config || {};
      Ext.applyIf(config, {
                        sciId : "terazonepricelistsearch"
                  });
	sc.pricing.terapricezonesearchscreen.search.terazonepricelistsearch.superclass.constructor.call(this, config);
}
Ext.extend(sc.pricing.terapricezonesearchscreen.search.terazonepricelistsearch, sc.pricing.terapricezonesearchscreen.terasbcsearch, {
	
		getScreenContext : function() {
			return "Pricelist";
	    },
	    getResultsScreenXtype : function() {
	    	return "terazonepricelistsearchresults";
	    },
	    basicSearch : {
				getButtonText : function() {
					return this.b_BasicPriceListSearchBtnText;
				},
				getTitle : function() {
					return this.b_BasicPriceListSearchBtnText;
				},
				getViewID : function() {
					return "BASIC_PRICELIST_SEARCH";
				},
				getXType : function() {
					return "terazoneseabasicpricelistsearch";
				}
			},
		advancedSearch : {
				getButtonText : function() {
					return this.b_AdvancedPriceListSearchBtnText;
				},
				getTitle : function() {
					return this.b_AdvancedPriceListSearchBtnText;
				},
				getViewID : function() {
					return "ADVANCED_PRICELIST_SEARCH";
				},
				getXType : function() {
					return "seaadvancedpricelistsearch";
				}
			}
});
Ext.reg('terazoneseapricelistsearch', sc.pricing.terapricezonesearchscreen.search.terazonepricelistsearch);

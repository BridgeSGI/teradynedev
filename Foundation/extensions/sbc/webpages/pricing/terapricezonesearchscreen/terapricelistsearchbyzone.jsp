<%
// Licensed Materials - Property of IBM
// IBM Sterling Business Center
// (C) Copyright IBM Corp. 2009, 2011 All Rights Reserved.
// US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
%>
 
<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIUserPreferences"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="com.sterlingcommerce.sbc.core.utils.SBCJSONUtils"%> 
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Document"%>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>

<%@page import="com.yantra.yfc.log.YFCLogCategory"%>
<%
YFCLogCategory log = YFCLogCategory.instance("Test.class");

System.out.println("\n\nReady To out-put All the request-attributes received from request in ZONE Search Screen- ");
log.info("\n\nReady To out-put All the request-attributes received from request in ZONE Search Screen- ");

Enumeration enAttr = request.getAttributeNames(); 
while(enAttr.hasMoreElements()){
String attributeName = (String)enAttr.nextElement();
System.out.println("\nAttribute Name - "+attributeName+", Value - "+(request.getAttribute(attributeName)).toString());
log.info("\nAttribute Name - "+attributeName+", Value - "+(request.getAttribute(attributeName)).toString());
}
%>


<%
SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);
Object ret = uiContext.getAttribute("getCommonCodeListOutput");
String json = SBCJSONUtils.getJSONFromXML(request, response, (Element)ret);
String stringSearchInputData = "";
String searchContext="";
searchContext=uiContext.getRequest().getParameter("searchContext");
stringSearchInputData = (String)uiContext.getRequest().getParameter("inputdata");
if(stringSearchInputData == null){
	stringSearchInputData = "";
}
%>
var getCommonCodeListOutput = '<%=json%>';

function handleGetSavedSearchDetails(res, options){
	var result = Ext.decode(res.responseText).output;
	var terazonepricelistsearch = new sc.pricing.terapricezonesearchscreen.search.terazonepricelistsearch();
	containerUtil.drawScreen(terazonepricelistsearch, {title : "Teradyne Price List Search By Zone", taskXML: "terazoneseapricelistsearch"});
	terazonepricelistsearch.postInit = function() {
		terazonepricelistsearch.initialize();
		terazonepricelistsearch.handleGetSavedSearchDetails(result);
	}
	terazonepricelistsearch.postInit();
};

function loadTeraZonePricelistSearch(){
	var searchKey='<%=stringSearchInputData%>';
		var searchContext='<%=searchContext%>';
	if(searchKey && searchContext)
	{
		sc.sbc.common.savedsearch.savedsearchutils.callGetSavedSearchDetails(searchKey,{callback : handleGetSavedSearchDetails},searchContext);
	} else {
		var terazonepricelistsearch = new sc.pricing.terapricezonesearchscreen.search.terazonepricelistsearch();
		containerUtil.drawScreen(terazonepricelistsearch, {title : "Teradyne Price List Search By Zone", taskXML: "terazoneseapricelistsearch"});
		terazonepricelistsearch.postCreate = function() {
				var commonCodeList = Ext.decode(getCommonCodeListOutput);
				var basicPricelistSearchScreen=terazonepricelistsearch.find("sciId", "seacstmbasicsearch")[0];
				basicPricelistSearchScreen.CommonCodeList = Ext.decode(getCommonCodeListOutput);
				terazonepricelistsearch.initialize();
			}
		terazonepricelistsearch.postCreate();
	}
};
sc.plat.JSLibManager.loadLibrary("teraZonePricelistSearch", loadTeraZonePricelistSearch);
<%
log.info("Calling GET-REQUEST-DOM in ZONE Search Screen: \n"+ getRequestDOM());
System.out.println("Calling GET-REQUEST-DOM in ZONE Search Screen: \n"+ getRequestDOM());  
%>

/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreen');

sc.pricing.terapricezonesearchscreen.ClassTeraZoneSearchResultsUIConfig = function() {
    return {
        xtype: "screen",
        sciId: "screenTeraZone",
        header: false,
        layout: "anchor",
        autoScroll: true,
        items: [{
            xtype: "editorgrid",
            sciId: "seaEditorGridResults",
            title: this.b_PriceLists,
            clicksToEdit: 1,
            autoHeight: true,
            tbar: [{
                xtype: "button",
                sciId: "seaTBttnCopy",
                text: this.b_Copy,
                handler: this.copyBttnHandler,
                scope: this,
                minWidth: 80,
                disabled: true
            }],
            columns: [this.checkBoxSelectionModel, {
                defid: "grid-column",
                sciId: "gridClmListName",
                header: this.b_pricelistnameHeader,
                sortable: true,
                dataIndex: "PricelistName",
                width: 230,
                renderer: this.showLinksWithImageInColumns,
                id: "sbcautoexpandcolumn",
                bindingData: {
                    defid: "object",
                    sFieldConfig: {
                        defid: "object",
                        mapping: "PricelistName"
                    }
                }
            },
            {
                defid: "grid-column",
                sciId: "gridClmStartDate",
                header: this.EffectiveStartDate,
                sortable: true,
                dataIndex: "StartDateActive",
                width: 160,
                renderer: this.returnFormattedStartDate,
                css: "sbc-cellValue-AlignLeft",
                bindingData: {
                    defid: "object",
                    sFieldConfig: {
                        defid: "object",
                        mapping: "StartDateActive"
                    }
                }
            },
            {
                defid: "grid-column",
                sciId: "gridClmEndDate",
                header: this.EffectiveEndDate,
                sortable: true,
                dataIndex: "EndDateActive",
                width: 160,
                renderer: this.returnFormattedEndDate,
                css: "sbc-cellValue-AlignLeft",
                bindingData: {
                    defid: "object",
                    sFieldConfig: {
                        defid: "object",
                        mapping: "EndDateActive"
                    }
                }
            },
            {
                defid: "grid-column",
                sciId: "gridClmCurrency",
                header: this.b_EnterCurrency,
                sortable: true,
                dataIndex: "Currency",
                width: 80,
                renderer: this.getCurrencyDesc
            },
            {
                defid: "grid-column",
                sciId: "gridClmStatus",
                header: this.Status,
                sortable: true,
                dataIndex: "PricingStatus",
                renderer: this.getStatusDesc,
                width: 95,
                bindingData: {
                    defid: "object",
                    sFieldConfig: {
                        defid: "object",
                        mapping: "PricingStatus"
                    }
                }
            },
            {
                defid: "grid-column",
                sciId: "seaGridClmHeaderKey",
                header: "Column 5",
                sortable: true,
                hidden: true,
                hideable: false,
                dataIndex: "PricelistHeaderKey",
                bindingData: {
                    defid: "object",
                    sFieldConfig: {
                        defid: "object",
                        mapping: "PricelistHeaderKey"
                    }
                }
            }],
            selModel: this.checkBoxSelectionModel,
            collapsible: false,
            titleCollapse: true,
            stripeRows: true,
            bbar: this.pagingToolBar,
            sctype: "GridPanel",
            border: true,
            bindingData: {
                defid: "object",
                sourceBinding: "getPriceListOutput:PricelistLineList.PricelistLine",
                pagination: {
                    defid: "object",
                    strategy: "NEXTPAGE",
                    url: this.getUrl(),
                    pageSize: 15,
                    root: "PricelistLineList.PricelistLine"
                }
            },
            listeners: {
                defid: "listeners",
                scope: this,
                cellclick: this.openSummary,
                rowdblclick: this.handleCellDoubleClick
            }
        }],
        border: false,
        autoWidth: true
    };
}

/*
@author Sourabh Goyal, Bridge Solutions Group
*/
 
Ext.namespace('sc.pricing.terapricezonesearchscreen');

sc.pricing.terapricezonesearchscreen.ClassTeraZoneBasicPriceListSearch = function(config) {
	sc.pricing.terapricezonesearchscreen.ClassTeraZoneBasicPriceListSearch.superclass.constructor.call(this, config);
	
}
Ext.extend(sc.pricing.terapricezonesearchscreen.ClassTeraZoneBasicPriceListSearch, sc.plat.ui.ExtensibleScreen, {
    className: 'sc.pricing.terapricezonesearchscreen.ClassTeraZoneBasicPriceListSearch',
    gridScreen : null,
    win:null,
    getUIConfig: sc.pricing.terapricezonesearchscreen.ClassTeraZoneBasicPriceListSearchUIConfig,
	namespaces: {
                source: ["commonCodes","getOrderByData","getPriceListOutput", "SavedSearch","ItemIdFromSearchScreen"],
                target: ["basicPriceListSearch"]
            },
            
    namespacesDesc: {
                sourceDesc: ["This namespace contains the model used to load the status model","This namespace contains the order by model used for ordering the search results.",
                             "Not used","This namespace contains the saved search model","Not used"],
                targetDesc: ["This namespace contains the input for searching the price lists"]
            },

    combostores: function(){	
		return new Ext.data.JsonStore({fields: ['CodeValue', 'CodeShortDescription']});
	},
	
	getPriceListHeaderDetailsValues:function(headerKey){
		var container = Ext.getCmp("mainBodyPanel");
		container.remove(this,true);
		this.destroy();
		//this.hide();
		container.doLayout();
		
		var pricelistSummaryScreen=new sc.pricing.terapricezonesearchscreen.ClassPriceListSummary();
		
		pricelistSummaryScreen.getPriceListHeaderDetailsValues(headerKey);
		pricelistSummaryScreen.loadPricelistLineList(headerKey);
		
		
		container.add(pricelistSummaryScreen);
		
		container.doLayout();
		container.syncSize();
		
	},
		
	
		resHandlerForOutput: function(res2, options) {
			var panelGrid=this.find("sciId","seaPanelSearchResults")[0];
			if(this.gridScreen == null){
			this.gridScreen =  new sc.pricing.terapricezonesearchscreen.ClassTeraZoneSearchResults();
			panelGrid.add(this.gridScreen);
		}
			var res2 = Ext.decode(res2.responseText);
		
		this.gridScreen.setModelForComponents(res2);
	},

/*Saved Search Code Begins*/

	menuButton : null,
	selectedSavedSearch : "",
	saveSearch : function() {
		var basicPriceListSearch = sc.plat.DataManager.getTargetModel("basicPriceListSearch")[0];
		basicPriceListSearch.PricelistLine.PricelistHeader.NameQryType = "LIKE";
		if(basicPriceListSearch.PricelistLine.PricelistHeader.CheckboxDateSelected){
			basicPriceListSearch.PricelistLine.PricelistHeader.StartDateActiveQryType="LT";
			basicPriceListSearch.PricelistLine.PricelistHeader.EndDateActiveQryType="GT";
		}
		basicPriceListSearch.PricelistLine.PricelistHeader.CheckboxDateSelected = (basicPriceListSearch.PricelistLine.PricelistHeader.CheckboxDateSelected ? "true" : "false");

		var createSearchInput = {};
		createSearchInput.SearchWrapper = {};
		createSearchInput.SearchWrapper.PricelistHeader.PricelistLine = basicPriceListSearch.PricelistHeader.PricelistLine;
		createSearchInput.SearchWrapper.SearchInfo = {SearchViewID : "YOMS010", AutoExec : "true"};
		
		if(!this.menuButton)
			this.menuButton = this.find("sciId", "seaPanelMain")[0].getTopToolbar().items.itemAt(3);
		
		var options = {};
		options.selectedSavedSearch = {SelectedSavedSearch : {SearchName : this.selectedSavedSearch}};
		options.createSearchInput = createSearchInput;
		options.menuButton = this.menuButton;
		options.owner = this;
		sc.sbc.savedsearchutils.saveSearch("admin", "Pricelist", options);
	},
	setModelForSavedSearch : function(savedSearchObject) {
		this.selectedSavedSearch = savedSearchObject.SavedSearch.SearchName;
		var searchData = Ext.decode(savedSearchObject.SavedSearch.SearchData);
		sc.plat.DataManager.setModel(searchData, "SavedSearch");
		if(searchData.SearchWrapper.SearchInfo.AutoExec == "true")
			this.searchHandler();
		else return;
	},
	handleSavedSearchMenu : function() {
		if(!this.menuButton)
			this.menuButton = this.find("sciId", "seaPanelMain")[0].getTopToolbar().items.itemAt(3);

		var options = {};
		options.menuButton = this.menuButton;
		options.owner = this;
		sc.sbc.savedsearchutils.paintSavedSearchMenu("admin", "Coupon", options);
	},
	
	getCheckBoxSelectionModelInResultsScreen:function(){
		return this.gridScreen.getCheckBoxSelectionModel();	
	},
	
	initialize : function() {
		var commonCodeList=this.CommonCodeList;
		sc.sbc.core.data.DataManager.setData("CommonCodesForStatus",commonCodeList);
		this.setModel(commonCodeList, "commonCodes");
		this.clean();
	},
    getScreenTargetModel : function(config) {
    	var model=this.getTargetModel("basicPriceListSearch")[0];
    	model.PricelistLine.PricelistHeader.PricelistName="PLC";
    	model.PricelistLine.PricelistHeader.PricelistNameQryType="FLIKE";
		/*Sourabh---Here Needs to do programming to set PriceListName to blank in case it is other than PLC_*_US
		var checkboxDateSelected=this.getModel("basicPriceListSearch");
		console.log("CheckboxDateSelected:"+checkboxDateSelected);
		sc.log("CheckboxDateSelected:"+checkboxDateSelected);
		*/
    	return model;
    },
    paintSavedSearch : function(searchData) {
    	var searchWrapper=searchData.SearchWrapper;
    	if(searchWrapper){
    		var pricelistLine=searchWrapper.PricelistHeader.PricelistLine;
    		if(pricelistLine){
    			var status=pricelistLine.PricingStatus;
    			if(status){
    				this.find("sciId","radio4")[0].setValue(false);
    				
    			}
    		}
    	}
    	this.clean();
		this.setModel(searchData, "SavedSearch");
    },
    handleResults : function(popupdata, popupscreen, parent,win){
    	if(popupdata){
    		var itemList=popupdata.ItemList;
    		if(itemList){
    			var itemElem=itemList.Item[0];	
    			var itemId=itemElem.ItemID;
    		}
    	}
    	
    	this.find("sciId","seaTriggerSearchItem")[0].setValue(itemId);
    	this.win=win;
    	if(this.win){
    		this.win[this.win.closeAction]();
    	}
    },
    
    openItemSearch:function(e){
		sc.sbc.util.ScreenPopupUtils.launchPopup({
					entity : sc.sbc.util.ScreenPopupUtils.Item,
					scope : this.scope,
					popupParams : {singleSelect : true, ItemID : this.getValue(), isForPopup : true,hideSearchServiceOnlyOption : false},
					popupConfig : {
						buttons : sc.sbc.util.WindowUtils.SELECTCLOSE,
						handlers : {"select" : this.scope.handleResults},
						dblclickevent : {"screendblclick" : this.scope.handleResults},
						scope : this.scope
					}
				});
	
    }

/*Saved Search Code Ends*/

});


Ext.reg('terazoneseabasicpricelistsearch', sc.pricing.terapricezonesearchscreen.ClassTeraZoneBasicPriceListSearch);

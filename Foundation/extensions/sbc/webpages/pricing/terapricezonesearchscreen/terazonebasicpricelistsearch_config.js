/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreen');

sc.pricing.terapricezonesearchscreen.ClassTeraZoneBasicPriceListSearchUIConfig = function() {
    return {
        xtype: "screen",
        sciId: "terazoneseabasicpricelistsearch",
        header: false,
        layout: "anchor",
        items: [{
            xtype: "panel",
            sciId: "seaPanelCriteria",
            layout: "table",
            header: false,
            border: false,
            items: [{
                xtype: "label",
                sciId: "seaLblListName",
                text: this.b_pricelistnamecontains,
                cls: "sc-plat-label",
                id: "lblListName"
            },
            {
                xtype: "textfield",
                sciId: "seaTxtListName",
                cls: "sc-plat-editable-text",
                id: "textListName",
                bindingData: {
                    defid: "object",
                    targetBinding: ["basicPriceListSearch:PricelistHeader.PricelistName"],
                    sourceBinding: "SavedSearch:SearchWrapper.PricelistHeader.PricelistName"
                }
            },
            {
                xtype: "panel",
                sciId: "seasearchpaddingpanel",
                layout: "table",
                header: false,
                border: false,
                width: 45,
                layoutConfig: {
                    defid: "tableLayoutConfig",
                    columns: 4
                }
            },
            {
                xtype: "label",
                sciId: "sealabelstatus",
                text: this.b_status,
                cls: "sc-plat-label-left"
            },
            {
                xtype: "panel",
                sciId: "seapanelstatus",
                layout: "table",
                border: false,
                header: false,
                items: [{
                    xtype: "radio",
                    sciId: "radio4",
                    boxLabel: this.b_All,
                    checked: true,
                    ctCls: "sc-plat-radiolabel",
                    bindingData: {
                        defid: "object",
                        targetBinding: ["basicPriceListSearch:PricelistHeader.PricingStatus"]
                    }
                },
                {
                    xtype: "radio",
                    sciId: "radio5",
                    boxLabel: this.b_Active,
                    ctCls: "sc-plat-radiolabel",
                    bindingData: {
                        defid: "object",
                        targetBinding: ["basicPriceListSearch:PricelistHeader.PricingStatus"],
                        selectedValue: "ACTIVE",
                        sourceBinding: "SavedSearch:SearchWrapper.PricelistHeader.PricingStatus"
                    }
                },
                {
                    xtype: "radio",
                    sciId: "radio3",
                    boxLabel: this.b_Inactive,
                    ctCls: "sc-plat-radiolabel",
                    bindingData: {
                        defid: "object",
                        selectedValue: "INACTIVE",
                        targetBinding: ["basicPriceListSearch:PricelistHeader.PricingStatus"],
                        sourceBinding: "SavedSearch:SearchWrapper.PricelistHeader.PricingStatus"
                    }
                }],
                ctCls: "radio-align-padding",
                layoutConfig: {
                    defid: "tableLayoutConfig",
                    columns: 3
                }
            },
            {
                xtype: "label",
                sciId: "seaLblItemID",
                text: this.b_pricelistcontainsitem,
                cls: "sc-plat-label ",
                id: "lblItemID"
            },
            {
                xtype: "trigger",
                sciId: "seaTriggerSearchItem",
                triggerClass: "x-form-search-trigger",
                colspan: 2,
                onTriggerClick: this.openItemSearch,
                scope: this,
                readOnly: false,
                ctCls: "sc-plat-triggerfield-text",
                bindingData: {
                    defid: "object",
                    targetBinding: ["basicPriceListSearch:PricelistHeader.PricelistLineList.PricelistLine.ItemID"],
                    sourceBinding: "SavedSearch:SearchWrapper.PricelistHeader.PricelistLineList.PricelistLine[0].ItemID"
                },
                cls: "sbc-trigger-width"
            },
            {
                xtype: "checkbox",
                sciId: "seaCheckboxDate",
                boxLabel: this.b_showpricelisteffectivetoday,
                cellCls: "sc-plat-checkboxlabel",
                colspan: 2,
                bindingData: {
                    defid: "object",
                    targetBinding: ["basicPriceListSearch:PricelistHeader.CheckboxDateSelected"],
                    checkedValue: "true",
                    unCheckedValue: "false",
                    sourceBinding: "SavedSearch:SearchWrapper.PricelistHeader.CheckboxDateSelected"
                }
            }],
            layoutConfig: {
                defid: "tableLayoutConfig",
                columns: 5
            }
        }],
        border: false
    };
}

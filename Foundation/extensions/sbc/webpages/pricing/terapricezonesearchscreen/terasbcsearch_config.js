/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreen');

sc.pricing.terapricezonesearchscreen.terasbcsearchUIConfig = function() {
    return {
        xtype: "screen",
        sciId: "teraseasearchSCIID",
        header: false,
        layout: "anchor",
        items: [{
            xtype: "panel",
            sciId: "seapnlholder",
            layout: "anchor",
            items: [{
                xtype: "panel",
                layout: "card",
                sciId: "searchCriteriaHolderPanel",
                activeItem: this.getActiveItem(),
                items: [{
                    xtype: "panel",
                    sciId: "seapnlbasicsearch",
                    layout: "anchor",
                    border: false,
                    header: false,
                    autoHeight: true,
                    items: [{
                        defid: "customct",
                        sciId: "seacstmbasicsearch",
                        xtype: this.getBasicSearchXType(),
                        scOwnerScr: this
                    }],
                    pnlBasicSearch: true,
                    autoWidth: true,
                    layoutConfig: {
                        defid: "layoutConfig",
                        columns: 3
                    }
                },
                {
                    xtype: "panel",
                    sciId: "seapnladvancedsearch",
                    layout: "anchor",
                    hidden: true,
                    header: false,
                    border: false,
                    autoScroll: false,
                    autoWidth: true,
                    autoHeight: true,
                    items: [{
                        defid: "customct",
                        sciId: "seacstmadvsearch",
                        xtype: this.getAdvancedSearchXType(),
                        scOwnerScr: this
                    }],
                    pnlBasicSearch: false,
                    layoutConfig: {
                        defid: "layoutConfig",
                        columns: 4
                    }
                }],
                border: false,
                keys: {
                    defid: "object",
                    key: Ext.EventObject.ENTER,
                    fn: this.handleSearch,
                    scope: this
                }
            }],
            header: true,
            border: true,
            bbar: [{
                xtype: "tbfill",
                sciId: "tbfill1"
            },
            {
                xtype: "label",
                sciId: "lblOrderBy",
                text: this.b_OrderBy,
                cls: "sc-plat-label"
            },
            " ", {
                xtype: "combo",
                sciId: "cmbAttrib",
                mode: "local",
                valueField: "Display",
                displayField: "Display",
                ctCls: "sc-plat-combo-selection",
                listClass: "sc-plat-combo-list-item",
                triggerAction: "all",
                typeAhead: true,
                editable: false,
                cls: "sbc-standard-combo",
                resizable: true,
                store: new Ext.data.JsonStore({
                    defid: "jsonstore",
                    fields: ["Display"],
                    sortInfo: {
                        defid: "object",
                        field: "Display",
                        direction: "ASC"
                    }
                })
            },
            " ", " ", " ", {
                xtype: "button",
                sciId: "btnSearch",
                text: this.b_Search,
                handler: this.handleSearch,
                scope: this,
                minWidth: 80,
                iconCls: "sbc-search"
            }],
            //tbar: this.getTopBar(),
            title: this.getHolderTitle(),
            collapsible: true,
            titleCollapse: true,
            sctype: "ScreenMainPanel",
            layoutConfig: {
                defid: "layoutConfig",
                columns: 1
            }
        },
        {
            xtype: "panel",
            sciId: "seapnlsearchresults",
            layout: "anchor",
            hidden: true,
            border: false,
            titleCollapse: true,
            items: [{
                defid: "customct",
                sciId: "seacstmresults",
                xtype: this.getResultsScreenXtype(),
                scOwnerScr: this
            }],
            cls: "sbc-mainpanel-vertSpacer",
            layoutConfig: {
                defid: "layoutConfig",
                columns: 4
            }
        }],
        border: false,
        cls: "sbc-margin",
        sctype: "ScreenBasePanel",
        autoScroll: true
    };
}

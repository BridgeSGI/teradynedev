<%
// Licensed Materials - Property of IBM
// IBM Sterling Business Center
// (C) Copyright IBM Corp. 2009, 2011 All Rights Reserved.
// US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
%>
 
<%@page import="com.sterlingcommerce.ui.web.framework.utils.SCUIContextHelper"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIUserPreferences"%>
<%@page import="com.sterlingcommerce.ui.web.framework.context.SCUIContext"%>
<%@page import="com.sterlingcommerce.sbc.core.utils.SBCJSONUtils"%> 
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Document"%>

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@page import="com.yantra.yfc.log.YFCLogCategory"%>

<%
YFCLogCategory log = YFCLogCategory.instance("Test.class");

System.out.println("\n\nReady To out-put All the request-attributes received from request in ZONE Search Result Screen- ");
log.info("\n\nReady To out-put All the request-attributes received from request in ZONE Search Result Screen- ");

Enumeration enAttr = request.getAttributeNames(); 
while(enAttr.hasMoreElements()){
String attributeName = (String)enAttr.nextElement();
System.out.println("\nAttribute Name - "+attributeName+", Value - "+(request.getAttribute(attributeName)).toString());
log.info("\nAttribute Name - "+attributeName+", Value - "+(request.getAttribute(attributeName)).toString());
}
%>

<%
response.setContentType("text/javascript");
SCUIContext uiContext = SCUIContextHelper.getUIContext(request, response);
Object ret = uiContext.getAttribute("teraPricelistLineListForItemSearchOutput");
Document doc = ((Element)ret).getOwnerDocument();
String json = SBCJSONUtils.getJSONFromXML(request, response, doc);
%>
{output:<%=json%>}["output"]

<%
log.info("Calling GET-REQUEST-DOM in ZONE Search Result Screen: \n"+ getRequestDOM());
System.out.println("Calling GET-REQUEST-DOM in ZONE Search Result Screen: \n"+ getRequestDOM());  
%>

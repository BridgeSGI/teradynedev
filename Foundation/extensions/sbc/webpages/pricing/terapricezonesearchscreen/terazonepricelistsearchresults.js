/*
@author Sourabh Goyal, Bridge Solutions Group
*/

Ext.namespace('sc.pricing.terapricezonesearchscreen');

sc.pricing.terapricezonesearchscreen.ClassTeraZoneSearchResults = function(config) {
	this.isForPopup = config.scOwnerScr.isForPopup;
	this.len=0;
	var singleSelect = false;
	var header = '<div class="x-grid3-hd-checker"> </div>';
	var ownerScreen = config.scOwnerScr;
	if(ownerScreen && ownerScreen.hasOwnProperty("singleSelect")){
		singleSelect = config.scOwnerScr.singleSelect;
		Ext.apply(config, {singleSelect : singleSelect});
		header="";
	}if(this.isForPopup!==true){
		singleSelect=true;
		header="";
	}


	this.checkBoxSelectionModel=new Ext.grid.CheckboxSelectionModel({
		listeners:{
			'selectionchange': this.selectionChangeHandler,
			scope:this
		},
		singleSelect:singleSelect,
		header:header
	});
	this.pagingToolBar = new sc.sbc.common.PagingToolbar({listeners : {'change' : this.selectionChangeHandler, scope : this}}),
    sc.pricing.terapricezonesearchscreen.ClassTeraZoneSearchResults.superclass.constructor.call(this, config);
    this.find("sciId", "seaEditorGridResults")[0].addEvents('selectedmultiple','handlerowdblclick');
}
Ext.extend(sc.pricing.terapricezonesearchscreen.ClassTeraZoneSearchResults, sc.plat.ui.ExtensibleScreen, {
    className: 'sc.pricing.terapricezonesearchscreen.ClassTeraZoneSearchResults',
    getUIConfig: sc.pricing.terapricezonesearchscreen.ClassTeraZoneSearchResultsUIConfig,
			getUrl : function() {
				return sc.sbc.helper.AppHelper.getAjaxURL("teraManagePricelist_search", "/sbc/pricing");
			},
    copyPricelistScreen:null,
    commonCodeOutput:null,

		namespaces: {
                        source: ["getPriceListOutput"]

            },
		selectionChangeHandler : function(sel){
			var gridResults=this.find("sciId","seaEditorGridResults")[0];
			var tBar=gridResults.getTopToolbar();
			bttn=tBar.items.itemAt(0);
			var len=this.checkBoxSelectionModel.getSelections().length;
			if(len > 1 || len===0){

			bttn.setDisabled(true);
			}else{
				bttn.setDisabled(false);
			}
		},
       copyBttnHandler:function(){
				var copyPricelistScreen=new sc.sbc.pricing.pricelist.copypricelist();
				var elemForCopy=this.checkBoxSelectionModel.getSelections()[0].data;
				var inputForScreen={'PricelistCopyElem' : elemForCopy};
					/*copyPricelistScreen.getModelFromResultsScreen({
					'PricelistCopyElem' : elemForCopy
				});*/

				var winCopyPricelist = sc.sbc.util.WindowUtils.getWindow({
                              screen : copyPricelistScreen,
                              buttons : sc.sbc.util.WindowUtils.CREATECLOSE,
                              handlers : {"ok" : copyPricelistScreen.okBttnHandler},
                              scope : copyPricelistScreen,
                              inputData : inputForScreen,
                              windowConfig:{maximizable : false,closeAction:"close"},
                              setDataHandler : copyPricelistScreen.getModelFromResultsScreen
                        });
				/*var modelElem=this.getModelForCopyPricelist();
					var winCopyPricelist = new Ext.Window({
					title : ' Copy Pricelist',
					closable : true,
					autoHeight : false,
					autoWidth : false,
					resizable : false,
					border : false,
					modal : true,
					plain : true,
					layout : 'anchor',
					height : 100,
					width : 300,
					items : [copyPricelistScreen],
					sciId : 'seacopyPricelist',
					id : 'seacopyPricelist',
					scope : this
				});*/

		if(this.checkBoxSelectionModel.getSelections().length > 0){
				winCopyPricelist.show();
			}
		},

		linkRenderer: function(attrName, metadata, record , rowindex, colIndex , store) {
			return ("<a href= '#'>" + attrName + "</a>");
	},

		openSummary: function(grid , rowIndex, columnIndex, e){
			grid.fireEvent('selectedmultiple', grid);
			var dataIndex=grid.getColumnModel().getDataIndex(columnIndex);
			if(dataIndex == 'PricelistName' && (e.getTarget("a"))) {

				if(this.scOwnerScr.isForPopup ){
					grid.fireEvent('handlerowdblclick', grid);
				}else{
					var headerKey=grid.getStore().data.itemAt(rowIndex).json.PricelistHeaderKey;
					var inputObj = {};
					inputObj.PricelistLine={};
    				inputObj.PricelistLine.PricelistHeaderKey=headerKey;
					var container = Ext.getCmp("mainBodyPanel");
					seaAjaxUtils.request({
					action : "pricelistdetail",
					actionNS : sc.sbc.App.PricingStrutsNS,
					params:{"pricelistDetailsInput":"{PricelistHeader:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
							//"pricelistLineListInput":"{PricelistLine:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
							"pricelistAssignmentInput" : "{PricelistAssignment:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
							"getPricelistSpecificAssignmentListInput" : "{PricelistAssignment:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
							"isFromSearchScreen" : "Y",
							scPageSize:15,
						  scPaginationStrategy : "NEXTPAGE",
						  managePricelistLineInput : Ext.encode(inputObj)},

					success : this.resHandlerForPriceListDetail,
					scope : this
				});
			}

		}

		},
		getCurrencyDesc : function(cellText, cell, rowData) {
			//var currencyKeyElem = sc.sbc.common.cache.currency.CurrencyManager.getCurrencyObjFromValue(cellText);
			//var description=currencyKeyElem.CurrencyDescription;
			return;
		},

		getStatusDesc:function(cellText, cell, rowData){
			var outputToCompare=sc.sbc.core.data.DataManager.getData("StatusCommonCode");
//			var outputToCompare=sc.sbc.core.data.DataManager.getData("CommonCodesForStatus");
			if(outputToCompare){
				var commonCodeList=outputToCompare.CommonCodeList;
				if(commonCodeList){
					var commonCode=commonCodeList.CommonCode;
					if(commonCode){
						var length=commonCode.length;
						for(var i=0;i<length;i++){

							var commonCodeElem=commonCode[i];
							var codeValue=commonCodeElem.CodeValue;
							if(codeValue === cellText)
								return commonCodeElem.CodeShortDescription;

						}

					}
				}
			}

		},
		checkBoxSelectionModel:new Ext.grid.CheckboxSelectionModel(),
		getModelForCopyPricelist:function(){
			var selectionModelElem=this.checkBoxSelectionModel.getSelections()[0].data;
			var pricelistHeaderKeyToCopy=selectionModelElem.PricelistHeaderKey;


		},
		resHandlerForPriceListDetail : function(res, options){
		eval(res.responseText);
	},

	getResultGrid : function() {
				return this.find("sciId", "seaEditorGridResults")[0];
			},

	getCheckBoxSelectionModel:function(){
		return this.checkBoxSelectionModel;
	},
	setModelForComponents:function(res){
		this.setModel(res.output,"getPriceListOutput");
	},

	performSearch : function(searchInput){
		//model[0].PricelistHeader.OrganizationCode="xml:CurrentContextOrg:/Organization/@OrganizationCode"
		//var pricelistLineList=searchInput.PricelistHeader.PricelistLineList;
		/*if(pricelistLineList){
			var pricelistLine=pricelistLineList.PricelistLine;
			var itemId=pricelistLine.ItemID;
			if(!itemId ){
				delete searchInput.PricelistHeader.PricelistLineList;
			}
		}*/

		if(searchInput.PricelistLine.PricelistHeader.CheckboxDateSelected === "true"){
			var currentDate=sc.plat.DateFormatter.convertToServerFormat(new Date, 'DATE');

			searchInput.PricelistLine.PricelistHeader.StartDateActive=currentDate;
			searchInput.PricelistLine.PricelistHeader.EndDateActive=currentDate;

			searchInput.PricelistLine.PricelistHeader.StartDateActiveQryType="LE";
			searchInput.PricelistLine.PricelistHeader.EndDateActiveQryType="GE";

		}
		//delete searchInput.PricelistHeader.CheckboxDateSelected;


		var inCreatePriceListScreen=this.ownerScr.inCreatePriceListScreen;
		if(inCreatePriceListScreen === "Y"){
			searchInput.PricelistLine.PricelistHeader.InheritFromPricelistHeaderKeyQryType="ISNULL";
		}
		this.pagingToolBar.on('beforerefresh', this.beforePageRefresh, this);
		var tblSearchResults = this.find("sciId", "seaEditorGridResults")[0];
		tblSearchResults.getStore().startPagination({teraPricelistLineListForItemSearchInput : Ext.encode(searchInput)});
		if(!sc.sbc.core.context.JSContext.isPricingOrg())
					sc.sbc.util.CoreUtils.getTopToolBarItemBySciId(this.getResultGrid(), "seaTBttnCopy").hide();
		/*seaAjaxUtils.request({
				action : "teraManagePricelist_search",
				actionNS : sc.sbc.App.PricingStrutsNS,
				inputNS : "teraPricelistLineListForItemSearchInput",
				inputObj : searchInput,
				success : this.resHandlerForOutput,
				scope : this
		});*/

	},

	resHandlerForOutput:function(res,options){
			var res = Ext.decode(res.responseText);
			this.setModelForComponents(res);
	},

	beforePageRefresh : function(store, records) {
		var inCreatePriceListScreen=this.ownerScr.inCreatePriceListScreen
			var noOfRecords=store.getCount();
			var noOfPages=store.getTotalPages();
			if(noOfRecords === 1 && noOfPages === 1 && inCreatePriceListScreen!=="Y"){
				var recordToOpen=store.getRange(store.getCount()-1,store.getCount())[0];
				var headerKey=recordToOpen.data.PricelistHeaderKey;
				var lineInputObj = {};
				lineInputObj.PricelistLine={};
    			lineInputObj.PricelistLine.PricelistHeaderKey=headerKey;
				seaAjaxUtils.request({
				action : "pricelistdetail",
				actionNS : sc.sbc.App.PricingStrutsNS,
				params:{"pricelistDetailsInput":"{PricelistHeader:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
						//"pricelistLineListInput":"{PricelistLine:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
						"pricelistAssignmentInput" : "{PricelistAssignment:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
						"getPricelistSpecificAssignmentListInput" : "{PricelistAssignment:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
						"isFromSearchScreen" : "Y",
						scPageSize:15,
				scPaginationStrategy : "NEXTPAGE",
				managePricelistLineInput : Ext.encode(lineInputObj)},

				success : this.resHandlerForPriceListDetail,
				scope : this
			});
		}
				var collapse = true;
				if(records.length === 0)
					collapse = false;
				//this.fireEvent("onsearch", collapse);
			},

	showLinksInColumns:function(val){
		return sc.sbc.util.RendererUtils.renderColumnInformationAsLink(val);
	},

	showLinksWithImageInColumns:function(value,data,record){
		return sc.sbc.util.RendererUtils.showLinksWithImageInColumns(value,data,record);
	},


	returnFormattedEndDate:function(endDate){
		if(!(endDate instanceof Date)){
		 	endDate=Date.parseDate(endDate,"c");
		}
		var formattedEndDate=endDate.dateFormat(sc.plat.Userprefs.getDateFormat());
		return formattedEndDate;

	},
	returnFormattedStartDate:function(startDate){
		if(!(startDate instanceof Date)){
		 	startDate=Date.parseDate(startDate,"c");
		}
		var formattedStartDate=startDate.dateFormat(sc.plat.Userprefs.getDateFormat());
		return formattedStartDate;

	},

	handleCellDoubleClick:function(grid, rowIndex, e){
		//var dataIndex=grid.getColumnModel().getDataIndex(columnIndex);
			//(dataIndex == 'PricelistName') {

				if(this.scOwnerScr.isForPopup ){
					grid.fireEvent('handlerowdblclick', grid);
				}else{
					var headerKey=grid.getSelectionModel().getSelections()[0].data.PricelistHeaderKey;
					var container = Ext.getCmp("mainBodyPanel");
					var lineInputObj = {};
					lineInputObj.PricelistLine={};
    				lineInputObj.PricelistLine.PricelistHeaderKey=headerKey;
					seaAjaxUtils.request({
					action : "pricelistdetail",
					actionNS : sc.sbc.App.PricingStrutsNS,
					params:{"pricelistDetailsInput":"{PricelistHeader:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
							//"pricelistLineListInput":"{PricelistLine:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
							"pricelistAssignmentInput" : "{PricelistAssignment:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
							"getPricelistSpecificAssignmentListInput" : "{PricelistAssignment:{PricelistHeaderKey:" + "'" + headerKey+ "'" +"}}",
							"isFromSearchScreen" : "Y",
							scPageSize:15,
							scPaginationStrategy : "NEXTPAGE",
							managePricelistLineInput : Ext.encode(lineInputObj)},

					success : this.resHandlerForPriceListDetail,
					scope : this
				});
			}

		//}

	}
});
Ext.reg('terazonepricelistsearchresults', sc.pricing.terapricezonesearchscreen.ClassTeraZoneSearchResults);
